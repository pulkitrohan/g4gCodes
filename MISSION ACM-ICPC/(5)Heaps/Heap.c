#include<stdio.h>
int heap_size = 10;
int n = 10;

int parent(int i)
{
    return i/2;
}

int left(int i)
{
    return 2*i;
}

int right(int i)
{
    return 2*i + 1;
}

void build_max_heap(int *a)
{
    int i;
    for(i = n/2 ; i > 0 ; i--)
        Max_heapify(a,i);
}

void Max_heapify(int *a,int i)
{
    int l = left(i);
    int r = right(i);
    int temp,largest;
    if(l <= heap_size && a[l] > a[i])
        largest = l;
    else
        largest = i;
    if(r <= heap_size && a[r] > a[largest])
        largest = r;
    if(largest != i)
    {
            temp = a[i];
            a[i] = a[largest];
            a[largest] = temp;
            Max_heapify(a,largest);
    }
}

int heap_maximum(int *a)
{
    return a[1];
}

int main(void)
{
    int n,i,a[100];
    printf("Enter the Size of Heap : ");
    scanf("%d",&n);
    printf("Enter the Elements of Heap : ");
    for(i=1;i<=n;i++)
        scanf("%d",&a[i]);
    build_max_heap(a);
    printf("After Max-Heapifying : ");
    for(i=1;i<=n;i++)
        printf("%d ",a[i]);
        printf("\nMaximum Element is %d",heap_maximum(a));
    return 0;
}
