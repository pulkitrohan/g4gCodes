#include<bits/stdc++.h>
using namespace std;
struct Heap
{
    int *elements;
    int size;
};

Heap *CreateHeap(int n)
{
    Heap *H = (Heap *)malloc(sizeof(Heap));
    H->size = n;
    H->elements = (int *)malloc(sizeof(int)*2*n);
    return H;
}
int parent(int i)
{
    return i/2;
}

int left(int i)
{
    return 2*i;
}

int right(int i)
{
    return 2*i + 1;
}

void Max_heapify(Heap *H,int i)
{
    int l = left(i);
    int r = right(i);
    int largest = i;
    if(l <= H->size && H->elements[l] > H->elements[largest])
        largest = l;
    if(r <= H->size && H->elements[r] > H->elements[largest])
        largest = r;
    if(largest != i)
    {
            swap(H->elements[largest],H->elements[i]);
            Max_heapify(H,largest);
    }
}

void build_max_heap(Heap *H)
{
    int i;
    for(i = H->size/2 ; i > 0 ; i--)
        Max_heapify(H,i);
}



int PrintMax(Heap *H)
{
	if(H->size > 0)
		return H->elements[1];
}
int ExtractMax(Heap *H)
{
    if(H->size < 1)
    {
        printf("Heap Underflow");
        return -1;
    }
    else
    {
        int max = H->elements[1];
        H->elements[1] = H->elements[H->size];
        H->size--;
        Max_heapify(H,1);
        return max;
    }
}
void Enqueue(Heap *H,int key)
{
    H->size++;
	H->elements[H->size] = key;
	swap(H->elements[1],H->elements[H->size]);
	build_max_heap(H);
}

int main(void)
{
    int N;
	cin>>N;
	Heap *H = CreateHeap(N);
	for(int i=1;i<=N;i++)
		cin>>H->elements[i];
	build_max_heap(H);
	cout<<PrintMax(H)<<endl;
	Enqueue(H,100);
	Enqueue(H,60);
	cout<<PrintMax(H);
	cout<<endl;
	for(int i=1;i<=H->size;i++)
		cout<<H->elements[i]<<" ";
	cout<<endl;
	cout<<ExtractMax(H)<<endl;
	for(int i=1;i<=H->size;i++)
		cout<<H->elements[i]<<" ";
	cout<<endl;
	cout<<PrintMax(H)<<endl;
}
