#include<stdio.h>
typedef struct heap
{
    int *elements;
    int size;
}heap;

heap *CreateHeap(int n)
{
    heap *H = (heap *)malloc(sizeof(heap));
    H->size = n;
    H->elements = (int *)malloc(sizeof(int)*n);
    return H;
}
int parent(int i)
{
    return i/2;
}

int left(int i)
{
    return 2*i;
}

int right(int i)
{
    return 2*i + 1;
}

void Max_heapify(heap *H,int i)
{
    int l = left(i);
    int r = right(i);
    int temp,largest;
    if(l <= H->size && H->elements[l] > H->elements[i])
        largest = l;
    else
        largest = i;
    if(r <= H->size && H->elements[r] > H->elements[largest])
        largest = r;
    if(largest != i)
    {
            temp = H->elements[i],H->elements[i] = H->elements[largest],H->elements[largest] = temp;
            Max_heapify(H,largest);
    }
}

void build_max_heap(heap *H)
{
    int i;
    for(i = H->size/2 ; i > 0 ; i--)
        Max_heapify(H,i);
}

int heap_maximum(heap *H)
{
    return H->elements[1];
}
void Heapsort(heap *H)
{
    build_max_heap(H);
    int i,temp,n = H->size;
    for(i=n;i>=2;i--)
    {
        temp = H->elements[1];
        H->elements[1] = H->elements[i];
        H->elements[i] = temp;
        H->size--;
        Max_heapify(H,1);
    }
}

int main(void)
{
    int n,i,a[100];
    printf("Enter the Size of Heap : ");
    scanf("%d",&n);
    heap *H = CreateHeap(n);
    printf("Enter the Elements of Heap : ");
    for(i=1;i<=n;i++)
        scanf("%d",&H->elements[i]);
        Heapsort(H);
  printf("After HeapSort : ");
  for(i=1;i<=n;i++)
    printf("%d " ,H->elements[i]);
    return 0;
}

