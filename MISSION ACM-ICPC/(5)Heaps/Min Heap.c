#include<stdio.h>
typedef struct heap
{
    int *elements;
    int size;
}heap;

heap *CreateHeap(int n)
{
    heap *H = (heap *)malloc(sizeof(heap));
    H->size = n;
    H->elements = (int *)malloc(sizeof(int)*n);
    return H;
}
int parent(int i)
{
    return i/2;
}

int left(int i)
{
    return 2*i;
}

int right(int i)
{
    return 2*i + 1;
}

void build_min_heap(heap *H)
{
    int i;
    for(i = H->size/2 ; i > 0 ; i--)
        Min_heapify(H,i);
}

void Min_heapify(heap *H,int i)
{
    int l = left(i);
    int r = right(i);
    int temp,smallest;
    if(l <= H->size && H->elements[l] < H->elements[i])
        smallest = l;
    else
        smallest = i;
    if(r <= H->size && H->elements[r] < H->elements[smallest])
        smallest = r;
    if(smallest != i)
    {
            temp = H->elements[i],H->elements[i] = H->elements[smallest],H->elements[smallest] = temp;
            Min_heapify(H,smallest);
    }
}

int heap_minimum(heap *H)
{
    return H->elements[1];
}
int Dequeue(heap *H)
{
    if(H->size < 1)
    {
        printf("Heap Underflow");
        return ;
    }
    else
    {
        int max = H->elements[1];
        H->elements[1] = H->elements[H->size];
        H->size--;
        Min_heapify(H,1);
        return max;
    }
}
void Enqueue(heap *H,int key)
{
    int temp;
    H->size++;
    H->elements[H->size] = key;
    temp = H->elements[1],H->elements[1] = H->elements[H->size],H->elements[H->size] = temp;
    build_min_heap(H);
}
int main(void)
{
    int n,i,a[100],key;
    printf("Enter the Size of Heap : ");
    scanf("%d",&n);
    heap *H = CreateHeap(n);
    printf("Enter the Elements of Heap : ");
    for(i=1;i<=n;i++)
        scanf("%d",&H->elements[i]);
        build_min_heap(H);
    printf("After Max-Heapifying : ");
    for(i=1;i<=n;i++)
        printf("%d ",H->elements[i]);
        printf("Enter the new element : ");
        scanf("%d",&key);
        Enqueue(H,key);
        printf("New Min-Heapifying : ");
        for(i=1;i<=H->size;i++)
        printf("%d ",H->elements[i]);
        printf("\nMinimum Element is %d",heap_minimum(H));
        printf("\nMinimum Element is Extracted");
        int max = Dequeue(H);
        printf("\nNow new Minimum is %d",heap_minimum(H));
    return 0;
}
