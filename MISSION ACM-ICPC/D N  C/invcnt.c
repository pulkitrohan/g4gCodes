#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define LL long long int

LL combine(long *A, long p, long q, long r) {
    LL cnt = 0;
        int i, j, k;
        int n1 = q - p + 1, n2 = r - (q + 1) + 1;
        int *B = (int *) malloc((n1 + 1) * sizeof(int));
        int *C = (int *) malloc((n2 + 1) * sizeof(int));

        for (j = 0; j < n1; ++j)
                B[j] = A[j + p];
        for (k = 0; k < n2; ++k)
                C[k] = A[k + q + 1];
        B[n1] = C[n2] = INT_MAX;

        for (i = p, j = 0, k = 0; i <= r; ++i)
        {
            if (B[j] <= C[k])
                A[i] = B[j++];
            else
                A[i] = C[k++], cnt += n1 - j;
        }

        free(B);
        free(C);

        return cnt;
}

LL merge(long *A, long p, long r) {
        LL cnt = 0;

        if (p < r) {
                int q = (p + r) / 2;
                cnt += merge(A, p, q);
                cnt += merge(A, q + 1, r);
                cnt += combine(A, p, q, r);
        }

        return cnt;
}

int main() {

            long  i;

            long n=5;
                long *A = (long *) malloc(n * sizeof(long));
                for (i = 0; i < n; ++i)
                        scanf("%ld", &A[i]);
                printf("%lld", merge(A, 0, n - 1));

                free(A);
       // fclose(fp1);
        return 0;
}
