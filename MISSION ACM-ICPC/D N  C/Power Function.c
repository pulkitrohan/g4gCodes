#include<stdio.h>

int Power(int a,int b)
{
    if(b == 0)
        return 1;
    int temp = Power(a,b/2);
    if(b%2 == 0)
        return temp*temp;
    else
        return temp*temp*a;
}

int main(void)
{
    int a,b;
    scanf("%d %d",&a,&b);
    printf("%d ",Power(a,b));
    return 0;
}
