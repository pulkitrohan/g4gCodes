#include<bits/stdc++.h>
using namespace std;

void generate(string S,int one,int zero,int N,int index)
{
	if(index == N)
	{
		cout<<S<<endl;
	}
	else
	{
		if(one > zero)
		{
			S[index] = '0';
			generate(S,one,zero+1,N,index+1);
		}
		
		S[index] = '1';
		generate(S,one+1,zero,N,index+1);
		
		
	}
}

int main()
{
	int N = 3;
	string S(N,'\0');
	generate(S,0,0,N,0);
	return 0;
}