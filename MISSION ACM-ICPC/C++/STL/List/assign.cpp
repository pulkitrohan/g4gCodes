//Assign function
//Assign Linked list values ie it changes the LL completely
//So suppose we have a LL with elements lets say 1 2 3
//Now we want to replace this LL with another LL with elements
//say 10 20 30
//Then we can use assign function
//IN LESS WORDS : Assigns new contents to the list container, replacing its
//                current contents, and modifying its size accordingly.


#include<iostream>
#include<list>
using namespace std;

typedef list<int> li;
typedef list<int>::iterator lii;
int main(void)
{
    li one;  //Declaring list
    li second;

    one.assign(7,100);    //Assign element 100 7 times
    second.assign(one.begin(),one.end());
    cout<<"Content of one : ";
    for(lii it = one.begin(); it != one.end();it++)
        cout <<*it<<" ";
    cout<<"\nContent of second : ";
    for(lii it = second.begin(); it != second.end();it++)
        cout <<*it<<" ";
    int myint[] = {1,2,3};
    one.assign(myint,myint + 3);
    cout<<"\nSize of one is "<<one.size();
    return 0;
}
