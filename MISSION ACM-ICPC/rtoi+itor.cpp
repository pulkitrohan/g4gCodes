#include<bits/stdc++.h>
using namespace std;
int H[256] = {0};

int rtoi(string S)
{
	int num = 0,prev = 0;
	for(int i=S.length()-1;i>=0;i--)
	{
		int temp = H[S[i]];
		if(temp < prev)
			num -= temp;
		else
			num += temp;
		prev = temp;
	}
	return num;
}

string itor(int N)
{
	string S = "";
	while(N)
	{
		if(N >= 1000)
		{
			S += "M";
			N -= 1000;
		}
		else if(N >= 900)
		{
			S += "CM";
			N -= 900;
		}
		
		else if(N >= 500)
		{
			S += "D";
			N -= 500;
		}
		
		else if(N >= 400)
		{
			S += "CD";
			N -= 400;
		}
		
		else if(N >= 100)
		{
			S += "C";
			N -= 100;
		}
		
		else if(N >= 90)
		{
			S += "XC";
			N -= 90;
		}
		
		else if(N >= 50)
		{
			S += "L";
			N -= 50;
		}
		
		else if(N >= 40)
		{
			S += "XL";
			N -= 40;
		}
		
		else if(N >= 10)
		{
			S += "X";
			N -= 10;
		}
		
		else if(N >= 9)
		{
			S += "IX";
			N -= 9;
		}
		
		else if(N >= 5)
		{
			S += "V";
			N -= 5;
		}
		
		else if(N >= 4)
		{
			S += "IV";
			N -= 4;
		}
		else if(N >= 1)
		{
			S += "I";
			N -= 1;
		}
	}
	return S;
}


int main(void)
{
	H['I'] = 1;
	H['V'] = 5;
	H['X'] = 10;
	H['L'] = 50;
	H['C'] = 100;
	H['D'] = 500;
	H['M'] = 1000;
	string S;
	cin>>S;
	int N = rtoi(S);
	cout<<N<<endl;
	cout<<itor(N)<<endl;
	return 0;
}