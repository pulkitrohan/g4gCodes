#include<bits/stdc++.h>
using namespace std;

int maxProductTriplet(int *A,int N)
{
	if(N < 3)
		return -1;
	
	int maxA = INT_MIN,maxB = INT_MIN,maxC = INT_MIN;
	int minA = INT_MAX,minB = INT_MAX;
	
	for(int i=0;i<N;i++)
	{
		if(A[i] > maxA)
		{
			maxC = maxB;
			maxB = maxA;
			maxA = A[i];
		}
		else if(A[i] > maxB)
		{
			maxC = maxB;
			maxB = A[i];
		}
		else if(A[i] > maxC)
			maxC = A[i];
			
		if(A[i] < minA)
		{
			minB = minA;
			minA = A[i];
		}
		else if(A[i] < minB)
			minB = A[i];
	}
	return max(minA*minB*maxA,maxA*maxB*maxC);
}


int main(void)
{
	int A[] = {1,-4,3,-6,7,0};
	int N = sizeof(A)/sizeof(A[0]);
	int ans = maxProductTriplet(A,N);
	if(ans == -1)
		cout<<"No Triplet Exists\n";
	else
		cout<<"Maximum Product is "<<ans;
	return 0;
}