#include<stdio.h>
#include<stdlib.h>

struct LL
{
	void *data;
	struct LL *next;
};


void insert(struct LL **head,void *data,size_t size)
{
	struct LL *temp = (struct LL *)malloc(sizeof(struct LL ));
	temp->next = (*head);
	temp->data = malloc(size);
	int i;
	for(i=0;i<size;i++)
		*(char *)(temp->data + i) = *(char *)(data + i);
	(*head) = temp;
}

void printInt(void *data)
{
	printf("%d ",*(int *)data);
}

void printFloat(void *data)
{
	printf("%.2f ",*(float *)data);
}

void printList(struct LL *head,void (*fptr)(void *))
{
	while(head)
	{
		(*fptr)(head->data);
		head = head->next;
	}
}

int main(void)
{
	struct LL *head = NULL;
	size_t int_size = sizeof(int);
	int A[] = {10,20,30,40,50};
	int i;
	for(i=4;i>=0;i--)
		insert(&head,&A[i],int_size);
	printList(head,printInt);
	
	printf("\n");
	
	head = NULL;
	size_t float_size = sizeof(float);
	float B[] = {10.1,20.2,30.3,40.4,50.5};
	for(i=4;i>=0;i--)
		insert(&head,&B[i],float_size);
	printList(head,printFloat);
	
	
	return 0;
}