#include<bits/stdc++.h>
using namespace std;

int highestProductOf3(int *A,int N)
{
	if(N < 3)
		return 0;
	int highest = max(A[0],A[1]);
	int lowest = min(A[0],A[1]);
	
	int highest_two = A[0]*A[1];
	int lowest_two = A[0]*A[1];
	
	int highest_three = A[0]*A[1]*A[2];
	
	for(int i=2;i<N;i++)
	{
		highest_three = max(highest_three,max(highest_two*A[i],lowest_two*A[i]));
		highest_two = max(highest_two,max(highest*A[i],lowest*A[i]));
		lowest_two = min(lowest_two,min(lowest*A[i],highest*A[i]));
		lowest = min(A[i],lowest);
		highest = max(A[i],highest);
	}
	return highest_three;
}

int main(void)
{
	int A[] = {1,10,-5,1,-100};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<highestProductOf3(A,N);
	return 0;
}