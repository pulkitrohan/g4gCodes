#include<bits/stdc++.h>
using namespace std;

int LongSubarrayEq(int *A,int *B,int N)
{
	int C[N];
	memset(C,0,sizeof(C));
	for(int i=0;i<N;i++)
		C[i] = A[i] - B[i];
	for(int i=1;i<N;i++)
		C[i] += C[i-1];
	int ans = INT_MIN;
	map<int,int> M;
	map<int,int>::iterator it;
	for(int i=0;i<N;i++)
	{
		it = M.find(C[i]);
		if(it == M.end())
			M.insert(it,make_pair(C[i],i));
		else
			ans = max(ans,i-it->second);
	}
	return ans;
}

int main(void)
{
	int A[] = {0,1,0,0,0,1,0,0,1,0,0};
	int B[] = {1,0,1,1,1,0,1,1,0,1,0};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<LongSubarrayEq(A,B,N);
	return 0;
}