#include <iostream>
using namespace std;

void FindFrequency(int *A,int N)
{
	int K = N+1;
	for(int i=0;i<N;i++)
		A[(A[i]-1)%K] += K;
	for(int i=0;i<N;i++)
		cout<<i+1<<" "<<A[i]/(N+1)<<endl;
}

void PrintFrequency2(int *A,int N)
{
	int pos = 0;
	while(pos < N)
	{
		int expectedPos = A[pos]-1;
		if(A[pos] > 0 && A[expectedPos] > 0)
		{
			swap(A[pos],A[expectedPos]);
			A[expectedPos] = -1;
		}
		else if(A[pos] > 0)
		{
			A[expectedPos]--;
			A[pos++] = 0;
		}
		else
			pos++;
	}
	for(int i=0;i<N;i++)
		cout<<i+1<<" "<<abs(A[i])<<endl;
}

int main()
{
	int A[] = {1, 3, 5, 7, 9, 1, 3, 5, 7, 9, 1};
	int N = sizeof(A)/sizeof(A[0]);
	PrintFrequency2(A,N);
	return 0;
}