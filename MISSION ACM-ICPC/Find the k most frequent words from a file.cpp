#include<bits/stdc++.h>
using namespace std;
#define SIZE 26

struct TrieNode {
	bool isEnd;
	int frequency;
	int minHeapIndex;
	TrieNode *child[SIZE];
}

struct MinHeapNode
{
	TrieNode *root;
	int frequency;
	string word;
};

struct MinHeap
{
	int maxSize;
	int currentSize;
	MinHeapNode *array;
};

MinHeap *createMinHeap(int size) {
	MinHeap *minHeap = new MinHeap;
	minHeap->maxSize = size;
	minHeap->currentSize = 0;
	minHeap->array = new MinHeapNode[size];
	return minHeap;
}

void printKMostFreq(FILE *fp, int k) {
	MinHeap *MinHeap = createMinHeap(k);
	TrieNode *root = NULL;
	char buffer[SIZE];
	while(fscanf(fp,"%s", buffer) != EOF) {
		insertTrieAndHeap(buffer, root, minHeap);
	}

}

int main(void) {
	int k = 5;
	FILE *fp = fopen("file.txt", "r");
	if(fp == null) {
		cout<<"File doesn't exist";
	}
	else {
		printKMostFreq(fp, k);
	}
	return 0;
}