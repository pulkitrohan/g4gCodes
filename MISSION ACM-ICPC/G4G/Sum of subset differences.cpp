#include<bits/stdc++.h>
using namespace std;

int sumSetDiff(int *A,int N)
{
	int left = 0,right = 0;
	int val = 1;
	for(int i=0;i<N;i++)
	{
		left += A[i]*val;
		val *= 2;
	}
	val = 1;
	for(int i=N-1;i>=0;i--)
	{
		right += A[i]*val;
		val *= 2;
	}
	return left-right;
}

int main(void){
	int N = 4;
	int A[] = {5,2,9,6};
	cout<<sumSetDiff(A,N);
	return 0;
}