#include<bits/stdc++.h>
using namespace std;

void bfs(int N,int M, int i)
{
	queue<int> Q;
	Q.push(i);
	while(!Q.empty()){
		int num = Q.front();
		Q.pop();
		if(num <= M && num >= N)
			cout<<num<<" ";
		if(i == 0 || num > M)
			continue;
		int lastDigit = num%10;
		int stepPrev = num*10 + lastDigit-1;
		int stepNext = num*10 + lastDigit+1;
		if(lastDigit == 0)
			Q.push(stepNext);
		else if(lastDigit == 9)
			Q.push(stepPrev);
		else{
			Q.push(stepPrev);
			Q.push(stepNext);
		}
	}
}

int main(void){
	int N = 0,M = 21;
	for(int i=0;i<=9;i++)
		bfs(N,M,i);
	return 0;
}