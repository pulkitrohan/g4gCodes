#include<bits/stdc++.h>
using namespace std;
#define MAX 100

int FindK(int A[MAX][MAX],int n,int m,int k){
	
	if(n < 1 || m < 1)
		return -1;
	if(k <= m)
		return A[0][k-1];
	if(k <= (m+n-1))
		return A[k-m][m-1];
	if(k <= (m+n-1+m-1))
		return A[n-1][m-1-(k-(m+n-1))];
	if(k <= (m+n-1+m+1+n-2))
		return A[n-1-(k-(m+n-1+m-1))][0];
	return findK((int (*)[MAX])(&(A[1][1])), n-2,m-2, k-(2*n+2*m-4));
}

int main(void)
{
	int A[MAX][MAX] = {{1,2,3,4,5,6},
					   {7,8,9,10,11,12},
					   {13,14,15,16,17,18}};
	int k = 17;
	cout<<FindK(A,3,6,k);
	return 0;
}