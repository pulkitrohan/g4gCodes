#include<bits/stdc++.h>
using namespace std;

int countSub(string S)
{
	int hash[256];
	memset(hash,-1,sizeof(hash));
	int N = S.length();
	int dp[N+1];
	dp[0] = 1;
	for(int i=1;i<=N;i++)
	{
		dp[i] = dp[i-1]*2;
		if(hash[S[i-1]] != -1)
			dp[i] -= dp[hash[S[i-1]]];
		hash[S[i-1]] = i-1;
	}
	return dp[N];
}


int main(void)
{
	cout<<countSub("gfg");
	return 0;
}