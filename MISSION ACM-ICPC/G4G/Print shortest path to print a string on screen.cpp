#include<bits/stdc++.h>
using namespace std;

void printPath(string S)
{
	int curX = 0,curY = 0;
	for(int i=0;i<S.length();i++)
	{
		int nextX = (S[i]-'A')/5;
		int nextY = (S[i]-'B'+1)%5;
		while(curX > nextX)
		{
			cout<<"Move Up"<<endl;
			curX--;
		}
		while(curY > nextY)
		{
			cout<<"Move Left"<<endl;
			curY--;
		}
		while(curX < nextX)
		{
			cout<<"Move  Down"<<endl;
			curX++;
		}
		while(curY < nextY)
		{
			cout<<"Move Right"<<endl;
			curY++;
		}
		cout<<"Press OK"<<endl;
	}
}

int main(void)
{
	string S = "COZY";
	printPath(S);
	return 0;
}