#include<bits/stdc++.h>
using namespace std;
#define COUNT 10
struct Tree
{
	int data;
	struct Tree *left,*right;
};

Tree *newNode(int data)
{
	Tree *node = (Tree *)malloc(sizeof(Tree));
	node->data = data;
	node->left = node->right = NULL;
	return node;
}

void print2D(Tree *root,int space)
{
	if(root)
	{
		space += COUNT;
		
		print2D(root->right,space);
		
		cout<<endl;
		for(int i=COUNT;i<space;i++)
			cout<<" ";
		cout<<root->data<<endl;
		
		print2D(root->left,space);
	}
}

int main(void)
{
	Tree *root = newNode(1);
	root->left   = newNode(2);
    root->right  = newNode(3);
 
    root->left->left  = newNode(4);
    root->left->right = newNode(5);
    root->right->left  = newNode(6);
    root->right->right = newNode(7);
 
    root->left->left->left  = newNode(8);
    root->left->left->right  = newNode(9);
    root->left->right->left  = newNode(10);
    root->left->right->right  = newNode(11);
    root->right->left->left  = newNode(12);
    root->right->left->right  = newNode(13);
    root->right->right->left  = newNode(14);
    root->right->right->right  = newNode(15); 
	
	print2D(root,0);
	return 0;
}