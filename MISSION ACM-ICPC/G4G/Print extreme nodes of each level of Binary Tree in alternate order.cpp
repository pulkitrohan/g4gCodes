#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right;
};

Tree *newNode(int data)
{
	Tree *node = (Tree *)malloc(sizeof(Tree));
	node->data = data;
	node->left = node->right = NULL;
	return node;
}

void printExtremeNodes(Tree *root)
{
	if(!root)
		return;
	queue<Tree *> Q;
	Q.push(root);
	bool flag = false;
	while(!Q.empty())
	{
		int N = Q.size();
		int count = N;
		while(1)
		{
			Tree *temp = Q.front();
			Q.pop();
			count--;
			if(temp->left)
				Q.push(temp->left);
			if(temp->right)
				Q.push(temp->right);
			if(flag && count == N-1)
				cout<<temp->data<<" ";
			if(!flag && count == 0)
				cout<<temp->data<<" ";
			if(count == 0)	
				break;
		}
		flag = !flag;
	}
}

int main(void)
{
	Tree *root = newNode(1);
	root->left = newNode(2);
    root->right = newNode(3);
 
    root->left->left  = newNode(4);
    root->left->right = newNode(5);
    root->right->right = newNode(7);
 
    root->left->left->left  = newNode(8);
    root->left->left->right  = newNode(9);
    root->left->right->left  = newNode(10);
    root->left->right->right  = newNode(11);
    root->right->right->left  = newNode(14);
    root->right->right->right  = newNode(15);
 
    root->left->left->left->left  = newNode(16);
    root->left->left->left->right  = newNode(17);
    root->right->right->right->right  = newNode(31);
	
	printExtremeNodes(root);
	return 0;
}