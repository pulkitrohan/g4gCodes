#include<bits/stdc++.h>
using namespace std;

bool isProduct(int *A,int x,int N)
{
	if(N < 2)
		return false;
	unordered_set<int> S;
	for(int i=0;i<N;i++)
	{
		if(A[i] == 0)
		{
			if(x == 0)
				return true;
			else	
				continue;
		}
		if(x%A[i] == 0)
		{
			if(S.find(x/A[i]) != S.end())
				return true;
			S.insert(A[i]);
		}
	}
	return false;
}

int main(void)
{
	int A[] = {10,20,9,40};
	int N = sizeof(A)/sizeof(A[0]);
	int x = 400;
	if(isProduct(A,x,N))
		cout<<"YES\n";
	else
		cout<<"NO\n";
	return 0;
}