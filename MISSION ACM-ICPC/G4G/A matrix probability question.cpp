#include<bits/stdc++.h>
using namespace std;

bool isSafe(int x, int y, int m, int n)
{
    return (x >= 0 && x < m && y >= 0 && y < n);
}

double Probability(int m,int n,int N,int i,int j)
{
	if(!isSafe(i,j,m,n))
		return 0.0;
	if(N == 0)
		return 1.0;
	double ans = 0.0;
	ans += Probability(m,n,N-1,i-1,j)*0.25;
	ans += Probability(m,n,N-1,i,j+1)*0.25;
	ans += Probability(m,n,N-1,i+1,j)*0.25;
	ans += Probability(m,n,N-1,i,j-1)*0.25;
	return ans;
}

int main(void)
{
	int m = 5,n = 5;
	int N = 2;
	int i = 1,j = 1;
	cout<<Probability(m,n,N,i,j);
	return 0;
}