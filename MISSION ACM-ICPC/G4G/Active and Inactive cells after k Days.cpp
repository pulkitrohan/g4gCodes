#include<bits/stdc++.h>
using namespace std;

void activeAndInactive(int *A,int N,int k)
{
	bool temp[N];
	for(int i=0;i<N;i++)
		temp[i] = A[i];
	while(k--)
	{
		temp[0] = 0^A[1];
		temp[N-1] = 0^A[N-2];
		for(int i=1;i<N-1;i++)
			temp[i] = A[i-1]^A[i+1];
		for(int i=0;i<N;i++)
			A[i] = temp[i];
	}
	int countActive = 0,countInactive = 0;
	for(int i=0;i<N;i++)
		if(A[i] == 1)
			countActive++;
		else
			countInactive++;
	cout<<countActive<<" "<<countInactive<<endl;
}

int main(void)
{
	int A[] = {0,1,0,1,0,1,0,1};
	int k = 3;
	int N = sizeof(A)/sizeof(A[0]);
	activeAndInactive(A,N,k);
	return 0;
}