#include<bits/stdc++.h>
using namespace std;

int countBits(int val){
	int count = 0;
	while(val)
	{
		if(val & 1)
			count++;
		val >>= 1;
	}
	return count;
}

void sortBySetBitCount(int *A,int N)
{
	vector<vector<int> > V(32);
	for(int i=0;i<N;i++)
	{
		int count = countBits(A[i]);
		V[count].push_back(A[i]);
	}
	int index = 0;
	for(int i=31;i>=0;i--)
	{
		vector<int> temp = V[i];
		for(int j=0;j<temp.size();j++)
			A[index++] = temp[j];
	}
}

int main(void){
	int A[] = {5,2,3,9,4,6,7,15,32};
	int N = sizeof(A)/sizeof(A[0]);
	sortBySetBitCount(A,N);
	for(int i=0;i<N;i++)
		cout<<A[i]<<" ";
	return 0;
}