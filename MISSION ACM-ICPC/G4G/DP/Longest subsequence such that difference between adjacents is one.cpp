#include<bits/stdc++.h>
using namespace std;

int longestSubseqWithDiffOne(int A[],int N){
	int DP[N+1];
	for(int i=0;i<N;i++)
		DP[i] = 1;
	for(int i=1;i<N;i++)
		for(int j=0;j<i;j++)
			if((A[i] == A[j] + 1) || (A[i] == A[j] - 1))
				DP[i] = max(DP[i],DP[j]+1);
	int result = 1;
	for(int i=0;i<N;i++)
		result = max(result,DP[i]);
	return result;
}

int main(void){
	int A[] = {1,2,3,4,5,3,2};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<longestSubseqWithDiffOne(A,N);
	return 0;
}
