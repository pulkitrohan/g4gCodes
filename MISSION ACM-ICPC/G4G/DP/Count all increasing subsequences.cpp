#include<bits/stdc++.h>
using namespace std;

int countSub(int A[], int N) {
	int count[10] = {0};
	for(int i=0;i<N;i++) {
		for(int j=A[i]-1;j>=0;j--)
			count[A[i]] += count[j];
		count[A[i]]++;
	}
	int ans = 0;
	for(int x : count)
		ans += x;
	return ans;
}

int main(void) {
	int A[] = {3, 2, 4, 5, 4};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<countSub(A, N)<<endl;
}