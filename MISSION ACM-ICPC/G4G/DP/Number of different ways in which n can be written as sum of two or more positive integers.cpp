#include<bits/stdc++.h>
using namespace std;

int countWays(int N){
	int dp[N+1];
	memset(dp,0,sizeof(dp));
	dp[0] = 1;
	for(int i=1;i<N;i++)
		for(int j=i;j<=N;j++)
			dp[j] += dp[j-i];
	return dp[N];		
}

int main(void){
	int N = 7;
	cout<<countWays(N);
	return 0;
}