#include<bits/stdc++.h>
using namespace std;

int maxSumSubarrayRemovingOneElement(int A[],int N){
	int fw[N],bw[N];
	int cur_max = A[0],max_so_far = A[0];
	fw[0] = cur_max;
	for(int i=1;i<N;i++){
		cur_max = max(A[i],cur_max+A[i]);
		max_so_far = max(cur_max,max_so_far);
		fw[i] = cur_max;
	}
	cur_max = max_so_far = A[N-1];
	bw[N-1] = A[N-1];
	for(int i=N-2;i>=0;i--){
		cur_max = max(A[i],cur_max+A[i]);
		max_so_far = max(max_so_far,cur_max);
		bw[i] = cur_max;
	}
	int ans = max_so_far;
	for(int i=1;i<N-1;i++)
		ans = max(ans,fw[i-1]+bw[i+1]);
	return ans;
}

int main(void){
	int A[] = {-2,-3,4,-1,-2,1,5,-3};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<maxSumSubarrayRemovingOneElement(A,N);
	return 0;
}