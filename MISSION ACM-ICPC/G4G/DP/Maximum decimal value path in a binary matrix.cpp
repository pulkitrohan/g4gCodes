#include<bits/stdc++.h>
using namespace std;

int MaximumDecimalValue(int A[][4],int N){
	int dp[N][N];
	memset(dp,0,sizeof(dp));
	if(A[0][0] == 1)
		dp[0][0] = 1;
	for(int i=1;i<N;i++){
		if(A[0][i] == 1)
			dp[0][i] = dp[0][i-1] + pow(2,i);
		else
			dp[0][i] = dp[0][i-1];
	}
	for(int i=1;i<N;i++){
		if(A[i][0] == 1)
			dp[i][0] = dp[i-1][0]  + pow(2,i);
		else
			dp[i][0] = dp[i-1][0];
	}
	for(int i=1;i<N;i++)
	{
		for(int j=1;j<N;j++){
			if(A[i][j] == 1)
				dp[i][j] = max(dp[i-1][j],dp[i][j-1]) + pow(2,i+j);
			else
				dp[i][j] = max(dp[i-1][j],dp[i][j-1]);
		}
	}
	return dp[N-1][N-1];
}

int main(void){
	int A[][4] = {
				   {1,1,0,1},
				   {0,1,1,0},
				   {1,0,0,1},
				   {1,0,1,1}
				 };
	cout<<MaximumDecimalValue(A,4)<<endl;
	return 0;
}