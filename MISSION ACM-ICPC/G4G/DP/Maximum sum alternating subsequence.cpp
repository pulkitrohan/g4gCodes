#include<bits/stdc++.h>
using namespace std;

int maxAlternateSum(int A[],int N){
	if(N == 1)
		return A[0];
	int inc[N+1],dec[N+1];
	memset(inc,0,sizeof(inc));
	memset(dec,0,sizeof(dec));
	inc[0] = dec[0] = A[0];
	bool flag = false;
	for(int i=1;i<N;i++)
	{
		for(int j=0;j<i;j++)
		{
			if(A[j] > A[i])
			{
				dec[i] = max(dec[i],A[i] + inc[j]);
				flag = true;
			}
			else if(A[i] > A[j] && flag)
				inc[i] = max(inc[i],A[i] + dec[j]);
		}
	}
	int ans = INT_MIN;
	for(int i=0;i<N;i++)
		ans = max(ans,max(inc[i],dec[i]));
	return ans;
}


int main(void){
	int A[] = {8,2,3,5,7,9,10};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<maxAlternateSum(A,N);

	return 0;
}