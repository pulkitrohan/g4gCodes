#include<bits/stdc++.h>
using namespace std;

void printDistSum(int *A,int N){
	int sum = 0;
	for(int i=0;i<N;i++)
		sum += A[i];
	bool dp[N+1][sum+1];
	memset(dp,false,sizeof(dp));
	for(int i=0;i<=N;i++)
		dp[i][0] = true;
	for(int i=1;i<=N;i++)
	{
		dp[i][A[i-1]] = true;
		for(int j=1;j<=sum;j++)
			if(dp[i-1][j])
			{
				dp[i][j] = true;
				dp[i][j+A[i-1]] = true;
			}	
	}
	for(int i=0;i<=sum;i++)
		if(dp[N][i])
			cout<<i<<" ";
}

int main(void){
	int A[] = {2,3,4,5,6};
	int N = sizeof(A)/sizeof(A[0]);
	printDistSum(A,N);
	return 0;
}