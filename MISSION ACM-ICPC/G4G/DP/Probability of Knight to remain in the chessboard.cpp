#include<bits/stdc++.h>
using namespace std;
#define N 8

int dx[] = { 1, 2, 2, 1, -1, -2, -2, -1 };
int dy[] = { 2, 1, -1, -2, -2, -1, 1, 2 };

bool inside(int x, int y)
{
    return (x >= 0 and x < N and y >= 0 and y < N);
}

double findProb(int start_x,int start_y,int steps){
	double DP[N][N][N];
	for(int i=0;i<N;i++)
		for(int j=0;j<N;j++)
			DP[i][j][0] = 1;
	for(int s=1;s<=steps;s++)
	{
		for(int x=0;x<N;x++)
			for(int y=0;y<N;y++)
			{
				double p = 0.0;
				for(int i=0;i<8;i++)
				{
					int nx = x + dx[i];
					int ny = y + dy[i];
					if(inside(nx,ny))
						p += DP[nx][ny][s-1]/8.0;

				}
				DP[x][y][s] = p;
			}
	}	
	return DP[start_x][start_y][steps];
}


int main(void){
	int K = 3;
	cout<<findProb(0,0,K);
	return 0;
}
