#include<bits/stdc++.h>
using namespace std;
 
int main(void)
{
	string S = "abaab";
	int N = S.length();
	int dp[N+1][N+1];
	memset(dp,0,sizeof(dp));
	int count = 0;
	for(int i=S.length()-1;i>=0;i--)
		for(int j=i;j<N;j++)
			if(((j-i) <= 2 || dp[i+1][j-1]) && S[i] == S[j]){
				dp[i][j] = 1;
				if(i != j)
					count++;
			}
				
	cout<<count<<endl;
	return 0;
}