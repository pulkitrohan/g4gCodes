#include<bits/stdc++.h>
using namespace std;

long long maxDotProduct(int A[],int B[],int M,int N){
	int dp[N+1][M+1];
	memset(dp,0,sizeof(dp));
	for(int i=1;i<=N;i++)
		for(int j=i;j<=M;j++)
			dp[i][j] = max(dp[i-1][j-1] + A[j-1]*B[i-1],dp[i][j-1]);
	return dp[N][M];		
}

int main(void){
	int A[] = {2,3,1,7,8};
	int B[] = {3,6,7};
	int M = sizeof(A)/sizeof(A[0]);
	int N = sizeof(B)/sizeof(B[0]);
	cout<<maxDotProduct(A,B,M,N);;
	return 0;
}
