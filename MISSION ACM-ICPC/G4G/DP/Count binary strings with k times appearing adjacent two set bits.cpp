#include<bits/stdc++.h>
using namespace std;

int countStrings(int N,int K)
{
	int DP[N+1][K+1][2];
	memset(DP,0,sizeof(DP));
	DP[1][0][0] = 1;
	DP[1][0][1] = 1;

	for(int i=2;i<=N;i++){
		for(int j=0;j<i;j++){
			DP[i][j][0] = DP[i-1][j][0] + DP[i-1][j][1];
			DP[i][j][1] = DP[i-1][j][0];
			if(j > 0)
				DP[i][j][1] += DP[i-1][j-1][1];
		}
	}
	return DP[N][K][0] + DP[N][K][1];
}


int main(void){
	int N = 5,K = 2;
	cout<<countStrings(N,K);
	return 0;
}