#include<bits/stdc++.h>
using namespace std;
const int M = 100;

int dp[M][M];

int numberOfPermWithKInversions(int N,int K)
{
	if(N == 0)
		return 0;
	if(K == 0)
		return 1;
	if(dp[N][K] != 0)
		return dp[N][K];
	int sum = 0;
	for(int i=0;i<=K;i++){
		if(i <= N-1)
			sum += numberOfPermWithKInversions(N-1,K-i);
	}
	dp[N][K] = sum;
	return sum;
}



int main(void){
	int N = 4,K = 2;
	cout<<numberOfPermWithKInversions(N,K);

	return 0;
}