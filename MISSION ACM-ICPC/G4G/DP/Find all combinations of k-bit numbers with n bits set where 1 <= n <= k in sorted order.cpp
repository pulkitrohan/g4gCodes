#include<bits/stdc++.h>
using namespace std;

void findBitCombinations(int k) {
	vector<string> dp[k+1][k+1];
	string S = "";
	for(int i=0;i<=k;i++) {
		dp[i][0].push_back(S);
		S += "0";
	}

	for(int i=1;i<=k;i++) {
		for(int setBits=1;setBits<=i;setBits++) {
			for(string str : dp[i-1][setBits]) 
				dp[i][setBits].push_back("0" + str);
			for(string str : dp[i-1][setBits-1])
				dp[i][setBits].push_back("1" + str);
		}
	}

	for(int i=1;i<=k;i++) {
		for(string str : dp[k][i])
			cout<<str<<" ";
		cout<<endl;
	}
}

int main(void) {
	int k = 5;
	findBitCombinations(k);
	return 0;
}