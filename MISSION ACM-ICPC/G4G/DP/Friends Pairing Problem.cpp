#include<bits/stdc++.h>
using namespace std;

int countFriendsPairing(int N)
{
	if(N == 0)
		return 0;
	if(N == 1)
		return 1;
	int prev_prev = 1;
	int prev = 2;
	for(int i=3;i<=N;i++)
	{
		int val = prev + prev_prev*(i-1);
		prev_prev = prev;
		prev = val;
	}
	return prev;
}

int main(void)
{
	int N = 4;
	cout<<countFriendsPairing(N);
	return 0;
}