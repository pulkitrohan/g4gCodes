#include<bits/stdc++.h>
using namespace std;

int minCost(string A[],int cost[],int N){
	int dp[N][2];
	dp[0][0] = 0;
	dp[0][1] = cost[0];

	string revStr[N];
	for(int i=0;i<N;i++)
	{
		revStr[i] = A[i];
		reverse(revStr[i].begin(),revStr[i].end());
	}
	string curStr;
	int curCost;

	for(int i=1;i<N;i++){
		for(int j=0;j<2;j++){
			dp[i][j] = INT_MAX;
			curStr = (j == 0) ? A[i] : revStr[i];
			curCost = (j == 0)? 0 : cost[i];
			if(curStr >= A[i-1])
				dp[i][j] = min(dp[i][j],dp[i-1][0] + curCost);
			if(curStr >= revStr[i-1])
				dp[i][j] = min(dp[i][j],dp[i-1][1] + curCost);
		}
	}
	for(int i=0;i<N;i++){
		for(int j=0;j<2;j++)
			cout<<dp[i][j]<<"\t";
		cout<<endl;
	}
	int ans = min(dp[N-1][0],dp[N-1][1]);
	return (ans == INT_MAX) ? -1 : ans;

}

int main(void){
	string A[] = {"aa","ba","ac"};
	int cost[] = {1,3,1};
	int N = sizeof(A)/sizeof(A[0]);
	int ans = minCost(A,cost,N);
	if(ans == -1)
		cout<<"Sorting not possible";
	else
		cout<<"Cost :" + ans<<endl;
	return 0;
}