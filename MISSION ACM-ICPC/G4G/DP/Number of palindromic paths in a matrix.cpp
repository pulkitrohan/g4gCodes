#include<bits/stdc++.h>
using namespace std;
#define R 3
#define C 4

struct cells{
	int rs,cs,re,ce;

	cells(int rs, int cs, int re, int ce) :
        rs(rs), cs(cs), re(re), ce(ce) { }
 
    bool operator <(const cells& other) const
    {
        return ((rs != other.rs) || (cs != other.cs) ||
               (re != other.re) || (ce != other.ce));
    }

};

int getPalindromePathsUtil(char A[R][C],int rs,int cs,int re,int ce,map<cells,int> &memo){
	if(rs < 0 || rs >= R || cs < 0 || cs >= C)
		return 0;
	if(re < 0 || rs > re || ce < 0 || cs > ce)
		return 0;
	if(A[rs][cs] != A[re][ce])
		return 0;
	if(abs((rs-re)+(cs-ce)) <= 1)
		return 1;
	if(memo.find(cells(rs,cs,re,ce)) != memo.end())
		return memo[cells(rs,cs,re,ce)];
	int ans = 0;
	ans += getPalindromePathsUtil(A,rs+1,cs,re-1,ce,memo);
	ans += getPalindromePathsUtil(A,rs+1,cs,re,ce-1,memo);
	ans += getPalindromePathsUtil(A,rs,cs+1,re-1,ce,memo);
	ans += getPalindromePathsUtil(A,rs,cs+1,re,ce-1,memo);
	memo[cells(rs,cs,re,ce)] = ans;
	return ans;
}


int getPalindromePaths(char A[R][C]){
	map<cells,int> dp;
	return getPalindromePathsUtil(A,0,0,R-1,C-1,dp);
}

int main(void){
	char A[R][C] = {'a','a','a','b',
					'b','a','a','a',
					'a','b','b','a'
					};
	cout<<getPalindromePaths(A);
	return 0;				
}