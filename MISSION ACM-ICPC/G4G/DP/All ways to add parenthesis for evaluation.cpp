#include<bits/stdc++.h>
using namespace std;

bool isOperator(char op)
{
    return (op == '+' || op == '-' || op == '*');
}

vector<int> possibleResultUtil(string input,map<string,vector<int> > memo){
	if(memo.find(input) != memo.end())
		return memo[input];
	vector<int> res;
	for(int i=0;i<input.size();i++)
	{
		if(isOperator(input[i]))
		{
			vector<int> pre = possibleResultUtil(input.substr(0,i),memo);
			vector<int> suf = possibleResultUtil(input.substr(i+1),memo);
			for(int j=0;j<pre.size();j++)
			{
				for(int k=0;k<suf.size();k++)
				{
					if(input[i] == '+')
						res.push_back(pre[j] + suf[k]);
					else if(input[i] == '-')
						res.push_back(pre[j] - suf[k]);
					else if(input[i] == '*')
						res.push_back(pre[j] * suf[k]);
				}
			}
		}
	}
	if(res.size() == 0)
		res.push_back(atoi(input.c_str()));
	memo[input] = res;
	return res;
}

vector<int> possibleResult(string input){
	map<string,vector<int> > memo;
	return possibleResultUtil(input,memo);
}


int main(void){
	string input = "5*4-3*2";
	vector<int> res = possibleResult(input);
	for(int i=0;i<res.size();i++)
		cout<<res[i]<<" ";
	return 0;
}