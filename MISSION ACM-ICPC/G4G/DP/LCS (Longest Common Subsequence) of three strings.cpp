#include<bits/stdc++.h>
using namespace std;

int main(void){
	string A = "AGGT12";
	string B = "12TXAYB";
	string C = "12XBA";

	int DP[A.length()+1][B.length()+1][C.length()+1];
	for(int i=0;i<=A.length();i++){
		for(int j=0;j<=B.length();j++){
			for(int k=0;k<=C.length();k++){
				if(i == 0 || j == 0 || k == 0)
					DP[i][j][k] = 0;
				else if(A[i-1] == B[j-1] && B[j-1] == C[k-1])
					DP[i][j][k] = DP[i-1][j-1][k-1] + 1;
				else
					DP[i][j][k] = max(max(DP[i-1][j][k],DP[i][j-1][k]),DP[i][j][k-1]);
			}
		}
	}
	cout<<DP[A.length()][B.length()][C.length()];
	return 0;
}