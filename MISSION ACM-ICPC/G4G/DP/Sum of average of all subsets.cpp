#include<bits/stdc++.h>
using namespace std;


int nCr(int N,int r){
	r = min(r,N-r);
	int ans = 1;
	for(int i=0;i<r;i++)
		ans *= (N-i)/(i+1);
	return ans;	
}

double resultOfAllSubsets(int A[],int N){
	int sum = 0;
	for(int i=0;i<N;i++)
		sum += A[i];
	double ans = 0.0;
	for(int n=1;n<=N;n++)
		ans += (double)(sum*(nCr(N-1,n-1)))/n;
	return ans;	
}

int main(void){
	int A[] = {2,3,5,7};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<resultOfAllSubsets(A,N)<<endl;
	return 0;
}