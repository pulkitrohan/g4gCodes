#include<bits/stdc++.h>
using namespace std;

int maxSumPairWitthDifferenceLessThanK(int A[],int N,int K){
	sort(A,A+N);
	int dp[N+1];
	dp[0] = 0;
	for(int i=1;i<N;i++)
	{
		if(A[i]-A[i-1] < K)
		{
			if(i >= 2)
				dp[i] = max(dp[i-1],dp[i-2] + A[i] + A[i-1]);
			else
				dp[i] = max(dp[i-1],A[i] + A[i-1]);
		}
		else
			dp[i] = dp[i-1];

	}
}

int maxSumPairWitthDifferenceLessThanKOptimised(int A[],int N,int K) {
	sort(A,A+N);
	int maxSum = 0;
	for(int i=N-1;i>0;i--) {
		if(A[i] - A[i-1] < K) {
			maxSum += A[i] + A[i-1];
			i--;
		}
	}
	return maxSum;
}


int main(void){
	int A[] = {3,5,10,15,17,12,9};
	int N = sizeof(A)/sizeof(A[0]);
	int K = 4;
	cout<<maxSumPairWitthDifferenceLessThanKOptimised(A,N,K);

	return 0;
}