#include<bits/stdc++.h>
using namespace std;
#define N 3

int maxPathSum(int A[][N],int n){
	for(int i=n-1;i>=0;i--)
		for(int j=0;j<=i;j++)
			A[i][j] += max(A[i+1][j],A[i+1][j+1]);
	return A[0][0];
}


int main(void){
	int A[N][N] = {	{1,0,0},
					{4,8,0},
					{1,5,3}};
	cout<<maxPathSum(A,2);
	return 0;
}