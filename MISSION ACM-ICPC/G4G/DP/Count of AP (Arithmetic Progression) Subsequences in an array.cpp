#include<bits/stdc++.h>
using namespace std;
#define MAX 1000000

int numOfAP(int A[],int N){
	int minNum = INT_MAX,maxNum = INT_MIN;
	for(int i=0;i<N;i++)
	{
		minNum = min(A[i],minNum);
		maxNum = max(A[i],maxNum);
	}
	int dp[N],sum[MAX+1];
	int ans = N+1;
	for(int d=(minNum-maxNum);d<=(maxNum-minNum);d++){
		memset(sum,0,sizeof(sum));
		for(int i=0;i<N;i++){
			dp[i] = 1;
			if(A[i] - d >= 1 && A[i] - d <= MAX)
				dp[i] += sum[A[i]-d];
			ans += dp[i]-1;
			sum[A[i]] += dp[i];
		}
	}

	return ans;
}

int main(void){
	int A[] = {1,2,3};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<numOfAP(A,N)<<endl;
	return 0;
}