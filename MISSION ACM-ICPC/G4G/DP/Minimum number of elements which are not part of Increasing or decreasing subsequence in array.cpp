#include<bits/stdc++.h>
using namespace std;
#define MAX 101

int countMin(int A[],int dp[MAX][MAX][MAX],int N,int dec,int inc,int i){
	if(dp[dec][inc][i] != -1)
		return dp[dec][inc][i];
	if(i == N)
		return 0;
	if(A[i] < A[dec])
		dp[dec][inc][i] = countMin(A,dp,N,i,inc,i+1);
	if(A[i] > A[inc])
	{
		if(dp[dec][inc][i] == -1)
			dp[dec][inc][i] = countMin(A,dp,N,dec,i,i+1);
		else
			dp[dec][inc][i] = min(countMin(A,dp,N,dec,i,i+1),dp[dec][inc][i]);
	}
	if(dp[dec][inc][i] == -1)
		dp[dec][inc][i]  = 1 + countMin(A,dp,N,dec,inc,i+1);
	else
		dp[dec][inc][i] = min(1 + countMin(A,dp,N,dec,inc,i+1),dp[dec][inc][i]);
	return dp[dec][inc][i];
}

int wrapper(int A[],int N){
	A[MAX-2] = INT_MAX;
	A[MAX-1] = INT_MIN;

	int dp[MAX][MAX][MAX];
	memset(dp,-1,sizeof(dp));
	return countMin(A,dp,N,MAX-2,MAX-1,0);
}

int main(void){
	int N = 12;
	int A[MAX] = {7,8,1,2,4,6,3,5,2,1,8,7};
	cout<<wrapper(A,N)<<endl;
	return 0;

}