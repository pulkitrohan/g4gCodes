#include<bits/stdc++.h>
using namespace std;
#define M 3
#define N 3

int findMaxPoints(int A[][N])
{
	int FirstStart[M+1][N+1],FirstEnd[M+1][N+1];
	memset(FirstStart,0,sizeof(FirstStart));
	memset(FirstEnd,0,sizeof(FirstEnd));

	int SecondStart[M+1][N+1],SecondEnd[M+1][N+1];
	memset(SecondStart,0,sizeof(SecondStart));
	memset(SecondEnd,0,sizeof(SecondEnd));

	for(int i=1;i<=N;i++)
		for(int j=1;j<=M;j++)
			FirstStart[i][j] = max(FirstStart[i-1][j],FirstStart[i][j-1]) + A[i-1][j-1];
	for(int i=N;i>=1;i--)
		for(int j=M;j>=1;j--)
			FirstEnd[i][j] = max(FirstEnd[i+1][j],FirstEnd[i][j+1]) + A[i-1][j-1];

	for(int i=N;i>=1;i--)
		for(int j=1;j<=M;j++)
			SecondStart[i][j] = max(SecondStart[i+1][j],SecondStart[i][j-1]) + A[i-1][j-1];
	for(int i=1;i<=N;i++)
		for(int j=M;j>=1;j--)
			SecondEnd[i][j] = max(SecondEnd[i-1][j],SecondEnd[i][j+1]) + A[i-1][j-1];

	int ans = 0;
	// i,j is meeting point
	for(int i=2;i<N;i++){
		for(int j=2;j<M;j++){
			int optionA = FirstStart[i][j-1] + FirstEnd[i][j+1] + 
						  SecondStart[i+1][j] + SecondEnd[i-1][j];
			int optionB = FirstStart[i-1][j] + FirstEnd[i+1][j] + 
						  SecondStart[i][j-1] + SecondEnd[i][j+1]; 	
			ans = max(ans,max(optionA,optionB));
		}
	}
	return ans;							
}

int main(void){
	int A[][M] = {{100,100,100},
				  {100,1,100},
				  {100,100,100}};
	cout<<"Max Points : " << findMaxPoints(A);
				  
}