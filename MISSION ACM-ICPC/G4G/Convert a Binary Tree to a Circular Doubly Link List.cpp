#include<bits/stdc++.h>
using namespace std;

struct treeNode{
	int data;
	struct treeNode *left,*right;
};


struct treeNode *newNode(int val){
	 treeNode *node = (treeNode *)malloc(sizeof(treeNode));
	 node->data = val;
	 node->left = node->right = NULL;
	 return node;
}

void convert(treeNode *root,treeNode *&head,treeNode *&prev){
	if(root){

		convert(root->left,head,prev);
		if(!prev)
			head = root;
		else{

			root->left = prev;
			prev->right = root;
		}
		prev = root;
		convert(root->right,head,prev);
	}
}

void print(treeNode *head){
	
	treeNode *temp;
	for(temp = head;temp->right != head;temp = temp->right)
		cout<<temp->data<<" ";
	cout<<temp->data<<endl;
}

int main(void){

	treeNode *root = newNode(10);
	root->left = newNode(12);
    root->right = newNode(15);
    root->left->left = newNode(25);
    root->left->right = newNode(30);
    root->right->left = newNode(36);
    treeNode *head = NULL,*last = NULL,*prev = NULL;
    convert(root,head,prev);
    head->left = prev;
    prev->right = head;
    print(head);
    return 0;
}