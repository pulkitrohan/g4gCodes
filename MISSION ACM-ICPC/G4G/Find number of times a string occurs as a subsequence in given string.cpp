#include<bits/stdc++.h>
using namespace std;

int countTimes(string A,string B)
{
	int N = A.length();
	int M = B.length();
	int dp[N+1][N+1] = {{0}};
	for(int i=0;i<=M;i++)
		dp[0][i] = 0;
	for(int i=0;i<=N;i++)
		dp[i][0] = 1;
	for(int i=1;i<=N;i++)
	{
		for(int j=1;j<=M;j++)
		{
			dp[i][j] = dp[i-1][j];
			if(A[i-1] == B[j-1])
				dp[i][j] += dp[i-1][j-1];
		}
	}
	return dp[N][M];
}

int main(void)
{
	string A = "GeeksforGeeks";
	string B = "Gks";
	cout<<countTimes(A,B);
	return 0;
}