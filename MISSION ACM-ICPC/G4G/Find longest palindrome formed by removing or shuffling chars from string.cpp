#include<bits/stdc++.h>
using namespace std;

string LP(string S)
{
	int count[256] = {0};
	for(int i=0;i<S.length();i++)
		count[S[i]]++;
	string A = "",B = "",C = "";
	for(char ch = 'a';ch <= 'z';ch++)
	{
		if(count[ch] & 1)
		{
			B = ch;
			count[ch--]--;
		}
		else
		{
			for(int i=0;i<count[ch]/2;i++)
				A += ch;
		}
	}
	C = A;
	reverse(C.begin(),C.end());
	return A + B + C;
}

int main(void)
{
	string S = "abbaccd";
	cout<<LP(S);
	return 0;
}