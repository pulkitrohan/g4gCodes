#include<bits/stdc++.h>
using namespace std;
#define MAX 40
int dp[MAX][9*MAX+1];

int countGroups(int index,int prev_sum,string S){
	
	if(index == S.length())
		return 1;
	if(dp[index][prev_sum] != -1)
		return dp[index][prev_sum];
	dp[index][prev_sum] = 0;
	int res = 0,sum = 0;
	for(int i=index;i<S.length();i++)
	{
		sum += (S[i]-'0');
		if(sum >= prev_sum)
			res += countGroups(i+1,sum,S);
	}
	dp[index][prev_sum] = res;
	return dp[index][prev_sum];
}

int main(void){
	
	string S = "1119";
	memset(dp,-1,sizeof(dp));
	cout<<countGroups(0,0,S);
	return 0;
}