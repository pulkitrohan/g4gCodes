#include<bits/stdc++.h>
using namespace std;

int spellCount(string S){
	int ans = 1;
	for(int i=0;i<S.length();i++)
	{
		int count = 1;
		while(i < S.length()-1 && S[i+1] == S[i]){
			count++;
			i++;
		}
		ans *= pow(2,count-1);
	}
	return ans;
}


int main(void){
	string S = "11112";
	cout<<spellCount(S);
	return 0;
}