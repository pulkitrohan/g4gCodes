#include<bits/stdc++.h>
using namespace std;

#define N 5
#define M 5

struct node
{
	int x,y,dist;
};

int R[] = {-1,0,1,0};
int C[] = {0,1,0,-1};

bool isValid(int x,int y){
	if((i < 0 || i > M-1) || (j < 0 || j > N-1))
		return false;
	return true;
}

bool isSafe(int x,int y,char A[][N],int V[][N]){
	if(A[x][y] != 'O' || V[x][y] != -1)
		return false;
	return true;
}

void findDistance(char M[][N])
{
	int output[M][N];
	queue<node> Q;
	for(int i=0;i<M;i++)
	{
		for(int j=0;j<N;j++)
		{
			output[i][j] = -1;
			if(M[i][j] == 'G')
			{
				node temp = {i,j,0};
				Q.push(temp);
				output[i][j] = 0;
			}
		}
	}
	while(!Q.empty())
	{
		node curr = Q.front();
		Q.pop();
		for(int i=0;i<4;i++)
		{
			if(isSafe(curr.x+row[i],curr.y+col[i],M,output) && isValid(curr.x+row[i],curr.y+col[i]))
			{
				output[curr.x+row[i]][curr.y+col[i]] = curr.dist + 1;
				node pos = {curr.x + row[i],curr.y+col[i],curr.dist+1};
				Q.push(pos);
			}
		}
	}
	for(int i=0;i<M;i++)
	{
		for(int j=0;j<N;j++)
			cout<<output[i][j]<<" ";
		cout<<endl;
	}
}

int main(void)
{
	 char matrix[][N] =
    {
        {'O', 'O', 'O', 'O', 'G'},
        {'O', 'W', 'W', 'O', 'O'},
        {'O', 'O', 'O', 'W', 'O'},
        {'G', 'W', 'W', 'W', 'O'},
        {'O', 'O', 'O', 'O', 'G'}
    };
    findDistance(matrix);
}