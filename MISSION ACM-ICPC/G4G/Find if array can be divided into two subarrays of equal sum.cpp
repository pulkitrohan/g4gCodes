#include<bits/stdc++.h>
using namespace std;

void printsubarray(int arr[], int start, int end)
{
    cout << "[ ";
    for (int i = start; i <= end; i++)
        cout << arr[i] << " ";
    cout << "] ";
}

bool divideArray(int *A,int N)
{
	int sum = 0;
	for(int i=0;i<N;i++)
		sum += A[i];
	int sum_so_far = 0;
	for(int i=0;i<N;i++)
	{
		if(2*sum_so_far + A[i] == sum)
		{
			printsubarray(A,0,i-1);
			printsubarray(A,i+1,N-1);
			return true;
		}
		sum_so_far += A[i];
	}
	cout<<"NOT FOUND";
	return false;
}

int main(void)
{
	int A[] = {6,2,3,2,1};
	int N = sizeof(A)/sizeof(A[0]);
	divideArray(A,N);
	return 0;
}