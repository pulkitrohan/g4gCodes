#include<bits/stdc++.h>
using namespace std;
#define R 5
#define C 3

void precompute(int A[R][C],int ryt[R+2][C+2],int dwn[R+2][C+2])
{
	for(int j=C-1;j>=0;j--)
	{
		for(int i=0;i<R;i++)
		{
			if(A[i][j] == 0)
				ryt[i][j] = 0;
			else
				ryt[i][j] = 1 + ryt[i][j+1];
		}
	}
	for(int i=R-1;i>=0;i--)
	{
		for(int j=0;j<C;j++)
		{
			if(A[i][j] == 0)
				dwn[i][j] = 0;
			else
				dwn[i][j] = 1 + dwn[i+1][j];
		}	
	}

}

int solveRowSwap(int ryt[R+2][C+2])
{
	int b[R] = {0},ans = 0;
	for(int j=0;j<C;j++)
	{
		for(int i=0;i<R;i++)
			b[i] = ryt[i][j];
		sort(b,b+R);
		for(int i=0;i<R;i++)
			ans = max(ans,b[i]*(R-i));
	}
	return ans;
}

int solveColSwap(int dwn[R+2][C+2])
{
	int b[C] = {0},ans = 0;
	for(int i=0;i<R;i++)
	{
		for(int j=0;j<C;j++)
			b[j] = dwn[i][j];
		sort(b,b+R);
		for(int j=0;j<C;j++)
			ans = max(ans,b[j]*(C-j));
	}
	return ans;
}


void findMax1s(int A[R][C]){
	int ryt[R+2][C+2],dwn[R+2][C+2];
	memset(ryt,0,sizeof(ryt));
	memset(dwn,0,sizeof(dwn));;
	precompute(A,ryt,dwn);
	int rowSwap = solveRowSwap(ryt);
	int colSwap = solveColSwap(dwn);

	(rowSwap > colSwap) ? (cout << "Row Swap\n" << rowSwap<<endl):
						  (cout << "Column Swap\n"<<colSwap<<endl);

}


int main(void)
{
	int A[R][C] = {{0,0,0},{1,1,0},{1,1,0},{0,0,0},{1,1,0}};
	findMax1s(A);

}