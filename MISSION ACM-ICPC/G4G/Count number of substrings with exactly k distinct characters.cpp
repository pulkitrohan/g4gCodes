#include<bits/stdc++.h>
using namespace std;

int countkDist(string S,int k)
{
	int count[26] = {0},ans = 0;
	for(int i=0;i<S.length();i++)
	{
		int dist_count = 0;
		memset(count,0,sizeof(count));
		for(int j=i;j<S.length();j++)
		{
			if(count[S[j]-'a'] == 0)
				dist_count++;
			count[S[j]-'a']++;
			if(dist_count == k)
				ans++;
		}
	}
	return ans;
}

int main(void)
{
	string S = "abcbaa";
	int k = 3;
	cout<<countkDist(S,k)<<endl;
	return 0;
}