#include<bits/stdc++.h>
using namespace std;

struct node{
	int data;
	struct node *left,*right;
};

node *newNode(int data)
{
	node *temp = (node *)malloc(sizeof(node));
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

bool isLeaf(node *root)
{
	return (root->left && root->left->right == root &&
			root->right && root->right->left == root);
}

int maxDepth(node *root)
{
	if(!root)
		return 0;
	if(isLeaf(root))
		return 1;
	return 1 + max(maxDepth(root->left),maxDepth(root->right));
}

int main(void)
{
	node *root = newNode(1);
	root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);
    root->left->left->left = newNode(6);
    node *L1 = root->left->left->left;
    node *L2 = root->left->right;
    node *L3 = root->right;
    L1->right = L2, L2->right = L3, L3->right = L1;
    L3->left = L2, L2->left = L1, L1->left = L3;
    cout<<maxDepth(root);
    return 0;
}