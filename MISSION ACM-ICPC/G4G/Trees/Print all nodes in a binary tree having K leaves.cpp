#include<bits/stdc++.h>
using namespace std;

struct Node{
  int data;
  struct Node *left,*right;
};

Node *newNode(int key){
  Node *temp = new Node;
  temp->data = key;
  temp->left = temp->right = NULL;
  return temp;
}

int kLeaves(Node *root,int k){
  if(!root)
    return 0;
  if(!root->left && !root->right)
    return 1;
  int total = kLeaves(root->left,k) + kLeaves(root->right,k);
  if(total == k)
    cout<<root->data<<" ";
  return total;
}

int main()
{
   struct Node *root = newNode(1);
    root->left        = newNode(2);
    root->right       = newNode(4);
    root->left->left  = newNode(5);
    root->left->right = newNode(6);
    root->left->left->left  = newNode(9);
    root->left->left->right  = newNode(10);
    root->right->right = newNode(8);
    root->right->left  = newNode(7);
    root->right->left->left  = newNode(11);
    root->right->left->right  = newNode(12);
    kLeaves(root,2);
   return 0;
}