#include<bits/stdc++.h>
using namespace std;

struct Node{
  int data;
  struct Node *left,*right;
};

Node *newnode(int key){
  Node *temp = new Node;
  temp->data = key;
  temp->left = temp->right = NULL;
  return temp;
}

pair<int,int> maxSumUtil(Node *root)
{
  if(!root)
  {
    pair<int,int> sum(0,0);
    return sum;
  }
  pair<int,int> A = maxSumUtil(root->left);
  pair<int,int> B = maxSumUtil(root->right);
  pair<int,int> sum;

  sum.first = A.second + B.second + root->data;
  sum.second = max(A.first,A.second) + max(B.first,B.second);
  return sum;

}

int maxSum(Node *root){
  pair<int,int> ans = maxSumUtil(root);
  return max(ans.first,ans.second);
}

int main()
{
    Node *root= newnode(10);
    root->left= newnode(1);
    root->left->left= newnode(2);
    root->left->left->left= newnode(1);
    root->left->right= newnode(3);
    root->left->right->left= newnode(4);
    root->left->right->right= newnode(5);
    cout<<maxSum(root)<<endl;
   return 0;
}