#include<bits/stdc++.h>
using namespace std;

struct Node{
  int data;
  struct Node *left,*right;
};

Node *newnode(int key){
  Node *temp = new Node;
  temp->data = key;
  temp->left = temp->right = NULL;
  return temp;
}

bool sumSubtreeUtil(Node *root,int &curSum,int sum){

  if(!root){
    curSum = 0;
    return false;
  }

  int leftSum = 0,rightSum = 0;

  return sumSubtreeUtil(root->left,leftSum,sum) || 
         sumSubtreeUtil(root->right,rightSum,sum) || 
         ((curSum = leftSum + rightSum + root->data) == sum);
}



bool sumSubtree(Node *root,int sum){
    int curSum = 0;
    return sumSubtreeUtil(root,curSum,sum);
}

int main()
{
    struct Node *root = newnode(8);
    root->left    = newnode(5);
    root->right   = newnode(4);
    root->left->left = newnode(9);
    root->left->right = newnode(7);
    root->left->right->left = newnode(1);
    root->left->right->right = newnode(12);
    root->left->right->right->right = newnode(2);
    root->right->right = newnode(11);
    root->right->right->left = newnode(3);
    int sum = 22;
    if(sumSubtree(root,sum))
      cout<<"YES";
    else
      cout<<"NO";
   return 0;
}