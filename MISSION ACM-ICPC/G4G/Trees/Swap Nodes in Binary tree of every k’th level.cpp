#include<bits/stdc++.h>
using namespace std;

struct Node{
  int data;
  struct Node *left,*right;
};

Node *newnode(int key){
  Node *temp = new Node;
  temp->data = key;
  temp->left = temp->right = NULL;
  return temp;
}

bool existPath(Node *root,int A[],int N,int index){
  if(!root)
    return (N == 0);
  if(!root->left && !root->right && root->data == A[index] && index == N-1)
    return true;
  return (index < N && root->data == A[index]) && (existPath(root->left,A,N,index+1) || existPath(root->right,A,N,index+1));
}

int main()
{
   int A[] = {5,8,6,7};
   int N = sizeof(A)/sizeof(A[0]);
   struct Node *root = newnode(5);
    root->left    = newnode(3);
    root->right   = newnode(8);
    root->left->left = newnode(2);
    root->left->right = newnode(4);
    root->left->left->left = newnode(1);
    root->right->left = newnode(6);
    root->right->left->right = newnode(7);
    if(existPath(root,A,N,0))
      cout<<"YES";
    else
      cout<<"NO";

 
   return 0;
}