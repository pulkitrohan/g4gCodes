#include<bits/stdc++.h>
using namespace std;

struct Node{
  int data;
  struct Node *left,*right;
};

Node *newnode(int key){
  Node *temp = new Node;
  temp->data = key;
  temp->left = temp->right = NULL;
  return temp;
}

void pathCountUtils(Node *root,unordered_map<int,int> &M,int level){
  if(!root)
    return;
  if(!root->left && !root->right){
    M[level]++;
    return;
  }
   pathCountUtils(root->left,M,level+1);
   pathCountUtils(root->right,M,level+1);
   
}

void pathCounts(Node *root){
  unordered_map<int,int> M;
  pathCountUtils(root,M,1);
  for(auto it = M.begin();it != M.end();it++){
    cout<<it->second<<" Paths have length: "<<it->first<<endl;
  }

}

int main()
{
    struct Node *root = newnode(10);
    root->left    = newnode(8);
    root->right   = newnode(2);
    root->left->left = newnode(3);
    root->left->right = newnode(5);
    root->right->right = newnode(4);
    root->right->left = newnode(2);
    pathCounts(root);
   return 0;
}