#include<bits/stdc++.h>
using namespace std;

int findDepth(string A,int &index){
	if(index >= A.length() || A[index] == 'l')
		return 0;
	index++;
	int left = findDepth(A,index);
	index++;;
	int right = findDepth(A,index);
	return max(left,right) + 1;	
}

int main(void){
	string A = "nlnnlll";
	int index = 0;
	cout<<findDepth(A,index);
}