#include<bits/stdc++.h>
using namespace std;

struct Node{
  char key;
  vector<Node *> child;
};

Node *newNode(int key){
  Node *temp = new Node;
  temp->key = key;
  return temp;
}

int depthOfTree(struct Node *ptr)
{
    if (!ptr)
        return 0;
 
    int maxdepth = 0;
    for (vector<Node*>::iterator it = ptr->child.begin();it != ptr->child.end(); it++)
        maxdepth = max(maxdepth, depthOfTree(*it)); 
    return maxdepth + 1 ;
}

int diameter(struct Node *root){
  if(!root)
    return 0;
  int max1 = 0 ,max2 = 0;
  for(auto it = root->child.begin();it != root->child.end();it++){
    int h = depthOfTree(*it);
    if(h > max1)
      max2 = max1,max1 = h;
    else if(h > max2)
      max2 = h;
  }
  int maxChildDiameter = 0;
  for(auto it = root->child.begin();it != root->child.end();it++)
    maxChildDiameter = max(maxChildDiameter,diameter(*it));
  return max(maxChildDiameter,max1+max2+1);
}
 
int main()
{
   Node *root = newNode('A');
    (root->child).push_back(newNode('B'));
    (root->child).push_back(newNode('F'));
    (root->child).push_back(newNode('D'));
    (root->child).push_back(newNode('E'));
    (root->child[0]->child).push_back(newNode('K'));
    (root->child[0]->child).push_back(newNode('J'));
    (root->child[2]->child).push_back(newNode('G'));
    (root->child[3]->child).push_back(newNode('C'));
    (root->child[3]->child).push_back(newNode('H'));
    (root->child[3]->child).push_back(newNode('I'));
    (root->child[0]->child[0]->child).push_back(newNode('N'));
    (root->child[0]->child[0]->child).push_back(newNode('M'));
    (root->child[3]->child[2]->child).push_back(newNode('L'));
 
   cout << diameter(root) << endl;
 
   return 0;
}