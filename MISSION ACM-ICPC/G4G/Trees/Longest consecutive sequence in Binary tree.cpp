#include<bits/stdc++.h>
using namespace std;

struct Node{
  int data;
  struct Node *left,*right;
};

Node *newNode(int key){
  Node *temp = new Node;
  temp->data = key;
  temp->left = temp->right = NULL;
  return temp;
}

void longestConsecutiveUtil(Node *root,int curLength,int expected,int &ans){
  if(root)
  {
    if(root->data == expected)
      curLength++;
    else
      curLength = 1;
    ans = max(ans,curLength);
    longestConsecutiveUtil(root->left,curLength,root->data+1,ans);
    longestConsecutiveUtil(root->right,curLength,root->data+1,ans);
  }
}

int longestConsecutive(Node *root){
  int ans = 0;
  longestConsecutiveUtil(root,0,root->data+1,ans);
  return ans;  
}

int main()
{
     Node *root = newNode(10);
    root->left = newNode(11);
    root->right = newNode(9);
    root->left->left = newNode(13);
    root->left->right = newNode(12);
    root->right->left = newNode(13);
    root->right->right = newNode(8);
    cout<<longestConsecutive(root)<<endl;
    return 0;
}