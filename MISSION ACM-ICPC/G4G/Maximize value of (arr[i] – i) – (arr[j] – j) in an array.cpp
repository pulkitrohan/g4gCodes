#include<bits/stdc++.h>
using namespace std;

int FindMaxDiff(int *A,int N)
{
	if(N < 2)
	{
		cout<<"Invalid";
		return 0;
	}
	int min_val = INT_MAX,max_val = INT_MIN;
	for(int i=0;i<N;i++)
	{
		max_val = max(max_val,A[i]-i);
		min_val = min(min_val,A[i]-i);
	}
	return max_val-min_val;
}

int main(void)
{
	int A[] = {9,15,4,12,13};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<FindMaxDiff(A,N);
	return 0;
}