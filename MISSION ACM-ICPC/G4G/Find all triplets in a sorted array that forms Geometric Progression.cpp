#include<bits/stdc++.h>
using namespace std;

int PGP(int *A,int N){
	for(int i=1;i<N-1;i++)
	{
		int j = i-1,k=i+1;
		while(j >= 0 && k <= N-1)
		{
			while(A[i]*A[i] == A[j]*A[k] && A[i]%A[j] == 0 && A[k]%A[i] == 0)
			{
				cout<<A[j]<<" "<<A[i]<<" " <<A[k]<<endl;
				k++,j--;
			}
			if(A[i]%A[j] == 0 && A[k]%A[i] == 0)
			{
				if(A[i]*A[i] > A[j]*A[k])
					k++;
				else
					j--;
			}
			else if(A[i]%A[j] == 0)
				k++;
			else
				j--;
		}
	}
}

int main(void){
	int A[] = {1, 2, 4, 16};
	int N = sizeof(A)/sizeof(A[0]);
	PGP(A,N);
	return 0;
}