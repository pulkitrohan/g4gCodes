#include<bits/stdc++.h>
using namespace std;

bool hasDuplicateParenthesis(string S)
{
	stack<char> Stack;
	for(int i=0;i<S.length();i++)
	{
		if(S[i] == ')')
		{
			 char temp = Stack.top();
			 Stack.pop();
			 if(temp == '(')
			 	return true;
			 else
			 {
			 	while(temp != '(')
			 	{
			 		temp = Stack.top();
			 		Stack.pop();
			 	}
			 }
		}
		else
			Stack.push(S[i]);
	}
	return false;
}

int main(void)
{
	string S = "(((a+(b))+(c+d)))";
	if(hasDuplicateParenthesis(S))
		cout<<"YES";
	else
		cout<<"NO";
	return 0;
}