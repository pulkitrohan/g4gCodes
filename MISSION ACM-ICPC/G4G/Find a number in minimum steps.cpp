#include<bits/stdc++.h>
using namespace std;
#define INF 99999

struct number{
	int no,level;
	public:
    number() {}
    number(int n, int l):no(n),level(l) {}
};

void findnthnumber(int n)
{
	 queue<number> q;
    struct number r(0, 1);
    q.push(r);
 
    // Do level order traversal
    while (!q.empty())
    {
        // Remove a node from queue
        struct number temp = q.front();
        q.pop();
 
        // To avoid infinite loop
        if (temp.no >= INF || temp.no <= -INF)
            break;
 
        // Check if dequeued number is same as n
        if (temp.no == n)
        {
            cout << "Found number n at level "
                 << temp.level-1;
            break;
        }
 
        // Insert children of dequeued node to queue
        q.push(number(temp.no+temp.level, temp.level+1));
        q.push(number(temp.no-temp.level, temp.level+1));
    }
}

int main(void){
	findnthnumber(13);
	return 0;
}