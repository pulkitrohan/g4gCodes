#include<bits/stdc++.h>
using namespace std;
 
struct Tree
{
	int data;
	struct Tree *left,*right;
};
 
Tree *newnode(int data)
{
	Tree *node = (Tree *)malloc(sizeof(Tree));
	node->data = data;
	node->left = node->right = NULL;
	return node;
}
 
bool isLeaf(Tree *root)
{
	return (!root->left && !root->right);
}
 
void sink(Tree *&root)
{
	if(!root || isLeaf(root))
		return;
 
	if(root->left && !(root->left->data & 1))
	{
		swap(root->data,root->left->data);
		sink(root->left);
	}
	else if(root->right && !(root->right->data & 1))
	{
		swap(root->data,root->right->data);
		sink(root->right);
	}
}
 
void sinkOddNodes(Tree *&root)
{
	if(!root || isLeaf(root))
		return;
	sinkOddNodes(root->left);
	sinkOddNodes(root->right);
	if(root->data & 1)
		sink(root);
}
 
void printLevelOrder(Tree* root)
{
    queue<Tree*> q;
    q.push(root);
 
    // Do Level order traversal
    while (!q.empty())
    {
        int nodeCount = q.size();
 
        // Print one level at a time
        while (nodeCount)
        {
            Tree *node = q.front();
            printf("%d ", node->data);
            q.pop();
            if (node->left != NULL)
                q.push(node->left);
            if (node->right != NULL)
                q.push(node->right);
            nodeCount--;
        }
 
        // Line separator for levels
        printf("\n");
    }
}
 
 
int main(void)
{
	Tree *root = newnode(1);
	root->left = newnode(5);
    root->right    = newnode(8);
    root->left->left = newnode(2);
    root->left->right = newnode(4);
    root->right->left = newnode(9);
    root->right->right = newnode(10);
 
	sinkOddNodes(root);
	printLevelOrder(root);
	return 0;
}