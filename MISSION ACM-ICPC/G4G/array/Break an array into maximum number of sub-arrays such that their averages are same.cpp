#include<bits/stdc++.h>
using namespace std;

void findSubarray(int A[],int N){
	vector<int> ans;
	int sum = 0;
	for(int i=0;i<N;i++)
		sum += A[i];
	int curr_sum = 0,prev_index = -1;
	for(int i=0;i<N;i++)
	{
		curr_sum += A[i];
		if(sum*(i-prev_index) == N*curr_sum)
		{
			curr_sum = 0;
			prev_index = i;
			ans.push_back(i);
		}	
	}
	if(prev_index != N-1)
		cout<<"Not Possible";
	cout<<"(0,"<<ans[0]<<")\n";
	for(int i=1;i<ans.size();i++)
		cout<<"("<<ans[i-1]+1<<","<<ans[i]<<")\n";
}

int main(void){
	int A[] = {1, 5, 7, 2, 0};
	int N = sizeof(A)/sizeof(A[0]);
	findSubarray(A,N);
	return 0;
}