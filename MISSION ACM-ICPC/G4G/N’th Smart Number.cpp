#include<bits/stdc++.h>
using namespace std;
#define MAX 3000

int smartNumber(int N){
	int primes[MAX] = {0};
	vector<int> ans;
	for(int i=2;i<MAX;i++)
	{
		if(!primes[i])
		{
			primes[i] = 1;
			for(int j=i*2;j<MAX;j+=i)
			{
				primes[j]--;
				if(primes[j] == -3)
					ans.push_back(j);
			}
		}
	}
	sort(ans.begin(),ans.end());;
	return ans[N-1];
}

int main(void){
	int N = 50;;
	cout<<smartNumber(N);
	return 0;
}