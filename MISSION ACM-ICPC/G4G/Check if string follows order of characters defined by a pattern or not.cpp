#include<bits/stdc++.h>
using namespace std;

bool followPattern(string A,string B)
{
	int hash[256] = {0};
	for(int i=0;i<B.length();i++)
		hash[B[i]] = 1;
	string C = "";
	for(int i=0;i<A.length();i++)
		if(hash[A[i]])
			C += A[i];
//	cout<<C<<endl;
	A = C;
	C = "";
	for(int i=0;i<A.length()-1;i++)
	{
		if(A[i] != A[i+1])
			C += A[i];
	}
	C += A[A.length()-1];
//	cout<<C<<endl;
	return (C == B);
}

int main(void)
{
	string A = "engineers rock";
	string B = "eger";
	cout<<followPattern(A,B);
	return 0;
}