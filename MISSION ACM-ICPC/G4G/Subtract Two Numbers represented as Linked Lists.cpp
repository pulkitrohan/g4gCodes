#include<bits/stdc++.h>
using namespace std;

struct node{
	int data;
	struct node *next;
};

node *newNode(int data)
{
	node *temp = new node;
	temp->data = data;
	temp->next = NULL;
	return temp;
}

int getLength(node *A)
{
	int len = 0;
	while(A)
	{
		A = A->next;
		len++;
	}
	return len;
}

node *padZero(node *A,int diff)
{
	if(!A)
		return NULL;
	node *temp = newNode(0);
	diff--;
	node *temp2 = temp;
	while(diff--)
	{
		temp2->next = newNode(0);
		temp2 = temp2->next;
	}
	temp2->next = A;
	return temp;
}

node *subtractUtil(node *A,node *B,bool &borrow)
{
	if(!A && !B && !borrow)
		return NULL;
	node *prev = subtractUtil(A ? A->next : NULL,B ? B->next:NULL,borrow);
	int valA = A->data;
	int valB = B->data;
	if(borrow)
	{
		valA--;
		borrow = false;
	}
	if(valA < valB)
	{
		borrow = true;
		valA += 10;
	}
	int diff = valA - valB;
	node *current = newNode(diff);
	current->next = prev;
	return current;
}

node *subtractLL(node *A,node *B)
{
	if(!A && !B)
		return NULL;
	int lenA = getLength(A);
	int lenB = getLength(B);

	node *small = NULL,*large = NULL;
	if(lenA != lenB)
	{
		if(lenA > lenB)
		{
			small = B;
			large = A;
		}
		else
		{
			small = A;
			large = B;
		}
		small = padZero(small,abs(lenA-lenB));
	}
	else
	{
		node *tempA = A,*tempB = B;
		while(A && B)
		{
			if(A->data != B->data)
			{
				if(A->data > B->data)
				{
					large = tempA;
					small = tempB;
				}
				else
				{
					large = tempB;
					small = tempA;
				}
				break;
			}
			A = A->next;
			B = B->next;
		}
	}
	bool borrow = false;
	return subtractUtil(large,small,borrow);
}

void print(node *result)
{
	while(result)
	{
		cout<<result->data<<" ";
		result = result->next;
	}
}


int main(void)
{
	node *A = newNode(1);
	A->next = newNode(0);
	A->next->next = newNode(0);
	node *B = newNode(1);
	node *result = subtractLL(A,B);
	print(result);
	return 0;
}