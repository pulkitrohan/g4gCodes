#include<bits/stdc++.h>
using namespace std;
#define R 3
#define C 10

struct Pair{
	bool found;
	int value;
};

Pair findLongestPathUtil(int A[R][C],int i,int j,int x,int y,bool visited[R][C]){
	if(i == x && j == y){
		Pair P = {true,0};
		return P;
	}
	if(i < 0 || i >= R || j < 0 || j >=C || A[i][j] == 0 || visited[i][j])
	{
		Pair P = {false,INT_MAX};
		return P;
	}
	visited[i][j] = true;
	int res  = INT_MIN;

	Pair sol = findLongestPathUtil(A,i,j-1,x,y,visited);
	if(sol.found)
		res = max(res,sol.value);

	sol = findLongestPathUtil(A,i,j+1,x,y,visited);
	if(sol.found)
		res = max(res,sol.value);

	sol = findLongestPathUtil(A,i-1,j,x,y,visited);
	if(sol.found)
		res = max(res,sol.value);

	sol = findLongestPathUtil(A,i+1,j,x,y,visited);
	if(sol.found)
		res = max(res,sol.value);

	visited[i][j] = false;
	if(res != INT_MIN)
	{
		Pair P = {true,1+res};
		return P;
	}	
	else
	{
		Pair P  = {false,INT_MAX};
		return P;
	}
}

void findLongestPath(int A[R][C],int i,int j,int x,int y){
	bool visited[R][C];
	memset(visited,false,sizeof(visited));
	Pair P = findLongestPathUtil(A,i,j,x,y,visited);
	if(P.found)
		cout<<P.value;
	else
		cout<<"not found";
}

int main(void){
	int A[R][C] = {
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 1, 0, 1, 1, 0, 1, 1, 0, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }
    };
    findLongestPath(A,0,0,1,7);
    return 0;
}