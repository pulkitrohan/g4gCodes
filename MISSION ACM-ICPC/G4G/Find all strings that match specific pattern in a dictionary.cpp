#include<bits/stdc++.h>
using namespace std;

string encode(string A)
{
	unordered_map<char,int> M;
	string ans = "";
	for(int i=0;i<A.length();i++)
	{
		if(M.find(A[i]) == M.end())
			M[A[i]] = i;
		ans += to_string(M[A[i]]);
	}
	return ans;
}

void findMatchedWords(vector<string> &dict,string P)
{
	int N = P.length();
	string hash = encode(P);
	for(int i=0;i<dict.size();i++)
	{
		string temp = dict[i];
		if(temp.length() == N && encode(temp) == hash)
			cout<<temp<<" ";
	}
}

int main(void)
{
	vector<string> dict;
	dict.push_back("abb");
	dict.push_back("aab");
	dict.push_back("xyz");
	dict.push_back("xyy");
	string P = "foo";
	findMatchedWords(dict,P);
	return 0;
}