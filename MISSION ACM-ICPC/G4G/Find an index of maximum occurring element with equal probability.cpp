#include<bits/stdc++.h>
using namespace std;

void FindRandom(int *A,int N)
{
	unordered_map<int,int> M;
	for(int i=0;i<N;i++)
		M[A[i]]++;
	int max_element,max_so_far = INT_MIN;
	for(pair<int,int> P : M)
	{
		if(P.second > max_so_far)
		{
			max_so_far = P.second;
			max_element = P.first;
		}
	}
	int r = (rand()%max_so_far)+1;
	int count = 0;
	for(iint i=0;i<N;i++)
	{
		if(A[i] == max_element)
			count++;
		if(count == r)
		{
			cout<<i<<endl;
			break;
		}
	}
}

int main(void)
{
	int A[] = {-1,4,9,7,7,2,7,3,0,9,6,5,7,8,9};
	int N = sizeof(A)/sizeof(A[0]);
	srand(time(NULL));
	FindRandom(A,N);
	return 0;
}