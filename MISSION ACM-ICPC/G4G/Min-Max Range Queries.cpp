#include<bits/stdc++.h>
using namespace std;

struct node
{
	int maximum,minimum;
};

void constructSTUtil(int *A,int start,int end,node *st,int index)
{
	if(start == end)
	{
		st[index].minimum = A[start];
		st[index].maximum = A[start];
		return;
	}
	int mid = start + (end-start)/2;
	constructSTUtil(A,start,mid,st,2*index + 1);
	constructSTUtil(A,mid+1,end,st,2*index+2);
	st[index].minimum = min(st[2*index+1].minimum,st[2*index+2].minimum);
	st[index].maximum = max(st[2*index+1].maximum,st[2*index+2].maximum);
}

node *constructST(int *A,int N)
{
	int x = (int)(ceil(log2(N)));
	int max_size = 2*(int)pow(2,x)-1;
	struct node *st = new struct node[max_size];
	constructSTUtil(A,0,N-1,st,0);
	return st;
}

node MaxMinUtil(node *st,int start_index,int end_index,int start,int end,int index)
{
	node temp,left,right;
	if(start <= start_index && end >= end_index)
		return st[index];

	if(end_index < start || start_index > end)
	{
		temp.minimum = INT_MAX;
		temp.maximum = INT_MIN;
		return temp;
	}
	int mid = start_index + (end_index-start_index)/2;;
	left = MaxMinUtil(st,start_index,mid,start,end,index*2+1);
	right = MaxMinUtil(st,mid+1,end_index,start,end,index*2+2);
	temp.minimum = min(left.minimum,right.minimum);
	temp.maximum = max(left.maximum,right.maximum);
	return temp;
}

node MaxMin(node *st,int start,int end,int N)
{
	node temp;
	if(start < 0 || end > N-1 || start > end)
	{
		cout<<"Invalid input";
		temp.minimum = INT_MIN;
		temp.maximum = INT_MAX;
		return temp;
	}
	return MaxMinUtil(st,0,N-1,start,end,0);
}



int main(void){

	int A[] = {1,8,5,9,6,14,2,4,3,7};
	int N = sizeof(A)/sizeof(A[0]);
	node *st = constructST(A,N);
	int start = 0,end = 8;
	node result = MaxMin(st,start,end,N);
	cout<<result.minimum<<" "<<result.maximum<<endl;
	return 0;
}