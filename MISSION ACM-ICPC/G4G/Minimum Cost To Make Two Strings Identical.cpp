#include<bits/stdc++.h>
using namespace std;

int FindMinCost(string A,string B,int A_cost,int B_cost)
{
	int dp[A.length()+1][B.length()+1];
	for(int i=0;i<=A.length();i++)
	{
		for(int j=0;j<=B.length();j++)
		{
			if(i == 0 || j == 0)
				dp[i][j] = 0;
			else if(A[i-1] == B[j-1])
				dp[i][j] = 1 + dp[i-1][j-1];
			else
				dp[i][j] = max(dp[i-1][j],dp[i][j-1]);
		}
	}
	int len = dp[A.length()][B.length()];
	return A_cost*(A.length()-len) + B_cost*(B.length()-len);
}

int main(void)
{
	string A = "ef";
	string B = "gh";
	cout<<FindMinCost(A,B,10,20);
	return 0;
}