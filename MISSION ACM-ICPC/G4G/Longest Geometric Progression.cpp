#include<bits/stdc++.h>
using namespace std;

int LGP(int *A,int N){
	if(N < 2)
	return N;
	if(N == 2)
	{
		if(A[1]%A[0] == 0)
			return N;
		else 
			return 1;
	}
	sort(A,A+N);
	int dp[N+1][N+1];
	int ans = 1;
	for(int i=0;i<N;i++)
	{
		if(A[N-1]%A[i] == 0)
			dp[i][N-1] = 2;
		else
			dp[i][N-1] = 1;
	}
	for(int i=N-2;i>=1;i--)
	{
		int j = i-1,k = i+1;
		while(j >= 0 && k <= N-1)
		{
			if(A[j]*A[k] < A[i]*A[i])
				k++;
			else if(A[j]*A[k] > A[i]*A[i])
			{
				if(A[i]%A[j] == 0)
					dp[j][i] = 2;
				else
					dp[j][i] = 1;
				j--;
			}
			else 
			{
				dp[j][i] = dp[i][k] + 1;
				if(dp[j][i] > ans)
					ans = dp[j][i];
				j--,k++;
			}
		}
		while(j >= 0)
		{
			if(A[i]%A[j] == 0)
				dp[j][i] = 2;
			else
				dp[j][i] = 1;
			j--;
		}
	}
	return ans;
}

int main(void){
	int A[] = {1,3,4,9,7,27};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<LGP(A,N)<<endl;
	return 0;
}