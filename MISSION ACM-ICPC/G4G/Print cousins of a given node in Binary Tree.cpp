#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right;
};

Tree *newNode(int data)
{
	Tree *node = (Tree *)malloc(sizeof(Tree));
	node->data = data;
	node->left = node->right = NULL;
	return node;
}

int FindLevel(Tree *root,Tree *node,int level)
{
	if(!root)
		return 0;
	if(root == node)
		return level;
	int left = FindLevel(root->left,node,level+1);
	if(left != 0)
		return left;
	return FindLevel(root->right,node,level+1);
}

void printCousinUtil(Tree *root,Tree *node,int level)
{
	if(!root || level < 2)
		return;
	if(level == 2)
	{
		if(root->left == node || root->right == node)
			return;
		if(root->left)
			cout<<root->left->data<<" ";
		if(root->right)
			cout<<root->right->data<<" ";
	}
	else
	{
		printCousinUtil(root->left,node,level-1);
		printCousinUtil(root->right,node,level-1);
	}
}


void printCousins(Tree *root,Tree *node)
{
	int level = FindLevel(root,node,1);
	if(level == 0)
	{
		cout<<"Node not found in tree\n";
		return;
	}
	printCousinUtil(root,node,level);
}

int main(void)
{
	Tree *root = newNode(1);
	root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);
    root->left->right->right = newNode(15);
    root->right->left = newNode(6);
    root->right->right = newNode(7);
    root->right->left->right = newNode(8);
	
	printCousins(root,root->left->right);
	return 0;
}