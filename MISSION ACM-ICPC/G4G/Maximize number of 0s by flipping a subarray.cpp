#include<bits/stdc++.h>
using namespace std;

int FindMaxZeroCount(bool *A,int N)
{
	int global_max = 0,cur_max = 0,zero_count = 0;
	for(int i=0;i<N;i++)
	{
		if(A[i] == 0)
			zero_count++;
		int val = (A[i] == 1) ? 1 : -1;
		cur_max = max(val,cur_max+val);
		global_max = max(global_max,cur_max);
	}
	global_max = max(0,global_max);
	return zero_count + global_max;
}

int main(void)
{
	bool A[] = {0,1,0,0,1,1,0};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<FindMaxZeroCount(A,N);
	return 0;
}