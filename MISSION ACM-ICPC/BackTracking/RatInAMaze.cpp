#include<cstdio>

#define N 4

bool isSafe(int M[N][N],int i,int j)
{
  if((i >= 0 && i < N) && (j >= 0 && j < N) && (M[i][j] == 1))
    return true;
  else
    return false;
}

bool solveMazeUtil(int M[N][N],int x,int y,int sol[N][N])
{
    if(x == N-1 && y == N-1)   // Base Case
    {
        sol[x][y] = 1;
        return true;
    }
    if(isSafe(M,x,y) == true)
    {
        sol[x][y] = 1;
        if(solveMazeUtil(M,x+1,y,sol))    //down
          return true;
        if(solveMazeUtil(M,x,y+1,sol))    //right
          return true;
        sol[x][y] = 0;
        return false;
    }
    return false;
}
void print(int sol[N][N])
{
  for(int i=0;i< N;i++)
  {
    for(int j=0;j<N;j++)
      printf("%d ",sol[i][j]);
      printf("\n");
  }
}

bool solveMaze(int M[N][N])
{
    int sol[N][N] = {0};
    if(!solveMazeUtil(M,0,0,sol))
        return false;
    print(sol);
    return true;
}



int main(void)
{
    int M[N][N];
    printf("Enter the Maze Elements : ");
    for(int i=0;i<N;i++)
        for(int j=0;j<N;j++)
                scanf("%d",&M[i][j]);
    solveMaze(M);
    return 0;
}
