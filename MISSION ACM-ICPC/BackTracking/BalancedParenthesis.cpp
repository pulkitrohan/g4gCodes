#include<bits/stdc++.h>
using namespace std;
char target[100];

void GenerateP(int N,int count,int open,int close)
{
	if(count > N || open > N/2 || close > N/2 || close > open)
		return;
	if(count == N)
	{
		target[count] = '\0';
		cout<<target<<endl;
		return;
	}
	target[count] = '(';
	GenerateP(N,count+1,open+1,close);
	target[count] = ')';
	GenerateP(N,count+1,open,close+1);
}

int main(void)
{
	int N = 2;
	GenerateP(N*2,0,0,0);
	return 0;
}