#include<stdio.h>

void TowerOfHanoi(char frompeg,char topeg,char interpeg,int N)
{
    if(N == 1)
        printf("Move disk %d from %c to %c\n",N,frompeg,topeg);
    else
	{
		TowerOfHanoi(frompeg,interpeg,topeg,N-1);
		printf("Move disk %d from %c to %c\n",N,frompeg,topeg);
		TowerOfHanoi(interpeg,topeg,frompeg,N-1);
	}

}

int main(void)
{
    int N;
    printf("Enter the number of Disks you want : ");
    scanf("%d",&N);
    TowerOfHanoi('A','C','B',N);
    return 0;
}
