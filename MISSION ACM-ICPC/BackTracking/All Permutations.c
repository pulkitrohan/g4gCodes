#include<stdio.h>

void swap(char *a,char *b)
{
	char t;
	t = *a;
	*a = *b;
	*b = t;
}

void perm(char *A,int start,int end)
{
	if(start == end)
		printf("%s\n",A);
	else
	{
		for(int i=start;i<=end;i++)
		{
			swap(A[start],A[i]);
			perm(A,start+1,end);
			swap(A[start],A[i]);
		}
	}
}

int main(void)
{
	char A[100];
	scanf("%s",&A);
	perm(A,0,strlen(A)-1);
	return 0;
}

