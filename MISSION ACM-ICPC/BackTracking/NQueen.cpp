#include<cstdio>
#include<cstdlib>

int A[100]={0},count =0;

bool place(int k,int i)
{
	for(int j=1;j<k;j++)
	{
		if(A[j] == i || abs(A[j] - i) == abs(j-k) ) //Same Column || Diagonal
		return false;
	}
	return true;
}

void NQueen(int k,int N)
{
	for(int i=1;i<=N;i++)
	{
		if(place(k,i))
		{
			A[k] = i;
			if(k == N)
			{
				for(int j=1;j<=N;j++)
                    printf("%d ",A[j]);
                printf("\n");
                count++;
			}
			else
				NQueen(k+1,N);
		}
	}
}

int main(void)
{
    int N;
	printf("Enter the Size of Chess Board : ");
	scanf("%d",&N);
	NQueen(1,N);
	printf("Count : %d",count);
	return 0;
}
