#include<bits/stdc++.h>
using namespace std;

void PrintAllPaths(int MAT[][3],int *path,int x,int y,int N,int M,int index)
{
	if(x == N-1)
	{
		for(int i = y;i<M;i++)
			path[index+i-y] = MAT[x][i];
		for(int i=0;i<index+M-y;i++)
			cout<<path[i]<<" ";
		cout<<endl;
		return;
	}
	if(y == M-1)
	{
		for(int i=x;i<N;i++)
			path[index+i-x] = MAT[i][y];
		for(int i=0;i<index+N-x;i++)
			cout<<path[i]<<" ";
		cout<<endl;
		return;
	}
	path[index] = MAT[x][y];
	PrintAllPaths(MAT,path,x+1,y,N,M,index+1);
	PrintAllPaths(MAT,path,x,y+1,N,M,index+1);
}

int main(void)
{
	int MAT[2][3] = {{1,2,3},{4,5,6}};
	int N = 2,M = 3;
	int path[N+M];
	PrintAllPaths(MAT,path,0,0,N,M,0);
}