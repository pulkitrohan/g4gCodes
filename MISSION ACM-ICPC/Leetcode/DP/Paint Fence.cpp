/*
There is a fence with n posts, each post can be painted with one of the k colors.
You have to paint all the posts such that no more than two adjacent fence posts have the same color.
Return the total number of ways you can paint the fence
*/

#include<bits/stdc++.h>
using namespace std;

int NoWays(int N,int K)
{
	int same[N+1],diff[N+1];
	same[0] = 0;
	diff[0] = K;
	for(int i=1;i<N;i++)
	{
		same[i] = diff[i-1];
		diff[i] = (K-1)*(same[i-1]+diff[i-1]);
	}
	return same[N-1] + diff[N-1];
}

int main(void)
{
	int N = 5,K = 3;
	cout<<NoWays(N,K);
	return 0;
}