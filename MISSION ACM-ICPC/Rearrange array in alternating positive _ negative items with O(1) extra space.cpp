#include<bits/stdc++.h>
using namespace std;

void Rearrange(int *A,int N)
{
	int start = -1,end = N;
	while(end > start)
	{
		while(A[++start] > 0);
		while(A[--end] < 0);
		if(end > start)
			swap(A[start],A[end]);
	}
	if(start == 0 || start == N)
		return;
	int k = 0;
	while(k < N && start < N)
	{
		swap(A[k],A[start]);
		start++;
		k += 2;
	}
}

int main(void)
{
	int A[] = {2,3,-4,-1,6,-9};
	int N = sizeof(A)/sizeof(A[0]);
	for(int i=0;i<N;i++)
		cout<<A[i]<<" ";
	cout<<endl;
	Rearrange(A,N);
	for(int i=0;i<N;i++)
		cout<<A[i]<<" ";
	return 0;
}