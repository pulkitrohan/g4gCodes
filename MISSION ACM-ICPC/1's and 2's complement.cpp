#include<bits/stdc++.h>
using namespace std;


void printOnesAndTwosComplement(string S)
{
	string ones="",twos="";
	for(int i=0;i<S.length();i++)
		if(S[i] == '0')
			ones += "1";
		else
			ones += "0";
	twos = ones;
	int i;
	for(i=S.length()-1;i>=0;i--)
	{
		if(ones[i] == '1')
			twos[i] = '0';
		else
		{
			twos[i] = '1';
			break;
		}
	}
	if(i == -1)
		twos  = "1" + twos;
	cout<<"1's complement : "<<ones<<endl;
	cout<<"2's complement : "<<twos<<endl;
}


int main(void)
{
	string S = "1100";
	printOnesAndTwosComplement(S);
	return 0;
}