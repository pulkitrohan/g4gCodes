#include<bits/stdc++.h>
using namespace std;
#define M 4
#define N 5
int main(void)
{
	int A[M][N] = {{1, 2, 3, 4, 6},
                    {5, 3, 8, 1, 2},
                    {4, 6, 7, 5, 5},
                    {2, 4, 8, 9, 4} };
   int aux[M][N];
   
	for(int i=0;i<N;i++)
		aux[0][i] = A[0][i];
		
	for(int i=1;i<M;i++)
		for(int j=0;j<N;j++)
			aux[i][j] = A[i][j] +aux[i-1][j];
			
	for(int i=0;i<M;i++)
		for(int j=1;j<N;j++)
			aux[i][j] += aux[i][j-1];
	
	int x1 = 2,y1 = 2,x2 = 3,y2 = 4;
	int ans = aux[x2][y2];
	if(x1 > 0)
		ans -= aux[x1-1][y2];
	if(y1 > 0)
		ans -= aux[x2][y1-1];
	if(x1 > 0 && y1 > 0)
		ans += aux[x1-1][y1-1];
	cout<<ans<<endl;
	return 0;
}