#include<bits/stdc++.h>
using namespace std;

int sumofdigits(int N)
{
	if(N < 10)
		return N*(N+1)/2;
	int d = log10(N);
	
	int dp[d+1];
	dp[0] = 0,dp[1] = 45;
	int val = 10;
	for(int i=2;i<=d;i++)
	{
		dp[i] = dp[i-1]*10 + 45*val;
		val *= 10;
	}
	int p = pow(10,d);
	int msd = N/p;
	
	return msd * dp[d] + (msd*(msd-1)/2)*p + msd*(1+N%p) + sumofdigits(N%p);
}

int main(void)
{
	int N = 328;
	cout<<sumofdigits(N);
	return 0;
}