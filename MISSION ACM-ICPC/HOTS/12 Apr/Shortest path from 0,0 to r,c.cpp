#include<bits/stdc++.h>
using namespace std;

struct dis
{
	int x,y,d;
};

int solveMaze(int M[4][4],int N)
{
	int xx[] = {-1,0,1,0};
	int yy[] = {0,-1,0,1};
	struct dis D;
	D.x = 0,D.y = 0,D.d = 0;
	bool visited[N][N];
	queue<struct dis> Q;
	Q.push(D);
	while(!Q.empty())
	{
		struct dis temp = Q.front();
		Q.pop();
		visited[temp.x][temp.y] = 1;
		if(temp.x == N-1 && temp.y == N-1)
			return temp.d;
		for(int i=0;i<4;i++)
		{
			int dx = temp.x + xx[i];
			int dy = temp.y + yy[i];
			int distance = temp.d + 1;
			if(dx >= 0 && dx < N && dy >= 0 && dy < N && !visited[dx][dy] && M[dx][dy]==1)
			{
				struct dis t;
				t.x = dx,t.y = dy,t.d = distance;
				Q.push(t);
			}
		}
	}
	return -1;
}

int main(void)
{
	int M[4][4] = {{1,0,0,0},{1,1,0,1},{0,1,0,0},{1,1,1,1}};
	int val = solveMaze(M,4);
	if(val != -1)
		cout<<"SHORTEST PATH DISTANCE : ",val;
	return 0;
}