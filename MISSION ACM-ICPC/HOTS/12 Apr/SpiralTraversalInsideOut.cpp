#include<bits/stdc++.h>
using namespace std;

void printSpiral(int MAT[][5],int M,int N,int k)
{
	int top = k,bottom = M-k-1;
	int left = k,right = N-k-1;
	if(top > bottom || left > right)
		return;
	printSpiral(MAT,M,N,k+1);
	for(int i=top+1;i<=bottom;i++)
		cout<<MAT[i][right]<<" ";
	for(int i=right-1;i>=left;i--)
		cout<<MAT[bottom][i]<<" ";
	for(int i=bottom-1;i>top;i--)
		cout<<MAT[i][left]<<" ";
	for(int i=left;i<=right;i++)
		cout<<MAT[top][i]<<" ";
}


int main(void)
{
	int M[5][5] = {
					{1,2,3,4,5},
				    {6,7,8,9,10},
				    {11,12,13,14,15},
				    {16,17,18,19,20},
				   	{21,22,23,24,25}
				    };
	printSpiral(M,5,5,0);
	return 0;
}