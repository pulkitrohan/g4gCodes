#include<bits/stdc++.h>
using namespace std;
//https://leetcode.com/discuss/9265/share-my-simple-o-log-m-n-solution-for-your-reference
int getkth(int s[], int m, int l[], int n, int k){
        // let m <= n
        if (m > n) 
            return getkth(l, n, s, m, k);
        if (m == 0)
            return l[k - 1];
        if (k == 1)
            return min(s[0], l[0]);

        int i = min(m, k / 2), j = min(n, k / 2);
        if (s[i - 1] > l[j - 1])
            return getkth(s, m, l + j, n - j, k - j);
        else
            return getkth(s + i, m - i, l, n, k - i);
        return 0;
    }

int main(void)
{
	int A[] = {10,20,25};
	int B[] = {30,50,100,110,200};
	int M = sizeof(A)/sizeof(A[0]);
	int N = sizeof(B)/sizeof(B[0]);
	int L = (M+N+1)/2;
	int R = (M+N)/2 + 1;
	cout<<(Find_Kth(A,M,B,N,L) + Find_Kth(A,M,B,N,R))/2.0;
	return 0;
}