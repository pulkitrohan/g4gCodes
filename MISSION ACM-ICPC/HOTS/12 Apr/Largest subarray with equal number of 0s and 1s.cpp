#include<bits/stdc++.h>
using namespace std;
#define VALUE(A) ((A == 1) ? 1 : -1)

int FindSubArray(int *A,int N)
{
	int maxsize = -1,start = -1;
	int sumleft[N];
	sumleft[0] = VALUE(A[0]);
	int min_val = sumleft[0],max_val = sumleft[0];
	for(int i=1;i<N;i++)
	{
		sumleft[i] = sumleft[i-1] + VALUE(A[i]);
		min_val = min(min_val,sumleft[i]);
		max_val = max(max_val,sumleft[i]);
	}
	for(int i=N-1;i>=0;i--)
	{
		if(sumleft[i] == 0)
		{
			maxsize = i+1;
			start = 0;
			break;
		}
	}
	int hash[max_val-min_val+1];
	memset(hash,-1,sizeof(hash));
	
	for(int i=0;i<N;i++)
	{
		if(hash[sumleft[i]-min_val] == -1)
			hash[sumleft[i]-min_val] = i;
		else
		{
			if(i-hash[sumleft[i]-min_val] > maxsize)
			{
				maxsize = i-hash[sumleft[i]-min_val];
				start = hash[sumleft[i]-min_val]+1;
			}
		}
	}
	if(maxsize == -1)
		printf("NO SUCH SUBARRAY\n");
	else
		printf("%d to %d\n",start,start+maxsize-1);
	return maxsize;
}
			

int main(void)
{	
	int A[] = {1,0,0,1,0,1,1};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<FindSubArray(A,N);
	return 0;
}

