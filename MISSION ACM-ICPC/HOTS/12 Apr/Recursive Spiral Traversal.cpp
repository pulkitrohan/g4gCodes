#include<bits/stdc++.h>
using namespace std;

void printSpiral(int MAT[][3],int M,int N,int k)
{
	int top = k,bottom = M-k-1;
	int left = k,right = N-k-1;
	if(top > bottom || left > right)
		return;
	for(int i=left;i<=right;i++)
		cout<<MAT[top][i]<<" ";
	for(int i=top+1;i<=bottom;i++)
		cout<<MAT[i][right]<<" ";
	for(int i=right-1;i>=left;i--)
		cout<<MAT[bottom][i]<<" ";
	for(int i=bottom-1;i>top;i--)
		cout<<MAT[i][left]<<" ";
	printSpiral(MAT,M,N,k+1);
}

int main(void)
{
	int M[3][3] = {{1,2,3},{4,5,6},{7,8,9}};
	printSpiral(M,3,3,0);
	return 0;
}