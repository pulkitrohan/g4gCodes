#include<bits/stdc++.h>
using namespace std;

int FindNumberOfTraingles(int *A,int N)
{
	sort(A,A+N);
	int count = 0;
	for(int i=0;i<N-2;i++)
	{
		int k = i+2;
		for(int j=i+1;j<N-1;j++)
		{
			while(k < N && A[i] + A[j] > A[k])
				k++;
			count += k-j-1;
		}
	}
	return count;
}

int main(void)
{
	int A[] = {10,21,22,100,101,200,300};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<FindNumberOfTraingles(A,N);
	return 0;
}
}