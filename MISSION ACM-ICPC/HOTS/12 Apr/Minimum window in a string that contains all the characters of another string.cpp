#include<bits/stdc++.h>
using namespace std;
//http://articles.leetcode.com/2010/11/finding-minimum-window-in-s-which.html
bool minWindow(char *S,char *T,int &start,int &final)
{
	int N = strlen(S);
	int M = strlen(T);
	int NeedToFind[256] = {0};
	for(int i=0;i<M;i++)
		NeedToFind[T[i]]++;
	int HasFound[256] = {0};
	int minlen = INT_MAX;
	int count = 0;
	for(int begin=0,end = 0;end < N;end++)
	{
		if(NeedToFind[S[end]] == 0)
			continue;
		HasFound[S[end]]++;
		if(HasFound[S[end]] <= NeedToFind[S[end]])
			count++;
		if(count == M)
		{
			while(NeedToFind[S[begin]] == 0 || HasFound[S[begin]] > NeedToFind[S[begin]])
			{
				if(HasFound[S[begin]] > NeedToFind[S[begin]])
					HasFound[S[begin]]--;
				begin++;
			}
			int len = end-begin+1;
			if(len < minlen)
			{
				start = begin;
				final = end;
				minlen = len;
			}
		}
	}
	if(count == M)
		return true;
	else
		return false;
}

int main(void)
{
	char S[] = "this is a test string";
	char T[] = "tist";
	int start = -1,end = -1;
	if(minWindow(S,T,start,end))
	{
		for(int i=start;i<=end;i++)
			cout<<S[i];
	}
	else
		cout<<"NOT FOUND\n";
	return 0;
}
		