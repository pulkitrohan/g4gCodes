#include<bits/stdc++.h>
using namespace std;

struct node
{
	int data;
	vector<node *> next;
};

node *newNode(int data)
{
	node *temp = (node *)malloc(sizeof(node));
	temp->data = data;
	return temp;
}

node *cloneGraph(node *G,unordered_map<node *,node *> &M)
{
	if(!G)
		return NULL;
	if(M.find(G) != M.end())
		return M[G];
	node *graphCopy = newNode(G->data);
	M[G] = graphCopy;
	for(int i=0;i<G->next.size();i++)
		graphCopy->next.push_back(cloneGraph(G->next[i],M));
	return graphCopy;
}

void bfs(node *G)
{
	int visited[10] = {0};
	queue<node *> Q;
	Q.push(G);
	while(!Q.empty())
	{
		node *temp = Q.front();
		Q.pop();
		visited[temp->data] = 1;
		printf("%d ",temp->data);
		for(int i=0;i<temp->next.size();i++)
		{
			if(!visited[temp->next[i]->data])
				Q.push(temp->next[i]);
		}
	}
	printf("\n");
}

int main(void)
{
	node *G = (node *)malloc(sizeof(node));
	node *A = (node *)malloc(sizeof(node));
	node *B = (node *)malloc(sizeof(node));
	G->data = 1;
	A->data = 2;
	B->data = 3;
	G->next.push_back(B);
	G->next.push_back(A);
	A->next.push_back(B);
	A->next.push_back(G);
	B->next.push_back(G);
	B->next.push_back(A);
	unordered_map<node *,node *> M;
	node *res = cloneGraph(G,M);
	for(int i=0;i<G->next.size();i++)
		cout<<G->next[i]->data<<" ";
	cout<<endl;
	for(int i=0;i<A->next.size();i++)
		cout<<A->next[i]->data<<" ";
	cout<<endl;
	for(int i=0;i<B->next.size();i++)
		cout<<B->next[i]->data<<" ";
	cout<<endl;
	return 0;
}