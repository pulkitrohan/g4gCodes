#include<bits/stdc++.h>
using namespace std;
//http://algorithmsandme.com/2014/12/find-kth-smallest-element-in-two-sorted-arrays/
int Find_Kth(int *A,int M,int *B,int N,int k)
{
	if(M > N)
		return Find_Kth(B,N,A,M,k);
	if(M == 0 && N > 0)
		return B[k-1];
	if(k == 1)
		return min(A[0],B[0]);
	int i = min(M,k/2);
	int j = min(N,k/2);
	if(A[i-1] > B[j-1])
		return Find_Kth(A,M,B+j,N-j,k-j);
	else
		return Find_Kth(A+i,M-i,B,N,k-i);
	return -1;
}

int main(void)
{
	int A[] = {10,20,25};
	int B[] = {30,50,100,110,200};
	int M = sizeof(A)/sizeof(A[0]);
	int N = sizeof(B)/sizeof(B[0]);
	cout<<Find_Kth(A,M,B,N,4);
	return 0;
}