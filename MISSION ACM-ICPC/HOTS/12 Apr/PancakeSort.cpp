#include<bits/stdc++.h>
using namespace std;

void flip(int *A,int end)
{
	int start = 0;
	while(start < end)
	{
		swap(A[start],A[end]);
		start++,end--;
	}
}

int FindMax(int *A,int N)
{
	int index = 0;
	for(int i=1;i<N;i++)
		if(A[i] > A[index])
			index = i;
	return index;
}

void printArray(int *A,int N)
{
	for(int i=0;i<N;i++)
		printf("%d ",A[i]);
	printf("\n");
}

void pancakeSort(int *A,int N)
{
	//int iteration = 1;
	for(int size = N;size > 1;size--)
	{
		int index = FindMax(A,size);
		if(index != size-1)
		{
			//printf("Iteration : %d\n",iteration++);
			flip(A,index);
			//printArray(A,N);
			flip(A,size-1);
			//printArray(A,N);
		}
	}
}

int main(void)
{
	int A[] = {10,20,23,12,6,7};
	int N = sizeof(A)/sizeof(A[0]);
	pancakeSort(A,N);
	printArray(A,N);
	return 0;
}