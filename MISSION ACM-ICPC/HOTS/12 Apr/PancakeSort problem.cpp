#include<bits/stdc++.h>
using namespace std;

void flip(int *A,int end)
{
	int start = 0;
	while(start < end)
	{
		swap(A[start],A[end]);
		start++,end--;
	}
}

void printArray(int *A,int N)
{
	for(int i=0;i<N;i++)
		printf("%d ",A[i]);
	printf("\n");
}

int FindCeil(int *A,int low,int high,int x)
{
	if(x <= A[low])
		return low;
	if(x > A[high])
		return -1;
	int mid = (low+high)/2;
	if(A[mid] == x)
		return mid;
	else if(A[mid] < x)
	{
		if(mid+1 <= high && A[mid+1] >= x)
			return mid+1;
		else
			return FindCeil(A,mid+1,high,x);
	}
	else
	{
		if(mid-1 >= low && x > A[mid-1])
			return mid;
		else
			return FindCeil(A,low,mid-1,x);
	}
}

void Sort(int *A,int N)
{
	for(int i=1;i<N;i++)
	{
		int j = FindCeil(A,0,i-1,A[i]);
		if(j != -1)
		{
			flip(A,j-1);
			flip(A,i-1);
			flip(A,i);
			flip(A,j);
		}
	}
}

int main(void)
{
	int A[] = {10,20,23,12,6,7};
	int N = sizeof(A)/sizeof(A[0]);
	Sort(A,N);
	printArray(A,N);
	return 0;
}