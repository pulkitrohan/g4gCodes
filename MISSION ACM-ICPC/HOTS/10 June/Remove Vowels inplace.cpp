#include<bits/stdc++.h>
using namespace std;

bool isVowel(char ch)
{
	if (ch == 'A' || ch == 'a' || ch == 'E' || ch == 'e' || ch == 'I' || ch == 'i' ||	ch == 'O' || ch == 'o' || ch == 'U' || ch == 'u')
		return true;
	return false;
}

int main()
{
	string S = "PUULKIIT";
	int count = 0,start = -1,len = S.length();
	for(int i=0;i<len;i++)
		if(!isVowel(S[i]))
			S[count++] = S[i];
	S = S.substr(0,count);
	cout<<S;
	return 0;
}