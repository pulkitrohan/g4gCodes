#include<stdio.h>

float SquareRoot(float N)
{
    float x = N;
    float y = 1;
    float e = 0.000001;
    while(x - y > e)
    {
        x = (x + y)/2;
        y = N/x;
    }
    return x;
}

int main(void)
{
    float N = 49;
    printf("Square Root of %f is %.2f\n",N,SquareRoot(N));
    return 0;
}
