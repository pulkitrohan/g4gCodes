#include<stdio.h>

int power(int x,int y)
{
    if(y == 0)
        return 1;
    int answer = x,increment = x;
    int i,j;
    for(i=1;i<y;i++)
    {
        for(j=1;j<x;j++)
            answer += increment;
        increment = answer;
    }
    return answer;
}

int main(void)
{
    printf("%d",power(5,6));
    return 0;
}
