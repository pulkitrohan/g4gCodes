#include<stdio.h>

void Print(int N)
{
    if(N < 10)
    {
        printf("%d\n",N+10);
        return;
    }
    int Res[100],j = 0,i;
    for(i=9;i>1;i--)
    {
        while(N % i == 0)
        {
            N = N/i;
            Res[j++] = i;
        }
    }
    if(N > 10)
    {
        printf("Not Possible\n");
        return;
    }
    for(i=j-1;i>=0;i--)
        printf("%d",Res[i]);
    printf("\n");
}

int main(void)
{
    Print(36);
    Print(100);
    Print(1);
    Print(13);
}
