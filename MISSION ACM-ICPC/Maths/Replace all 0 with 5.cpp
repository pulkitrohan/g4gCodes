#include<bits/stdc++.h>
using namespace std;

int convert0to5Rec(int N)
{
	if(N == 0)
		return 0;
	int digit = N%10;
	if(digit == 0)
		digit = 5;
	return convert0to5Rec(N/10)*10 + digit;
}

int main(void)
{
	int N = 102;
	cout<<convert0to5Rec(N);
	return 0;
}