#include<stdio.h>
#define MAX_SIZE 1000000
#define MAX_POINT 5

void PrintArray(int *A,int N)
{
    int i;
    for(i=0;i<N;i++)
        printf("%d ",A[i]);
    printf("\n");
}

void PrintCompositions(int N,int i)
{
    static A[MAX_SIZE];
    if(N == 0)
    {
        PrintArray(A,i);
    }
    else if(N > 0)
    {
        int j;
        for(j = 1;j<= MAX_POINT;j++)
        {
            A[i] = j;
            PrintCompositions(N-j,i+1);
        }
    }
}

int main(void)
{
    int N = 4;
    PrintCompositions(N,0);
    return 0;
}
