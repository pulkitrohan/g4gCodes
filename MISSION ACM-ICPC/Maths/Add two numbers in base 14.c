#include<stdio.h>
#include<stdio.h>

int GetNumeralValue(char A)
{
    if(A >= '0' && A <= '9')
        return (A - '0');
    else if(A >= 'A' && A <= 'D')
        return (A - 'A' + 10);
}

char GetNumeral(int A)
{
    if(A >= 0 && A <= 9)
        return (A + '0');
    else if(A >= 10 && A <= 14)
        return (A + 'A' - 10);
}

char *Add(char *A,char *B)
{
    if(strlen(A) != strlen(B))
        return ;
    int N = strlen(A);
    int carry = 0,i;
    char *Sum = (char *)malloc(sizeof(char) * (N+2));
    Sum[N+1] = '\0';
    for(i=N-1;i>=0;i--)
    {
        int num1 = GetNumeralValue(A[i]);
        int num2 = GetNumeralValue(B[i]);
        int sum = num1 + num2 + carry;
        if(sum >= 14)
        {
            carry = 1;
            sum -= 14;
        }
        else
            carry = 0;
        Sum[i+1] = GetNumeral(sum);
    }
    if(carry == 0)
        return (Sum + 1);
    Sum[0] = '1';
    return Sum;
}

int main(void)
{
    char A[] = "12A";
    char B[] = "CD3";
    printf("%s",Add(A,B));
    return 0;
}
