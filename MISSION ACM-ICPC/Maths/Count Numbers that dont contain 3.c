#include<stdio.h>
#include<math.h>
int Count (int N)
{
    if(N < 3)
        return N;
    else if(N >= 3 && N < 10)
        return N-1;
    int temp = N,count = 0;
    while(temp)
    {
        count++;
        temp /= 10;
    }
    int po = pow(10,count-1);
    int msd = N/po;
    if(msd != 3)
        return Count(msd) * Count(po-1) + Count(msd) + Count(N%po);
    else
        return Count(msd * po - 1);
}

int main(void)
{
    printf("%d",Count(578));
    return 0;
}
