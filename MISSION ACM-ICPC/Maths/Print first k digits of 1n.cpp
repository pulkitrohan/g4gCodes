#include<bits/stdc++.h>
using namespace std;

void Print(int N,int K)
{
	int rem = 1;
	while(K--)
	{
		cout<<(rem*10)/N;
		rem = (rem*10)%N;
	}
}

int main(void)
{
	int N = 7,k = 3;
	Print(N,k);
	return 0;
};