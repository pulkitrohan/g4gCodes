#include<stdio.h>

float Avg(int x)
{
    static sum,N;
    sum += x;
    return (((float)sum)/++N);
}

void StreamAvg(int *A,int N)
{
    int i;
    for(i=0;i<N;i++)
        printf("Average of %d numbers is %.2f\n",i+1,Avg(A[i]));
}

int main(void)
{
    int A[] = {10,20,30,40,50,60};
    int N = sizeof(A)/sizeof(A[0]);
    StreamAvg(A,N);
    return 0;
}
