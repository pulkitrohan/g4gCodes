#include<bits/stdc++.h>
using namespace std;

int gcdExtended(int A,int B,int &x,int &y)
{
	if(A == 0)
	{
		x = 0,y = 1;
		return B;
	}
	int x1,y1;
	int gcd = gcdExtended(B%A,A,x1,y1);
	x = y1 - (B/A)*x1;
	y = x1;
	return gcd;
}

int main(void)
{
	int A = 35,B = 15,x,y;
	int ans = gcdExtended(A,B,x,y);
	cout<<ans<<" "<<x<<" "<<y<<endl;
	return 0;
}