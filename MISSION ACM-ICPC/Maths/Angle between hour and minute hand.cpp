#include<bits/stdc++.h>
using namespace std;

int calcAngle(double h,double m)
{
	if(h > 12 || h < 0 || m > 60 || m < 0)
		return -1;
	if(h == 12)	h = 0;
	if(m == 60)	m = 0;
	int hour_angle = (h*60+m)*0.5;
	int minute_angle = 6*m;
	int angle = abs(hour_angle-minute_angle);
	return min(360-angle,angle);
}

int main(void)
{	
	cout<<calcAngle(12,15)<<endl;
	cout<<calcAngle(9,60)<<endl;
	return 0;
}