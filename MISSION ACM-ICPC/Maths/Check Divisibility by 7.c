/*
A Number Say N can be represented in the form N = 10*a + b
So any number N represented in above form is divisible by 7 if
a - 2*b is divisible by 7.

Proof :
    => 10*a + b
    => 20*a + 2*b
    => 21*a - a + 2*b
    => -a + 2*b(Discarding 21*a)
    => a - 2*b
*/
int IsDivisible(int N)
{
    if(N < 0)
        return IsDivisible(-N);
    else if(N == 0 || N == 7)
        return 1;
    else if(N < 10)
        return 0;
    else
        return IsDivisible(N/10 - 2*(N%10));
}

int main(void)
{
    if(IsDivisible(616))
        return 1;
    else
        return 0;
}
