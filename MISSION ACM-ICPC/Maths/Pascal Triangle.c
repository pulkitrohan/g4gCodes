#include<stdio.h>

void PrintPascal(int N)
{
    int line,i;
    for(line = 1;line<=N;line++)
    {
        int C = 1;
        for(i = 1;i<=line;i++)
        {
            printf("%d ",C);
            C = C * (line-i)/i;
        }
        printf("\n");
    }
}

int main(void)
{
    PrintPascal(5);
    return 0;
}
