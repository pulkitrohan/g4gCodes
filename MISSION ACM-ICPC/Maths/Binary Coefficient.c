#include<stdio.h>
#define max(A,B) ((A > B) ? A : B)
int Binomial(int N,int R)
{
    int result = 1,i;
    for(i=0;i<max(R,N-R);i++)
    {
        result *= (N-i);
        result /= (i+1);
    }
    return result;
}

int main(void)
{
    printf("Value of C(%d,%d) is %d\n",8,2,Binomial(8,2));
    return 0;

}
