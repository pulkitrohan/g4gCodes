#include<cstdio>
#include<cstring>
#include<algorithm>

using namespace std;

void Swap(char *A,char *B)
{
    int temp = *A;
    *A = *B;
    *B = temp;
}

void NextGreater(char *A,int N)
{
    int i,j;
    for(i=N-1;i>0;i--)
    {
        if(A[i] > A[i-1])
            break;
    }
    if(i == 0)
        return;
    int min_index = i;
    int num = A[i-1];
    for(j=i+1;j<N;j++)
    {
        if(num < A[j] && A[min_index] > A[j])
        {
            min_index = j;
        }
    }
    Swap(&A[i-1],&A[min_index]);
    sort(A+i,A+N);
    printf("%s",A);
}

int main(void)
{
    char A[] = "534976";
    int N = strlen(A);
    NextGreater(A,N);
    return 0;
}
