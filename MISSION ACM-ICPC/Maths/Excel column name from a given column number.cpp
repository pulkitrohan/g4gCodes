#include<bits/stdc++.h>
using namespace std;
void ColName(int N)
{
    string S = "";
    while(N)
    {
        S += ((N-1)%26 + 'A');
		N = (N-1)/26;
    }
    reverse(S.begin(),S.end());
	cout<<S<<endl;
}

void ColNum(char *A)
{
    int N = strlen(A);
	int j = 0,num = 0;
	int val = 1;
	for(int i=N-1;i>=0;i--)
	{
		num += (A[i]-'A'+1)*val;
		val *= 26; 
	}
	cout<<num<<endl;
}

int main(void)
{
	ColName(51);
    ColNum("Z");
    ColNum("AY");
    ColNum("AZ");
    ColNum("CB");
    ColNum("YZ");
    ColNum("ZZ");
    ColNum("AAC");
    return 0;
}
