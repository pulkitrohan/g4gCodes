#include<stdio.h>
int main(void)
{
	int N = 10,m = 3,i,j;
	int ways[N+1];
	ways[0] = 0,ways[1] = 1;
	for(i=2;i<N;i++)
	{
	    ways[i] = 0;
		for(j=1;j<=m && j<=i;j++)
			ways[i] += ways[i-j];
	}
	for(i=0;i<N;i++)
        printf("%d ",ways[i]);
	return 0;
}
