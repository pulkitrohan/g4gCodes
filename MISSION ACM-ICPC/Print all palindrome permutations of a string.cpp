#include<bits/stdc++.h>
using namespace std;

bool isPalin(string S,int *F)
{
	for(int i=0;i<S.length();i++)
		F[S[i]-'a']++;
	int odd = 0;
	for(int i=0;i<26;i++)
		if(F[i]%2)
			odd++;
	if((S.length()%2 != 0 && odd == 1) || (S.length()%2 == 0 && odd == 0))
		return true;
	return false;
}

string reverse(string S)
{
	reverse(S.begin(),S.end());
	return S;
}

void printAllPalindromes(string S)
{
	int F[26] = {0};
	if(!isPalin(S,F))
		return;
	char ch;
	string half = "";
	for(int i=0;i<26;i++)
	{
		if(F[i]%2)
			ch = i + 'a';
		half += string(F[i]/2,i+'a');
	}
	do
	{
		string ans = half;
		if(S.length()%2)
			ans += ch;
		ans += reverse(half);
		cout<<ans<<endl;
	}while(next_permutation(half.begin(),half.end()));
}

int main(void)
{
	 string S = "aabbcadad";
	 printAllPalindromes(S);
	 return 0;
}