#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right;
};

Tree *newNode(int data)
{
	Tree *node = (Tree *)malloc(sizeof(Tree *));
	node->data = data;
	node->left = node->right = NULL;
	return node;
}

void LeafDown(Tree *node,int level,int &min_dist)
{
	if(node)
	{
		if(!node->left && !node->right)
			if(level < min_dist)
				min_dist = level;
		else
		{
			LeafDown(node->left,level+1,min_dist);
			LeafDown(node->right,level+1,min_dist);
		}
	}
}

int LeafParent(Tree *root,Tree *node,int &min_dist)
{
	if(!root)
		return -1;
	if(root == node)
		return 0;
	
	int l = LeafParent(root->left,node,min_dist);
	if(l != -1)
	{
		LeafDown(root->right,l+2,min_dist);
		return l+1;
	}
	
	int r = LeafParent(root->right,node,min_dist);
	if(r != -1)
	{
		LeafDown(root->left,r+2,min_dist);
		return r+1;
	}
	return -1;
}

int minDistance(Tree *root,Tree *node)
{
	int min_dist = INT_MAX;
	LeafDown(node,0,min_dist);
	LeafParent(root,node,min_dist);
	return min_dist;
}

int main(void)
{
	Tree *root = newNode(1);
	root->left  = newNode(12);
    root->right = newNode(13);
    root->right->left   = newNode(14);
    root->right->right  = newNode(15);
    root->right->left->left   = newNode(21);
    root->right->left->right  = newNode(22);
    root->right->right->left  = newNode(23);
    root->right->right->right = newNode(24);
    root->right->left->left->left  = newNode(1);
    root->right->left->left->right = newNode(2);
    root->right->left->right->left  = newNode(3);
    root->right->left->right->right = newNode(4);
    root->right->right->left->left  = newNode(5);
    root->right->right->left->right = newNode(6);
    root->right->right->right->left = newNode(7);
    root->right->right->right->right = newNode(8);
    Tree *x = root->right;
	cout<<minDistance(root,x)<<endl;
	return 0;
}