#include<bits/stdc++.h>
using namespace std;

int count(int N)
{
	int dp[N+1];
	dp[0] = 0;
	dp[1] = 1;
	dp[2] = 2;
	for(int i=3;i<=N;i++)
		dp[i] = dp[i-1] + dp[i-2];
	return dp[N];
}

int main(void)
{
	cout<<count(5);
	return 0;
}