#include<bits/stdc++.h>
using namespace std;

int dp[100][100],s[100][100];

int MatChainMult(int *A,int start,int end)
{
	if(start == end)
		return 0;
	if(dp[start][end] != -1)
		return dp[start][end];
	dp[start][end] = INT_MAX;
	for(int k=start;k<end;k++)
	{
	    int val= MatChainMult(A,start,k)+ MatChainMult(A,k+1,end) + A[start-1]*A[k]*A[end];
	    if(val < dp[start][end])
	    {
	        dp[start][end] = val;
	        s[start][end] = k;
	    }
	}
		
	return dp[start][end];
}

void print(int i,int j){
    if(i==j)
        printf("A%d",i);
    else{
        printf("(");
        print(i,s[i][j]);
        print(s[i][j]+1,j);
        printf(")");
    }
}

int main(void)
{
	int A[] = {40, 20, 30, 10, 30};
	int N = sizeof(A)/sizeof(A[0]);
	memset(dp,-1,sizeof(dp));
	printf("Ans : %d\n",MatChainMult(A,1,N-1));
	print(1,N-1);
	return 0;
}
