#include<stdio.h>
#include<string.h>
int FindWays(int faces,int N,int sum)
{
	int dp[N+1][sum+1];
	memset(dp,0,sizeof(dp));
	for(int dice=1;dice<=N;dice++)
	{
		for(int val=1;val<=sum;val++)
		{
			for(int f=1;f<=faces && f <= val;f++)
			{
				if(dice == 1)
					dp[dice][val] = 1;
				else
					dp[dice][val] += dp[dice-1][val-f];
			}
		}
	}
	return dp[N][sum];
}


int main(void)
{
	printf("%d\n",FindWays(4,3,5));
	return 0;
}
