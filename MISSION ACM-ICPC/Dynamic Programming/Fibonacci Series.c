/*
    The Fibonacci numbers are the numbers in the following integer sequence.

    0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 141, ��..

    In mathematical terms, the sequence Fn of Fibonacci numbers is defined by the recurrence relation

    F_n = F_{n-1} + F_{n-2}
    Running Time : O(n)
    Space : O(n)
*/
#include<stdio.h>
int main(void)
{
    int n,i;
    printf("How many numbers do you want in Fibonacci Series : ");
    scanf("%d",&n);
    unsigned long long  a[n+1];
    a[0] = 1,a[1] = 1;
    for(i=2;i<n;i++)
        a[i] = a[i-1] + a[i-2] ;
    printf("\nFibonacci Series : ");
    for(i=0;i<n;i++)
        printf("%llu ",a[i]);
    return 0;
}


/* Space Optimized O(1)

if(n== 1)
    printf("1");
else
{
    int a=1,b=1;
    printf("%d %d",a,b);
    for(i=2;i<n;i++)
    {
    c = a+b;
    a = b;
    b = c;
    printf("%d ",c);
    }
}
*/
