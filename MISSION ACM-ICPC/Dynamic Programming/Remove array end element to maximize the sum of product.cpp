#include<bits/stdc++.h>
using namespace std;

int solve(vector< vector<int> > &dp, int A[], int low, int high, int turn) {
	if(low == high)
		return A[low]*turn;
	if(dp[low][high] != 0)
		return dp[low][high];
	dp[low][high] = max(A[low]*turn + solve(dp, A, low+1, high,turn+1),
						A[high]*turn + solve(dp, A, low, high-1, turn+1));
	return dp[low][high];
}

int main(void) {
	int A[] = {1,3,1,5,2};
	int N = sizeof(A)/sizeof(A[0]);
	vector<vector<int>> dp(N,vector<int>(N, 0));
	cout<<solve(dp, A, 0, N-1, 1)<<endl;
	return 0;
}