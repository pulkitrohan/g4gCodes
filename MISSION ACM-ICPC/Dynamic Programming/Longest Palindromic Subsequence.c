#include<bits/stdc++.h>
using namespace std;

int LPS(char *A,int N)
{
	int dp[N][N];
	memset(dp,0,sizeof(dp));
	for(int i=N-1;i>=0;i--)
	{
		for(int j=i;j<N;j++)
		{
			if(i == j)
				dp[i][j] = 1;
			else if((i+1 == j) && A[i] == A[j])
				dp[i][j] = 2;
			else if(A[i] == A[j])
				dp[i][j] = 2 + dp[i+1][j-1];
			else
				dp[i][j] = max(dp[i+1][j],dp[i][j-1]);
		}
	}
	return dp[0][N-1];
}
	
int main(void)
{
    char A[] = "GEEKS FOR GEEKS";
    int N = strlen(A);
    printf("The length of LPS is %d\n",LPS(A,N));
    return 0;
}
