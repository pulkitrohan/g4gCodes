#include<bits/stdc++.h>
using namespace std;

int dp[100][100];

int CountSeq(int N,int diff)
{
	if(abs(diff) > N)
		return 0;
	if(N == 1 && diff == 0)
		return 2;
	if(N == 1 && abs(diff) == 1)
		return 1;
	if(dp[N][N+diff] != -1)
		return dp[N][N+diff];
	int res = 2*CountSeq(N-1,diff) + CountSeq(N-1,diff+1) + CountSeq(N-1,diff-1);
	return dp[N][N+diff] = res;
}

int CountSeq2(int N)
{
	int ans = 1,res = 1;
	for(int r=1;r<=N;r++)
	{
		ans = ans*(N+1-r)/r;
		res += ans*ans;
	}
	return res;
}

int main(void)
{	
	memset(dp,-1,sizeof(dp));
	cout<<CountSeq(2,0)<<endl;
	cout<<CountSeq2(2)<<endl;
	return 0;
}