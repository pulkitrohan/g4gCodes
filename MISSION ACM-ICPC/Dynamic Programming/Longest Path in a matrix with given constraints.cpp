#include<bits/stdc++.h>
using namespace std;
#define n 3
int dp[n][n];


int LongestPath(int i,int j,int M[][n])
{
	if(i < 0 || i >= n || j < 0 || j >= n)
		return 0;
	if(dp[i][j] != -1)
		return dp[i][j];
	if(M[i][j]+1 == M[i][j+1])
		return dp[i][j] = 1 + LongestPath(i,j+1,M);
	if(M[i][j]+1 == M[i+1][j])
		return dp[i][j] = 1 + LongestPath(i+1,j,M);
	if(M[i][j]+1 == M[i-1][j])
		return dp[i][j] = 1 + LongestPath(i-1,j,M);
	if(M[i][j]+1 == M[i][j-1])
		return dp[i][j] = 1 + LongestPath(i,j-1,M);
	return dp[i][j] = 1;
}

int main(void)
{
	int M[n][n] = {{1,2,9},
				   {5,3,8},
				   {4,6,7}};
	int result = 1;
	memset(dp,-1,sizeof(dp));
	for(int i=0;i<n;i++)
	{
		for(int j=0;j<n;j++)
		{
			if(dp[i][j] == -1)
				LongestPath(i,j,M);
			result = max(result,dp[i][j]);
		}
	}
	cout<<result<<endl;
	return 0;
}