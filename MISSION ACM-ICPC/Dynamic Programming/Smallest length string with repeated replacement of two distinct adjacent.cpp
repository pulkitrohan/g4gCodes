#include<bits/stdc++.h>
using namespace std;

int stringReduction(string S) {
	int N = S.length();
	int count[3] = {0};
	for(int i=0;i<N;i++)
		count[S[i]-'a']++;
	if(count[0] == N || count[1] == N || count[2] == N)
		return N;
	if(count[0]%2 == count[1]%2  && count[1]%2 == count[2]%2)
		return 2;
	return 1;
}


int main(void)
{
	string S = "abcbbaacb";
	cout<<stringReduction(S)<<endl;
	return 0;
}