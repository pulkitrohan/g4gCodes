#include<bits/stdc++.h>
using namespace std;

int maximumDifferenceSum(int *A,int N) {
	int dp[N][2];
	memset(dp, 0, sizeof(dp));
	for(int i=0;i<N-1;i++) {
		dp[i+1][0] = max(dp[i][0], dp[i][1] + abs(1-A[i]));
		dp[i+1][1] = max(dp[i][0] + abs(A[i+1] - 1), dp[i][1] + abs(A[i+1]-A[i]));
	}
	return max(dp[N-1][0], dp[N-1][1]);

}

int main(void) {
	int A[] = {3,2,1,4,5};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<maximumDifferenceSum(A, N)<<endl;
	return 0;
}