
/*
    Given a binary matrix, find out the maximum size square sub-matrix with all 1s.

*/
#include<stdio.h>
int min(int A,int B)
{
    return (A > B) ? B : A;
}
int min3(int A,int B,int C)
{
    return min(min(A,B),C);
}
int main(void)
{
    int r,c,i,j,a[100][100],s[100][100],max,max_i,max_j;
    printf("Enter the rows and colums of Matrix : ");
    scanf("%d %d",&r,&c);
    printf("Enter the Matrix : ");
    for(i=0;i<r;i++)
        for(j=0;j<c;j++)
        scanf("%d",&a[i][j]);
    for(i=0;i<r;i++)
        s[i][0] = a[i][0];
    for(j=0;j<c;j++)
        s[0][j] = a[0][j];
    for(i=1;i<r;i++)
        for(j=1;j<c;j++)
            if(a[i][j] == 1)
                s[i][j] = min3(s[i-1][j],s[i][j-1],s[i-1][j-1]) + 1;
            else
                s[i][j] = 0;
    max = a[0][0],max_i = 0,max_j = 0;
    for(i=0;i<r;i++)
        for(j=0;j<c;j++)
            if(max < s[i][j])
    {
        max = s[i][j];
        max_i = i;
        max_j = j;
    }
    for(i=max_i;i > max_i - max;i--)
    {
        for(j=max_j;j>max_j-max;j--)
        {
            printf("%d ",a[i][j]);
        }
        printf("\n");
    }
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
            printf("%d ",s[i][j]);
        printf("\n");
    }

        return 0;
}
