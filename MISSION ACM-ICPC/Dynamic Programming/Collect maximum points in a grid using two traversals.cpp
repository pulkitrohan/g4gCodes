#include<bits/stdc++.h>
using namespace std;
#define R 5
#define C 4

int dp[R][C][C];

int getMaxCollection(int M[R][C],int x,int y1,int y2)
{
	if(x < 0 || x > R || y1 < 0 || y1 >= C || y2 < 0 || y2 >= C)
		return INT_MIN;
	if(x == R-1 && y1 == 0 && y2 == C-1)
		return M[x][y1] + M[x][y2];
	if(x == R-1)
		return INT_MIN;
	if(dp[x][y1][y2] != -1)
		return dp[x][y1][y2];
	int ans = INT_MIN;
	int temp = (y1 == y2) ? M[x][y1] : M[x][y1] + M[x][y2];
	ans = max(ans,temp + getMaxCollection(M,x+1,y1,y2-1));
	ans = max(ans,temp + getMaxCollection(M,x+1,y1,y2));
	ans = max(ans,temp + getMaxCollection(M,x+1,y1,y2+1));
	ans = max(ans,temp + getMaxCollection(M,x+1,y1-1,y2-1));
	ans = max(ans,temp + getMaxCollection(M,x+1,y1-1,y2));
	ans = max(ans,temp + getMaxCollection(M,x+1,y1-1,y2+1));
	ans = max(ans,temp + getMaxCollection(M,x+1,y1+1,y2-1));
	ans = max(ans,temp + getMaxCollection(M,x+1,y1+1,y2));
	ans = max(ans,temp + getMaxCollection(M,x+1,y1+1,y2+1));
	return (dp[x][y1][y2] = ans);
}

int main(void)
{
	int M[R][C] = {{3,6,8,2},
				   {5,2,4,3},
				   {1,1,20,10},
				   {1,1,20,10},
				   {1,1,20,10}};
	memset(dp,-1,sizeof(dp));
	cout<<getMaxCollection(M,0,0,C-1);
	return 0;
}