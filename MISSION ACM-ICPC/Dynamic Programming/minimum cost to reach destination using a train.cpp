#include<bits/stdc++.h>
using namespace std;

#define INF 1000
#define N 4

int MinCost(int M[][N])
{
	int dp[N];
	for(int i=0;i<N;i++)
		dp[i] = INF;
	dp[0] = 0;
	for(int i=0;i<N;i++)
		for(int j=i+1;j<N;j++)
			dp[j] = min(dp[j],dp[i]+M[i][j]);
	return dp[N-1];
}


int main(void)
{
	int cost[N][N] = {{0,15,80,90},{INF,0,40,50},{INF,INF,0,70},{INF,INF,INF,0}};
	cout<<MinCost(cost);
	return 0;
}