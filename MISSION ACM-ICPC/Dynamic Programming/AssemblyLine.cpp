#include<bits/stdc++.h>
using namespace std;
#define NUM_STATION 4

int CarAssembly(int A[][NUM_STATION],int T[][NUM_STATION],int *E,int *X)
{
	int T1[NUM_STATION],T2[NUM_STATION];
	T1[0] = E[0] + A[0][0];
	T2[0] = E[1] + A[1][0];
	
	for(int i=1;i<NUM_STATION;i++)
	{
		T1[i] = min(T1[i-1] + A[0][i],T2[i-1] + A[0][i] + T[1][i]);
		T2[i] = min(T2[i-1] + A[1][i],T1[i-1] + A[0][i] + T[0][i]);
	}
	return min(T1[NUM_STATION-1]+X[0],T2[NUM_STATION-1]+X[1]);
}

int main(void)
{
	int A[][NUM_STATION] = {{4,5,3,2},{2,10,1,4}};
	int T[][NUM_STATION] = {{0,7,4,5},{0,9,2,8}};
	int E[] = {10,12};
	int X[] = {18,7};
	cout<<CarAssembly(A,T,E,X);
	return 0;
}