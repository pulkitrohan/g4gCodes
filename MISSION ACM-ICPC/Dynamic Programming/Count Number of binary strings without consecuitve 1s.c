#include<stdio.h>

int CountStrings(int N)
{
    int A[N],B[N],i;
    A[0] = B[0] = 1;
    for(i=1;i<N;i++)
    {
        A[i] = A[i-1]+B[i-1];
        B[i] = A[i-1];
    }
    return A[N-1] + B[N-1];
}

int main(void)
{
    int N = 2;
    printf("%d",CountStrings(N));
    return 0;
}
