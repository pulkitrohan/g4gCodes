#include<bits/stdc++.h>
using namespace std;

int MinPalPartition(string A)
{
	int N = A.length();
	bool dp[N][N];
	int maxlength = 1,start = 0;
	memset(dp,false,sizeof(dp));
	for(int i=N-1;i>=0;i--)
	{
		for(int j=i;j<N;j++)
		{
			
			if(((j == i+1) || dp[i+1][j-1]) && A[i] == A[j])
				dp[i][j] = true;
		}
	}
	vector<int> C(N,0);
	for(int i=0;i<N;i++)
	{
		if(!dp[0][i])
		{
			C[i] = INT_MAX;
			for(int j=0;j<i;j++)
				if(dp[j+1][i] && C[i] > 1 + C[j])
					C[i] = 1 + C[j];
		}
	}
	return C[N-1];
}

int main(void)
{
	char S[] = "ababbbabbababa";
	printf("%d\n",MinPalPartition(S));
	return 0;
}
