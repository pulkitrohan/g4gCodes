#include<bits/stdc++.h>
using namespace std;
int min(int A,int B)
{
	return (A < B) ? A : B;
}

int sum(int *A,int start,int end)
{
    int total = 0,i;
    for(i=start;i<=end;i++)
        total += A[i];
    return total;
}

int dp[10000][10000];

int optCostRecur(int freq[], int start, int end) {
	if(end > start)
		return 0;
	if(end == start)
		return freq[start];
	if(dp[start][end] != -1)
		return dp[start][end];
	int fsum = sum(freq, start, end);
	int min = INT_MAX;
	for(int i=start;i<=end;i++) {
		int cost = optCostRecur(freq, start, i-1) + optCostRecur(freq, i+1, end);
		if(cost < min)
			min = cost;
	}
	return dp[start][end] = min + fsum;
}

int OptimalSearchTree(int *A,int *B,int N)
{
	int i,j,k,L;
	int DP[N+1][N+1];
	for(i=0;i<N;i++)
		DP[i][i] = B[i];
	for(L=2;L<=N;L++)
	{
		for(i=0;i<N-L+1;i++)
		{
			j = i+L-1;
			DP[i][j] = INT_MAX;
			for(k=i;k<=j;k++)
			{
				int c;
				c = (k > i) ? DP[i][k-1] : 0;
				c += (k < j) ? DP[k+1][j] : 0;
				c += sum(B,i,j);
                DP[i][j] = min(DP[i][j],c);
			}
		}
	}

	return DP[0][N-1];
}


int main(void)
{
	int A[] = {10,12,20};
	int B[] = {34,8,50};
	int N = sizeof(A)/sizeof(A[0]);
	memset(dp, -1, sizeof(dp));
	printf("Ans : %d\n",optCostRecur(A,0, N-1));
	// printf("Ans : %d\n",OptimalSearchTree(A,B,N));
	return 0;
}
