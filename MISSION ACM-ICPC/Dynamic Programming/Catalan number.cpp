#include<bits/stdc++.h>
using namespace std;

int binomial(int N,int R)
{
	R = min(R,N-R);
	int ans = 1;
	for(int i=0;i<R;i++)
	{
		ans *= (N-i);
		ans /= (i+1);
	}
	return ans;
}

int catalan(int N)
{
	return binomial(2*N,N)/(N+1);
}

int main(void)
{
	cout<<catalan(4);
	return 0;
}