#include<stdio.h>

int HasPartition(int A[],int N,int sum)
{
	int part[sum+1][N+1];
	for(i=0;i<=N;i++)
		part[0][i] = 1;
	for(i=1;i<=sum/2;i++)
		part[i][0] = 0;

	for(i=1;i<=sum;i++)
	{
		for(j=1;j<=N;j++)
		{
			if(i >= A[j-1])
				part[i][j] = part[i][j-1] || part[i-A[j-1]][j-1];
		}
	}
	return part[sum][N];
}

int main(void)
{
	int A[] = {3,34,4,12,5,2};
	int sum = 9;
	int N = sizeof(A)/sizeof(A[0]);
	if(HasPartition(A,N,sum))
		printf("YES\n");
	else
		printf("NO\n");
	return 0;
}
