#include<bits/stdc++.h>
using namespace std;

int countStringsUtil(int dp[][2][3],int N,int count,int b,int c)
{
	if(count >= N)
		return 1;
	if(dp[count][b][c] != -1)
		return dp[count][b][c];
	int ans = 0;
	ans += countStringsUtil(dp,N,count+1,b,c);
	if(b == 0)
		ans += countStringsUtil(dp,N,count+1,b+1,c);
	if(c < 2)
		ans += countStringsUtil(dp,N,count+1,b,c+1);
	dp[count][b][c] = ans;
	return dp[count][b][c];
}


int countStrings(int N)
{
	int dp[N+1][2][3];
	memset(dp,-1,sizeof(dp));
	return countStringsUtil(dp,N,0,0,0);
}


int main(void)
{
	int N = 3;
	cout<<countStrings(N);
	return 0;
}