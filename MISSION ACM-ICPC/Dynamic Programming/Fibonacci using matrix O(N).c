#include<stdio.h>

int fib(int N)
{
    int F[2][2] = {{1,1},{1,0}};
    if(N == 0)
        return 0;
    power(F,N-1);
    return F[0][0];
}

void power(int F[2][2],int N)
{
    int i;
    int M[2][2] = {{1,1},{1,0}};
    for(i=2;i<=N;i++)
        Multiply(F,M);
}

void Multiply(int F[2][2],int M[2][2])
{
    int A = F[0][0] * M[0][0] + F[0][1] * M[1][0];
    int B = F[0][0] * M[0][1] + F[0][1] * M[1][1];
    int C = F[1][0] * M[0][0] + F[1][1] * M[1][0];
    int D = F[1][0] * M[0][1] + F[1][1] * M[1][1];

    F[0][0] = A;
    F[0][1] = B;
    F[1][0] = C;
    F[1][1] = D;
}

int main(void)
{
    int N;
    printf("Enter the number : ");
    scanf("%d",&N);
    printf("%d",fib(N));
    return 0;
}
