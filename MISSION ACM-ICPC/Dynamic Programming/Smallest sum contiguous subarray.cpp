#include <bits/stdc++.h>
using namespace std;

int smallestSumSubarray(int *A,int N) {
	long min_ending_here = INT_MAX;
	long min_so_far = INT_MAX;
	for(int i=0;i<N;i++){
		min_ending_here = min(min_ending_here + A[i],(long)A[i]);
		min_so_far = min(min_so_far,min_ending_here);
	}
	return min_so_far;
}

int main(void) {
	int A[] = { 3, -4, 2, -3, -1, 7, -5};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<smallestSumSubarray(A, N);
	return 0;
}