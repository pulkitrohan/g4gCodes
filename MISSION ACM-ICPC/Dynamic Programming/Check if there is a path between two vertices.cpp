#include<cstdio>
#include<vector>
#include<cstdlib>

using namespace std;

struct AL
{
    int data;
    struct AL *next;
};


void addEdge(vector<struct AL *> &G,int start,int end)
{
    struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = end;
    temp->next = G[start];
    G[start] = temp;
}
int V[10] = {0};
int IsReachable(vector<struct AL *> &G,int start,int end)
{
    if(start == end)
        return 1;
    V[start] = 1;
    struct AL *temp = G[start];
    while(temp)
    {
        if(!V[temp->data])
            if(IsReachable(G,temp->data,end))
                return 1;
        temp = temp->next;
    }
    return 0;
}

int main(void)
{
    vector<struct AL *> Graph;
    for(int i = 0;i<4;i++)
        Graph.push_back(NULL);
    addEdge(Graph,0,1);
    addEdge(Graph,0,2);
    addEdge(Graph,1,2);
    addEdge(Graph,2,0);
    addEdge(Graph,2,3);
    addEdge(Graph,3,3);
    int u = 1,v = 3;
    if(IsReachable(Graph,u,v))
        printf("Yes,there is a path from %d to %d\n",u,v);
    else
        printf("No,there is no path from %d to %d\n",u,v);
    return 0;
}
