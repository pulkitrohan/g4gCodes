#include<bits/stdc++.h>
using namespace std;
#define MOD 1000000007
vector<int> capList[101];
int allmask;
int dp[1025][101];

long long countWaysUtil(int mask,int i)
{
	if(mask == allmask)
		return 1;
	if(i > 100)
		return 0;
	if(dp[mask][i] != -1)
		return dp[mask][i];
	long long ways = countWaysUtil(mask,i+1);
	int size = capList[i].size();
	for(int j=0;j<size;j++)
	{
		if(mask & (1 << capList[i][j]))
			continue;
		else
			ways += countWaysUtil(mask | (1 << capList[i][j]),i+1);
		ways %= MOD;
	}
	dp[mask][i] = ways;
	return dp[mask][i];
}

void countWays(int N)
{
	string str;
	int temp;
	getline(cin,str);
	for(int i=0;i<N;i++)
	{
		getline(cin,str);
		stringstream ss(str);
		while(ss >> temp)
			capList[temp].push_back(i);
	}	
	allmask = (1 << N) - 1;
	memset(dp,-1,sizeof(dp));
	cout<<countWaysUtil(0,1)<<endl;
	
}


int main(void)
{
	int N;
	cin>>N;
	countWays(N);
	return 0;
}