#include<bits/stdc++.h>
using namespace std;

int computeLIS(int Arr[],int start,int end) {
	vector<int> dp(end-start, 1);
	for(int i=start+1;i<end;i++)
		for(int j=start;j<i;j++)
			if(Arr[i] > Arr[j] && dp[i] < dp[j] + 1)
				dp[i] = 1 + dp[j];
	int ans = INT_MIN;
	for(int i=start;i<end;i++)
		ans = max(ans, dp[i]);
	return ans;
}

int LICS(int A[],int N) {
	int circBuff[2*N];
	for(int i=0;i<N;i++)
		circBuff[i] = A[i];
	for(int i=N;i<2*N;i++)
		circBuff[i] = A[i-N];
	int ans = INT_MIN;
	for(int i=0;i<N;i++)
		ans = max(ans, computeLIS(circBuff, i, i+N));
	return ans;
}

int main(void) {
	int A[] = {1,4,6,2,3};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<LICS(A, N);
	return 0;
}