/*
    Maximum Value Contiguous Subsequence
    Given a sequence of n real numbers A(1) ... A(n), determine a contiguous subsequence A(i) ... A(j)
    for which the sum of elements in the subsequence is maximized.
        Handles all -ve weights as well.
*/

#include<stdio.h>

int max(int A,int B)
{
    return (A > B) ? A : B;
}

int main(void)
{
    int N,i;
    scanf("%d",&N);
    int S[2*N],A[N+1];
    for(i=1;i<=N;i++)
        scanf("%d",&A[i]);
    S[0] = 0;
    for(i=1;i<=N;i++)
        S[i] = max(S[i-1] + A[i],S[i-1]);
        int m = -1;
    for(i=1;i<=N;i++)
    {
        if(S[i] > m)
            m = S[i];
    }
    printf("%d",m);
    return 0;
}
