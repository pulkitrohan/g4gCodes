/*
    Ugly numbers are numbers whose only prime factors are 2, 3 or 5. The sequence
    1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 15, …
    shows the first 11 ugly numbers. By convention, 1 is included.
    Write a program to find and print the 150′th ugly number.
*/
#include<stdio.h>
#define min(a,b) ((a > b) ? b : a)
int main(void)
{
    unsigned int n,i_2,i_3,i_5,ugly[200],i,i2=0,i3=0,i5=0,ugly_no;
    printf("Enter how many ugly numbers you want : ");
    scanf("%u",&n);
    i_2 = 2;
    i_3 = 3;
    i_5 = 5;
    ugly[0] = 1;
    for(i=1;i<n;i++)
    {
        ugly_no = min(min(i_2,i_3),i_5);
        ugly[i] = ugly_no;
        if(ugly_no == i_2)
           {
               i2++;
               i_2 = ugly[i2]*2;
           }
       if(ugly_no == i_3)
            {
                i3++;
                i_3 = ugly[i3]*3;
            }
        if(ugly_no == i_5)
            {
                i5++;
                i_5 = ugly[i5]*5;
            }
    }
    for(i=0;i<n;i++)
        printf("%d ",ugly[i]);
    printf("\n%dth Ugly Number is : %d",n,ugly[n-1]);
    return 0;
}
