#include<bits/stdc++.h>
using namespace std;

int maxSumBitonicSubsequence(int *A, int N) {
	if(N < 1) {
		return 0;
	}
	int inc[N+1], dec[N+1];

	for(int i=0;i<N;i++) {
		inc[i] = A[i];
		dec[i] = A[i];
	}

	for(int i=1;i<N;i++) {
		for(int j=0;j<i;j++)
			if(A[i] > A[j] && (inc[j] + A[i]) > inc[i])
				inc[i] = inc[j] + A[i];
	}

	for(int i=N-2;i>=0;i--) {
		for(int j=N-1;j>i;j--) 
			if(A[i] > A[j] && (dec[j] + A[i]) > dec[i])
				dec[i] = dec[j] + A[i];
	}
		
	int ans = INT_MIN;
	for(int i=0;i<N;i++)
		ans = max(ans, inc[i] + dec[i] - A[i]);
	return ans;
}

int main(void) {
	int A[] = {1 , 15 ,51 ,45 ,33 ,100 ,12 ,18 ,9};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<maxSumBitonicSubsequence(A, N);
	return 0;
}