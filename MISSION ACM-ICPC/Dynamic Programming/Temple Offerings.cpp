#include<bits/stdc++.h>
using namespace std;

int offeringNumbers(int A[], int N) {
	vector<pair<int, int> > chainSize;
	for(int i=0;i<N;i++)
		chainSize.push_back(make_pair(-1, -1));
	chainSize[0].first = 1;
	chainSize[N-1].second = 1;
	for(int i=1;i<N;i++) {
		if(A[i] > A[i-1])
			chainSize[i].first = chainSize[i-1].first + 1;
		else
			chainSize[i].first = 1;
	}
	for(int i=N-2;i>=0;i--) {
		if(A[i] > A[i+1])
			chainSize[i].second = chainSize[i+1].second + 1;
		else
			chainSize[i].second = 1;
	}
	int ans = 0;
	for(int i=0;i<N;i++)
		ans += max(chainSize[i].first, chainSize[i].second);
	return ans;
}

int main(void) {
	int A[] = {1, 4, 3, 6, 2, 1};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<offeringNumbers(A, N)<<endl;
}