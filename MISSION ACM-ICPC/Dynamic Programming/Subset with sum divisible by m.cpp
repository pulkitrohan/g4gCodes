#include<bits/stdc++.h>
using namespace std;

int modularSum(int A[], int N, int M) {
	if(N > M)
		return true;
	bool dp[M];
	memset(dp, false, M);
	for(int i=0;i<N;i++) {
		if(dp[0])
			return true;
		bool temp[M];
		memset(temp, false, M);
		for(int j=0;j<M;j++) {
			if(dp[j]) {
				if(dp[(j+A[i])%M] == false)
					temp[(j+A[i])%M] = true;
			}
		}
		for(int j=0;j<M;j++)
			if(temp[j])
				dp[j] = true;
		dp[A[i]%M] = true;
	}
	return dp[0];
}

int main(void) {
	int A[] = {3,1,7,5};
	int N = sizeof(A)/sizeof(A[0]);
    int M = 6;
    cout<<modularSum(A, N, M) ? 'YES' : 'NO';
    return 0;
}