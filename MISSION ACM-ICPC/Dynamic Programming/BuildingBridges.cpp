#include<bits/stdc++.h>
using namespace std;

struct Pair
{
	int x,y;
};

bool comp(struct Pair A,struct Pair B)
{
	return B.x > A.x;
}

int main(void)
{
	struct Pair P[] = {{8,1},{1,2},{4,3},{3,4},{5,5},{2,6},{6,7},{7,8}};
	int N = sizeof(P)/sizeof(P[0]);
	sort(P,P+N,comp);
	int LIS[N+1];
	for(int i=0;i<N;i++)
		LIS[i] = 1;
	for(int i=1;i<N;i++)
		for(int j=0;j<i;j++)
			if(P[i].y > P[j].y && LIS[i] < LIS[j]+1)
				LIS[i] = LIS[j]+1;
	int max = 1;
	for(int i=0;i<N;i++)
		if(LIS[i] > max)
			max = LIS[i];
	printf("%d\n",max);
	for(int i=0;i<N;i++)
		cout<<LIS[i]<<" ";
	return 0;
}
