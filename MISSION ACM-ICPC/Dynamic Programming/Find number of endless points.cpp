#include<bits/stdc++.h>
using namespace std;

const int MAX = 100;

int countEndless(bool input[][MAX], int N) {
	int row[N][N], col[N][N];

	for(int j=0;j<N;j++) {
		bool isEndless = 1;
		for(int i=N-1;i>=0;i--) {
			if(input[i][j] == 0)
				isEndless = 0;
			col[i][j] = isEndless;
		}
	}

	for(int i=0;i<N;i++) {
		bool isEndless = 1;
		for(int j=N-1;j>=0;j--) {
			if(input[i][j] == 0)
				isEndless = 0;
			row[i][j] = isEndless;
		}
	}

	int ans = 0;
	for(int i=0;i<N;i++)
		for(int j=1;j<N;j++)
			if(row[i][j] && col[i][j])
				ans++;
	return ans;
}

int main(void) {
	bool input[][MAX] = { {1, 0, 1, 1},
                         {0, 1, 1, 1},
                         {1, 1, 1, 1},
                         {0, 1, 1, 0}};

    int N = 4;
    cout<<countEndless(input, N);
    return 0;
}