#include<bits/stdc++.h>
using namespace std;
#define ROW 4
#define COL 5

int kadane(int *A,int &start,int &finish,int N)
{
	int sum = 0,maxSum = INT_MIN,local_start = 0;
	finish = -1;
	for(int i=0;i<N;i++)
	{
		sum += A[i];
		if(sum < 0)
		{
			sum = 0;
			local_start = i+1;
		}
		else if(sum > maxSum)
		{
			maxSum = sum;
			start = local_start;
			finish = i;
		}
	}
	if(finish != -1)
		return maxSum;
	maxSum = A[0];
	start = finish = 0;
	for(int i=1;i<N;i++)
	{
		if(A[i] > maxSum)
		{
			maxSum = A[i];
			start = finish = i;
		}
	}
	return maxSum;
}

void FindMaxSum(int M[][COL])
{
	int maxsum = INT_MIN,finalLeft,finalRight,finalTop,finalBottom;
	int left,right,i;
	int temp[ROW],sum,start,finish;
	for(left = 0;left<COL;left++)
	{
		memset(temp,0,sizeof(temp));
		for(right=left;right<COL;right++)
		{
			for(int i=0;i<ROW;i++)
				temp[i] += M[i][right];
			
			sum = kadane(temp,start,finish,ROW);
			if(sum > maxsum)
			{
				maxsum = sum;
				finalLeft = left;
				finalRight = right;
				finalTop = start;
				finalBottom = finish;
			}
		}
	}
	printf("(Top,Left) (%d,%d)\n",finalTop,finalLeft);
	printf("(Bottom,Right) (%d,%d)\n",finalLeft,finalRight);
	printf("Max sum : %d\n",maxsum);
}


int main(void)
{
	int M[ROW][COL] = {{1,2,-1,-4,-20},{-8,-3,4,2,1},{3,8,10,1,3},{-4,-1,1,7,-6}};
	FindMaxSum(M);
	return 0;
}