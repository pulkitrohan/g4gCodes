#include <bits/stdc++.h>
using namespace std;

int minRemove(int *A,int N) {
	vector<int> dp(N, 1);
	int maxLen = 1;
	for(int i=1;i<N;i++) {
		for(int j=0;j<i;j++) {
			if(A[i] > A[j]) {
				dp[i] = max(dp[i], dp[j] + 1);
			}
		}
		maxLen = max(maxLen, dp[i]);
	}
	return (N - maxLen);
}

int main(void) {
	int A[] = { 1, 2, 6, 5, 4 };
	int N = sizeof(A)/sizeof(A[0]);
	cout<<minRemove(A, N);
	return 0;
}
