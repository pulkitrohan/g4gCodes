#include<bits/stdc++.h>
using namespace std;

int NoWays(int N)
{
	if(N == 1)
		return 4;
	int countS = 1,countB = 1,prevCountB,prevCountS;
	
	for(int i=2;i<=N;i++)
	{
		prevCountB = countB;
		prevCountS = countS;
		
		countS = prevCountB + prevCountS;
		countB = prevCountS;
	}
	int ans = countS + countB;
	return ans*ans;
}

int main(void)
{
	int N = 3;
	cout<<NoWays(N);
	return 0;
}