#include<stdio.h>
#define max(A,B) ((A > B) ? A : B)

int LCSubString(char A[],char B[],int M,int N)
{
	int DP[M+1][N+1];
	int i,j,result = 0;

	for(i=0;i<=M;i++)
	{
		for(j=0;j<=N;j++)
		{
			if(i == 0 || j == 0)
				DP[i][j] = 0;

			else if(A[i-1] == B[j-1])
			{
				DP[i][j] = 1 + DP[i-1][j-1];
				result = max(result,DP[i][j]);
			}

			else
				DP[i][j] = 0;
		}
	}
	return result;
}

int main(void)
{
	char X[] = "OldSite:GeeksforGeeks.org";
	char Y[] = "NewSite:GeeksQuiz.com";
	int M = strlen(X);
	int N = strlen(Y);
	printf("%d\n",LCSubString(X,Y,M,N));
	return 0;
}
