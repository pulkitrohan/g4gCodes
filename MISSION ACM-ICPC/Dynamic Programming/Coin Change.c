/*
M = 3 N = 4
1 2 3
i = 0
j = 1
table[1] = table[1] + table[1-1] = 1
i = 0
j = 2
table[2] = table[2] + table[2-1] = 1
i = 0
j = 3
table[3] = table[3] + table[3-1] = 1
i = 0
j = 4
table[4] = table[4] + table[4-1] = 1
i = 1
j = 2
table[2] = table[2] + table[2-2] = 1 + 1 = 2
i = 1
j = 3
table[3] = table[3] + table[3-2] = 1 + 1 = 2
i = 1
j = 4
table[4] = table[4] + table[4-2] = 1 + 2 = 3
i = 2
j = 3
table[3] = table[3] + table[3-3] = 2 + 1 = 3
i = 2
j= 4
table[4] = table[4] + table[4-3] = 3 + 1 = 4



*/


#include<stdio.h>

int CoinChange(int *A,int M,int N)
{
    int table[N+1],i,j;
    for(i=0;i<=N;i++)
        table[i] = 0;
    table[0] = 1;
    for(i=0;i<M;i++)
    {
        for(j=A[i];j<=N;j++)
            table[j] += table[j-A[i]];
    }
    return table[N];
}

int main(void)
{
    int A[] = {1,2,3};
    int M = sizeof(A)/sizeof(A[0]);
    int N = 4;
    printf("Total Possible Coin Change : %d\n",CoinChange(A,M,N));
    return 0;
}
