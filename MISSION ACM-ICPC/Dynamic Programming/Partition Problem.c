#include<stdio.h>

int HasPartition(int A[],int N)
{
	int sum = 0,i,j;
	for(i=0;i<N;i++)
		sum += A[i];
	if(sum % 2)
		return 0;
	int part[sum/2+1][N+1];
	for(i=0;i<=sum/2;i++)
	{
		for(j=0;j<=N;j++)
		{
			if(i == 0)
				part[i][j] = 1;
			else if(j == 0)
				part[i][j] = 0;
			else if(i >= A[j-1])
				part[i][j] = part[i][j-1] || part[i-A[j-1]][j-1];
			else
				part[i][j] = part[i][j-1];
		}
	}
	return part[sum/2][N];
}

int main(void)
{
	int A[] = {3,1,1,2,2,1};
	int N = sizeof(A)/sizeof(A[0]);
	if(HasPartition(A,N))
		printf("YES\n");
	else
		printf("NO\n");
	return 0;
}
