/*
KnapSack problem is a popular dynamic programming problem.
In this we are given two arrays.One containing values and other
containing their respective weights.Suppose the weight of the knapsack
is W.So we have to fill the Knapsack in such a way that value is maximum
and weight of these values is less or equal to W.
Suppose their are N elemenets.
Starting from last value if its weight is greater than W then we reject it
and check for N-1 elements.
else choose the max of value when this element is included and when this element
    is not included and recursively check for N-1 elements.

*/

#include<stdio.h>
#define Max(a,b) ((a > b) ? a : b)

int KnapSack(int W,int wt[],int val[],int N)
{
    int i,w,K[N+1][W+1] ;
    for(i=0;i<=N;i++)
    {
        for(w = 0;w <= W;w++)
        {
            if(i == 0 || w == 0)
                K[i][w] = 0;
            else if(wt[i-1] <= W)   //Weight of item is less than Knapsack Weight
                K[i][w] = Max((val[i-1] + K[i-1][w-wt[i-1]]),K[i-1][w]);    //Max of item included and when not included.
            else
                K[i][w] = K[i-1][w];    //Item not included
        }
    }
    for(i=0;i<=N;i++)
    {
        for(w=0;w<=W;w++)
            printf("%d ",K[i][w]);
        printf("\n\n");
    }
    return K[N][W];
}

int main(void)
{
    int val[] = {60,100,120};
    int wt[] = {10,20,30};
    int W = 50;
    int N = sizeof(val)/sizeof(val[0]);
    printf("%d ",KnapSack(W,wt,val,N));
    return 0;
}
