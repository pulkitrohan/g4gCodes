#include<bits/stdc++.h>
using namespace std;

int getMinSteps(int N, vector<int> V)
{
	if(N == 1)
		return 0;
	if(V[N] != -1)
		return V[N];
	int res = getMinSteps(N-1, V);
	if(N%2 == 0)
		res = min(res, getMinSteps(N/2, V));
	if(N%3 == 0)
		res = min(res, getMinSteps(N/3, V));
	V[N] = 1 + res;
	return V[N];
}

int main(void)
{
	int N = 10;
	vector<int> V(N+1, -1);
	cout<<getMinSteps(N, V);
	return 0;
}