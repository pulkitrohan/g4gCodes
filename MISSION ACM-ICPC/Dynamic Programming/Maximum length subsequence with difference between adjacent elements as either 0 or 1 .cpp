#include<bits/stdc++.h>
using namespace std;

int maxLenSub(int *A,int N) {
	vector<int> dp(N,1);
	for(int i=1;i<N;i++) {
		for(int j=0;j<i;j++) {
			if(abs(A[i]-A[j]) <= 1 && (dp[j] + 1) > dp[i])
				dp[i] = dp[j] + 1;
		}
	}
	int max = INT_MIN;
	for(int i=0;i<N;i++)
		if(dp[i] > max)
			max = dp[i];
	return max;
}

int main(void) {
	int A[] = {2, 5, 6, 3, 7, 6, 5, 8};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<maxLenSub(A, N);
}