#include<stdio.h>

int CountPaths(int M,int N)
{
    int Count[M][N],i,j;
    for(i=0;i<M;i++)
        Count[i][0] = 1;
    for(i=0;i<N;i++)
        Count[0][i] = 1;
    for(i=1;i<M;i++)
    {
        for(j=1;j<N;j++)
            Count[i][j] = Count[i-1][j] + Count[i][j-1];
    }
    return Count[M-1][N-1];
}

int main(void)
{
    int M = 3,N = 3;
    printf("%d ",CountPaths(M,N));
    return 0;
}
