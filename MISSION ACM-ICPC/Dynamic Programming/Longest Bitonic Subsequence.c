#include<stdio.h>

int BitonicSeq(int *A,int N)
{
    int LIS[N+1],LDS[N+1],i,j;
    for(i=0;i<N;i++)
    {
        LIS[i] = 1;
        LDS[i] = 1;
    }
    for(i=1;i<N;i++)
    {
        for(j=0;j<i;j++)
        {
            if(A[i] > A[j] && LIS[i] < LIS[j] + 1)
                LIS[i] = LIS[j] + 1;
        }
    }
    for(i=N-2;i>=0;i--)
    {
        for(j=N-1;j>i;j--)
        {
            if(A[i] > A[j] && LDS[i] < LDS[j] + 1)
                LDS[i] = LDS[j] + 1;
        }
    }
    int max = LIS[0] + LDS[0] -1;
    for(i=0;i<N;i++)
    {
        if(max < (LIS[i] + LDS[i] -1))
            max = LIS[i] + LDS[i] -1;
    }
    return max;
}

int main(void)
{
    int A[] = {1,11,2,10,4,5,2,1};
    int N = sizeof(A)/sizeof(A[0]);
    printf("Longest Bitonic Subsequence : %d\n",BitonicSeq(A,N));
    return 0;
}
