//http://www.programcreek.com/2014/05/leetcode-paint-house-java/

#include<bits/stdc++.h>
using namespace std;

int minCost(int cost[][3],int N)
{
	for(int i=1;i<N;i++)
	{
		cost[i][0] += min(cost[i-1][1],cost[i-1][2]);
		cost[i][1] += min(cost[i-1][0],cost[i-1][2]);
		cost[i][2] += min(cost[i-1][0],cost[i-1][1]);
	}
	return min(cost[N-1][0],min(cost[N-1][1],cost[N-1][2]));
}

int main(void)
{
	int cost[][3] = {{1,2,3},{4,5,6},{7,8,9}};
	int N = 3;
	cout<<minCost(cost,N);
	return 0;
}