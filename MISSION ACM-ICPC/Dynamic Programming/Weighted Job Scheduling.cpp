#include<bits/stdc++.h>
using namespace std;

struct Job
{
	int start,finish,profit;
};

bool cmp(Job A,Job B)
{
	return (A.finish < B.finish);
}

int LatestNonConflict(Job A[],int i)
{
	for(int j=i-1;j>=0;j--)
	{
		if(A[j].finish <= A[i].start)
			return j;
	}
	return -1;
}

int FindMaxProfit(Job A[],int N)
{
	sort(A,A+N,cmp);
	int dp[N+1];
	dp[0] = A[0].profit;
	for(int i=1;i<N;i++)
	{
		dp[i] = A[i].profit;
		int index = LatestNonConflict(A,i);
		if(index != -1)
			dp[i] += dp[index];
		dp[i] = max(dp[i],dp[i-1]);
	}
	return dp[N-1];
}

int main(void)
{
	 Job A[] = {{3,10,20},{1,2,50},{6,19,100},{2,100,200}};
	 int N = sizeof(A)/sizeof(A[0]);
	 cout<<FindMaxProfit(A,N);
	 return 0;
}
	 