#include<bits/stdc++.h>
using namespace std;

int FindMin(int *A,int N)
{
	int sum = 0;
	for(int i=0;i<N;i++)
		sum += A[i];
	bool dp[N+1][sum+1];
	for(int i=0;i<=N;i++)
		dp[i][0] = true;
	for(int i=1;i<=sum;i++)
		dp[0][i] = false;
	for(int i=1;i<=N;i++)
	{
		for(int j=1;j<=sum;j++)
		{
			if(A[i-1] <= j)
				dp[i][j] = dp[i-1][j] | dp[i-1][j-A[i-1]];
			else
				dp[i][j] = dp[i-1][j];
		}
	}
	int diff = INT_MAX;
	for(int i=sum/2;i>=0;i--)
		if(dp[N][i])
		{
			diff = sum - 2*i;
			break;
		}
	return diff;
}

int main(void)
{
	int A[] = {3,1,4,2,2,1};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<FindMin(A,N);
	return 0;
}