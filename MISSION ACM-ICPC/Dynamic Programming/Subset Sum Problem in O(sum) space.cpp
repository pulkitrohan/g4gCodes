#include<bits/stdc++.h>
using namespace std;

bool isSubsetSum(int *A, int N ,int sum) {
	if(sum == 0) {
		return true;
	}
	if(N == 0) {
		return false;
	}
	bool dp[2][sum+1];
	for(int i=0;i<=N;i++) {
		for(int curSum=0;curSum <= sum;curSum++) {
			if(i == 0) {
				dp[i%2][curSum] = false;
			}
			else if(curSum == 0) {
				dp[i%2][curSum] = true;
			}
			else if(curSum < A[i-1]) {
				dp[i%2][curSum] = dp[(i+1)%2][curSum];
			}
			else {
				dp[i%2][curSum] = dp[(i+1)%2][curSum-A[i-1]] ||
								  dp[(i+1)%2][curSum];
			}
		}
	}
	return dp[N%2][sum];
}


int main(void) {
	int A[] = { 6, 2, 5 };
	int N = sizeof(A)/sizeof(A[0]);
	int sum = 7;
	if(isSubsetSum(A, N, sum)) {
		cout<<"YES\n";
	}
	else {
		cout<<"NO\n";
	}
	return 0;
}