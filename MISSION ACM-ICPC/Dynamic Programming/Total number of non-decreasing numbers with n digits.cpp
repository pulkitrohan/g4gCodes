#include<bits/stdc++.h>
using namespace std;

long long CountIncreasing(int r)
{
	int N = 10;
	long long ans = 1;
	for(int i=1;i<=r;i++)
	{
		ans *= (N+i-1);
		ans /= i;
	}
	return ans;
}

int main(void)
{
	int r = 3;
	cout<<CountIncreasing(r);
	return 0;
}