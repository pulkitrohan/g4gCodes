#include<bits/stdc++.h>
using namespace std;

long countWays(int N) {
	long dp[2][N+1];
	dp[0][1] = 1;
	dp[1][1] = 2;
	for(int i=2;i<=N;i++) {
		dp[0][i] = dp[0][i-1] + dp[1][i-1];
		dp[1][i] = dp[0][i-1]*2 + dp[1][i-1];
	}
	return dp[0][N] + dp[1][N];
}

int main(void) {
	int N = 5;
	cout<<countWays(N)<<endl;
}