#include<bits/stdc++.h>
using namespace std;

/*

If you are at breakpoint - b; then till that point you have achieved findoptimal(b) number of A's
Now you have (N-b) chances left. Now you will do ctrl+a and ctrl+c.
So you have findoptimal(b) in your buffer which can be used for copy.
After these two key press you have N-b-2 chances left. 
So number of times you can do ctrl+v is N-b-2
So you will get further (N-b-2)*buffer i.e. (N-b-2)*findoptimal(b) of A's

totoal number of A's =
A's till break point b + A's after breakpoint b
=findoptimal(b)+ (N-b-2)*findoptimal(b)
=(N-b-2+1)findoptimal(b)
=(N-b-1)findoptimal(b)

*/


int FindOptimal(int N)
{
	if(N <= 6)
		return N;
	int DP[N+1];
	memset(DP,0,sizeof(DP));
	for(int i=1;i<=6;i++)
		DP[i] = i;
	for(int i=7;i<=N;i++)
		for(int j=i-3;j>=1;j--)
			DP[i] = max(DP[i],(i-j-1)*DP[j]);
	return DP[N];
}

int main(void)
{
	for(int i=1;i<=20;i++)
		cout<<FindOptimal(i)<<" ";
	return 0;
}