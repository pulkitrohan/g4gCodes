#include <bits/stdc++.h>
using namespace std;

int countTransformation(string A, string B) {
	if(B.size() == 0) {
		return 1;
	}
	int dp[B.size()+1][A.size()+1];
	memset(dp, 0, sizeof(dp));
	for(int i=0;i<B.size();i++){
		for(int j=i;j<A.size();j++) {
			if(i == 0) {
				if(B[i] == A[i] && j == 0) {
					dp[i][j] = 1;
				}
				else if(B[i] == A[j]) {
					dp[i][j] = 1 + dp[i][j-1];
				}
				else {
					dp[i][j] = dp[i][j-1];
				}
			}
			else {
				if(B[i] == A[j]) {
					dp[i][j] = dp[i][j-1] + dp[i-1][j-1];
				}
				else {
					dp[i][j] = dp[i][j-1];
				}
			}
		}
	}
	return dp[B.size()-1][A.size()-1];
}

int main(void) {
	string A = "abcccdf";
	string B = "abccdf";
	cout<<countTransformation(A, B)<<endl;
	return 0;
}