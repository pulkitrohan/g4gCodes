#include<stdio.h>
#define LL long long
LL fib(LL N)
{
    LL F[2][2] = {{1,1},{1,0}};
    if(N == 0)
        return 0;
    power(F,N-1);
    return F[0][0];
}

void power(LL F[2][2],LL N)
{
	if(N == 0 || N == 1)
		return;
	LL M[2][2] = {{1,1},{1,0}};
	power(F,N/2);
	Multiply(F,F);
	if(N%2 != 0)
		Multiply(F,M);
}

void Multiply(LL F[2][2],LL M[2][2])
{
    LL A = F[0][0] * M[0][0] + F[0][1] * M[1][0];
    LL B = F[0][0] * M[0][1] + F[0][1] * M[1][1];
    LL C = F[1][0] * M[0][0] + F[1][1] * M[1][0];
    LL D = F[1][0] * M[0][1] + F[1][1] * M[1][1];

    F[0][0] = A;
    F[0][1] = B;
    F[1][0] = C;
    F[1][1] = D;
}

int main(void)
{
    LL N;
    printf("Enter the number : ");
    scanf("%lld",&N);
    printf("%lld",fib(N));
    return 0;
}
