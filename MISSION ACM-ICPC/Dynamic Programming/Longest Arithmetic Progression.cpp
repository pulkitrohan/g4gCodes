#include<bits/stdc++.h>
using namespace std;

int LongestAP(int *A,int N)
{
	if(N <= 2)
		return N;
	int dp[N][N];
	int max_len = 2;
	for(int i=0;i<N-1;i++)
		dp[i][N-1] = 2;
		
	for(int i=N-2;i>=1;i--) {
		int j = i-1,k = i+1;
		while(j >= 0 && k <= N-1) {
			if(A[j]+A[k] < 2*A[i])
				k++;
			else if(A[j]+A[k] > 2*A[i]) {
				dp[j][i] = 2;
				j--;
			}
			else {
				dp[j][i] = dp[i][k] + 1;
				max_len = max(max_len,dp[j][i]);
				j--,k++;
			}
		}
		while(j >= 0)
			dp[j--][i] = 2;
	}
	return max_len;
}

int main(void)
{
	int A[] = {1,7,10,13,14,19};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<LongestAP(A,N)<<endl;
	return 0;
}