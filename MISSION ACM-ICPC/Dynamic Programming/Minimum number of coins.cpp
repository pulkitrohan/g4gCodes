#include<bits/stdc++.h>
using namespace std;
#define INF 10000
int count(vector<int> coins,int S)
{
	int dp[S+1];
	memset(dp,100,sizeof(dp));
	dp[0] = 0;
	for(int i=1;i<=S;i++)
		for(int j=0;j<coins.size();j++)
			if(coins[j] <= i)
				dp[i] = min(dp[i],dp[i-coins[j]]+1);
	return dp[S];
}

int main(void)
{
	vector<int> V;
	V.push_back(1);
	V.push_back(2);
	V.push_back(5);
	V.push_back(10);
	V.push_back(20);
	V.push_back(50);
	V.push_back(100);
	cout<<count(V,23);
	return 0;
}