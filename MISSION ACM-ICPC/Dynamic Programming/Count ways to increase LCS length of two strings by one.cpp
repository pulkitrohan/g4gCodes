#include<bits/stdc++.h>
using namespace std;

int waysToIncreaseLCSBy1(string A, string B)
{
	int N = A.length(), M = B.length();
	vector<int> position[26];
	for(int i=1;i<=M;i++) {
		position[(B[i-1]-'a')].push_back(i);
	}
	int LCS[N+2][M+2];
	int LCSR[N+2][M+2];
	memset(LCS, 0, sizeof(LCS));
	memset(LCSR, 0, sizeof(LCSR));

	for(int i=1;i<=N;i++) {
		for(int j=1;j<=M;j++) {
			if(A[i-1] == B[j-1])
				LCS[i][j] = 1 + LCS[i-1][j-1];
			else
				LCS[i][j] = max(LCS[i-1][j], LCS[i][j-1]);
		}
	}
	for(int i=N;i>=1;i--) {
		for(int j=M;j>=1;j--) {
			if(A[i-1] == B[j-1]) 
				LCSR[i][j] = 1 + LCSR[i+1][j+1];
			else
				LCSR[i][j] = max(LCSR[i+1][j],LCSR[i][j+1]);
		}
	}
	int ways = 0;
	for(int i=0;i<=N;i++) {
		for(char ch = 'a'; ch <= 'z'; ch++) {
			for(int j=0;j<position[ch-'a'].size();j++) {
				int pos = position[ch-'a'][j];
				if(LCS[i][pos-1] + LCSR[i+1][pos+1] == LCS[N][M])
					ways++;
			}
		}
	}
	return ways;
}

int main(void)
{
	string A = "abcabc";
	string B = "abcd";
	cout<<waysToIncreaseLCSBy1(A, B);
	return 0;
}