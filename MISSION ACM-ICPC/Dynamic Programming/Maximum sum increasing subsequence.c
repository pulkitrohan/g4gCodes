#include<stdio.h>
#include<limits.h>
int MaxSumSequence(int *A,int N)
{
    int i,j;
    int Sum[N+1];
    for(i=0;i<N;i++)
        Sum[i] = A[i];
    for(i=1;i<N;i++)
    {
        for(j=0;j<i;j++)
        {
            if(A[i] > A[j] && Sum[i] < Sum[j]+A[i])
                Sum[i] = Sum[j] + A[i];
        }
    }
    int max = INT_MIN;
    for(i=0;i<N;i++)
    {
        if(max < Sum[i])
            max = Sum[i];
    }
    return max;
}

int main(void)
{
    int A[] = {1,101,2,3,100,4,5};
    int N = sizeof(A)/sizeof(A[0]);
    printf("Maximum sum increasing subsequence : %d\n",MaxSumSequence(A,N));
    return 0;
}
