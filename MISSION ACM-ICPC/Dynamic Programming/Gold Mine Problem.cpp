#include<bits/stdc++.h>
using namespace std;
const int MAX = 100;

int getMaxGold(int gold[][MAX],int M,int N)
{
	int dp[M][N];
	memset(dp,0,sizeof(dp));
	for(int col=N-1;col>=0;col--)
	{
		for(int row=0;row<M;row++)
		{
			int right = (col == N-1) ? 0 : dp[row][col+1];
			int right_up = (row == 0 || (col == N-1)) ? 0 : dp[row-1][col+1];
			int right_down = ((row == M-1) || (col == N-1)) ? 0 : dp[row+1][col+1];
			dp[row][col] = gold[row][col] + max(right,max(right_up,right_down));
		}
	}
	int ans = dp[0][0];
	for(int i=1;i<M;i++)
		ans = max(ans,dp[i][0]);
	return ans;
}

int main(void)
{
	int gold[MAX][MAX]= { {1, 3, 1, 5},
        {2, 2, 4, 1},
        {5, 0, 2, 3},
        {0, 6, 1, 2}
    };
    int m = 4, n = 4;
    cout << getMaxGold(gold, m, n);
    return 0;
}