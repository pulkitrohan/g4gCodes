#include<bits/stdc++.h>
using namespace std;
#define R 3
#define C 3
#define MAX_K 1000

int dp[R][C][MAX_K];

int CountPaths(int MAT[][C],int M,int N,int k)
{
	if(M < 0 || N < 0)
		return 0;
	if(M == 0 && N == 0)
		return (k == MAT[M][N]);
	if(dp[M][N][k] != -1)
		return dp[M][N][k];
	dp[M][N][k] = CountPaths(MAT,M-1,N,k-MAT[M][N]) + CountPaths(MAT,M,N-1,k-MAT[M][N]);
	return dp[M][N][k];
}

int main(void)
{
	int k = 12;
	int MAT[R][C] = { {1,2,3},
					  {4,6,5},
					  {3,2,1}
					};
	memset(dp,-1,sizeof(dp));
	cout<<CountPaths(MAT,R-1,C-1,k);
	return 0;
}