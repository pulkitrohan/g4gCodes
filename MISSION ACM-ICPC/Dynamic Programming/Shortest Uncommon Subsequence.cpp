#include<bits/stdc++.h>
using namespace std;
#define MAX 1005

int shortestSubseq(string A, string B) {
	int M = A.length(), N = B.length();
	int dp[M+1][N+1];
	for(int i=0;i<=M;i++)
		dp[i][0] = 1;
	for(int i=0;i<=N;i++)
		dp[0][i] = MAX;
	for(int i=1;i<=M;i++) {
		for(int j=1;j<=N;j++) {
			char ch = A[i-1];
			int index = -1;
			for(index=j-1;index >= 0;index--)
				if(B[index] == ch)
					break;
			if(index == -1)
				dp[i][j] = 1;
			else
				dp[i][j] = min(dp[i-1][j], dp[i-1][index] + 1);
		}
	}
	return (dp[M][N] >= MAX) ? -1 : dp[M][N];
}

int main(void)
{
	string A = "babab", B = "babba";
	cout<<shortestSubseq(A, B)<<endl;
	return 0;
}
