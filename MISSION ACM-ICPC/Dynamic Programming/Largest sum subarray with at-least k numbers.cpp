#include<bits/stdc++.h>
using namespace std;

int maxSumWithK(int A[], int N, int k) {
	int dp[N];
	dp[0] = A[0];
	int cur_max = A[0];
	for(int i=1;i<N;i++) {
		cur_max = max(A[i], cur_max + A[i]);
		dp[i] = cur_max;
	}
	int sum = 0;
	for(int i=0;i<k;i++)
		sum += A[i];
	int ans = sum;
	for(int i=k;i<N;i++) {
		sum += A[i] - A[i-k];
		ans = max(ans, sum);
		ans = max(ans, sum + dp[i-k]);
	}
	return ans;
}

int main(void)
{
	int A[] = {1, 2, 3, -10, -3};
	int k = 4;
	int N = sizeof(A)/sizeof(A[0]);
	cout<<maxSumWithK(A, N, k);
	return 0;
}
