#include<bits/stdc++.h>
using namespace std;

int func(int x,vector<int> &dp) {
	if(x == 0)
		return 0;
	if(dp[x] == -1)
		dp[x] = x + func(x/2, dp);
	return dp[x];
}

void printFloor(int A[],int N, vector<int> &dp) {
	for(int i=0;i<N;i++)
		cout<<func(A[i], dp)<<" ";
}


int main(void) {
	int A[] = {8,6};
	int N = sizeof(A)/sizeof(A[0]);
	int maxElement = *max_element(A, A+N);
	vector<int> dp(maxElement+1, -1);
	printFloor(A, N, dp);
	return 0;
}