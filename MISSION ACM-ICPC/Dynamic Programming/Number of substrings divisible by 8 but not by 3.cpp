#include<bits/stdc++.h>
using namespace std;

int countSubstr(string S) {

	int length = S.length();
	int dp[length+1][3], sum[length+1];
	memset(dp, 0, sizeof(dp));
	memset(sum, 0, sizeof(sum));

	dp[0][0] = 1;
	int cur = 0;
	for(int i=1;i<=length;i++) {
		int digit = S[i-1]-'0';
		cur = (cur + digit)%3;
		sum[i] = cur;
		for(int j=0;j<3;j++)
			dp[i][j] = dp[i-1][j];
		dp[i][sum[i]]++;
	}

	int ans = 0;
	for(int i=1;i<=length;i++) {
		int digit = S[i-1]-'0';
		if(digit == 8) 
			ans++;
		if(i >= 2) {
			int dprev = S[i-2]-'0';
			int value = dprev*10 + digit;
			if(value%8 == 0 && value%3 != 0)
				ans++;
		}
		if(i >= 3) {
			int dprev2 = S[i-3]-'0';
			int dprev = S[i-2]-'0';
			int value = dprev2*100 + dprev*10 + digit;
			if(value%8 != 0)
				continue;
			ans += i-2;
			ans -= dp[i-3][sum[i]];
		}
	}
	return ans;

}

int main(void)
{
	string S = "6564525600";
	cout<<countSubstr(S)<<endl;
	return 0;
}
