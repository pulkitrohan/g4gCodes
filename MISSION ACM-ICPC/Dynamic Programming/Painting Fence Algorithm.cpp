#include<bits/stdc++.h>
using namespace std;

long countWays(int N, int K) {
	long total = K;
	int same = 0, diff = K;
	for(int i=2;i<=N;i++) {
		same = diff;
		diff = total * (K-1);
		total = same + diff;
	}
	return total;
}

int main(void) {
	int N = 3, K = 2;
	cout<<countWays(N, K)<<endl;
	return 0;
}