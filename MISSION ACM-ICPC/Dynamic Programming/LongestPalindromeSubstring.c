#include<bits/stdc++.h>
using namespace std;
int LongestPalin(string A)
{
	int N = A.length();
	bool dp[N][N];
	int maxlength = 1,start = 0;
	memset(dp,false,sizeof(dp));
	for(int i=N-1;i>=0;i--)
	{
		for(int j=i;j<N;j++)
		{
			
			if((j-i <= 2 || dp[i+1][j-1]) && A[i] == A[j])
			{
				dp[i][j] = true;
				if(maxlength < j-i+1)
				{
					start = i;
					maxlength = j-i+1;
				}
			}
		}
	}
	cout<<A.substr(start,maxlength)<<endl;
	
	return maxlength;
}

int main(void)
{
	string S = "forgeeksskeegfor";
	printf("%d\n",LongestPalin(S));
	return 0;
}
