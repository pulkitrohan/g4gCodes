#include<bits/stdc++.h>
using namespace std;

float findWater(int i, int j, float X) {
	if(j > i) {
		return -1;
	}
	float dp[i*(i+1)/2];
	memset(dp, 0, sizeof(dp));

	int index = 0;
	dp[index] = X;

	for(int row=1;row<=i;row++) {
		for(int col=1;col<=j;col++, index++) {
			X = dp[index];
			dp[index] = (X > 1.0f) ? 1.0f : X;
			X = (X >= 1.0f) ? (X-1) : 0.0f;

			dp[index+row] += X/2;
			dp[index+row+1] += X/2;
		}
	}
	return dp[i*(i-1)/2 + j-1];
}

int main(void)
{
	int row = 2, col = 2;
	float X = 2.0;
	cout<<findWater(row, col, X);
	return 0;
}
