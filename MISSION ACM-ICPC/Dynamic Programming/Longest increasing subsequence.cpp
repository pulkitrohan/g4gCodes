/*

    The longest Increasing Subsequence (LIS) problem is to find the length of the longest subsequence of
    a given sequence such that all elements of the subsequence are sorted in increasing order. For example,
    length of LIS for { 10, 22, 9, 33, 21, 50, 41, 60, 80 } is 6 and LIS is {10, 22, 33, 50, 60, 80}.
    Time Complexity : O(n^2)
    Space Complexity : O(n)
    Solution with Time Complexity O(NlogN) also available.
10 22 9 33 21 50 41 60 80
10=>lis[0] = 1;
22=>lis[1] = 2;
9 =>lis[2] = 1;
33=>lis[3] = 3;
21=>lis[4] = 2;
50=>lis[5] = 4;
41=>lis[6] = 4;
60=>lis[7] = 5;
80=>lis[8] = 6;

*/

#include<stdio.h>
int main(void)
{
    int n,a[100],lis[100],i,j,max = 0;
    printf("Enter the size of array : ");
    scanf("%d",&n);
    for(i=0;i<n;i++)
        scanf("%d",&a[i]);
    for(i=0;i<n;i++)
        lis[i] = 1;
    for(i=1;i<n;i++)
        for(j=0;j<i;j++)
            if(a[i] > a[j] && lis[i] < lis[j] + 1)
                lis[i] = lis[j] + 1;
    for(i=0;i<n;i++)
        if(lis[i] > max)
            max = lis[i];
        printf("Length : %d",max);
    return 0;
}
