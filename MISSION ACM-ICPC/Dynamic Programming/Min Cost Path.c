/*
1 2 3
4 8 2
1 5 3
m = 2,n = 2
tc[0][0] = 1
tc[0][1] = 3
tc[0][2] = 6
tc[1][0] = 5
tc[2][0] = 6

tc[1][1] = min(tc[0][0],tc[0][1],tc[1][0]) + cost[1][1]
		 = min(1,3,5) + 8 => 9
tc[1][2] = min(tc[0][1],tc[0][2],tc[1][1]) + 2
		 = min(3,6,9) + 2 => 5
tc[2][1] = min(tc[1][0],tc[2][0],tc[1][1]) + 5
		 = min(5,6,9) + 5 => 10
tc[2][2] = min(tc[1][1],tc[2][1],tc[1][2]) + 3
		 = min(9,10,5) + 3 => 8

*/

#include<stdio.h>
int min(int A,int B)
{
    return (A > B) ? B : A;
}
int min3(int A,int B,int C)
{
    return min(min(A,B),C);
}
int main(void)
{
    int r,c,i,j;
    printf("Enter the rows and columns of the Matrix : ");
    scanf("%d %d",&r,&c);
    int a[r+1][c+1],temp[r+1][c+1];
    for(i=0;i<r;i++)
        for(j=0;j<c;j++)
            scanf("%d",&a[i][j]);
    printf("Enter the coordinates to which minimum Cost is to be found : ");
    scanf("%d %d",&r,&c);
    temp[0][0] = a[0][0];
    for(i=1;i<=r;i++)
        temp[i][0] = temp[i-1][0] + a[i][0];
    for(j=1;j<=c;j++)
        temp[0][j] = temp[0][j-1] + a[0][j];
    for(i=1;i<=r;i++)
        for(j=1;j<=c;j++)
            temp[i][j] = min3(temp[i-1][j],temp[i][j-1],temp[i-1][j-1]) + a[i][j];
    printf("%d",temp[r][c]);

    return 0;
}
