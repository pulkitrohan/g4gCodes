#include<bits/stdc++.h>
using namespace std;

int countWays(int N, int insert, int remove, int copy) {
	if(N == 0)	return 0;
	if(N == 1)	return insert;

	int dp[N+1] = {0};

	for(int i=1;i<=N;i++) {
		if(i%2 == 0)
			dp[i] = min(dp[i-1] + insert, dp[i/2] + copy);
		else
			dp[i] = min(dp[i-1] + insert, dp[(i+1)/2] + copy + remove);
	}
	return dp[N];
}

int main(void)
{
	int N = 7, insert = 1, remove = 2, copy = 1;
	cout<<countWays(N, insert, remove, copy)<<endl;
	return 0;
}
