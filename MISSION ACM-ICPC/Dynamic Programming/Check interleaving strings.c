#include<stdio.h>
#include<string.h>
int isInterLeaving(char *A,char *B,char *C)
{
	int M = strlen(A),N = strlen(B);
	int dp[M+1][N+1];
	memset(dp,0,sizeof(dp));
	if(M+N != strlen(C))
		return 0;
	int i,j;
	for(i=0;i<=M;i++)
	{
		for(j=0;j<=N;j++)
		{
			if(i == 0 && j == 0)
				dp[i][j] = 1;
			else if(i == 0 && B[j-1] == C[j-1])
				dp[i][j] = dp[i][j-1];
			else if(j == 0 && A[i-1] == C[i-1])
				dp[i][j] = dp[i-1][j];
			else if(A[i-1] == C[i+j-1] && B[j-1] != C[i+j-1])
				dp[i][j] = dp[i-1][j];
			else if(A[i-1] != C[i+j-1] && B[j-1] == C[i+j-1])
				dp[i][j] = dp[i][j-1];
			else if(A[i-1] == C[i+j-1] && B[j-1] == C[i+j-1])
				dp[i][j] = dp[i-1][j] || dp[i][j-1];
		}
	}
	return dp[M][N];
}

int main(void)
{
	char A[] = "XY";
	char B[] = "WZ";
	char C[] = "WZXY";
	if(isInterLeaving(A,B,C))
		printf("YES\n");
	else
		printf("NO\n");
}
