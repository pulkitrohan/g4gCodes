#include<bits/stdc++.h>
using namespace std;

int EggDrops(int N,int K)
{
	int dp[N+1][K+1];
	memset(dp,10000,sizeof(dp));
	int i,j,x;
	for(int egg = 1;egg<=N;egg++)
	{
		for(int floor = 0;floor<=K;floor++)
		{
			if(egg == 1 || floor == 1 || floor == 0)
				dp[egg][floor] = floor;
			for(int cur = 1;cur <= floor;cur++)
				dp[egg][floor] = min(dp[egg][floor],1 + max(dp[egg-1][cur-1],dp[egg][floor-cur]));
		}
	}
	return dp[N][K];
}


int main(void)
{
	int eggs = 2,floors = 36;
	printf("%d\n",EggDrops(eggs,floors));
	return 0;
}
