#include<bits/stdc++.h>
using namespace std;

int countDivisibleSubSeq(string S,int N)
{
	int len = S.length();
	int dp[len][N];
	memset(dp,0,sizeof(dp));
	for(int i=0;i<N;i++)
		dp[i][(S[i]-'0')%N]++;
	for(int i=1;i<len;i++)
	{
		for(int j=0;j<N;j++)
		{
			dp[i][j] += dp[i-1][j];
			dp[i][(j*10 + (S[i]-'0'))%N] += dp[i-1][j];
		}
	}
	return dp[len-1][0];
}

int main(void)
{
	string S = "1234";
	int N = 4;
	cout<<countDivisibleSubSeq(S,N);
    return 0;
}