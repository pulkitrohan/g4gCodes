#include<bits/stdc++.h>
using namespace std;

int getMinSquares(int N)
{
	int dp[N+1];
	for(int i=0;i<=N;i++)
		dp[i] = i;
	for(int i=4;i<=N;i++)
		for(int x=1;x*x<=i;x++)
			dp[i] = min(dp[i],1 + dp[i-x*x]);
	return dp[N];
}

int main(void)
{
	cout<<getMinSquares(6);
	return 0;
}