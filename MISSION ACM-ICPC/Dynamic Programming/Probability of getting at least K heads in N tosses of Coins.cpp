#include<bits/stdc++.h>
using namespace std;
#define MAX 100001
double dp[MAX];

void precompute()
{
	for(int i=2;i<MAX;i++)
		dp[i] = log2(i) + dp[i-1];
}

double probabilty(int k,int N)
{
	double ans = 0;
	for(int i=k;i<=N;i++)
	{
		double val= dp[N]-dp[i]-dp[N-i]-N;
		ans += pow(2.0,val);
	}
	return ans;
}


int main(void){
	precompute();
	cout<<probabilty(2,3)<<endl;
	return 0;
}

