#include<bits/stdc++.h>
using namespace std;

int dp[1001][15001];

void computeDpArray(int *A, int N) {
	for(int i = 0; i < N; i++) {
		for(int j = 1; j <= 5000; j++) {
			if(i == 0) 
				dp[i][j] = (j == A[i]);
			else 
				dp[i][j] = dp[i-1][j] + (j == A[i]);
		}
	}
}

int countTripletSum(int *A, int N) {
	computeDpArray(A, N);
	int ans = 0;
	for(int i=0;i<N-2;i++) {
		for(int j=i+1;j<N-1;j++) {
			for(int k=1;k<=24;k++) {
				int cube = k*k*k;
				int rem = cube - (A[i] + A[j]);
				if(rem > 0) 
					ans += dp[N-1][rem] - dp[j][rem];
			}
		}
	}
	return ans;
}

int main(void) {
	int A[] = {2, 5, 1, 20, 6};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<countTripletSum(A, N);
	return 0;
}