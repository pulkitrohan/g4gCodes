#include <bits/stdc++.h>
using namespace std;

int maxSubsequenceSubstring(string A, string B) {
	int dp[B.size()+1][A.size()+1];
	memset(dp, 0,sizeof(dp));
	for(int i=1;i<=B.size();i++) {
		for(int j=1;j<=A.size();j++) {
			if(A[j-1] == B[i-1]) {
				dp[i][j] = dp[i-1][j-1] + 1;
			}
			else {
				dp[i][j] = dp[i][j-1];
			}
		}
	}
	int ans = 0;
	for(int i=1;i<=B.size();i++) {
		ans = max(ans, dp[i][A.size()]);
	}
	return ans;
}

int main(void) {
	string A = "ABCD";
	string B = "BACDBDCD";
	cout<<maxSubsequenceSubstring(A, B)<<endl;
	return 0;
}