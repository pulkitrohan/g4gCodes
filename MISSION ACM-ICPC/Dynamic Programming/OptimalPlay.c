#include<stdio.h>
#include<string.h>
int N;
int dp[1000][1000];

#define max(A,B) ((A > B) ? A : B)

int OptimalPlay(int *A,int start,int end)
{
	if(start > end)
		return 0;
	if(dp[start][end] != -1)
		return dp[start][end];
	int sum1 = 0,sum2 = 0;
	if(A[start+1] >= A[end])
		sum1 = A[start] + OptimalPlay(A,start+2,end);
	else
		sum1 = A[start] + OptimalPlay(A,start+1,end-1);

	if(A[start] >= A[end-1])
		sum2 = A[end] + OptimalPlay(A,start+1,end-1);
	else
		sum2 = A[end] + OptimalPlay(A,start,end-2);

	return dp[start][end] = max(sum1,sum2);
}

int main(void)
{
	int A[] = {20,30,2,2,2,10};
	N = sizeof(A)/sizeof(A[0]);
	memset(dp,-1,sizeof(dp));
	printf("%d\n",OptimalPlay(A,0,N-1));
	return 0;
}
