#include<bits/stdc++.h>
using namespace std;

int countSubsequence(string S) {
	int aCount = 0, bCount = 0, cCount = 0;
	for(int i=0;i<S.length();i++) {
		if(S[i] == 'a') {
			aCount = (1 + 2*aCount);
		}
		else if(S[i] == 'b') {
			bCount = (aCount + 2*bCount);
		}
		else if(S[i] == 'c') {
			cCount = (bCount + 2*cCount);
		}
	}
	return cCount;
}

int main(void) {
	string S = "abbc";
	cout<<countSubsequence(S)<<endl;
	return 0;
}