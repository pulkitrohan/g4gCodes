#include<stdio.h>
#define min(a,b) ((a > b) ? b : a)
#define Min(a,b,c) (min(min(a,b),c))
#define EDIT_COST 1
int EditDistance(char A[],char B[])
{
    int M = strlen(A);
    int N = strlen(B);
    int i,j;
    int dp[M+1][N+1];
    for(int i=0;i<=M;i++)
	{
		for(int j=0;j<=N;j++)
		{
			if(i == 0)
				dp[i][j] = j;
			else if(j == 0)	
				dp[i][j] = i;
			else if(A[i-1] == B[j-1])
				dp[i][j] = dp[i-1][j-1];
			else
				dp[i][j] = min(dp[i][j-1],dp[i-1][j],dp[i-1][j-1])+1;
		}
	}
	return dp[M][N];
}

int main(void)
{
    char A[] = "ALLAHBAD";
    char B[] = "YATINDRA";
    printf("Minimum edits required are %d \n",EditDistance(A,B));
    return 0;
}
