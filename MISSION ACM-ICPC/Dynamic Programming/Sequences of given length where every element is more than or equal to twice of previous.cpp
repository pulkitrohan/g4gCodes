#include<bits/stdc++.h>
using namespace std;

int getTotalNumberOfSequences(int M, int N) {
	int dp[M+1][N+1];
	for(int i=0;i<=M;i++) {
		for(int j=0;j<=N;j++) {
			if(i == 0 || j == 0)
				dp[i][j] = 0;
			else if(j > i)
				dp[i][j] = 0;
			else if(j == 1)
				dp[i][j] = i;
			else
				dp[i][j] = dp[i-1][j] + dp[i/2][j-1];
		}
	}
	return dp[M][N];
}

int main(void)
{
	int M = 10, N = 4;
	cout<<getTotalNumberOfSequences(M, N);
	return 0;
}
