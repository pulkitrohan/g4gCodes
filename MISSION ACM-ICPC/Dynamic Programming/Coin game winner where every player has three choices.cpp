#include<bits/stdc++.h>
using namespace std;

int findWinner(int x, int y, int N) {
	bool dp[N+1];
	dp[0] = false;
	dp[1] = true;
	for(int i=2;i<=N;i++) {
		if(i>=1 && !dp[i-1])
			dp[i] = true;
		else if(i>=x && !dp[i-x])
			dp[i] = true;
		else if(i>=y && !dp[i-y])
			dp[i] = true;
		else
			dp[i] = false;
	}
	return dp[N];
}

int main(void) {
	int x = 3, y = 4, N = 5;
	if (findWinner(x, y, N))
        cout << 'A';
    else
        cout << 'B';
	return 0;
}