#include<bits/stdc++.h>
using namespace std;
#define INF 1000000000

int minCost(int *A, int N, int K) {
	int dp[N+1][K+1];
	for(int i=0;i<=N;i++)
		for(int j=0;j<=K;j++)
			dp[i][j] = INF;
	dp[0][0] = 0;
	for(int i=1;i<=N;i++) {
		for(int j=1;j<=K;j++) {
			for(int m=i-1;m>=0;m--)
				dp[i][j] = min(dp[i][j], dp[m][j-1] + (A[i-1]-A[m]) * (A[i-1] - A[m]));
		}
	}
	return dp[N][K];
}


int main(void)
{
	int k = 2;
	int A[] = {1,5,8,10};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<minCost(A,N,k)<<endl;
	return 0;
}