#include<stdio.h>
int main(void)
{
    int N,i,sum_ending_here = 0,sum_so_far = 0;
	printf("Enter the Total Elements you want : ");
	scanf("%d",&N);
	int A[N+1];
	for(i=0;i<N;i++)
		scanf("%d",&A[i]);
	for(i=0;i<N;i++)
	{
		sum_ending_here += A[i];
		if(sum_ending_here < 0)
			sum_ending_here = 0;
		else if(sum_so_far < sum_ending_here)
			sum_so_far = sum_ending_here;
	}
	printf("Largest sum Contiguous Subarray : %d",sum_so_far);
	return 0;
}
