#include<bits/stdc++.h>
using namespace std;

int maxRevenue(int M,int x[],int revenue[],int N,int t)
{
	int dp[M+1];
	memset(dp,0,sizeof(dp));
	int nextBB = 0;
	for(int mile=1;mile<=M;mile++)
	{
		if(nextBB < N)
		{
			if(x[nextBB] != mile)
				dp[mile] = dp[mile-1];
			else
			{
				if(mile <= t)
					dp[mile] = max(dp[mile-1],revenue[nextBB]);
				else
					dp[mile] = max(dp[mile-t-1] + revenue[nextBB],dp[mile-1]);
				nextBB++;
			}
		}
		else
			dp[mile] = dp[mile-1];
	}
	return dp[M];
}


int main(void)
{
	int M = 20;
	int x[] = {6,7,12,13,14};
	int revenue[] = {5,6,5,3,1};
	int N = sizeof(x)/sizeof(x[0]);
	int t = 5;
	cout<<maxRevenue(M,x,revenue,N,t)<<endl;
	return 0;
}