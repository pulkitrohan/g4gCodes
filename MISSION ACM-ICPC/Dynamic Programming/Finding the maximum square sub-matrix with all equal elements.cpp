#include<bits/stdc++.h>
using namespace std;
#define ROW 6
#define COL 6

int largestSubMatrix(int A[][COL])
{
	int dp[ROW][COL];
	memset(dp,0,sizeof(dp));
	int ans = 0;
	for(int i=0;i<ROW;i++)
	{
		for(int j=0;j<COL;j++)
		{
			if(i == 0 || j == 0)
				dp[i][j] = 1;
			else
			{
				if(A[i][j] == A[i-1][j] && A[i][j] == A[i][j-1] && A[i][j] == A[i-1][j-1])
					dp[i][j] = min(min(dp[i-1][j],dp[i][j-1]),dp[i-1][j-1]) + 1;
				else
					dp[i][j] = 1;
			}
			ans = max(ans,dp[i][j]);
		}
	}
	return ans;
}

int main(void)
{
	int A[ROW][COL] = { 2, 2, 3, 3, 4, 4,
                        5, 5, 7, 7, 7, 4,
                        1, 2, 7, 7, 7, 4,
                        4, 4, 7, 7, 7, 4,
                        5, 5, 5, 1, 2, 7,
                        8, 7, 9, 4, 4, 4
                      };
    cout<<largestSubMatrix(A)<<endl;
    return 0;
}