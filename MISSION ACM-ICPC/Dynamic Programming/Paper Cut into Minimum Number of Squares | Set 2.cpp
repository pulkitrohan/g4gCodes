#include<bits/stdc++.h>
using namespace std;

int minimumSquare(int M,int N, vector<vector<int>> &dp) {
	int vertical_min = INT_MAX;
	int horizontal_min = INT_MAX;

	if(M == N)
		return 1;
	if(dp[M][N])
		return dp[M][N];

	for(int i=1;i<=M/2;i++)
		horizontal_min = min(horizontal_min, minimumSquare(i, N, dp) + minimumSquare(M-i, N, dp));

	for(int j=1;j<=N/2;j++)
		vertical_min = min(vertical_min, minimumSquare(M, j, dp) + minimumSquare(M, N-j, dp));

	dp[M][N] = min(horizontal_min, vertical_min);
	return dp[M][N];
}

int main(void) {
	int M = 30, N = 35;
	vector<vector<int> > dp(M, vector<int>(N, 0));
	cout<<minimumSquare(M, N, dp);
	return 0;
}