#include<bits/stdc++.h>
using namespace std;
struct Box
{
	int h,w,d;
};

bool cmp(struct Box A,struct Box B)
{
	return (A.w*A.w+A.d*A.d > B.w*B.w+A.d*A.d);
}

int MaxStackHeight(struct Box A[],int N)
{
	struct Box B[3*N];
	int index = 0;
	for(int i=0;i<N;i++)
	{
		B[index++] = A[i];
		B[index].h = A[i].w;
		B[index].d = max(A[i].h,A[i].d);
		B[index++].w = min(A[i].h,A[i].d);
		
		B[index].h = A[i].d;
		B[index].d = max(A[i].h,A[i].w);
		B[index++].w = min(A[i].h,A[i].w);
	}
	N = N*3;
	sort(B,B+N,cmp);
	int LIS[N];
	for(int i=0;i<N;i++)
		LIS[i] = B[i].h;
	for(int i=1;i<N;i++)
	{
		for(int j=0;j<i;j++)
		{
			if(B[i].w < B[j].w && B[i].d < B[j].d && (B[i].h + LIS[j] > LIS[i]))
				LIS[i] = B[i].h+LIS[j];
		}
	}
	int max = INT_MIN;
	for(int i=0;i<N;i++)
		if(max < LIS[i])
			max = LIS[i];
	return max;
}

int main(void)
{
	struct Box A[] = {{4,6,7},{1,2,3},{4,5,6},{10,12,32}};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<MaxStackHeight(A,N);
	return 0;
}