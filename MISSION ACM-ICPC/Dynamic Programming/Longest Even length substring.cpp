#include<bits/stdc++.h>
using namespace std;

int main(void)
{
	string S = "1538023";
	int N = S.length(),maxlen = 0,start = 0;
	for(int i=1;i<N-1;i++)
	{
		int j = i;
		int k = i+1;
		int leftsum = 0,rightsum = 0;
		while(j >= 0 && k < N)
		{
			leftsum += (S[j]-'0');
			rightsum += (S[k]-'0');
			if(leftsum == rightsum)
			{
				if(k-j+1 > maxlen)
				{
					start = j;
					maxlen = k-j+1;
				}
			}
			j--,k++;
		}
	}
	cout<<maxlen<<" "<<S.substr(start,maxlen)<<endl;
	return 0;
}