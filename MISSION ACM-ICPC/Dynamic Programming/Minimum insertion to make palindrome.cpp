#include<bits/stdc++.h>
using namespace std;

int MinInsertion(string S)
{
	int N = S.length();
	if(N == 0)
		return 0;
	int dp[N][N];
	memset(dp,0,sizeof(dp));
	for(int gap=1;gap<N;gap++)
	{
		for(int i=0,j=gap;j<N;i++,j++)
		{
			if(S[i] == S[j])
				dp[i][j] = dp[i+1][j-1];
			else
				dp[i][j] = min(dp[i][j-1],dp[i+1][j]) + 1;
		}
	}
	return dp[0][N-1];
}

int main(void)
{
	string S = "geeks";
	cout<<MinInsertion(S);
	return 0;
}