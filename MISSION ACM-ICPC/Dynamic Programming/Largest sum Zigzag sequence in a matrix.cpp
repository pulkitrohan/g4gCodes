#include<bits/stdc++.h>
using namespace std;

int dp[100][100];

int largestZigZag(int mat[][100], int row, int col, int N) {
	if(dp[row][col] != -1)
		return dp[row][col];
	if(row == N-1)
		return (dp[row][col] = mat[row][col]);

	int ans = 0;
	for(int i=0;i<N;i++) {
		if(i != col)
			ans = max(ans, largestZigZag(mat, row+1, i, N));
	}
	return dp[row][col] = ans + mat[row][col];
}


int main(void) {
	int N = 3;
	int mat[][100] = {
						 { 4, 2, 1 },
						 { 3, 9, 6 },
						 { 11, 3, 15 }
					};
	memset(dp, -1, sizeof(dp));
	int ans = 0;
	for(int col=0;col<N;col++) {
		ans = max(ans, largestZigZag(mat, 0, col, N));
	}
	cout<<ans;
    return 0;
}