/*
    LCS Problem Statement: Given two sequences, find the length of longest subsequence present in both of them.
    A subsequence is a sequence that appears in the same relative order, but not necessarily contiguous.
    LCS for input Sequences �ABCDGH� and �AEDFHR� is �ADH� of length 3.
    Time Complexity : O(n*m)
    Space Complexity : O(n*m)

      A B C D G H
	0 0 0 0 0 0 0
A	0 1 1 1 1 1 1
E   0 1 1 1 1 1 1
D   0 1 1 1 2 2 2
F   0 1 1 1 2 2 2
H   0 1 1 1 2 2 3
R   0 1 1 1 2 2 3

Ans : 3
L[i][j] = L[i-1][j-1] + 1 if A[i] == B[j]
          Max(L[i-1][j],L[i][j-1]) else
*/
#include<stdio.h>
#include<string.h>

int max(int A,int B)
{
    return (A > B) ? A : B;
}
int main(void)
{
    char a[100],b[100];
    int n,m,i,j;
    printf("Enter two strings : ");
    scanf("%s %s",a,b);
    n = strlen(a);
    m = strlen(b);
    int L[n+1][m+1];
    for(i=0;i<=n;i++)
        L[i][0] = 0;
    for(j=0;j<=m;j++)
        L[0][j] = 0;
    for(i=1;i<=n;i++)
        for(j=1;j<m;j++)
    {
        if(a[i-1] == b[j-1])
            L[i][j] = L[i-1][j-1] + 1;
        else
            L[i][j] = max(L[i-1][j],L[i][j-1]);
    }
    printf("Length : %d",L[n][m]);
    return 0;
}
