#include<stdio.h>
#include<string.h>

int CountDecoding(char *A,int N)
{
    int count[N+1];
    int i;
	memset(count,0,sizeof(count));
    count[0] = 1;
    count[1] = 1;
    for(i=2;i<=N;i++)
    {
        //count[i] = 0;
        if(A[i-1] > '0')
            count[i] = count[i-1];
        if(A[i-2] < '2' || (A[i-2] == '2' && A[i-1] < '7'))
           count[i] += count[i-2];
    }
    return count[N];
}

int main(void)
{
    char A[] = "1234";
    printf("%d",CountDecoding(A,strlen(A)));
    return 0;
}
