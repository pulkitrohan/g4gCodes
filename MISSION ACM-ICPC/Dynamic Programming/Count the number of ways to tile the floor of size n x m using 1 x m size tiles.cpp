#include<bits/stdc++.h>
using namespace std;

int countWays(int N, int M) {
	int count[N+1];
	count[0] = 0;
	for(int i=1;i<=N;i++) {
		if(i > M)
			count[i] = count[i-1] + count[i-M];
		else if(i < M)
			count[i] = 1;
		else
			count[i] = 2;
	}
	return count[N];
}

int main(void)
{
	int N = 7, M = 4;
	cout<<countWays(N, M)<<endl;
	return 0;
}
