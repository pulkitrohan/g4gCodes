#include<bits/stdc++.h>
using namespace std;

void countWaysToJump(int A[], int N) {
	int dp[N] = {0};
	for(int i=N-2;i>=0;i--) {
		if(i + A[i] >= N-1)
			dp[i]++;
		for(int j=i+1;j<N-1 && j <= i + A[i];j++) {
			if(dp[j] != -1)
				dp[i] += dp[j];
		}
		if(dp[i] == 0)
			dp[i] = -1;
	}
	for(int i=0;i<N;i++)
		cout<<dp[i]<<" ";
}

int main(void)
{
	int A[] = {1, 3, 5, 8, 9, 1, 0, 7, 6, 8, 9};
	int N = sizeof(A)/sizeof(A[0]);
	countWaysToJump(A, N);
	return 0;
}
