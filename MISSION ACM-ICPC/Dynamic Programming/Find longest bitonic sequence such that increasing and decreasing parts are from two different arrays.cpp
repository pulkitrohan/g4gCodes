#include<bits/stdc++.h>
using namespace std;
int GetCeilIndex(int A[], vector<int> &T, int l,int r, int key) {
	while(r - l > 1) {
		int m = l + (r-l)/2;
		if(A[T[m]] >= key)
			r = m;
		else
			l = m;
	}
	return r;
}

vector<int> LIS(int A[],int N) {
	vector<int> tailIndeces(N, 0);
	vector<int> prevIndeces(N, -1);

	int len = 1;
	for(int i=1;i<N;i++) {
		if(A[i] < A[tailIndeces[0]]) 
			tailIndeces[0] = i;
		else if(A[i] > A[tailIndeces[len-1]]) {
			prevIndeces[i] = tailIndeces[len-1];
			tailIndeces[len++] = i;
		}
		else {
			int pos = GetCeilIndex(A, tailIndeces, -1, len-1, A[i]);
			prevIndeces[i] = tailIndeces[pos-1];
			tailIndeces[pos] = i;
		}
	}
	vector<int> res;
	for(int i=tailIndeces[len-1]; i>=0; i = prevIndeces[i]) 
		res.push_back(A[i]);
	return res;
}


void longestBitonic(int A[], int N, int B[], int M) {
	vector<int> res1 = LIS(A, N);
	reverse(res1.begin(), res1.end());
	reverse(B, B + M);
	vector<int> res2 = LIS(B, M);
	res1.insert(res1.end(), res2.begin(), res2.end());
	for(int i=0; i<res1.size();i++)
		cout<<res1[i]<< " ";
}

int main(void) {
	int A[] = {1,2,4,3,2};
	int B[] = {8,6,4,7,8,9};
	int N = sizeof(A)/sizeof(A[0]);
	int M = sizeof(B)/sizeof(B[0]);
	longestBitonic(A, N, B, M);
	return 0;
}