#include<bits/stdc++.h>
using namespace std;

int MaxProTwoTran(int *A,int N)
{
	int left[N+1],right[N+1];
	memset(left,0,sizeof(left));
	memset(right,0,sizeof(right));
	int mini = A[0];
	for(int i=1;i<N;i++)
	{
		left[i] = max(left[i-1],A[i]-mini);
		mini = min(mini,A[i]);
	}
	int maxi = A[N-1];
	for(int i=N-2;i>=0;i--)
	{
		right[i] = max(right[i+1],maxi-A[i]);
		maxi = max(maxi,A[i]);
	}
	int ans = 0;
	for(int i=0;i<N;i++)
		ans = max(ans,left[i]+right[i]);
	return ans;
}

int main(void)
{
	int A[] = {10, 22, 5, 75, 65, 80};
	int N = sizeof(A)/sizeof(int);
	cout<<MaxProTwoTran(A,N);
	return 0;
}