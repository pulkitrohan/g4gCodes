#include<bits/stdc++.h>
using namespace std;

int sumOfSubstrings(string S) {
	vector<int> sumOfDigit(S.length(), 0);
	sumOfDigit[0] = S[0]-'0';
	int ans = sumOfDigit[0];
	for(int i=1;i<S.length();i++) {
		sumOfDigit[i] = (i+1)*(S[i]-'0') + 10 * sumOfDigit[i-1];
		ans += sumOfDigit[i];
	}
	return ans;

}

int main(void) {
	string S = "1234";
	cout<<sumOfSubstrings(S)<<endl;
	return 0;
}