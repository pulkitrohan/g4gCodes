#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
int MinJumps(int *A,int N)
{
   if(N <= 1)
        return 0;
    int maxReachable = 0,maxNextAvailable = 0;
    int minSteps = 0;
    for(int i=0;i<N;i++)
    {
        if(i > maxReachable)
        {
            if(maxNextAvailable > maxReachable)
            {
                maxReachable = maxNextAvailable;
                minSteps++;
            }
            else
                return -1;
        }
        maxNextAvailable = max(maxNextAvailable,A[i]+i);
        if(maxNextAvailable >= N-1)
            return minSteps+1;
    }
}

int main(void)
{
    int A[] = {1,3,6,1,0,9};
    int N = sizeof(A)/sizeof(A[0]);
    printf("%d",MinJumps(A,N));
    return 0;
}
