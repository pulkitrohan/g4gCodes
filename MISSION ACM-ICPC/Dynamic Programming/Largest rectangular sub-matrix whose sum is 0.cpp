#include<bits/stdc++.h>
using namespace std;

bool sumZero(int temp[],int &start, int &end, int N) {
	map<int,int> M;
	int sum = 0;
	int maxLength = 0;
	for(int i = 0;i<N;i++) {
		sum += temp[i];
		if(temp[i] == 0 && maxLength == 0) {
			start = i;
			end = i;
			maxLength = 1;
		}
		if(sum == 0) {
			if(maxLength < i+1) {
				start = 0;
				end = i;
			}
			maxLength = i+1;
		}
		if(M.find(sum) != M.end()) {
			int prevLen = maxLength;
			maxLength = max(maxLength, i - M[sum]);
			if(prevLen < maxLength) {
				end = i;
				start = M[sum] + 1;
			}
		}
		else {
			M[sum] = i;
		}
	}
	return maxLength != 0;
}

void sumZeroMatrix(int A[][4], int row,int col) {
	int temp[row];

	int fup = 0,fdown = 0,fleft = 0,fright = 0;
	int up,down;
	int maxElements = INT_MIN;
	for(int left = 0;left < col;left++) {
		memset(temp, 0, sizeof(temp));
		for(int right=left;right<col;right++) {
			for(int i=0;i<row;i++)
				temp[i] += A[i][right];
			bool isSumZero = sumZero(temp, up, down, row);
			int totalElements = (down-up + 1) * (right-left + 1);
			if(isSumZero && totalElements > maxElements) {
				fup = up;
				fdown = down;
				fleft = left;
				fright = right;
				maxElements = totalElements;
			}
		}
	}
	if(fup == 0 && fdown == 0 && fleft == 0 && fright == 0 && A[0][0] != 0) {
		return;
	}
	for(int i=fup;i<=fdown;i++) {
		for(int j=fleft;j<=fright;j++)
			cout<<A[i][j]<<" ";
		cout<<endl;
	}
}

int main(void) {
	int A[][4] = { { 9, 7, 16, 5 }, { 1, -6, -7, 3 },
                      { 1, 8, 7, 9 }, { 7, -2, 0, 10 } };
 
    int row = 4, col = 4;
    sumZeroMatrix(A, row, col);
    return 0;
}