#include<bits/stdc++.h>
using namespace std;

int maxSumBitonicArr(int *A, int N) {
	if(N < 1) {
		return 0;
	}
	int inc[N+1], dec[N+1];

	inc[0] = A[0];
	for(int i=1;i<N;i++)
		if(A[i] > A[i-1]) 
			inc[i] = inc[i-1] + A[i];
		else
			inc[i] = A[i];

	dec[N-1] = A[N-1];
	for(int i=N-2;i>=0;i--) 
		if(A[i] > A[i+1]) 
			dec[i] = dec[i+1] + A[i];
		else
			dec[i] = A[i];
		
	int ans = INT_MIN;
	for(int i=0;i<N;i++)
		if(ans < (inc[i] + dec[i] - A[i]))
			ans = inc[i] + dec[i] - A[i];
	return ans;
}

int main(void) {
	int A[] = {5,3,9,2,7,6,4};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<maxSumBitonicArr(A, N);
	return 0;
}