//http://www.programcreek.com/2014/05/leetcode-pain-fence-java/

#include<bits/stdc++.h>
using namespace std;

int numWays(int N,int K)
{
	int dp[] = {0,K,K*K,0};
	if(N <= 2)
		return dp[N];
	for(int i=2;i<N;i++)
	{
		dp[3] = (K-1)*(dp[1]+dp[2]);
		dp[1] = dp[2];
		dp[2] = dp[3];
	}
	return dp[3];
}

int main(void)
{
	int N = 5,K = 3;
	cout<<numWays(N,K)<<endl;
	return 0;
}