#include<bits/stdc++.h>
using namespace std;
#define MAX 100
int dp[MAX][MAX][MAX][2];

int CountPaths(int M,int N,int k,int d)
{
	if(M < 0 || N < 0)
		return 0;
	if(M == 0 && N == 0)
		return 1;
	if(k == 0)
	{
		if(d == 0 && M == 0)
			return 1;
		if(d == 1 && N == 0)
			return 1;
		else
			return 0;
	}
	if(dp[M][N][k][d] != -1)
		return dp[M][N][k][d];
	if(d == 0)
		return dp[M][N][k][d] = CountPaths(M-1,N,k-1,!d) + CountPaths(M,N-1,k,d);
	else
		return dp[M][N][k][d] = CountPaths(M-1,N,k,d) + CountPaths(M,N-1,k-1,!d);
}

int main(void)
{
	int M = 3,N = 3,k = 2;
	memset(dp,-1,sizeof(dp));
	int i = M-1,j = N-1;
	cout<<CountPaths(i-1,j,k,1) + CountPaths(i,j-1,k,0);
	return 0;
}