#include<bits/stdc++.h>
using namespace std;

vector<int> findLIS(vector<int> A,int N)
{
	vector<vector<int> > L(N);
	L[0].push_back(A[0]);
	for(int i=1;i<N;i++)
	{
		for(int j=0;j<i;j++)
			if(A[i] > A[j])
				L[i] = L[j];
		L[i].push_back(A[i]);
	}
	int maxSize = 1;
	vector<int> ans;
	for(auto x : L)
	{
		if(x.size() > maxSize)
		{
			ans = x;
			maxSize = x.size();
		}
	}
	return ans;
}

void minimize(int A[],int N)
{
	vector<int> arr(A,A+N);
	while(arr.size() != 0)
	{
		vector<int> lis = findLIS(arr,arr.size());
		if(lis.size() < 2)
			break;
		for(int i=0;i < arr.size() && lis.size() > 0;i++)
		{
			if(arr[i] == lis[0])
			{
				arr.erase(arr.begin()+i);
				i--;
				lis.erase(lis.begin());
			}
		}
	}
	if(arr.size() == 0)
		cout<<"-1";
	else
		for(int i=0;i<arr.size();i++)
			cout<<arr[i]<<" ";
}

int main(void)
{
	int A[] = {3,2,6,4,5,1};
	int N = sizeof(A)/sizeof(A[0]);
	minimize(A,N);
    return 0;
}