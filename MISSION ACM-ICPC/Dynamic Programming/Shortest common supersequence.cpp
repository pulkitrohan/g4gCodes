#include<bits/stdc++.h>
using namespace std;

int SCS(string A,string B)
{
	int N = A.length(),M = B.length();
	int dp[N+1][M+1];
	for(int i=0;i<=N;i++)
	{
		for(int j=0;j<=M;j++)
		{
			if(i == 0)
				dp[i][j] = j;
			else if(j == 0)
				dp[i][j] = i;
			else if(A[i-1] == B[j-1])
				dp[i][j] = 1 + dp[i-1][j-1];
			else
				dp[i][j] = 1 + min(dp[i-1][j],dp[i][j-1]);
		}
	}
	return dp[N][M];
}


int main(void)
{
	string A = "AGGTAB";
	string B = "GXTXAYB";
	cout<<SCS(A,B);
	return 0;
}