#include<stdio.h>
#include<limits.h>

#define max(a,b) ((a > b) ? a : b)
int CutRod(int *A,int N)
{
    int val[N+1],i,j,max_val;
    for(i=0;i<=N;i++)
		val[i] = INT_MIN;
    val[0] = 0;
    for(i=1;i<=N;i++)
    {
        for(j=1;j<=i;j++)
        {
            val[i] = max(val[i],A[j-1] + val[i-j]);
            printf("val[%d] = %d\n",i,val[i]);
        }

    }
    return val[N];
}

int main(void)
{
    int A[] = {1,5,8,9,10,17,17,20};
    int size = sizeof(A)/sizeof(A[0]);
    printf("Maximum Obtainable Value : %d\n",CutRod(A,size));
    return 0;
}
