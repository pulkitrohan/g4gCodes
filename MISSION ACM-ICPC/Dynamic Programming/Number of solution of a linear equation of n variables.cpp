#include<bits/stdc++.h>
using namespace std;

int CountSol(int *A,int N,int sum)
{
	int dp[sum+1];
	memset(dp,0,sizeof(dp));
	dp[0] = 1;
	for(int i=0;i<N;i++)
		for(int j=A[i];j<=sum;j++)
			dp[j] += dp[j-A[i]];
	return dp[sum];
}

int main(void)
{
	int A[] = {2,2,5};
	int sum = 4;
	int N = sizeof(A)/sizeof(A[0]);
	cout<<CountSol(A,N,sum);
	return 0;
}