vector< pair<int,int> > merge(vector< pair<int,int> > &A,vector< pair<int,int> > &B)
    {
         vector<pair<int,int>> C;
         int h1 = 0,h2 = 0,i = 0,j = 0;
         while(i < A.size() && j < B.size())
         {
             int x = 0,h = 0;
             if(A[i].first < B[j].first)
             {
                 x = A[i].first;
                 h1 = A[i].second;
                 h = max(h1,h2);
                 i++;
             }
             else if(A[i].first > B[j].first)
             {
                 x = B[j].first;
                 h2 = B[j].second;
                 h = max(h1,h2);
                 j++;
             }
             else
             {
                 x = A[i].first;
                 h1 = A[i].second;
                 h2 = B[j].second;
                 h = max(h1,h2);
                 i++,j++;
             }
             if(C.size() == 0 || h != C[C.size()-1].second)
                C.push_back(make_pair(x,h));
         }
         while(i < A.size())
            C.push_back(A[i++]);
         while(j < B.size())
            C.push_back(B[j++]);
        return C;
    }
        
    vector< pair<int,int> > recurSkyline(vector< vector<int> > &A,int start,int end)
    {
        if(end > start)
        {
            int mid = start + (end-start)/2;
            vector< pair<int,int> > B = recurSkyline(A,start,mid);
            vector< pair<int,int> > C = recurSkyline(A,mid+1,end);
            return merge(B,C);
        }
        else
        {
            vector<pair<int,int>> V;
            V.push_back(make_pair(A[start][0],A[start][2]));
            V.push_back(make_pair(A[start][1],0));
            return V;
        }
    }