#include<bits/stdc++.h>
using namespace std;

struct point
{
	int x,y;
};

int orientation(point A,point B,point C)
{
	int val = (B.y - A.y) * (C.x - B.x) - (C.y - B.y) * (B.x - A.x);
	if(val == 0)	return 0;
	return (val > 0) ? 1 : 2;
}

bool onSegment(Point A,point B,point C)
{
	return (B.x <= max(A.x,C.x) && B.x >= min(A.x,C.x) && B.y <= max(A.y,C.y) && B.y >= min(A.y,C.y))
}

bool DoIntersect(point P1,point Q1,point P2,point Q2)
{
	int o1 = orientation(P1,Q1,P2);
	int o2 = orientation(P1,Q1,Q2);
	int o3 = orientation(P2,Q2,P1);
	int o4 = orientation(P2,Q2,Q1);
	
	if(o1 != o2 && o3 != o3)
		return true;
	if(o1 == 0 && onSegment(P1,P2,Q1))
		return true;
	if(o2 == 0 && onSegment(P1,Q2,Q1))
		return true;
	if(o3 == 0 && onSegment(P2,P1,Q2))
		return true;
	if(o4 == 0 && onSegment(P2,Q1,Q2))
		return true;
	return false;
}

int main(void)
{
	struct point P1 = {1,1},Q1 = {10,1};
	struct point P2 = {1,2},Q2 = {10,2};
	cout<<DoIntersect(P1,Q1,P2,Q2);
	return 0;
}