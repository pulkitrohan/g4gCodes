#include<cstdio>
#include<stack>
#include<algorithm>
using namespace std;
class Point
{
	public: 
		int x,y;
};
	
Point pivot;

int ccw(Point A,Point B,Point C)
{
	int area = (B.x - A.x)*(C.y - A.y) - (B.y - A.y)*(C.x - A.x);
	if(area > 0)
		return -1;
	else if(area < 0)
		return 1;
	return 0;
}

int sqrtDist(Point A,Point B)
{
	int dx = A.x - B.x,dy = A.y - B.y;
	return dx*dx + dy*dy;
}

bool POLAR_ORDER(Point a,Point b)
{
	int order = ccw(pivot,a,b);
	if(order == 0)
		return sqrtDist(pivot,a) < sqrtDist(pivot,b);
	return (order == -1);
}

stack<Point> GrahamScan(Point *P,int N)
{
	stack<Point> hull;
	
	if(N < 3)
		return hull;
	
	int leastY = 0;
	for(int i=1;i<N;i++)
		if(P[i].y < P[leastY].y || (P[i].y == P[leastY].y && P[i].x < P[leastY].x))
			leastY = i;
			
	Point temp = P[0];
	P[0] = P[leastY];
	P[leastY] = temp;
	
	pivot = P[0];
	sort(P+1,P+N,POLAR_ORDER);
	
	hull.push(P[0]);
	hull.push(P[1]);
	hull.push(P[2]);
	for(int i=3;i<N;i++)
	{
		Point top = hull.top();
		hull.pop();
		while(ccw(hull.top(),top,P[i]) != -1)
		{
			top = hull.top();
			hull.pop();
		}
		hull.push(top);
		hull.push(P[i]);
	}
	return hull;
}
	


int main(void)
{
	Point points[] = {{0, 0}, {1, 1}, {2, 2}, {3, -1}};
    int N = sizeof(points)/sizeof(points[0]);

    stack<Point> hull = GrahamScan(points, N);
    while (!hull.empty())   {
        Point p = hull.top();
        hull.pop();

        printf("(%d, %d)\n", p.x, p.y);
    }

	return 0;
}