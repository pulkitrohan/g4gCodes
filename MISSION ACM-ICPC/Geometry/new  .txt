#include<bits/stdc++.h>
using namespace std;

struct point
{
	int x,y;
};

int distance(point A,point B)
{
	return (A.x-B.x)*(A.x-B.x) + (A.y-B.y)*(A.y-B.y);
}

bool isSquare(point P1,point P2,point P3,point P4)
{
	int d2 = distance(P1,P2);
	int d3 = distance(P1,P3);
	int d4 = distance(P1,P4);
	if(d2 == d3 && 2*d2 == d4)
	{
		int d = distance(P2,P4);
		return (d == distance(P3,P4) && d == d2);
	}
	
	if(d3 == d4 && 2*d3 == d2)
	{
		int d = distance(P2,P3);
		return (d == distance(P2,P4) && d == d3);
	}
	
	if(d2 == d4 && 2*d2 == d3)
	{
		int d = distance(P2,P3);
		return (d == distance(P3,P4) && d == d2);
	}
}

int main(void)
{
	point p1 = {20,10},p2 = {10,20},p3 = {20,20},p4 = {10,10};
	cout<<isSquare(p1,p2,p3,p4);
	return 0;
}
#include<bits/stdc++.h>
using namespace std;

struct point
{
	int x,y;
};

int distance(point A,point B)
{
	return (A.x-B.x)*(A.x-B.x) + (A.y-B.y)*(A.y-B.y);
}

bool isSquare(point P1,point P2,point P3,point P4)
{
	int d2 = distance(P1,P2);
	int d3 = distance(P1,P3);
	int d4 = distance(P1,P4);
	if(d2 == d3 && 2*d2 == d4)
	{
		int d = distance(P2,P4);
		return (d == distance(P3,P4) && d == d2);
	}
	
	if(d3 == d4 && 2*d3 == d2)
	{
		int d = distance(P2,P3);
		return (d == distance(P2,P4) && d == d3);
	}
	
	if(d2 == d4 && 2*d2 == d3)
	{
		int d = distance(P2,P3);
		return (d == distance(P3,P4) && d == d2);
	}
}

int main(void)
{
	point p1 = {20,10},p2 = {10,20},p3 = {20,20},p4 = {10,10};
	cout<<isSquare(p1,p2,p3,p4);
	return 0;
}
