#include<stdio.h>
//Process structure
 struct pro
 {
	char name[10]; //Name of processes
	int burst;     //Burst Time of process
	int arrival;   //Arrival time of process
	int end;       //Eend time of process
	int wait;      //Wait time of process
	int turn;      //Turn around time of process
 }process[10];

void main()
{
   int n=0,i=0,min,pos,total=0,elapse=0;
   float avg_wait=0,avg_turn=0;
   printf("\t\tFCFS Scheduling algorithm\n\n\n");
   printf("Enter number of processes to be scheduled:\n");
   scanf("%d",&n);
   printf("\nEnter Details of processes:-\n\n");
   for(i=0;i<n;i++)
      {
	   printf("\nEnter name of process:\n");
	   scanf("%s",process[i].name);
	   printf("Enter burst time of process:\n");
	   scanf("%d",&process[i].burst);
	   printf("Enter arrival time of process:\n");
	   scanf("%d",&process[i].arrival);
	   }

        i=0;

   while(i<n)
        {
         if(i==0)    //For first process wait is zero
           {
            process[i].wait=0;
           }

        else
	        {
	         total+=process[i-1].burst;   //Total time elapsed
             process[i].wait=total-process[i].arrival; //Wait of otherprocesses is time elapsed - arrival time
            }

            process[i].turn=process[i].wait+process[i].burst; //Turn around time is sum of burst time and wait time

           i++;

          }
           printf("\n\nPROCESS DETAILS:\n");
          printf("\nProcess\t Waiting time\tTurn around time\n");
          for(i=0;i<n;i++)
            {
             printf("  %s\t\t%d\t\t%d\n",process[i].name,process[i].wait,process[i].turn);
   		     avg_wait+=(float)process[i].wait/n;
   		     avg_turn+=(float)process[i].turn/n;
            }


   printf("\nAverage waiting time : %f",avg_wait);
   printf("\nAverage turn around time: %f\n",avg_turn);
  }

/*
	#include<stdio.h>

struct pro
{
	char name[10];
	int arrival;
	int burst;
	int wait;
	int turn;
}process[10];

int main(void)
{
	printf("Enter the number of processes : ");
	scanf("%d",&N);
	for(i=0;i<N;i++)
	{
		printf("Enter the process name : ");
		scanf("%s",process[i].name);
		printf("Enter the arrival time : ");
		scanf("%d",&process[i].arrival);
		printf("Enter the burst time : ");
		scanf("%d",&process[i].burst);
	}
	int total = 0;
	for(i=0;i<N;i++)
	{
		if(i == 0)
		{
			process[i].wait = 0;
		}
		else
		{
			total += process[i-1].burst;
			process[i].wait = total - process[i].arrival;
		}
	process[i].turn = process[i].burst+process[i].wait;
	}
	int total_wait = 0,total_turn = 0
	for(i=0;i<n;i++)
	{
		total_wait += process[i].wait;
		total_turn += process[i].turn;
	}
	avg_wait = (float)total_wait/n;
	avg_turn = (float)total_turn/n;

	return 0;
}

p0	0	5
p1	1   7
p2	2 	9

*/


