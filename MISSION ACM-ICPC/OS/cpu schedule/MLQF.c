#include<stdio.h>
// Queue one using Round robin with time quantum 2 ms
// Queue two using Round robin with time quantum 1 ms
// Queue three using  FCFS
//Structure of process
 struct pro{
            char name[10]; //Name of processes
            int burst;     //Burst Time of process
            int arrival;   //Arrival time of process
            int end;       //Eend time of process
            int wait;      //Wait time of process
            int turn;      //Turn around time of process
           }process[50];

void main()
  {
   int n=0,i=0,min,pos,elapse=0,quant1=2,quant2=1;
   float avg_wait,avg_turn;

   printf("\tMultilevel Queue with feedback Scheduling\n");
   printf("Queue one using Round robin with time quantum 2 ms\nQueue two using Round robin with time quantum 1 ms\nQueue three using  FCFS");
   printf("\n\nEnter number of processes to be scheduled:\n");
   scanf("%d",&n);
   printf("\nEnter Details of processes:-\n\n");
   	   printf("Enter burst time of process:\n");
   for(i=0;i<n;i++)
      {
	   scanf("%d",&process[i].burst);
	  }
	  printf("Enter arrival time of process:\n");
   for(i=0;i<n;i++)
	  {
	   
	   scanf("%d",&process[i].arrival);
	   process[i].wait=-(process[i].burst);
	   process[i].turn=0;
	  }
	  
   printf("\nEXECUTION:\n");
// Queue one using Round robin with time quantum 2 ms

	i=0;
    while(i<n)
         {
             if(process[i].arrival<=elapse &&process[i].burst>0)//Filter for the process which have arrived and have burst time more than 0
           {
            printf("\nProcess %s executes from t= %d ",process[i].name,elapse);
            if(process[i].burst>quant1) //Process with burst time greater than quantum
              {
               process[i].burst-=quant1;//Burst time decreased by quantum
              elapse+=quant1;  //Elapse time increased by Quantum
              }

        else if(process[i].burst==quant1) //Process with burst time equal to time quantum
               {
                process[i].burst=0; //Burst time is zero
                elapse+=quant1;       //Elapse time is incremented by time quantum
                process[i].end=elapse; //End time is set

               }

        else if(process[i].burst<quant1) //Process with burst time lesss than quantum
                {
                 elapse+=process[i].burst;//Elapse time is incremented by burst time
                 process[i].burst=0;//Burst time is zero
                 process[i].end=elapse;//End time is set
                 }


      }
      i++;//Round robin implemented on all proceses once and then feedbacked to next queue
   }

// Queue two using Round robin with time quantum 1 ms
	i=0;
    while(i<n)
         {
             if(process[i].arrival<=elapse &&process[i].burst>0)//Filter for the process which have arrived and have burst time more than 0
           {
            printf("\nProcess %s executes from t= %d ",process[i].name,elapse);
            if(process[i].burst>quant2) //Process with burst time greater than quantum
              {
               process[i].burst-=quant2;//Burst time decreased by quantum
              elapse+=quant2;  //Elapse time increased by Quantum
              }

        else if(process[i].burst==quant2) //Process with burst time equal to time quantum
               {
                process[i].burst=0; //Burst time is zero
                elapse+=quant2;       //Elapse time is incremented by time quantum
                process[i].end=elapse; //End time is set

               }

        else if(process[i].burst<quant2) //Process with burst time lesss than quantum
                {
                 elapse+=process[i].burst;//Elapse time is incremented by burst time
                 process[i].burst=0;//Burst time is zero
                 process[i].end=elapse;//End time is set
                 }

      }
      i++;//Round robin implemented on all proceses once and then feedbacked to next queue
   }
// Queue three using  FCFS
    i=0;
   while(i<n)
       {
           if(process[i].burst>0)
              {
                printf("\nProcess %s executed from t= %d to t= %d",process[i].name,elapse,elapse+process[i].burst);
                elapse+=process[i].burst;//Elapse is incremented
                process[i].end=elapse;//End time is set

              }
               i++;
         }



  printf("\n\nPROCESS DETAILS:\n");
            printf("\nProcess\t Waiting time\tTurn around time\n");
          for(i=0;i<n;i++)
            {
            process[i].turn=process[i].end-process[i].arrival;
            process[i].wait+=process[i].turn;
             printf("  %s\t\t%d\t\t%d\n",process[i].name,process[i].wait,process[i].turn);
   		     avg_wait+=(float)process[i].wait/n;
   		     avg_turn+=(float)process[i].turn/n;
            }


   printf("\nAverage waiting time : %f",avg_wait);
   printf("\nAverage turn around time: %f\n",avg_turn);

}
