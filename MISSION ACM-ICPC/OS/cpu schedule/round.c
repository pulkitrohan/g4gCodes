#include<stdio.h>
//Structure of the process
struct pro
          {
            char name[10]; //Name of processes
            int burst;     //Burst Time of process
            int arrival;   //Arrival time of process
            int end;       //Eend time of process
            int wait;      //Wait time of process
            int turn;      //Turn around time of process
          }process[10];

 void main()
{

   int n=0,i=0,min,pos,quant,burst_total=0,elapse=0;
   float avg_wait,avg_turn;
   printf("\t  Round Robin Scheduling algorithm\t-By Manish Jain 739/IT/12\n\n\n");
   printf("Enter number of processes to be scheduled:\n");
   scanf("%d",&n);
   printf("\nEnter Details of processes:-\n\n");
   for(i=0;i<n;i++)
      {
	   printf("\nEnter name of process:\n");
	   scanf("%s",process[i].name);
	   printf("Enter burst time of process:\n");
	   scanf("%d",&process[i].burst);
	   printf("Enter arrival time of process:\n");
	   scanf("%d",&process[i].arrival);
	   process[i].wait=-(process[i].burst);
	   process[i].turn=0;
	   burst_total+=process[i].burst;
	   }
	   printf("\nEnter time quantum");
       scanf("%d",&quant);

	   printf("\nEXECUTION:\n");
       i=0;
     while(elapse<burst_total)
       {
        if(process[i].arrival<=elapse &&process[i].burst>0)//Filter for the process which have arrived and have burst time more than 0
           {
            printf("\nProcess %s executes from t= %d ",process[i].name,elapse);
            if(process[i].burst>quant) //Process with burst time greater than quantum
              {
               process[i].burst-=quant;//Burst time decreased by quantum
              elapse+=quant;  //Elapse time increased by Quantum
              }

        else if(process[i].burst==quant) //Process with burst time equal to time quantum
               {
                process[i].burst=0; //Burst time is zero
                elapse+=quant;       //Elapse time is incremented by time quantum
                process[i].end=elapse; //End time is set

               }

        else if(process[i].burst<quant) //Process with burst time lesss than quantum
                {
                 elapse+=process[i].burst;//Elapse time is incremented by burst time
                 process[i].burst=0;//Burst time is zero
                 process[i].end=elapse;//End time is set
                 }

          printf("to t= %d ",elapse);

          }

        i=(i+1)%n; //Round robin between processes
      }


     printf("\n\nPROCESS DETAILS:\n");
            printf("\nProcess\t Waiting time\tTurn around time\n");
          for(i=0;i<n;i++)
            {
             process[i].turn=process[i].end-process[i].arrival;
             process[i].wait+=process[i].turn;
             printf("  %s\t\t%d\t\t%d\n",process[i].name,process[i].wait,process[i].turn);
   		     avg_wait+=(float)process[i].wait/n;
   		     avg_turn+=(float)process[i].turn/n;
            }


   printf("\nAverage waiting time : %f",avg_wait);
   printf("\nAverage turn around time: %f\n",avg_turn);
}
