#include<stdio.h>
// Priority 1 for System process using premptive SRTF
// Priority 2 for Interactive process using RR with time quantum 2 ms
// Priority 3 for Interactive editing process using STF
// Priority 4 for  Batch process using FCFS

//Strucutre of the process
 struct pro{
            char name[10]; //Name of processes
            int burst;     //Burst Time of process
            int arrival;   //Arrival time of process
            int start;     //Starting time of execution of process
            int end;       //Eend time of process
            int wait;      //Wait time of process
            int turn;      //Turn around time of process
            int prior;     //Priorityof Process
   }process[50];

void main()
{
   int n=0,i=0,min,pos,quant=2,total1=0,total2=0,total3=0,total4=0,elapse=0;
   float avg_wait=0,avg_turn=0;

  printf("\t\tMultilevel Queue Scheduling\n");
      printf("Priority 1 for System process using premptive SRTF\nPriority 2 for Interactive process using RR with time quantum 2 ms\nPriority 3 for Interactive editing process using STF\nPriority 4 for  Batch process using FCFS");
      printf("\n0\nEnter number of processes to be scheduled:\n");
      scanf("%d",&n);
      printf("\nEnter Details of processes:-\n");
    for(i=0;i<n;i++)
      {
	   printf("Enter burst time of process:\n");
	   scanf("%d",&process[i].burst);
	}
	for(i=0;i<n;i++)
	{
	   printf("Enter arrival time of process:\n");
	   scanf("%d",&process[i].arrival);
	   process[i].wait=0;
       printf("Enter priority of process:\n");
	   scanf("%d",&process[i].prior);
	   process[i].turn=0;
	}

      for(i=0;i<n;i++) //To find total burst time of individual queues
        {
         if(process[i].prior==1) //For Queue 1
            total1+=process[i].burst;

        else if(process[i].prior==2)// For Queue 2
               total2+=process[i].burst;

        else if(process[i].prior==3)//For Queue 3
                total3+=process[i].burst;

        else if(process[i].prior==4)// Ffor Queue 4
                total4+=process[i].burst;
}
 printf("\nEXECUTION:\n");
// Priority 1 for System process using premptive SRTF QUEUE
   while(elapse<total1)
       {
        min=9999;
   		pos=9999;

   		for(i=0;i<n;i++)//To choose the shortest burst time process
   		{
   		  if(process[i].arrival<=elapse&&process[i].prior==1)//To filter out processes which haven't arrived by that insiant
    		  {
	   		  	if(process[i].burst<min&&process[i].burst>0)//To getthe value of minimum burst time and position of that process
	    		  	{
		   		  	 min=process[i].burst;
		   		  	 pos=i;
	    		  	}
   	     	  }
   		 }


   		  for(i=0;i<n&&(i!=pos);i++)
	   		{
	   		  if(process[i].arrival<=elapse&&process[i].burst>0&&(process[i].prior==1))//If process has arrived and is not executed
	   		  {
		   		  	process[i].wait++;//wait time is incremented
		   		  	process[i].turn++;//Turn around time is incremented
	   		  }
	   	    }

	   	  process[pos].burst--;//For executing process burst is decremented
	   	  process[pos].turn++;//Turn is incremented
   	      elapse++;

         }

//Priority 2 for Interactive process using RR with time quantum 2 ms
      for(i=0;i<n;i++)
          {
           if(process[i].prior==2)
             {
              process[i].wait=-(process[i].burst); //Initialisations for ease of calculations
             }
          }

     i=0;

   while(elapse<(total1+total2))
      {
        if((process[i].arrival<=elapse)&&(process[i].burst>0)&&(process[i].prior==2))//Process with burst time greater than quantum
          {
            if(process[i].burst>quant)
              {
               process[i].burst-=quant;//Burst time decreased by quantum
               elapse+=quant; //Elapse time increased by Quantum
               }

           else if(process[i].burst==quant)//Process with burst time equal to time quantum
                  {
                   process[i].burst=0; //Burst time is zero
                   elapse+=quant;       //Elapse time is incremented by time quantum
                   process[i].end=elapse; //End time is set
                  }

          else if(process[i].burst<quant) //Process with burst time lesss than quantum
                 {
                  elapse+=process[i].burst;//Elapse time is incremented by burst time
                  process[i].burst=0;//Burst time is zero
                  process[i].end=elapse;//End time is set
                 }
            printf("to t= %d ",elapse);

          }

          i=(i+1)%n; //For Round robin between processes
          }


        for(i=0;i<n;i++)
            {
             if(process[i].prior==2)
               {
                process[i].turn=process[i].end-process[i].arrival;
                process[i].wait+=process[i].turn;
                }
            }

// Priority 3 for Interactive editing process using STF
i=0;

   while(elapse<total1+total2+total3)
   {
   		min=9999;
   		pos=9999;
   		for(i=0;i<n;i++)//To choose the shortest burst time process
   		{
   		  if((process[i].prior==3)&&process[i].arrival<=elapse)//To filter out processes which haven't arrived by that instant
   		  {
	   		  	if(process[i].burst<min&&process[i].burst>0) //To get the value of minimum burst time and position of that process
	   		  	{
		   		  	 min=process[i].burst;
		   		  	 pos=i;
	   		  	}
   		  }
   	    }
   		  	process[pos].start=elapse;
   		  	elapse=elapse+process[pos].burst;
   		  	process[pos].end=elapse;
   		  	process[pos].wait=(process[pos].end-process[pos].arrival)-process[pos].burst;
   		  	process[pos].burst=0;
   		  	process[pos].turn=process[pos].end-process[pos].arrival;
   }

// Priority 4 for  Batch process using FCFS
  i=0;

   while(i<n)
        { if(process[i].prior==4)
          {
            elapse+=process[i].burst;
            process[i].end=elapse;
           process[i].turn=process[i].end-process[i].arrival;
           process[i].wait=process[i].turn-process[i].burst;
         }
        i++;
      }


  printf("\n\nPROCESS DETAILS:\n");
            printf("\nProcess\t Waiting time\tTurn around time\n");
          for(i=0;i<n;i++)
            {
             printf("p%d\t\t%d\t\t%d\n",i,process[i].wait,process[i].turn);
   		     avg_wait+=(float)process[i].wait/n;
   		     avg_turn+=(float)process[i].turn/n;
            }


   printf("\nAverage waiting time : %f",avg_wait);
   printf("\nAverage turn around time: %f\n",avg_turn);

}
