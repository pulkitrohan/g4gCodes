#include<stdio.h>
//Process Structure
struct pro{
            char name[10]; //Name of processes
            int burst;     //Burst Time of process
            int arrival;   //Arrival time of process
            int start;     //Starting time of execution of process
            int end;       //Eend time of process
            int wait;      //Wait time of process
            int turn;      //Turn around time of process
         }process[10];

void main()
  {
   int n=0,i=0,min,pos,burst_total=0,elapse=0;
   float avg_wait,avg_turn;
   printf("\t\tSTF Scheduling algorithm\t-By Manish Jain 739/IT/12\n\n\n");
   printf("Enter number of processes to be scheduled:\n");
   scanf("%d",&n);
   printf("\nEnter Details of processes:-\n\n");
   for(i=0;i<n;i++)
      {
	   printf("\nEnter name of process:\n");
	   scanf("%s",process[i].name);
	   printf("Enter burst time of process:\n");
	   scanf("%d",&process[i].burst);
	   printf("Enter arrival time of process:\n");
	   scanf("%d",&process[i].arrival);
	   burst_total+=process[i].burst;
	   }
	   printf("\nEXECUTION:\n");
    while(elapse<burst_total)
       {
   		min=9999;
   		pos=9999;
   		for(i=0;i<n;i++) //To choose the shortest burst time process
   		{
   		  if(process[i].arrival <= elapse) //To filter out processes which haven't arrived by that instant
   		  {
	   		  	if(process[i].burst < min && process[i].burst > 0) //To get the value of minimum burst time and position of that process
	   		  	{
		   		  	 min=process[i].burst;
		   		  	 pos=i;
	   		  	}
   		  }
   	    }
   		  	process[pos].start=elapse;
   		  	elapse=elapse+min;
   		  	process[pos].end=elapse;
   		  	process[pos].wait=(process[pos].start-process[pos].arrival);
   		  	process[pos].burst=0;
   		  	process[pos].turn=process[pos].end-process[pos].arrival;
   		  	printf("\nProcess  %s executed from t= %d to t=%d\n",process[pos].name,process[pos].start,process[pos].end);


   }

            printf("\n\nPROCESS DETAILS:\n");
            printf("\nProcess\t Waiting time\tTurn around time\n");
          for(i=0;i<n;i++)
            {
             printf("  %s\t\t%d\t\t%d\n",process[i].name,process[i].wait,process[i].turn);
   		     avg_wait+=(float)process[i].wait/n;
   		     avg_turn+=(float)process[i].turn/n;
            }


   printf("\nAverage waiting time : %f",avg_wait);
   printf("\nAverage turn around time: %f\n",avg_turn);



  }



