//Header files
#include<stdio.h>

//structure definations:

//Strucutre of memory hole
struct hole
 {
  int hsize; //Hole size
  struct pro* ptr;//Pointer to first allocated process
 };

//structure of process
  struct pro
 {
  char name[5]; //Process name
  int psize;     //Process size
  struct pro* link; //Pointer to the process allocated to the same hole
 };

//Main
 void main()
 {
  int nh=0,i,size,ch2;
  char ch1;
  printf("\t\t\tMemory Mangaegement  Algorithms\n\n");
  printf("  First Fit,Best Fit and Worst Fit Algorithms\t\tBy Manish Jain 739/IT/12\n\n\n");
   printf("Details of  holes :\n\n");
  printf("Enter the no. of holes  ");
  scanf("%d",&nh);
  printf("\n");
  struct hole arr[nh];
  for(i=0;i<nh;i++) //Tto enter size of each hole
  {
   printf("\nEnter size of hole %d\n",i+1);
     scanf("%d",&arr[i].hsize);
     arr[i].ptr=NULL;
  }
  do
    {

     struct pro* temp = (struct pro*)malloc(sizeof(struct pro)); //To genetare a new process  data element
     printf("\n\nEnter process name\n");
     scanf("%s",temp->name);
     printf("Enter the size of process\n");
     scanf("%d",&temp->psize);
     temp->link=NULL;
     fflush(stdin);
     printf("\nEnter choice:\n1)First fit\n2)Best fit\n3)worst fit\n\n");
     scanf("%d",&ch2);
     switch(ch2)
     {
     case 1 :{
               int flag=0;  //First Fit algorithm
               for(i=0;i<nh;i++)
                  {
                   if(temp->psize<=arr[i].hsize) //Comparing size of process with i th hole to find the first hole large enough to fit the process
                     {
                       arr[i].hsize-=temp->psize;

                      if(arr[i].ptr!=NULL) //If one or more process is already alloted to selected hole
                      {
                        struct pro* temp2;
                        temp2=arr[i].ptr;
                        while(temp2->link!=NULL)
                              temp2=temp2->link;
                        temp2->link=temp;
                      }

                     else                 //First process to be alloted to selected hole
                        arr[i].ptr=temp;
                     printf("Process %s allocated to hole %d\n",temp->name,i+1);
                     flag=1;
                     break;
                   }
               }

              if(flag!=1)
              {
                  printf("Not given due to memory shortage\n");
              }
              break;
               }
    case 2 : {  //Best Fit algorithm
              int min,pos;
              min=9999;
              pos=-1;
              for(i=0;i<nh;i++)
              {
                 if(temp->psize<=arr[i].hsize&&arr[i].hsize<min)//To find the whole with minimum size to be able to fit theprocess
                 {
                     min=arr[i].hsize;
                     pos=i;
                 }
             }
              if(pos==-1)
                  printf("Not given due to memory shortage\n");
              else
              {
                      arr[pos].hsize-=temp->psize;
                     if(arr[pos].ptr!=NULL) //If one or more process is already alloted to selected hole
                      {
                        struct pro* temp2;
                        temp2=arr[pos].ptr;
                        while(temp2->link!=NULL)
                              temp2=temp2->link;
                        temp2->link=temp;
                      }

                     else  //First process to be alloted to selected hole
                        arr[pos].ptr=temp;
                     printf("Process %s allocated to hole %d\n",temp->name,pos+1);
                   }
                   break;
           }
case 3 :   { //Worst fir algorithm
             int max,pos1;
             max=0;
             pos1=-1;
             for(i=0;i<nh;i++)
             {
                 if(temp->psize<=arr[i].hsize&&arr[i].hsize>max)//To find the hole with maximum remaining holespac to fit the process
                 {
                     max=arr[i].hsize;
                     pos1=i;
                 }
             }

              if(pos1==-1)
                  printf("Not given due to memory shortage\n");
              else
              {
                      arr[pos1].hsize-=temp->psize;
                     if(arr[pos1].ptr!=NULL)  //If one or more process is already alloted to selected hole
                      {
                        struct pro* temp2;
                        temp2=arr[pos1].ptr;
                        while(temp2->link!=NULL)
                              temp2=temp2->link;
                        temp2->link=temp;
                      }

                     else    //First process to be alloted to selected hole
                        arr[pos1].ptr=temp;
                  printf("Process %s allocated to hole %d\n",temp->name,pos1+1);
                   }
                   break;
                    }
 }
    printf("\nDetails of Holes:\n");
     for(i=0;i<nh;i++) //Details for each hole
  {   int sz;
     printf("\n\nHole %d\n",i+1);
     struct pro* k=arr[i].ptr;
     while(k!=NULL)
     {
         printf(" Allocated to process %s of size  : %d\n",k->name,k->psize);
         k=k->link;
     }
     printf(" Remaining space in hole %d: %d\n",i+1,arr[i].hsize);
   }

  printf("\nDo you wanna enter more??(y/Y)\n");
  fflush(stdin);
  scanf("%c",&ch1);
  }while(ch1=='y');
}
