#include<stdio.h>
#include<stdlib.h>
//max[i][j]=k,then process Pi may atmost request kinstances of resource type Rj
//alloc[i][j]=k,then process Pi is currenty allocated k instances of resources Rj
//avail[j]=k,then there are k instances of resource Rj available
//need[i][j]=k,then Pi mayneed k more instances of resource type Rj to complete task
//complete[i]=0 for incomplete process else complete[i]=1
//safesequence array stores a safe sequence of process execution
  int main()
 {
    int max[20][20],need[20][20],alloc[20][20],avail[20],complete[20],safesequence[20];
    int p,r,i,j,process,count=0;
    printf("\t\tBanker's Algorithm\t\tBy Manish Jain 739/IT/12\n\n");
    printf("Enter the no. of processes to be executed: ");
    scanf("%d",&p);

      for(i=0;i<p;i++)  //initialisation ofcompete array to '0'
      complete[i]=0;

      printf("\n\nEnter the no. of resource types : ");
      scanf("%d",&r);

      printf("\n\nEnter the Maximum matrix for each process : ");

      for(i=0;i<p;i++) //Input maximum array
        {
         printf("\nFor process %d : \n",i+1);
         for(j=0;j<r;j++)
            scanf("%d",&max[i][j]);
        }


       printf("\n\nEnter the Allocation matrix for each process : "); //Input allocation array

       for(i=0;i<p;i++)
         {
          printf("\nFor process %d : \n",i+1);
          for(j=0;j<r;j++)
            {
             scanf("%d",&alloc[i][j]);
             need[i][j]=max[i][j]-alloc[i][j]; //needed resources =maximum resources- allocated resources
            }
         }

        printf("\n\nEnter the available resources : \n");
        for(i=0;i<r;i++) //Input available instances of each resource types
        scanf("%d",&avail[i]);

//Execution starts
       do
          {
           printf("\nMaximum matrix \tAllocation matrix \n");
          for(i=0;i<p;i++)
           {
            printf("    ");

            for(j=0;j<r;j++)
            printf(" %d",max[i][j]);

            printf("\t      ");

            for(j=0;j<r;j++)
            printf(" %d",alloc[i][j]);

           printf("\n");
           }

         process=-1;
       for(i=0;i<p;i++)
        {
         if(complete[i] == 0) //if process is not completed
          {
             process=i;
          for(j=0;j<r;j++)
           {
            if(avail[j]<need[i][j]) //if available resources are less then neede then break out
             {
                process=-1;
                break;
             }
           }
          }

          if(process!=-1)
            break;
       }

       if(process!=-1)
        {
         printf("\n Process %d runs to completion ",process+1);
         safesequence[count]=process+1;
         count++;
          //process is executed
         for(j=0;j<r;j++)
          {
            avail[j]+=alloc[process][j]; //available resources is incremented by allocated resources of process
            alloc[process][j]=0; //Resources instance of type Rj allocated to process is set to zero after execution
            max[process][j]=0;//Maximum resource instances oftype Rj of process is set to zero
            complete[process]=1;//Process is completed
          }

       }
    }

     while(count!=p&&process!=-1);
     if(count==p)//All processes are executed and no deadlock ,hence safe
       {
        printf("\n The system is in safe state\n ");
        printf("Safe sequence : ");

        for(i=0;i<p;i++)
         printf("%d ",safesequence[i]);
         printf("\n");
       }
     else
       printf("\nThe System is in unsafe state");
    return 0;
  }
