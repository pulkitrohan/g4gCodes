//Header Files
#include<stdio.h>

//Global Iniatialisations
int front=-1;
int rear=-1;

//Function to enqueue a queue
void enqueue(int a[],int ele,int n)
  {
    if(rear==-1)
    {front=0;
    rear=0;
    }
    else if(rear==n-1)
        rear=0;
    else
        rear++;
    a[rear]=ele;
  }

//Function to dequeue a queue
void dequeue(int a[],int n)
  {
    if(front==rear)
    {front=-1;
    rear=-1;
    }
    if(front==n-1)
        front=0;
    else
        front++;

  }

//Function to search an element in a queue and return its position
int search(int a[],int ele,int n)
 {
    int i;
    if(front==-1)//Empty queue
        return -1;

    if(front<=rear)
        {
         for(i=front;i<=rear;i++)
            {
             if(a[i]==ele)
                return i;
            }
        }

    else
        {
          for(i=front;i<=n-1;i++)
            {
              if(a[i]==ele)
                return i;
             }
         for(i=0;i<=rear;i++)
             {
              if(a[i]==ele)
                return i;
             }
        }

  return -1;//Element not found
}
//Main Function
void main()
 {  int nr=0,nf=0,i,faults=0;

    printf("\tFIFO Page Replacement Algorithm \t By Manish Jain 739/IT/12\n\n");
    printf("Enter the number of elements in the reference string\n");
    scanf("%d",&nr);
    int ref[nr];

    printf("\nEnter the elements of the reference string\n");
    for(i=0;i<nr;i++)
       scanf("%d",&ref[i]);//Input reference string

     printf("\nEnter the number of available frames\n");
     scanf("%d",&nf);

     int frames[nf];
     for(i=0;i<nf;i++)
        frames[i]=-1; //initialisation of frames

      printf("\nPage");

      for(i=0;i<nf;i++)
         printf("\tFrame %d",i+1);
         printf("\n");

     for(i=0;i<nr;i++)
       {
         int j;
        printf("\n%d",ref[i]);
        for(j=0;j<nf;j++)
         {
          if(frames[j]==-1)
             printf("\t ");
          else
             printf("\t%d",frames[j]);
          }

         printf("\n");

        if(search(frames,ref[i],nf)==-1) //Demamnded page is not found in frames
          {
           if(((rear-front)==nf-1)||(front==(rear+1))) //Frames queue is full
             {
              dequeue(frames,nf); //Dequeue a page from queue
              }
                enqueue(frames,ref[i],nf); //Enqueue demanded page
                faults++;
            }

     }
printf("\n\nNumber of Page Faults : %d",faults);
 }
