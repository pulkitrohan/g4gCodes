//Header  Files
#include<stdio.h>

//Global Iniatialisation
int top=-1;

//Function to find the distance of next appearance of a page in reference string in future
int optim(int a[],int ele,int start,int n)
 {
     int i;
         for(i=start;i<n;i++)
             {
              if(a[i]==ele)
                return (i-start+1);
             }

  return 9999 ;
}

//Function to Search a particular element in ag given array and return its position
int search(int a[],int ele)
 {
    int i;
   for(i=0;i<=top;i++)
    {
     if(a[i]==ele)
     return i;
    }
   return -1;
 }

//Main Function
void main()
 {
     int nr=0,nf=0,i,faults=0;
     printf("\tOptimum Page Replacement Algorithm\t By Manish Jain 739/IT/12\n\n");
    printf("Enter the number of elements in the reference string\n");
    scanf("%d",&nr);
    int ref[nr];

    printf("\nEnter the elements of the reference string\n");
    for(i=0;i<nr;i++)
       scanf("%d",&ref[i]);//Input reference string

     printf("\nEnter the number of frames\n");
     scanf("%d",&nf);
     int frames[nf];
     for(i=0;i<nf;i++)
        frames[i]=-1; //Iniatialisation of frame array

     printf("\nPage");
      for(i=0;i<nf;i++)
         printf("\tFrame %d",i+1);
         printf("\n");


     for(i=0;i<nr;i++)
       {
         int j;
         printf("\n%d",ref[i]);
         for(j=0;j<nf;j++)
          {
            if(frames[j]==-1)
               printf("\t ");
            else
               printf("\t%d",frames[j]);
          }

         printf("\n");

        if(search(frames,ref[i])==-1) //Demanded page is not found in frames
       {
        if(top==nf-1) //Frame queue is full
         {
            int j,max=0,pos=-1;

           for(j=0;j<nf;j++)
            {
             if(optim(ref,frames[j],i+1,nr)>max)//Searching for page in frames which is to be used at the last between all frames in future
                 {
                  max=optim(ref,frames[j],i+1,nr);
                  pos=j;
                 }
             }
           frames[pos]=ref[i]; //Swap demanded page in frames array
         }

        else
            {
                frames[++top]=ref[i]; //Pushing demanded page in frames array
            }

                faults++;
         }


      }

     printf("\n\nNumber of Page Faults: %d",faults);


}
