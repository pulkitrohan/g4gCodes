int N,empty,in=0,item,mutex=1,full=0,int s;

void wait(int s)
{
	if(s < 0)
	exit(0);
	s--;
}

void signal(int s)
{
	s++;
}

void Producer()
{
	do
	{
		wait(empty);
		wait(mutex);
		printf("Enter the item : "):
		scanf("%d",&item);
		buffer[in++] = item;
		signal(mutex);
		signal(full);
	}
		while(in < N);
}

int main(void)
{
	printf("Enter the value of n : ")
	scanf("%d",&N);
	empty = N;
	while(in < N)
		Producer();
