#include<bits/stdc++.h>
using namespace std;

int main(void)
{
	int A[] = {1,2,5,-7,2,5};
	int N = sizeof(A)/sizeof(A[0]);
	int sum = 0,maxsum = INT_MIN,local_start =0,finish = -1,start = -1;
	for(int i=0;i<N;i++)
	{
		if(A[i] >= 0)
		{
			sum += A[i];
			if(sum > maxsum)
			{
				maxsum = sum;
				start = local_start;
				finish = i;
			}
		}
		else
		{
			sum = 0;
			local_start = i+1;
		}
	}
	if(finish == -1)
		cout<<"No subarray with positive integer\n";
	else
		for(int i=start;i<=finish;i++)
			cout<<A[i]<<" ";
	return 0;
}