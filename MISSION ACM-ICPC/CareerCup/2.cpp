#include<bits/stdc++.h>
using namespace std;


int translate(int N)
{
	int ans = 0;
	for(int i=32;i>0;i--)
	{
		int mask = 1 << (i-1);
		ans = ans*10;
		if(mask & N)
			ans += 9;
	}
	return ans;
}
			

int main(void)
{
	int N = 10;
	int bin = 1;
	while(1)
	{
		int res = translate(bin);
		if(res % N == 0)
		{
			cout<<res<<endl;
			break;
		}
		bin++;
	}
	return 0;
}