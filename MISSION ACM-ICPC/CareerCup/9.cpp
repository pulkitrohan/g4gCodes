//http://stackoverflow.com/questions/2277749/calculate-the-sum-of-elements-in-a-matrix-efficiently
//http://www.careercup.com/question?id=5726362876772352
#include<bits/stdc++.h>
using namespace std;

int main(void)
{
    int N,M,i,j;
    scanf("%d %d",&N,&M);
    int A[100][100];
    for(i=0;i<N;i++)
        for(j=0;j<M;j++)
            scanf("%d",&A[i][j]);
    int S[100][100];
    S[0][0] = A[0][0];
    for(i=1;i<M;i++)
        S[0][i] = A[0][i] + S[0][i-1];
    for(j=1;j<N;j++)
        S[j][0] = A[j][0] + S[j-1][0];
    for(i=1;i<N;i++)
        for(j=1;j<M;j++)
            S[i][j] = A[i][j] + S[i-1][j] + S[i][j-1] - S[i-1][j-1];
	int x1,x2,y1,y2;
    cin>>x1>>y1>>x2>>y2;
	if(x1 > x2 || y1 > y2)
		return -1;
	if(x1 < 0 || x1 >= N || y1 < 0 || y1 >= M || x2 < 0 || x2 >= N || y1 < 0 || y2 >= M)
		return -1;
	int sum = S[x2][y2];
	if(x1-1 >= 0)
		sum -= S[x1-1][y2];
	if(y1-1 >= 0)
		sum -= S[x2][y1-1];
	if(x1-1 >= 0 && y1-1 >= 0)
		sum += S[x1-1][y1-1];
	cout<<sum;
    return 0;
}
