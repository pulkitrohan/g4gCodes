#include<stdio.h>

int max(int A,int B)
{
    return ((A > B) ? A : B);
}

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
};

int MaxSumSubset(struct Tree *root)
{
    if(!root)
        return 0;
    if(!root->left && !root->right)
        return root;
    int x = root->data + MaxSumSubset(root->left->left) + MaxSumSubset(root->left->right) + MaxSumSubset(root->right->left)+MaxSumSubset(root->right->right);
    int y = MaxSumSubset(root->left)+MaxSumSubset(root->right);
    return max(x,y);
}

int main(void)
{
    struct Tree *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    root->right->right = CreateNode(6);
    root->right->right->right = CreateNode(100);
    printf("%d",MaxSumSubset(root));
    return 0;
}
