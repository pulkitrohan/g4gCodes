 #include<bits/stdc++.h>
using namespace std;

void Rearrange(int M[][4],int R,int C)
{
	int newrow = 0,newcol = 0;
	if(R == 0 && C == 0)
	{
		M[R][C] = INT_MIN;
		return;
	}
	else if(R == 0)
	{
		newrow = R;
		newcol = C-1;
	}
	else if(C == 0)
	{
		newrow = R-1;
		newcol = C;
	}
	else if(M[R][C-1] > M[R-1][C])
	{
		newrow = R;
		newcol = C-1;
	}
	else
	{
		newrow = R-1;
		newcol = C;
	}
	M[R][C] = M[newrow][newcol];
	Rearrange(M,newrow,newcol);
}

int FindKthLargest(int M[][4],int k)
{
	for(int i=0;i<k-1;i++)
		Rearrange(M,3,3);
	return M[3][3];
}

int main(void)
{
	int M[][4] = {{5,7,8,9},{6,9,10,13},{7,11,12,15},{8,13,16,17}};
	cout<<FindKthLargest(M,8);
	return 0;
}