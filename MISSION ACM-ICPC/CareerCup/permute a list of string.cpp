#include<bits/stdc++.h>
using namespace std;

void permutate(vector<string> &V,int index,string S)
{
	if(index == V.size())
		cout<<S<<endl;
	else
	{
		string A = V[index];
		for(int i=0;i<V.size();i++)
			permutate(V,index+1,S + A[i]);
	}
}

int main(void)
{
	vector<string> V;
	V.push_back("red");
	V.push_back("fox");
	V.push_back("super");
	permutate(V,0,"");
	return 0;
}