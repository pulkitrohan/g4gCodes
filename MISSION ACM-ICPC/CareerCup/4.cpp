#include<bits/stdc++.h>
using namespace std;

int FindMaxIndex(int *A,int start,int end)
{
	int index = start;
	for(int i=start+1;i<end;i++)
	{
		if(A[i] > A[index])
			index = i;
	}
	return index;
}

int main(void)
{
	int A[] = {1,5,2,9,3,7,2,8,9,3};
	int N = sizeof(A)/sizeof(A[0]);
	int swaps = 5;
	for(int j=0;j<N && swaps > 0;j++)
	{	
		int max_index = FindMaxIndex(A,j,j+swaps);
		if(j != max_index)
			swap(A[j],A[max_index]);
		swaps -= abs(max_index-j);
	}
	for(int i=0;i<N;i++)
		cout<<A[i]<<" ";
	return 0;
	
}