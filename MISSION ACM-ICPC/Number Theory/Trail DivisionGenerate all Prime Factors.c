#include<stdio.h>
#include<math.h>
int main(void)
{
    int n,k,i;
    printf("Enter the Number : ");
    scanf("%d",&n);
    while(n%2 == 0)
    {
        printf("2");
        n /= 2;
    }
    for(i = 3 ; i <= sqrt(n) ; i = i+2)
    {
        while( n % i == 0)
        {
            printf("%d ",i);
            n /=i;
        }
    }
    if(n > 1)       //For prime numbers
        printf("%d ",n);
    return 0;
}
