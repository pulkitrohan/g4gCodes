#include<bits/stdc++.h>
#define MOD 100;
#define LL long long
int main(void)
{
	int a = 2,b = 7;
	LL ans = 1;
	while(b)
	{
	    if(b & 1)
            ans = (ans * a)%MOD;
		a = (a*a)%MOD;
		b >>=1;
	}
	printf("%d\n",ans);
	return 0;
}
