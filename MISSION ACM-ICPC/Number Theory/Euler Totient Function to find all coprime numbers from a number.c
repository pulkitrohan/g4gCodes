/*
    The number of positive integers, not greater than n, and relatively prime with n, equals to Euler’s
    totient function φ (n). In symbols we can state that
        φ (n) ={a Î N: 1 ≤ a ≤ n, gcd(a, n) = 1}

This function has the following properties:

If p is prime, then φ (p) = p – 1 and φ (pa) = p a * (1 – 1/p) for any a.
If m and n are coprime, then φ (m * n) = φ (m) * φ (n).
If n = , then Euler function can be found using formula:
φ (n) = n * (1 – 1/p 1) * (1 – 1/p 2) * ... * (1 – 1/p k) */

#include<stdio.h>
int main(void)
{
    int n,i;
    printf("Enter the number whose no. of coprimes are to be found : ");
    scanf("%d",&n);
    int result = n;
    for(i=2;i*i<=n;i++)
    {
        if(n%i == 0)
        {
			result -=result/i;
			while(n%i == 0)
				n /=i;
		}
    }
    if(n > 1)
        result -=result/n;
    printf("Count : %d",result);
    return 0;
}
