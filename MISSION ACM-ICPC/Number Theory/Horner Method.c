#include<stdio.h>
int main(void)
{
    int N,i,sum,X;
    printf("Enter the Degree of the Polynomial : ");
    scanf("%d",&N);
    int A[N+1];
    printf("Enter the coefficients of the Polynomial : ");
    for(i=N;i>=0;i--)
        scanf("%d",&A[i]);
    printf("Enter the value of x : ");
    scanf("%d",&X);
    sum = 0;
    for(i=N;i>0;i--)
    {
        sum = (sum + A[i])*X;
    }
    sum += A[0];
    printf("Value of the Polynomial : %d",sum);
}
