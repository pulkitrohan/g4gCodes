#include<bits/stdc++.h>
using namespace std;
#define MAX 1000000
#define LL long long
#define vi vector<int>
bool bs[MAX+1];
vi primes;
void sieve(void)
{
	memset(bs,1,sizeof(bs));
	for(int i=2;i<=MAX;i++)
	{
		if(bs[i])
		{
			for(int j=i*i;j<=MAX;j+=2)
				bs[j] = 0;
		}
		primes.push_back(i);
	}
}

void PrimeFactors(LL N)
{
	vi factors;
	LL index = 0;
	LL PF = primes[index];
	while(PF*PF <=N)
	{
		while(N % PF == 0)
		{
			factors.push_back(PF);
			N /= PF;
		}
		PF = primes[++index];
	}
	if(N != 1)
		factors.push_back(N);
	for(int i=0;i<factors.size();i++)
		printf("%d ",factors[i]);
	printf("\n");
}

int CountPrimeFactors(LL N)
{
	int index = 0,count = 0;
	int PF = primes[index];
	while(PF*PF <=N)
	{
		while(N % PF == 0)
		{
			count++;
			N /= PF;
		}
		PF = primes[++index];
	}
	if(N != 1)
		count++;
	return count;
}

int CountDiffPrimeFactors(LL N)
{
	int index = 0,flag,count = 0;
	int PF = primes[index];
	while(PF*PF <=N)
	{
		flag = 1;
		while(N % PF == 0)
		{
			if(flag)
				count++;
			N /= PF;
			flag = 0;
		}
		PF = primes[++index];
	}
	if(N != 1)
		count++;
	return count;
}

LL SumPrimeFactors(LL N)
{
	int index = 0;
	int PF = primes[index];
	LL sum = 0;
	while(PF*PF <=N)
	{
		while(N % PF == 0)
		{
			sum += PF;
			N /= PF;
		}
		PF = primes[++index];
	}
	if(N != 1)
		sum += N;
	return sum;
}

int CountDivisors(LL N)
{
	int index = 0,count,ans = 1;
	int PF = primes[index];
	while(PF*PF <=N)
	{
		count = 0;
		while(N % PF == 0)
		{
			count++;
			N /= PF;
		}
		PF = primes[++index];
		ans = ans *(count+1);
	}
	if(N != 1)
		ans *= 2;
	return ans;
}

LL SumDivisors(LL N)
{
	int index = 0,count;
	int PF = primes[index];
	LL ans = 1;
	while(PF*PF <=N)
	{
		count = 0;
		while(N % PF == 0)
		{
			count++;
			N /= PF;
		}
		ans = ans * ((LL)(pow(PF,count+1))-1)/(PF-1);
		PF = primes[++index];
		
	}
	if(N != 1)
		ans *= (LL)(pow(N,2)-1)/(N-1);
	return ans;
}

int EulerTotient(LL N)
{
	int index = 0;
	int PF = primes[index];
	LL ans = N;
	while(PF*PF <=N)
	{
		if(N % PF == 0)
		{
			ans -= ans/PF;
			while(N % PF == 0)
				N /= PF;
		}
		PF = primes[++index];
	}
	if(N != 1)
		ans -= ans/N;
	return ans;
}


int main(void)
{
	sieve();
	int N = 36;
	printf("Prime Factors of %d : ",N);
	PrimeFactors(N);
	printf("Total Prime Factors of %d : %d\n",N,CountPrimeFactors(N));
	printf("Sum of Prime Factors of %d : %lld\n",N,SumPrimeFactors(N));
	printf("Distinct Prime Factors of %d : %d\n",N,CountDiffPrimeFactors(N));
	printf("Total Divisors of %d : %d\n",N,CountDivisors(N));
	printf("Sum of Divisors of %d : %lld\n",N,SumDivisors(N));
	printf("Number of coprimes of %d : %d\n",N,EulerTotient(N));
	return 0;
}