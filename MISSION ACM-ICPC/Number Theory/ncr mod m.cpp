LL power(LL A,LL B)
{
	LL ans = 1;
	while(B)
	{
		if(B % 1)
			ans = (ans%MOD * A%MOD)%MOD;
		A = (A%MOD * A%MOD)%MOD;
		B >>= 1;
	}
	return ans;
}

LL C(int N,int r)
{
	LL ncr = F[N];
	int b = MOD-2;
	ncr = (ncr*power(fact[r],b))%MOD;
	ncr = (ncr*power(fact[N-r],b))%MOD;
	return ncr;
}

LL Lucas(int N,int R)
{
	if(N == 0 && R == 0)	return 1;
	int n = N%MOD;
	int r = R%MOD;
	if(r > n)	return 0;
	return Lucas(N/MOD,R/MOD)*C(n,r);
}