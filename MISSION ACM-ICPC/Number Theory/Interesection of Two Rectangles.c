#include<stdio.h>

int max(int A,int B)
{
    return (A > B) ? A : B;
}

int min(int A,int B)
{
    return (A > B) ? B : A;
}
int main(void)
{
    int x1,x2,x3,x4,y1,y2,y3,y4,X1,Y1,X2,Y2;
    printf("Enter the Coordinates of Bottom Left Side of First Rectangle : ");
    scanf("%d %d",&x1,&y1);
    printf("Enter the Coordinates of Upper Right Side of First Rectangle : ");
    scanf("%d %d",&x2,&y2);
    printf("Enter the Coordinates of Bottom Left Side of Second Rectangle : ");
    scanf("%d %d",&x3,&y3);
    printf("Enter the Coordinates of Upper Right Side of Second Rectangle : ");
    scanf("%d %d",&x4,&y4);

    if(max(x1,x3) > min(x2,x4) || max(y1,y3) > min(y2,y4))
        printf("Not Intersecting\n");
    else
    {
        X1 = max(x1,x3);
        Y1 = max(y1,y3);
        X2 = min(x2,x4);
        Y2 = min(y2,y4);
    }
    printf("Coordinates for Bottom Left Side : (%d,%d)\n",X1,Y1);
    printf("Coordinates for Upper Right Side : (%d,%d)\n",X2,Y2);
    return 0;
}
