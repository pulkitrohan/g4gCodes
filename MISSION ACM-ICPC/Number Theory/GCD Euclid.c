#include<stdio.h>
int main(void)
{
    int a,b,k;
    printf("Enter the numbers whose GCD is to be found : ");
    scanf("%d %d",&a,&b);
    int c = b,lcm;
    while(b != 0)
    {
        k = a%b;
        a = b;
        if(k != 0)
        b = k;
        else
        {
            k = b;
            b = 0;
        }
    }
    lcm = (a*c)/k;
    printf("GCD : %d\nLCM : %d",k,lcm);
    return 0;
}
