/*Goldbach's Conjecture:
For any integer n (n ≥ 4) there exist two prime numbers p1 and p2 such that p1 + p2 = n. */
#include<stdio.h>
#define N 32768
int a[N];
int findPairs(int p)
{
    int count =0,i;
    if(a[p])
        return 0;
    for(i=2;i<=p/2;i++)
        if(a[i] && a[p-i])
        {
            count++;
            printf("(%d,%d) ",i,p-i);
        }
    return count;
}
int main(void)
{
    int i,j,count = 0;
    for(i=2;i<N;i++)
        a[i] = 1;
    for(i=2;i<N;i++)
    {
        if(a[i])
         for(j=i;i*j<N;j++)
            a[i*j] = 0;
    }
    for(i=0;i<100;i++)
    {

        printf("%02d %2d\n",i,findPairs(i));
    }
return 0;
}
