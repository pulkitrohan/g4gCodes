#include<cstdio>
#include<cmath>
#include<algorithm>

using namespace std;

void SumClosestToZero(int *A,int N)
{
    int i = 0,j = N-1,l,r,min_sum = 10000,sum;
    while(j > i)
    {
        sum = A[i] + A[j];
        if(abs(sum) < abs(min_sum))
        {
            min_sum = sum;
            l = i;
            r = j;
        }
        if(sum < 0)
            i++;
        else
            j--;
    }
    printf("The two whose sum is closest to zero are %d,%d",A[l],A[r]);
}


int main(void)
{
    int N,i;
    printf("Enter the size of Array : ");
    scanf("%d",&N);
    int A[N+1];
    if(N)
    printf("Enter the Array Elements : ");
    for(i=0;i<N;i++)
        scanf("%d",&A[i]);
    sort(A,A+N);
    SumClosestToZero(A,N);

    return 0;
}
