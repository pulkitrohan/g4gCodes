#include<stdio.h>
int FirstZeroIndex(int *A,int low,int high)
{
    if(high >= low)
    {
        int mid = low + (high - low)/2;
        if(mid == 0 || (A[mid-1] == 1 && A[mid] == 0))
            return mid;
        if(A[mid] == 1)
            return FirstZeroIndex(A,mid+1,high);
        else
            return FirstZeroIndex(A,low,mid-1);
    }
    return -1;
}

int CountZeroes(int *A,int N)
{
    int k = FirstZeroIndex(A,0,N-1);
    if(k == -1)
        return 0;
    else
        return N-k;
}

int main(void)
{
    int N,i;
    printf("Enter the size of Array : ");
    scanf("%d",&N);
    int A[N+1];
    if(N)
    printf("Enter the Array Elements : ");
    for(i=0;i<N;i++)
        scanf("%d",&A[i]);
    printf("\nTotal Zeroes : %d",CountZeroes(A,N));
    return 0;
}
