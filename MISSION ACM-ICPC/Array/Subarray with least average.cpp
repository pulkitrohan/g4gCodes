#include<bits/stdc++.h>
using namespace std;

void MinAvgSubarray(int *A,int N,int k)
{
	if(N < k)
		return ;
	int res_index = 0;
	int cur_sum = 0;
	for(int i=0;i<k;i++)
		cur_sum += A[i];
	int min_sum = cur_sum;
	for(int i=k;i<N;i++)
	{
		cur_sum += A[i]-A[i-k];
		if(cur_sum < min_sum)
		{
			min_sum = cur_sum;
			res_index = i-k+1;
		}
	}
	cout<<res_index<<" "<<res_index+k-1;
}

int main(void)
{
	int A[] = {3,7,90,20,10,50,40};
	int k = 3;
	int N = sizeof(A)/sizeof(A[0]);
	MinAvgSubarray(A,N,k);
	return 0;
}