#include<bits/stdc++.h>
using namespace std;

int findMinSum(int A[],int B[],int N)
{
	sort(A,A+N);
	sort(B,B+N);
	int ans = 0;
	for(int i=0;i<N;i++)
		ans += abs(A[i]-B[i]);
	return ans;
}

int main(void)
{
	int A[] = {4,1,8,7};
	int B[] = {2,3,6,5};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<findMinSum(A,B,N);
	return 0;
}