#include<bits/stdc++.h>
using namespace std;

int SmallestSubArraywithSumGreater(int *A,int N,int x)
{
	int start = 0,end = 0,sum = 0;
	int len = INT_MAX;
	while(end < N)
	{
		while(sum <= x && end < N)
			sum += A[end++];
		
		while(sum > x && start <= end)
		{
			len = min(len,end-start);
			sum -= A[start++];
		}
	}
	return len;
}

int main(void)
{
	int A[] = {1,45,1,46,1,45,1};
	int x = 46;
	int N = sizeof(A)/sizeof(A[0]);
	cout<<SmallestSubArraywithSumGreater(A,N,x)<<endl;
	return 0;
}