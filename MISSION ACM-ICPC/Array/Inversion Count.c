#include<stdio.h>
#include<stdlib.h>

int MergeSort(int *A,int *temp,int left,int right)
{
    int inv_count = 0;
    if(right > left)
    {
        int mid = (left + right)/2;
        inv_count = MergeSort(A,temp,left,mid);
        inv_count += MergeSort(A,temp,mid+1,right);
        inv_count += Merge(A,temp,left,mid+1,right);
    }
    return inv_count;
}

int Merge(int *A,int *temp,int left,int mid,int right)
{
    int i = left,j = mid,k = left,inv_count = 0;
    while(i <= mid-1 && j<= right)
    {
        if(A[i] <= A[j])
            temp[k++] = A[i++];
        else
        {
            temp[k++] = A[j++];
            inv_count += (mid - i);
        }
    }
    while(i <= mid -1)
        temp[k++] = A[i++];
    while(j <= right)
        temp[k++] = A[j++];
    for(i=left;i<=right;i++)
        A[i] = temp[i];
    return inv_count;
}
int main(void)
{
    int A[] = {5,4,3,2,1};
    int N = sizeof(A)/sizeof(A[0]);
    int *temp = (int *)malloc(sizeof(int) * N);
    printf("Inversion : %d\n",MergeSort(A,temp,0,N-1));
    return 0;
}
