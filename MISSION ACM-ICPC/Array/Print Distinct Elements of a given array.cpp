#include<bits/stdc++.h>
using namespace std;

void PrintDistinctElements(int *A,int N)
{
	set<int> S;
	for(int i=0;i<N;i++)
	{
		if(S.find(A[i]) == S.end())
		{
			cout<<A[i]<<" ";
			S.insert(A[i]);
		}
	}
}

int main(void)
{
	int A[] = {10,5,3,4,3,5,6};
	int N = sizeof(A)/sizeof(A[0]);
	PrintDistinctElements(A,N);
	return 0;
}
