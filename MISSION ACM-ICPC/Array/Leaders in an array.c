#include<stdio.h>

void PrintLeaders(int *A,int N)
{
    printf("%d ",A[N-1]);
    int max = A[N-1],i;
    for(i=N-2;i >= 0 ; i--)
    {
        if(A[i] > max)
        {
            printf("%d ",A[i]);
            max = A[i];
        }
    }
}

/*
    A[] = {16,17,4,3,5,2}
    Start from rightmost element and move towards first element.
    If any element is greater than current max element than print it
    and update max to this element.

*/
int main(void)
{
    int N,i;
    printf("Enter the size of Array : ");
    scanf("%d",&N);
    int A[N+1];
    printf("Enter the Array Elements : ");
    for(i=0;i<N;i++)
        scanf("%d",&A[i]);
    PrintLeaders(A,N);
    return 0;
}
