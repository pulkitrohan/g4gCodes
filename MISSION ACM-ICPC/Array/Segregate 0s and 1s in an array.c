#include<stdio.h>

void SegregateArray(int *A,int N)
{
    int start = 0,end = N-1;
    while(end > start)
    {
        while(A[start] == 0)
            start++;
        while(A[end] == 1)
            end--;
        if(end > start)
        {
            A[end--] = 1;
            A[start++] = 0;
        }
    }
}

void SegregateArrayMethod2(int *A,int N)
{
    int j = 0,i;
    for(i=0;i<N;i++)
    {
        if(A[i] != 0)
            A[j++] = A[i];
    }
    for(i=j;i<N;i++)
        A[i] = 0;
}

int main(void)
{
    int A[] = {0,1,0,1,0,0,1,1,1,0};
    int N = sizeof(A)/sizeof(A[0]);
    SegregateArrayMethod2(A,N);
    int i;
    for(i=0;i<N;i++)
        printf("%d ",A[i]);
    return 0;
}
