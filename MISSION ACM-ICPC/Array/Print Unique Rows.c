#include<stdio.h>
#include<stdlib.h>

#define ROW 4
#define COL 5

struct Node
{
    int isEndofCol;
    struct Node *child[2];
};

struct Node *NewNode()
{
    struct Node *temp = (struct Node *)malloc(sizeof(struct Node));
    temp->isEndofCol = 0;
    temp->child[0] = temp->child[1] = NULL;
    return temp;
};

int insert(struct Node *&root,int M[ROW][COL],int row,int col)
{
    if(root == NULL)
        root = NewNode();
    if(col < COL)
        return insert(root->child[M[row][col]],M,row,col+1);
    else
    {
        if(!root->isEndofCol)
        {
            root->isEndofCol = 1;
            return 1;
        }
        return 0;
    }
}

void printRow(int M[ROW][COL],int row)
{
    int i;
    for(i=0;i<COL;i++)
        printf("%d ",M[row][i]);
    printf("\n");
}

void FindUniqueRows(int M[ROW][COL])
{
    int i;
    struct Node *root = NULL;
    for(i=0;i<ROW;i++)
        if(insert(root,M,i,0))
            printRow(M,i);
}


int main(void)
{
    int M[ROW][COL] = { {0,1,0,0,1},{1,0,1,1,0},{0,1,0,0,1},{1,0,1,0,0}};
    FindUniqueRows(M);
}
