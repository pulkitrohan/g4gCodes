#include<bits/stdc++.h>
using namespace std;

int MaxOnesIndex(int A[],int N)
{
	for(int i=0;i<N;i++)
		if(A[i] == 0)
			A[i] = INT_MIN;
	int count = 0;
	for(int i=0;i<N;i++)
	{
		if(A[i] < 0)
		{
			A[i] += count;
			count = 0;
		}
		count++;
	}
	for(int i=N-1;i>=0;i--)
	{
		if(A[i] < 0)
		{
			A[i] += count;
			count = 0;
		}
		count++;
	}
	int val = INT_MIN,index = -1;
	int maxVal = INT_MIN;
	for(int i=0;i<N;i++)
	{
		if(A[i] < 0 && A[i] > val)
		{
			if(maxVal < A[i])
			{
				maxVal = A[i];
				index = i;
			}
			
		}
	}
	return index;
	
}


int main(void)
{
	int A[] = {1,1,0,0};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<MaxOnesIndex(A,N);
	return 0;
}
