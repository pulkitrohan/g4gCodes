#include<bits/stdc++.h>
using namespace std;

int MaxElement(int *A,int low,int high)
{
	if(low == high)
		return A[low];
	if(high == low + 1)
		return max(A[low],A[high]);
	int mid = low + (high-low)/2;
	if(A[mid] > A[mid-1] && A[mid] > A[mid+1])
		return A[mid];
	if((A[mid] == A[mid-1] && A[mid] > A[mid+1]) || (A[mid] == A[mid+1] && A[mid] < A[mid-1]))
		return MaxElement(A,low,mid-1);
	if((A[mid] == A[mid-1] && A[mid] < A[mid+1]) || (A[mid] == A[mid+1] && A[mid] > A[mid-1]))
		return MaxElement(A,mid+1,high);
	if(A[mid] == A[mid-1] && A[mid+1] == A[mid])
		return max(MaxElement(A,low,mid-1),MaxElement(A,mid+1,high));
	if(A[mid] > A[mid-1] && A[mid] < A[mid+1])
		return MaxElement(A,mid+1,high);
	return MaxElement(A,low,mid-1);
}

int main(void)
{
    int A[] = {1, 3, 50, 10, 9, 7, 6};
    int N = sizeof(A)/sizeof(A[0]);
    printf("Maximum Element : %d\n",MaxElement(A,0,N-1));
    return 0;
}
