#include<stdio.h>
#include<limits.h>
#define min(A,B) ((A < B) ? A : B)
int MinDis(int *A,int N,int x,int y)
{
    int i,prev,MinDist = INT_MAX;
    for(i=0;i<N;i++)
    {
        if(A[i] == x || A[i] == y)
        {
            prev = i;
            break;
        }
    }
    for(;i<N;i++)
    {
        if(A[i] == x || A[i] == y)
        {
            if(A[i] != A[prev])
                MinDist = min(MinDist,i-prev);
            prev = i;
        }
    }
    return MinDist;

}

int main(void)
{
    int A[] = {3,5,4,2,6,3,0,0,5,4,8,3};
    int N = sizeof(A)/sizeof(A[0]);
    int x = 3,y = 6;
    printf("Minimum distance between %d and %d is %d",x,y,MinDis(A,N,x,y));
    return 0;
}
