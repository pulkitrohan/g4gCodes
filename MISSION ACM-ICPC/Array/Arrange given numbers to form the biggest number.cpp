#include<iostream>
#include<vector>
#include<algorithm>
#include<string>

using namespace std;

int compare(string A,string B)
{
    string AB = A.append(B);
    string BA = B.append(A);
    return AB.compare(BA) > 0 ? 1 : 0;
}

void PrintLargest(vector<string> A)
{
    sort(A.begin(),A.end(),compare);
    for(int i=0;i<A.size();i++)
        cout<<A[i];
}

int main(void)
{
    vector<string> A;
    A.push_back("54");
    A.push_back("546");
    A.push_back("548");
    A.push_back("60");
    PrintLargest(A);
    return 0;
}
