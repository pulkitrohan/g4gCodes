#include<bits/stdc++.h>
using namespace std;

int Partition(int *A,int low,int high)
{
	int j = -1,i;
	for(i=low;i<=high;i++)
	{
		if(A[i] < 0)
		{
			j++;
			Swap(A[i],A[j]);
		}
	}
	return j;
}

void Rearrange(int *A,int N)
{
    int index = Partition(A,0,N-1);
    printf("%d\n",index);
    int pos = index+1,neg = 0;
    while(pos < N && neg < pos && A[neg] < 0)
    {
        Swap(&A[neg],&A[pos]);
        pos++;
        neg += 2;
    }
}

int main(void)
{
    int A[] = {-1,2,-3,4,5,6,-7,8,9};
    int N = sizeof(A)/sizeof(A[0]),i;
    Rearrange(A,N);
    for(i=0;i<N;i++)
        printf("%d ",A[i]);
    return 0;
}
