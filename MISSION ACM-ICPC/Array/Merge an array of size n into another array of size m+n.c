#include<stdio.h>

#define NA -1

void MergeArray(mPlusN,N,n,m)
{
    PushtoEnd(mPlusN,m);
    PrintArray(mPlusN,m);
    Merge(mPlusN,N,n,m);
    PrintArray(mPlusN,m);
}


void PushtoEnd(int *A,int m)
{
    int k = m-1,i;
    for(i = m-1;i>=0;i--)
    {
        if(A[i] != NA)
            A[k--] = A[i];
    }
}

void Merge(int *A,int *B,int n,int m)
{
    int j = n;
    int k = 0,i;
    for(i=0;i<m;i++)
    {
        if(B[k] <= A[j] && k != n)
            A[i] = B[k++];
        else if(B[k] > A[j] && j != m)
            A[i] = A[j++];
    }
}

void PrintArray(int *A,int m)
{
    int i;
    for(i = 0;i<m;i++)
        printf("%d ",A[i]);
    printf("\n");
}


int main(void)
{
    int mPlusN[] = {2,8,NA,NA,NA,13,NA,15,20};
    int N[] = {5,7,9,18};
    int n = sizeof(N)/sizeof(N[0]);
    printf("%d\n",n);
    int m = sizeof(mPlusN)/sizeof(mPlusN[0]);
    printf("%d\n",m);
    MergeArray(mPlusN,N,n,m);
    return 0;
}
