#include<bits/stdc++.h>
using namespace std;

int findSplitPoint(int A[],int N)
{
	int sum = 0;
	for(int i=0;i<N;i++)
		sum += A[i];
	int leftsum = 0;
	for(int i=0;i<N;i++)
	{
		leftsum += A[i];
		sum -= A[i];
		if(sum == leftsum)
			return i;
	}
	return -1;
}

void printTwoParts(int A[],int N)
{
	int splitPoint = findSplitPoint(A,N);
	if(splitPoint == -1)
	{
		cout<<"Not possible";
		return;
	}
	for(int i=0;i<N;i++)
	{
		cout<<A[i]<<" ";
		if(splitPoint == i)
			cout<<endl;
	}
}

int main(void)
{
	int A[] = {1,2,3,4,5,5};
	int N = sizeof(A)/sizeof(A[0]);
	printTwoParts(A,N);
	return 0;
}