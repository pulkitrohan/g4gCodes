#include<bits/stdc++.h>
using namespace std;

int FindMissing(int *A,int low,int high,int diff)
{
	if(low >= high)
		return INT_MAX;
	int mid = low + (high-low)/2;
	if(A[mid+1]-A[mid] != diff)
		return A[mid]+diff;
	if(mid > 0 && A[mid]-A[mid-1] != diff)
		return A[mid-1] + diff;
	if(A[mid] == A[0] + mid*diff)
		return FindMissing(A,mid+1,high,diff);
	return FindMissing(A,low,mid-1,diff);
}

int main(void)
{
	int A[] = {2,4,8,10,12,14};
	int N = sizeof(A)/sizeof(A[0]);
	int diff = (A[N-1]-A[0])/N;
	cout<<FindMissing(A,0,N-1,diff);
	return 0;
}