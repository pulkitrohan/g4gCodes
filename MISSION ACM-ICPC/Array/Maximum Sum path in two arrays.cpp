#include<bits/stdc++.h>
using namespace std;

int FindMaxSumPath(int *A,int *B,int N,int M)
{
	int result = 0,sum1 = 0,sum2 = 0;
	int i = 0,j = 0;
	while(i < N && j < M)
	{
		if(A[i] < B[j])
			sum1 += A[i++];
		else if(A[i] > B[j])
			sum2 += B[j++];
		else
		{
			result += max(sum1,sum2);
			sum1 = 0,sum2 = 0;
			while(i<N && j < M && A[i] == B[j])
			{
				result += A[i++];
				j++;
			}
		}
	}
	while(i < N)
		sum1 += A[i++];
	while(j < M)
		sum2 += B[j++];
	result += max(sum1,sum2);
	return result;
}

int main(void)
{
	int A[] = {2,3,7,10,12};
	int B[] = {1,5,7,8};
	int N = sizeof(A)/sizeof(A[0]);
	int M = sizeof(B)/sizeof(B[0]);
	cout<<FindMaxSumPath(A,B,N,M);
	return 0;
}