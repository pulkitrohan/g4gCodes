#include<stdio.h>
int FirstIndex(int *A,int low,int high,int x)
{
    while(high >= low)
	{
		int mid = low + (high-low)/2;
		if(A[mid] == x)
		{
			if(mid == low || A[mid-1] < x)
				return mid;
			else
				high = mid-1;
		}
		else if(A[mid] > x)
			high = mid-1;
		else
			low = mid+1;
	}
	return -1;
}

int LastIndex(int *A,int low,int high,int x,int N)
{
    while(high >= low)
	{
		int mid = low + (high-low)/2;
		if(A[mid] == x)
		{
			if(mid == high || A[mid+1] > x)
				return mid;
			else
				low = mid+1;
		}
		else if(A[mid] > x)
			high = mid-1;
		else
			low = mid+1;
	}
	return -1;
}

int CountElements(int *A,int N,int x)
{
    int k = FirstIndex(A,0,N-1,x);
    if(k == -1)
        return 0;
    else
    {
        int j = LastIndex(A,0,N-1,x,N);
        return j-k+1;
    }
}

int main(void)
{
    int A[] = {1,2,2,2,3,3,3};
	int N = sizeof(A)/sizeof(A[0]);
	int x = 5;
    if(!CountElements(A,N,x) )
        printf("Element Not Present\n");
    else
        printf("Total Count : %d",CountElements(A,N,x));
    return 0;
}
