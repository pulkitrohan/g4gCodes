#include<cstdio>
#include<list>

using namespace std;

int Matrix[4][4] = {{0,0,1,0},{0,0,1,0},{0,0,0,0},{0,0,1,0}};

int IsAcquiantance(int A,int B)
{
    return Matrix[A][B];
}

int CelebrityProblem(int N)
{
    stack<int> S;
    int i;
    for(int i=0;i<N;i++)
		S.push(i);
    int A = S.top();
    S.pop();
    int B = S.top();
    S.pop();
    while(S.size() != 1)
    {
        if(IsAcquiantance(A,B))
        {
            A = S.top();
			S.pop();
        }
        else
        {
            B = S.top();
			S.pop();
        }
    }
    int C = S.top();
	S.pop();

    if(IsAcquiantance(C,B))
        C = B;
    if(IsAcquiantance(C,A))
        C = A;
   // printf("C = %d\n",C);
    for(i=0;i<N;i++)
    {
        if(i != C)
        {
            if(IsAcquiantance(C,i))
            return -1;
            if(!IsAcquiantance(i,C))
            return -1;
        }
    }
    return C;
}

int main(void)
{
    int size = 4;
    int id = CelebrityProblem(size);
    if(id == -1)
        printf("No Celebrity\n");
    else
        printf("Has a Celebrity with ID %d\n",id);
    return 0;
}
