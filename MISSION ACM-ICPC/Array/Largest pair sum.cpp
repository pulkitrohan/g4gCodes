#include<bits/stdc++.h>
using namespace std;

int FindLargestSumPair(int *A,int N)
{
	if(N < 2)
		return 0;
	int first = max(A[0],A[1]);
	int second = min(A[0],A[1]);
	for(int i=2;i<N;i++)
	{
		if(A[i] > first)
		{	
			second = first;
			first = A[i];
		}
		else if(A[i] > second && A[i] != first)
			second = A[i];
	}
	return first+second;
}

int main(void)
{	
	int A[] = {12,34,10,6,40};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<FindLargestSumPair(A,N);
	return 0;
}