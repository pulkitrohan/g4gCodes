#include<bits/stdc++.h>
using namespace std;

void findAndPrintOriginalArray(int A[],int N)
{
	int sum = 0;
	for(int i=0;i<N;i++)
		sum += A[i];
	sum /= (N-1);
	for(int i=0;i<N;i++)
		cout<<sum - A[i]<<" ";
}

int main(void)
{
	int A[] = {10,14,12,13,11};
	int N = sizeof(A)/sizeof(A[0]);
	findAndPrintOriginalArray(A,N);
	return 0;
}