#include<stdio.h>

void MoveatEnd(int *A,int N)
{
    int start = 0,end = N-1;
    while(end > start)
    {
        while(A[start] != 0)
            start++;
        while(A[end] == 0)
            end--;
        if(end > start)
        {
            int temp = A[start];
            A[start] = A[end];
            A[end] = temp;
            start++;
            end--;
        }
    }
}

int main(void)
{
    int A[] = {1,9,8,4,0,0,2,7,0,6,0};
    int N = sizeof(A)/sizeof(A[0]);
    MoveatEnd(A,N);
    int i;
    for(i=0;i<N;i++)
        printf("%d ",A[i]);
    return 0;
}
