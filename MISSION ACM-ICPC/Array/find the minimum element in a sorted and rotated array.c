#include<stdio.h>

void FindMin(int *A,int low,int high)
{
	if(low < high)
		return A[0];
	if(low == high)
		return A[low];
	int mid = low + (high-low)/2;
	if(mid > low && A[mid-1] > A[mid])
		return A[mid];
	if(mid < high && A[mid+1] < A[mid])
		return A[mid+1];
	if(A[low] == A[mid] && A[mid] == A[high])
		return min(FindMin(A,low,mid-1),FindMin(A,mid+1,high));
	if(A[mid] >= A[high])
		return FindMin(A,mid+1,high);
	else
		return FindMin(A,low,mid-1);
}

int main(void)
{
    int A[] = {2,3,4,5,6,7,8,1};
    int N = sizeof(A)/sizeof(A[0]);
    printf("%d\n",FindMin(A,N));
    return 0;
}
