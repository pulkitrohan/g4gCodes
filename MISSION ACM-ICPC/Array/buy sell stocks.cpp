#include<bits/stdc++.h>
using namespace std;
struct bs
{
	int buy,sell;
};

void stockBuySell(int *price,int N)
{
	if(N == 1)
		return;
	int count = 0;
	struct bs I[N/2+1];
	int i = 0;
	while(i < N)
	{
		while(i<N-1 && price[i+1] <= price[i])
			i++;
		if(i == N-1)
			break;
		I[count].buy = i++;
		while(i < N-1 && price[i] >= price[i-1])
			i++;
		I[count].sell = i-1;
		count++;
	}
	if(count == 0)
		return;
	else
		for(int i=0;i<count;i++)
			printf("%d %d\n",I[i].buy,I[i].sell);
}

int main(void)
{
	int A[] = {100,180,260,310,40,535,695};
	int N = sizeof(A)/sizeof(A[0]);
	stockBuySell(A,N);
	return 0;
}