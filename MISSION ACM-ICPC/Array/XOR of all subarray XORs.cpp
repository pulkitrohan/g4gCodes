#include<bits/stdc++.h>
using namespace std;

int getTotalXorOfSubarrayXors(int A[],int N)
{
	int ans = 0;
	for(int i=0;i<N;i++)
	{
		int freq = (i+1)*(N-i);
		if(freq & 1)
			ans ^= A[i];
	}
	return ans;
}

int main(void)
{
	int A[] = {3,5,2,4,6};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<getTotalXorOfSubarrayXors(A,N);
	return 0;
}