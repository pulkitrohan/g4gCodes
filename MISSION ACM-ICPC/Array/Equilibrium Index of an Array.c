#include<stdio.h>

void PrintEquilibrium(int *A,int N)
{
    int i,sum = 0,leftsum = 0;
    for(i=0;i<N;i++)
        sum += A[i];
    for(i=0;i<N;i++)
    {
        sum -= A[i];
        if(sum == leftsum)
        {
            printf("Equilibrium Index : %d\n",i);
            break;
        }
        leftsum += A[i];
    }
    if(i == N)
        printf("No Equilibrium Index\n");
}

int main(void)
{
    int A[] = {-7,1,5,2,-4,3,0};
    int N = sizeof(A)/sizeof(A[0]);
    PrintEquilibrium(A,N);
    return 0;
}
