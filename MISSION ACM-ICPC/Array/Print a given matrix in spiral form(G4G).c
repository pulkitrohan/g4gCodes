#include<stdio.h>

void PrintSpiral(int M[3][6],int N,int K)
{

    int left = 0,right = K-1,top = 0,bottom = N-1,i;
    while(1)
    {
        if(left > right || top > bottom)
            break;
        for(i=left;i<=right;i++)
            printf("%d ",M[top][i]);
        top++;
        if(left > right || top > bottom)
            break;
        for(i=top;i<=bottom;i++)
            printf("%d ",M[i][right]);
        right--;
        if(left > right || top > bottom)
            break;
        for(i=right;i>=left;i--)
            printf("%d ",M[bottom][i]);
        bottom--;
        if(left > right || top > bottom)
            break;
        for(i=bottom;i >= top;i--)
            printf("%d ",M[i][left]);
        left++;
    }
}

int main(void)
{
    int M[3][6] = {{1,2,3,4,5,6},{7,8,9,10,11,12},{13,14,15,16,17,18}};
    PrintSpiral(M,3,6);
    return 0;
}
