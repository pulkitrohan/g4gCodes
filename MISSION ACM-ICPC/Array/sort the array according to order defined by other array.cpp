#include<bits/stdc++.h>
using namespace std;

int firstOccurence(int *A,int N,int x)
{
	int low = 0,high = N-1;
	while(high >= low)
	{
		int mid = low + (high-low)/2;
		if(A[mid] == x && (mid == low || A[mid-1] < x))
			return mid;
		else if(A[mid] > x)
			high = mid-1;
		else
			low = mid+1;
	}
	return -1;
}

void sortAccording(int *A,int *B,int M,int N)
{
	int temp[M],visited[M];
	for(int i=0;i<M;i++)
	{
		temp[i] = A[i];
		visited[i] = 0;
	}
	sort(temp,temp+M);
	int k = 0;
	for(int i=0;i<N;i++)
	{
		int index = firstOccurence(temp,M,B[i]);
		if(index == -1)
			continue;
		for(int j=index;j<M && temp[j] == B[i];j++)
		{
			A[k++] = temp[j];
			visited[j] = 1;
		}
	}
	for(int i=0;i<M;i++)
		if(!visited[i])
			A[k++] = temp[i];
}			

int main(void)
{
	int A[] = {2,1,2,5,7,1,9,3,6,8,8};
	int B[] = {2,1,8,3};
	int M = sizeof(A)/sizeof(A[0]);
	int N = sizeof(B)/sizeof(B[0]);
	sortAccording(A,B,M,N);
	for(int i=0;i<M;i++)	
		cout<<A[i]<<" ";
	return 0;
}