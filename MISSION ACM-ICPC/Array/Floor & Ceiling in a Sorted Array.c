#include<stdio.h>
#define Size(A) (sizeof(A)/sizeof(A[0]))



int Ceil(int *A,int N,int x)
{
    int low = 0,high = N-1;
    if(A[low] >= x)
        return low;
    if(A[high] < x)
        return -1;
    while(high >= low)
    {
        int mid = (low+high)/2;
        if(A[mid] == x)
            return mid;
        else if(A[mid] > x)
        {
            if(mid -1 >= low && A[mid-1] < x)
                return mid;
            else
                high = mid-1;
        }
        else
        {
            if(mid+1 <= high && A[mid+1] >= x)
                return mid+1;
            else
                low = mid + 1;
        }
    }
}

int Floor(int *A,int N,int x)
{
    int low = 0,high = N-1;
    if(A[high] <= x)
        return high;
    if(A[low] > x)
        return -1;
    while(high >= low)
    {
        int mid = (low+high)/2;
        if(A[mid] == x)
            return mid;
        else if(A[mid] <= x)
        {
            if(mid+1 <= high && A[mid+1] > x)
                return mid;
            else
                low = mid + 1;
        }
        else
        {
            if(mid-1 >= low && A[mid-1] <= x)
                return mid-1;
            else
                high = mid-1;
        }
    }
}


int main(void)
{
    int A[] = {1,2,8,10,10,12,19};
    int N = Size(A);
    int x = 5;
    int ceil_index = Ceil(A,N,x);
    int floor_index = Floor(A,N,x);
    if(ceil_index == -1)
        printf("Ceil not found\n");
    else
        printf("Ceil = %d\n",A[ceil_index]);
    if(floor_index == -1)
        printf("Floor not found\n");
    else
        printf("Floor = %d\n",A[floor_index]);
    return 0;
}
