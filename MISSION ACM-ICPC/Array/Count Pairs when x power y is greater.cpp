#include<bits/stdc++.h>
using namespace std;

int countP(int x,int *Y,int N,int *NoOfY)
{
	if(x == 0)
		return 0;
	if(x == 1)
		return NoOfY[0];
	int *index = upper_bound(Y,Y+N,x);
	int ans = (Y + N) - index;
	ans += NoOfY[0] + NoOfY[1];
	if(x == 2)
		ans -= NoOfY[3] + NoOfY[4];
	if(x == 3)
		ans += NoOfY[2];
	return ans;
}

int CountPairs(int *X,int *Y,int M,int N)
{
	int NoOfY[5] = {0};
	for(int i=0;i<N;i++)
		if(Y[i] < 5)
			NoOfY[Y[i]]++;
	sort(NoOfY,NoOfY + N);
	int count = 0;
	for(int i=0;i<M;i++)
		count += countP(X[i],Y,N,NoOfY);
	return count;
}

int main(void)
{
	int X[] = {2,1,6};
	int Y[] = {1,5};
	int M = sizeof(X)/sizeof(X[0]);
	int N = sizeof(Y)/sizeof(Y[0]);
	cout<<CountPairs(X,Y,M,N);
	return 0;
}