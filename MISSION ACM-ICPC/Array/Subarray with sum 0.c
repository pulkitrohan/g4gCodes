#include<bits/stdc++.h>
using namespace std;

void FindSubArray(int *A,int N,int sum)
{
    int cur_sum = A[0],start = 0,i;
	if(A[0] == 0)
	{
		cout<<"0"<<"0"<<endl;
		return;
	}
    for(i=1;i<N;i++)
    {
        while(cur_sum > sum && i-start > 1)
            cur_sum -= A[start++];
        if(cur_sum == sum)
        {
            printf("Sum found between indexes %d and %d\n",start,i-1);
            return;
        }
		if(A[i] == 0)
		{
			printf("Sum found between indexes %d and %d\n",i,i);
            return;
		}
		
        cur_sum += A[i];
    }
    printf("No subarray with given sum found\n");
}

int main(void)
{
    int A[] = {4,2,-6,4,6};
    int sum = 0;
    int N = sizeof(A)/sizeof(A[0]);
    FindSubArray(A,N,sum);

    return 0;
}
