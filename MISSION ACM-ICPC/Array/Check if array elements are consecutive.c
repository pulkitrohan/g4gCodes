#include<stdio.h>

int GetMax(int *A,int N)
{
    int max_element = A[0];
    int i;
    for(i=1;i<N;i++)
    {
        if(A[i] > max_element)
            max_element = A[i];
    }
    return max_element;
}

int GetMin(int *A,int N)
{
    int min_element = A[0],i;
    for(i=1;i<N;i++)
    {
        if(A[i] < min_element)
            min_element = A[i];
    }
    return min_element;
}
/*
//Method 1
//Time Complexity : O(N)
//Space Complexity : O(N)
//Works for all inputs
int ConsecutiveElements(int *A,int N)
{
    int min = GetMin(A,N);
    int max = GetMax(A,N);
    if(max - min + 1 != N)      //Means Elements are not consecutive
        return 0;
    int temp[N+1],i;
    for(i=0;i<N;i++)
        temp[i] = 0;
    for(i=0;i<N;i++)
    {
        if(temp[A[i]-min] == 1)     //Means a duplicate Element
            return 0;
        temp[A[i]-min] = 1;
    }
    return 1;   //Each element occurs only once
}
*/

//Method 2
//Time Complexity : O(N)
//Space Complexity : O(1)
//Modifies Array
//Sometimes might not work for -ve Integers
int ConsecutiveElements(int *A,int N)
{
    int min = GetMin(A,N);
    int max = GetMax(A,N);
    if(max - min + 1 != N)      //Means Elements are not consecutive
        return 0;
    int i;
    for(i=0;i<N;i++)
    {
        if(A[(abs(A[i])-min)] > 0)  //Making A[i] +ve and subtracting from min if it +ve then it is te
                                    //first occurence of the element.
            A[(abs(A[i]-min))] = -A[(abs(A[i])-min)];
        else        //2nd Occurance of that element So it means their is a repetition
            return 0;
    }
    return 1;
}

int main(void)
{
    int A[] = {83,78,80,81,79,82};
    int N = sizeof(A)/sizeof(A[0]);
    if(ConsecutiveElements(A,N))
        printf("Yes,Array elements are consecutive\n");
    else
        printf("No,Array elements are not consecutive\n");
    return 0;
}
