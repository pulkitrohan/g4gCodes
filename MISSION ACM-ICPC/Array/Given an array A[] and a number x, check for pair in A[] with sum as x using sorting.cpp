#include<cstdio>
#include<algorithm>
using namespace std;
void FindAndPrint(int *A,int sum,int N)
{
    int l = 0,r = N-1;
    while(r > l)
    {
        if(A[l] + A[r] > sum)
            r--;
        else if(A[l] + A[r] < sum)
            l++;
        else
        {
            printf("Pair whose sum = %d is %d,%d\n",sum,A[l],A[r]);
            r--;
            l++;
        }
    }
}

int main(void)
{
    int N,i,sum;
    printf("Enter the size of Array : ");
    scanf("%d",&N);
    int A[N+1];
    if(N)
    printf("Enter the Array Elements : ");
    for(i=0;i<N;i++)
        scanf("%d",&A[i]);
    printf("Enter the sum required : ");
    scanf("%d",&sum);
    sort(A,A+N);
    FindAndPrint(A,sum,N);
    return 0;
}
