#include<bits/stdc++.h>
using namespace std;

#define MOD 103
#define D 256
void CheckPalindrome(string S)
{
	int N = S.length();
	cout<<S[0]<<"\tYES\n";
	if(N == 1)
		return ;
		
	int first = S[0] % MOD;
	int second = S[1] % MOD;
	int h = 1,j;
	for(int i=1;i<N;i++)
	{
		if(first == second)
		{
			for( j=0;j<i/2;j++)
				if(S[j] != S[i-j])
					break;
			(j == i/2) ? cout<<S[i]<<"\tYES\n" : cout<<S[i]<<"\tNO\n";
		}
		else
			cout<<S[i]<<"\tNO\n";
			
		if(i != N-1)
		{
			if(i % 2 == 0)
			{
				h = (h*D)%MOD;
				first = (first + h*S[i/2])%MOD;
				second = (second*D + S[i+1])%MOD;
			}
			else
				second = (D*(second + MOD - S[(i+1)/2]*h)%MOD + S[i+1])%MOD;
		}
	}
}

int main(void)
{
	string S = "aabaacaabaa";
	CheckPalindrome(S);
	return 0;
}