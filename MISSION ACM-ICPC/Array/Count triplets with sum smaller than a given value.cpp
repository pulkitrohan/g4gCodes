#include<bits/stdc++.h>
using namespace std;

void CountTriplets(int *A,int N,int sum)
{
	sort(A,A+N);
	int ans = 0;
	for(int i=0;i<N-2;i++)
	{
		int j=i+1,k=N-1;
		while(j < k)
		{
			if(A[i]+A[j]+A[k] >= sum)
				k--;
			else
			{
				ans += k-j;
				j++;
			}
		
		}
	}
	cout<<ans<<endl;
}

int main(void)
{
	int A[] = {5,1,3,4,7};
	int sum = 12;
	int N = sizeof(A)/sizeof(A[0]);
	CountTriplets(A,N,sum);
	return 0;
}
}