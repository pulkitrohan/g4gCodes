#include<bits/stdc++.h>
using namespace std;

int CountOnes(int *A,int N)
{
	int start = 0,end = N-1;
	while(end >= start)
	{
		int mid = start + (end-start)/2;
		if(A[mid] == 1)
		{
			if(mid == end || A[mid+1] == 0)
				return mid+1;
			else
				mid = start+1;
		}
		else
			end = mid-1;
	}
	return 0;
}

int main(void)
{	
	int A[] = {1,1,1,1,0,0,0};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<CountOnes(A,N);
	return 0;
}