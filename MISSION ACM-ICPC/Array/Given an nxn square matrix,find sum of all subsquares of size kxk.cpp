#include<bits/stdc++.h>
using namespace std;
#define N 5


void PrintSum(int MAT[N][N],int k)
{
	if(k > N)
		return;
	int A[N][N];
	
	for(int i=0;i<N;i++)
	{
		int sum = 0;
		for(int j=0;j<k;j++)
			sum += MAT[j][i];
		A[0][i] = sum;
		
		for(int j=1;j<N-k+1;j++)
		{
			sum += (MAT[j+k-1][i] - MAT[j-1][i]);
			A[j][i] = sum;
		}
	}
	
	for(int i=0;i<N-k+1;i++)
	{
		int sum = 0;
		for(int j=0;j<k;j++)
			sum += A[i][j];
		cout<<sum<<" ";
		for(int j=1;j<N-k+1;j++)
		{
			sum += (A[i][j+k-1] - A[i][j-1]);
			cout<<sum<<" ";
		}
		cout<<endl;
	}
}

int main(void)
{
	int M[N][N] = {{1,1,1,1,1},
				   {2,2,2,2,2},
				   {3,3,3,3,3},
				   {4,4,4,4,4},
				   {5,5,5,5,5}
				  };
	int k = 3;
	PrintSum(M,k);
	return 0;
}