#include<stdio.h>
#include<stdlib.h>

void Swap(int *A,int *B)
{
    int temp = *A;
    *A = *B;
    *B = temp;
}

int Segregate(int *A,int N)
{
    int i,j=0;
    for(i=0;i<N;i++)
    {
        if(A[i] <= 0)
        {
            Swap(&A[i],&A[j]);
            j++;
        }
    }
    return j;
}

void FindSmallest(int *A,int N)
{
    int index = Segregate(A,N);
    int i;
    //printf("%d",index);
    for(i=index;i<N;i++)
    {
        if((abs(A[i])-1 < N-index) && (A[abs(A[i])-1] > 0))
            A[abs(A[i])-1] = -A[abs(A[i])-1];
    }
    for(i=index;i<N;i++)
    {
        if(A[i] > 0)
        {
            printf("%d\n",i+1-index);
            return ;
        }
    }
    printf("%d",N+1);
}

int main(void)
{
    int A[] = {0,10,2,-10,-20};
    int N = sizeof(A)/sizeof(A[0]);
    FindSmallest(A,N);
    return 0;
}
