#include<stdio.h>

void ReplaceNextGreatest(int *A,int N)
{
    int max_from_right = A[N-1],i;
    A[N-1] = -1;
    for(i = N-2;i>=0;i--)
    {
        int temp = A[i];
        A[i] = max_from_right;
        if(max_from_right < temp)
            max_from_right = temp;
    }
}

int main(void)
{
    int A[] = {16,17,4,3,5,2};
    int N = sizeof(A)/sizeof(A[0]),i;
    ReplaceNextGreatest(A,N);
    for(i=0;i<N;i++)
        printf("%d ",A[i]);
    return 0;
}
