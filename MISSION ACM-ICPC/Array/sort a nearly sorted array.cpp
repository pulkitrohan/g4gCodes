#include<bits/stdc++.h>
using namespace std;

class cmp
{
	public:
		bool operator()(int &n1,int &n2)
		{
			return (n1 > n2);
		}
};

void printArray(int *A,int N,int k)
{
	priority_queue<int,vector<int>,cmp> H;
	for(int i=0;i<=k;i++)
		H.push(A[i]);
	int count = 0;
	for(int i=k+1;count<N;i++,count++)
	{
		int temp = H.top();
		H.pop();
		A[count] = temp;
		if(i < N)
			H.push(A[i]);
	}
}

int main(void)
{
	int k = 3;
	int A[] = {2,6,3,12,56,8};
	int N = sizeof(A)/sizeof(A[0]);
	printArray(A,N,k);
	for(int i=0;i<N;i++)
		cout<<A[i]<<" ";
	return 0;
}