#include<stdio.h>

int FindPeak(int *A,int low,int high,int N)
{
    int mid = (high-low)/2 + low/2;
    if((mid == 0 || A[mid-1] < A[mid]) && (mid == N-1) || A[mid+1] < A[mid])
        return mid;
    else if(mid > 0 && A[mid-1] > A[mid])
        return FindPeak(A,low,mid-1,N);
    else
        return FindPeak(A,mid+1,high,N);
}

int main(void)
{
    int A[] = {1,3,20,4,1,0};
    int N = sizeof(A)/sizeof(A[0]);
    printf("Index of Peak Element : %d\n",FindPeak(A,0,N-1,N));
    return 0;
}
