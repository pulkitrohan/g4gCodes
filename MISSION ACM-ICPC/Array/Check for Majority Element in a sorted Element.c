#include<stdio.h>

int BinarySearch(int *A,int X,int N)
{
    int low = 0,high = N-1;
    while(high >= low)
    {
        int mid = (high + low)/2;
        if((mid == 0 || A[mid-1] < X) && A[mid] == X)
            return mid;
        else if(A[mid] < X)
            low = mid + 1;
        else
            high = mid - 1;
    }
    return -1;
}

int isMajorityElement(int *A,int X,int N)
{
    int index = BinarySearch(A,X,N);
    if(index == -1)
        return 0;
    if(((index + N/2) <= (N-1)) && A[index + N/2] == X)
        return 1;
    else
        return 0;
}

int main(void)
{
    int N,i,X;
    scanf("%d",&N);
    int A[N+1];
    for(i=0;i<N;i++)
        scanf("%d",&A[i]);
    scanf("%d",&X);
    if(isMajorityElement(A,X,N))
        printf("Yes,%d is Majority Element\n",X);
    else
        printf("No,%d is not a Majority Element\n",X);
    return 0;
}
