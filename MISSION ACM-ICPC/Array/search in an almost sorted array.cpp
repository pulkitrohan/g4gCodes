#include<bits/stdc++.h>
using namespace std;

int BinarySearch(int *A,int start,int end,int x)
{
	if(end >= start)
	{
		int mid = start + (end-start)/2;
		if(A[mid] == x)
			return mid;
		if(mid > start && A[mid-1] == x)
			return mid-1;
		if(mid < end && A[mid+1] == x)
			return mid+1;
		if(A[mid] > x)
			return BinarySearch(A,start,mid-2,x);
		return BinarySearch(A,mid+2,end,x);
	}
}

int main(void)
{
	int A[] = {3,2,10,4,40};
	int N = sizeof(A)/sizeof(A[0]);
	int x = 4;
	cout<<BinarySearch(A,0,N-1,x);
	return 0;
}