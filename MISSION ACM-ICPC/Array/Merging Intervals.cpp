#include<cstdio>
#include<algorithm>
#include<stack>
using namespace std;

struct Interval
{
    int start;
    int end;
};

int compare(struct Interval I1,struct Interval I2)
{
    return (I1.start < I2.start) ? 1 : 0 ;
}

void MergeIntervals(struct Interval *I,int N)
{
    if(N == 0)
        return;
    stack<struct Interval> S;
    sort(I,I+N,compare);
    int i;
    S.push(I[0]);
    for(i=1;i<N;i++)
    {
        struct Interval top = S.top();
        if(top.end < I[i].start)
            S.push(I[i]);
        else if(top.end < I[i].end)
        {
            top.end = I[i].end;
            S.pop();
            S.push(top);
        }
    }
    while(!S.empty())
    {
        struct Interval top = S.top();
        printf("[%d,%d]\n",top.start,top.end);
        S.pop();
    }
}

int main(void)
{
    struct Interval I[] = {{1,3},{7,9},{4,6},{10,13}};
    int N = sizeof(I)/sizeof(I[0]);
    MergeIntervals(I,N);
    return 0;
}
