#include<stdio.h>

int gcd(int A,int B)
{
    if(B == 0)
        return A;
    else
        return gcd(B,A%B);
}

void Rotate(int *A,int N,int d)
{
    int i,j,k,t;
    for(i=0;i<gcd(N,d);i++)
    {
        int temp = A[i];
        j = i;
        while(1)
        {
            k = j + d;
            if(k >= N)
               k = k - N;
            if(k == i)
                break;
            A[j] = A[k];
            j = k;
       // for(t=0;t<N;t++)
       //     printf("%d ",A[t]);
       // printf("\n");
        }
        A[j] = temp;
    }
}

void Reverse(int *A,int start,int end)
{
    while(end > start)
    {
        int temp = A[start];
        A[start] = A[end];
        A[end] = temp;
        start++;
        end--;
    }
}
//Using Reverse Array Procedure

void Rotate1(int *A,int N,int D)
{
    Reverse(A,0,D-1);
    Reverse(A,D,N-1);
    Reverse(A,0,N-1);
}

int main(void)
{
    int N,i,D;
    printf("Enter the size of Array : ");
    scanf("%d",&N);
    int A[N+1];
    if(N)
    printf("Enter the Array Elements : ");
    for(i=0;i<N;i++)
        scanf("%d",&A[i]);
    printf("By how many positions do you want to Rotate it : ");
    scanf("%d",&D);
    Rotate(A,N,D);
    printf("After Rotation : ");
    for(i=0;i<N;i++)
        printf("%d ",A[i]);
    return 0;
}
