#include<stdio.h>
/*
void Reverse(int *A,int low,int high)
{
    int temp;
    while(high > low)
    {
        temp = A[low];
        A[low] = A[high];
        A[high] = temp;
        high--;
        low++;
    }
} */

void Reverse(int *A,int low,int high)
{
    int temp;
    if(low > high)
        return;
    temp = A[low];
    A[low] = A[high];
    A[high] = temp;
    Reverse(A,low+1,high-1);
}
int main(void)
{
    int N,i;
    printf("Enter the size of Array : ");
    scanf("%d",&N);
    int A[N+1];
    if(N)
    printf("Enter the Array Elements : ");
    for(i=0;i<N;i++)
        scanf("%d",&A[i]);
    Reverse(A,0,N-1);
    printf("\nOn Reversing : ");
    for(i=0;i<N;i++)
        printf("%d ",A[i]);
    return 0;
}
