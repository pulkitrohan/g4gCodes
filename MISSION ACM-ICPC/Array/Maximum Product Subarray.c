#include<stdio.h>
#define max(A,B) ((A > B) ? A : B)
#define min(A,B) ((A > B) ? B : A)
int MaxProductSubarray(int *A,int N)
{
    int cur_max = 1,cur_min = 1,max_product = INT_MIN;
	for(int i=0;i<N;i++)
	{
		int A = cur_max*A[i];
		int B = cur_min*A[i];
		
		cur_max = max(max(A,B),A[i]);
		cur_min = min(min(A,B),A[i]);
		max_product = max(cur_max,max_product);
	}
	return max_product;
}


int main(void)
{
    int A[] = {-1,-3,-10,0,60};
    int N = sizeof(A)/sizeof(A[0]);
    printf("%d",MaxProductSubarray(A,N));
    return 0;
}
