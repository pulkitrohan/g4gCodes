#include<bits/stdc++.h>
using namespace std;
#define N 4


bool wPreferm1Overm(int prefer[2*N][N],int w,int m,int m1)
{
	for(int i=0;i<N;i++)
	{
		if(prefer[w][i] == m1)
			return true;
		if(prefer[w][i] == m)
			return false;
	}
}

void stableMarriage(int prefer[2*N][N])
{
	vector<int> wPartner(N,-1);
	vector<bool> mFree(N,true);
	int count = N;
	while(count > 0)
	{
		int m;
		for(m=0;m<N;m++)
			if(mFree[m])
				break;
		for(int i=0;i<N && mFree[m];i++)
		{
			int w = prefer[m][i];
			if(wPartner[w-N] == -1)
			{
				wPartner[w-N] = m;
				mFree[m] = false;
				count--;
			}
			else
			{
				int m1 = wPartner[w-N];
				if(wPreferm1Overm(prefer,w,m,m1) == false)
				{
					wPartner[w-N] = m;
					mFree[m] = false;
					mFree[m1] = true;
				}
			}
		}
	}
	cout<<"Man\tWoman\n";
	for(int i=0;i<N;i++)
		cout<<" "<<wPartner[i]<<"\t"<<i+N<<endl;
}

int main(void)
{
	int prefer[2*N][N] = { {7,5,6,4},
						   {5,4,6,7},
						   {4,5,6,7},
						   {4,5,6,7},
						   {0,1,2,3},
						   {0,1,2,3},
						   {0,1,2,3},
						   {0,1,2,3}
						 };
	stableMarriage(prefer);
	return 0;
}