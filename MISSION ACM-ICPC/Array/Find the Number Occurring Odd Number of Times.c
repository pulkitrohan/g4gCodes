#include<stdio.h>

int PrintOccuringOddTimes(int *A,int N)
{
    int i;
    for(i=1;i<N;i++)
        A[0] ^= A[i];
    return A[0];
}

int main(void)
{
    int N,i;
    printf("Enter the size of Array : ");
    scanf("%d",&N);
    int A[N+1];
    if(N)
    printf("Enter the Array Elements : ");
    for(i=0;i<N;i++)
        scanf("%d",&A[i]);
    printf("Number Occuring Odd Times is %d",PrintOccuringOddTimes(A,N));
    return 0;
}
