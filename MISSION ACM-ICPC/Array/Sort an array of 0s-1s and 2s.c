#include<stdio.h>
/*
0, 1, 1, 0, 1, 2
low = 0,mid = 0,high = 5
A[mid] = 0
swap(A[0],A[0])
low = 1 mid = 1 high = 5
A[mid] = 1
low = 1 mid = 2 high = 5
A[mid] = 1
low = 1 mid = 3 high = 5
A[mid] = 0
swap(A[1],A[3])
0 0 1 1 1 2
low = 2 mid = 4 high = 5
A[mid] = 1
low = 2 mid = 5 high = 5
A[mid] = 2
swap(A[5],A[5])
low = 2 mid = 5 high = 4
*/

void Swap(int *A,int *B)
{
    int temp = *A;
    *A = *B;
    *B = temp;
}
void SortArray(int *A,int N)
{
    int low = 0,mid = 0,high = N-1;
    while(high >= mid)
    {
        switch(A[mid])
        {
            case 0 : Swap(&A[low],&A[mid]);
                     low++,mid++;
                     break;
            case 1 : mid++;
                     break;
            case 2 : Swap(&A[mid],&A[high]);
                     high--;
                     break;
        }
    }
}

int main(void)
{
    int A[] = {0,1,1,0,1,2,1,2,0,0,0,1};
    int N = sizeof(A)/sizeof(A[0]);
    SortArray(A,N);
    int i;
    printf("After Sorting : ");
    for(i=0;i<N;i++)
        printf("%d ",A[i]);
    return 0;
}
