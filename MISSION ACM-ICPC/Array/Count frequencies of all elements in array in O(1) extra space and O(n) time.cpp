#include<bits/stdc++.h>
using namespace std;

void printFrequency(int A[],int N)
{
	for(int i=0;i<N;i++)
		A[i] -= 1;
	for(int i=0;i<N;i++)
		A[A[i]%N] = A[A[i]%N] + N;
	for(int i=0;i<N;i++)
		cout<<i+1<<" -> "<<A[i]/N<<endl;	
}


int main(void)
{
	int A[] = {2,3,3,2,5};
	int N = sizeof(A)/sizeof(A[0]);
	printFrequency(A,N);
	return 0;
}
