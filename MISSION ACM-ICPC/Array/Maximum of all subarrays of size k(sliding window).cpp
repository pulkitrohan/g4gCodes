#include<cstdio>
#include<utility>
#include<deque>

using namespace std;
int main(void)
{
    int N,K,i;
    int A[] = {1,5,4,3,2};
    N = sizeof(A)/sizeof(A[0]);
    K = 1;
    deque<int> Q;
    for(i=0;i<K;i++)
    {
        while(!Q.empty() && A[i] >= A[Q.back()])
            Q.pop_back();
        Q.push_back(i);
    }
    for(;i<N;i++)
    {
        printf("%d ",A[Q.front()]);
        while(!Q.empty() && i-Q.front() >= K)
            Q.pop_front();
        while(!Q.empty() && A[i] >= A[Q.back()])
            Q.pop_back();
        Q.push_back(i);
    }
    printf("%d ",A[Q.front()]);
    return 0;
}

