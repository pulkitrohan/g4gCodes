#include<bits/stdc++.h>
using namespace std;

void combinationUtil(int *A,int N,int r,int index,int *data,int i)
{
	if(index == r)
	{
		for(int j=0;j<r;j++)
			cout<<data[j]<<" ";
		cout<<endl;
		return;
	}
	if(i >= N)
		return;
	data[index] = A[i];
	combinationUtil(A,N,r,index+1,data,i+1);
	combinationUtil(A,N,r,index,data,i+1);
}

void printCombinations(int *A,int N,int r)
{
	int data[r+1];
	combinationUtil(A,N,r,0,data,0);
}



int main(void)
{
	int A[] = {1,2,3,4,5};
	int r = 3;
	int N = sizeof(A)/sizeof(A[0]);
	printCombinations(A,N,r);
	return 0;
}