#include<bits/stdc++.h>
using namespace std;
#define ROW 3
#define COL 3

void PrintDiagonal(int MAT[][COL])
{
	for(int line = 0;line < (ROW+COL-1);line++)
	{
		for(int i = line,j = 0;i>=0 && j <= line;i--,j++)
		{
			if(i < ROW && j < COL)
				cout<<MAT[i][j]<<" ";
		}
		cout<<endl;
	}
}

int main(void)
{
	int MAT[ROW][COL] = {{1,2,3},
						 {4,5,6},
						 {7,8,9}
						};
	for(int i=0;i<ROW;i++)
	{
		for(int j=0;j<COL;j++)
			cout<<MAT[i][j]<<" ";
		cout<<endl;
	}
	PrintDiagonal(MAT);
	return 0;
}