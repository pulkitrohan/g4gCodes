#include<bits/stdc++.h>
using namespace std;
#define M 4
#define N 5


int FindCommon(int MAT[M][N])
{
	int col[M];
	for(int i=0;i<N;i++)
		col[i] = N-1;
		
	int min_row = 0;
	while(col[min_row] >= 0)
	{
		for(int i=0;i<M;i++)
			if(MAT[i][col[i]] < MAT[min_row][col[min_row]])
				min_row = i;
		int count = 0;
		for(int i=0;i<M;i++)
		{
			if(MAT[i][col[i]] > MAT[min_row][col[min_row]])
			{
				if(col[i] == 0)
					return -1;
				col[i]--;
			}
			else
				count++;
		}
		if(count == M)
			return MAT[min_row][col[min_row]];
	}
	return -1;
}
	

int main(void)
{
	int MAT[M][N] = {
					 {1,2,3,4,5},
					 {2,4,5,8,10},
					 {3,5,7,9,11},
					 {1,3,5,7,9}
					 };
	int ans = FindCommon(MAT);
	if(ans == -1)
		cout<<"NO COMMON ELEMENT\n";
	else
		cout<<ans<<endl;
	return 0;
}