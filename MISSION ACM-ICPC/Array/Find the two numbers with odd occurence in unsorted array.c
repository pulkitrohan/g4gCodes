#include<stdio.h>

void OddOccurence(int *A,int N)
{
    int xor = A[0],i;
    for(i=1;i<N;i++)
        xor ^= A[i];
    int set_bit_no = xor & ~(xor-1);
    int x = 0, y = 0;
    for(i=0;i<N;i++)
    {
        if(set_bit_no & A[i])
            x ^= A[i];
        else
            y ^= A[i];
    }
    printf("The two Odd elements are %d %d\n",x,y);
}

int main(void)
{
    int A[] = {12,23,34,12,12,23,12,45};
    int N = sizeof(A)/sizeof(A[0]);
    OddOccurence(A,N);
    return 0;
}
