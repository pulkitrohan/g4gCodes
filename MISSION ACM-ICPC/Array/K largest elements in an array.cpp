//Code for K Largest element in an unsorted array

//Expected Running time for this code is O(N)

#include<cstdio>
#include<algorithm>

using namespace std;

int RandomizedPivot(int *A,int l,int r) //Same Code as Randomized Partition.
{
    int PivotIndex = l + (rand() % (r - l +1));
    int pivot = A[PivotIndex];
    swap(A[PivotIndex],A[l]);
    int i = l,j,temp;
    for(j=l+1;j<=r;j++)
    {
        if(A[j] > pivot)    //We want k largest elements.So doing reverse from what it was in Partition Algorithm
        {
            i++;
            swap(A[i],A[j]);
        }
    }
    swap(A[i],A[l]);
    return i;
}


void RandomizedSelect(int *A,int l,int r,int k)
{
    if(l == r)
        return;
    int q = RandomizedPivot(A,l,r);
    if(q+1 == k)    //if pivot's index is equal to kth largest element then return the index
        return;
    else if(q + 1 > k)  //if pivot's index  is greater than kth largest element then kth largest element is in left partition
        RandomizedSelect(A,l,q-1,k);
    else
        RandomizedSelect(A,q+1,r,k);    //else in right partition
}

int main(void)
{
    int N=7,i,k=3;
    int A[] = {1,23,12,9,30,2,50};
    RandomizedSelect(A,0,N-1,k);
    for(i=0;i<k;i++)
        printf("%d ",A[i]);
    return 0;
}
