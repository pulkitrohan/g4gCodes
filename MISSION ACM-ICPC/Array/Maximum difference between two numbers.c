#include<stdio.h>

int MaxDiff(int *A,int N)
{
    int diff = A[1]-A[0];
    int min = A[0],i;
    for(i=1;i<N;i++)
    {
        if(A[i]-min > diff)
            diff = A[i] - min;
        if(A[i] < min)
            min = A[i];
    }
    return diff;
}

int main(void)
{
    int A[] = {2,3,10,6,4,8,1};
    //int A[] = {7,9,5,6,3,2};
    int N = sizeof(A)/sizeof(A[0]);
    printf("Maximum Difference is %d\n",MaxDiff(A,N));
    return 0;
}
