#include<stdio.h>
#define max(a,b) (a > b ? a : b)
#define min(a,b) (a > b ? b : a)
struct pair
{
    int max;
    int min;
};
//Method 1
//Worst Case Comparisons : 2(N-2) + 1
//Best Case Comparisons : (N-2) + 1
struct pair FindMaxMin(int *A,int N)
{
    struct pair MaxMin;
    int i;
    if(N == 1)
    {
        MaxMin.max = A[0];
        MaxMin.min = A[0];
        return MaxMin;
    }
    MaxMin.max = max(A[1],A[0]);
    MaxMin.min = min(A[1],A[0]);
    for(i=2;i<N;i++)        //N-2
    {
        if(A[i] > MaxMin.max)
            MaxMin.max = A[i];
        else if(A[i] < MaxMin.min)
            MaxMin.min = A[i];
    }
    return MaxMin;
}

//Method2
// 3/2n -2 comparisons if n is a power of 2.
// more than 3/2n -2 comparisons if n is not a power of 2.
struct pair Method2(int *A,int low,int high)
{
    struct pair MaxMin,MM1,MM2;
    if(low == high)
    {
        MaxMin.max = A[low];
        MaxMin.min = A[low];
        return MaxMin;
    }
    else if(high == low + 1)
    {
        if(A[low] > A[high])
        {
            MaxMin.max = A[low];
            MaxMin.min = A[high];
        }
        else
        {
            MaxMin.max = A[high];
            MaxMin.min = A[low];
        }
        return MaxMin;
    }
    int mid = (low + high)/2;
    MM1 = Method2(A,low,mid);
    MM2 = Method2(A,mid+1,high);
    if(MM1.min > MM2.min)
        MaxMin.min = MM2.min;
    else
        MaxMin.min = MM1.min;
    if(MM1.max > MM2.max)
        MaxMin.max = MM1.max;
    else
        MaxMin.max = MM2.max;
    return MaxMin;
}
/*
If n is odd:    3*(n-1)/2
If n is even:   1 Initial comparison for initializing min and max,
                and 3(n-2)/2 comparisons for rest of the elements
                =  1 + 3*(n-2)/2 = 3n/2 -2
Second and third approaches make equal number of comparisons when
 n is a power of 2.

*/
struct pair Method3(int *A,int N)
{
    struct pair MaxMin;
    int i;
    if(N % 2 == 0)
    {
        if(A[0] > A[1])
        {
            MaxMin.min = A[1];
            MaxMin.max = A[0];
        }
        else
        {
            MaxMin.max = A[1];
            MaxMin.min = A[0];
        }
        i = 2;
    }
    else
    {
        MaxMin.max = A[0];
        MaxMin.min = A[0];
        i = 1;
    }
    for(;i<N-1;i += 2)
    {
        if(A[i] > A[i+1])
        {
            if(A[i] > MaxMin.max)
                MaxMin.max = A[i];
            if(A[i+1] < MaxMin.min)
                MaxMin.min = A[i+1];
        }
        else
        {
            if(A[i+1] > MaxMin.max)
                MaxMin.max = A[i+1];
            if(A[i] < MaxMin.min)
                MaxMin.min = A[i];
        }
    }
    return MaxMin;
}

int main(void)
{
    int A[] = {1000,11,445,1,330,3000};
    int N = sizeof(A)/sizeof(A[0]);
   // struct pair MaxMin = FindMaxMin(A,N);
   //struct pair MaxMin = Method2(A,0,N-1);
   struct pair MaxMin = Method3(A,N);
    printf("Maximum is %d\nMinimum is %d\n",MaxMin.max,MaxMin.min);

    return 0;
}
