#include<stdio.h>

void FindSubArray(int *A,int N,int sum)
{
    int cur_sum = A[0],start = 0,i;
    for(i=1;i<N;i++)
    {
        while(cur_sum > sum && start < i-1)
            cur_sum -= A[start++];
        if(cur_sum == sum)
        {
            printf("Sum found between indexes %d and %d\n",start,i-1);
            return;
        }
        cur_sum += A[i];
    }
    printf("No subarray with given sum found\n");
}

int main(void)
{
    int A[] = {1,2,3,4,5,6,7};
    int sum = 7;
    int N = sizeof(A)/sizeof(A[0]);
    FindSubArray(A,N,sum);

    return 0;
}
