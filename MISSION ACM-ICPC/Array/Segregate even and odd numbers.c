#include<stdio.h>

void SegregateArray(int *A,int N)
{
    int start = 0,end = N-1;
    while(end > start)
    {
        while(A[start] %2 == 0)
            start++;
        while(A[end] %2 != 0)
            end--;
        if(end > start)
        {
            int temp = A[start];
            A[start] = A[end];
            A[end] = temp;
            start++;
            end--;
        }
    }
}

int main(void)
{
    int A[] = {12,34,45,9,8,90,3};
    int N = sizeof(A)/sizeof(A[0]);
    SegregateArray(A,N);
    int i;
    for(i=0;i<N;i++)
        printf("%d ",A[i]);
    return 0;
}
