#include<stdio.h>

void Union(int *A,int *B,int N,int M)
{
    int i = 0,j = 0;
    printf("Union : ");
    while(i < N && j < M)
    {
        if(A[i] < B[j])
            printf("%d ",A[i++]);
        else if(B[j] < A[i])
            printf("%d ",B[j++]);
        else
        {
            printf("%d ",A[i++]);
            j++;
        }
    }
    while(i < N)
        printf("%d ",A[i++]);
    while(j < M)
        printf("%d ",B[j++]);
}

void Intersection(int *A,int *B,int N,int M)
{
    int i = 0,j = 0;
    printf("Intersection : ");
    while(i < N && j < M)
    {
        if(A[i] < B[j])
            i++;
        else if(B[j] < A[i])
            j++;
        else
        {
            printf("%d ",A[i++]);
            j++;
        }
    }
}

int main(void)
{
    int A[] = {1,3,4,5,7};
    int B[] = {2,3,5,6};
    int N = sizeof(A)/sizeof(A[0]);
    int M = sizeof(B)/sizeof(B[0]);
    Union(A,B,N,M);
    printf("\n");
    Intersection(A,B,N,M);

    return 0;
}
