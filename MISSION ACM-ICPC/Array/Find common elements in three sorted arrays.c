#include<stdio.h>

void CommonElem(int *A,int *B,int *C,int N1,int N2,int N3)
{
	int i = 0,j = 0,k = 0;
	while(i < N1 && j < N2 && k < N3)
	{
		if(A[i] == B[j] && B[j] == C[k])
		{
			printf("%d ",A[i]);
			i++,j++,k++;
		}
		else if(A[i] < B[j])
			i++;
		else if(B[j] < C[k])
			j++;
		else
			k++;
	}
}

int main(void)
{
	int A[] = {1,5,10,20,40,80};
	int B[] = {6,7,20,80,100};
	int C[] = {3,4,15,20,30,70,80,120};
	int N1 = sizeof(A)/sizeof(A[0]);
	int N2 = sizeof(B)/sizeof(B[0]);
	int N3 = sizeof(C)/sizeof(C[0]);
	CommonElem(A,B,C,N1,N2,N3);
	return 0;
}
