#include<bits/stdc++.h>
using namespace std;

void findLargest(int A[],int N)
{
	sort(A,A+N);
	vector<int> divCount(N,1);
	vector<int> prev(N,-1);
	int max_index = 0;
	for(int i=1;i<N;i++)
	{
		for(int j=0;j<i;j++)
		{
			if(A[i]%A[j] == 0)
			{
				if(divCount[i] < divCount[j] + 1)
				{
					divCount[i] = divCount[j] + 1;
					prev[i] = j;
				}
			}
		}
		if(divCount[max_index] < divCount[i])
			max_index = i;
	}
	int k = max_index;
	while(k >= 0)
	{
		cout<<A[k]<<" ";
		k = prev[k];
	}
}

int main(void)
{
	int A[] = {1,2,17,4};
	int N = sizeof(A)/sizeof(A[0]);
	findLargest(A,N);
	return 0;
}