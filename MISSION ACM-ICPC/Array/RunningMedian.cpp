#include<bits/stdc++.h>
using namespace std;

class cmp
{
	public:
		bool operator()(int &n1,int &n2)
		{
			return (n1 > n2);
		}
};

priority_queue<int,vector<int> > MaxHeap;
priority_queue<int,vector<int>,cmp> MinHeap;

int getMedian(int data,int &m)
{
	int size1 = MaxHeap.size();
	int size2 = MinHeap.size();
	if(size1 == size2)
	{
		if(data < m)
		{
			MaxHeap.push(data);
			m = MaxHeap.top();
		}
		else
		{
			MinHeap.push(data);
			m = MinHeap.top();
		}
	}
	else if(size1 > size2)
	{
		if(data < m)
		{
			MinHeap.push(MaxHeap.top());
			MaxHeap.pop();
			MaxHeap.push(data);
		}
		else
			MinHeap.push(data);
		m = (MinHeap.top() + MaxHeap.top())/2.0;
	}
	else
	{
		if(data < m)
			MaxHeap.push(data);
		else
		{
			MaxHeap.push(MinHeap.top());
			MinHeap.pop();
			MinHeap.push(data);
		}
		m = (MinHeap.top() + MaxHeap.top())/2.0;
	}
	return m;
}


void printMedian(int *A,int N)
{
	
	int median = 0;
	for(int i=0;i<N;i++)
	{
		median = getMedian(A[i],median);
		cout<<median<<" ";
	}
}

int main(void)
{
	int A[] = {5,15,1,3,2,8,7,9,10,6,11,4};
	int N = sizeof(A)/sizeof(A[0]);
	printMedian(A,N);
	
	return 0;
}