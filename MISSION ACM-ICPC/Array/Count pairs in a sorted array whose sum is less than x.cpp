#include<bits/stdc++.h>
using namespace std;

int findPairs(int A[],int N,int x)
{
	int left = 0,right = N-1;
	int ans = 0;
	while(right > left)
	{
		if(A[left] + A[right] < x)
		{
			ans += right-left;
			left++;
		}
		else
			right--;
	}
	return ans;
}

int main(void)
{
	int A[] = {1,2,3,4,5,6,7,8};
	int N = sizeof(A)/sizeof(A[0]);
	int x = 7;
	cout<<findPairs(A,N,x);
	return 0;
}