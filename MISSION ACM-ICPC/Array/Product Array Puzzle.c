#include<stdio.h>

void ProductArray(int *A,int N)
{
    int prod[N+1],i;
    for(i=0;i<N;i++)
        prod[i] = 1;
    int temp = 1;
    for(i=1;i<N;i++)
    {
        temp *= A[i-1];
        prod[i] = temp;

    }
    temp = 1;
    for(i=N-2;i>=0;i--)
    {
        temp *= A[i+1];
        prod[i] *= temp;

    }
    printf("Product Array : ");
    for(i=0;i<N;i++)
        printf("%d ",prod[i]);
}

int main(void)
{
    int A[] = {10,3,5,6,2};
    int N = sizeof(A)/sizeof(A[0]);
    ProductArray(A,N);
    return 0;
}
