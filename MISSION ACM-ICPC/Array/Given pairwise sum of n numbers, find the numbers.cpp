#include<bits/stdc++.h>
using namespace std;

void findNumbers(int A[],int N)
{
	int ans[N];
	int b_minus_a = A[N-1]-A[1];
	ans[0] = (A[0] - b_minus_a)/2;
	for(int i=0;i<N-1;i++)
		ans[i+1] = A[i]-ans[0];
	for(int i=0;i<N;i++)
		cout<<ans[i]<<" ";
}

int main(void)
{
	int A[] = {13,10,14,9,17,21,16,18,13,17};
	int N = 5;
	findNumbers(A,N);
	return 0;
}