#include<bits/stdc++.h>
using namespace std;

int LongestCommonSum(bool *A,bool *B,int N)
{
	int maxLen = 0;
	int sumA = 0,sumB = 0;
	int diff[2*N+1];
	memset(diff,-1,sizeof(diff));
	for(int i=0;i<N;i++)
	{
		sumA += A[i];
		sumB += B[i];
		int cur_diff = sumA - sumB;
		int diffIndex = N+cur_diff;
		if(cur_diff == 0)
			maxLen = i+1;
		else if(diff[diffIndex] == -1)
			diff[diffIndex] = i;
		else
		{
			int len = i-diff[diffIndex];
			maxLen = max(maxLen,len);
		}
	}
	return maxLen;
}

int main(void)
{	
	bool A[] = {0,1,0,1,1,1,1};
	bool B[] = {1,1,1,1,1,0,1};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<LongestCommonSum(A,B,N);
	return 0;
}