#include<bits/stdc++.h>
using namespace std;

int minSwaps(int A[],int N)
{
	pair<int,int> arrPos[N];
	for(int i=0;i<N;i++)
	{
		arrPos[i].first = A[i];
		arrPos[i].second = i;
	}
	sort(arrPos,arrPos+N);
	vector<bool> visited(N,false);
	int ans = 0;
	for(int i=0;i<N;i++)
	{
		if(visited[i] || arrPos[i].second == i)
			continue;
		int size = 0;
		int index = i;
		while(!visited[index])
		{
			visited[index] = true;
			index = arrPos[index].second;
			size++;
		}
		ans += (size-1);
	}
	return ans;
}

int main(void)
{
	int A[] = {1,5,4,3,2};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<minSwaps(A,N);
	return 0;
}