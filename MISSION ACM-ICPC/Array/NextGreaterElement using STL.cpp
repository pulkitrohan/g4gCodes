#include<cstdio>
#include<stack>

using namespace std;
void NextGreaterElement(int *A,int N)
{
    stack<int> S;
    S.push(A[0]);
    int i;
    for(i=1;i<N;i++)
    {
        int next = A[i];
        while(!S.empty() && S.top() < next)
        {
            printf("%2d\t%2d\n",S.top(),next);
            S.pop();
        }
        S.push(next);
    }
    while(!S.empty())
    {
        int k = -1;
        printf("%2d\t%d\n",S.top(),k);
        S.pop();
    }
}

int main(void)
{
    int A[] = {4,5,2,25};
    int N = sizeof(A)/sizeof(A[0]);
    NextGreaterElement(A,N);
    return 0;
}
