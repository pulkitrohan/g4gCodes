#include<stdio.h>
/*



A Very Simple DP Solution.
Time : O(n).
Space: O(1).

Please go through this algorithm.
Let sum[i] represent maximum non-consecutive subsequence sum till ith element of input[], then
sum[i] = max(sum[i-1], input[i] + sum[i-2])

which says that new sum would either be obtained by not including ith element i.e previous sum, or
by including it with last to previous sum i.e input[i-2]. The new sum would be maximum of these two possibilities.

Since space complexity is O(1), instead of using sum[] array, we only need 3 variables, to store current,
last and second last values of sum.
I m using 3 variables here.
a -> for (i-2)th index.
b -> for (i-1)th index.
c -> for ith index. ( This stores our answer).


inc => Max sum including previous element
exc1 = Max sum excluding previous element
*/


#define max(a,b) (a > b ? a : b)

int MaxSum(int *A,int start,int end)
{
    int dp[2] = {a[0],a[1]};
	for(int i=2;i<a.size();i++)
	{
		int temp = dp[1];
		dp[1] = dp[0]+a[i];
		dp[0] = max(dp[0],temp);
	}
	int ans = max(dp[0],dp[1]);
	return ans;
}

int main(void)
{
    int A[] = {3,2,5,10,7};
    int len = sizeof(A)/sizeof(A[0]);
    printf("Maximum Sum is : %d\n",MaxSum(A,0,len));
    return 0;
}
