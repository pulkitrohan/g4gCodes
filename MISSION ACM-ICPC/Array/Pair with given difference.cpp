#include<cstdio>
#include<algorithm>

using namespace std;


void FindPair(int *A,int N,int diff)
{
    sort(A,A+N);
    int i = 0,j = 1;
    while(i < N && j < N)
    {
        if(i != j && A[j]-A[i] == diff)
        {
            printf("%d %d\n",A[j],A[i]);
            i++,j++;
        }
        else if(A[j]-A[i] < diff)
            j++;
        else
            i++;
    }
    //printf("No Such Pair Found\n");
}

int main(void)
{
    int A[] = {8,12,16,4,0,20};
    int N = sizeof(A)/sizeof(A[0]);
    int diff = 4;
    FindPair(A,N,diff);
    return 0;
}
