#include<stdio.h>

int max(int A,int B)
{
    return (A > B) ? A : B;
}

int MaxSubArraySum(int *A,int N)    //Doesnt Handle Case of all Negative Integers
{
    int max_so_far = 0,max_ending_here = 0,i,start=0,end=0,a=0;
    for(i=0;i<N;i++)
    {
        max_ending_here += A[i];
        if(max_ending_here < 0)
        {
            max_ending_here = 0;
            a = i+1;
        }
        else if(max_ending_here > max_so_far)
        {
            max_so_far = max_ending_here;
			start = a;
            end = i;
        }
    }
    printf("Starting Index : %d\nEnding Index : %d\n",start,end);
    return max_so_far;
}


int MaxSumArraySumWithAllNegatives(int *A,int N)
{
    int sum = 0,a = 0,start,end,maxsum = INT_MIN;
	for(int i=0;i<N;i++)
	{
		sum += A[i];
		if(sum < 0)
		{	
			sum = 0;
			a = i+1;
		}
		else if(sum > maxsum)
		{
			maxsum = sum;
			start = a;
			end = i;
		}
}


int main(void)
{
    int A[] = {-2,-3,4,-1,2,-1,-5,-3};
	int N = sizeof(A)/sizeof(A[0]);
    printf("%d",MaxSubArraySum(A,N));
}
