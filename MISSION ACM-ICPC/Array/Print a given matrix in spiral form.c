#include<stdio.h>

PrintSpiral(int A[4][6],int N,int M)
{
    int i = 0,j = 0,k;
    while(i < N && j < M)
    {
        for(k = 0;k<M;k++)
            printf("%d ",A[j][k]);
        i++;
        j++;
        if(j < N)
        {
            for(k = M-1;k>=0;k--)
            printf("%d ",A[j][k]);
            i++;
            j++;
        }
    }
}

int main(void)
{
    int MAT[4][6] = {{1,2,3,4,5,6},{7,8,9,10,11,12},{13,14,15,16,17,18}};
    PrintSpiral(MAT,3,6);
    return 0;
}
