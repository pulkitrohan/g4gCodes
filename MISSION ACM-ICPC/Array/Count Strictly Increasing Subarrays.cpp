#include<bits/stdc++.h>
using namespace std;

int countIncreasing(int *A,int N)
{
	int count = 0,len = 1;
	for(int i=0;i<N-1;i++)
	{
		if(A[i+1] > A[i])
			len++;
		else
		{
			count += ((len-1)*len)/2;
			len = 1;
		}
	}
	count += ((len-1)*len)/2;
	return count;
}

int main(void)
{
	int A[] = {1,2,2,4};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<countIncreasing(A,N);
	return 0;
}