#include<bits/stdc++.h>
using namespace std;

int MaxIncreasingSubseq(int *A,int N)
{
	int LSL[N],LGR[N];
	int largest = -1;
	for(int i=N-1;i>=0;i--)
	{
		if(largest == -1 || A[i] < A[largest])
			LGR[i] = largest;
		else
			LGR[i] = -1;
		if(largest == -1 || A[i] > A[largest])
			largest = i;
	}
	stack<int> S;
	for(int i = 0;i<N;i++)
	{
		if(LGR[i] == -1)
		{
			LSL[i] = -1;
			continue;
		}
		int max = -1;
		while(!S.empty() && A[S.top()] < A[i])
		{
			max = S.top();
			S.pop();
		}
		LSL[i] = max;
		S.push(i);
	}
	int maxproduct = -1;
	int maxi = -1,maxj = -1,maxk = -1;
	for(int i=1;i<N-1;i++)
	{
		if(LSL[i] != -1 && LGR[i] != -1)
		{
			if(maxproduct < A[LSL[i]]*A[i]*A[LGR[i]])
			{
				maxi = LSL[i];
				maxj = i;
				maxk = LGR[i];
				maxproduct = A[LSL[i]]*A[i]*A[LGR[i]];
			}
		}
	}
	if(maxproduct == -1)
		cout<<"No such sequence exist\n";
	else
		cout<<A[maxi]<<" "<<A[maxj]<<" "<<A[maxk]<<" "<<maxproduct<<endl;
}
	
int main(void)
{
	int A[] = {6,7,8,1,2,3,9,10};
	int N = sizeof(A)/sizeof(A[0]);
	MaxIncreasingSubseq(A,N);
	return 0;
}