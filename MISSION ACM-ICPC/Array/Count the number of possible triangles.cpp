#include<cstdio>
#include<algorithm>

using namespace std;


int CountTriangles(int *A,int N)
{
    sort(A,A+N);
    int i,j,k,count = 0;
    for(i=0;i<N-2;i++)
    {
        k = i+2;
        for(j=i+1;j<N-1;j++)
        {
            while(k < N && A[i] + A[j] > A[k])
                k++;

            count += k-j-1;
        }
    }
    return count;
}

int main(void)
{
    int A[] = {4,6,3,7};
    int N = sizeof(A)/sizeof(A[0]);
    printf("%d",CountTriangles(A,N));
    return 0;
}
