#include<bits/stdc++.h>
using namespace std;

void sortInWave(int *A,int N)
{
	for(int i=0;i<N;i+=2)
	{
		if(i>0 && A[i] < A[i-1])
			swap(A[i],A[i-1]);
		if(i<N-1 && A[i+1] > A[i])
			swap(A[i],A[i+1]);
	}
}

int main(void)
{
	int A[] = {5,1,3,2,4};
	int N = sizeof(A)/sizeof(A[0]);
	sortInWave(A,N);
	for(int i=0;i<N;i++)
		cout<<A[i]<<" ";
	return 0;
}
