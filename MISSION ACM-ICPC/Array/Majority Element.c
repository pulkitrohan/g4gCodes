#include<stdio.h>

void CheckMajority(int *A,int N)
{
    int maj_index = 0,count = 1,i;
    for(i=1;i<N;i++)
    {
        if(A[i] == A[maj_index])
            count++;
        else
            count--;
        if(count == 0)
        {
            maj_index = i;
            count = 1;
        }
    }
    if(CheckIfMajority(A,A[maj_index],N))
        printf("%d is Majority Element\n",A[maj_index]);
    else
        printf("No Majority Element\n");
}

int CheckIfMajority(int *A,int cand,int N)
{
    int count = 0,i;
    for(i=0;i<N;i++)
    {
        if(A[i] == cand)
            count++;
        if(count > N/2)
            return 1;
    }
    return 0;
}

int main(void)
{
    int N,i;
    printf("Enter the size of Array : ");
    scanf("%d",&N);
    int A[N+1];
    if(N)
    {
        printf("Enter the Array Elements : ");
        for(i=0;i<N;i++)
            scanf("%d",&A[i]);
        CheckMajority(A,N);
    }
    return 0;
}
