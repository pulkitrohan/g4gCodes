#include<stdio.h>
#include<stdlib.h>

void PrintTwoElements(int *A,int N)
{
    int i;
    for(i=0;i<N;i++)
    {
        if(A[abs(A[i]) - 1] > 0)
            A[abs(A[i]) - 1] = -A[abs(A[i]) - 1];
        else
            printf("Repeated Element : %d\n",abs(A[i])); //Repeated Element
    }
    for(i=0;i<N;i++)
    {
        if(A[i] > 0)
            printf("Missing Element : %d\n",i+1); //Missing Element
    }
}

int main(void)
{
    int A[] = {4,3,6,2,1,1};
    int N= sizeof(A)/sizeof(A[0]);
    PrintTwoElements(A,N);

    return 0;
}
