//Create a left Array which contains min element at an index among all the elements on its left
//Create a right Array which contains max element at an index among all the elements on its right
//Compare the two arrays and if right element is greater than left element then calculate diff and replace it with maxdiff if it
//greater than it.
//Advice : Try printing left and right array and then see the code once.

#include<stdio.h>
#define Min(a,b) (a > b ? b : a)
#define Max(a,b) (a > b ? a : b)

void FindMax(int *A,int N)
{
    int left[N+1],right[N+1],i,j,diff;
    left[0] = A[0],right[N-1] = A[N-1];
    for(i=1;i<N;i++)
        left[i] = Min(left[i-1],A[i]);
    for(i=N-2;i>=0;i--)
        right[i] = Max(right[i+1],A[i]);

    for(i=0;i<N;i++)
        printf("%d ",left[i]);
    printf("\n");
    for(i=0;i<N;i++)
        printf("%d ",right[i]);
    printf("\n");
    i = 0,j = 0,diff = -1;
    while(i < N && j < N)
    {
        if(left[i] < right[j])
        {
            diff = Max(diff,j-i);
            j++;
        }
        else
            i++;
    }
    printf("Maximum Difference j - i : %d",diff);
}

int main(void)
{
    int A[] = {34,8,10,3,2,80,30,33,1};
    int N = sizeof(A)/sizeof(A[0]);
    FindMax(A,N);
    return 0;
}
