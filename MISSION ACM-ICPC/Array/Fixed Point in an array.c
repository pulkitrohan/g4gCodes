#include<stdio.h>

int HasFixedPoint(int *A,int N)
{
    int low = 0,high = N-1;
    while(high >= low)
    {
        int mid = (low+high)/2;
        if(A[mid] == mid)
            return mid;
        else if(A[mid] > mid)
            high = mid - 1;
        else
            low = mid + 1;
    }
    return -1;
}

int main(void)
{
    int A[] = {-10,-5,3,4,7,9};
    int N = sizeof(A)/sizeof(A[0]);
    if(HasFixedPoint(A,N) == -1)
        printf("There is no fixed point\n");
    else
        printf("Fixed point : %d\n",HasFixedPoint(A,N));
    return 0;
}
