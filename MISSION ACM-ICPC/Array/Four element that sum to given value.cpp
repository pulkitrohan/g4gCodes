#include<bits/stdc++.h>
using namespace std;

struct element
{
	int sum;
	int f,s;
};

bool cmp(struct element P1,struct element P2)
{	
	return (P2.sum > P1.sum);
}

int NoCommon(struct element P1,struct element P2)
{
	if(P1.f == P2.f || P1.f == P2.s || P2.f == P1.s || P2.s == P1.s)
		return 0;
	return 1;
}

void FindFourElements(int *A,int N,int X)
{
	int size = (N*(N-1))/2;
	struct element P[size];
	int count = 0;
	for(int i=0;i<N-1;i++)
	{
		for(int j=i+1;j<N;j++)
		{
			P[count].sum = A[i] + A[j];
			P[count].f = i;
			P[count].s = j;
			count++;
		}
	}
	sort(P,P+count,cmp);
	int i = 0,j = count-1;
	while(i < count &&  j >= 0)
	{
		if(P[i].sum + P[j].sum == X && NoCommon(P[i],P[j]))
		{
			printf("%d %d %d %d\n",A[P[i].f],A[P[i].s],A[P[j].f],A[P[j].s]);
			return;
		}
		else if(P[i].sum + P[j].sum < X)
			i++;
		else
			j--;
	}
}


int main(void)
{
	int A[] = {10,20,30,40,1,2};
	int N = sizeof(A)/sizeof(A[0]);
	int X = 91;
	FindFourElements(A,N,X);
	return 0;
}