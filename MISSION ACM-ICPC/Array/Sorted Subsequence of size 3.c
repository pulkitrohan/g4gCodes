#include<stdio.h>
#include<stdlib.h>

void SortedSubseq(int *A,int N)
{
    int *greater = (int *)malloc(sizeof(int) * N);
    int *smaller = (int *)malloc(sizeof(int) * N);
    int min = 0,max = N-1,i;
    smaller[min] = -1;
    for(i=1;i<N;i++)
    {
        if(A[i] > A[min])
            smaller[i] = min;
        else
        {
            min = i;
            smaller[i] = -1;
        }
    }
    greater[N-1] = -1;
    for(i=N-2;i>=0;i--)
    {
        if(A[i] < A[max])
            greater[i] = max;
        else
        {
            max = i;
            greater[i] = -1;
        }
    }
    for(i=0;i<N;i++)
    {
        if(smaller[i] != -1 && greater[i] != -1)
        {
            printf("%d %d %d",A[smaller[i]],A[i],A[greater[i]]);
            return;
        }
    }
    printf("No such triplet found\n");
}

int main(void)
{
    int A[] = {12,11,10,5,6,2,30};
    int N = sizeof(A)/sizeof(A[0]);
    SortedSubseq(A,N);
    return 0;
}
