#include<cstdio>
#include<algorithm>

using namespace std;

void PrintTriplet(int *A,int N,int sum)
{
    sort(A,A+N);
    int i,j,k;
    for(i=0;i<N-2;i++)
    {
        k = N-1;
        for(j=i+1;j<N-1;j++)
        {
            if(A[i]+A[j]+A[k] == sum)
            {
                printf("Triplet : %d,%d,%d\n",A[i],A[j],A[k]);
                return;
            }
            else if(A[i]+A[j]+A[k] > sum)
                k--;
            else
                j++;
        }
    }
    printf("No Triplet found\n");
}

int main(void)
{
    int A[] = {12,3,4,1,6,9};
    int N = sizeof(A)/sizeof(A[0]);
    int sum = 24;
    PrintTriplet(A,N,sum);
    return 0;
}
