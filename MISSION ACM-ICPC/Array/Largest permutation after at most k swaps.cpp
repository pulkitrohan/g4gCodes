#include<bits/stdc++.h>
using namespace std;

void kSwapPermutation(int A[],int N,int k)
{
	int pos[N+1];
	for(int i=0;i<N;i++)
		pos[A[i]] = i;
	for(int i=0;i<N && k;i++)
	{
		if(A[i] == N-i)
			continue;
		int temp = pos[N-i];
		pos[A[i]] = pos[N-i];
		pos[N-i] = i;
		swap(A[temp],A[i]);
		k--;
	}
	for(int i=0;i<N;i++)
		cout<<A[i]<<" ";
}

int main(void)
{
	int A[] = {4,5,2,1,3};
	int N = sizeof(A)/sizeof(A[0]);
	int k = 3;
	kSwapPermutation(A,N,k);
	return 0;
}