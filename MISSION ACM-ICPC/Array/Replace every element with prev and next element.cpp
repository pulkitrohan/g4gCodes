#include<bits/stdc++.h>
using namespace std;

void MultTwo(int *A,int N)
{
	if(N < 2)
		return;
	int prev = A[0];
	A[0] = A[0]*A[1];
	int cur;
	for(int i=1;i<N-1;i++)
	{
		cur = A[i];
		A[i] = prev*A[i+1];
		prev = cur;
	}
	A[N-1] = prev*A[N-1];
}

int main(void)
{
	int A[] = {2,3,4,5,6};
	int N = sizeof(A)/sizeof(A[0]);
	for(int i=0;i<N;i++)
		cout<<A[i]<<" ";
	cout<<endl;
	MultTwo(A,N);
	for(int i=0;i<N;i++)
		cout<<A[i]<<" ";
	cout<<endl;
	return 0;
}