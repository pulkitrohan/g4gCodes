#include<stdio.h>

void MinimumLen(int *A,int N)
{
    int s,e,i;
    for(s=0;s<N-1;s++)
        if(A[s] > A[s+1])
            break;
    for(e=N-1;e>0;e--)
        if(A[e-1] > A[e])
            break;
    int max = A[s],min = A[s];
    for(i=s+1;i<=e;i++)
    {
        if(A[i] > max)
            max = A[i];
        if(A[i] < min)
            min = A[i];
    }
    for(i=0;i<s;i++)
    {
        if(A[i] > min)
        {
            s = i;
            break;
        }
    }
    for(i=N-1;i>e;i--)
    {
        if(A[i] < max)
        {
            e = i;
            break;
        }
    }
   printf("%d %d\n",s,e);

}

int main(void)
{
    int A[] = {10,12,20,30,25,40,32,31,35,50,60};
    int N = sizeof(A)/sizeof(A[0]);
    MinimumLen(A,N);
    return 0;
}
