#include<bits/stdc++.h>
using namespace std;

void zigzag(int A[],int N)
{
	bool flag = true;
	for(int i=0;i<N-1;i++)
	{
		if(flag)
		{
			if(A[i] > A[i+1])
				swap(A[i],A[i+1]);
		}
		else
		{
			if(A[i] < A[i+1])
				swap(A[i],A[i+1]);

		}
		flag = !flag;
	}
}

int main(void)
{
	int A[] = {4,3,7,8,6,2,1};
	int N = sizeof(A)/sizeof(A[0]);
	zigzag(A,N);
	for(int i=0;i<N;i++)
		cout<<A[i]<<" ";
	return 0;
}