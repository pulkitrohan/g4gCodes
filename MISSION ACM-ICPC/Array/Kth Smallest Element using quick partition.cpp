#include<bits/stdc++.h>
using namespace std;
int RandomizedPivot(int *A,int l,int r)
{
    int i = l,j,temp;
	int pivot = A[l];
    for(j=l+1;j<=r;j++)
    {
        if(A[j] < pivot)
        {
            i++;
            swap(A[i],A[j]);
        }
    }
    swap(A[i],A[l]);
    return i;
}

int RandomizedSelect(int *A,int l,int r,int k)
{
    if(l == r)
        return A[l];
    int q = RandomizedPivot(A,l,r);
    if(q+1 == k)
        return A[q];
    else if(q + 1 > k)
        return RandomizedSelect(A,l,q-1,k);
    else
        return RandomizedSelect(A,q+1,r,k);
}

int main(void)
{
     int A[] = {7,10,4,3,20,5};
	int N = sizeof(A)/sizeof(A[0]);
	int k = 1;
	if(k == 1)
    {
        int i,max2 = INT_MIN;
        for(i=0;i<N;i++)
            if(A[i] > max2)
                max2 = A[i];
        return max2;
    }
    if(N == 2)
    {
        if(k == 1)
            return max(A[0],A[1]);
        else
            return min(A[0],A[1]);
    }
    cout<<RandomizedSelect(A,0,N-1,k)<<endl;
    return 0;
}
