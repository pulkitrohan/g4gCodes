#include<stdio.h>

int main(void)
{
    int MAT[4][4] = {{0,1,1,1},{0,0,1,1},{1,1,1,1},{0,0,0,0}};
    int N = 4,M = 4;
    int max_row_index = 0,i,j;
    for(i=0;i<M;i++)
        if(MAT[0][i] == 1)
            break;
    if(i == M)
        j = M-1;
    else
        j = i;
    for(i=1;i<N;i++)
    {
        while(j >= 0 && MAT[i][j] == 1)
        {
            j--;
            max_row_index = i;
        }
    }
    printf("%d",max_row_index);
    return 0;
}
