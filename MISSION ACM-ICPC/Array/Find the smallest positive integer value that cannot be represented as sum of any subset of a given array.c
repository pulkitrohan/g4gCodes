#include<stdio.h>

int SmallestNum(int *A,int N)
{
	int res = 1,i;
	for(i=0;i<N;i++)
	{
		if(A[i] > res)
			return res;
		res += A[i];
	}
	return res;
}

int main(void)
{
	int A[] = {1,3,6,10,11,15};
	int N = sizeof(A)/sizeof(A[0]);
	printf("%d\n",SmallestNum(A,N));
	return 0;
}
