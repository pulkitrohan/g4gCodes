#include<bits/stdc++.h>
using namespace std;

vector<int> constructArray(int *pair,int N)
{
	vector<int> V;
	V.push_back((pair[0]+pair[1]-pair[N-1])/2);
	for(int i=1;i<N;i++)
		V.push_back(pair[i-1]-V[0]);
	return V;
}

int main(void)
{
	int pair[] = {15,13,11,10,12,10,9,8,7,5};
	int N = 5;
	vector<int> V = constructArray(pair,N);
	for(int i=0;i<V.size();i++)
		cout<<V[i]<<" ";
	return 0;
}
