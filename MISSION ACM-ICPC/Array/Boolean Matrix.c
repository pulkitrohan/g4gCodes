#include<stdio.h>

void PrintBoolean(int Mat[3][4],int N,int M)
{
    int colflag = 0,rowflag = 0,i,j;
    for(i=0;i<N;i++)
    {
        if(Mat[i][0] == 1)
        {
            colflag = 1;
            break;
        }
    }
    for(i=0;i<M;i++)
    {
        if(Mat[0][i] == 1)
        {
            rowflag = 1;
            break;
        }
    }



    for(i=1;i<N;i++)
    {
        for(j=1;j<M;j++)
        {
            if(Mat[i][j] == 1)
            {
                Mat[i][0] = 1;
                Mat[0][j] = 1;
            }
        }
    }
    for(i=1;i<N;i++)
    {
        for(j=1;j<M;j++)
        {
            if(Mat[i][0] == 1 || Mat[0][j] == 1)
                Mat[i][j] = 1;
        }
    }
    if(rowflag)
    {
        for(i=0;i<M;i++)
            Mat[0][i] = 1;
    }
    if(colflag)
    {
        for(i=0;i<N;i++)
            Mat[i][0] = 1;
    }
    for(i=0;i<N;i++)
    {
        for(j=0;j<M;j++)
            printf("%d ",Mat[i][j]);
        printf("\n");
    }
}

int main(void)
{
    int Mat[3][4] = {{0,0,0,1},{1,0,0,0},{1,0,0,0}};
    PrintBoolean(Mat,3,4);
    return 0;
}
