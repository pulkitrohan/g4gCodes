#include<bits/stdc++.h>
using namespace std;

int FindMaxAverage(int A[],int N,int k)
{
	if(k > N)
		return -1;
	int sum = A[0];
	for(int i=1;i<k;i++)
		sum += A[i];
	int max_sum = sum,end = k-1;
	for(int i=k;i<N;i++)
	{
		sum = sum + A[i] - A[i-k];
		if(sum > max_sum)
		{
			max_sum = sum;
			end = i;
		}
	}
	return (end-k+1);
}

int main(void)
{	
	int A[] = {1,12,-5,-6,50,3};
	int k = 4;
	int N = sizeof(A)/sizeof(A[0]);
	cout<<FindMaxAverage(A,N,k)<<endl;
	return 0;
}