#include<bits/stdc++.h>
using namespace std;

void printMaxOfMin(int A[],int N)
{
	stack<int> S;
	vector<int> left(N+1,-1),right(N+1,N);
	for(int i=0;i<N;i++)
	{
		while(!S.empty() && A[S.top()] >= A[i])
			S.pop();
		if(!S.empty())
			left[i] = S.top();
		S.push(i);
	}
	while(!S.empty())
		S.pop();
	for(int i=N-1;i>=0;i--)
	{
		while(!S.empty() && A[S.top()] >= A[i])
			S.pop();
		if(!S.empty())
			right[i] = S.top();
		S.push(i);
	}
	vector<int> ans(N+1,0);
	for(int i=0;i<N;i++)
	{
		int len = right[i]-left[i]-1;
		ans[len] = max(ans[len],A[i]);
	}
	for(int i=N-1;i>=1;i--)
		ans[i] = max(ans[i],ans[i+1]);
	for(int i=1;i<=N;i++)
		cout<<ans[i]<<" " ;
}

int main(void)
{
	int A[] = {10,20,30,50,10,70,30};
	int N = sizeof(A)/sizeof(A[0]);
	printMaxOfMin(A,N);
	return 0;
}