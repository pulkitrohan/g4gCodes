#include<stdio.h>
/*
//Linear Search => O(N) Solution
void FindSmallest(int *A,int N)
{
    int i;
    for(i=0;i<N;i++)
    {
        if(A[i] != i)
        {
            printf("Smallest Element : %d\n",i);
            return;
        }
    }
    if(i == N)  //All elements are consecutive
        printf("Smallest Element : %d\n",N);
}
*/
//BinarySearch
void FindSmallest(int *A,int low,int high)
{
    while(high >= low)
    {
        if(A[low] != low)
        {
            printf("Smallest Element : %d\n",low);
            return ;
        }
        int mid = (low + high)/2;
        if(A[mid] > mid)
            high = mid;
        else
            low = mid +1;
    }
    printf("Smallest Element : %d\n",high+1);   //when elements are consecutive
}


int main(void)
{
    int A[] = {0,1,2,3};
    int N = sizeof(A)/sizeof(A[0]);
    //FindSmallest(A,N);
    FindSmallest(A,0,N-1);
    return 0;
}
