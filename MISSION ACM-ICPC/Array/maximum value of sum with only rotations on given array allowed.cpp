#include<bits/stdc++.h>
using namespace std;

int MaxSum(int *A,int N)
{
	int sum = 0,cur_val = 0;
	for(int i=0;i<N;i++)
	{
		sum += A[i];
		cur_val += i*A[i];
	}
	int max_val = cur_val;
	for(int j=1;j<N;j++)
	{
		cur_val += sum - N*A[N-j];
		max_val = max(max_val,cur_val);
	}
	return max_val;
}

int main(void)
{
	int A[] = {10,1,2,3,4,5,6,7,8,9};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<MaxSum(A,N);
	return 0;
}