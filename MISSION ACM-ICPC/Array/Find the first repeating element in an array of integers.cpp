#include<bits/stdc++.h>
using namespace std;

void FirstRepeatingElement(int *A,int N)
{
	set<int> S;
	int min = -1;
	for(int i=N-1;i>=0;i--)
	{
		if(S.find(A[i]) != S.end())
			min = i;
		else
			S.insert(A[i]);
	}
	if(min == -1)
		printf("No Repeating Element\n");
	else
		printf("%d\n",A[min]);
}

int main(void)
{
	int A[] = {10,5,3,4,3,5,6};
	int N = sizeof(A)/sizeof(A[0]);
	FirstRepeatingElement(A,N);
	return 0;
}
