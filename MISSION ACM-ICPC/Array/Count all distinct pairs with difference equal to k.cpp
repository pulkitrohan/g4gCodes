//We will Discuss two Solution
//One Solution using Sorting => Time Complexity => O(NLogN)
//One Solution using Hashing => Time Complexity => O(N) Space Complexity => O(M)
// M =>Range of Numebers

#include<bits/stdc++.h>

using namespace std;

void CountPairs(int *A,int N,int k)
{
    sort(A,A+N);
    int i,j,count = 0;
    for(i=0,j=1;i < N && j < N;)
    {
        if(i != j && A[j]-A[i] == k)
        {
            count++;
            j++;
        }
        else if(A[j]-A[i] < k)
            j++;
        else
            i++;
    }
    printf("Count : %d\n",count);
}
//Using Hashing

/*
A - B = K
A = B + K
B = A - K
*/
#define MAX 100000  //  Range of Elements assumed
void CountPairsMethod2(int *A,int N,int k)
{
    set<int> S;
	int count = 0;
	for(int i=0;i<N;i++)
	{
		if(S.find(A[i]-k) != S.end())
			count++;
		if(S.find(A[i]+k) != S.end())
			count++;
		S.insert(A[i]);
	}
	cout<<count<<endl;
}
int main(void)
{
    int A[] = {8,12,16,4,0,20};
    int N = sizeof(A)/sizeof(A[0]);
    int k = 4;
    CountPairsMethod2(A,N,k);
    return 0;
}
