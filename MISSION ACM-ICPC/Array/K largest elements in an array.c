#include<cstdio>
#include<algorithm>
using namespace std;
int RandomizedPivot(int *A,int l,int r)
{
    int PivotIndex = l + (rand() % (r - l +1)); //Randomized Pivot to ensure Expected Running time to be O(N).
    int pivot = A[PivotIndex];
    swap(A[PivotIndex],A[l]);
    int i = l,j,temp;
    for(j=l;j<=r;j++)
    {
        if(A[j] >= pivot)
        {
            i++;
            swap(A[i],A[j]);
        }
    }
    swap(A[i],A[l]);
    return i;
}

void RandomizedSelect(int *A,int l,int r,int k)
{
    if(l == r)
        return;
    int q = RandomizedPivot(A,l,r);
    if(q+1 == k)
        return;
    else if(q + 1 > k)
        RandomizedSelect(A,l,q-1,k);
    else
        RandomizedSelect(A,q+1,r,k);
}

int main(void)
{
    int N,i,k;
    printf("Enter the size of Array : ");
    scanf("%d",&N);
    int A[N+1];
    printf("Enter the Elements : ");
    for(i=0;i<N;i++)
        scanf("%d",&A[i]);
    printf("Enter the order statistics to be found : ");
    scanf("%d",&k);
    RandomizedSelect(A,0,N-1,k);
    for(i=0;i<k;i++)
        printf("%d ",A[i]);
    return 0;
}
