#include<cstdio>
#include<algorithm>

using namespace std;
int BinarySearch(int *A,int N,int x)
{
    int low = 0,high = N-1;
    while(high >= low)
    {
        int mid = (low+high)/2;
        if(A[mid] == x)
            return 1;
        else if(A[mid] > x)
            high = mid - 1;
        else
            low = mid + 1;
    }
    return -1;
}
//Method 2
/*
//Complexity : O(NLogN + MLogN)
//Cannot Handle Duplicates.
int IsSubset(int *A,int *B,int N,int M)
{
    sort(A,A+N);
    int i;
    for(i=0;i<M;i++)
    {
        int index = BinarySearch(A,N,B[i]);
        if(index == -1)
            return 0;
    }
    return 1;
}*/
//Method 3
//Best Method
//Complexity : O(NLogN + MLogM)
int IsSubset(int *A,int *B,int N,int M)
{
    sort(A,A+N);
    sort(B,B+M);
    int i = 0,j = 0;
    while(i < N && j < M)
    {
        if(A[i] < B[j])
            i++;
        else if(A[i] > B[j])
            return 0;
        else
            i++,j++;
    }
    if(j == M)
        return 1;
    else
        return 0;
}

int main(void)
{
    int A[] = {10,5,2,23,19};
    int B[] = {19,5,3};
    int N = sizeof(A)/sizeof(A[0]);
    int M = sizeof(B)/sizeof(B[0]);
    if(IsSubset(A,B,N,M))
        printf("Yes,it is a subset\n");
    else
        printf("No,not a subset\n");
    return 0;
}
