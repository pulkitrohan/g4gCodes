#include<bits/stdc++.h>
using namespace std;

int findLength(int *A,int N)
{
	int maxLen = 1;
	for(int i=0;i<N-1;i++)
	{
		int maxNum = A[i],minNum = A[i];
		for(int j=i+1;j<N;j++)
		{
			maxNum = max(maxNum,A[j]);
			minNum = min(minNum,A[j]);
			if((maxNum-minNum) == j-i)
				maxLen = max(maxLen,j-i+1);
		}
	}
	return maxLen;
}

int findLengthDuplicates(int *A,int N)
{
	int maxLen = 1;
	for(int i=0;i<N-1;i++)
	{
		set<int> S;
		int maxNum = A[i],minNum = A[i];
		S.insert(A[i]);
		for(int j=i+1;j<N;j++)
		{
			if(S.find(A[j]) != S.end())
				break;
			S.insert(A[j]);
			maxNum = max(maxNum,A[j]);
			minNum = min(minNum,A[j]);
			if((maxNum-minNum) == j-i)
				maxLen = max(maxLen,j-i+1);
		}
	}
	return maxLen;
}

int main(void)
{
	int A[] = {1,56,58,57,90,92,94,93,91,45};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<findLength(A,N)<<endl;
	int B[] = {10, 12, 12, 10, 10, 11, 10};
	int M = sizeof(B)/sizeof(B[0]);
	cout<<findLengthDuplicates(B,M)<<endl;
	return 0;
}