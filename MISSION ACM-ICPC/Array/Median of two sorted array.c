#include<stdio.h>
#define max(a,b) ((a > b) ? a : b)
#define min(a,b) ((a > b) ? b : a)
/*
if N = 1 return Average of first element of Array
if N = 2 return (Max(A[0],B[0]) + Min(A[1],B[1]))/2;
Calculate Median of A and B.
if m1 == m2 return either of m1 or m2
else if m1 > m2 return GetMedian(A,B+N/2,N-N/2) when N is odd
                return GetMedian(A,B+N/2-1,N-N/2+1) when N is even.
else
                return GetMedian(A+N/2,B,N-N/2) when N is odd
                return GetMedian(A+N/2-1,N,N-N/2+1) when N is even

*/
int Median(int *A,int N)
{
    if(N%2 == 0)
        return (A[N/2] + A[N/2-1])/2;
    else
        return A[N/2];
}

int GetMedian(int *A,int *B,int N)
{
    if(N <= 0)
        return -1;
    if(N == 1)
        return (A[0]+B[0])/2;
    if(N == 2)
        return (max(A[0],B[0]) + min(A[1],B[1]))/2;
    int m1 = Median(A,N);
    int m2 = Median(B,N);
    if(m1 == m2)
        return m1;
    else if(m1 > m2)
    {
        if(N % 2 == 0)
            return GetMedian(A,B+N/2-1,N-N/2+1);
        else
            return GetMedian(A,B+N/2,N-N/2);
    }
    else
    {
        if(N%2 == 0)
            return GetMedian(A+N/2-1,B,N-N/2+1);
        else
            return GetMedian(A+N/2,B,N-N/2);
    }
}

int main(void)
{
    int A[] = {1,12,15,26,38};
    int B[] = {2,13,17,30,45};
    int N = sizeof(A)/sizeof(A[0]);
    printf("Median : %d\n",GetMedian(A,B,N));
    return 0;
}
