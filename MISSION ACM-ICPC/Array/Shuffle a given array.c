#include<bits/stdc++.h>
using namespace std;

void printArray(int *A,int N)
{
	for(int i=0;i<N;i++)
		printf("%d ",A[i]);
	printf("\n");
}

void printArray(int *A,int N)
{
	for(int i=0;i<N;i++)
		printf("%d ",A[i]);
	printf("\n");
}

void Randomize(int *A,int N)
{
    int i,j;
    srand(time(NULL));
    for(i=N-1;i>=0;i--)
    {
        j = rand()%(i+1);
        swap(A[i],A[j]);
		PrintArray(A,N);
    }
}

int main(void)
{
    int A[] = {1,2,3,4,5,6,7};
    int N = sizeof(A)/sizeof(A[0]),i;
    Randomize(A,N);
    for(i=0;i<N;i++)
        printf("%d ",A[i]);
    return 0;
}
