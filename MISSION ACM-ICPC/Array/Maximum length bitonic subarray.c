#include<stdio.h>

int FindMax(int *A,int N)
{
    int low = 0,high = N-1;
    while(high >= low)
    {
        int mid = (low+high)/2;
        if(low == high)
            return low;
        else if(low + 1 == high && A[low] >= A[high])
            return low;
        else if(A[mid] > A[mid-1] && A[mid] > A[mid+1])
            return mid;
        else if(A[mid] > A[mid-1] && A[mid] < A[mid+1])
            low = mid+1;
        else
            high = mid -1;
    }
}

int main(void)
{
    int A[] = {2,5,10,8,12,15,7,3,0};
    int N = sizeof(A)/sizeof(A[0]);
    int index = FindMax(A,N);
    int i,count = 1;
    for(i=0;i<index;i++)
    {
        if(A[i] < A[i+1])
            count++;
        else
            count = 1;
    }
    for(i=index;i<N-1;i++)
    {
        if(A[i] > A[i+1])
            count++;
        else
        {
            printf("Length : %d\n",count);
            break;
        }
    }
        if(i == N-1)
            printf("Length : %d\n",count);
    return 0;
}
