#include<bits/stdc++.h>
using namespace std;

int invCount(int *A,int N)
{
	int ans = 0;
	for(int i=1;i<N-1;i++)
	{
		int small = 0,great = 0;
		for(int j=i+1;j<N;j++)
			if(A[i] > A[j])
				small++;
		for(int j=i-1;j>=0;j--)
			if(A[i] < A[j])
				great++;
		ans += small*great;
	}
	return ans;
}

int main(void)
{
	int A[] = {8,4,2,1};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<invCount(A,N);
	return 0;
}