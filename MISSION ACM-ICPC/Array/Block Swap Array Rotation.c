#include<stdio.h>

void swap(int *A,int i,int j,int D)
{
    int k;
    for(k = 0;k< D;k++)
    {
        int temp = A[k+i];
        A[k+i] = A[k+j];
        A[k+j] = temp;
    }
}

void Rotate(int *A,int N,int D)
{
    if(D == 0 || D == N)    //No Rotation Required
        return;
    int i = D,j = N-D;
    while(i != j)
    {
        if(i < j)
        {
            swap(A,D-i,D+j-i,i);
            j -= i;
        }
        else
        {
            swap(A,D-i,D,j);
            i -= j;
        }
    }
    swap(A,D-i,D,i);
}

int main(void)
{
    int N,i,D;
    printf("Enter the size of Array : ");
    scanf("%d",&N);
    int A[N+1];
    if(N)
    printf("Enter the Array Elements : ");
    for(i=0;i<N;i++)
        scanf("%d",&A[i]);
    printf("By how many positions do you want to Rotate it : ");
    scanf("%d",&D);
    Rotate(A,N,D);
    printf("After Rotation : ");
    for(i=0;i<N;i++)
        printf("%d ",A[i]);
    return 0;
}
