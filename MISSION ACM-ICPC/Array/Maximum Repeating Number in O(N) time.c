#include<stdio.h>

int MaxRepeating(int *A,int N,int k)
{
    int i;
    for(i=0;i<N;i++)
        A[A[i]%k] += k;
    int max = A[0],index=0;
    for(i=1;i<N;i++)
        if(A[i] > max)
        {
            max = A[i];
            index = i;
        }
    return index;
}

int main(void)
{
    int A[] = {1,2,2,2,0,2,0,2,3,8,0,9,2,3};
    int N = sizeof(A)/sizeof(A[0]);
    int k = 9;
    printf("%d",MaxRepeating(A,N,k));
    return 0;
}
