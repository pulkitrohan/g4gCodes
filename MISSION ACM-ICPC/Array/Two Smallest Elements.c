#include<stdio.h>
#include<limits.h>

//12 13 1 10 34 1

void PrintSmallest(int *A,int N)
{
    if(N < 2)
    {
        printf("Invalid Input\n");
        return;
    }
    int first = INT_MAX,second = INT_MAX,i;
    for(i=0;i<N;i++)
    {
        if(A[i] < first)
        {
            second = first;
            first = A[i];
        }
        else if(second > A[i] && A[i] != first)
            second = A[i];
    }
    if(second == INT_MAX)
        printf("No Second Smallest Element\n");
    else
        printf("Two Smallest Elements are %d and %d\n",first,second);
}

void PrintLargest(int *A,int N)
{
    if(N < 2)
    {
        printf("Invalid Input\n");
        return;
    }
    int first = INT_MIN,second = INT_MIN,i;
    for(i=0;i<N;i++)
    {
        if(A[i] > first)
        {
            second = first;
            first = A[i];
        }
        else if(second < A[i] && A[i] != first)
            second = A[i];
    }
    if(second == INT_MAX)
        printf("No Second Largest Element\n");
    else
        printf("Two Largest Elements are %d and %d\n",first,second);

}

int main(void)
{
    int i,N;
    printf("Enter N : ");
    scanf("%d",&N);
    int A[N+1];
    for(i=0;i<N;i++)
        scanf("%d",&A[i]);
    PrintLargest(A,N);
    return 0;
}
