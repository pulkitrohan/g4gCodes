#include<stdio.h>

int SearchElement(int MAT[4][4],int N,int x)
{
    int i = 0,j = N-1;
    while(i < N && j >= 0)
    {
        if(MAT[i][j] == x)
        {
            printf("Element found at <%d,%d>\n",i,j);
            return 1;
        }
        else if(MAT[i][j] > x)
            j--;
        else
            i++;
    }
    return 0;
}

int main(void)
{
    int MAT[][4] = {
                    {10,20,30,40},
                    {15,25,35,45},
                    {25,29,37,48},
                    {32,33,39,50}
                    };
    int N = 4;
    int x = 51;
    if(!SearchElement(MAT,N,x))
        printf("Element not found\n");
    return 0;
}
