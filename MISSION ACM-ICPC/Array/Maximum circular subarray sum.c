#include<bits/stdc++.h>
using namespace std;

int Kadane(int *A,int N)
{
    int max_so_far = A[0],max_ending_here = A[0],i;
    for(i=1;i<N;i++)
    {
        max_ending_here = max(max_ending_here + A[i],A[i]);
		max_so_far = max(max_ending_here,max_so_far);
    }
    return max_so_far;
}

int KadaneMin(int *A,int N)
{
	int min_so_far = A[0],min_ending_here = A[0],i;
    for(i=1;i<N;i++)
    {
        min_ending_here = min(min_ending_here + A[i],A[i]);
		min_so_far = min(min_ending_here,min_so_far);
    }
    return min_so_far;
}

int MaxCircularSum(int *A,int N)
{
    int max_non_circular = Kadane(A,N);
    //cout<<max_non_circular<<endl;
	int totalsum = 0;
	for(int i=0;i<N;i++)	
		totalsum += A[i];
	int max_circular = totalsum - KadaneMin(A,N);
//	cout<<max_circular<<endl;
	return max(max_non_circular,max_circular);
}

int main(void)
{
    int A[] = {8, -8, 9, -9, 10, -11, 12};
    int N = sizeof(A)/sizeof(A[0]);
    printf("%d\n",MaxCircularSum(A,N));
    return 0;
}
