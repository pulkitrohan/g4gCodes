#include<bits/stdc++.h>
using namespace std;

int nobleInteger(int A[],int N)
{
	sort(A,A+N);
	for(int i=0;i<N-1;i++)
	{
		if(A[i] == A[i+1])
			continue;
		if(A[i] == N-i-1)
			return A[i];
	}
	if(A[N-1] == 0)
		return A[N-1];
	return -1;
}

int main(void)
{
	int A[] = {10,3,20,40,2};
	int N = sizeof(A)/sizeof(A[0]);
	int ans = nobleInteger(A,N);
	if(ans != -1)
		cout<<"The noble integer is "<<ans;
	else
		cout<<"No noble integer found";
	return 0;
}