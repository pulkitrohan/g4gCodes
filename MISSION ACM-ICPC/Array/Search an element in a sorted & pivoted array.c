#include<stdio.h>

int FindElementRotated(int *A,int N,int key)
{
	int low = 0,high = N-1;
	while(high >= low)
	{
		int mid = (high -low)/2 + low;
		if(A[mid] == key)
			return mid;
		//first half of array is sorted
		else if(A[low] <= A[mid])
		{
			if(A[low] <= key && key < A[mid])
				high = mid - 1;
			else
				low = mid + 1;
		}
		//second half of array is sorted
		else
		{
            if(A[high] >= key && A[mid] < key)
                low = mid+1;
            else
                high = mid-1;
		}
	}
	return -1;
}


int main(void)
{
    int N,i,ans,key;
    printf("Enter the size of array : ");
    scanf("%d",&N);
    int A[N+1];
    printf("Enter the array : ");
    for(i=0;i<N;i++)
        scanf("%d",&A[i]);
    printf("Enter the element to be searched : ");
    scanf("%d",&key);
    ans = FindElementRotated(A,N,key);
    if(ans == -1)
        printf("Not Found\n");
    else
        printf("Element found at index : %d\n",ans);
    return 0;
}
