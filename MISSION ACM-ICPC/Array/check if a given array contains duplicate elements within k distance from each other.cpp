#include<bits/stdc++.h>
using namespace std;

int HasDupAtKDistance(int *A,int N,int k)
{
	set<int> S;
	for(int i=0;i<N;i++)
	{
		if(S.find(A[i]) != S.end())
			return 1;
		S.insert(A[i]);
		if(i >= k)
			S.erase(A[i-k]);
	}
	return 0;
}

int main(void)
{
	int A[] = {10,5,3,4,3,5,6};
	int N = sizeof(A)/sizeof(A[0]);
	int k = 3;
	if(HasDupAtKDistance(A,N,k))
		cout<<"yes\n";
	else
		cout<<"no\n";
	return 0;
}
}
