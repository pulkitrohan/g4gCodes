#include<bits/stdc++.h>
using namespace std;

int LZZS(int *A,int N)
{
	int dp[N][2];
	for(int i=0;i<N;i++)
		dp[i][0] = dp[i][1] = 1;

	int ans = 1;
	for(int i=1;i<N;i++)
	{
		for(int j=0;j<i;j++)
		{
			if(A[i] > A[j])
				dp[i][0] = max(dp[i][0],dp[j][1]+1);
			if(A[j] > A[i])
				dp[i][1] = max(dp[i][1],dp[j][0]+1);
			ans = max(ans,max(dp[i][0],dp[i][1]));
		}
	}
	return ans;
}

int main(void)
{
	int A[] = {10,22,9,33,49,50,31,60};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<LZZS(A,N);
	return 0;
}