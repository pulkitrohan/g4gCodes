#include<bits/stdc++.h>
using namespace std;

string units[] = { "Zero", "One", "Two", "Three",
           "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven",
           "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen",
           "Seventeen", "Eighteen", "Nineteen" };
string tens[] = { "", "", "Twenty", "Thirty", "Forty",
   "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

string convert(int N)
{
	if(N < 0)
		return "Negative " + convert(-1*N);
	if(N < 20)
		return units[N];
	if(N < 100)
		return tens[N/10] + ((N % 10) ? " " + convert(N%10) : "");
	if(N < 1000)
		return units[N/100] + " Hundred" + ((N % 100) ? " and " + convert(N%100) : "");
	if(N < 100000)
		return convert(N/1000) + " Thousand" + ((N % 1000) ? " " + convert(N%1000) : "");
	if(N < 10000000)
		return convert(N/100000) + " Lakh" + ((N % 100000) ? " " + convert(N%100000) : "");
	return convert(N/10000000) + " Crore" + ((N%10000000) ? " " + convert(N%10000000) : "");
}

int main(void)
{	
	cout<<convert(-1241241251)<<endl;
	return 0;
}