#include<stdio.h>
#include<stdlib.h>
typedef struct graph
{
    int V,E;
    int adj[100][100];
}Graph;
#define INF 1000

int Min(int A,int B)
{
  return A > B ? B : A;
}

void warshall(Graph *G)
{
    int i,j,k,distance[100][100];
    for(i=0;i<G->V;i++)
        for(j=0;j<G->V;j++)
        distance[i][j] = G->adj[i][j];
    for(k=0;k<G->V;k++)
        for(i=0;i<G->V;i++)
            for(j=0;j<G->V;j++)
                distance[i][j] =  Min(distance[i][j],distance[i][k] + distance[k][j]);
            for(i=0;i<G->V;i++)
            {
                for(j=0;j<G->V;j++)
                {
                    if(distance[i][j] == INF)
                        printf("INF\t");
                    else
                    printf("%d\t",distance[i][j]);
                }
                printf("\n");
            }
}


int main(void)
{
    int i,j,k,u,v,cost;
    Graph *G = (Graph *)malloc(sizeof(Graph));
    printf("\nEnter Total Vertices : ");
    scanf("%d",&G->V);
    for(i=0;i<G->V;i++)
        for(j=0;j<G->V;j++)
                G->adj[i][j] = INF;
    printf("\nEnter Total Edges : ");
    scanf("%d",&G->E);
    for(k = 0;k<G->E;k++)
    {
        printf("Enter the Starting and Destination Vertices : ");
        scanf("%d %d",&u,&v);
        printf("Enter the cost : ");
        scanf("%d",&cost);
        G->adj[u][v] = cost;          //Assumed Undirected Graph
    }
    warshall(G);
    return 0;
}
