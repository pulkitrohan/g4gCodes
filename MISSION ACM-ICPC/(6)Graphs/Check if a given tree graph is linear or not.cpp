#include<bits/stdc++.h>
using namespace std;

class Graph {
	
	private:
		int V;
		vector< vector<int> > adj;

	public:
		Graph(int V) {
			this->V = V;
			for(int i=0;i<V;i++) {
				adj.push_back({});
			}
		}

		void addEdge(int start,int end) {
			adj[start].push_back(end);
			adj[end].push_back(start);
		}
		void printGraph() {
			for(int i=0;i<V;i++) {
				for(int j=0;j<adj[i].size();j++)
					cout<<adj[i][j]<<" ";
				cout<<endl;
			}
		}

		bool isLinear() {
			if(V == 1)
				return true;
			int count = 0;
			for(auto V : adj)
				if(V.size() == 2)
					count++;
			return (count == V-2);
		}

};

int main(void) {
	Graph *G = new Graph(3);
	G->addEdge(0, 1);
	G->addEdge(0, 2);
	cout<<G->isLinear();
	return 0;
}