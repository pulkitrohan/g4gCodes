#include<bits/stdc++.h>
using namespace std;

struct contact
{
	string field1,field2,field3;
};

void BuildGraph(contact A[],int N,vector<vector<int> > &G)
{
	for(int i=0;i<N;i++)
	{
		for(int j=i+1;j<N;j++)
		{
			if(A[i].field1 == A[j].field1 || A[i].field1 == A[j].field2 || A[i].field1 == A[j].field3 || A[i].field2 == A[j].field1 ||
			   A[i].field2 == A[j].field2 || A[i].field2 == A[j].field3  || A[i].field3 == A[j].field1 || A[i].field3 == A[j].field2 ||
			   A[i].field3 == A[j].field3)
			{
				G[i][j] = G[j][i] = 1;
				break;
			}
		}
	}
}

void dfs(vector<vector<int> > &G,vector<bool> &visited,vector<int> &ans,int N,int start)
{
	visited[start] = true;
	ans.push_back(start);
	for(int i=0;i<N;i++)
		if(G[start][i] && !visited[i])
			dfs(G,visited,ans,N,i);
}

void FindSameContacts(contact A[],int N)
{
	vector<int> ans;
	vector<vector<int> > G(N,vector<int>(N,0));
	vector<bool> visited(N,false);
	BuildGraph(A,N,G);
	for(int i=0;i<N;i++)
	{
		if(!visited[i])
		{
			dfs(G,visited,ans,N,i);
			ans.push_back(-1);
		}
	}
	for(int i=0;i<ans.size();i++)
		if(ans[i] == -1)
			cout<<endl;
		else
			cout<<ans[i]<<" ";
}

int main(void)
{
	contact A[] = {{"Gaurav", "gaurav@gmail.com", "gaurav@gfgQA.com"},
                     {"Lucky", "lucky@gmail.com", "+1234567"},
                     {"gaurav123", "+5412312", "gaurav123@skype.com"},
                     {"gaurav1993", "+5412312", "gaurav@gfgQA.com"},
                     {"raja", "+2231210", "raja@gfg.com"},
                     {"bahubali", "+878312", "raja"}
                    };
	int N = sizeof(A)/sizeof(A[0]);
	FindSameContacts(A,N);
	return 0;
}