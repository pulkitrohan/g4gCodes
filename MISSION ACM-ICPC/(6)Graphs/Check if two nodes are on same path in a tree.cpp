#include<bits/stdc++.h>
using namespace std;
struct AL
{
    int data;
    struct AL *next;
};

int V[10] = {0};


void addEdge(vector<struct AL *> &G,int start,int end)
{
    struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = end;
    temp->next = G[start];  //G[start] initially contains NULL
    G[start] = temp;    //Now G[start] contains value at temp
}

void print(vector<struct AL *> &G,int N)
{
    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        printf("Node %d ",i);
        while(temp)
        {
            printf(" -> %d ",temp->data);
            temp = temp->next;
        }
        printf("\n");
    }
}


void dfs(vector<struct AL *> &G,int start, vector<int> &intime, vector<int> &outtime, int &timer) 
{
    V[start] = 1;
    intime[start] = ++timer;
    struct AL *temp = G[start];
    while(temp)
    {   
        if(!V[temp->data])
            dfs(G,temp->data, intime, outtime, timer);
        temp = temp->next;
    }
    outtime[start] = ++timer;
}

bool query(int first, int second, vector<int> &intime, vector<int> &outtime) {
    return (intime[first] < intime[second] && outtime[first] > outtime[second]) ||
           (intime[first] > intime[second] && outtime[first] < outtime[second]);
}
   

int main(void)
{
    vector<struct AL *> Graph;
    int v = 9,i;
    for(int i=0;i<v;i++)
        Graph.push_back(NULL);
    addEdge(Graph, 0, 1);
    addEdge(Graph, 0, 2);
    addEdge(Graph, 2, 5);
    addEdge(Graph, 1, 3);
    addEdge(Graph, 1, 4);
    addEdge(Graph, 4, 6);
    addEdge(Graph, 4, 7);
    addEdge(Graph, 4, 8);

    vector<int> intime(Graph.size(), 0), outtime(Graph.size(), 0);
    int timer = 0;
    dfs(Graph, 0, intime, outtime, timer);
    cout<<query(0, 4, intime, outtime)<<endl;
    cout<<query(1, 8, intime, outtime)<<endl;
    cout<<query(1, 5, intime, outtime)<<endl;
    return 0;
}
