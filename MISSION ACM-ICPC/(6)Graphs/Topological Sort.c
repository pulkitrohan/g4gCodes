#include<stdio.h>
#include<stdlib.h>
typedef struct graph
{
    int V,E;
    int **adj;
}Graph;
typedef struct queue
{
    int *element;
    int capacity;
    int front;
    int rear;
    int size;
}queue;

queue *createQueue(int MaxSize)
{
    queue *Q;
    Q = (queue *)malloc(sizeof(queue));
    Q->element = (int *)malloc(sizeof(int)*MaxSize);
    Q->capacity = MaxSize;
    Q->front = 0;
    Q->rear = -1;
    Q->size = 0;
    return Q;
}
void Enqueue(queue *Q,int element)
{
       Q->size++;
       Q->rear++;
       Q->element[Q->rear] = element;
}

int Dequeue(queue *Q)
{
        Q->size--;
        int data = Q->element[Q->front];;
        Q->front++;
        return data;
}
int isEmpty(queue *Q)
{
    if(Q->size == 0)
        return 1;
    else return 0;
}
int main(void)
{
    int i,j,k,u,v,counter = 0,topologicalsort[100],indegree[100];
    Graph *G = (Graph *)malloc(sizeof(Graph));
    queue *Q = createQueue(100);
    printf("\nEnter Total Vertices : ");
    scanf("%d",&G->V);
    G->adj = (int **)malloc(sizeof(int *) * (G->V+1));
    for(i=1;i<=G->V;i++)
    {
        indegree[i] = 0;
        G->adj[i] = (int *)malloc(sizeof(int) * (G->V+1));
        for(j=1;j<=G->V;j++)
            G->adj[i][j] = 0;
    }
    printf("\nEnter Total Edges : ");
    scanf("%d",&G->E);
    for(k = 0;k<G->E;k++)
    {
        printf("Enter the Starting and Destination Vertices : ");
        scanf("%d %d",&u,&v);
        G->adj[u][v] = 1;          //Assumed directed Graph
    }
    for(i=1;i<=G->V;i++)     //For Calculating Indegree
    {
        for(j=1;j<=G->V;j++)
            if(G->adj[j][i])
                indegree[i]++;
    }
    for(v=1;v<=G->V;v++)
        if(indegree[v] == 0)
            Enqueue(Q,v);
        j=0;
    while(!isEmpty(Q))
    {
        k = Dequeue(Q);
        topologicalsort[j++] = k;
        counter++;
        for(i=1;i<=G->V;i++)
        {
            if(G->adj[k][i])
            {
                G->adj[k][i] = 0;
                indegree[i]--;
                if(indegree[i] == 0)
                    Enqueue(Q,i);
            }
        }
    }
    if(counter != G->V)
        printf("Graph has Cycle");
    else
    for(i=0;i<G->V;i++)
        printf("%d ",topologicalsort[i]);
return 0;
}

