#include<bits/stdc++.h>
using namespace std;
struct AL
{
    int data;
    struct AL *next;
};

void addEdge(vector<struct AL *> &G,int start,int end)
{
    struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = end;
    temp->next = G[start];  //G[start] initially contains NULL
    G[start] = temp;    //Now G[start] contains value at temp
}

void print(vector<struct AL *> &G,int N)
{
    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        printf("Node %d ",i);
        while(temp)
        {
            printf(" -> %d ",temp->data);
            temp = temp->next;
        }
        printf("\n");
    }
}

void CreateTransposeGraph(vector<struct AL *>&T,vector<struct AL *> &G,int N)
{
    for(int i=0;i<N;i++)
        T.push_back(NULL);
    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        while(temp)
        {
            addEdge(T, temp->data, i);
            temp = temp->next;
        }
    }
}

void dfs1(vector<struct AL *> &G,int i,int v[],stack<int> &S)
{
    v[i] = 1;
    struct AL *temp = G[i];
    while(temp)
    {
        if(!v[temp->data])
            dfs1(G,temp->data,v,S);
        temp = temp->next;
    }
	S.push(i);
}

void dfs2(vector<struct AL *> &G,int i,int v[])
{
    v[i] = 1;
	printf("%d ",i);
    struct AL *temp = G[i];
    while(temp)
    {
        if(!v[temp->data])
            dfs2(G,temp->data,v);
        temp = temp->next;
    }
}

void SSC(vector<struct AL *> &G,int N)
{
	stack<int> S;
	int v[N+1];
	for(int i=0;i<N;i++)
		v[i] = 0;
	for(int i=0;i<N;i++)
		if(!v[i])
			dfs1(G,i,v,S);
	vector<struct AL *> T;
    CreateTransposeGraph(T,G,N);
	for(int i=0;i<N;i++)
		v[i] = 0;
	while(!S.empty())
	{
		int u = S.top();
		S.pop();
		if(!v[u])
		{
			dfs2(T,u,v);
			printf("\n");
		}
	}
}


int main(void)
{
    vector<struct AL *> Graph;
    int v = 5,i;
    for(int i=0;i<v;i++)
        Graph.push_back(NULL);
    addEdge(Graph, 1,0);
    addEdge(Graph, 0, 2);
    addEdge(Graph, 2, 1);
    addEdge(Graph, 0, 3);
    addEdge(Graph, 3, 4);
	SSC(Graph,v);
    return 0;
}


