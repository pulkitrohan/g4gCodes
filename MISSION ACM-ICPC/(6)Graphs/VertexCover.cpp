#include<bits/stdc++.h>
using namespace std;

struct node 
{
	int data;
	struct node *next;
};

void AddEdge(vector<struct node *> &G,int start,int end)
{
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp->data = end;
	temp->next = G[start];
	G[start] = temp;
	temp = (struct node *)malloc(sizeof(struct node));
	temp->data = start;
	temp->next = G[end];
	G[end] = temp;
}

void printVertexCover(vector<struct node *> &G,int N)
{
	bool visited[N];
	memset(visited,false,sizeof(visited));
	for(int i=0;i<N;i++)
	{
		if(!visited[i])
		{
			struct node *temp = G[i];
			while(temp)
			{
				if(!visited[temp->data])
				{
					visited[i] = true;
					visited[temp->data] = true;
					break;
				}
				temp = temp->next;
			}
		}
	}
	for(int i=0;i<N;i++)
		if(visited[i])
			cout<<i<<" ";
}

int main(void)
{
	int N = 7;
	vector<struct node *> G;
	for(int i=0;i<N;i++)
		G.push_back(NULL);
	AddEdge(G,0,1);
	AddEdge(G,0,2);
	AddEdge(G,1,3);
	AddEdge(G,3,4);
	AddEdge(G,4,5);
	AddEdge(G,5,6);
	printVertexCover(G,N);
	return 0;
}
	