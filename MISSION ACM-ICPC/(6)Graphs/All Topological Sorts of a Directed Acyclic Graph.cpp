#include<bits/stdc++.h>
using namespace std;
struct AL
{
    int data;
    struct AL *next;
};

void addEdge(vector<struct AL *> &G,int start,int end)
{
    struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = end;
    temp->next = G[start];  //G[start] initially contains NULL
    G[start] = temp;    //Now G[start] contains value at temp
}

void AllTopologicalSortUtil(vector<struct AL *> &G, vector<int> &indegree,
    vector<bool> &visited, vector<int> &result) {

    bool flag = false;
    for(int i=0;i<G.size();i++) {
        if(indegree[i] == 0 && !visited[i]) {
            
            struct AL *temp = G[i];
            while(temp) {
                indegree[temp->data]--;
                temp = temp->next;
            }

            result.push_back(i);
            visited[i] = true;
            AllTopologicalSortUtil(G, indegree, visited, result);

            result.erase(result.end()-1);
            visited[i] = false;

            temp = G[i];
            while(temp) {
                indegree[temp->data]++;
                temp = temp->next;
            }

            flag = true;

        }
    }
    if(!flag) {
        for(int i=0;i<result.size();i++)
            cout<<result[i]<<" ";
        cout<<endl;
    }
}

void AllTopologicalSort(vector<struct AL *> &Graph) {

    vector<bool> visited(Graph.size(), false);
    vector<int> result;
    vector<int> indegree(Graph.size(), 0);
    for(int i=0;i<Graph.size();i++)
    {
        struct AL *temp = Graph[i];
        while(temp)
        {
            indegree[temp->data]++;
                temp = temp->next;
        }
    }

    AllTopologicalSortUtil(Graph, indegree, visited, result);

}

int main(void)
{
    vector<struct AL *> Graph;
    int v = 6;
    for(int i=0;i<v;i++)
        Graph.push_back(NULL);
    addEdge(Graph, 5, 2);
    addEdge(Graph, 5, 0);
    addEdge(Graph, 4, 0);
    addEdge(Graph, 4, 1);
    addEdge(Graph, 2, 3);
    addEdge(Graph, 3, 1);
    AllTopologicalSort(Graph);
   // print(Graph,v);
    return 0;
}
