#include<cstdio>
#include<vector>
#include<cstdlib>
#include<queue>
using namespace std;
struct AL
{
    int data;
    struct AL *next;
};

void addEdge(vector<struct AL *> &G,int start,int end)
{
    struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = end;
    temp->next = G[start];  //G[start] initially contains NULL
    G[start] = temp;    //Now G[start] contains value at temp
}

void print(vector<struct AL *> &G,int N)
{
    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        printf("Node %d ",i);
        while(temp)
        {
            printf(" -> %d ",temp->data);
            temp = temp->next;
        }
        printf("\n");
    }
}


void UnweightedShortestPath(vector<struct AL *> &G,int i,int *Distance)
{
    queue<int> Q;
    Q.push(i);
    // printf("%d ",i);
//    V[i] = 1;
    Distance[i] = 0;
    while(!Q.empty())
    {
        int j = Q.front();
        Q.pop();
        struct AL *temp = G[j];
        while(temp)
        {
            if(Distance[temp->data] == -1)
            {
                Distance[temp->data] = (1 + Distance[j]);
                Q.push(temp->data);
            }
            temp = temp->next;
        }
    }
}

int main(void)
{
    vector<struct AL *> Graph;
    int v = 7,i;
    for(int i=0;i<v;i++)
        Graph.push_back(NULL);
    addEdge(Graph, 0, 1);
    addEdge(Graph, 1, 2);
    addEdge(Graph, 2, 3);
    addEdge(Graph, 3, 4);
    addEdge(Graph, 4, 5);
    addEdge(Graph, 0, 5);
    addEdge(Graph, 1, 6);
    addEdge(Graph, 2, 6);
    addEdge(Graph, 6, 4);
    addEdge(Graph, 6, 5);
    int s = 0;
    int Distance[v+1];
    for(int i = 0;i<7;i++)
        Distance[i] = -1;
    UnweightedShortestPath(Graph,s,Distance);

    for(int i=0;i<7;i++)
    {
        printf("Minimum Distance of vertex %d from %d is %d\n",i,s,Distance[i]);
    }
    return 0;
}


