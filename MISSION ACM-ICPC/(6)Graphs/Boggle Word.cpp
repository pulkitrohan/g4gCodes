#include<bits/stdc++.h>
using namespace std;
string dictonary[] = {"GEEKS","FOR","QUIZ","GO"};
int N = sizeof(dictonary)/sizeof(dictonary[0]);

bool isWord(string &S)
{
	for(int i=0;i<N;i++)
		if(S == dictonary[i])
			return true;
	return false;
}

void find(char boogle[][3],vector<vector<bool> > &visited,int row,int col,string S,int N,int M)
{
	visited[row][col] = true;
	S.push_back(boogle[row][col]);
	if(isWord(S))
		cout<<S<<endl;
	for(int i=row-1;i<=row+1 && i<M;i++)
		for(int j=col-1;j<=col+1 && j<N;j++)
			if(i >= 0 && j >= 0 && !visited[i][j])
				find(boogle,visited,i,j,S,N,M);
	S.pop_back();
	visited[row][col] = false;
}

void findWords(char boogle[][3],int N,int M)
{
	vector<vector<bool> > visited(N,vector<bool>(M,0));
	string S;
	for(int i=0;i<N;i++)
		for(int j=0;j<M;j++)
			find(boogle,visited,i,j,S,N,M);
}

int main(void)
{
	char boogle[3][3] = {{'G','I','Z'},
						 {'U','E','K'},
						 {'Q','S','E'}};
	findWords(boogle,3,3);
	return 0;
}