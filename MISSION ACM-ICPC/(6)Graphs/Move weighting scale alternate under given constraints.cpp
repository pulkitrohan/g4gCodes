#include<bits/stdc++.h>
using namespace std;

bool dfs(int residue, int curSteps, int wt[], int arr[], int N, int steps) {
    if(curSteps > steps)
        return true;

    for(int i=0;i<N;i++) {
        if(arr[i] > residue && arr[i] != wt[curSteps-1]) {
            wt[curSteps] = arr[i];
            if(dfs(arr[i]-residue, curSteps+1, wt, arr, N, steps))
                return true;
        }
    }
    return false;
}

void printWeightOnScale(int A[], int N, int steps) {
    int wt[steps];
    if(dfs(0, 0, wt, A, N, steps)) {
        for(int i=0;i<steps;i++)
            cout<<wt[i]<<" ";
        cout<<endl;
    }
    else {
        cout<<"Not possible";
    }
}

int main(void)
{
    int A[] = {2,3,5,6};
    int N = sizeof(A)/sizeof(A[0]);
    int steps = 10;
    printWeightOnScale(A, N, steps);
    return 0;
}
