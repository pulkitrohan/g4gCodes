/*In this Method,a V*V matrix is used.Starting and destination Vertices are inputted
from the user and output is given in matrix Form.
Space Complexity : O(V^2)
*/
#include<stdio.h>
typedef struct graph
{
    int V,E;
    int **adj
}Graph;
int main(void)
{
    int i,j,k,u,v;
    Graph *G = (Graph *)malloc(sizeof(Graph));
    printf("\nEnter Total Vertices : ");
    scanf("%d",&G->V);
    G->adj = (int **)malloc(G->V * sizeof(int *));
    for(i=0;i<G->V;i++)
    {
        G->adj[i] = (int *)malloc(G->V * sizeof(int));
        for(j=0;j<G->V;j++)
            G->adj[i][j] = 0;
    }
    printf("\nEnter Total Edges : ");
    scanf("%d",&G->E);
    for(k = 0;k<G->E;k++)
    {
        printf("Enter the Starting and Destination Vertices : ");
        scanf("%d %d",&u,&v);
        G->adj[u][v] = 1;          //Assumed Undirected Graph
        G->adj[v][u] = 1;
    }
    for(i=0;i<G->V;i++)
    {
        for(j=0;j<G->V;j++)
        printf("%d ",G->adj[i][j]);
        printf("\n");
    }
    return 0;
}
