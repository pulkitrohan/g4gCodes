#include<bits/stdc++.h>
using namespace std;
struct AL
{
    int data;
    int weight;
    struct AL *next;
};
#define MAX 100000
typedef pair<int,int> P;

void addEdge(vector<struct AL *> &G,int start,int end,int weight)
{
    struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
    temp->weight = weight;
    temp->data = end;
    temp->next = G[start];  //G[start] initially contains NULL
    G[start] = temp;    //Now G[start] contains value at temp
    temp = (struct AL *)malloc(sizeof(struct AL));
    temp->weight = weight;
    temp->data = start;
    temp->next = G[end];
    G[end] = temp;
}

void print(vector<struct AL *> &G,int N)
{
    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        printf("Node %d ",i);
        while(temp)
        {
            printf(" -> %d ",temp->data);
            temp = temp->next;
        }
        printf("\n");
    }
}

class cmp
{	
	public:
		bool operator()(P &A,P &B)
		{
			return A.second > B.second;
		}
};

void Prim(vector<struct AL *> &G,int start,int N)
{
    int distance[N+1],v[N+1],parent[N+1],i;
    for(i=0;i<N;i++)
    {
		distance[i] = MAX;
		v[i] = 0;
	}
    distance[start] = 0;
	parent[start] = -1;
	priority_queue<P,vector<P >,cmp > PQ;
	PQ.push(make_pair(start,0));
	P A;
	int count = 0;
	while(!PQ.empty() && count < N-1)
	{
		A = PQ.top();
		PQ.pop();
		int u = A.first;
		int d = A.second;
		if(v[u] == 1)
			continue;
		count++;
		v[u] = 1;
		struct AL *temp = G[u];
		while(temp)
		{
			if(distance[temp->data] > distance[u] + temp->weight)
			{
				distance[temp->data] = distance[u] + temp->weight;
				parent[temp->data] = u;
			}
			if(!v[temp->data])
				PQ.push(make_pair(temp->data,distance[temp->data]));
			temp = temp->next;
		}
	}
		
	for(i=1;i<N;i++)
		printf("%d -- %d\n",parent[i],i);
}

int main(void)
{
    vector<struct AL *> graph;
    int v = 9,i;
    for(int i=0;i<v;i++)
        graph.push_back(NULL);
    addEdge(graph, 0, 1, 4);
    addEdge(graph, 0, 7, 8);
    addEdge(graph, 1, 2, 8);
    addEdge(graph, 1, 7, 11);
    addEdge(graph, 2, 3, 7);
    addEdge(graph, 2, 8, 2);
    addEdge(graph, 2, 5, 4);
    addEdge(graph, 3, 4, 9);
    addEdge(graph, 3, 5, 14);
    addEdge(graph, 4, 5, 10);
    addEdge(graph, 5, 6, 2);
    addEdge(graph, 6, 7, 1);
    addEdge(graph, 6, 8, 6);
    addEdge(graph, 7, 8, 7);
    Prim(graph,0,v);
    return 0;
}


