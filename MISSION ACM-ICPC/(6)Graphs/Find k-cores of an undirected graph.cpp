#include<bits/stdc++.h>
using namespace std;
struct AL
{
    int data;
    struct AL *next;
};

int V[10] = {0};


void addEdge(vector<struct AL *> &G,int start,int end)
{
    struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = end;
    temp->next = G[start];  //G[start] initially contains NULL
    G[start] = temp;    //Now G[start] contains value at temp

    temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = start;
    temp->next = G[end];
    G[end] = temp;
}

void print(vector<struct AL *> &G,int N)
{
    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        printf("Node %d ",i);
        while(temp)
        {
            printf(" -> %d ",temp->data);
            temp = temp->next;
        }
        printf("\n");
    }
}

int getSize(struct AL *node) {
    int count = 0;
    while(node) {
        count++;
        node = node->next;
    }
    return count;
}

bool dfs(vector<struct AL *> &G, int startVertex,
    vector<bool> &visited, vector<int> &vDegree, int k) {

    visited[startVertex] = true;
    AL *temp = G[startVertex];

    while(temp) {
        if(vDegree[startVertex] < k)
            vDegree[temp->data]--;
        if(!visited[temp->data] && dfs(G, temp->data, visited, vDegree, k))
            vDegree[startVertex]--; 
        temp = temp->next;
    }
    return (vDegree[startVertex] < k);
}

void printKCores(vector<struct AL *> &G, int k) {
    vector<bool> visited(G.size(), false);

    int minDeg = INT_MAX;
    int startVertex;

    vector<int> vDegree(G.size());
    for(int i=0;i<G.size();i++) {
        vDegree[i] = getSize(G[i]);
        if(vDegree[i] < minDeg) {
            minDeg = vDegree[i];
            startVertex = i;
        }
    }
    dfs(G, startVertex, visited, vDegree, k);
    for(int i=0;i<G.size();i++)
        if(!visited[i])
            dfs(G, i, visited, vDegree, k);
    for(int i=0;i<G.size();i++) {
        if(vDegree[i] >= k) {
            cout<<"\n["<<i<<"]";
            AL *temp = G[i];
            while(temp) {
                if(vDegree[temp->data] >= k) 
                    cout<<"->"<<temp->data;
            }
        }
    }
}        

int main(void)
{
    vector<struct AL *> Graph;
    int v = 9,i;
    for(int i=0;i<v;i++)
        Graph.push_back(NULL);
    addEdge(Graph, 0, 1);
    addEdge(Graph, 0, 2);
    addEdge(Graph, 0, 3);
    addEdge(Graph, 1, 4);
    addEdge(Graph, 1, 5);
    addEdge(Graph, 1, 6);
    addEdge(Graph, 2, 7);
    addEdge(Graph, 2, 8);
    addEdge(Graph, 2, 9);
    addEdge(Graph, 3, 10);
    addEdge(Graph, 3, 11);
    addEdge(Graph, 3, 12);

    printKCores(Graph, 3);
    return 0;
}
