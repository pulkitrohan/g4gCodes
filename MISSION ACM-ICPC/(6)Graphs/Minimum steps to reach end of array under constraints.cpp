#include<bits/stdc++.h>
using namespace std;

int getMinStepsToReachEnd(int A[], int N) {
    vector<bool> visited(N, false);
    int distance[N];
    vector<int> digit[10];
    for(int i=1;i<N;i++)
        digit[A[i]].push_back(i);
    distance[0] = 0;
    visited[0] = true;
    queue<int> Q;
    Q.push(0);

    while(!Q.empty()) {
        int index = Q.front(); 
        Q.pop();
        if(index == N-1)
            break;

        int val = A[index];
        for(int i=0;i<digit[val].size();i++) {
            int digitIndex = digit[val][i];
            if(!visited[digitIndex]) {
                visited[digitIndex] = true;
                Q.push(digitIndex);
                distance[digitIndex] = distance[index] + 1;
            }
        }

        digit[val].clear();

        if(index -1 >= 0 && !visited[index-1]) {
            visited[index-1] = true;
            Q.push(index-1);
            distance[index-1] = distance[index] + 1;
        }

        if(index + 1 < N && !visited[index+1]) {
            visited[index+1] = true;
            Q.push(index+1);
            distance[index+1] = distance[index] + 1;
        }
    }
    return distance[N-1];
}

int main(void)
{
    int A[] = {0, 1, 2, 3, 4, 5, 6, 7, 5, 4, 3, 6, 0, 1, 2, 3, 4, 5, 7};
    int N = sizeof(A)/sizeof(A[0]);
    cout<<getMinStepsToReachEnd(A, N);
    return 0;
}
