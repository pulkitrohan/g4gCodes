#include<stdio.h>
#include<stdlib.h>

typedef struct node{
   int key;
   struct node* next;
   struct adjacency* adj;
}node;

typedef struct adjacency{
    struct node* dest;
    struct adjacency* link;
}adjacency;

node* findnode( node* ,int);
void findedge( node* , int, int);
node*  insedge( node* , int ,int);
node* insnode( node* ,int);
node* delnode( node* ,int);
void deledge( node* , int ,int);
node* deloutedge(node* ,int);
void delinedge( adjacency* , node* );

int main( void )
{
    node* head= NULL;
    node* loc;
    int opt,u,v;
    do
    {
        printf("Choose an option:\n1.Find node\n2.Find edge\n3.Insert node\n4.Insert Edge\n5.Delete node\n6.Delete edge\n7.Display Graph\n8.Exit\n");
        scanf("%d",&opt);
        switch( opt )
        {
           case 1: printf("Enter the node to search:\n");
                scanf("%d",&u);
                loc=findnode(head , u);
                if( loc == NULL )
                  printf("Node not a part of graph.\n");
                else
                   printf("Node found in graph.\n");
                break;
           case 2: printf("Enter the edge to search:\n");
                    scanf("%d%d",&u,&v);
                    findedge(head , u, v);
                    break;
           case 3:  printf("Enter the node to insert:\n");
                  scanf("%d",&u);
                  head=insnode(head, u);
                  break;
           case 4: printf("Enter the edge to insert:\n");
                 scanf("%d%d",&u,&v);
                 head=insedge(head,u, v);
                 break;
           case 5: printf("Enter the node to delete:\n");
                    scanf("%d",&u);
                    head=delnode( head, u);
                    break;
           case 6: printf("Enter the edge to delete:\n");
                    scanf("%d%d",&u,&v);
                    deledge( head, u, v);
                    break;
           //case 7: printf("The graph is:\n");
             //       display( head );
        }
    }
    while( opt != 8);
    return 0;
}

node* findnode(node* head, int u)
{
    if( head == NULL)
       return NULL;
    node* ptr= head;
    while( ptr != NULL )
    {
        if( ptr->key == u )
          return ptr;
        ptr=ptr->next;
    }
    return NULL;
}

void findedge(node* head, int u,int v)
{
    if( head == NULL)
    {
       printf("Graph is empty.\n");
       return ;
    }
    node* locu, * locv;
    adjacency* temp;
    locu=findnode( head, u);
    locv=findnode( head, v);
    if( locu == NULL || locv == NULL )
    {
        printf("Edge not present.\n");
        return ;
    }
    temp=locu->adj;
    while( temp != NULL )
    {
        if( temp->dest == locv )
        {
            printf("Edge found.\n");
            return;
        }
        temp=temp->link;
    }
    printf("Edge not a part of graph.\n");
}

node* insnode( node* head, int u)
{
   node* loc;
   loc=findnode( head, u);
   if( loc != NULL )
   {
      printf("\nNode is already present.\n");
   }
   else
   {
      node* newn= malloc( sizeof(node));
      newn->key=u;
      newn->adj=NULL;
      newn->next= head;
      head= newn;
      printf("Node inserted successfully.\n");
   }
   return head;
}
node* insedge(node* head, int u ,int v)
{
    node* locu,*locv;
    locu= findnode( head, u);
    locv= findnode(head , v);
    if( locu == NULL )
    {
        head= insnode( head, u);
        locu= findnode( head, u);
    }
    if( locv == NULL )
    {
       head= insnode( head, v);
       locv= findnode( head, v);
    }
    adjacency* temp;
    temp= locu->adj;
    adjacency* newadj= malloc( sizeof(adjacency));
    newadj->dest=locv;
    newadj->link=temp;
    locu->adj=newadj;
    return head;
}

void deledge(node* head, int u, int v)
{
    if( head == NULL )
    {
       printf("Graph is empty");
       return ;
    }
    node* locu,* locv;
    locu= findnode( head, u);
    locv= findnode( head , v);
    if( locu == NULL || locv == NULL )
    {
       printf("Nodes not present in graph");
       return;
    }

    if( locu->adj != NULL && locu->adj->dest == locv )
    {
       adjacency* temp = locu->adj;
       locu->adj=temp->link;
       free( temp );
       return;
    }
    else
    {
       adjacency* prev= NULL;
       adjacency* ptr=locu->adj;
       while( ptr != NULL )
       {
          if( ptr->dest == locv )
          {
              prev->link=ptr->link;
              free( ptr );
              return;
          }
          prev= ptr;
          ptr=ptr->link;
       }
       printf("\nEdge not present in graph.\n");
    }
}

node* delnode( node* head, int u)
{
     node* locu;
     locu = findnode( head , u);
     node* ptr= head;
     while( ptr != NULL )
     {
        if( ptr == locu )
        {
           ptr=ptr->next;
           continue;
        }
        delinedge(ptr->adj , locu);
        ptr=ptr->next;
     }
     return deloutedge( head, u);
}
void delinedge( adjacency* head ,  node* item)
{
    adjacency* temp = head;
    if( head != NULL && head->dest == item )
    {
        head=head->link;
        free( temp );
    }
    else
    {
       adjacency* prev= NULL;
       while( temp != NULL )
       {
           if( temp->dest == item)
           {
               prev->link= temp->link;
               free( temp );
               return;
           }
           prev=temp;
           temp=temp->link;
       }
    }
}
node* deloutedge( node* head, int u)
{
    node* temp = head;
    if( temp->key == u )
    {
       head=head->next;
       free( temp );
       return head;
    }
    else
    {
        node* prev= NULL;
        while( temp!= NULL)
        {
           if( temp->key == u )
           {
               prev->next = temp->next;
               free( temp->adj );
               free( temp);
               return head;
           }
           prev= temp;
           temp= temp->next;
        }
        return head;
    }
}
