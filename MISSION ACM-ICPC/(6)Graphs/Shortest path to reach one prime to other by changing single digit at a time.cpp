#include<bits/stdc++.h>
 
int prime[10000] = {0};
void sieve()
{
	for(int i=2;i<=100;i++)
	{
		if(!prime[i])
		{
			for(int j=i*i;j<=10000;j+=i)
				prime[j] = 1;
		}
	}
}
 
void arr(int A[],int num)
{
	int w = 3;
	while(num)
	{
		A[w--] = num%10;
		num /= 10;
	}
}
 
int conv_num(int A[])
{
	int temp = 0,k = 0;
	while(k < 4)
		temp = temp*10 + A[k++];
	return temp;
}
 
using namespace std;
 
int main(void)
{
	int T;
	scanf("%d",&T);
	sieve();
	while(T--)
	{
		int A,B;
		scanf("%d %d",&A,&B);
		int dist[10010],parent[10009],digit[4];
		memset(dist,-1,sizeof(dist));
		memset(parent,-1,sizeof(parent));
		queue<int> Q;
		dist[A] = 0;
		Q.push(A);
		parent[A] = 0;
		while(!Q.empty())
		{
			int num = Q.front();
			for(int k=0;k<=3;k++)
			{
				arr(digit,num);
				for(int i=0;i<=9;i++)
				{
					digit[k] = i;
					int temp = conv_num(digit);
					if(!prime[temp] && dist[temp] == -1 && temp >= 1000)
					{
						dist[temp] = dist[num] + 1;
						Q.push(temp);
					}
				}
			}
			Q.pop();
		}
		if(dist[B] == -1)
			printf("Impossible\n");
		else
			printf("%d\n",dist[B]);
	}
	return 0;
}