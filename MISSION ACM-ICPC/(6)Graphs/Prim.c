#include<stdio.h>
#include<stdlib.h>
#define INF 10000
typedef struct Graph
{
    int V,E;
    int adj[100][100];
}Graph;
int distance[100],parent[100];
void Prim(Graph *G,int s)
{
    int count = 0,min,i,visited[100],u;
    for(i=0;i<G->V;i++)
        {
            distance[i] = INF;
            visited[i] = 0;
        }
        distance[s] = 0;
        parent[s] = -1;
        while(count < G->V - 1)   // V-1 Edges
        {
            min = 99;
            for(i=0;i<G->V;i++)
            {
                if(distance[i] < min && !visited[i])
                    {
                        min = distance[i];
                        u = i;
                    }
            }
            visited[u] = 1;
            count++;
            for(i=0;i<G->V;i++)
            {
                if(G->adj[u][i] && !visited[i] && (G->adj[u][i] < distance[i]))
                {
                    distance[i] = G->adj[u][i];
                    parent[i] = u;
                }
            }
        }
         printf("Edge   Weight\n");
            for (i = 1; i < G->V; i++)
      printf("%d - %d    %d \n", parent[i], i, G->adj[i][parent[i]]);
}
int main(void)
{
    int i,j,u,v,cost,min,count = 1,visited[100],total_cost = 0,a,b;
    Graph *G = (Graph *)malloc(sizeof(Graph));
    printf("Enter the total Vertices : ");
    scanf("%d",&G->V);
    for(i=0;i<G->V;i++)
    {
        visited[i] = 0;
        for(j=0;j<G->V;j++)
            G->adj[i][j] = INF;
    }
    printf("Enter the total Edges : ");
    scanf("%d",&G->E);
    int E = G->E;
    while(G->E--)
    {
        printf("Enter the source and destination Vertices : ");
        scanf("%d %d",&u,&v);
        printf("Enter the cost : ");
        scanf("%d",&cost);
        G->adj[u][v] = cost;
        G->adj[v][u] = cost;
    }
    int s = 0;
        Prim(G,s);
        int total = 0;
    for(i=0;i<G->V;i++)
    {
        total = total + distance[i];
    }
    printf("Total Cost : %d",total);
    return 0;
}

