#include<cstdio>
#include<algorithm>
#define V 4


void TransitiveClosure(int G[][V])
{
    int reach[V][V];
    for(int i=0;i<V;i++)
        for(int j=0;j<V;j++)
            reach[i][j] = G[i][j];
    for(int k=0;k<V;k++)
        for(int i=0;i<V;i++)
            for(int j=0;j<V;j++)
                reach[i][j] = reach[i][j] || (reach[i][k] && reach[k][j]);
    for(int i=0;i<V;i++)
    {
        for(int j=0;j<V;j++)
            printf("%d ",reach[i][j]);
        printf("\n");
    }

}


int main(void)
{
    int G[V][V] = {{1,1,0,1},
                   {0,1,1,0},
                   {0,0,1,1},
                   {0,0,0,1}
                  };
    TransitiveClosure(G);
    return 0;
}

