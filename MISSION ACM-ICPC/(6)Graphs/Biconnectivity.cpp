#include<cstdio>
#include<vector>
#include<cstdlib>
#include<algorithm>
#define NIL -1
using namespace std;
struct AL
{
    int data;
    struct AL *next;
};

void addEdge(vector<struct AL *> &G,int start,int end)
{
    struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = end;
    temp->next = G[start];  //G[start] initially contains NULL
    G[start] = temp;    //Now G[start] contains value at temp
    temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = start;
    temp->next = G[end];
    G[end] = temp;
}

void print(vector<struct AL *> &G,int N)
{
    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        printf("Node %d ",i);
        while(temp)
        {
            printf(" -> %d ",temp->data);
            temp = temp->next;
        }
        printf("\n");
    }
}
int visited[100] = {0},disc[100] = {0},low[100] = {0},parent[100] = {0};


int CutVertex(vector<struct AL *> &G,int u)
{
    static int time = 0;
	int children = 0;
	visited[u] = 1;
	disc[u] = low[u] = ++time;
	struct AL *temp = G[u];
	while(temp)
	{
		if(!visited[temp->data])
		{
			children++;
			parent[temp->data] = u;
			CutVertex(G,temp->data);
			low[u] = min(low[u],low[temp->data]);

			if(parent[u] == NIL && children > 1)
				return 1;
			if(parent[u] != NIL && low[temp->data] >= disc[u])
				return 1;
		}
		else if(temp->data != parent[u])
			low[u] = min(low[u],disc[temp->data]);
        temp = temp->next;
	}
	return 0;
}

int CutVertexUtil(vector<struct AL *> &G,int src,int N)
{
	for(int i=0;i<N;i++)
	{
		parent[i] = NIL;
		visited[i] = 0;
		//AP[i] = 0;
	}
    if(CutVertex(G,src))
        return 0;
	for(int i=0;i<N;i++)
		if(!visited[i])
			return 0;
    return 1;
}

int main(void)
{
    vector<struct AL *> Graph;
    int v = 5,i;
    for(int i=0;i<v;i++)
        Graph.push_back(NULL);
    addEdge(Graph, 1, 0);
    addEdge(Graph, 0, 2);
    addEdge(Graph, 2, 1);
    addEdge(Graph, 0, 3);
    addEdge(Graph, 3, 4);
    addEdge(Graph,2,4);
   // print(Graph,v);
    if(CutVertexUtil(Graph,0,v))
        printf("Graph is Biconnected\n");
    else
        printf("Graph is not Biconnected\n");
    return 0;
}
