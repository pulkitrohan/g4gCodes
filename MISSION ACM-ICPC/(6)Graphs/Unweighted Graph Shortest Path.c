#include<stdio.h>
int distance[100],path[100];
typedef struct graph
{
    int V,E;
    int adj[100][100];
}Graph;
typedef struct queue
{
    int *element;
    int capacity;
    int front;
    int rear;
    int size;
}queue;

queue *createQueue(int MaxSize)
{
    queue *Q;
    Q = (queue *)malloc(sizeof(queue));
    Q->element = (int *)malloc(sizeof(int)*MaxSize);
    Q->capacity = MaxSize;
    Q->front = 0;
    Q->rear = -1;
    Q->size = 0;
    return Q;
}
void Enqueue(queue *Q,int element)
{
       Q->size++;
       Q->rear++;
       Q->element[Q->rear] = element;
}

int Dequeue(queue *Q)
{
        Q->size--;
        int data = Q->element[Q->front];;
        Q->front++;
        return data;
}
int isEmpty(queue *Q)
{
    if(Q->size == 0)
        return 1;
    else return 0;
}

void UnweightedShortestPath(Graph *G,int s)
{
    queue *Q = createQueue(100);
    int i,v,w;
    Enqueue(Q,s);
    for(i=0;i<G->V;i++)
        distance[i] = -1;
    distance[s] = 0;
    while(!isEmpty(Q))
    {
        v = Dequeue(Q);
        for(w=1;w<=G->V;w++)
        {
            if(G->adj[v][w])
                if(distance[w] == -1)
                {
                    distance[w] = distance[v] + 1;
                    Enqueue(Q,w);
                }
        }
    }
}

int main(void)
{
    int i,j,k,u,v,s=0;
    Graph *G = (Graph *)malloc(sizeof(Graph));
    printf("\nEnter Total Vertices : ");
    scanf("%d",&G->V);
    for(i=0;i<=G->V;i++)
    {
        for(j=0;j<=G->V;j++)
            G->adj[i][j] = 0;
    }
    printf("\nEnter Total Edges : ");
    scanf("%d",&G->E);
    for(k = 0;k<G->E;k++)
    {
        printf("Enter the Starting and Destination Vertices : ");
        scanf("%d %d",&u,&v);
        G->adj[u][v] = 1;          //Assumed directed Graph
    }
    UnweightedShortestPath(G,s);
    for(i=0;i<G->V;i++)
    {
        printf("\nMinimum Distance of vertex %d from %d is %d",i,s,distance[i]);
    }
    return 0;
}
