#include<bits/stdc++.h>
using namespace std;
#define ROW 5
#define COL 5

int isSafe(int M[][COL],int row,int col,bool visited[][COL])
{
	return (row >= 0 && row < ROW && col >= 0 && col <= COL && M[row][col] && !visited[row][col]);
}

void dfs(int M[][COL],int r,int c,bool visited[][COL])
{
	static int row[] = {-1,-1,-1,0,0,1,1,1};
	static int col[] = {-1,0,1,-1,1,-1,0,1};
	visited[r][c] = true;
	for(int i=0;i<8;i++)
		if(isSafe(M,r+row[i],c+col[i],visited))
			dfs(M,r+row[i],c+col[i],visited);
}

int countIslands(int M[][COL])
{
	bool visited[ROW][COL];
	memset(visited,false,sizeof(visited));
	int count = 0;
	for(int i=0;i<ROW;i++)
		for(int j=0;j<COL;j++)
			if(M[i][j] && !visited[i][j])
			{
				dfs(M,i,j,visited);
				count++;
			}
	return count;
}

int main(void)
{
	int M[ROW][COL] = { {1,1,0,0,0},{0,1,0,0,1},{1,0,0,1,1},{0,0,0,0,0},{1,0,1,0,1}};
	cout<<countIslands(M)<<endl;
	return 0;
}
						