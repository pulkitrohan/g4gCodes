#include<bits/stdc++.h>
using namespace std;
struct AL
{
	int data;
	struct AL *next;
};

void AddEdge(vector<struct AL *> &G,int start,int end)
{
	struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
	temp->data = end;
	temp->next = G[start];
	G[start] = temp;
}

void PrintAllPaths(vector<struct AL *> &G,int start,int end,int &count,int *visited,vector<int> &path)
{
	visited[start] = 1;
	path.push_back(start);
	if(start == end)
	{
		for(int i=0;i<path.size();i++)
			cout<<path[i]<<" ";
		cout<<endl;
	}
	else
	{
		struct AL *temp = G[start];
		while(temp)
		{	
			if(!visited[temp->data])
				PrintAllPaths(G,temp->data,end,count,visited,path);
			temp = temp->next;
		}
	}
	path.pop_back();
	visited[start] = 0;
}

int main(void)
{
	int N = 4;
	vector<struct AL *> G;
	for(int i=0;i<N;i++)
		G.push_back(NULL);
	AddEdge(G,0,1);
	AddEdge(G,0,2);
	AddEdge(G,0,3);
	AddEdge(G,2,0);
	AddEdge(G,2,1);
	AddEdge(G,1,3);
	int start = 2,end = 3;
	int count = 0;
	int visited[N];
	memset(visited,0,sizeof(visited));
	vector<int>path;
	PrintAllPaths(G,start,end,count,visited,path);
	return 0;
}