#include<stdio.h>

typedef struct LL
{
    char data;
    int freq;
    struct LL *next;
    struct LL *left,*right;
}node;
void CreateNode(node *start)
{
    node *temp = start;
    char data;
    int freq;
    printf("Enter the character and frequency : ");
    scanf("%c %d",&data,&freq);
    while(temp->next && temp->next->freq < freq)
        temp = temp->next;
    if(temp->next == NULL)
    {
        temp->next = (node *)malloc(sizeof(temp));
        temp = temp->next;
        temp->data = data;
        temp->freq = freq;
        temp->next = NULL;
        temp->left = NULL;
        temp->right = NULL;
    }
    else
    {
        node *newNode = (node *)malloc(sizeof(node));
        newNode->next = temp->next;
        temp->next = newNode;
        newNode->data = data;
        newNode->freq = freq;
        newNode->left = NULL;
        newNode->right = NULL;
    }
}

void insert(node *start,node *pointer)
{
    node *temp = start;
    while(temp->next && temp->next->freq < pointer->freq)
        temp = temp->next;
    if(temp->next == NULL)
    {
        temp->next = (node *)malloc(sizeof(temp));
        temp = temp->next;
        temp->data = pointer->data;
        temp->freq = pointer->freq;;
        temp->next = NULL;
    }
    else
    {
        node *newNode = (node *)malloc(sizeof(node));
        newNode->next = temp->next;
        temp->next = newNode;
        newNode->data = pointer->data;;
        newNode->freq = pointer->freq;
    }
}
node *delete(node *start)
{
    node *temp = start->next;
    if(start->next == NULL)
        printf("Underflow");
    else
    {
        start->next = temp->next;
        return temp;
    }
}
void Convert(node *start)
{
    while(start->next)
    {
        node *temp = (node *)malloc(sizeof(node));
        if(start->next)
            temp->left = delete(start);
        if(start->next)
            temp->right = delete(start);
        temp->freq = temp->left->freq + temp->right->freq;
        insert(start,temp);
    }
}

int main(void)
{
    int n;
    node *start = (node *)malloc(sizeof(node));
    start->next = NULL;
    printf("Enter the total number of elements : ");
    scanf("%d",&n);
    while(n--)
        CreateNode(start);
        Convert(start);

    return 0;
}
