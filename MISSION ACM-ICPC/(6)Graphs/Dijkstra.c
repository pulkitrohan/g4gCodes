#include<stdio.h>
#include<stdlib.h>
#define INF 1000
int distance[100],path[100];
typedef struct graph
{
    int V,E;
    int adj[100][100];
}Graph;


void Dijkstra(Graph *G,int s)
{
    int count = 0,min,i,visited[100],u;
    for(i=0;i<G->V;i++)
        {
            distance[i] = INF;
            visited[i] = 0;
        }
        distance[s] = 0;
        while(count <= G->V)
        {
            min = 99;
            for(i=0;i<G->V;i++)
            {
                if(distance[i] < min && !visited[i])
                    {
                        min = distance[i];
                        u = i;
                    }
            }
            visited[u] = 1;
            count++;
            for(i=0;i<G->V;i++)
            {
                if(G->adj[u][i] && !visited[i])
                    if((distance[u] + G->adj[u][i] < distance[i]))
                        distance[i] = distance[u] + G->adj[u][i];
            }
        }
}
int main(void)
{
    int i,j,k,u,v,s=0,cost;
    Graph *G = (Graph *)malloc(sizeof(Graph));
    printf("\nEnter Total Vertices : ");
    scanf("%d",&G->V);
    for(i=0;i<=G->V;i++)
    {
        for(j=0;j<=G->V;j++)
            G->adj[i][j] = 0;
    }
    printf("\nEnter Total Edges : ");
    scanf("%d",&G->E);
    for(k = 0;k<G->E;k++)
    {
        printf("Enter the Starting and Destination Vertices : ");
        scanf("%d %d",&u,&v);
        printf("Enter the Cost : ");
        scanf("%d",&cost);
        G->adj[u][v] = cost;          //Assumed directed Graph
    }
    Dijkstra(G,s);
    for(i=0;i<G->V;i++)
    {
        printf("\nDistance of vertex %d from %d is %d",i,s,distance[i]);
    }
    printf("Enter the destination node : ");
    scanf("%d",&v);
    printf("\nDistance from source %d is %d",s,distance[v]);
    return 0;
}
