#include<bits/stdc++.h>
using namespace std;
struct AL
{
    int data;
    struct AL *next;
};

int V[10] = {0};
void addEdge(vector<struct AL *> &G,int start,int end)
{
    struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = end;
    temp->next = G[start];  //G[start] initially contains NULL
    G[start] = temp;    //Now G[start] contains value at temp
}

void print(vector<struct AL *> &G,int N)
{
    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        printf("Node %d ",i);
        while(temp)
        {
            printf(" -> %d ",temp->data);
            temp = temp->next;
        }
        printf("\n");
    }
}


int getMaxNumCountInBipartite(vector<struct AL *> &G,int src,int v)
{
    int colorArr[v];
    int i;
    for(i=0;i<v;i++)
        colorArr[i] = -1;
    colorArr[src] = 1;
    int colorCount[2] = {0};
    colorCount[1]++;
    queue<int> Q;
    Q.push(src);
    while(!Q.empty())
    {
        int j = Q.front();
        Q.pop();
        struct AL *temp = G[j];
        while(temp)
        {
            if(colorArr[temp->data] == -1)
            {
                colorArr[temp->data] = 1-colorArr[j];
                colorCount[1-colorArr[j]]++;
                Q.push(temp->data);
            }
            else if(colorArr[j] == colorArr[temp->data])
                return 0;
            temp = temp->next;
        }
    }
    return colorCount[0] * colorCount[1] - (G.size()-1);
}

int main(void)
{
    vector<struct AL *> Graph;
    int v = 5,i;
    for(int i=0;i<v;i++)
        Graph.push_back(NULL);
    addEdge(Graph, 0, 1);
    addEdge(Graph, 0, 2);
    addEdge(Graph, 1, 3);
    addEdge(Graph, 2, 4);
    cout<<getMaxNumCountInBipartite(Graph, 0, v)<<endl;
    return 0;
}


