#include<stdio.h>
typedef struct graph
{
    int V,E;
    int adj[100][100];
}Graph;
reach[100];

typedef struct queue
{
    int *element;
    int capacity;
    int front;
    int rear;
    int size;
}queue;

queue *createQueue(int MaxSize)
{
    queue *Q;
    Q = (queue *)malloc(sizeof(queue));
    Q->element = (int *)malloc(sizeof(int)*MaxSize);
    Q->capacity = MaxSize;
    Q->front = 0;
    Q->rear = -1;
    Q->size = 0;
    return Q;
}
void Enqueue(queue *Q,int element)
{
       Q->size++;
       Q->rear++;
       Q->element[Q->rear] = element;
}

int Dequeue(queue *Q)
{
        Q->size--;
        int data = Q->element[Q->front];;
        Q->front++;
        return data;
}
int isEmpty(queue *Q)
{
    if(Q->size == 0)
        return 1;
    else return 0;
}
void bfs(Graph *G,int u)
{
    int i;
    queue *Q = createQueue(100);
    Enqueue(Q,u);
    while(!isEmpty(Q))
    {
        u = Dequeue(Q);
        if(reach[u] == 0)
            printf("%d ",u);
        reach[u] = 1;
        for(i=0;i<G->V;i++)
        {
            if(!reach[i] && G->adj[u][i])
                Enqueue(Q,i);
        }
    }
}

int main(void)
{
    int i,j,u,v,count = 0;
    Graph *G = (Graph *)malloc(sizeof(Graph));
    printf("Enter the total Vertex : ");
    scanf("%d",&G->V);
    for(i=0;i<G->V;i++)
      {
        reach[i] = 0;
          for(j=0;j<G->V;j++)
            G->adj[i][j] = 0;
      }
    printf("Enter the total Edges : ");
    scanf("%d",&G->E);
    for(i=0;i<G->E;i++)
    {
        printf("Enter the starting and destination vertex : ");
        scanf("%d %d",&u,&v);
        G->adj[u][v] = 1;
    }
    bfs(G,0);
    for(i=0;i<G->V;i++)
    {
        if(reach[i])
            count++;
    }
    if(count == G->V)
        printf("\nConnected");
    else
        printf("Not Connected");
    return 0;
}
