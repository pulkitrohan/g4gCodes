#include<bits/stdc++.h>
using namespace std;
#define V 4
#define INF 10000

int shortestPath(int graph[][V],int start,int end,int k)
{
	int dp[V][V][k+1];
	for(int e=0;e<=k;e++)
	{
		for(int i=0;i<V;i++)
		{
			for(int j=0;j<V;j++)
			{
				dp[i][j][e] = INF;
				if(e == 0 && i == j)
					dp[i][j][e] = 0;
				if(e == 1 && graph[i][j] != INF)
					dp[i][j][e] = graph[i][j];
				if(e > 1)
					for(int a=0;a<V;a++)
						if(graph[i][a] != INF && i != a && j != a && dp[a][j][e-1] != INF)
							dp[i][j][e] = min(dp[i][j][e],dp[a][j][e-1] + graph[i][a]);
			}
		}
	}
	return dp[start][end][k];
}

int main(void)
{
	int graph[V][V] = { {0, 10, 3, 2},
                        {INF, 0, INF, 7},
                        {INF, INF, 0, 6},
                        {INF, INF, INF, 0}
                      };
    int u = 0, v = 3, k = 2;
    cout << shortestPath(graph, u, v, k);
    return 0;
}