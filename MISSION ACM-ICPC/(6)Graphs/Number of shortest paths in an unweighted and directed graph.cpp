#include<bits/stdc++.h>
using namespace std;
struct AL
{
    int data;
    struct AL *next;
};

void addEdge(vector<struct AL *> &G,int start,int end)
{
    struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = end;
    temp->next = G[start];  //G[start] initially contains NULL
    G[start] = temp;    //Now G[start] contains value at temp
}

void print(vector<struct AL *> &G,int N)
{
    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        printf("Node %d ",i);
        while(temp)
        {
            printf(" -> %d ",temp->data);
            temp = temp->next;
        }
        printf("\n");
    }
}


void bfs(vector<struct AL *> &G,int start, vector<int> &distance, vector<int> &paths)
{
    
    int N = G.size();   
    vector<bool> visited(N, false);
    distance[start] = 0;
    paths[start] = 1;

    queue<int> Q;
    Q.push(start);
    visited[start] = true;
    while(!Q.empty())
    {
        int j = Q.front();
        Q.pop();
        struct AL *temp = G[j];
        while(temp)
        {
            if(!visited[temp->data]) {
                Q.push(temp->data);
                visited[temp->data] = true;
            }
                
            if(distance[temp->data] > distance[j] + 1) {
                distance[temp->data] = distance[j] + 1;
                paths[temp->data] = paths[j];
            }
            else if(distance[temp->data] == distance[j] + 1) {
                paths[temp->data] += paths[j];
            }
            temp = temp->next;
        }
    }
}

void findShortestPaths(vector<struct AL *> &G, int start) {
    int N = G.size();
    vector<int> distance(N, INT_MAX-100);
    vector<int> paths(N, 0);
    bfs(G, start, distance, paths);
    for(int i=0;i<N;i++)
        cout<<paths[i]<<" ";
}

int main(void)
{
    vector<struct AL *> Graph(7, NULL);
    addEdge(Graph, 0, 1);
    addEdge(Graph, 0, 2);
    addEdge(Graph, 1, 2);
    addEdge(Graph, 1, 3);
    addEdge(Graph, 2, 3);
    addEdge(Graph, 3, 4);
    addEdge(Graph, 3, 5);
    addEdge(Graph, 4, 6);
    addEdge(Graph, 5, 6);
    findShortestPaths(Graph, 0);
    return 0;
}

