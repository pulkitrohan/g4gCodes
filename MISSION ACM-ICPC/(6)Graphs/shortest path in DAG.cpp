#include<cstdio>
#include<vector>
#include<cstdlib>
#include<queue>
#include<limits.h>
#define INF 100
using namespace std;
struct AL
{
    int data;
    struct AL *next;
    int cost;
};

void addEdge(vector<struct AL *> &G,int start,int end,int cost)
{
    struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = end;
    temp->cost = cost;
    temp->next = G[start];  //G[start] initially contains NULL
    G[start] = temp;    //Now G[start] contains value at temp

}

void print(vector<struct AL *> &G,int N)
{
    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        printf("Node %d\n",i);
        printf("Adj Vertex\tCost\n");
        while(temp)
        {
            printf("%d\t\t%d\n",temp->data,temp->cost);
            temp = temp->next;
        }
        printf("\n");
    }
}

void ShortestPathDAG(vector<struct AL *> &G,int N,int s)
{
    int indegree[100] = {0};
    int Sorted[N+1];
    queue<int> Q;

    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        while(temp)
        {
            indegree[temp->data]++;
                temp = temp->next;
        }
    }

    for(int i = 0;i<N;i++)
        if(!indegree[i])
            Q.push(i);
    int k = 0,count = 0;
    while(!Q.empty())
    {
        int j = Q.front();
        Q.pop();
        Sorted[k++] = j;
        count++;
        struct AL *temp = G[j];
        while(temp)
        {

            indegree[temp->data]--;
            if(!indegree[temp->data])
                Q.push(temp->data);
            temp = temp->next;
        }
    }
    int i;
    int dist[N+1];
    for(i=0;i<N;i++)
        dist[i] = INF;
    dist[s] = 0;
    for(i=0;i<N;i++)
    {
        int j = Sorted[i];
        struct AL *temp = G[j];
        while(temp)
        {
            //printf("%d %d %d\n",j,temp->data,temp->cost);
            if(dist[temp->data] > dist[j]+temp->cost)
                dist[temp->data] = dist[j]+temp->cost;
            temp = temp->next;
        }
    }
    for(i=0;i<N;i++)
    {
        if(dist[i] == INF)
            printf("INF ");
        else
            printf("%d ",dist[i]);
    }
}


int main(void)
{
    vector<struct AL *> G;
    int v = 6;
    for(int i=0;i<v;i++)
        G.push_back(NULL);
    addEdge(G,0, 1, 5);
    addEdge(G,0, 2, 3);
    addEdge(G,1, 3, 6);
    addEdge(G,1, 2, 2);
    addEdge(G,2, 4, 4);
    addEdge(G,2, 5, 2);
    addEdge(G,2, 3, 7);
    addEdge(G,3, 5, 1);
    addEdge(G,3, 4, -1);
    addEdge(G,4, 5, -2);
    ShortestPathDAG(G,v,1);
    //print(G,v);
    return 0;
}
