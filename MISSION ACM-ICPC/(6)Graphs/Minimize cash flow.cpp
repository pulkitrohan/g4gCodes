#include<bits/stdc++.h>
using namespace std;

int getMax(vector<int> &amount)
{
	int index = 0;
	for(int i=1;i<amount.size();i++)
		if(amount[i] > amount[index])
			index = i;
	return index;
}

int getMin(vector<int> &amount)
{
	int index = 0;
	for(int i=0;i<amount.size();i++)
		if(amount[i] < amount[index])
			index = i;
	return index;
}

void minCash(vector<int> &amount)
{
	int maxcredit = getMax(amount);
	int maxdebit = getMin(amount);
	if(amount[maxcredit] == 0 && amount[maxdebit] == 0)
		return;
	int min2 = min(-amount[maxdebit],amount[maxcredit]);
	amount[maxcredit] -= min2;
	amount[maxdebit] += min2;
	cout<<"Person "<<maxdebit<<" pays "<<min2<<" to Person "<< maxcredit<<endl;
	minCash(amount);
}

void minCashFlow(int graph[][3],int N)
{
	vector<int> amount(N,0);
	for(int i=0;i<N;i++)
		for(int j=0;j<N;j++)
			amount[i] += (graph[j][i] - graph[i][j]);
	minCash(amount);
}

int main(void)
{
	int graph[3][3] = {{0,1000,2000},
					   {0,0,5000},
					   {0,0,0}};
					   
	minCashFlow(graph,3);
	return 0;
}