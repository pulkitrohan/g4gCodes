#include<cstdio>
#include<cstdlib>
#include<vector>

using namespace std;
struct AL
{
    int data;
    struct AL *next;
};
int visited[100] = {0};

void addEdge(vector<struct AL *> &G,int start,int end)
{
    struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = end;
    temp->next = G[start];  //G[start] initially contains NULL
    G[start] = temp;    //Now G[start] contains value at temp
    temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = start;
    temp->next = G[end];
    G[end] = temp;
}

int DetectCycleUndirected(vector<struct AL *> Graph,int start,int parent)
{

    visited[start] = 1;
    struct AL *temp = Graph[start];
    while(temp)
    {
        if(!visited[temp->data] && DetectCycleUndirected(Graph,temp->data,start))
            return 1;
        else if(temp->data != parent)
            return 1;
        temp = temp->next;
    }
    return 0;
}

int main(void)
{
    vector<struct AL *> Graph;
    int V = 5,i;
    for(i=0;i<V;i++)
        Graph.push_back(NULL);
    addEdge(Graph, 0, 1);
    addEdge(Graph, 0, 2);
    addEdge(Graph, 1, 2);
    addEdge(Graph, 0, 3);
    addEdge(Graph, 3, 4);
    for(i=0;i<V;i++)
        if(DetectCycleUndirected(Graph,i,-1))
        {
            printf("Cycle Present\n");
            return 0;
        }
    printf("Cycle Not Present\n");
    return 0;
}

