#include<bits/stdc++.h>
using namespace std;
struct AL
{
    int data;
    struct AL *next;
};

void addEdge(vector<struct AL *> &G,int start,int end)
{
    struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = end;
    temp->next = G[start];  //G[start] initially contains NULL
    G[start] = temp;    //Now G[start] contains value at temp
    temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = start;
    temp->next = G[end];
    G[end] = temp;
}

void print(vector<struct AL *> &G,int N)
{
    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        printf("Node %d ",i);
        while(temp)
        {
            printf(" -> %d ",temp->data);
            temp = temp->next;
        }
        printf("\n");
    }
}

void CreateTransposeGraph(vector<struct AL *>&T,vector<struct AL *> &G,int N)
{
    for(int i=0;i<N;i++)
        T.push_back(NULL);
    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        while(temp)
        {
            addEdge(T, temp->data, i);
            temp = temp->next;
        }
    }
}

void dfs(vector<struct AL *> &G,int u,int *v)
{
	v[u] = 1;
	struct AL *temp = G[u];
	while(temp)
	{
		if(!v[temp->data])
			dfs(G,temp->data,v);
		temp = temp->next;
	}
}



int isConnected(vector<struct AL *> &G,int N)
{
	int v[N+1];
	for(int i=0;i<N;i++)
		v[i] = 0;
    int i;
	for( i=0;i<N;i++)	// Finding vertex with non zero outdegree
	{
		struct AL *temp = G[i];
		if(temp)
			break;
	}
	if(i == N)
		return 1;
	dfs(G,i,v);

	for(int i=0;i<N;i++)
	{
		struct AL *temp = G[i];
		if(temp && !v[i])
			return 0;
	}
	return 1;
}

int isEulerian(vector<struct AL *> &G,int N)
{
	if(!isConnected(G,N))
		return 0;

	int odd = 0;
	for(int i=0;i<N;i++)
	{
		struct AL *temp = G[i];
		int count = 0;
		while(temp)
		{
			count++;
			temp = temp->next;
		}
		if(count & 1)
            odd++;
	}

	if(odd > 2)
		return 0;
	if(odd == 2)
		return 1;
	if(odd == 0)
		return 2;
	
}

void Euler(vector<struct AL *> &G,int N)
{
	int res = isEulerian(G,N);
	if(res == 0)
		printf("Graph is not Eulerian\n");
	else if(res == 1)
		printf("Graph has a Euler path\n");
	else
		printf("Graph has a Euler cycle\n");
}

int main(void)
{
    vector<struct AL *> Graph;
    int v = 5,i;
    for(int i=0;i<v;i++)
        Graph.push_back(NULL);
    addEdge(Graph, 1,0);
    addEdge(Graph, 0, 2);
    addEdge(Graph, 2, 1);
    addEdge(Graph, 0, 3);
    addEdge(Graph, 3, 4);
    addEdge(Graph,1,3);
	Euler(Graph,v);
    return 0;
}
