#include<stdio.h>
#include<stdlib.h>
typedef struct graph
{
    int V,E;
    int adj[100][100];
}Graph;
int reach[100];
void dfs(Graph *G,int v)
{
    reach[v] = 1;
    int i;
    for(i=0;i<G->V;i++)
    {
        if(G->adj[v][i] && !reach[i])
        {
            printf("\nEdge : %d -> %d",v,i);
            dfs(G,i);
        }
    }
    return 0;
}

int main(void)
{
    int i,j,k,u,v,count = 0;
    Graph *G = (Graph *)malloc(sizeof(Graph));
    printf("\nEnter Total Vertices : ");
    scanf("%d",&G->V);
    for(i=0;i<G->V;i++)
    {
        reach[i] = 0;
        for(j=0;j<G->V;j++)
        G->adj[i][j] = 0;
    }
    printf("\nEnter Total Edges : ");
    scanf("%d",&G->E);
    for(k = 0;k<G->E;k++)
    {
        printf("Enter the Starting and Destination Vertices : ");
        scanf("%d %d",&u,&v);
        G->adj[u][v]++;
    }
    k = 1;
    for(i=0;i<G->V;i++)
    {
        if(!reach[i])
           dfs(G,i);
    }

    for(i=0;i<G->V;i++)
        if(reach[i])
            count++;
        if(count == G->V)
            printf("Connected");
    return 0;
}

