#include<bits/stdc++.h>
using namespace std;
struct AL
{
    int data;
    struct AL *next;
};

void addEdge(vector<struct AL *> &G,int start,int end)
{
    struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = end;
    temp->next = G[start];  //G[start] initially contains NULL
    G[start] = temp;    //Now G[start] contains value at temp
}

void print(vector<struct AL *> &G,int N)
{
    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        printf("Node %d ",i);
        while(temp)
        {
            printf(" -> %d ",temp->data);
            temp = temp->next;
        }
        printf("\n");
    }
}


void bfs(vector<struct AL *> &G,int start,int N)
{
    queue<int> Q;
    Q.push(start);
	int visited[N];
	memset(visited,0,sizeof(visited));
    while(!Q.empty())
    {
		int j = Q.front();
		Q.pop();
		cout<<j<<" ";
		visited[j] = 1;
		struct AL *temp = G[j];
        while(temp)
        {
            if(!visited[temp->data])
                Q.push(temp->data);
            temp = temp->next;
        }
    }
}

int main(void)
{
    vector<struct AL *> Graph;
    int v = 5,i;
    for(int i=0;i<v;i++)
        Graph.push_back(NULL);
    addEdge(Graph, 0, 1);
    addEdge(Graph, 0, 2);
    addEdge(Graph, 1, 2);
    addEdge(Graph, 2, 0);
    addEdge(Graph, 2, 3);
    addEdge(Graph, 3, 3);
    bfs(Graph,2,v);
    return 0;
}

