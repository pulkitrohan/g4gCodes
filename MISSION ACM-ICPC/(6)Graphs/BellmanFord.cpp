#include<bits/stdc++.h>
using namespace std;
struct AL
{
    int data,weight;
    struct AL *next;
};

void addEdge(vector<struct AL *> &G,int start,int end,int weight)
{
    struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = end;
    temp->next = G[start];  //G[start] initially contains NULL
	temp->weight = weight;
    G[start] = temp;    //Now G[start] contains value at temp

}

void print(vector<struct AL *> &G,int N)
{
    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        printf("Node %d ",i);
        while(temp)
        {
            printf(" -> %d ",temp->data);
            temp = temp->next;
        }
        printf("\n");
    }
}

void BellmanFord(vector<struct AL *> G,int start,int N)
{
	int dist[N+1];
	for(int i=0;i<N;i++)
		dist[i] = INT_MAX;
	dist[start] = 0;
	for(int i=0;i<N-1;i++)
	{
		for(int j=0;j<N;j++)
		{
			struct AL *temp = G[j];
			while(temp)
			{
				if(dist[j] != INT_MAX && (dist[temp->data] > dist[j] + temp->weight))
					dist[temp->data] = dist[j] + temp->weight;
				temp = temp->next;
			}
		}
	}
	int flag = 0;
	for(int i=0;i<N;i++)
	{
		struct AL *temp = G[i];
		while(temp)
		{
			if(dist[i] != INT_MAX && (dist[temp->data] > dist[i] + temp->data))
			{
				printf("GRAPH CONTAINS NEGATIVE CYCLE\n");
				return;
			}
		}
	}
	printf("Vertex  Distance\n");
	for(int i=0;i<N;i++)
		printf("%d\t%2d\n",i,dist[i]);
}


int main(void)
{
    vector<struct AL *> Graph;
    int v = 5;
    for(int i=0;i<v;i++)
        Graph.push_back(NULL);
    addEdge(Graph, 0, 1,-1);
	addEdge(Graph, 0, 2,4);
	addEdge(Graph, 1, 2,3);
	addEdge(Graph, 1, 3,2);
	addEdge(Graph, 1, 4,2);
	addEdge(Graph, 3, 1,1);
	addEdge(Graph, 3, 2,5);
	addEdge(Graph, 4, 3,-3);
    BellmanFord(Graph,0,v);
    return 0;
}
