#include<stdio.h>
#include<stdlib.h>
typedef struct LL
{
	int data;
	struct LL *next;
}LL;

typedef struct Header
{
	LL *head;
}node;

typedef struct Graph
{
	int V;
	node *adj;
}Graph;

Graph *CreateGraph(int V)
{
    int i;
	Graph *G = (Graph *)malloc(sizeof(Graph));
	G->V = V;
	G->adj = (node *)malloc(sizeof(node) * V);
	for(i=0;i<G->V;i++)
		G->adj[i].head = NULL;
	return G;
}

void addEdge(Graph *G,int s,int d)
{
	LL *newNode = (LL *)malloc(sizeof(LL));
	newNode->data = d;
	newNode->next = G->adj[s].head;
	G->adj[s].head = newNode;
}

void Print(Graph *G)
{	int i;
	for(i=0;i<G->V;i++)
	{
		LL *Crawl = G->adj[i].head;
		printf("Adjacency List of Vertex %d : %d",i,i);
		while(Crawl)
		{
			printf("->%d ",Crawl->data);
			Crawl = Crawl->next;
		}
		printf("\n");
	}
}

int main(void)
{
    int V,E,u,v;
    printf("Enter the number of Vertices : ");
    scanf("%d",&V);
    printf("Enter the number of Edges : " );
    scanf("%d",&E);
    Graph *G = CreateGraph(V);
    while(E--)
    {
        printf("Enter Source and Destination Vertices : ");
        scanf("%d %d",&u,&v);
        addEdge(G,u,v);
    }
    Print(G);
    return 0;
}

