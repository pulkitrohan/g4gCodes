#include<bits/stdc++.h>
using namespace std;

int fillHeight(int parent[], int node, vector<bool> &visited, vector<int> &height) {

    if(parent[node] == -1) {
        visited[node]= true;
        return 0;
    }
    if(visited[node])
        return height[node];

    visited[node] = true;
    height[node] = 1 + fillHeight(parent, parent[node], visited, height);
    return height[node];
}

int findHeight(int parent[], int N) {
    int maxHeight = 0;
    vector<bool> visited(N, 0);
    vector<int> height(N, 0);

    for(int i=0;i<N;i++) {
        if(!visited[i])
            height[i] = fillHeight(parent, i, visited, height);
        maxHeight = max(maxHeight, height[i]);
    }
    return maxHeight;
}

int main(void) {
    int parent[] = {-1, 0, 0, 0, 3, 1, 1, 2};
    int N = sizeof(parent)/sizeof(parent[0]);
    cout<<findHeight(parent, N);
    return 0;
}