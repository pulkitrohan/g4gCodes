#include<bits/stdc++.h>
using namespace std;
struct AL
{
    int data;
    struct AL *next;
};

int V[10] = {0};


void addEdge(vector<struct AL *> &G,int start,int end)
{
    struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = end;
    temp->next = G[start];  //G[start] initially contains NULL
    G[start] = temp;    //Now G[start] contains value at temp
}

void print(vector<struct AL *> &G,int N)
{
    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        printf("Node %d ",i);
        while(temp)
        {
            printf(" -> %d ",temp->data);
            temp = temp->next;
        }
        printf("\n");
    }
}


void dfs(vector<struct AL *> &G,int start)
{
    V[start] = 1;
    cout<<start<<" ";
    struct AL *temp = G[start];
    while(temp)
    {   
        if(!V[temp->data])
            dfs(G,temp->data);
        temp = temp->next;
    }
}

void dfsIterative(vector<struct AL *> &G,int start)
{
    vector<bool> visited(G.size(),false);
    stack<int> S;
    S.push(start);
    while(!S.empty())
    {
        int node = S.top();
        S.pop();

        if(!visited[node]) {
            cout<<node<< " ";
            visited[node] = true;
        }

        struct AL *temp = G[node];
        while(temp)
        {
            if(!visited[temp->data])
                S.push(temp->data);
            temp = temp->next;
        }
    }
}
        

int main(void)
{
    vector<struct AL *> Graph;
    int v = 3,i;
    for(int i=0;i<v;i++)
        Graph.push_back(NULL);
    addEdge(Graph, 0, 1);
    addEdge(Graph, 0, 2);
    addEdge(Graph, 1, 2);
    addEdge(Graph, 2, 0);
    addEdge(Graph, 2, 3);
    addEdge(Graph, 3, 3);
    print(Graph,v);
    dfs(Graph,0);
    printf("\n");
    dfsIterative(Graph,0);
    return 0;
}
