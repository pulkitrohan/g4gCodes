#include<bits/stdc++.h>
using namespace std;
struct AL
{
    int data;
    struct AL *next;
};

void addEdge(vector<struct AL *> &G,int start,int end)
{
    struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = end;
    temp->next = G[start];  //G[start] initially contains NULL
    G[start] = temp;    //Now G[start] contains value at temp
}

bool DetectCycle(vector<struct AL *> &G,int start,vector<bool> &visited,vector<bool> &Stack)
{
	visited[start] = true;
	Stack[start] = true;
	struct AL *temp = G[start];
	while(temp)
	{
		if(!visited[temp->data] && DetectCycle(G,temp->data,visited,Stack))
			return true;
		else if(Stack[temp->data])
			return true;
		temp = temp->next;
	}
	Stack[start] = false;
	return false;
}

int main(void)
{
    vector<struct AL *> Graph;
    int V = 4,i;
    for(i=0;i<V;i++)
        Graph.push_back(NULL);
    addEdge(Graph, 0, 1);
    addEdge(Graph, 0, 2);
    addEdge(Graph, 1, 2);
    addEdge(Graph, 2, 0);
    addEdge(Graph, 2, 3);
    addEdge(Graph, 3, 3);
    vector<bool> visited(Graph.size(),false),Stack(Graph.size(),false);
	cout<<DetectCycle(Graph,0,visited,Stack);
    return 0;
}
