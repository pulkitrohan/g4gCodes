/*

Dijkstra fails for negative edges graph as it works on greedy programming paradigm. In greedy programming, we assume
that optimal solution to a subproblem will lead to the optimal solution to the problem.
       A
      / \
     /   \
    /     \
   5       2
  /         \
  B--(-10)-->C

V={A,B,C} ; E = {(A,C,2), (A,B,5), (B,C,-10)}

Note that this is important, because in each relaxation step, the algorithm assumes the "cost" to the "closed" nodes
 is indeed minimal, and thus the node that will next be selected is also minimal.

The idea of it is: If we have a vertex in open such that its cost is minimal - by adding any positive number to 
any vertex - the minimality will never change. 
Without the constraint on positive numbers - the above assumption is not true.

Since we do "know" each vertex which was "closed" is minimal - we can safely do the relaxation step - without
 "looking back". If we do need to "look back" - Bellman-Ford offers a recursive-like (DP) solution of doing so.

In this graph, path from A->C will be 2 when traversed from A as per greedy programming.
 But as we move to B we can see that it will be -5 which breaks the greedy problem.
*/


#include<bits/stdc++.h>
using namespace std;
struct AL
{
    int data;
    int weight;
    struct AL *next;
};
#define MAX 100000
typedef pair<int,int> P;

void addEdge(vector<struct AL *> &G,int start,int end,int weight)
{
    struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
    temp->weight = weight;
    temp->data = end;
    temp->next = G[start];  //G[start] initially contains NULL
    G[start] = temp;    //Now G[start] contains value at temp
}

void print(vector<struct AL *> &G,int N)
{
    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        printf("Node %d ",i);
        while(temp)
        {
            printf(" -> %d ",temp->data);
            temp = temp->next;
        }
        printf("\n");
    }
}

class cmp
{	
	public:
		bool operator()(P &A,P &B)
		{
			return A.second > B.second;
		}
};

void dijkstra(vector<struct AL *> &G,int start,int N)
{
    int distance[N+1],v[N+1],i;
    for(i=0;i<N;i++)
    {
		distance[i] = MAX;
		v[i] = 0;
	}
    distance[start] = 0;
	priority_queue<P,vector<P >,cmp > PQ;
	PQ.push(make_pair(start,0));
	P A;
	while(!PQ.empty())
	{
		A = PQ.top();
		PQ.pop();
		int u = A.first;
		int d = A.second;
		if(v[u] == 1)
			continue;
		v[u] = 1;
		struct AL *temp = G[u];
		while(temp)
		{
			if(distance[temp->data] > distance[u] + temp->weight)
				distance[temp->data] = distance[u] + temp->weight;
			if(!v[temp->data])
				PQ.push(make_pair(temp->data,distance[temp->data]));
			temp = temp->next;
		}
	}
		
	for(i=0;i<N;i++)
		printf("%d -- %d ",i,distance[i]);
  
}

int main(void)
{
    vector<struct AL *> Graph;
    int v = 5,i;
    for(int i=0;i<v;i++)
        Graph.push_back(NULL);
    addEdge(Graph, 0, 1,10);
    addEdge(Graph, 0, 2,3);
    addEdge(Graph, 1, 2,1);
    addEdge(Graph, 2, 1,4);
    addEdge(Graph, 2, 3,8);
    addEdge(Graph, 1, 3,2);
    addEdge(Graph, 2, 4,2);
    addEdge(Graph, 3, 4,7);
    addEdge(Graph, 4, 3,9);
    dijkstra(Graph,0,v);
    return 0;
}


 