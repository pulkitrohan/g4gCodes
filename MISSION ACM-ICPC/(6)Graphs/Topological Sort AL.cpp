#include<bits/stdc++.h>
using namespace std;
struct AL
{
    int data;
    struct AL *next;
};

void addEdge(vector<struct AL *> &G,int start,int end)
{
    struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = end;
    temp->next = G[start];  //G[start] initially contains NULL
    G[start] = temp;    //Now G[start] contains value at temp
}

void print(vector<struct AL *> &G,int N)
{
    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        printf("Node %d ",i);
        while(temp)
        {
            printf(" -> %d ",temp->data);
            temp = temp->next;
        }
        printf("\n");
    }
}

void TopologicalSort(vector<struct AL *> &G,int N)
{
    int indegree[N+1];
	memset(indegree,0,sizeof(indegre));
    int Sorted[N+1];
    queue<int> Q;

    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        while(temp)
        {
            indegree[temp->data]++;
                temp = temp->next;
        }
    }

    for(int i = 0;i<N;i++)
        if(!indegree[i])
            Q.push(i);
    int k = 0,count = 0;
    while(!Q.empty())
    {
        int j = Q.front();
        Q.pop();
        Sorted[k++] = j;
        count++;
        struct AL *temp = G[j];
        while(temp)
        {

            indegree[temp->data]--;
            if(!indegree[temp->data])
                Q.push(temp->data);
            temp = temp->next;
        }
    }
    if(count != N)
        printf("Graph has cycle\n");
    else
    {
        for(int i = 0;i<N;i++)
            printf("%d ",Sorted[i]);
    }
}

int main(void)
{
    vector<struct AL *> Graph;
    int v = 6;
    for(int i=1;i<=v;i++)
        Graph.push_back(NULL);
    addEdge(Graph, 6, 3);
    addEdge(Graph, 6, 1);
    addEdge(Graph, 5, 1);
    addEdge(Graph, 5, 2);
    addEdge(Graph, 3, 4);
    addEdge(Graph, 4, 2);
    TopologicalSort(Graph,v);
   // print(Graph,v);
    return 0;
}
