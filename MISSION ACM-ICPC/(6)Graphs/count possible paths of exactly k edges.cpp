#include<bits/stdc++.h>
using namespace std;
#define V 4

int countwalks(int graph[][V],int start,int end,int k)
{
	int dp[V][V][k+1];
	for(int e=0;e<=k;e++)
	{
		for(int i=0;i<V;i++)
		{
			for(int j=0;j<V;j++)
			{
				dp[i][j][e] = 0;
				if(e == 0 && i == j)
					dp[i][j][e] = 1;
				if(e == 1 && graph[i][j])
					dp[i][j][e] = 1;
				if(e > 1)
					for(int a=0;a<V;a++)
						if(graph[i][a])
							dp[i][j][e] += dp[a][j][e-1];
			}
		}
	}
	return dp[start][end][k];
}

int main(void)
{
	int graph[V][V] = { {0, 1, 1, 1},
                        {0, 0, 0, 1},
                        {0, 0, 0, 1},
                        {0, 0, 0, 0}
                      };
    int u = 0, v = 3, k = 2;
    cout << countwalks(graph, u, v, k);
    return 0;
}