#include<bits/stdc++.h>
using namespace std;

void bfs(int start, int end, int stepNum) {

    queue<int> Q;
    Q.push(stepNum);
    while(!Q.empty()) {
        stepNum = Q.front();
        Q.pop();
        if(stepNum >= start && stepNum <= end)
            cout<<stepNum<<" ";
        if(stepNum == 0 || stepNum > end)
            continue;

        int lastDigit = stepNum%10;
        int stepNumA = stepNum*10 + (lastDigit - 1);
        int stepNumB = stepNum*10 + (lastDigit + 1);

        if(lastDigit == 0)
            Q.push(stepNumB);
        else if(lastDigit == 9)
            Q.push(stepNumA);
        else {
            Q.push(stepNumA);
            Q.push(stepNumB);
        }
    }
}


int main(void)
{
    int start = 0, end = 21;
    for(int i=0;i<=9;i++)
        bfs(start, end, i);
    return 0;
}
