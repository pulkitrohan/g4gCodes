#include<cstdio>
#include<vector>
#include<cstdlib>
#include<queue>
using namespace std;
struct AL
{
    int data;
    struct AL *next;
};

int V[10] = {0};
void addEdge(vector<struct AL *> &G,int start,int end)
{
    struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = end;
    temp->next = G[start];  //G[start] initially contains NULL
    G[start] = temp;    //Now G[start] contains value at temp
}

void print(vector<struct AL *> &G,int N)
{
    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        printf("Node %d ",i);
        while(temp)
        {
            printf(" -> %d ",temp->data);
            temp = temp->next;
        }
        printf("\n");
    }
}


int bfs(vector<struct AL *> &G,int src,int dest)
{
    queue<int> Q;
    Q.push(src);
    V[src] = 1;
    while(!Q.empty())
    {
        int j = Q.front();
        Q.pop();
        struct AL *temp = G[j];
        while(temp)
        {
            if(temp->data == dest)
                return 1;
            if(!V[temp->data])
            {
                Q.push(temp->data);
                V[temp->data] = 1;
            }
            temp = temp->next;
        }
    }
    return 0;
}

int main(void)
{
    vector<struct AL *> Graph;
    int v = 5,i;
    for(int i=0;i<v;i++)
        Graph.push_back(NULL);
    addEdge(Graph, 0, 1);
    addEdge(Graph, 0, 2);
    addEdge(Graph, 1, 2);
    addEdge(Graph, 2, 0);
    addEdge(Graph, 2, 3);
    addEdge(Graph, 3, 3);
    if(bfs(Graph,1,3))
        printf("Yes\n");
    else
        printf("No\n");
    return 0;
}


