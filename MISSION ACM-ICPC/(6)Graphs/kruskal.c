#include<stdio.h>
#include<stdlib.h>

struct Edge
{
    int src,dest,weight;
};

struct Graph
{
    int V,E;
    struct Edge *Edge;
};

struct Graph *CreateGraph(int V,int E)
{
    struct Graph *G = (struct Graph *)malloc(sizeof(struct Graph));
    G->V = V;
    G->E = E;
    G->Edge = (struct Edge *)malloc(sizeof(struct Edge) * E);
    return G;
}

int mycompare(const void *A,const void *B)
{
    struct Edge *A1 = (struct Edge *)A;
    struct Edge *B1 = (struct Edge *)B;
    return A1->weight > B1->weight;
}

void KruskalMST(struct Graph *G)
{
    int V = G->V;
    qsort(G->Edge,G->E,sizeof(G->Edge[0]),mycompare);
    struct Edge Results[G->V];
    int E = 0,i = 0;
}

int main(void)
{
    int V = 4,E = 5;
    struct Graph *G = CreateGraph(V,E);
    G->Edge[0].src = 0;
    G->Edge[0].dest = 1;
    G->Edge[0].weight = 10;

    G->Edge[1].src = 0;
    G->Edge[1].dest = 2;
    G->Edge[1].weight = 6;

    G->Edge[2].src = 0;
    G->Edge[2].dest = 3;
    G->Edge[2].weight = 5;

    G->Edge[3].src = 1;
    G->Edge[3].dest = 3;
    G->Edge[3].weight = 15;

    G->Edge[4].src = 2;
    G->Edge[4].dest = 3;
    G->Edge[4].weight = 4;

    KruskalMST(G);
    return 0;
}
