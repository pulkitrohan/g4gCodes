#include<cstdio>
#include<vector>
#include<cstdlib>
#include<queue>
using namespace std;
struct AL
{
    int data;
    struct AL *next;
};

void addEdge(vector<struct AL *> &G,int start,int end)
{
    struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = end;
    temp->next = G[start];  //G[start] initially contains NULL
    G[start] = temp;    //Now G[start] contains value at temp
}

void print(vector<struct AL *> &G,int N)
{
    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        printf("Node %d ",i);
        while(temp)
        {
            printf(" -> %d ",temp->data);
            temp = temp->next;
        }
        printf("\n");
    }
}


int bfs(vector<struct AL *> &G,int src,int N)
{
    queue<int> Q;
    Q.push(src);
    int V[N+1],i;
    for(i=0;i<N;i++)
        V[i] = 0;
    //V[i] = 1;
    while(!Q.empty())
    {
        int j = Q.front();
        //printf("%d ",j);
        V[j] = 1;
        Q.pop();
        struct AL *temp = G[j];
        while(temp)
        {
            if(!V[temp->data])
            {
                Q.push(temp->data);
               // V[temp->data] = 1;
            }
            temp = temp->next;
        }
    }
    for(i=0;i<N;i++)
        if(!V[i])
            return 0;
    return 1;
}

void CreateTransposeGraph(vector<struct AL *>&T,vector<struct AL *> &G,int N)
{
    for(int i=0;i<N;i++)
        T.push_back(NULL);
    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        while(temp)
        {
            addEdge(T, temp->data, i);
            temp = temp->next;
        }
    }
}
int main(void)
{
    vector<struct AL *> Graph;
    int v = 4,i;
    for(int i=0;i<v;i++)
        Graph.push_back(NULL);
    addEdge(Graph, 0, 1);
    addEdge(Graph, 1, 2);
    addEdge(Graph, 2, 4);
    addEdge(Graph, 4, 2);
    addEdge(Graph, 2, 3);
    addEdge(Graph, 3, 0);
    if(!bfs(Graph,0,v))
            printf("Not Strongly Connected\n");
    else
    {
        vector<struct AL *> T;
        CreateTransposeGraph(T,Graph,v);
        if(!bfs(T,0,v))
             printf("Not Strongly Connected\n");
        else
        {
            printf("Strongly Connected\n");
           // return 0;
        }
    }
    return 0;
}


