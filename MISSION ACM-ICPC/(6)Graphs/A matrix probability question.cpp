#include<bits/stdc++.h>
using namespace std;
 
bool isSafe(int x, int y, int M, int N) {
	return (x >= 0 && x < M && y >= 0 && y < N);
}

double findProbability(int M, int N, int x, int y, int steps) {

	if(!isSafe(x, y, M, N))
		return 0.0;
	if(steps == 0)
		return 1.0;

	return findProbability(M, N , x-1, y, steps-1) * 0.25 + 
		   findProbability(M, N , x, y-1, steps-1) * 0.25 +
		   findProbability(M, N , x+1, y, steps-1) * 0.25 +
		   findProbability(M, N , x, y+1, steps-1) * 0.25;
}

int main(void)
{
	int M = 5, N = 5;
	int i = 1, j = 1;
	int steps = 2;
	cout<<findProbability(M, N, i, j, steps);
	return 0;
}