#include<bits/stdc++.h>
using namespace std;

int GetMinDiceMoves(int *moves,int N)
{
	int visited[N];
	memset(visited,0,sizeof(visited));
	queue<pair<int,int> > Q;
	visited[0] = 1;
	Q.push(make_pair(0,0));
	pair<int,int> P;
	while(!Q.empty())
	{
		P = Q.front();
		Q.pop();
		visited[P.second] = true;
		if(P.second == N-1)
			break;
		for(int j = P.second+1;j <= P.second+6 && j < N;j++)
		{
			if(!visited[j])
			{
				pair<int,int> temp;
				if(moves[j] != -1)
					temp.second = moves[j];
				else
					temp.second = j;
				Q.push(make_pair(P.first+1,temp.second));
			}
		}
	}
	return P.first;
}

int main(void)
{
	int N = 30;
	int moves[N];
	for(int i=0;i<N;i++)
		moves[i] = -1;
	moves[2] = 21;
    moves[4] = 7;
    moves[10] = 25;
    moves[19] = 28;
    moves[26] = 0;
    moves[20] = 8;
    moves[16] = 3;
    moves[18] = 6;
	cout<<GetMinDiceMoves(moves,N);
	return 0;
}