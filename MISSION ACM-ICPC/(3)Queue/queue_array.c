#include<stdio.h>

#define MAX 100
typedef struct Queue
{
	int *elements;
	int rear;
	int front;
	int size;
}Queue;

#define MAX 100

Queue *CreateQueue()
{
	Queue *Q = (Queue *)malloc(sizeof(Queue));
	Q->elements = (int *)malloc(sizeof(int) * MAX);
	Q->front = 0;
	Q->size = 0;
	Q->rear = MAX-1;
	return Q;
}

int isFull(Queue *Q)
{
	if(Q->size == MAX)
		return 1;
	else
		return 0;
}

int isEmpty(Queue *Q)
{
	if(Q->size == 0)
		return 1;
	else
		return 0;
}

void Enqueue(Queue *Q,int data)
{
	if(isFull(Q))
		return;
	else
	{
		Q->rear = (Q->rear+1)%MAX;
		Q->size++;
		Q->elements[Q->rear] = data;
	}
}

int Dequeue(Queue *Q)
{
	if(isEmpty(Q))
		return INT_MIN;
	int item = Q->elements[Q->front];
	Q->front = (Q->front+1)%MAX;
	Q->size--;
	return item;
}

int front(Queue *Q)
{
	return Q->elements[Q->front];
}

int main(void)
{
    Queue *Q = CreateQueue();
    int data,ch;
    label:
        printf("\n1.Enqueue");
        printf("\n2.Dequeue");
        printf("\n3.Print Front");
        printf("\n4.Exit");

    printf("\nEnter your choice:");
    scanf("%d",&ch);
    switch(ch)
    {
        case 1:  printf("\nEnter the element to be Enqueued :");
                 scanf("%d",&data);
                 Enqueue(Q,data);
                 break;
        case 2:  printf("%d\n",Dequeue(Q));
                 break;
        case 3:  printf("\nTop element is : %d",front(Q));
                 break;
        case 4:  exit(0);
                 break;
    }
    goto label;
    return 0;
}
