#include<bits/stdc++.h>
using namespace std;

int main(void)
{
    int N;
    scanf("%d",&N);
    queue<string> Q;
    Q.push("1");
    while(N--)
    {
        string S1 = Q.front();
        Q.pop();
        cout<<S1<<endl;
        Q.push(S1 + "0");
        Q.push(S1 + "1");
    }
    return 0;
}
