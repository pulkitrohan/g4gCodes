#include<cstdio>
#include<queue>
using namespace std;
int main(void)
{
        queue<int> Q;
        int sum = 0,max,N,K,i;
        printf("Enter the size of array : ");
        scanf("%d",&N);
        int A[N+1];
        printf("Enter the Elements : ");
        for(i=0;i<N;i++)
            scanf("%d",&A[i]);
        printf("Enter the Window Size : ");
        scanf("%d",&K);
        for(i=0;i<K;i++)
        {
            Q.push(A[i]);
            sum += A[i];
        }
        max = sum;
        for(i=K;i<N;i++)
        {
            sum += A[i] - Q.front();
            Q.pop();
            Q.push(A[i]);
            if(sum > max)
                max = sum;
        }
        printf("\nMaximum Sum Sub-array : %d",max);

    return 0;
}
