#include<stdio.h>
#include<stdlib.h>

typedef struct queue
{
    int data;
    struct queue *next;
}queue;

queue *rear = NULL,*front = NULL;


void Enqueue(int element)
{
    queue * temp = (queue *)malloc(sizeof(queue));
    temp->data = element;
    temp->next = NULL;
    if(rear == NULL)
        {
            rear = front = temp;
            rear->next = rear;
        }
    else
    {
        temp->next = rear->next;
        rear->next = temp;
        rear = temp;
    }
}

void Dequeue()
{
    if(front == NULL)
        {
            printf("\nQueue is Empty\n");
        }
    else
        {
            printf("\nThe element removed from the queue is : %d\n",front->data);
        queue *temp=front;
        rear->next = front->next;
        front=front->next;
        free(temp);
        }
}


int frontprint()
{
    if(!front)
    {
        printf("\nQueue is Empty\n");
        return ;
    }
    return front->data;
}

void print()
{
    if(front->next == NULL)
        printf("Empty");
    queue *temp = front;
    printf("\nQueue is : ");
    while(temp->next != front)
    {
        printf("%d ",temp->data);
        temp = temp->next;
    }
    printf("%d",temp->data);
}
int main(void)
{
    int n,ch,data;

        printf("1.Enqueue");
        printf("\n2.Dequeue");
        printf("\n3.Print Front");
        printf("\n4.Print");
        printf("\n5.Exit");
    label:
    printf("\nEnter your choice:");
    scanf("%d",&ch);
    switch(ch)
    {
        case 1:  printf("\nEnter the element to be Enqueued :");
                 scanf("%d",&data);
                 Enqueue(data);
                 break;
        case 2:  Dequeue();
                 break;
        case 3:  printf("\nTop element is : %d\n",frontprint());
                 break;
        case 4: print();
                break;
        case 5:  exit(0);
                 break;
    }
    goto label;
        return 0;
}
