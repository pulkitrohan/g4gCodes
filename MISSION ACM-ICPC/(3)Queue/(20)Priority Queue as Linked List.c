#include<stdio.h>
#include<stdlib.h>

typedef struct queue
{
    int data;
    struct queue *next;
    int priority;
}queue;

queue *rear = NULL,*front = NULL;


void Enqueue(int element,int pri)
{
    queue * temp = (queue *)malloc(sizeof(queue));
    temp->data = element;
    temp->next = NULL;
    temp->priority = pri;
    if(rear == NULL)
        rear = front = temp;
    else
    {
        queue *new = front;
        rear->next = temp;
        rear = temp;
        while(new != rear)
        {
            if(new->priority <= rear->priority)
                new = new->next;
            else
            {
                int data1,data2;
                data1 = new->data;
                new->data = rear->data;
                rear->data = data1;
                data2 = new->priority;
                new->priority = rear->priority;
                rear->priority = data2;
                new = new->next;
            }
        }
    }
}

void Dequeue()
{
    if(front == NULL)
        {
            printf("\nQueue is Empty\n");
        }
    else
        {
            printf("\nThe element of priority %d removed from the queue is : %d\n",front->priority,front->data);
        queue *temp=front;
        front=front->next;
        free(temp);
        }
}


int frontprint()
{
    if(!front)
    {
        printf("\nQueue is Empty\n");
        return ;
    }
    return front->data;
}

void print()
{
    if(front->next == NULL)
        printf("Empty");
    queue *temp = front;
    printf("\nQueue is : \n");
    printf("DATA\tPRIORITY\n");
    while(temp != rear)
    {
        printf("%d\t%d\n",temp->data,temp->priority);
        temp = temp->next;
    }
    printf("%d\t%d\n",temp->data,temp->priority);
}
int main(void)
{
    int n,ch,data,pri;

        printf("1.Enqueue");
        printf("\n2.Dequeue");
        printf("\n3.Print Front");
        printf("\n4.Print");
        printf("\n5.Exit");
    label:
    printf("\nEnter your choice:");
    scanf("%d",&ch);
    switch(ch)
    {
        case 1:  printf("\nEnter the element to be Enqueued : ");
                 scanf("%d",&data);
                 printf("Enter the Priority : ");
                 scanf("%d",&pri);
                 Enqueue(data,pri);
                 break;
        case 2:  Dequeue();
                 break;
        case 3:  printf("\nTop element is : %d\n",frontprint());
                 break;
        case 4: print();
                break;
        case 5:  exit(0);
                 break;
    }
    goto label;
        return 0;
}
