#include<cstdio>
#include<utility>
#include<deque>

using namespace std;
int main(void)
{
    int N,K,i;
    scanf("%d %d",&N,&K);
    int A[N+1];
    for(i=0;i<N;i++)
        scanf("%d",&A[i]);
    deque<pair<int,int> > Q;
    for(i=0;i<N;i++)
    {
        while(!Q.empty() && Q.back().first <= A[i])
            Q.pop_back();

    Q.push_back(make_pair(A[i],i));
    while(Q.front().second <= i-K)
            Q.pop_front();
    if(i+1 >= K)
        printf("%d ",Q.front().first);
    }
    return 0;
}
