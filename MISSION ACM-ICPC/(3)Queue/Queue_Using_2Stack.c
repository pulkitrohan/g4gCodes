#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
    int data;
    struct Node *next;
}node;

typedef struct Queue
{
    node *S1;
    node *S2;
}Queue;

void push(node *S,int data)   //Same as insertion at beginning
{
		node *temp;
        temp= (node *)malloc(sizeof(node));
        temp->next = S->next;   //S->next = NULL
        temp->data = data;
        S->next = temp;
}
int pop(node *S)
{
    if(S->next == NULL)
    {
        printf("\nUnderflow!!");
    }
    else
    {
        node *temp;
        temp = S->next;
        S->next = temp->next;
        return temp->data;
    }
}

void Enqueue(Queue *Q,int data)
{
    push(Q->S1,data);
}

int Dequeue(Queue *Q)
{
    if(Q->S2->next)
        return pop(Q->S2);
    else
    {
        while(Q->S1->next->next)    //N-1 Elements Popped
            push(Q->S2,pop(Q->S1));
        return pop(Q->S1);
    }
}

int main(void)
{
    Queue *Q = (Queue *)malloc(sizeof(Queue));
    Q->S1 = (node *)malloc(sizeof(node));
    Q->S1->next = NULL;
    Q->S2 = (node *)malloc(sizeof(node));
    Q->S2->next = NULL;
    Enqueue(Q,1);
    Enqueue(Q,2);
    Enqueue(Q,3);
    printf("%d Dequeued\n",Dequeue(Q));
    printf("%d Dequeued\n",Dequeue(Q));
    printf("%d Dequeued\n",Dequeue(Q));
    return 0;
}
