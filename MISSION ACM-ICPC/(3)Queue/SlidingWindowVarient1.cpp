#include<cstdio>
#include<queue>
using namespace std;
int main(void)
{
        queue<int> Q;
        int sum = 0,min=1000,N,K,i,size=0,S;
        printf("Enter the size of array : ");
        scanf("%d",&N);
        int A[N+1];
        printf("Enter the Elements : ");
        for(i=0;i<N;i++)
            scanf("%d",&A[i]);
        printf("Enter the Sum of Sub-Array : ");
        scanf("%d",&S);
        for(i=0;i<N;i++)
        {
            if(sum <= S)
            {
                Q.push(A[i]);
                sum += A[i];
                size++;
            }
            else
            {
                while(sum > S)
                {
                    sum -= Q.front();
                    Q.pop();
                    size--;
                }
            }
        }
        printf("Smallest Subarray Size : %d",size);

    return 0;
}
