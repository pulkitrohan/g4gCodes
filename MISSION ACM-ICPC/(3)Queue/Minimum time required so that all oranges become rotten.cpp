#include<bits/stdc++.h>
using namespace std;
#define R 3
#define C 5

bool isDelim(pair<int,int> P)
{
	return (P.first == -1 && P.second == -1);
}

bool isValid(int x,int y)
{
	return (x >= 0 && x < R && y >= 0 && y < C);
}

int canRot(int A[][C])
{
	queue<pair<int,int> > Q;
	pair<int,int> P;
	int ans = 0;
	for(int i=0;i<R;i++)
		for(int j=0;j<C;j++)
			if(A[i][j] == 2)
				Q.push(make_pair(i,j));
	Q.push(make_pair(-1,-1));
	while(!Q.empty())
	{
		bool flag = false;
		while(!isDelim(Q.front()))
		{
			P = Q.front();
			if(isValid(P.first+1,P.second) && A[P.first+1][P.second] == 1)
			{
				if(!flag)
				{	
					ans++;
					flag = true;
				}
				A[P.first+1][P.second] = 2;
				Q.push(make_pair(P.first+1,P.second));
			}
			if(isValid(P.first-1,P.second) && A[P.first-1][P.second] == 1)
			{
				if(!flag)
				{	
					ans++;
					flag = true;
				}
				A[P.first-1][P.second] = 2;
				Q.push(make_pair(P.first-1,P.second));
			}
			if(isValid(P.first,P.second+1) && A[P.first][P.second+1] == 1)
			{
				if(!flag)
				{	
					ans++;
					flag = true;
				}
				A[P.first][P.second+1] = 2;
				Q.push(make_pair(P.first,P.second+1));
			}
			if(isValid(P.first,P.second-1) && A[P.first][P.second-1] == 1)
			{
				if(!flag)
				{	
					ans++;
					flag = true;
				}
				A[P.first][P.second-1] = 2;
				Q.push(make_pair(P.first,P.second-1));
			}
			Q.pop();
		}
		Q.pop();
		if(!Q.empty())
			Q.push(make_pair(-1,-1));
	}
	for(int i=0;i<R;i++)
		for(int j=0;j<C;j++)
			if(A[i][j] == 1)
				return -1;
	return ans;
}
	

int main(void)
{
	int A[][C] = {{2,1,0,2,1},
				  {1,0,1,2,1},
				  {1,0,0,2,1}};
	int ans = canRot(A);
	if(ans == -1)
		cout<<"No\n";
	else
		cout<<"Time required for all oranges to rot : "<<ans<<endl;
	return 0;
}