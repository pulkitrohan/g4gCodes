#include<stdio.h>
#include<stdlib.h>
#define MAX 5
typedef struct Queue
{
	int *elements;
	int rear,front,size;
}Queue;


Queue *CreateQueue(int n)
{
    int i;
	Queue *Q = (Queue *)malloc(sizeof(Queue));
	Q->elements = (int *)malloc(sizeof(int)*n);
	Q->rear = Q->front = -1;
	Q->size = n;
	for(i=0;i<n;i++)
		Q->elements[i] = 0;
	return Q;
}

void Enqueue(Queue *Q,int data)
{
	if(Q->front == (Q->rear + 1)%Q->size)
		printf("FULL");
	else
	{
		if(Q->front == -1)
			Q->front = Q->rear = 0;
		else
			Q->rear = (Q->rear + 1)%Q->size;
		Q->elements[Q->rear] = data;
	}
}

int Dequeue(Queue *Q)
{
	if(Q->front == -1)
		printf("EMPTY");
	else
	{
		int data = Q->elements[Q->front];
		if(Q->rear == Q->front)
			Q->rear = Q->front = -1;
		else
			Q->front = (Q->front + 1)%Q->size;
	}
}

int front(Queue *Q)
{
    if(Q->front == -1)
        {
            printf("Queue is Empty");
            return -1;
        }
    else
    return Q->elements[Q->front];
}

int main(void)
{
    int n,ch,data;
    table:
        printf("Enter the size of Queue : ");
        scanf("%d",&n);
        if(n <= 0)
        {
            printf("Enter the size again..\n");
            goto table;
        }
     Queue *Q = CreateQueue(n);

    label:
        printf("\n1.Enqueue");
        printf("\n2.Dequeue");
        printf("\n3.Print Front");
        printf("\n4.Exit");

    printf("\nEnter your choice:");
    scanf("%d",&ch);
    switch(ch)
    {
        case 1:  printf("\nEnter the element to be Enqueued :");
                 scanf("%d",&data);
                 Enqueue(Q,data);
                 break;
        case 2:  Dequeue(Q);
                 break;
        case 3:  printf("\nTop element is : %d",front(Q));
                 break;
        case 4:  exit(0);
                 break;
    }
    goto label;
        return 0;
}
