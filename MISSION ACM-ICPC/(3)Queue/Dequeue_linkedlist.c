#include<stdio.h>
#include<stdlib.h>

typedef struct queue
{
    int data;
    struct queue *next;
    struct queue *prev;
}queue;

queue *rear = NULL,*front = NULL;



void EnqueueatRear(int element)
{
    queue * temp = (queue *)malloc(sizeof(queue));
    temp->data = element;
    temp->next = NULL;
    temp->prev = NULL;
    if(rear == NULL)
        rear = front = temp;
    else
    {
        rear->next = temp;
        temp->prev = rear;
        rear = temp;
    }
}
void Enqueueatfront(int element)
{
    queue * temp = (queue *)malloc(sizeof(queue));
    temp->data = element;
    temp->next = NULL;
    temp->prev = NULL;
    if(rear == NULL)
        rear = front = temp;
    else
    {
        front->prev = temp;
        temp->next = front;
        front = temp;
    }
}

void DequeueatFront()
{
    if(front == NULL)
        {
            printf("\nQueue is Empty\n");
        }
    else
        {
            printf("\nThe element removed from the queue is : %d\n",front->data);
        queue *temp=front;
        front=front->next;
        free(temp);
        }
}
void DequeueatRear()
{
    if(rear == NULL)
        {
            printf("\nQueue is Empty\n");
        }
    else
        {
            printf("\nThe element removed from the queue is : %d\n",rear->data);
        queue *temp=rear;
        rear = rear->prev;
        free(temp);
        }
}


int frontprint()
{
    if(!front)
    {
        printf("\nQueue is Empty\n");
        return ;
    }
    return front->data;
}

void print()
{
    if(front->next == NULL)
        printf("Empty");
    queue *temp = front;
    printf("\nQueue is : ");
    while(temp != rear)
    {
        printf("%d ",temp->data);
        temp = temp->next;
    }
    printf("%d\n",temp->data);  //To print rear's element
}
int main(void)
{
    int n,ch,data;

        printf("1.EnqueueatFront");
        printf("\n2.EnqueueatRear");
        printf("\n3.DequeueatFront");
        printf("\n4.DequeueatRear");
        printf("\n5.Print Front");
        printf("\n6.Print");
        printf("\n7.Exit");
    label:
    printf("\nEnter your choice:");
    scanf("%d",&ch);
    switch(ch)
    {
        case 1:  printf("\nEnter the element to be Enqueued :");
                 scanf("%d",&data);
                 Enqueueatfront(data);
                 break;
        case 2:  printf("\nEnter the element to be Enqueued :");
                 scanf("%d",&data);
                 EnqueueatRear(data);
                 break;
        case 3:  DequeueatFront();
                 break;
        case 4:  DequeueatRear();
                 break;
        case 5:  printf("\nTop element is : %d\n",frontprint());
                 break;
        case 6: print();
                break;
        case 7:  exit(0);
                 break;
    }
    goto label;
        return 0;
}
