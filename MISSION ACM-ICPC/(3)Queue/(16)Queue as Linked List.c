#include<stdio.h>
#include<stdlib.h>

struct node
{
    int data;
    struct node *next;
};

struct Queue
{
	struct node *rear,*front;
};

struct node *CreateNode(int data)
{
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp->data = data;
	temp->next = NULL;
	return temp;
}

struct Queue *CreateQueue()
{
	struct Queue *Q = (struct Queue *)malloc(sizeof(struct Queue));
	Q->rear = Q->front = NULL;
	return Q;
}

void Enqueue(struct Queue *Q,int data)
{
	struct node *temp = CreateNode(data);
	if(!Q->rear)
		Q->front = Q->rear = temp;
	else
	{
		Q->rear->next = temp;
		Q->rear = temp;
	}
}

int Deque(struct Queue *Q)
{
	if(!Q->front)
		return INT_MIN;
	struct node *temp = Q->front;
	if(Q->front == Q->rear)
		Q->front = Q->rear = NULL;
	else
		Q->front = Q->front->next;
	return temp->data;
}
	
int PrintFront(struct Queue *Q)
{
	if(!Q->front)
		return INT_MIN;
	else
		return Q->front->data;
}

int main(void)
{
	struct Queue *Q = CreateQueue();
	
	int n,ch,data;

        printf("1.Enqueue");
        printf("\n2.Dequeue");
        printf("\n3.Print Front");
        printf("\n4.Exit");
    label:
    printf("\nEnter your choice:");
    scanf("%d",&ch);
    switch(ch)
    {
        case 1:  printf("\nEnter the element to be Enqueued :");
                 scanf("%d",&data);
                 Enqueue(Q,data);
                 break;
        case 2:  printf("%d\n",Dequeue(Q));
                 break;
        case 3:  printf("\nTop element is : %d\n",PrintFront(Q));
                 break;
        case 4:  exit(0);
                 break;
    }
    goto label;
	return 0;
}
	