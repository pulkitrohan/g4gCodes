#include<bits/stdc++.h>
using namespace std;

class BTreeNode
{
	int *keys;
	int t;
	BTreeNode ** C;
	int N;
	bool leaf;

public:
	BTreeNode(int _t,bool _leaf);
	void insertNonFull(int k);
	void splitChild(int i,BTreeNode *y);
	void traverse();
	BTreeNode *search(int k);

	friend class BTree;
};

class BTree
{
	BTreeNode *root;
	int t;
public:

	BTree(int _t)
	{
		root = NULL;
		t = _t;
	}

	void traverse()
	{
		if(root)
			root->traverse();
	}

	BTreeNode *search(int k)
	{
		if(!root)
			return NULL;
		return root->search(k);
	}

	void insert(int k);
};

BTreeNode::BTreeNode(int t1,bool leaf1)
{
	t = t1;
	leaf = leaf1;
	keys = new int[2*t-1];
	C = new BTreeNode *[2*t];
	N = 0;
}

void BTreeNode::traverse()
{
	for(int i=0;i<N;i++)
	{
		if(!leaf)
			C[i]->traverse();
		cout<<" "<<keys[i];
	}
	if(!leaf)
		C[i]->traverse();
}

BTreeNode *BTreeNode::search(int k)
{
	int i=0;
	while(i < N && k > keys[i])
		i++;
	if(keys[i] == k)
		return this;
	if(leaf)
		return NULL;
	return C[i]->search(k);
}

void BTree::insert(int k)
{
	if(!root)
	{
		root = new BTreeNode(t,true);
		root->keys[0] = k;
		root->n = 1;
	}
	else
	{
		if(root->n == 2*t-1)
		{
			BTreeNode *s = new BTreeNode(t,false);
			s->C[0] = root;
			s->splitChild(0,root);
			int i = 0;
			if(s->keys[0] < k)
				i++;
			s->C[i]->insertNonFull(k);
			root = s;
		}
		else
			root->insertNonFull(k);
	}
}

void BTreeNode::insertNonFull(int k)
{
	int i=N-1;
	if(leaf)
	{
		while(i >= 0 && keys[i] > k)
		{
			keys[i+1] = keys[i];
			i--;
		}
		keys[i+1] = k;
		n++;
	}
	else
	{
		while(i >= 0 && keys[i] > k)
			i--;
		if(C[i+1]->n == 2*t-1)
		{
			splitChild(i+1,C[i+1]);
			if(keys[i+1] < k)
				i++;
		}
		C[i+1]->insertNonFull(k);
	}
}

void BTreeNode:splitChild(int i,BTreeNode *y)
{
	BTreeNode *z = new BTreeNode(y->t,y->leaf);
	z->N = t-1;

	for(int j=0;j<t-1;j++)
		z->keys[j] = y->keys[j+t];
	if(!y->leaf)
	{
		for(int j=0;j<t;j++)
			z->C[j] = y->C[j+t];
	}
	y->N = t-1;
	for(int j=N;j>=i+1;j--)
		C[j+1] = C[j];
	C[j+1] = z;
	for(int j=N-1;j>=i;j--)
		keys[j+1] = keys[j];
	keys[i] = y->keys[t-1];
	N++;
}

int main(void)
{
	BTree t(3);
	t.insert(10);
    t.insert(20);
    t.insert(5);
    t.insert(6);
    t.insert(12);
    t.insert(30);
    t.insert(7);
    t.insert(17);
    cout<<"Traversal of the constructed tree is : ";
    t.traverse();
    int k = 6;
    if(t.search(k))
    	cout<<"\nPresent";
    else
    	cout<<"\nNot Present";
    k = 15;
    if(t.serach(k))
    	cout<<"\nPresent";
    else
    	cout<<"\nNot Present";
    return 0;
}