#include<stdio.h>
#include<stdlib.h>

struct node
{
    int data;
    struct node *next;
};


void Insert(struct node *head,int data)
{
    struct node *temp = (struct node *)malloc(sizeof(struct node));
    temp->data = data;
    temp->next = head->next;
    head->next = temp;
}


void print(struct node *head)
{
    while(head->next)
    {
        head = head->next;
        printf("%d ",head->data);
    }
    printf("\n");
}

void Segregate(struct node *head)
{
    struct node *tail = head;
    while(tail->next)
        tail = tail->next;
    struct node *tail1 = tail;
	int count = 0;
    while(head->next != tail)
    {
        struct node *temp = head->next;
		count++;
        if(count %2 == 0)
        {
            head->next = temp->next;
            temp->next = tail->next;
            tail->next = temp;
        }
        else
            head = head->next;
    }
}

int main(void)
{
    struct node *head = (struct node *)malloc(sizeof(struct node));
    head->next = NULL;
	for(int i=20;i>=12;i-=2)
		Insert(head,i);
    print(head);
    Segregate(head);
    print(head);
    return 0;
}
