#include<stdio.h>
#include<stdlib.h>
typedef struct node
{
    int data;
    struct node *next;
}node;


void insertatend(node *pointer,int data)
{
    while(pointer->next != NULL)
        pointer = pointer->next;
        pointer->next = (node *)malloc(sizeof(node));
        pointer = pointer->next;
        pointer->data = data;
        pointer->next = NULL;
}

node *IntersectionLL(node *pointer1,node *pointer2)
{
    node *head = (node *)malloc(sizeof(node));
    head->next = NULL;
    while(pointer1->next && pointer2->next)
    {
        if(pointer1->next->data > pointer2->next->data)
            pointer2 = pointer2->next;
        else if(pointer1->next->data < pointer2->next->data)
            pointer1 = pointer1->next;
        else
        {
            insertatend(head,pointer1->next->data);
            pointer1 = pointer1->next;
            pointer2 = pointer2->next;
        }
    }
    return head;
}

void Print(node *head)
{
    while(head->next)
    {
        head = head->next;
        printf("%d ",head->data);
    }
}

int main(void)
{
    node *pointer1 = (node *)malloc(sizeof(node));
    pointer1->next = NULL;
    node *pointer2 = (node *)malloc(sizeof(node));
    pointer2->next = NULL;
    int i;
    for(i=1;i<=6;i++)
        insertatend(pointer1,i);
    for(i=2;i<=8;i+=2)
        insertatend(pointer2,i);
    node *head = IntersectionLL(pointer1,pointer2);
    Print(head);
    return 0;
}
