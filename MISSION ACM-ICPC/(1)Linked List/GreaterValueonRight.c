#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
    int data;
    struct Node *next;
}node;


void insertatend(node *pointer,int data)
{
    while(pointer->next != NULL)
        pointer = pointer->next;
        pointer->next = (node *)malloc(sizeof(node));
        pointer = pointer->next;
        pointer->data = data;
        pointer->next = NULL;
}
void reverse(node *pointer)
{
	//Non Recursive
	node *q = pointer->next;
	node *r = NULL,*s;
	while(q)
	{
		s = r;
		r = q;
		q = q->next;
		r->next = s;
	}
	pointer->next = r;


	/*Recursive
    if(!pointer->next)
        return NULL;
    else
    {
        reverse(pointer->next);
        printf("%d ",pointer->next->data);
    }
    */
}
void printandcount(node *pointer)
{
    int count=0;
    printf("\nThe list is : ");
    pointer = pointer->next;
    while(pointer != NULL)
    {
        printf("%d ",pointer->data);
        pointer = pointer->next;
        count++;
    }
    printf("\nTotal elements are : %d",count);
}

void DeleteGreaterRight(node *pointer)
{
    int max = 0;
    while(pointer->next)
    {
        if(pointer->next->data < max)
        {
			node *temp = pointer->next;
			pointer->next = temp->next;
			free(temp);
		}
        else
        {
            max = pointer->next->data;
            pointer = pointer->next;
        }
    }
}

int main(void)
{
    node *head = (node *)malloc(sizeof(node));
    head->next = NULL;
    int A[] = {60,50,40,30,20,10};
    int i;
    for(i=0;i<6;i++)
        insertatend(head,A[i]);
    printandcount(head);
    reverse(head);
    printandcount(head);
    DeleteGreaterRight(head);
    reverse(head);
    printandcount(head);
    return 0;
}
