#include<stdio.h>
#include<stdlib.h>
struct node
{
    int data;
    struct node *next;
    struct node *prev;
};

void Insert(struct node *&head,int data)
{
    struct node *temp = (struct node *)malloc(sizeof(struct node));
    temp->data = data;
	temp->next = temp->prev = NULL;
	if(!head)
		head = temp;
	else
	{
		temp->next = head;
		head->prev = temp;
		head = temp;
	}
}

void Print(struct node *head)
{
    while(head)
    {
        printf("%d ",head->data);
        head = head->next;
    }
    printf("\n");
}


void Swap(int *A,int *B)
{
    int temp = *A;
    *A = *B;
    *B = temp;
}

//Split into 2 LL
struct node *split(struct node *head)
{
	struct node *fast = head,*slow = head;
	while(fast->next && fast->next->next)
	{
		fast = fast->next->next;
		slow = slow->next;
	}
	struct node *temp = slow->next;
	slow->next = NULL;
	temp->prev = NULL;
	return temp;
}


struct node *Merge(struct node *first,struct node *second)
{
	if(!first)
		return second;
	if(!second)
		return first;
	struct node *result = NULL;
	if(first->data < second->data)
	{
		result = first;
		result->next = Merge(first->next,second);
		result->next->prev = first;
		result->prev = NULL;
	}
	else
	{
		result = second;
		result->next = Merge(first,second->next);
		result->next->prev = second;
		result->prev = NULL;
	}
	return result;
}

struct node *MergeSort(struct node *head)
{
	if(!head || !head->next)
		return head;
	struct node *second = split(head);
	head = MergeSort(head);
	second = MergeSort(second);
	return Merge(head,second);
}



int main(void)
{
    struct node *head = NULL;
    Insert(head,5);
    Insert(head,20);
    Insert(head,4);
    Insert(head,3);
    Insert(head,30);
    Insert(head,10);
    Print(head);
    head = MergeSort(head);
    Print(head);
    return 0;
}
