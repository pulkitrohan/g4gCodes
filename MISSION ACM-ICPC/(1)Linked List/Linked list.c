#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
    int data;
    struct Node *next;
}node;

void insertatbeg(node *pointer,int data)
{
		node *temp;
        temp= (node *)malloc(sizeof(node));
        temp->next = pointer->next;
        temp->data = data;
        pointer->next = temp;
}

void insertatmid(node *pointer,int data,int pos)
{
    int count=1;
    while(count != pos)
    {
        pointer = pointer->next;
        count++;
    }
    node *temp;
    temp=(node *)malloc(sizeof(node));
    temp->next=pointer->next;
    temp->data=data;
    pointer->next=temp;
}

void insertatend(node *pointer,int data)
{
    while(pointer->next != NULL)
        pointer = pointer->next;
        pointer->next = (node *)malloc(sizeof(node));
        pointer = pointer->next;
        pointer->data = data;
        pointer->next = NULL;
}
void reverse(node *pointer)
{
	//Non Recursive
	node *prev = NULL;
	node *current=pointer->next,*next;
	while(current)
    {
        next = current->next;
        current->next = prev;
        prev = current;
        current = next;
    }
    pointer->next = prev;

	/*Recursive
    if(!pointer->next)
        return NULL;
    else
    {
        reverse(pointer->next);
        printf("%d ",pointer->next->data);
    }
    */

}

void sorting(node *p) // 3 nodes
{
    int i,j,temp;
    node *q;
    p = p->next;
    for(i=0;i<3;i++)
    {
        q = p->next;
        for(j = i+1;j<3;j++)
        {
            if(p->data > q->data)
            {
                temp = p->data;
                p->data = q->data;
                q->data = temp;
            }
            q = q->next;
        }
        p = p->next;
    }
}



void find(node *pointer,int data)
{
    while(pointer->next != NULL && (pointer->next)->data != data)
        pointer = pointer->next;
    if(pointer->next == NULL)
    {
        printf("\nElement not found");
    }
    else if((pointer->next)->data == data)
    {
        printf("\nElement found");
    }
}

void delete(node *pointer,int data)
{
    while(pointer->next != NULL && (pointer->next)->data != data)
        pointer = pointer->next;
    if(pointer->next == NULL)
    {
        printf("\nElement not found");
    }
    else
    {
        node *temp;
        temp = pointer->next;
        pointer->next = temp->next;
        free(temp);
        printf("\nElement deleted");
    }
}
void printandcount(node *pointer)
{
    int count=0;
    printf("\nThe list is : ");
    pointer = pointer->next;
    while(pointer != NULL)
    {
        printf("%d ",pointer->data);
        pointer = pointer->next;
        count++;
    }
    printf("\nTotal elements are : %d",count);
}

int max(node *pointer)
{
    int max=0;
    pointer = pointer->next;
    while(pointer != NULL)
    {
        if(pointer->data > max)
            max = pointer->data;
        pointer = pointer->next;
    }
    return max;
}

int main(void)
{
    node *start,*temp,*rlist;
    start = (node *)malloc(sizeof(node));
    start->next = NULL;

    int ch,data,pos;
label:
    printf("\n1.Insert");
    printf("\n2.Delete");
    printf("\n3.Print & Count");
    printf("\n4.Find");
    printf("\n5.Max. Value");
    printf("\n6.Reverse");
    printf("\n7.Sorting");
    printf("\nEnter your choice:");
    scanf("%d",&ch);
    switch(ch)
    {
        case 1:  printf("\nEnter the element to be inserted :");
                 scanf("%d",&data);
                 printf("\n1.Insert at beg");
                 printf("\n2.Inset at middle");
                 printf("\n3.Insert at end");
                 printf("\nEnter your choice:");
                 scanf("%d",&ch);
                 switch(ch)
                 {
                 case 1: insertatbeg(start,data);
                         break;
                 case 2: printf("\nEnter the position where element is to be added:");
                         scanf("%d",&pos);
                         insertatmid(start,data,pos);
                         break;
                 case 3: insertatend(start,data);
                         break;
                 }
                 break;

        case 2:  printf("\nEnter the element to be deleted :");
                 scanf("%d",&data);
                 delete(start,data);
                 break;
        case 3:  printandcount(start);
                 break;
        case 4:  printf("\nEnter the element to be find :");
                 scanf("%d",&data);
                 find(start,data);
                 break;
        case 5:  printf("\nMax. Value is %d ",max(start));
                 break;
        case 6:  reverse(start);
                 printandcount(start);
                 break;
        case 7:  sorting(start);
                 printandcount(start);
                 break;
    }
    goto label;
    return 0;
}
