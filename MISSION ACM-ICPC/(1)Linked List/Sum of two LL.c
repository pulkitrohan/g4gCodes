#include<stdio.h>
#include<stdlib.h>
//#define size(A) (sizeof(A)/sizeof(A[0])

struct node
{
    int data;
    struct node *next;
};

struct node *CreateList(int *A,int N)
{
    int i;
    struct node *head = (struct node *)malloc(sizeof(struct node));
    head->next = NULL;
    for(i=N-1;i>=0;i--)
    {
        struct node *temp = (struct node *)malloc(sizeof(struct node));
        temp->data = A[i];
        temp->next = head->next;
        head->next = temp;
    }
    return head;
}

void Reverse(struct node *head)
{
    struct node *current,*prev=NULL,*next;
    current = head->next;
    while(current)
    {
        next = current->next;
        current->next = prev;
        prev = current;
        current = next;
    }
    head->next = prev;
}

void Print(struct node *head)
{
    while(head->next)
    {
         head = head->next;
        printf("%d",head->data);
    }
    printf("\n");
}

int Size(struct node *head)
{
    int count = 0;
    while(head)
    {
        head = head->next;
        count++;
    }
    return count;
}

void Insert(struct node *head,int data)
{
    struct node *temp = (struct node *)malloc(sizeof(struct node));
    temp->data = data;
    while(head->next)
        head = head->next;
    temp->next = head->next;
    head->next = temp;
}

struct node *createNode(int data)
{
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp->data = data;
	temp->next = NULL;
	return temp;
}
	
struct node *AddList(struct node *head1,struct node *head2)
{
    if(!head1)
        return head2;
    else if(!head2)
        return head1;
    Reverse(head1);
    Reverse(head2);
    Print(head1);
    Print(head2);
    head1 = head1->next;
    head2 = head2->next;
    int sum ,carry=0;
    struct node *head3 = (struct node *)malloc(sizeof(struct node));
    head3->next = NULL;
    while(head1 || head2 || carry)
    {
        sum = carry;
        if(head1)
        {
            sum += head1->data;
            head1 = head1->next;
        }
        if(head2)
        {
            sum += head2->data;
            head2 = head2->next;
        }
        carry = sum/10;
        sum = sum % 10;
        struct LL *temp = createNode(sum);
		if(!head3->next)
			head3->next = temp;
		else
			prev->next = temp;
		prev = temp;
    }
    Print(head3);
    Reverse(head3);
    return head3;
}


int main(void)
{
    struct node *head1 = NULL;
    struct node *head2 = NULL,*head3 = NULL;
    int A[] = {5,6,3};
    int B[] = {8,4,2};
    head1 = CreateList(A,sizeof(A)/sizeof(A[0]));
    head2 = CreateList(B,sizeof(B)/sizeof(B[0]));
    //head3 = AddList(head1,head2);
    Print(head1);
    Print(head2);
    head3 = AddList(head1,head2);
    Print(head3);
    return 0;
}
