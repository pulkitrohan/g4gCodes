#include<bits/stdc++.h>
using namespace std;

struct node
{
	int data;
	struct node *next;
};

void push(struct node** head_ref, int new_data)
{
    /* allocate node */
    struct node* new_node = (struct node*) malloc(sizeof(struct node));
 
    /* put in the data  */
    new_node->data  = new_data;
 
    /* link the old list off the new node */
    new_node->next = (*head_ref);
 
    /* move the head to point to the new node */
    (*head_ref)    = new_node;
}
 
/* A utility function to print linked list */
void print(struct node *node)
{
    while (node != NULL)
    {
        printf("%d  ", node->data);
        node = node->next;
    }
    printf("\n");
}
 
// A utility function to swap two pointers
void swapPointer( node** a, node** b )
{
    node* t = *a;
    *a = *b;
    *b = t;
}
 
/* A utility function to get size of linked list */
int getSize(struct node *node)
{
    int size = 0;
    while (node != NULL)
    {
        node = node->next;
        size++;
    }
    return size;
}

node *addSameSize(node *A,node *B,int &carry)
{
	if(!A)
		return NULL;
	int sum = 0;
	node *result = (node *)malloc(sizeof(node));
	result->next = addSameSize(A->next,B->next,carry);
	sum = A->data + B->data + carry;
	carry = sum/10;
	sum = sum%10;
	result->data = sum;
	return result;
}

void addCarryToRemaining(node *A,node *cur,int &carry,node **result)
{
	int sum = 0;
	if(A != cur)
	{
		addCarryToRemaining(A->next,cur,carry,result);
		sum = A->data + carry;
		carry = sum/10;
		sum = sum%10;
		push(result,sum);
	}
}

void AddList(node *A,node *B,node **R)
{
	if(!A)
		*R = A;
	else if(!B)
		*R = B;
	else
	{
		int N = getSize(A);
		int M = getSize(B);
		int carry = 0;
		node *cur;
		if(N == M)
			*R = addSameSize(A,B,carry);
		else
		{
			int diff = abs(N-M);
			if(N < M)
				swapPointer(&A,&B);
			cur = A;
			while(diff--)
				cur = cur->next;
			*R = addSameSize(cur,B,carry);
			addCarryToRemaining(A,cur,carry,R);
		}
		if(carry)
			push(R,carry);
	}
}
		

int main(void)
{
	node *head1 = NULL,*head2 = NULL,*result = NULL;
	int A[] = {9,9,9};
	int B[] = {1,8};
	int N = sizeof(A)/sizeof(A[0]);
	int M = sizeof(B)/sizeof(B[0]);
	for(int i=N-1;i>=0;i--)
		push(&head1,A[i]);
	for(int i=M-1;i>=0;i--)
		push(&head2,B[i]);
	AddList(head1,head2,&result);
	print(result);
	return 0;
}