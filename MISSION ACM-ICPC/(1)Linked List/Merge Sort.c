#include<stdio.h>
#include<stdlib.h>
struct LL
{
    int data;
    struct LL *next;
};

struct LL *Insert(struct LL *head,int data)
{
    struct LL *temp = (struct LL *)malloc(sizeof(struct LL));
    temp->data = data;
    temp->next = NULL;
    if(!head)
        head = temp;
    else
    {
        temp->next = head;
        head = temp;
    }
    return head;
}

void Print(struct LL *head)
{
    while(head)
    {
        printf("%d ",head->data);
        head = head->next;
    }
    printf("\n");
}
struct LL *Merge(struct LL *a,struct LL *b)
{
    if(!a)
        return b;
    else if(!b)
        return a;
    struct LL *result = NULL;
    if(a->data <= b->data)
    {
        result = a;
        result->next = Merge(a->next,b);
    }
    else
    {
        result = b;
        result->next = Merge(a,b->next);
    }
    return result;
}

struct LL * split(struct LL *head)
{
	if(!head)
		return NULL;
   struct LL *fast = head,*slow = head;
	while(fast->next && fast->next->next)
	{
		fast = fast->next->next;
		slow = slow->next;
	}
	struct LL *temp = slow->next;
	slow->next = NULL;
	return temp;
}

struct LL* MergeSort(struct LL *head)
{

    if(!head || !head->next)
        return head;
	struct LL *second = split(head);
	head = MergeSort(head);
	second = MergeSort(second);
    return Merge(head,second);
}




int main(void)
{
    struct LL *head = NULL;
    int i;
    for(i=1;i<=9;i++)
        head = Insert(head,i);
    Print(head);
    head = MergeSort(head);
    Print(head);
    return 0;
}
