#include<stdio.h>
#include<stdlib.h>
struct LL
{
	int data;
	struct LL *next;
};

struct LL *Insert(struct LL *head,int data)
{
	struct LL *temp = (struct LL *)malloc(sizeof(struct LL));
	temp->data = data;
	temp->next = NULL;
	if(head)
		temp->next = head;
	head = temp;
	return head;
}

void print(struct LL *head)
{
	while(head)
	{
		printf("%d ",head->data);
		head = head->next;
	}
	printf("\n");
}

void FinalMaxSumList(struct LL *head1,struct LL *head2)
{
	struct LL *result = NULL;
	struct LL *pre1 = head1,*cur1 = head1;
	struct LL *pre2 = head2,*cur2 = head2;
	
	while(cur1 || cur2)
	{
		int sum1 = 0,sum2 = 0;
		while(cur1 && cur2 && cur1->data != cur2->data)
		{
			if(cur1->data < cur2->data)
			{
				sum1 += cur1->data;
				cur1 = cur1->next;
			}
			else
			{
				sum2 += cur2->data;
				cur2 = cur2->next;
			}
		}
		
		if(!cur1)
		{
			while(cur2)
			{
				sum2 += cur2->data;
				cur2 = cur2->next;
			}
		}
		if(!cur2)
		{
			while(cur1)
			{
				sum1 += cur1->data;
				cur1 = cur1->next;
			}
		}
		if(pre1 == head1 && pre2 == head2)
		{
			if(sum1 > sum2)
				result = pre1;
			else
				result = pre2;
		}
		else
		{
			if(sum1 > sum2)
				pre2->next = pre1->next;
			else
				pre1->next = pre2->next;
		}
		pre1 = cur1,pre2 = cur2;
		if(cur1)
			cur1 = cur1->next;
		if(cur2)
			cur2 = cur2->next;
	}
	print(result);
}

int main(void)
{
	struct LL *head1 = NULL,*head2 = NULL;
	head1 = Insert(head1,120);
	head1 = Insert(head1,110);
	head1 = Insert(head1,90);
	head1 = Insert(head1,30);
	head1 = Insert(head1,3);
	head1 = Insert(head1,1);
	
	head2 = Insert(head2,130);
	head2 = Insert(head2,120);
	head2 = Insert(head2,100);
	head2 = Insert(head2,90);
	head2 = Insert(head2,32);
	head2 = Insert(head2,12);
	head2 = Insert(head2,3);
	head2 = Insert(head2,0);
	
	print(head1);
	print(head2);
	
	FinalMaxSumList(head1,head2);
	
	return 0;
}