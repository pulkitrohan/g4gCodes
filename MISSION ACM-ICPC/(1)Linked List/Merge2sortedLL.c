#include<stdio.h>
#include<stdlib.h>
typedef struct Node
{
    int data;
    struct Node *next;
}node;

void insertatend(node *pointer,int data)
{
    while(pointer->next != NULL)
        pointer = pointer->next;
        pointer->next = (node *)malloc(sizeof(node));
        pointer = pointer->next;
        pointer->data = data;
        pointer->next = NULL;
}

void printandcount(node *pointer)
{
    int count=0;
    printf("\nThe list is : ");
    //pointer = pointer->next;
    while(pointer != NULL)
    {
        printf("%d ",pointer->data);
        pointer = pointer->next;
        count++;
    }
    printf("\nTotal elements are : %d",count);
}

node *SortedList(node *a,node *b)
{
    node *temp = NULL;
    if(!a)
        return b;
    else if(!b)
        return a;
    else if(a->data <= b->data)
    {
        temp = a;
        temp->next = SortedList(a->next,b);
    }
    else
    {
        temp = b;
        temp->next = SortedList(a,b->next);
    }
    return temp;
}

int main(void)
{
    node *head1 = (node *)malloc(sizeof(node));
    head1->next = NULL;
    node *head2 = (node *)malloc(sizeof(node));
    head2->next = NULL;
    int i;
    for(i=1;i<=5; i += 2)
        insertatend(head1,i);
    for(i=2;i<=13;i += 2)
        insertatend(head2,i);
    printandcount(head1->next);
    printandcount(head2->next);
    printandcount(SortedList(head1->next,head2->next));
    return 0;
}

