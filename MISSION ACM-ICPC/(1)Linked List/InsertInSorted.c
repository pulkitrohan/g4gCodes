#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
    int data;
    struct Node *next;
}node;


void InsertInsorted(node *pointer,int data)
{
    while(pointer->next != NULL && pointer->next->data < data)
        pointer = pointer->next;
    node *temp = (node *)malloc(sizeof(node));
    temp->data = data;
    temp->next = pointer->next;
    pointer->next = temp;
}

void printandcount(node *pointer)
{
    if(!pointer)
        return;
    printandcount(pointer->next);
    printf("%d ",pointer->data);
}

int main(void)
{
    node *pointer = (node *)malloc(sizeof(node));
    pointer->next = NULL;
    int i;
    for(i=10;i>0;i--)
    InsertInsorted(pointer,i);
    printandcount(pointer->next);
    return 0;
}
