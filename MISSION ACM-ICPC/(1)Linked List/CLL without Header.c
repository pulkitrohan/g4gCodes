#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
    int data;
    struct Node *next;
}node;

node *insertatbeg(node *pointer,int data)
{
    if(!pointer)
    {
        pointer = (node *)malloc(sizeof(node));
        pointer->data = data;
        pointer->next = pointer;
    }
    else
    {
        node *temp = (node *)malloc(sizeof(node));
        temp->data = data;
        node *A = pointer;
        while(A != pointer)
            A = A->next;
        temp->next = pointer;
        A->next = temp;
        pointer = temp;
    }
    return pointer;
}

void insertatmid(node *pointer,int data,int pos)    //Same as Singly Linked List
{
    int count=0;
    while(count != pos - 1)
    {
        pointer = pointer->next;
        count++;
    }
    node *temp;
    temp=(node *)malloc(sizeof(node));
    temp->next=pointer->next;
    temp->data=data;
    pointer->next=temp;

}
void insertatend(node *pointer,int data)
{
    node *temp,*current;
    current = pointer;
    temp = (node *)malloc(sizeof(node));
    temp->data = data;
    temp->next = pointer;
    while(current->next != pointer)
        current = current->next;
        current->next = temp;
}

void find(node *pointer,int data)
{
    node *start = pointer;
    while(pointer->next != start && (pointer->next)->data != data)
        pointer = pointer->next;
    if(pointer->next == start)
    {
        printf("\nElement not found");
    }
    else if((pointer->next)->data == data)
    {
        printf("\nElement found");
    }
}

void delete(node *pointer,int data)
{
    node *start = pointer;
    while(pointer->next != start && (pointer->next)->data != data)
        pointer = pointer->next;
    if(pointer->next == start)
    {
        printf("\nElement not found");
    }
    else
    {
        node *temp;
        temp = pointer->next;
        pointer->next = temp->next;
        free(temp);
        printf("\nElement deleted");

    }
}
void print(node *start)     //(y)
{
    int count=0;
    node *pointer = start;
    printf("\nThe list is : ");
    while(pointer->next != start)
    {
        pointer = pointer->next;
        printf("%d ",pointer->data);
        count++;
    }
    printf("\nTotal elements are : %d",count);
}
int maxval(node *pointer)                 //(y)
{
    int max=0;
    node *start = pointer;
    pointer = pointer->next;
    while(pointer != start)
    {
        if(pointer->data > max)
            max = pointer->data;
        pointer = pointer->next;
    }
    return max;
}

int main(void)
{
    node *start,*temp;
    start = NULL;
    int ch,data,pos;

    printf("1.Insert");
    printf("\n2.Delete");
    printf("\n3.Print & Count");
    printf("\n4.Find");
    printf("\n5.Max. Value");

    label:
    printf("\nEnter your choice:");
    scanf("%d",&ch);
    switch(ch)
    {
        case 1:  printf("\nEnter the element to be inserted :");
                 scanf("%d",&data);
                 printf("\n1.Insert at beg");
                 printf("\n2.Inset at middle");
                 printf("\n3.Insert at end");
                 printf("\nEnter your choice:");
                 scanf("%d",&ch);
                 switch(ch)
                 {
                 case 1: start = insertatbeg(start,data);
                         break;
                 case 2: printf("\nEnter the position where element is to be added:");
                         scanf("%d",&pos);
                         insertatmid(start,data,pos);
                         break;
                 case 3: insertatend(start,data);
                         break;
                 }
                 break;

        case 2:  printf("\nEnter the element to be deleted :");
                 scanf("%d",&data);
                 delete(start,data);
                 break;
        case 3:  print(start);
                 break;
        case 4:  printf("\nEnter the element to be find :");
                 scanf("%d",&data);
                 find(start,data);
                 break;
        case 5:  printf("\nMax. Value is %d ",maxval(start));
                 break;
        case 6:  exit(0);
                 break;
    }
    goto label;
    return 0;
}
