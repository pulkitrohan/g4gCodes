#include<stdio.h>

typedef struct CLL
{
    int data;
    struct CLL *next;
}node;

void insertatbeg(node *head,int data)
{
    node *temp = (node *)malloc(sizeof(node));
    temp->data = data;
    if(head->next)
        temp->next = head->next;
    else
    {
        temp->next = head;
        head->next = temp;
    }
}

void insertatmid(node *head,int data,int pos)
{
    int count = 0;
    while(count != pos - 1)
    {
        head = head->next;
        count++;
    }
    node *temp = (node *)malloc(sizeof(node));
    temp->data = data;
    temp->next = head->next;
    head->next = temp;
}

void insertatend(node *head,int data)
{
    node *p=head;
    while(p->next != head)
        p = p->next;
    node *temp = (node *temp)malloc(sizeof(node));
    temp->data = data;
    temp->next = head;
    p->next = temp;
}

void delete(node *head,int data)
{
    node *p = head;
    while(p->next != head && p->next->data != data)
        p = p->next;
    if(p->next == head)
         printf("Element not found\n");
    else
    {
        node *temp = p->next;
        p->next = temp->next;
        free(temp);
    }
}



int main(void)
{
    node *head = (node *)malloc(sizeof(node));
    head->next = head;
}
