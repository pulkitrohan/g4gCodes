#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
    int data;
    struct Node *next;
}node;

void insertatend(node *pointer,int data)
{
    while(pointer->next != NULL)
        pointer = pointer->next;
        pointer->next = (node *)malloc(sizeof(node));
        pointer = pointer->next;
        pointer->data = data;
        pointer->next = NULL;
}

void SwapPairWise(node *pointer)
{
    while(pointer->next && pointer->next->next)
    {
        int temp = pointer->next->data;
        pointer->next->data = pointer->next->next->data;
        pointer->next->next->data = temp;
        pointer = pointer->next->next;
    }
}

void printandcount(node *pointer)
{
    int count=0;
    printf("\nThe list is : ");
    pointer = pointer->next;
    while(pointer != NULL)
    {
        printf("%d ",pointer->data);
        pointer = pointer->next;
        count++;
    }
    printf("\nTotal elements are : %d",count);
}

int main(void)
{
    node *pointer = (node *)malloc(sizeof(node));
    pointer->next = NULL;
    int i;
    for(i=1;i<=6;i++)
        insertatend(pointer,i);
    printandcount(pointer);
    SwapPairWise(pointer);
    printandcount(pointer);
    return 0;
}
