#include<stdio.h>
#include<stdlib.h>
//http://tech-queries.blogspot.in/2011/04/copy-linked-list-with-next-and-random.html
struct LL
{
    int data;
    struct LL *next;
    struct LL *arbit;
};

struct LL *Copy_list(struct LL *head)
{
    struct LL *cur = head,*temp;
    while(cur)
    {
        temp = (struct LL *)malloc(sizeof(struct LL));
        temp->data = cur->data;
        temp->arbit = NULL;
        temp->next = cur->next;
        cur->next = temp;
        cur = cur->next->next;
    }
    struct LL *res = head->next;
    cur = head;
    while(cur)
    {
        cur->next->arbit = cur->arbit->next;
        cur = cur->next->next;
    }
    cur = head;
    temp = head->next;
    while(cur && temp)
    {
        cur->next = cur->next->next;
        cur = cur->next;
        if(temp->next)
        {
            temp->next = temp->next->next;
            temp = temp->next;
        }
    }
    return res;
}

void PrintArbit(struct LL *head)
{

    int count = 5;
    while(head && count--)
    {
        printf("%d ",head->data);
        head = head->arbit;
    }
}

int main(void)
{
    struct LL *node1 = (struct LL *)malloc(sizeof(struct LL));
    struct LL *node2 = (struct LL *)malloc(sizeof(struct LL));
    struct LL *node3 = (struct LL *)malloc(sizeof(struct LL));
    struct LL *node4 = (struct LL *)malloc(sizeof(struct LL));
    struct LL *node5 = (struct LL *)malloc(sizeof(struct LL));
    node1->data = 1;
    node2->data = 2;
    node3->data = 3;
    node4->data = 4;
    node5->data = 5;
    node1->next = node2;
    node2->next = node3;
    node3->next = node4;
    node4->next = node5;
    node5->next = NULL;
    node1->arbit = node3;
    node2->arbit = node1;
    node3->arbit = node5;
    node4->arbit = node3;
    node5->arbit = node2;
    struct LL *head = Copy_list(node1);
    PrintArbit(head);
    return 0;
}
