#include<bits/stdc++.h>
using namespace std;

struct LL 
{
	int data;
	struct LL *next;
};

void print(LL *head)
{
	while(head)
	{
		cout<<head->data<<" ";
		head = head->next;
	}
	cout<<endl;
}

LL *Merge(LL *A,LL *B)
{
	if(!A)
		return B;
	if(!B)
		return A;
	LL *ans = NULL;
	if(A->data <= B->data)
	{
		ans = A;
		ans->next = Merge(A->next,B);
	}
	else
	{
		ans = B;
		ans->next = Merge(A,B->next);
	}
	return ans;
}

LL *reverse(LL *head)
{
	LL *cur = head,*prev = NULL,*next;
	while(cur)
	{
		next = cur->next;
		cur->next = prev;
		prev = cur;
		cur = next;
	}
	return prev;
}



LL *sortList(LL *head)
{
	if(!head)
		return NULL;
	LL *head2 = NULL;
	LL *cur = head,*temp;
	while(cur && cur->next)
	{
		temp = cur->next;
		cur->next = cur->next->next;
		temp->next = head2;
		head2 = temp;
		cur = cur->next;
	}
	print(head);
	head2 = reverse(head2);
	print(head2);
	return Merge(head,head2);
}

void insert(LL * &head,int val)
{
	LL *temp = (LL *)malloc(sizeof(LL));
	temp->data = val;
	temp->next = head;
	head = temp;
}

int main(void)
{
	struct LL *head = NULL;
	insert(head,89);
	insert(head,67);
	insert(head,53);
	insert(head,43);
	insert(head,30);
	insert(head,12);
	insert(head,10);
	head = sortList(head);
	print(head);
	return 0;
}