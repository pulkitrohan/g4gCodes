#include<bits/stdc++.h>
using namespace std;

struct Node
{
    int data;
    struct Node *next;
};

struct Node *Insert(struct Node *head,int data)
{
    struct Node *temp = (struct Node *)malloc(sizeof(struct Node));
    temp->data = data;
    temp->next = NULL;
    if(!head)
        head = temp;
    else
    {
        temp->next = head;
        head = temp;
    }
    return head;
}

void Print(struct Node *head)
{
    while(head)
    {
        printf("%d ",head->data);
        head = head->next;
    }
}

int IsPalindrome(struct Node * &left,struct Node *right)
{
    if(!right)
        return 1;
    int isp = IsPalindrome(left,right->next);
    if(!isp)
        return 0;
    int isp1 = (right->data == left->data);
    left = left->next;
    return isp1;
}

int main(void)
{
    struct Node *head = NULL;
    head = Insert(head,1);
    head = Insert(head,2);
    head = Insert(head,3);
    head = Insert(head,2);
    head = Insert(head,1);
    Print(head);
    if(IsPalindrome(head,head))
        printf("\nYes\n");
    else
        printf("NO\n");
    return 0;
}
