#include<bits/stdc++.h>
using namespace std;

struct LL 
{
	int data;
	struct LL *next;
};

void print(LL *head)
{
	while(head)
	{
		cout<<head->data<<" ";
		head = head->next;
	}
	cout<<endl;
}

LL *reverse(LL *head)
{
	LL *cur = head,*prev = NULL,*next;
	while(cur)
	{
		next = cur->next;
		cur->next = prev;
		prev = cur;
		cur = next;
	}
	return prev;
}

void insert(LL * &head,int val)
{
	LL *temp = (LL *)malloc(sizeof(LL));
	temp->data = val;
	temp->next = head;
	head = temp;
}

LL *MergeAlt(LL *&A,LL *B)
{
	LL *head =(LL *)malloc(sizeof(LL));
	LL *cur = head;
	while(A || B)
	{
		if(A)
		{
			cur->next = A;
			cur = cur->next;
			A = A->next;
		}
		if(B)
		{
			cur->next = B;
			cur = cur->next;
			B = B->next;
		}
	}
	return head->next;
}

LL *split(LL *head)
{
	if(!head)
		return NULL;
	if(!head->next)
		return head;
	LL *slow = head,*fast = head;
	while(fast->next && fast->next->next)
	{
		slow = head->next;
		fast = fast->next->next;
	}
	LL *temp = slow->next;
	slow->next = NULL;
	return temp;
}

LL *Rearrange(LL *head)
{
	if(!head)
		return NULL;
	LL *head2 = split(head);
	print(head);
	print(head2);
	head = MergeAlt(head,head2);
	return head;
}

int main(void)
{
	LL *head = NULL;
	insert(head,5);
	insert(head,4);
	insert(head,3);
	insert(head,2);
	insert(head,1);
	print(head);
	head = Rearrange(head);
	print(head);
	return 0;
}
