#include<stdio.h>
#include<stdlib.h>

struct LL
{
    int data;
    struct LL *next;
    struct LL *down;
};

struct LL *Insert(struct LL *head,int data)
{
    struct LL *temp = (struct LL *)malloc(sizeof(struct LL));
    temp->data = data;
    temp->down = NULL;
    temp->next = NULL;
    if(!head)
        head = temp;
    else
    {
        temp->down = head;
        head = temp;
    }

    return head;
}

struct LL *Merge(struct LL *A,struct LL *B)
{
    if(!A)
        return B;
    if(!B)
        return A;
    struct LL *temp = NULL;
    if(A->data < B->data)
    {
        temp = A;
        temp->down = Merge(A->down,B);
    }
    else
    {
        temp = B;
        temp->down = Merge(A,B->down);
    }
    return temp;
}

struct LL *flatten(struct LL *head)
{
    if(!head || !head->next)
        return head;
    return Merge(head,flatten(head->next));
}

void PrintList(struct LL *head)
{
    while(head)
    {
        printf("%d ",head->data);
        head = head->down;
    }
}

int main(void)
{
    struct LL *head = NULL;

    head = Insert(head,30);
    head = Insert(head,8);
    head = Insert(head,7);
    head = Insert(head,5);

    head->next = Insert(head->next,20);
    head->next = Insert(head->next,10);

    head->next->next = Insert(head->next->next,50);
    head->next->next = Insert(head->next->next,22);
    head->next->next = Insert(head->next->next,19);

    head->next->next->next = Insert(head->next->next->next,45);
    head->next->next->next = Insert(head->next->next->next,40);
    head->next->next->next = Insert(head->next->next->next,35);
    head->next->next->next = Insert(head->next->next->next,28);

    head = flatten(head);
    PrintList(head);
}
