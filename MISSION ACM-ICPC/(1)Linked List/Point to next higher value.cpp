#include<bits/stdc++.h>
using namespace std;

struct LL
{
	int data;
	struct LL *next,*arbit;
};

struct LL *newNode(struct LL *head,int data)
{
	struct LL *node = (struct LL *)malloc(sizeof(struct LL));
	node->data = data;
	node->arbit = NULL;
	node->next = head;
	head = node;
	return head;
}

struct LL *split(struct LL *head)
{
	struct LL *slow = head,*fast = head;
	while(fast->arbit && fast->arbit->arbit)
	{
		fast = fast->arbit->arbit;
		slow = slow->arbit;
	}
	struct LL *temp = slow->arbit;
	slow->arbit = NULL;
	return temp;
}

struct LL *Merge(struct LL *A,struct LL *B)
{
	if(!A)
		return B;
	if(!B)
		return A;
	struct LL *result = NULL;
	if(A->data < B->data)
	{
		result = A;
		result->arbit = Merge(A->arbit,B);
	}
	else
	{
		result = B;
		result->arbit = Merge(A,B->arbit);
	}
	return result;
}

struct LL *MergeSort(struct LL *head)
{
	if(!head || !head->arbit)
		return head;
	struct LL *second = split(head);
	head = MergeSort(head);
	second = MergeSort(second);
	return Merge(head,second);
}

struct LL *PopulateArbit(struct LL *head)
{
	struct LL *temp = head;
	while(temp)
	{
		temp->arbit = temp->next;
		temp = temp->next;
	}
	return MergeSort(head);
}

void print(struct LL *head,struct LL *A)
{
	cout<<"Arbit : ";
	while(A)
	{
		cout<<A->data<<" ";
		A = A->arbit;
	}
	cout<<endl;
	cout<<"Original : ";
	while(head)
	{
		cout<<head->data<<" ";
		head = head->next;
	}
}

int main(void)
{
	struct LL *head = NULL;
	head = newNode(head,3);
	head = newNode(head,2);
	head = newNode(head,10);
	head = newNode(head,5);
	
	struct LL *ar = PopulateArbit(head);
	print(head,ar);
	return 0;
}