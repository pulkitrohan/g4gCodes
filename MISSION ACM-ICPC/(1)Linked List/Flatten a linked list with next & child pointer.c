#include<stdio.h>
#include<stdlib.h>
#define size(A) (sizeof(A)/sizeof(A[0]))
struct node
{
    int data;
    struct node *next;
    struct node *child;
};

struct node *CreateList(int *A,int N)
{
    struct node *head = NULL;
    int i;
    for(i=N-1;i>=0;i--)
    {
        struct node *temp = (struct node *)malloc(sizeof(struct node));
        temp->data = A[i];
        temp->next = temp->child = NULL;
        if(!head)
            head = temp;
        else
        {
            temp->next = head;
            head = temp;
        }
    }
    return head;
}

void PrintList(struct node *head)
{
    while(head)
    {
        printf("%d ",head->data);
        head = head->next;
    }
}

void FlattenMultilevelLL(struct node *head)
{
    struct node *cur,*tail;
    cur = tail = head;
    while(tail->next)
        tail = tail->next;
    while(cur != tail)
    {
        if(cur->child)
        {
            tail->next = cur->child;
            struct node *temp = cur->child;
            while(temp->next)
                temp = temp->next;
            tail = temp;
        }
        cur = cur->next;
    }
}
int main(void)
{
    int A[] = {10,5,12,7,11};
    int B[] = {4,20,13};
    int C[] = {17,6};
    int D[] = {2};
    int E[] = {16};
    int F[] = {9,8};
    int G[] = {3};
    int H[] = {19,15};
    struct node *head1 = CreateList(A,size(A));
    struct node *head2 = CreateList(B,size(B));
    struct node *head3 = CreateList(C,size(C));
    struct node *head4 = CreateList(D,size(D));
    struct node *head5 = CreateList(E,size(E));
    struct node *head6 = CreateList(F,size(F));
    struct node *head7 = CreateList(G,size(G));
    struct node *head8 = CreateList(H,size(H));
    head1->child = head2;
    head1->next->next->next->child = head3;
    head2->next->child = head4;
    head2->next->next->child = head5;
    head3->child = head6;
    head5->child = head7;
    head6->child = head8;
    FlattenMultilevelLL(head1);
    PrintList(head1);
    return 0;
}
