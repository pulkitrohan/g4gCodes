#include<bits/stdc++.h>
using namespace std;

struct Node
{
	int data;
	struct Node *ptr;
};

Node *XOR(Node *A,Node *B)
{
	return (Node *)((unsigned int)(A) ^ (unsigned int)(B));
}	

void insert(Node *&head,int data)
{
	Node *temp = (Node *)malloc(sizeof(Node));
	temp->data = data;
	temp->ptr = XOR(head,NULL);
	if(head)
	{
		Node *next = XOR(head->ptr,NULL);
		head->ptr = XOR(temp,next);
	}
	head = temp;
}

void print(Node *head)
{
	Node *cur = head,*prev = NULL,*next;
	while(cur)
	{
		cout<<cur->data<<" ";
		next = XOR(prev,cur->ptr);
		prev = cur;
		cur = next;
	}
}

int main(void)
{
	Node *head = NULL;
	insert(head,10);
	insert(head,20);
	insert(head,30);
	insert(head,40);
	print(head);
	return 0;
}