#include<stdio.h>
#include<stdlib.h>

struct node
{
    int data;
    struct node *next;
};

void Insert(struct node *head,int data)
{
    struct node *temp = (struct node *)malloc(sizeof(struct node));
    temp->data = data;
    temp->next = head->next;
    head->next = temp;
}

void print(struct node *head)
{
    while(head->next)
    {
        head = head->next;
        printf("%d ",head->data);
    }
    printf("\n");
}

void RotateLL(struct node *head,int k)
{
    struct node *cur = head;
    while(k-- && cur->next)
        cur = cur->next;
    if(!cur->next)
        return;
    struct node *temp,*temp1;
    temp = temp1 = cur->next;
    while(temp->next)
        temp = temp->next;
    cur->next = temp->next;
    temp->next = head->next;
    head->next = temp1;
}

int main(void)
{
    struct node *head = (struct node *)malloc(sizeof(struct node));
    head->next = NULL;
    Insert(head,60);
    Insert(head,50);
    Insert(head,40);
    Insert(head,30);
    Insert(head,20);
    Insert(head,10);
    print(head);
    RotateLL(head,5);
    print(head);
    return 0;
}
