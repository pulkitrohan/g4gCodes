#include<stdio.h>
#include<stdlib.h>
struct LL
{
    int data;
    struct LL *next;
};

struct LL *Insert(struct LL *head,int data)
{
    struct LL *temp = (struct LL *)malloc(sizeof(struct LL));
    temp->data = data;
    temp->next = NULL;
	if(head)
		temp->next = head;
	head = temp;
    return head;
}

void Print(struct LL *head)
{
    while(head)
    {
        printf("%d ",head->data);
        head = head->next;
    }
    printf("\n");
}

struct LL *Reverse(struct LL *head)
{
    struct LL *prev = NULL,*next;
    while(head)
    {
        next = head->next;
        head->next = prev;
        prev = head;
        head = next;
    }
    return prev;
}

struct LL *ReverseRecursive(struct LL *head)
{
    struct LL *temp;
	if(head->next)
	{
		temp = ReverseRecursive(head->next);
		head->next->next = head;
		head->next = NULL;
	}
	else
		temp = head;
	return temp;
}

int main(void)
{
    struct LL *head = NULL;
    int i;
    for(i=4;i>0;i--)
        head = Insert(head,i);
    Print(head);
    head = Reverse(head);
    Print(head);
    head = ReverseRecursive(head);
    Print(head);
    return 0;
}
