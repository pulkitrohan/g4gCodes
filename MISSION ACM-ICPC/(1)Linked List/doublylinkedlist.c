#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
    int data;
    struct Node *next;
    struct Node *prev;
}node;


void insertatbeg(node *pointer,int data)
{
    node *temp = (node *)malloc(sizeof(node));
    temp->data = data;
    temp->prev = NULL;
    temp->next = pointer->next;
    if(pointer->next)       //This is the case when first element is inserterd into DLL
        pointer->next->prev = temp;
    pointer->next = temp;
}

void reverse(node *pointer)
{
    node *prev;
    node *cur = pointer->next;
    while(cur)
    {
            prev = cur->prev;
            cur->prev = cur->next;
            cur->next = prev;
            cur = cur->prev;
    }
    pointer->next = prev->prev;
}

node *RDLL(node *head)
{
	if(!head)
		return NULL;
	swap(&head->next,&head->prev);
	if(!head->prev)
		return head;
	return RDLL(head->prev);
}

void insertatmid(node *pointer,int data,int pos)
{
     int count = 1;
    while(count != pos)
    {
        pointer = pointer->next;
        count++;
    }
    node *temp;
    temp = (node *)malloc(sizeof(node));
    temp->data = data;
    temp->prev = pointer;
    temp->next = pointer->next;
    pointer->next->prev = temp;
    pointer->next = temp;
}

void insertatend(node *pointer,int data)
{
    node *temp = (node *)malloc(sizeof(node));
    temp->data = data;
    if(!pointer->next)
    {
        temp->next = pointer->next;
        temp->prev = NULL;
        pointer->next = temp;
    }
    else
    {
    while(pointer->next != NULL)
        pointer = pointer->next;
    temp->next = pointer->next;
    temp->prev = pointer;
    pointer->next = temp;
    }
}

void find(node *pointer,int data)
{
    while(pointer->next != NULL && (pointer->next)->data != data)
        pointer = pointer->next;
    if(pointer->next == NULL)
    {
        printf("\nElement not found");
    }
    else if((pointer->next)->data == data)
    {
        printf("\nElement found");
    }
}

void delete(node *pointer,int data)
{
    while(pointer->next != NULL && (pointer->next)->data != data)
        pointer = pointer->next;
    if(pointer->next == NULL)
    {
        printf("\nElement not found");
    }
    else
    {
        node *temp;
        temp = pointer->next;
        if(temp->next)
            temp->next->prev = pointer;
        if(temp->prev)
            temp->prev->next = temp->next;
        free(temp);
        printf("\nElement deleted");

    }
}
void print(node *pointer)
{
    int count=0;
    printf("\nThe list is : ");
    pointer = pointer->next;
    while(pointer != NULL)
    {
        printf("%d ",pointer->data);
        pointer = pointer->next;
        count++;
    }
    printf("\nTotal elements are : %d",count);
}
void sorting(node *p) // 3 nodes
{
    int i,j,temp;
    node *q;
    p = p->next;
    for(i=0;i<3;i++)
    {
        q = p->next;
        for(j = i+1;j<3;j++)
        {
            if(p->data > q->data)
            {
                temp = p->data;
                p->data = q->data;
                q->data = temp;
            }
            q = q->next;
        }
        p = p->next;
    }
}

int main(void)
{
    node *start,*temp;
    start = (node *)malloc(sizeof(node));
    start->next = NULL;
    start->prev = NULL;

    int ch,data,pos;

    printf("1.Insert");
    printf("\n2.Delete");
    printf("\n3.Print & Count");
    printf("\n4.Find");
    printf("\n5.Exit");
    printf("\n6.Sort");
    printf("\n7.Reverse");

    label:
    printf("\nEnter your choice:");
    scanf("%d",&ch);
    switch(ch)
    {
        case 1:  printf("\nEnter the element to be inserted :");
                 scanf("%d",&data);
                 printf("\n1.Insert at beg");
                 printf("\n2.Inset at middle");
                 printf("\n3.Insert at end");
                 printf("\nEnter your choice:");
                 scanf("%d",&ch);
                 switch(ch)
                 {
                 case 1:    insertatbeg(start,data);
                            break;

                 case 2:    printf("\nEnter the position :");
                            scanf("%d",&pos);
                            insertatmid(start,data,pos);
                            break;
                 case 3:    insertatend(start,data);
                            break;
                 }
                 break;

        case 2:  printf("\nEnter the element to be deleted :");
                 scanf("%d",&data);
                 delete(start,data);
                 break;
        case 3:  print(start);
                 break;
        case 4:  printf("\nEnter the element to be find :");
                 scanf("%d",&data);
                 find(start,data);
                 break;
        case 5:  exit(0);
                 break;
        case 6:  sorting(start);
                 break;
        case 7:  reverse(start);
                 break;
    }
    goto label;
    return 0;
}
