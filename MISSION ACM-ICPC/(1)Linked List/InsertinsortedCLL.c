#include<stdio.h>

typedef struct CLL
{
    int data;
    struct CLL *next;
}node;

void insertsorted(node *head,int data)
{
    node *p=head;
    while(p->next != head && p->next->data <= data)
        p = p->next;
    node *temp = (node *)malloc(sizeof(node));
    temp->data = data;
    temp->next = p->next;
    p->next = temp;
}

void print(node *head)
{
    node *p = head;
    printf("List : ");
    while(p->next != head)
    {
        printf("%d ",p->next->data);
        p = p->next;
    }
}

int main(void)
{
    node *head = (node *)malloc(sizeof(node));
    head->next = head;
    int A[] = {2,5,3,10,7},i;
    for(i=0;i<5;i++)
    {
        insertsorted(head,A[i]);
        print(head);
        printf("\n");
    }
    return 0;
}
