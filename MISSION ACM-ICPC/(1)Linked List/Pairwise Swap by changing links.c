#include<stdio.h>
#include<stdlib.h>
struct node
{
    int data;
    struct node *next;
};

void Insert(struct node *head,int data)
{
    struct node *temp = (struct node *)malloc(sizeof(struct node));
    temp->data = data;
    temp->next = head->next;
    head->next = temp;
}

void Print(struct node *head)
{
    while(head->next)
    {
        head = head->next;
        printf("%d ",head->data);
    }
    printf("\n");
}

void PairwiseSwap(struct node *head)
{
    while(head->next && head->next->next)
    {
        struct node *temp = head->next->next;
        struct node *temp1 = head->next;
        temp1->next = temp->next;
        temp->next = temp1;
        head->next = temp;
        head = head->next->next;
    }
}

int main(void)
{
    struct node *head = (struct node *)malloc(sizeof(struct node));
    head->next = NULL;
    Insert(head,7);
    Insert(head,6);
    Insert(head,5);
    Insert(head,4);
    Insert(head,3);
    Insert(head,2);
    Insert(head,1);
    Print(head);
    PairwiseSwap(head);
    Print(head);
    return 0;
}
