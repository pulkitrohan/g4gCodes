#include<bits/stdc++.h>
using namespace std;

struct LL
{
	int data;
	struct LL *next;
};

void printRandom(LL *head)
{
	if(!head)
		return;
	srand(time(NULL));
	int ans = head->data;
	for(int i=2;head;i++)
	{
		if(rand()%i == 0)
			ans = head->data;
		head = head->next;
	}
	cout<<ans<<endl;
}

void insert(LL *&head,int val)
{
	LL *temp = (LL *)malloc(sizeof(LL));
	temp->data = val;
	temp->next = head;
	head = temp;
}


int main(void)
{
	LL *head = NULL;
	insert(head,5);
	insert(head,20);
	insert(head,4);
	insert(head,3);
	insert(head,30);
	printRandom(head);
	return 0;
}