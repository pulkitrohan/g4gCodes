#include<bits/stdc++.h>
using namespace std;

struct LL
{
	int data;
	struct LL *next;
};

void insert(LL *&head,int data)
{
	LL *temp = (LL *)malloc(sizeof(LL));
	temp->data = data;
	temp->next = head;
	head = temp;
}

int countNodes(LL *head)
{
	int count = 0;
	while(head)
	{	
		count++;
		head = head->next;
	}
	return count;
}

void printList(LL *head)
{
	while(head)
	{
		cout<<head->data<<" ";
		head = head->next;
	}
	cout<<endl;
}

void swapNode(LL *&head,int k)
{
	int N = countNodes(head);
	if(N < k)
		return;
	if(2*k-1 == N)
		return;
	LL *x = head;
	LL *x_prev = NULL;
	for(int i=1;i<k;i++)
	{
		x_prev = x;
		x = x->next;
	}
	LL *y = head;
	LL *y_prev = NULL;
	for(int i=1;i<N-k+1;i++)
	{
		y_prev = y;
		y = y->next;
	}
	if(x_prev)
		x_prev->next = y;
	if(y_prev)
		y_prev->next = x;
	swap(x->next,y->next);
	if(k == 1)
		head = y;
	if(k == N)
		head = x;
}	

int main(void)
{
	LL *head = NULL;
	for(int i=8;i>=1;i--)
		insert(head,i);
	printList(head);
	swapNode(head,3);
	printList(head);
	return 0;
}