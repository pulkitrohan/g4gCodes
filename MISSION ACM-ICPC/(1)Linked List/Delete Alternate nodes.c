#include<stdio.h>
struct node
{
    int data;
    struct node *next;
};

void Insert(struct node *head,int data)
{
    struct node *temp = (struct node *)malloc(sizeof(struct node));
    temp->data = data;
    temp->next = head->next;
    head->next = temp;
}

void Print(struct node *head)
{
    while(head->next)
    {
        head = head->next;
        printf("%d ",head->data);
    }
    printf("\n");
}

void DeleteAlternate(struct node *head)
{
    struct node *temp;
    head = head->next;
  while(head && head->next)
  {
      temp = head->next;
      head->next = temp->next;
      free(temp);
      head = head->next;
  }

}

int main(void)
{
    struct node *head = (struct node *)malloc(sizeof(struct node));
    head->next = NULL;
    Insert(head,5);
    Insert(head,4);
    Insert(head,3);
    Insert(head,2);
     Insert(head,1);
    //Insert(head,5);
    Print(head);
    DeleteAlternate(head);
    Print(head);
    return 0;
}
