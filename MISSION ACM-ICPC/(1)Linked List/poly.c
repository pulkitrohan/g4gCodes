#include<stdio.h>
#include<stdlib.h>
typedef struct poly
{
	int term;
	int coeff;
	struct poly *next;
}node;


void create_poly(node *start)
{
    int n,coeff,power;
	printf("\nEnter the number of terms in the polynomial : ");
	scanf("%d",&n);
	while(n > 0)
	{
		printf("Enter the Coefficient : ");
		scanf("%d",&coeff);
		printf("Enter the power : ");
		scanf("%d",&power);
		insert(start,coeff,power);
		n--;
	}
}

void insert(node *start,int coeff,int power)
{
    node *pointer = start;
    while(pointer->next)
    pointer= pointer->next;
    pointer->next = (node *)malloc(sizeof(node));
    pointer = pointer->next;
    pointer->coeff = coeff;
    pointer->term = power;
    pointer->next = NULL;
}

node *add(node *A,node *B)
{
    node *C = (node *)malloc(sizeof(node));
    C->next = NULL;
    while(A->next && B->next)
    {
        if(A->next->term > B->next->term)
            {
                A = A->next;
                insert(C,A->coeff,A->term);
            }
        else if(A->next->term < B->next->term)
            {
                B = B->next;
                insert(C,B->coeff,B->term);
            }
        else if(A->next->term == B->next->term)
          {
              A = A->next;
              B = B->next;
              int k = A->coeff + B->coeff;
              insert(C,k,A->term);
          }
    }
    while(A->next)
    {
        A = A->next;
        insert(C,A->coeff,A->term);
    }
    while(B->next)
    {
        B = B->next;
        insert(C,B->coeff,B->term);
    }
    return C;
}

node *sub(node *A,node *B)
{
    node *C = (node *)malloc(sizeof(node));
    C->next = NULL;
    while(A->next && B->next)
    {
        if(A->next->term > B->next->term)
            {
                A = A->next;
                insert(C,A->coeff,A->term);
            }
        else if(A->next->term < B->next->term)
            {
                B = B->next;
                insert(C,B->coeff,B->term);
            }
        else if(A->next->term == B->next->term)
          {
              A = A->next;
              B = B->next;
              int k = A->coeff - B->coeff;
              insert(C,k,A->term);
          }
    }
    while(A->next)
    {
        A = A->next;
        insert(C,A->coeff,A->term);
    }
    while(B->next)
    {
        B = B->next;
        insert(C,B->coeff,B->term);
    }
    return C;
}

node *mult(node *A,node *B)
{
    node *D = (node *)malloc(sizeof(node));
    D->next = NULL;
    node *C = (node *)malloc(sizeof(node));
    C->next = NULL;
    node *k = B;
    while(A->next)
    {
        A = A->next;

        while(B->next)
        {
            B = B->next;
            insert(C,A->coeff * B->coeff,A->term + B->term);
        }
        D = add(D,C);
        B = k;
    }
    return D;
}
void display(node *start)
{
    while(start->next)
    {
        start = start->next;
        if(!start->next)
            printf("%dx^%d\n",start->coeff,start->term);
        else
            printf("%dx^%d + ",start->coeff,start->term);
    }
}

int main(void)
{
    int ch;
    node *start1,*start2,*C;
    start1 = (node *)malloc(sizeof(node));
    start1->next = NULL;
    start2 = (node *)malloc(sizeof(node));
    start2->next = NULL;
    C = (node *)malloc(sizeof(node));
    C->next = NULL;
    printf("Enter Polynomial 1 : ");
    create_poly(start1);
    printf("Enter Polynomial 2 : ");
    create_poly(start2);
    display(start1);
    display(start2);
    printf("\nAddition\nSubtraction\nMultiplication\n");
    printf("Enter your choice : ");
    scanf("%d",&ch);
    switch(ch)
    {
        case 1 : C = add(start1,start2);
                 printf("\nOn Addition : ");
                 display(C);
                 break;
        case 2 : C = sub(start1,start2);
                 printf("\nOn Subtraction : ");
                 display(C);
                 break;
        case 3 : C = mult(start1,start2);
                 printf("\nOn Multiplication : ");
                 display(C);
                 break;
    }
    return 0;
}
