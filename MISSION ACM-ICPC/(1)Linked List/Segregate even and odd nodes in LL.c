#include<stdio.h>
#include<stdlib.h>

struct node
{
    int data;
    struct node *next;
};


void Insert(struct node *head,int data)
{
    struct node *temp = (struct node *)malloc(sizeof(struct node));
    temp->data = data;
    temp->next = head->next;
    head->next = temp;
}


void print(struct node *head)
{
    while(head->next)
    {
        head = head->next;
        printf("%d ",head->data);
    }
    printf("\n");
}

void Segregate(struct node *head)
{
    struct node *tail = head;
    while(tail->next)
        tail = tail->next;
    struct node *tail1 = tail;
    while(head->next != tail)
    {
        struct node *temp = head->next;
        if(temp->data %2 != 0)
        {
            head->next = temp->next;
            temp->next = tail1->next;
            tail1->next = temp;
            tail1 = tail1->next;
        }
        else
            head = head->next;
    }
}

int main(void)
{
    struct node *head = (struct node *)malloc(sizeof(struct node));
    head->next = NULL;
Insert(head,6);
    Insert(head,7);
    Insert(head,1);
    Insert(head,4);
    Insert(head,5);
  Insert(head,10);
   Insert(head,12);
   Insert(head,8);
    Insert(head,15);
    Insert(head,17);
    print(head);
    Segregate(head);
    print(head);
    return 0;
}
