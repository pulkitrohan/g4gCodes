#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
    int data;
    struct Node *next;
}node;

void insertatend(node *pointer,int data)
{
    while(pointer->next != NULL)
        pointer = pointer->next;
        pointer->next = (node *)malloc(sizeof(node));
        pointer = pointer->next;
        pointer->data = data;
        pointer->next = NULL;
}

void printandcount(node *pointer)
{
    int count=0;
    printf("\nThe list is : ");
    pointer = pointer->next;
    while(pointer != NULL)
    {
        printf("%d ",pointer->data);
        pointer = pointer->next;
        count++;
    }
    printf("\nTotal elements are : %d",count);
}

void DeleteNafterM(node *pointer,int M,int N)
{
    while(pointer->next)
    {
        int count = M;
        while(count-- && pointer->next)
            pointer = pointer->next;
        count = N;
        while(count-- && pointer->next)
        {
            node *temp = pointer->next;
            pointer->next = temp->next;
            free(temp);
        }
    }
}

int main(void)
{
    node *pointer = (node *)malloc(sizeof(node));
    pointer->next = NULL;
    int M,N,i;
    for(i=1;i<=10;i++)
        insertatend(pointer,i);
    printandcount(pointer);
    M = 3,N = 2;
    DeleteNafterM(pointer,M,N);
    printandcount(pointer);
    return 0;
}
