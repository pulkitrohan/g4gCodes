#include<stdio.h>

typedef struct Node
{
    int data;
    struct Node *next;
}node;

int count = 0;
void insertatend(node *pointer,int data)
{
    node *head = pointer;
    int k = 0;
    while(pointer->next != NULL)
        pointer = pointer->next;
        pointer->next = (node *)malloc(sizeof(node));
        pointer = pointer->next;
        pointer->data = data;
        count++;
        if(count == 9)
        {
            while(k != 5)
            {
                k++;
                head = head->next;
            }
            pointer->next = head;
        }
        else
        pointer->next = NULL;
}

void print(node *pointer)
{
    //int count=0;
    printf("\nThe list is : ");
    pointer = pointer->next;
    while(pointer != NULL)
    {
        printf("%d ",pointer->data);
        pointer = pointer->next;
        //count++;
    }
   // printf("\nTotal elements are : %d",count);
}

int LoopChecking(node *pointer)
{
    node *hare,*tort;
    hare = tort = pointer;
    while(hare->next && tort->next)
    {
        hare = hare->next->next;
        tort = tort->next;
        if(!hare || !tort)
            return 0;
        if(hare == tort)
            return 1;
    }
    return 0;
}

void LoopCheckingFindstartnode(node *pointer)
{
    node *hare,*tort;
    hare = tort = pointer->next;
    int loopexist = 0;
    while(hare->next && tort->next)
    {
        hare = hare->next->next;
        tort = tort->next;
        if(!hare || !tort)
            return 0;
        if(hare == tort)
        {
            loopexist = 1;
            break;
        }
    }
    if(!loopexist)
        printf("No Loops Exist\n");
    else
    {
        printf("Loop Exist\n");
        node *temp = pointer->next;
        while(temp->next != hare->next)
        {
            temp = temp->next;
            hare = hare->next;
        }
        printf("Loop starts at node with value %d",temp->data);
    }
}

void LoopCheckingFindlengthloop(node *pointer)
{
    node *hare,*tort;
    hare = tort = pointer->next;
    int loopexist = 0;
    while(hare->next && tort->next)
    {
        hare = hare->next->next;
        tort = tort->next;
        if(!hare || !tort)
            return 0;
        if(hare == tort)
        {
            loopexist = 1;
            break;
        }
    }
    if(!loopexist)
        printf("No Loops Exist\n");
    else
    {
        printf("Loop Exist\n");
        int countloop = 0;
        node *temp = hare;
        while(temp->next != hare)
        {
            printf("%d ",temp->data);
            temp = temp->next;
            countloop++;
           // hare = hare->next;
        }
        printf("\n%d\n",countloop+1); //+1 because the hare node will not be counted
    }
}

int main(void)
{
    node *pointer = (node *)malloc(sizeof(node));
    pointer->next = NULL;
    int i,k;
    for(i=1;i<=9;i++)
        insertatend(pointer,i);
    /*if(LoopChecking(pointer))
        printf("Cycle Present\n");
    else
        printf("No Cycle\n");*/
    //LoopCheckingFindstartnode(pointer);
    LoopCheckingFindlengthloop(pointer);
    int count = 0;
}
