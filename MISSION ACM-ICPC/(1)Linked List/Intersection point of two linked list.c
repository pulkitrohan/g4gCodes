#include<stdio.h>
#include<stdlib.h>

struct node
{
    int data;
    struct node *next;
};

int Count(struct node *head)
{
    int count = 0;
    while(head->next)
    {
        head = head->next;
        count++;
    }
    return count;
}

int IntersectionPoint(struct node *head1,struct node *head2)
{
    int size1 = Count(head1);
    int size2 = Count(head2);
    int diff = abs(size1-size2);
    if(size1 > size2)
    {
      while(diff--)
        head1 = head1->next;
    }
    else if(size2 > size1)
    {
        while(diff--)
            head2 = head2->next;
    }
    while(head1->next && head2->next)
    {
        if(head1->next == head2->next)
            return head1->next->data;
        else
        {
            head1 = head1->next;
            head2 = head2->next;
        }
    }
    return 0;
}

void Insert(struct node *head,int data)
{
    struct node *temp = (struct node *)malloc(sizeof(struct node));
    temp->data = data;
    temp->next = head->next;
    head->next = temp;
}

int main(void)
{
    struct node *head1 = (struct node *)malloc(sizeof(struct node));
    head1->next = NULL;
    struct node *head2 = (struct node *)malloc(sizeof(struct node));
    head2->next = NULL;
    Insert(head1,30);
    Insert(head1,15);
    Insert(head1,9);
    Insert(head1,6);
    Insert(head1,3);
    Insert(head2,10);
    head2->next->next = head1->next->next->next->next;
    head2->next->next->next = head1->next->next->next->next->next;
    head2->next->next->next = head1->next->next->next->next->next;
    int i = IntersectionPoint(head1,head2);
    if(i)
        printf("Intersection Point : %d\n",i);
    return 0;
}
