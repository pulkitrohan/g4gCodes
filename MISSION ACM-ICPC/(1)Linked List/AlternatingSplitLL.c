#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
    int data;
    struct Node *next;
}node;

void insertatend(node *pointer,int data)
{
    while(pointer->next != NULL)
        pointer = pointer->next;
        pointer->next = (node *)malloc(sizeof(node));
        pointer = pointer->next;
        pointer->data = data;
        pointer->next = NULL;
}

void printandcount(node *pointer)
{
    int count=0;
    printf("\nThe list is : ");
    pointer = pointer->next;
    while(pointer != NULL)
    {
        printf("%d ",pointer->data);
        pointer = pointer->next;
        count++;
    }
    printf("\nTotal elements are : %d",count);
}

void AlternateSplit(node *head,node *b)
{
    head = head->next;
    while(head && head->next)
    {
        b->next = head->next;
        b = b->next;
        head->next = head->next->next;
        head = head->next;
    }
   /* if(head->next)
        head = head->next;*/
    b->next = NULL;
}

int main(void)
{
    node *head = (node *)malloc(sizeof(node));
    head->next = NULL;
    int i;
    for(i=1;i<=6;i++)
        insertatend(head,i);
    printandcount(head);
    node *b = (node *)malloc(sizeof(node));
    AlternateSplit(head,b);
    printandcount(head);
    printandcount(b);
    return 0;
}
