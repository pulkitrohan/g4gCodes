#include<stdio.h>
#include<stdlib.h>

struct node
{
    int data;
    struct node *next;
};

void Insert(struct node *head,int data)
{
    struct node *temp = (struct node *)malloc(sizeof(struct node));
    temp->data = data;
    temp->next = head->next;
    head->next = temp;
}

void Print(struct node *head)
{
    while(head)
    {
        printf("%d ",head->data);
        head = head->next;
    }
}

struct node *ReverseGroup(struct node *head,int k)
{
    if(!head)
        return NULL;
   struct node *current = head;
   struct node *next,*prev = NULL;
   int count = 0;
   while(current && count < k)
   {
       next = current->next;
       current->next = prev;
       prev = current;
       current = next;
       count++;
   }
   if(current)
        head->next = ReverseGroup(current,k);
   return prev; //Returning new head
}
int main(void)
{
    struct node *head = (struct node *)malloc(sizeof(struct node));
    head->next = NULL;
    int i;
    for(i=8;i>=1;i--)
        Insert(head,i);
    Print(head->next);
    int k = 3;
    head = ReverseGroup(head->next,k);
    printf("\n");
    Print(head);
    return 0;
}
