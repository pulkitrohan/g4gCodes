#include<stdio.h>

typedef struct CLL
{
    int data;
    struct CLL *next;
}node;

void insertatend(node *head,int data)
{
    node *p=head;
    while(p->next != head)
        p = p->next;
    node *temp = (node *)malloc(sizeof(node));
    temp->data = data;
    temp->next = p->next;
    p->next = temp;
}

void printandcount(node *head)
{
    node *temp = head->next;
    int count=0;
    printf("List : ");
    while(temp != head)
    {
        printf("%d ",temp->data);
        temp = temp->next;
        count++;
    }
    printf("\nCount : %d\n",count);
}

void SplitCLL(node *head)
{
    node *slow,*fast;
    slow = fast = head;
    while(fast->next != head && fast->next->next != head)
    {
        slow = slow->next;
        fast = fast->next->next;
    }
    node *temp = slow->next;
    if(fast->next)
        fast->next->next = temp;
    else
        fast->next = temp;
    slow->next = head;

    printandcount(head->next);
    printandcount(temp);
}

int main(void)
{
    node *head = (node *)malloc(sizeof(node));
    head->next = head;
    int i;
    for(i=1;i<=10;i++)
        insertatend(head,i);
    printandcount(head);
    SplitCLL(head);

}
