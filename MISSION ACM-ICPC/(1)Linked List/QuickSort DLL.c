#include<stdio.h>
#include<stdlib.h>
struct node
{
    int data;
    struct node *next;
    struct node *prev;
};

void Insert(struct node **head,int data)
{
    struct node *temp = (struct node *)malloc(sizeof(struct node));
    temp->data = data;
	temp->next = temp->prev = NULL;
	if(!(*head))
		(*head) = temp;
	else
	{
		temp->next = *head;
		(*head)->prev = temp;
		(*head) = temp;
	}
}

void Print(struct node *head)
{
    while(head)
    {
        printf("%d ",head->data);
        head = head->next;
    }
    printf("\n");
}

struct node *lastnode(struct node *head)
{
    while(head && head->next)
        head = head->next;
    return head;
}

void Swap(int *A,int *B)
{
    int temp = *A;
    *A = *B;
    *B = temp;
}

struct node *partition(struct node *head,struct node *tail)
{
    int x = tail->data;
    struct node *i = head->prev;
    struct node *j;
    for(j = head;j != tail;j = j->next)
    {
        if(j->data <= x)
        {
            i = (i == NULL) ? head : i->next;
            Swap(&(i->data), &(j->data));
        }
    }
    i = (i == NULL) ? head : i->next;
    Swap(&(i->data), &(tail->data));
    return i;
}

void QuickSort(struct node *head,struct node *tail)
{
    if(tail && head != tail && head != tail->next)
    {
        struct node *p = partition(head,tail);
        QuickSort(head,p->prev);
        QuickSort(p->next,tail);
    }
}

int main(void)
{
    struct node *head = NULL;
    Insert(&head,5);
    Insert(&head,20);
    Insert(&head,4);
    Insert(&head,3);
    Insert(&head,30);
    Print(head);
    QuickSort(head,lastnode(head));
    Print(head);
    return 0;
}
