#include<stdio.h>
#include<stdlib.h>
struct node
{
    int data;
    struct node *next;
};

void Insert(struct node *head,int data)
{
    struct node *temp = (struct node *)malloc(sizeof(struct node));
    temp->data = data;
    temp->next = head->next;
    head->next = temp;
}

void Print(struct node *head)
{
    while(head->next)
    {
        head = head->next;
        printf("%d ",head->data);
    }
    printf("\n");
}

void Merge(struct node *head1,struct node *head2)
{
    struct node *p = head1->next;
    struct node *q = head2->next;
    while(p && q)
    {
        struct node *temp = p->next;
        struct node *temp1 = q->next;
        q->next = temp;
        p->next = q;
        p = temp;
        q = temp1;
    }
    head2->next = q;
}

int main(void)
{
    struct node *head1 = (struct node *)malloc(sizeof(struct node));
    head1->next = NULL;
    struct node *head2 = (struct node *)malloc(sizeof(struct node));
    head2->next = NULL;
    Insert(head1,3);
    Insert(head1,2);
    Insert(head1,1);
    Print(head1);
    Insert(head2,8);
    Insert(head2,7);
    Insert(head2,6);
   // Insert(head2,5);
    //Insert(head2,4);
    Print(head2);
    Merge(head1,head2);
    Print(head1);
    Print(head2);
    return 0;
}
