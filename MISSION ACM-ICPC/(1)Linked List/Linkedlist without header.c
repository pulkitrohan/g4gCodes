#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
    int data;
    struct Node *next;
}node;

node *insertatbeg(node *head,int data)
{
	if(!head)
	{
		head = (node *)malloc(sizeof(node));
		head->data = data;
		head->next = NULL;
	}
	else
	{
		node *temp = (node *)malloc(sizeof(node));
		temp->data = data;
		temp->next = head;
		head = temp;
	}
	return head;
}

void insertatmid(node *pointer,int data,int pos)
{
    int count=1;
    while(count != pos-1)
    {
        pointer = pointer->next;
        count++;
    }
    node *temp;
    temp=(node *)malloc(sizeof(node));
    temp->next=pointer->next;
    temp->data=data;
    pointer->next=temp;

}
node *insertatend(node *pointer,int data)
{
    node *temp;
   if(pointer == NULL)
   {
       temp = (node *)malloc(sizeof(node));
       temp->data = data;
       temp->next = NULL;
       pointer = temp;
   }
   else
   {
       temp = pointer;
        while(temp->next != NULL)
        temp = pointer->next;
        temp->next = (node *)malloc(sizeof(node));
        temp = temp->next;
        temp->data = data;
        temp->next = NULL;
   }
   return pointer;
}


node *reverse(node *pointer)
{
   node *q = pointer;
   node *r = NULL;
   node *s;
   while(q)
   {
       s = r;
       r = q;
       q = q->next;
       r->next = s;
   }
   pointer = r;
   return pointer;
}

void sorting(node *p)
{
    int i,j,temp;
    node *q;
    for(i=0;i<3;i++)
    {
        q = p->next;
        for(j = i+1;j<3;j++)
        {
            if(p->data > q->data)
            {
                temp = p->data;
                p->data = q->data;
                q->data = temp;
            }
            q = q->next;
        }
        p = p->next;
    }
}

void find(node *pointer,int data)
{
    while(pointer->next != NULL && (pointer->data != data))
        pointer = pointer->next;
    if(pointer->next == NULL)
    {
        printf("\nElement not found");
    }
    else if(pointer->data == data)
    {
        printf("\nElement found");
    }
}

void delete(node *pointer,int data)
{
    while(pointer->next != NULL && pointer->data != data)
        pointer = pointer->next;
    if(pointer == NULL)
    {
        printf("\nElement not found");
    }
    else
    {
        node *temp;
        temp = pointer->next;
        pointer->next = temp->next;
        free(temp);
        printf("\nElement deleted");
    }
}
void printandcount(node *pointer)
{
    int count=0;
    printf("\nThe list is : ");
    while(pointer)
    {
        printf("%d ",pointer->data);
        pointer = pointer->next;
        count++;
    }
    printf("\nTotal elements are : %d",count);
}
int maxval(node *pointer)
{
    int max=0;
    pointer = pointer->next;
    while(pointer != NULL)
    {
        if(pointer->data > max)
            max = pointer->data;
        pointer = pointer->next;
    }
    return max;
}

int main(void)
{
    node *start;
    start = NULL;
    int ch,data,pos;
label:
    printf("\n1.Insert");
    printf("\n2.Delete");
    printf("\n3.Print & Count");
    printf("\n4.Find");
    printf("\n5.Max. Value");
    printf("\n6.Reverse");
    printf("\n7.Sorting");
    printf("\nEnter your choice:");
    scanf("%d",&ch);
    switch(ch)
    {
        case 1:  printf("\nEnter the element to be inserted :");
                 scanf("%d",&data);
                 printf("\n1.Insert at beg");
                 printf("\n2.Inset at middle");
                 printf("\n3.Insert at end");
                 printf("\nEnter your choice:");
                 scanf("%d",&ch);
                 switch(ch)
                 {
                 case 1: start = insertatbeg(start,data);
                         break;
                 case 2: printf("\nEnter the position where element is to be added:");
                         scanf("%d",&pos);
                         insertatmid(start,data,pos);
                         break;
                 case 3: start = insertatend(start,data);
                         break;
                 }
                 break;

        case 2:  printf("\nEnter the element to be deleted :");
                 scanf("%d",&data);
                 delete(start,data);
                 break;
        case 3:  printandcount(start);
                 break;
       /* case 4:  printf("\nEnter the element to be find :");
                 scanf("%d",&data);
                 find(start,data);
                 break;
        case 5:  printf("\nMax. Value is %d ",maxval(start));
                 break;*/
        case 6:  start = reverse(start);
                 break;
     /*   case 7:  sorting(start);
                 printandcount(start);
                 break;*/
    }
    goto label;
    return 0;
}
