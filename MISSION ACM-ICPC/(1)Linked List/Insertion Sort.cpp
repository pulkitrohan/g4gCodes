#include<bits/stdc++.h>
using namespace std;

struct LL 
{
	int data;
	struct LL *next;
};

void print(LL *head)
{
	while(head)
	{
		cout<<head->data<<" ";
		head = head->next;
	}
	cout<<endl;
}

void insert(LL * &head,int val)
{
	LL *temp = (LL *)malloc(sizeof(LL));
	temp->data = val;
	temp->next = head;
	head = temp;
}

void InsertSorted(LL * &head,LL *cur)
{
	if(!head || head->data >= cur->data)
	{
		cur->next = head;
		head = cur;
	}
	else
	{
		LL *temp = head;
		while(temp->next && temp->next->data < cur->data)
			temp = temp->next;
		cur->next = temp->next;
		temp->next = cur;
	}
}

void insertionSort(LL *&head)
{
	LL *ans = NULL,*cur = head;
	while(cur)
	{
		LL *next = cur->next;
		InsertSorted(ans,cur);
		cur = next;
	}
	head = ans;
}

int main(void)
{
	LL *head = NULL;
	insert(head,5);
	insert(head,20);
	insert(head,4);
	insert(head,3);
	insert(head,30);
	print(head);
	insertionSort(head);
	print(head);
	return 0;
}