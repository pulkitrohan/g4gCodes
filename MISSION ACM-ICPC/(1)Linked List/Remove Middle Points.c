#include<stdio.h>

struct LL
{
	int x,y;
	struct LL *next;
};

void Insert(struct LL *head,int x,int y)
{
	struct LL *temp = (struct LL *)malloc(sizeof(struct LL));
	temp->x = x;
	temp->y = y;
	temp->next = head->next;
	head->next = temp;
}

void Print(struct LL *head)
{
	while(head->next)
	{
		head = head->next;
		printf("(%d,%d) ",head->x,head->y);
	}
	printf("\n");
}

int RemoveMiddlePoints(struct LL *head)
{
    struct LL *cur = head;
    while(cur && cur->next && cur->next->next)
    {
        struct LL *Next = cur->next;
        struct LL *NextNext = cur->next->next;
        if(cur->x == Next->x)
        {
            while(NextNext && Next->x == NextNext->x)
            {
                cur->next = NextNext;
                free(Next);
                Next = NextNext;
                NextNext = NextNext->next;
            }
        }
        else if(cur->y == Next->y)
        {
            while(NextNext && Next->y == NextNext->y)
            {
                cur->next = NextNext;
                free(Next);
                Next = NextNext;
                NextNext = NextNext->next;
            }
        }
        else
            return 0;
        cur = cur->next;
    }
    return 1;
}

int main(void)
{
	struct LL *head = (struct LL *)malloc(sizeof(struct LL));
	head->next = NULL;
	Insert(head,40,5);
	Insert(head,20,5);
	Insert(head,7,5);
	Insert(head,7,10);
	Insert(head,5,10);
	Insert(head,1,10);
	Insert(head,0,10);
	/*Insert(head,12,3);
	Insert(head,10,3);
	Insert(head,6,3);
	Insert(head,4,3);
	Insert(head,2,3);*/
	Print(head);
	if(!RemoveMiddlePoints(head->next))
    {
        printf("Error in input\n");
        return 1;
    }
	Print(head);
	return 0;
}
