#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
    int data;
    struct Node *next;
}node;

void insertatend(node *pointer,int data)
{
    while(pointer->next != NULL)
        pointer = pointer->next;
        pointer->next = (node *)malloc(sizeof(node));
        pointer = pointer->next;
        pointer->data = data;
        pointer->next = NULL;
}

void printandcount(node *pointer)
{
    int count=0;
    printf("\nThe list is : ");
    pointer = pointer->next;
    while(pointer != NULL)
    {
        printf("%d ",pointer->data);
        pointer = pointer->next;
        count++;
    }
    printf("\nTotal elements are : %d",count);
}

void DeleteDuplicatesSorted(node *pointer)
{
    if(!pointer->next)
        return;
    pointer = pointer->next;
    node *temp;
    while(pointer->next)
    {
        if(pointer->data == pointer->next->data)
        {
            temp = pointer->next;
            pointer->next = temp->next;
            free(temp);
        }
        else
            pointer = pointer->next;
    }
}

void DeleteDuplicatesUnSorted(node *pointer)
{
    int A[10000] = {0};
    while(pointer->next)
    {
        if(A[pointer->next->data])
            pointer->next = pointer->next->next;
        else
        {
            A[pointer->next->data]++;
            pointer = pointer->next;
        }
    }
}

int main(void)
{
    node *pointer1 = (node *)malloc(sizeof(node));
    pointer1->next = NULL;
    node *pointer2 = (node *)malloc(sizeof(node));
    pointer2->next = NULL;
    int A[8] = {11,11,11,21,43,43,60};
    int B[8] = {12,11,12,21,41,43,21};
    int i;
    for(i=0;i<7;i++)
        insertatend(pointer1,A[i]);
    printandcount(pointer1);
    DeleteDuplicatesSorted(pointer1);
    printandcount(pointer1);
    /*for(i=0;i<7;i++)
        insertatend(pointer2,B[i]);
    printandcount(pointer2);
    DeleteDuplicatesUnSorted(pointer2);
    printandcount(pointer2);*/
    return 0;
}
