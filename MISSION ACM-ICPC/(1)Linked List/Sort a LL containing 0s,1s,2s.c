#include<stdio.h>
#include<stdlib.h>

struct LL
{
    int data;
    struct LL *next;
};

void Insert(struct LL *head,int data)
{
    struct LL *temp = (struct LL *)malloc(sizeof(struct LL));
    temp->data = data;
    temp->next = head->next;
    head->next = temp;
}

void PrintList(struct LL *head)
{
    while(head->next)
    {
        head = head->next;
        printf("%d ",head->data);
    }
}

void SortList(struct LL *head)
{
    int A[3] = {0};
    struct LL *temp = head;
    while(temp->next)
    {
        temp = temp->next;
        A[temp->data]++;
    }
    int i =0;
    while(head->next)
    {
        if(A[i] == 0)
            i++;
        else
        {
            head = head->next;
            head->data = i;
            A[i]--;
        }
    }
}

int main(void)
{
    struct LL *head = (struct LL *)malloc(sizeof(struct LL));
    head->next = NULL;
    Insert(head,0);
    Insert(head,1);
    Insert(head,0);
    Insert(head,2);
    Insert(head,1);
    Insert(head,1);
    Insert(head,2);
    Insert(head,1);
    Insert(head,2);
    printf("List before Sorting : ");
    PrintList(head);
    SortList(head);
    printf("\nList after Sorting : ");
    PrintList(head);
    return 0;
}
