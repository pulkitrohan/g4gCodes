#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
    int data;
    struct Node *next;
}node;

void insertatend(node *pointer,int data)
{
    while(pointer->next != NULL)
        pointer = pointer->next;
        pointer->next = (node *)malloc(sizeof(node));
        pointer = pointer->next;
        pointer->data = data;
        pointer->next = NULL;
}

void printandcount(node *pointer)
{
    int count=0;
    printf("\nThe list is : ");
    pointer = pointer->next;
    while(pointer != NULL)
    {
        printf("%d ",pointer->data);
        pointer = pointer->next;
        count++;
    }
    printf("\nTotal elements are : %d",count);
}

void Merge(node *pointer1,node *pointer2)
{
    node *a,*b,*a_next,*b_next;
    a = pointer1->next;
    b = pointer2->next;
    while(a && b)
    {
        a_next = a->next;
        b_next = b->next;

        b->next = a_next;
        a->next = b;

        a = a_next;
        b = b_next;
    }
    pointer2->next = b;
}

int main(void)
{
    node *pointer1 = (node *)malloc(sizeof(node));
    pointer1->next = NULL;
    node *pointer2 = (node *)malloc(sizeof(node));
    pointer2->next = NULL;
    int A[] = {5,7,17,13,11};
    int B[] = {12,10,2,4,6,30};
    int i;
    for(i=0;i<5;i++)
        insertatend(pointer1,A[i]);
    printandcount(pointer1);
    for(i=0;i<6;i++)
        insertatend(pointer2,B[i]);
    printandcount(pointer2);
    Merge(pointer1,pointer2);
    printandcount(pointer1);
    printandcount(pointer2);
    return 0;
}
