#include<bits/stdc++.h>
using namespace std;

int main(void)
{
	int M = 4,N = 5;
	int A[][N] = {{1,2,3,4,6},
				  {5,3,8,1,2},
				  {4,6,7,5,5},
				  {2,4,8,9,4}};
	int B[M][N];
	for(int i=0;i<N;i++)
		B[0][i] = A[0][i];
		
	for(int i=1;i<M;i++)
		for(int j=0;j<N;j++)
			B[i][j] = A[i][j] + B[i-1][j];
	
	for(int i=0;i<M;i++)
		for(int j=1;j<N;j++)
			B[i][j] += B[i][j-1];
	int starti = 2,startj = 2,endi = 3,endj = 4;
	int ans = B[endi][endj];
	if(starti > 0)
		ans -= B[starti-1][endj];
	if(startj > 0)
		ans -= B[endi][startj-1];
	if(starti > 0 && startj > 0)
		ans += B[starti-1][startj-1];
	cout<<ans<<endl;
	return 0;
}