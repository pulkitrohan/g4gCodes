#include<bits/stdc++.h>
using namespace std;
int TernarySearch(int *A,int start,int end,int x)
{
	if(end >= start)
	{
		int mid1 = start + (end-start)/3;
		int mid2 = mid1 + (end-start)/3;
		if(A[mid1] == x)
			return mid1;
		if(A[mid2] == x)
			return mid2;
		if(A[mid1] > x)
			return TernarySearch(A,start,mid1-1,x);
		if(A[mid2] < x)
			return TernarySearch(A,mid2+1,end,x);
		return TernarySearch(A,mid1+1,mid2-1,x);
	}
	return -1;
}

int main(void)
{
	int A[] = {1,2,3,4,5,6,7};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<TernarySearch(A,0,N-1,5);
	return 0;
}