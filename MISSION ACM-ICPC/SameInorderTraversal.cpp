#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	char data;
	struct Tree *left,*right;
};

struct Tree *insert(char ch)
{
	struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
	temp->data = ch;
	temp->left = temp->right = NULL;
	return temp;
}

bool HasSameInorder(struct Tree *A,struct Tree *B)
{
	stack<struct Tree *> S1,S2;
	while(A)
	{
		S1.push(A);
		A = A->left;
	}
	while(B)
	{
		S2.push(B);
		B = B->left;
	}
	while(!S1.empty() && !S2.empty())
	{
		A = S1.top();	S1.pop();
		B = S2.top();	S2.pop();
		
		if(A->data != B->data)
			return false;
		if(A->right)
		{
			S1.push(A->right);
			A = A->right;
		}
		while(A->left)
		{
			S1.push(A->left);
			A = A->left;
		}
		if(B->right)
		{
			S2.push(B->right);
			B = B->right;
		}
		while(B->left)
		{
			S2.push(B->left);
			B = B->left;
		}
	}
	return S1.empty() && S2.empty();
}

int main(void)
{
	struct Tree *root1 = insert('B');
	root1->left = insert('A');
	root1->right = insert('C');
	struct Tree *root2 = insert('A');
	root2->right = insert('B');
	root2->right->right = insert('C');
	cout<<HasSameInorder(root1,root2);
	return 0;
}