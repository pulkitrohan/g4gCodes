#include<stdio.h>
#include<stdlib.h>
#define MAX 100
typedef struct Stack
{
    int capacity;
    int top;
    int *elements;
}Stack;

Stack *createStack(int maxElements)
{
    Stack *S;
    S=(Stack *)malloc(sizeof(Stack));
    S->elements = (int *)malloc(sizeof(int)*maxElements);
    S->top = 0;
    S->capacity = maxElements;
    return S;
}

void push(Stack *s,int element)
{
    if(s->top == s->capacity)
        printf("Stack Overflow!!\n");
    else
        {
            s->elements[++s->top] = element;
          //  printf("\nElement Pushed");
        }
}

void pop(Stack *s)
{

    if(s->top == 0)
        printf("Stack Underflow!!\n");
    else
        {
            s->top--;
         //   printf("\nElement is Popped");
        }
}

int top(Stack *s)
{
    return s->elements[s->top];
}

int IsPalindrome(Stack *S,char *A)
{
    int i=0;
    while(A[i] != 'X')
        push(S,A[i++]);
    i++;
    while(A[i] != '\0')
    {
        if(S->top != 0 && A[i] == S->elements[S->top])
        {
            pop(S);
            i++;
        }
        else
        return 0;
    }
    return 1;
}

int main(void)
{
    Stack *S = createStack(MAX);
    char input[100];
    scanf("%s",input);
    if(IsPalindrome(S,input))
        printf("Palindrome\n");
    else
        printf("Not Palindrome\n");

}

