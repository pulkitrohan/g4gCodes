#include<bits/stdc++.h>
using namespace std;

int validateStackSequence(int pushed[], int popped[], int N) {
    stack<int> S;
    int j = 0;
    for(int i=0;i<N;i++) {
        S.push(pushed[i]);
        while(!S.empty() && j < N && popped[j] == S.top()) {
            S.pop();
            j++;
        }
    }
    return j == N;
}

int main(void) {
    int pushed[] = {1,2,3,4,5};
    int popped[] = {4,5,3,2,1};
    int N = sizeof(pushed)/sizeof(pushed[0]);
    cout<<validateStackSequence(pushed, popped, N);
    return 0;
}