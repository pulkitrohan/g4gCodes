#include<bits/stdc++.h>
using namespace std;

char logicalExpressionEvaluation(string S) {
    stack<char> ans;
    for(int i=S.length()-1;i>=0;i--) {
        if(S[i] == '[') {
            vector<char> s;
            while(ans.top() != ']') {
                s.push_back(ans.top());
                ans.pop();
            }
            ans.pop();

            if(s.size() == 3) 
                S[2] == '1' ? ans.push('0') : ans.push('1');
            else if(s.size() == 5) {
                int A = S[0] - 48, B = S[4] - 48, C;
                int C = S[2] == '&' ? A & B : A | B;
                cout<<C<<endl;;
                ans.push((char)C + 48);
            }

        } else {
            ans.push(S[i]);
        }
    }
    return ans.top();
}

int main(void) {
    string str = "[[0,&,1],|,[!,1]]"; 
    cout << logicalExpressionEvaluation(str); 
    
    return 0;
}