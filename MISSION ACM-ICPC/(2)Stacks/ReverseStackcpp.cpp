#include<bits/stdc++.h>
using namespace std;

void BottomInsert(stack<int> &S,int x)
{
	if(S.empty())
		S.push(x);
	else
	{
		int y = S.top();
		S.pop();
		BottomInsert(S,x);
		S.push(y);
	}
}

void reverse(stack<int> &S)
{
	if(!S.empty())
	{
		int x = S.top();
		S.pop();
		reverse(S);
		BottomInsert(S,x);
	}
}

int main(void)
{
	stack<int> S;
	int N = 10;
	for(int i=1;i<=N;i++)
		S.push(i);
	cout<<S.top()<<endl;
	reverse(S);
	cout<<S.top()<<endl;
	return 0;
}