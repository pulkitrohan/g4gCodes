#include<bits/stdc++.h>
using namespace std;

vector<int> maxRodLength(int A[], int N, int M) {
    sort(A, A+N);

    queue<int> Q;
    stack<int> S;
    for(int i=0;i<N;i++)
        S.push(A[i]);

    vector<int> ans;

    while(!S.empty() || !Q.empty()) {
        int val;

        if(Q.empty()) {
            val = S.top();
            ans.push_back(val);
            S.pop();
            val /= 2;

            if(val)
                Q.push(val);
        } else if(S.empty()) {
            val = Q.front();
            ans.push_back(val);
            Q.pop();
            val /= 2;

            if(val)
                Q.push(val);
        } else {
            val = S.top();
            int fr = Q.front();
            if(fr > val) {
                ans.push_back(fr);
                Q.pop();
                fr /= 2;
                if(fr)
                    Q.push(fr);
            } else {
                ans.push_back(val);
                S.pop();
                val /= 2;
                if(val)
                    Q.push(val);
            }
        }
    }
    return ans;
}

int main(void) {
    int N = 5, M = 10;
    int A[] = {6, 5, 9, 10, 12};
    vector<int> ans = maxRodLength(A, N, M);
    int query[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }; 
    int size = sizeof(query) / sizeof(query[0]); 
    for (int i = 0; i < size; i++) 
        cout << ans[query[i] - 1] << " "; 

    return 0;
}