#include<stdio.h>
#include<stdlib.h>

typedef struct Stack
{
    int capacity;
    int top;
    int *elements;
}Stack;

Stack *createStack(int maxElements)
{
    Stack *S;
    S=(Stack *)malloc(sizeof(Stack));
    S->elements = (int *)malloc(sizeof(int)*maxElements);
    S->top = -1;
    S->capacity = maxElements;
    return S;
}
void doublestacksize(Stack *S)
{
    S->capacity *= 2;
    S->elements = realloc(S->elements,S->capacity);
}
void push(Stack *s,int element)
{
    if(s->top == s->capacity-1)
        doublestacksize(s);
    s->elements[++s->top] = element;
}

int pop(Stack *s)
{

    if(s->top == -1)
    {
        printf("Stack Underflow!!\n");
        return;
    }
    else
        return s->elements[s->top--];
}

int top(Stack *s)
{
    if(s->top == -1)
    {
        printf("Stack is Empty\n");
        return ;
    }
    return s->elements[s->top];
}

int main(void)
{
    int n,ch,data;
        printf("Enter the size of Stack : ");
        scanf("%d",&n);
        Stack *S = createStack(n);
        printf("1.Push");
        printf("\n2.Pop");
        printf("\n3.Print Top");
        printf("\n4.Exit");

    label:
    printf("\nEnter your choice:");
    scanf("%d",&ch);
    switch(ch)
    {
        case 1:  printf("\nEnter the element to be Pushed :");
                 scanf("%d",&data);
                 push(S,data);
                 printf("\nElement Pushed");
                 break;
        case 2:  pop(S);
                 printf("\nElement is Popped");
                 break;
        case 3:  printf("\nTop element is : %d",top(S));
                 break;
        case 4:  exit(0);
                 break;
    }
    goto label;
        return 0;
}
