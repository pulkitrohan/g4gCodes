#include<bits/stdc++.h>
using namespace std;

void countEle(stack<int> &S, vector<int> A, int N) {
    set<int> st;
    for(int x : A) {
        if(st.find(x) != st.end())
            cout<<"0 ";
        else {
            int count = 0;
            while(!S.empty() && S.top() != x) {
                st.insert(S.top());
                S.pop();
                count++;
            }
            S.pop();
            count++;
            cout<<count<<" ";
        }
    }
}

int main(void) {
   int N = 5; 
  
    stack<int> s; 
    s.push(1); 
    s.push(2); 
    s.push(3); 
    s.push(4); 
    s.push(6); 
  
    vector<int> a = { 6, 3, 4, 1, 2 }; 
    countEle(s, a, N); 
    return 0;
}