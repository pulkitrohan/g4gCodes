#include<stdio.h>

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
typedef struct Stack
{
	int capacity;
	char *elements;
	int top;
}Stack;
int n = 100;
Stack *CreateStack(int n)
{
	Stack *S = (Stack *)malloc(sizeof(Stack));
	S->top = 0;
	S->capacity = n;
	S->elements = (char *)malloc(sizeof(char)*n);
	return S;
}

void push(Stack *S,char data)
{
		S->elements[++S->top] = data;
}

char pop(Stack *S)
{
	return S->elements[S->top--];
}

char top(Stack *S)
{
	return S->elements[S->top];
}
int priority(char ch)
{
    switch(ch)
    {
        case '(':   return 1;
        case '+':   case '-': return 2;
        case '*':   case '/': return 3;
        case '$':   return 4;
    }
}

int main(void)
{
    Stack *SV = CreateStack(n);
    Stack *SO = CreateStack(n);
    char infix[50],postfix[50],ch,op;
    int i,k=0,op2,op1;
	printf("Enter the Infix Expression : ");
	scanf("%s",infix);
	for(i = 0;infix[i] != '\0';i++)
	{
		ch = infix[i];
		if(ch == '(')
            push(SO,ch);
        else if(isdigit(ch))
            push(SV,ch -'0');
        else if(ch == ')')
        {
            while(top(SO) != '(')
            {
                op = pop(SO);
                op2 = pop(SV);
                op1 = pop(SV);
                switch(op)
                {
                    case '+' : push(SV,op1 + op2);
                    case '-' : push(SV,op1 - op2);
                    case '*' : push(SV,op1 * op2);
                    case '/' : push(SV,op1 / op2);
                    case '$' : push(SV,pow(op1,op2));
                }
            }
            pop(SO);
        }
        else            //Operator
        {
            while(SO->top != 0 && priority(top(SO)) >= priority(ch) )
            {
                op = pop(SO);
                op2 = pop(SV);
                op1 = pop(SV);
                switch(op)
                {
                    case '+' : push(SV,op1 + op2);
                    case '-' : push(SV,op1 - op2);
                    case '*' : push(SV,op1 * op2);
                    case '/' : push(SV,op1 / op2);
                    case '$' : push(SV,pow(op1,op2));
                }
            }
            //push(SO,ch);
        }
    }
    while(SO->top)
    {
                op = pop(SO);
                op2 = pop(SV);
                op1 = pop(SV);
                switch(op)
                {
                    case '+' : push(SV,op1 + op2);
                    case '-' : push(SV,op1 - op2);
                    case '*' : push(SV,op1 * op2);
                    case '/' : push(SV,op1 / op2);
                    case '$' : push(SV,pow(op1,op2));
                }
        SO->top--;
    }
        printf("\nPostfix Evaluation : %d",top(SV));
        return 0;
}
