#include<bits/stdc++.h>
using namespace std;

bool checkSorted(queue<int> &Q) {
	stack<int> S;
	int expected = 1;
	int N = Q.size();
	int frontElement;

	while(!Q.empty()) {
		frontElement = Q.front();
		Q.pop();

		if(frontElement == expected)
			expected++;
		else {
			if(S.empty())
				S.push(frontElement);
			else if(S.top() < frontElement)
				return false;
			else
				S.push(frontElement);
		}
		while(!S.empty() && S.top() == expected) {
			S.pop();
			expected++;
		}
	}
	return expected == N+1 && S.empty();
}

int main(void) {
	queue<int> Q;
	Q.push(5);
	Q.push(1);
	Q.push(2);
	Q.push(3);
	Q.push(4);
	cout<<checkSorted(Q)<<endl;
	return 0;
}