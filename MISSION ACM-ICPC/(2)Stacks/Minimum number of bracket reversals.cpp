#include<bits/stdc++.h>
using namespace std;

int CountMinReversal(string A)
{
	if(A.length() & 1)
		return -1;
	stack<char> S;
	for(int i=0;i<A.length();i++)
	{
		if(A[i] == '{')
			S.push(A[i]);
		else
		{
			if(!S.empty())
			{
				if(S.top() == '{')
					S.pop();
				else
					S.push(A[i]);
			}
			else
				S.push(A[i]);
		}
	}
	int new_len = S.size();
	int closing = 0;
	while(!S.empty() && S.top() == '{')
	{
		S.pop();
		closing++;
	}
	int opening = new_len-closing;
	return ceil(opening/2.0) + ceil(closing/2.0);
}

int main(void)
{
	string S = "}}{{";
	cout<<CountMinReversal(S);
	return 0;
}
}