#include<stdio.h>
#include<stdlib.h>

typedef struct Stack
{
    int capacity;
    int top;
    int *elements;
}Stack;

Stack *createStack(int maxElements)
{
    Stack *S;
    S=(Stack *)malloc(sizeof(Stack));
    S->elements = (int *)malloc(sizeof(int)*maxElements);
    S->top = 0;
    S->capacity = maxElements;
    return S;
}

void push(Stack *s,int element)
{
    if(s->top == s->capacity)
        printf("Stack Overflow!!\n");
    else
        {
            s->elements[++s->top] = element;
         //   printf("\nElement Pushed");
        }
}

int pop(Stack *s)
{

    if(s->top == 0)
        printf("Stack Underflow!!\n");
    else
        {
            return s->elements[s->top--];
         //   printf("\nElement is Popped");
        }
}

void print(Stack *s)
{
    int index = s->top;
    if(s->top == 0)
        printf("Stack is Empty");
    else
    {
        while(index != 0)
    {
        printf("%d ",s->elements[index]);
        index--;
    }
    }
}

int IsEmpty(Stack *S)
{
    return (S->top == 0);
}

void InsertatBottom(Stack *S,int data)
{
    int temp;
    if(IsEmpty(S))
        push(S,data);
    else
    {
        temp = pop(S);
        InsertatBottom(S,data);
        push(S,temp);
    }
}

void Reverse(Stack *S)
{
    int temp;
    if(!IsEmpty(S))
    {
        temp = pop(S);
        Reverse(S);
        InsertatBottom(S,temp);
    }
}


int top(Stack *s)
{
    if(s->top == 0)
    {
        printf("Stack is Empty\n");
        return;
    }
    return s->elements[s->top];
}

int main(void)
{
    int n,ch,data;
        printf("Enter the size of Stack : ");
        scanf("%d",&n);
        Stack *S = createStack(n);
        int i;
        for(i=1;i<=n;i++)
            push(S,i);
        print(S);
        printf("\n");
        Reverse(S);
        print(S);
        return 0;
}

