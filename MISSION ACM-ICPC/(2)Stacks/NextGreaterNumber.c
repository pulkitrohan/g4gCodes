#include<stdio.h>
#include<stdlib.h>

typedef struct Stack
{
    int capacity;
    int top;
    int *elements;
}Stack;

Stack *createStack(int maxElements)
{
    Stack *S;
    S=(Stack *)malloc(sizeof(Stack));
    S->elements = (int *)malloc(sizeof(int)*maxElements);
    S->top = 0;
    S->capacity = maxElements;
    return S;
}

void push(Stack *s,int element)
{
    if(s->top == s->capacity)
        printf("Stack Overflow!!\n");
    else
        {
            s->elements[++s->top] = element;
          //  printf("\nElement Pushed");
        }
}

void pop(Stack *s)
{

    if(s->top == 0)
        printf("Stack Underflow!!\n");
    else
        {
            s->top--;
          //  printf("\nElement is Popped");
        }
}

void print(Stack *s)
{
    int index = s->top;
    if(s->top == 0)
        printf("Stack is Empty");
    else
    {
        while(index != 0)
    {
        printf("%d ",s->elements[index]);
        index--;
    }
    }
}

int top(Stack *s)
{
    if(s->top == 0)
    {
        printf("Stack is Empty\n");
        return;
    }
    return s->elements[s->top];
}

void NextGreaterElement(Stack *S,int *A,int N)
{
    push(S,A[0]);
    int i;
    for(i=1;i<N;i++)
    {
        int next = A[i];
        while(S->top != 0 && top(S) < next)
        {
            printf("%d\t%d\n",top(S),next);
            pop(S);
        }
        push(S,next);
    }
    while(S->top != 0)
    {
        int k = -1;
        printf("%d\t%d\n",top(S),k);
        pop(S);
    }
}

int main(void)
{
    int n,ch,data;
        printf("Enter the size : ");
        scanf("%d",&n);
        Stack *S = createStack(n);
    printf("Enter the array\n");
    int A[n+1],i;
    for(i=0;i<n;i++)
        scanf("%d",&A[i]);
    NextGreaterElement(S,A,n);
    return 0;
}

