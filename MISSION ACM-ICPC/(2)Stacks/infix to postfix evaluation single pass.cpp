#include<cstdio>
#include<stack>
#include<cctype>
using namespace std;

int priority(char ch)
{
    switch(ch)
    {
        case '(':   return 1;
        case '+':   case '-': return 2;
        case '*':   case '/': return 3;
        case '$':   return 4;
    }
}

int main(void)
{
    stack<int> myoperand;
    stack<char> myoperator;
    char infix[100];
    int i;
    char ch;
    scanf("%s",infix);
    for(i=0;infix[i] != '\0';i++)
    {
        ch = infix[i];
        if(isdigit(ch))
            myoperand.push(ch-'0');
        else if(ch == '(')
            myoperator.push(ch);
        else if(ch == ')')
        {
            while(!myoperator.empty() && myoperator.top() != '(')
            {
	
                char op = myoperator.top();
                myoperator.pop();
                int op2 = myoperand.top();
                myoperand.pop();
                int op1 = myoperand.top();
                myoperand.pop();
                switch(op)
                {
                    case '+' : myoperand.push(op1 + op2); break;
                    case '-' : myoperand.push(op1 - op2); break;
                    case '*' : myoperand.push(op1 * op2); break;
                    case '/' : myoperand.push(op1 / op2); break;
                }
            }
			if(myoperator.empty())
				return -1;
            myoperator.pop();
        }
        else
        {
            while(!myoperator.empty() && priority(myoperator.top()) >= priority(ch))
            {
                char op = myoperator.top();
                myoperator.pop();
                int op2 = myoperand.top();
                myoperand.pop();
                int op1 = myoperand.top();
                myoperand.pop();
                switch(op)
                {
                    case '+' : myoperand.push(op1 + op2); break;
                    case '-' : myoperand.push(op1 - op2); break;
                    case '*' : myoperand.push(op1 * op2); break;
                    case '/' : myoperand.push(op1 / op2); break;
                }
            }
            myoperator.push(ch);
        }
    }
    while(!myoperator.empty())
    {
        char op = myoperator.top();
        myoperator.pop();
        int op2 = myoperand.top();
        myoperand.pop();
        int op1 = myoperand.top();
        myoperand.pop();
        switch(op)
        {
            case '+' : myoperand.push(op1 + op2); break;
            case '-' : myoperand.push(op1 - op2); break;
            case '*' : myoperand.push(op1 * op2); break;
            case '/' : myoperand.push(op1 / op2); break;
        }
    }
    printf("Result : %d",myoperand.top());
    return 0;
}
