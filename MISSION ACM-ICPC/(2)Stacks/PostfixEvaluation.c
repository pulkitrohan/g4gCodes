#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
typedef struct Stack
{
	int capacity;
	int *elements;
	int top;
}Stack;
int n = 100;
Stack *CreateStack(int n)
{
	Stack *S = (Stack *)malloc(sizeof(Stack));
	S->top = 0;
	S->capacity = n;
	S->elements = (int *)malloc(sizeof(int)*n);
	return S;
}

void push(Stack *S,int data)
{
		S->elements[++S->top] = data;
}

int pop(Stack *S)
{
	return S->elements[S->top--];
}

int top(Stack *S)
{
	return S->elements[S->top];
}

int main(void)
{
    Stack *S = CreateStack(n);
    char postfix[50],ch;
    int i,op1,op2;
	printf("Enter the Postfix Expression : ");
	scanf("%s",postfix);

	for(i = 0;i<strlen(postfix);i++)
	{
	    ch = postfix[i];
	    if(isdigit(ch))
            push(S,ch-'0');
        else
        {
            op2 = pop(S);
            op1 = pop(S);
            switch(ch)
            {
                case '+' : push(S,op1 + op2); break;
                case '-' : push(S,op1 - op2); break;
                case '*' : push(S,op1 * op2); break;
                case '/' : push(S,op1 / op2); break;
                case '$' : push(S,pow(op1,op2)); break;
            }
        }
	}
        printf("\nResult : %d",top(S));
        return 0;
}
