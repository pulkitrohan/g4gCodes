#include<bits/stdc++.h>
using namespace std;

struct DLL
{
	struct DLL *next,*prev;
	int data;
};

struct Stack
{
	struct DLL *head;
	struct DLL *mid;
	int count;
};

Stack *createStack()
{
	Stack *S = (Stack *)malloc(sizeof(Stack));
	S->head = S->mid = NULL;
	S->count = 0;
	return S;
}

void push(Stack *S,int data)
{
	struct DLL *temp = (struct DLL *)malloc(sizeof(struct DLL));
	temp->data = data;
	temp->prev = NULL;
	temp->next = NULL;
	S->count++;
	if(!S->head)
	{
		S->head = temp;
		S->mid = temp;
	}
	else
	{
		temp->next = S->head;
		S->head->prev = temp;
		S->head = temp;
		if(S->count & 1)
			S->mid = S->mid->prev;
		
	}
}

int pop(Stack *S)
{
	if(S->count == 0)
		return -1;
	int item = S->head->data;
	struct DLL *temp = S->head;
	S->head = S->head->next;
	free(temp);
	if(S->head)
		S->head->prev = NULL;
	S->count--;
	if(S->count && S->count % 2 == 0)
		S->mid = S->mid->next;
	return item;
}

int findMiddle(Stack *S)
{
	if(S->count == 0)
		return -1;
	return S->mid->data;
}
		

int main(void)
{
	Stack *S = createStack();
	push(S,11);
	push(S,22);
	push(S,33);
	push(S,44);
	push(S,55);
	push(S,66);
	push(S,77);
	cout<<pop(S)<<endl;
	cout<<pop(S)<<endl;
	cout<<findMiddle(S)<<endl;
	return 0;
}