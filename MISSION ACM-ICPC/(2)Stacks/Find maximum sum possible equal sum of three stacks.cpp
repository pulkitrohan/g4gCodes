#include<bits/stdc++.h>
using namespace std;
#define vi vector<int>

int maxSum(vector<int> &A, vector<int> &B, vector<int> &C) {
	
	int sum1 = 0, sum2 = 0, sum3 = 0;
	for(auto x : A) 
		sum1 += x;
	for(auto x : B)
		sum2 += x;
	for(auto x : C)
		sum3 += x;

	int top1 = 0, top2 = 0, top3 = 0;
	int ans = 0;

	while(1) {

		if(top1 == A.size() || top2 == B.size() || top3 == C.size())
			return 0;

		if(sum1 == sum2 && sum2 == sum3) 
			return sum1;

		if(sum1 >= sum2 && sum1 >= sum3) 
			sum1 -= A[top1++];

		else if(sum2 >= sum1 && sum2 >= sum3)
			sum2 -= B[top2++];

		else if(sum3 >= sum1 && sum3 >= sum2)
			sum3 -= C[top3++];
	}
}

int main(void) {
	
	vi A = {3,2,1,1,1};
	vi B = {4,3,2};
	vi C = {1,1,4,1};

	cout<<maxSum(A, B, C);
	return 0;
}