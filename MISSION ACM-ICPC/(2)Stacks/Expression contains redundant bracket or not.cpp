#include<bits/stdc++.h>
using namespace std;

bool findRedundant(string S) {
	stack<char> st;

	for(auto &ch : S) {
		if(ch == ')') {
			char top = st.top();
			st.pop();

			bool flag = true;
			while(top != '(') {
				if(top == '+' || top == '-' || top == '*' || top == '/')
					flag = false;
				top = st.top();
				st.pop();
			}
			if(flag == true)
				return true;
		} else {
			st.push(ch);
		}
	}
	return false;
}


int main(void) {
    string str = "((a+b))"; 
    cout<<findRedundant(str)<<endl;
  
    str = "(a+(b)/c)"; 
    cout<<findRedundant(str)<<endl;
  
    str = "(a+b*(c-d))"; 
    cout<<findRedundant(str)<<endl;

    return 0;
}