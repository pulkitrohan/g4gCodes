#include<stdio.h>
#include<stdlib.h>
#include<string.h>
typedef struct Stack
{
	int capacity;
	char *elements;
	int top;
}Stack;
int n = 100;
Stack *CreateStack(int n)
{
	Stack *S = (Stack *)malloc(sizeof(Stack));
	S->top = 0;
	S->capacity = n;
	S->elements = (char *)malloc(sizeof(char)*n);
	return S;
}

void push(Stack *S,char data)
{
		S->elements[++S->top] = data;
}

char pop(Stack *S)
{
	return S->elements[S->top--];
}

char top(Stack *S)
{
	return S->elements[S->top];
}
int priority(char ch)
{
    switch(ch)
    {
        case '(':   return 1;
        case '+':   case '-': return 2;
        case '*':   case '/': return 3;
        case '$':   return 4;
    }
}

int main(void)
{
    Stack *S = CreateStack(n);
    char infix[50],postfix[50],ch;
    int i,k=0;
	printf("Enter the Infix Expression : ");
	scanf("%s",infix);
	//push(S,'#');
	for(i = 0;infix[i] != '\0';i++)
	{
		ch = infix[i];
		if(ch == '(')
            push(S,ch);
        else if(isalnum(ch))
            postfix[k++] = ch;
        else if(ch == ')')
        {
            while(top(S) != '(')
                    postfix[k++] = pop(S);
            pop(S);
        }
        else            //Operator
        {
            while(S->top != 0 && priority(top(S)) >= priority(ch))
                postfix[k++] = pop(S);
            push(S,ch);
        }
    }
    while(S->top >= 0)
    {
        postfix[k++] = pop(S);
        S->top--;
    }
        postfix[k] = '\0';
        printf("\nPostfix Expression : %s",postfix);
        return 0;
}
