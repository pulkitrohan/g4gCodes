#include<stdio.h>
#include<stdlib.h>

typedef struct Stack
{
    int capacity;
    int top;
    int *elements;
}Stack;

Stack *createStack(int maxElements)
{
    Stack *S;
    S=(Stack *)malloc(sizeof(Stack));
    S->elements = (int *)malloc(sizeof(int)*maxElements);
    S->top = 0;
    S->capacity = maxElements;
    return S;
}

void push(Stack *s,int element)
{
    if(s->top == s->capacity)
        printf("Stack Overflow!!\n");
    else
        {
            s->elements[++s->top] = element;
           // printf("\nElement Pushed");
        }
}

void pop(Stack *s)
{

    if(s->top == 0)
        printf("Stack Underflow!!\n");
    else
        {
            s->top--;
          //  printf("\nElement is Popped");
        }
}

void print(Stack *s)
{
    int index = s->top;
    if(s->top == 0)
        printf("Stack is Empty");
    else
    {
        while(index != 0)
    {
        printf("%d ",s->elements[index]);
        index--;
    }
    }
}

int top(Stack *s)
{

    return s->elements[s->top];
}

int IsEmpty(Stack *S)
{
    return (S->top == 0);
}

/*Algorithm:
1)Create a Stack.
2)Loop around the input array.If the array is empty then set P = -1
3)while stack is not empty and top of stack containing index of A has value less than A[i],then pop the stack
4)else Set P equal to top of stack
5)Set span for index i  equal to i - P
*/
void FindSpan(int *A,int *S,int N)
{
    Stack *Stack = createStack(N);
    int P,i;
    for(i=0;i<N;i++)
    {
        while(!IsEmpty(Stack) && A[i] > A[top(Stack)])
            pop(Stack);
        if(IsEmpty(Stack))
            P = -1;
        else
            P = top(Stack);
        S[i] = i - P;
        push(Stack,i);
    }
}

int main(void)
{
    int i,N;
    printf("Enter N : ");
    scanf("%d",&N);
    int A[N+1],S[N+1];
    printf("Enter the elements : ");
    for(i=0;i<N;i++)
        scanf("%d",&A[i]);
    FindSpan(A,S,N);
    printf("Day \tA[i] \tS[i]");
    for(i=0;i<N;i++)
        printf("\n%d\t%d\t%d",i,A[i],S[i]);
    return 0;
}

