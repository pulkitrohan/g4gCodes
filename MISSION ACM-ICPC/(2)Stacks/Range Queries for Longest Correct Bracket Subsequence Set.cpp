#include<bits/stdc++.h>
using namespace std;

void constructBalanceArray(string S, int BOP[], int BCP[], int N) {
    stack<int> st;
    for(int i=0;i<N;i++) {
        if(S[i] == '(')
            st.push(i);
        else {
            if(!st.empty()) {
                BCP[i] = 1;
                BOP[st.top()] = 1;
                st.pop();
            } else
                BCP[i] = 0;
        }
    }
    for(int i=1;i<N;i++) {
        BOP[i] += BOP[i-1];
        BCP[i] += BCP[i-1];
    }
}

int getQueryAns(int BOP[], int BCP[], int s, int e) {
    if (BOP[s - 1] == BOP[s]) { 
        return (BCP[e] - BOP[s]) * 2; 
    } 
  
    else { 
        return (BCP[e] - BOP[s] + 1) * 2; 
    } 
}

int main(void) {
    string S = "())(())(())(";
    int N = S.length();
    int BCP[N+1] = {0}, BOP[N+1] = {0};
    constructBalanceArray(S, BOP, BCP, N);
    cout<<getQueryAns(BOP, BCP, 5, 11);
    return 0;
}