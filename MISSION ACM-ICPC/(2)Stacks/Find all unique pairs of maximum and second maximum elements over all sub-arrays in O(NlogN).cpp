#include<bits/stdc++.h>
using namespace std;

set<pair<int, int> > pairs(vector<int> &V) {
	set<pair<int, int> > st;
	stack<int> S;

	if(V.empty())
		return st;

	for(auto x : V) {
		while(!S.empty() && x > S.top()) {
			st.insert(make_pair(S.top(), x));
			S.pop();
		}
		if(!S.empty()) {
			st.insert(make_pair(x, S.top()));
		}
		S.push(x);
	}
	return st;
}

int main(void) {
	vector<int> V = {1, 2, 6, 4, 5};
	set<pair<int, int> > st = pairs(V);
	for(auto &x: st)
		cout<<"( "<<x.first<<", "<<x.second<<") "<<endl;
	return 0;
}