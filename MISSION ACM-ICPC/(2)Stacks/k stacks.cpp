#include<bits/stdc++.h>
using namespace std;

struct Stack
{
	int *A,*top,*next;
	int N,K,free;
};

Stack *createStack(int k,int N)
{
	Stack *S = (Stack *)malloc(sizeof(Stack));
	S->A = (int *)malloc(sizeof(int)*N);
	S->top = (int *)malloc(sizeof(int)*k);
	S->next = (int *)malloc(sizeof(int)*N);
	S->N = N;
	S->K = k;
	S->free = 0;
	for(int i=0;i<k;i++)
		S->top[i] = -1;
	for(int i=0;i<N-1;i++)
		S->next[i] = i+1;
	S->next[N-1] = -1;
	return S;
}

void push(Stack *S,int data,int k)
{
	if(S->free == -1)
		return;
	int i = S->free;
	S->free = S->next[i];
	S->next[i] = S->top[k];
	S->top[k] = i;
	S->A[i] = data;
}

int pop(Stack *S,int k)
{
	if(S->top[k] == -1)
		return INT_MAX;
	int i = S->top[k];
	S->top[k] = S->next[i];
	S->next[i] = S->free;
	S->free = i;
	return S->A[i];
}

int main(void)
{
	Stack *S = createStack(3,10);
	push(S,15,2);
	push(S,45,2);
	push(S,17,1);
	push(S,49,1);
	push(S,39,1);
	push(S,11,0);
	push(S,9,0);
	push(S,7,0);
	cout<<pop(S,2)<<endl;
	cout<<pop(S,1)<<endl;
	cout<<pop(S,0)<<endl;
	return 0;
}
	