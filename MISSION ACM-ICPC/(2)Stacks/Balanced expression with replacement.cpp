#include<bits/stdc++.h>
using namespace std;

bool isMatching(char a, char b) 
{ 
    return (a == '{' && b == '}') ||
    	   (a == '[' && b == ']') ||
    	   (a == '(' && b == ')') ||
    	   (a == 'X');
} 

bool isBalanced(string S, stack<char> st, int index) {
	if(index == S.length())
		return st.empty();

	char ch = S[index];
	if(ch == '{' || ch == '(' || ch == '[') {
		st.push(ch);
		return isBalanced(S, st, index+1);
	} else if(ch == '}' || ch == ')' || ch == ']') {
		if(st.empty())
			return 0;

		char topElement = st.top();
		st.pop();

		return isMatching(topElement, ch) && isBalanced(S, st, index+1);
	} else if(ch == 'X') {
		stack<char> temp = st;
		temp.push(ch);
		if(isBalanced(S, temp, index+1))
			return true;
		if(st.empty())
			return false;
		st.pop();
		return isBalanced(S, st, index+1);
	}
}

int main(void) {
	string S = "{(X}[]";
  	stack<char> st;
    cout<<isBalanced(S, st, 0); 
    return 0;
}