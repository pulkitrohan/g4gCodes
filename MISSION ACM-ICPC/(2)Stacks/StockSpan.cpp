#include<bits/stdc++.h>
using namespace std;

int main(void)
{
	int N;
	scanf("%d",&N);
	int price[N+1],span[N+1];
	for(int i=0;i<N;i++)
		scanf("%d",&price[i]);
	stack<int> S;
	for(int i=0;i<N;i++)
	{
		while(!S.empty() && price[i] >= price[S.top()])
			S.pop();
		if(S.empty())
			span[i] = i+1;
		else
			span[i] = i - S.top();
		S.push(i);
	}
	for(int i=0;i<N;i++)
		printf("%d ",span[i]);
	return 0;
}
