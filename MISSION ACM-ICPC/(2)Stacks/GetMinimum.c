#include<stdio.h>
#include<stdlib.h>

typedef struct Stack
{
    int capacity;
    int top;
    int *elements;
}Stack;

int min_stack[100] = {0};
int j=0;

Stack *createStack(int maxElements)
{
    Stack *S;
    S=(Stack *)malloc(sizeof(Stack));
    S->elements = (int *)malloc(sizeof(int)*maxElements);
    S->top = 0;
    S->capacity = maxElements;
    return S;
}

void push(Stack *s,int element)
{
    if(s->top == s->capacity)
        printf("Stack Overflow!!\n");
    else
        {
            s->elements[++s->top] = element;
            if(j == 0 || min_stack[j] >= element)
				min_stack[++j] = element;
        }
}

void pop(Stack *s)
{

    if(s->top == 0)
        printf("Stack Underflow!!\n");
    else
        {
            int data = s->elements[s->top--];
            if(min_stack[j] == data)
				j--;
        }
}

void print(Stack *s)
{
    int index = s->top;
    if(s->top == 0)
        printf("Stack is Empty");
    else
    {
        while(index != 0)
    {
        printf("%d ",s->elements[index]);
        index--;
    }
    }
}

int GetMinimum()
{
	return min_stack[j];
}

int top(Stack *s)
{
    if(s->top == 0)
    {
        printf("Stack is Empty\n");
        return;
    }
    return s->elements[s->top];
}

int main(void)
{
    int n,ch,data;
        printf("Enter the size of Stack : ");
        scanf("%d",&n);
        Stack *S = createStack(n);
    label:
        printf("\n1.Push");
        printf("\n2.Pop");
        printf("\n3.Print Top");
        printf("\n4.Print Minimum Top");
        printf("\n5.Exit");

    printf("\nEnter your choice:");
    scanf("%d",&ch);
    switch(ch)
    {
        case 1:  printf("\nEnter the element to be Pushed :");
                 scanf("%d",&data);
                 push(S,data);
                 break;
        case 2:  pop(S);
                 break;
        case 3:  printf("\nTop element is : %d",top(S));
                 break;
        case 4:  printf("\nTop element is : %d",GetMinimum());
                 break;
        case 5:  exit(0);
                 break;
    }
    goto label;
        return 0;
}
