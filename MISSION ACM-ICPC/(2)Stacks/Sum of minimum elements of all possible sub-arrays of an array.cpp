#include<bits/stdc++.h>
using namespace std;

int findMaxSum(int A[], int N) {
	int left[N] = {0}, right[N] = {0};
	stack<int> S;

	for(int i=0;i<N;i++) {
		while(!S.empty() && A[S.top()] >= A[i]) {
			left[i] += left[S.top()] + 1;
			S.pop();
		}
		S.push(i);
	}
	while(!S.empty())
		S.pop();

	for(int i=N-1;i>=0;i--) {
		while(!S.empty() && A[S.top()] > A[i]) {
			right[i] += right[S.top()] + 1;
			S.pop();
		}
		S.push(i);
	}
	int ans = 0;
	for(int i=0;i<N;i++)
		ans += (left[i] + 1) * (right[i] + 1) * A[i];

	return ans;
}

int main(void) {
	int A[] = {1, 3, 2};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<findMaxSum(A, N);
}