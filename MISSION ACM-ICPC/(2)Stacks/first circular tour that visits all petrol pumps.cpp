#include<stdio.h>

struct PetrolPump
{
    int petrol;
    int dist;
};

int FirstPetrolPump(struct PetrolPump *A,int N)
{
    int start = 0,total = 0,cur = 0;
	for(int i=0;i<N;i++)
	{
		total += A[i].petrol - A[i].dist;
		cur += A[i].petrol - A[i].dist;
		if(cur < 0)
		{
			cur = 0;
			start = i+1;
		}
	}
	if(total < 0)
		return -1;
	return start;
}

int main(void)
{
    struct PetrolPump Arr[] = {{4,6},{6,5},{7,3},{4,5}};
    int N = sizeof(Arr)/sizeof(Arr[0]);
    int start = FirstPetrolPump(Arr,N);
    if(start == -1)
        printf("No such path possible\n");
    else
        printf("%d ",start);
    return 0;
}
