//http://algophilic.blogspot.in/2012/08/sort-stack-in-ascendingdescending-order.html

#include<bits/stdc++.h>
using namespace std;

void Insert(stack<int> &S,int x)
{
    if(S.empty() || x < S.top())
        S.push(x);
    else
    {
        int y = S.top();
        S.pop();
        Insert(S,x);
        S.push(y);
    }
}

void sortRecursion(stack<int> &S)
{
    if(!S.empty())
    {
        int x = S.top();
        S.pop();
        sortRecursion(S);
        Insert(S,x);
    }
}

void sortNonRecursive(stack<int> &S1)
{
    stack<int> S2;
    while(!S1.empty())
	{
		int x = S1.top();
		S1.pop();
		while(!S2.empty() && x > S2.top())
		{
			S1.push(S2.top());
			S2.pop();
		}
		S2.push(x);
	}
	while(!S2.empty())
	{
		printf("%d\n",S2.top());
		S2.pop();
	}
}

int main(void)
{
	int N,x;
	scanf("%d",&N);
	stack<int> S1;
	for(int i=0;i<N;i++)
	{
		scanf("%d",&x);
		S1.push(x);
	}
    sortRecursion(S1);
    while(!S1.empty())
    {
        printf("%d ",S1.top());
        S1.pop();
    }
	return 0;
}
