#include<bits/stdc++.h>
using namespace std;
#define N 3

void findPrefixCount(int p_arr[][N], bool set_bit[][N]) {
	for(int i=0;i<N;i++) {
		for(int j=N-1;j>=0;j--) {
			if(set_bit[i][j])
				continue;
			if(j != N-1) 
				p_arr[i][j] += p_arr[i][j+1];
			p_arr[i][j] += (int)(!set_bit[i][j]);
		}
	}
}

int maxtrixOrValueOne(bool set_bit[][N]) {
	int p_arr[N][N] = {0};

	findPrefixCount(p_arr, set_bit);
	int count_zero_submatrices = 0;
	for(int j=0;j<N;j++) {
		int i = N-1;
		stack<pair<int, int> > S;
		int to_sum = 0;
		while(i >= 0) {
			int c = 0;
			while(S.size() != 0 && S.top().first > p_arr[i][j]) {
				to_sum -= (S.top().second + 1) * (S.top().first - p_arr[i][j]);
				c += S.top().second + 1;
				S.pop();
			}
			to_sum += p_arr[i][j];
			count_zero_submatrices += to_sum;
			S.push({ p_arr[i][j], c });
			i--;
		}
	}
	return (N * (N+1) * N * (N+1))/4 - count_zero_submatrices;
}

int main(void) {
	bool A[][N] = { { 0, 0, 0 }, 
                      { 0, 1, 0 }, 
                      { 0, 0, 0 }	
				 };
	cout<<maxtrixOrValueOne(A);
	return 0;
}