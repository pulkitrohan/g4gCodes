#include<stdio.h>
#include<stdlib.h>

#define MAX 100
typedef struct Stack
{
    char *elements;
    int top;
}stack;


stack *CreateStack(int N)
{
    stack *S = (stack *)malloc(sizeof(stack));
    S->elements = (char *)malloc(sizeof(char) * N);
    S->top = 0;
    return S;
}

void push(stack *S,char data)
{
    if(S->top == MAX)
        printf("Stack Overflow\n");
    else
        S->elements[++S->top] = data;
}

char pop(stack *S)
{
    if(S->top == 0)
        return 0;
    else
        return S->elements[S->top--];
}

char top(stack *S)
{
    if(S->top == 0)
        return NULL;
    else
        return S->elements[S->top];
}

int isMatching(char ch1,char ch2)
{
    if(ch1 == '(' && ch2 == ')')
        return 1;
    else if(ch1 == '{' && ch2 == '}')
        return 1;
    else if(ch1 == '[' && ch2 == ']')
        return 1;
    else
        return 0;
}

int main(void)
{
    stack *S = CreateStack(MAX);
    char input[100];
    int i;
    scanf("%s",input);
    for(i=0;input[i] != '\0';i++)
    {
        char ch = input[i];
        if(ch == '(' || ch == '{' || ch == '[')
            push(S,ch);
        else if(ch == ')' || ch == ')' || ch == ']')
        {
            if(isMatching(ch,top(S)))
                pop(S);
            else
            {
                printf("Expression is not balanced\n");
                return -1;
            }
        }
    }
    if(!top(S))
        printf("Expression is balanced\n");
    else
        printf("Expression is not balanced\n");
    return 0;
}
