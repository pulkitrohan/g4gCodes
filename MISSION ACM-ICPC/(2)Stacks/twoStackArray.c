#include<stdio.h>
#include<stdlib.h>

typedef struct Stack
{
    int capacity;
    int top1;
    int top2;
    int *elements;
}Stack;

Stack *createStack(int maxElements)
{
    Stack *S;
    S=(Stack *)malloc(sizeof(Stack));
    S->elements = (int *)malloc(sizeof(int)*maxElements);
    S->top1 = -1;
    S->top2 = maxElements;
    S->capacity = maxElements;
    return S;
}

void pushstack1(Stack *s,int element)
{
    if(s->top1 + 1 < s->top2)
        s->elements[++s->top1] = element;
    else
        printf("Stack Overflow\n");
}

void pushstack2(Stack *S,int element)
{
    if(S->top1+1 < S->top2)
        S->elements[--S->top2] = element;
    else
        printf("Stack Overflow\n");
}


int  popstack1(Stack *s)
{
    if(s->top1 == -1)
        printf("Stack Underflow!!\n");
    else
        return s->elements[s->top1--];
}

int popstack2(Stack *s)
{

    if(s->top2 == s->capacity)
        printf("Stack Underflow!!\n");
    else
        return s->elements[s->top2++];
}

/*void print(Stack *s)
{
    int index = s->top;
    if(s->top == 0)
        printf("Stack is Empty");
    else
    {
        while(index != 0)
    {
        printf("%d ",s->elements[index]);
        index--;
    }
    }
}

int top(Stack *s)
{
    if(s->top == 0)
    {
        printf("Stack is Empty\n");
        return;
    }
    return s->elements[s->top];
}
*/
int main(void)
{
    int n,ch,data;
        printf("Enter the size of Stack : ");
        scanf("%d",&n);
        Stack *S = createStack(n);
        pushstack1(S,5);
        pushstack2(S,10);
        pushstack2(S,15);
        pushstack1(S,11);
        pushstack2(S,7);
        printf("Popped element from stack1 is %d\n",popstack1(S));
        pushstack2(S,40);
        printf("Popped element from stack2 is %d\n",popstack2(S));
        return 0;
}

