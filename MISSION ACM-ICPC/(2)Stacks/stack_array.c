#include<stdio.h>
#include<stdlib.h>

struct Stack
{
    int capacity;
    int top;
    int *elements;
};

Stack *createStack(int maxElements)
{
    Stack *S = (Stack *)malloc(sizeof(Stack));
    S->elements = (int *)malloc(4*maxElements);
    S->top = -1;
    S->capacity = maxElements;
    return S;
}

void push(Stack *s,int element)
{
    if(s->top == s->capacity)
        printf("Stack Overflow!!\n");
    else
    {
        s->elements[++s->top] = element;
        printf("\nElement Pushed");
    }
}

int pop(Stack *s)
{

    if(s->top == -1)
    {
        printf("Stack Underflow!!\n");
        return ;
    }
    else
        return s->elements[s->top--];
}

void print(Stack *s)
{
    int index = s->top;
    if(s->top == -1)
        printf("Stack is Empty");
    else
    {
        while(index != -1)
        {
            printf("%d ",s->elements[index]);
            index--;
        }
    }
}

int top(Stack *s)
{
    if(s->top == -1)
    {
        printf("Stack is Empty\n");
        return;
    }
    return s->elements[s->top];
}

int main(void)
{
    int n,ch,data;
	cout<<"Enter the size of Stack : ";
	cin>>n;
	Stack *S = createStack(n);
    while(1)
	{
		cout<<"\n1.Push";
		cout<<"\n2.Pop";
		cout<<"\n3.Print stack top element";
		cout<<"\n4.Exit";
		cout<<"\nEnter your choice : ";
		cin>>ch;
		switch(ch)
		{
			case 1:  cout<<"Enter the element to be inserted in stack : ";
					 cin>>data;
					 push(S,data);
					 break;
			case 2:  cout<<"Element removed from stack : "<<pop(S)<<endl;
					 break;
			case 3:  cout<<"Top element on the stack : "<<top(S)<<endl;
					 break;
			case 4:  exit(0);
					 break;
	  case default:	 cout<<"Wrong choice entered.Try Again!";
					 break;
		}
	}
	return 0;
}
