#include<bits/stdc++.h>
using namespace std;

string removeKCharacters(string S, int k) {
    stack<pair<int, int> > st;
    for(int i=0;i<S.length();i++) {
        if(!st.empty() && st.top().second == k) {
            char ch = st.top().first;
            while(!st.empty() && st.top().first == ch)
                st.pop();
        }
        if(!st.empty() && st.top().first == S[i]) {
            pair<int, int> P = st.top();
            st.push(make_pair(P.first, P.second + 1));
        }
        else {
            st.push(make_pair(S[i], 1));
        }
    }
    if(!st.empty() && st.top().second == k) {
        char ch = st.top().first;
        while(!st.empty() && st.top().first == ch)
            st.pop();
    }
    string ans = "";
    while(!st.empty()) {
        ans += st.top().first, st.pop();
    }
    reverse(ans.begin(), ans.end());
    return ans.c_str();
}

int main(void) {
    string S = "geeksforgeeks";
    int k = 2;
    cout<<removeKCharacters(S, k);
    return 0;
}