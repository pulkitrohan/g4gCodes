#include<stdio.h>
#include<stdlib.h>

struct node
{
    int data;
    struct node *next;
};

void push(node *S,int data)   //Same as insertion at beginning
{
		node *temp;
        temp= (node *)malloc(sizeof(node));
        temp->next = S->next;   //S->next = NULL
        temp->data = data;
        S->next = temp;
}
int pop(node *S)
{
    if(S->next == NULL)
        printf("\nUnderflow!!");
    else
    {
        node *temp;
        temp = S->next;
        S->next = temp->next;
        return temp->data;
    }
}

int top(node *S)
{
    return S->next->data;
}


int main(void)
{
    int n,ch,data;
    node *S = NULL;
    while(1)
	{
		cout<<"\n1.Push";
		cout<<"\n2.Pop";
		cout<<"\n3.Print stack top element";
		cout<<"\n4.Exit";
		cout<<"\nEnter your choice : ";
		cin>>ch;
		switch(ch)
		{
			case 1:  cout<<"Enter the element to be inserted in stack : ";
					 cin>>data;
					 push(S,data);
					 break;
			case 2:  pop(S)<<endl;
					 break;
			case 3:  cout<<"Top element on the stack : "<<top(S)<<endl;
					 break;
			case 4:  exit(0);
					 break;
	  case default:	 cout<<"Wrong choice entered.Try Again!";
					 break;
		}
	}
    return 0;
}
