http://tech-queries.blogspot.in/2011/03/maximum-area-rectangle-in-histogram.html

#include<bits/stdc++.h>
using namespace std;

int getMaxArea(int *A,int N)
{
	stack<int> S;
	int area[N],t;
	memset(area,0,sizeof(area));
	for(int i=0;i<N;i++)
	{
		while(!S.empty() && A[i] <= A[S.top()])
			S.pop();
		if(S.empty())
			area[i] = i;
		else
			area[i] = i-S.top()-1;
		S.push(i);
	}
	while(!S.empty())
		S.pop();
		
	for(int i=N-1;i>=0;i--)
	{
		while(!S.empty() && A[i] <= A[S.top()])
			S.pop();
		if(S.empty())
			area[i] += N-i-1;
		else
			area[i] += S.top()-i-1;
		S.push(i);
	}
	int max_area = 0;
	for(int i=0;i<N;i++)
	{
		area[i] = A[i] *(area[i]+1);
		max_area = max(max_area,area[i]);
	}
	return max_area;
}

int main(void)
{
	int A[] = {6,2,5,4,5,1,6};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<getMaxArea(A,N);
	return 0;
}