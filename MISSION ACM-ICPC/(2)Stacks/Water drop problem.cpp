#include<bits/stdc++.h>
using namespace std;

int drops(int length, int position[], int speed[], int N) {
    vector<pair<int, double> > V;
    for(int i=0;i<N;i++)
        V.push_back({ position[i], (length-position[i]) * 1.0/speed[i] });
    sort(V.begin(), V.end());
    int ans = 0;
    stack<double> S;
    for(int i=N-1;i>=0;i--) {
        if(S.empty())
            S.push(V[i].second);
        if(V[i].second > S.top()) {
            S.pop();
            ans++;
            S.push(V[i].second);
        }
    }
    if(!S.empty()) {
        S.pop();
        ans++;
    }
    return ans;
}

int main(void) {
    int length = 12; // length of pipe 
    int position[] = { 10, 8, 0, 5, 3 }; // position of droplets 
    int speed[] = { 2, 4, 1, 1, 3 }; // speed of each droplets 
    int n = sizeof(speed)/sizeof(speed[0]); 
    cout << drops(length, position, speed, n); 

    return 0;
}