#include<stdio.h>
#include<stdlib.h>
#include<string.h>
typedef struct Stack
{
	int capacity;
	char *elements;
	int top;
}Stack;
int n = 100;
Stack *CreateStack(int n)
{
	Stack *S = (Stack *)malloc(sizeof(Stack));
	S->top = 0;
	S->capacity = n;
	S->elements = (char *)malloc(sizeof(char)*n);
	return S;
}

void push(Stack *S,char data)
{
		S->elements[++S->top] = data;
}

char pop(Stack *S)
{
	return S->elements[S->top--];
}

char top(Stack *S)
{
	return S->elements[S->top];
}
int priority(char ch)
{
    switch(ch)
    {

        case ')':   return 1;
        case '+':   case '-': return 2;
        case '*':   case '/': return 3;
        case '$':   return 4;
    }
}

int main(void)
{
    Stack *S = CreateStack(n);
    char infix[50],prefix[50],ch;
    int i,k=0,j=0;
	printf("Enter the Infix Expression : ");
	scanf("%s",infix);
	strrev(infix);
	push(S,'#');
	for(i = 0;infix[i] != '\0';i++)
	{
		ch = infix[i];
		if(ch == ')')
            push(S,ch);
        else if(isalnum(ch))
            prefix[k++] = ch;
        else if(ch == '(')
        {
            while(top(S) != ')')
                    prefix[k++] = pop(S);
            pop(S);
        }
        else            //Operator
        {
            while(priority(top(S)) > priority(ch) && S->top != 0)
                prefix[k++] = pop(S);
            push(S,ch);
        }
    }
    while(top(S) != '#')
    {
        prefix[k++] = pop(S);
    }
    prefix[k] = '\0';
    strrev(prefix);
    printf("Prefix Expression : %s",prefix);
    return 0;
}
