#include<stdio.h>

void FindSpan(int *A,int N,int *S)
{
    int i,j;
    for(i=0;i<N;i++)
    {
        j = 1;
        while(j <= i && A[i] >= A[i-j])
            j++;
        S[i] = j;
    }
}

int main(void)
{
    int N,i;
    printf("Enter N : ");
    scanf("%d",&N);
    int A[N+1],S[N+1];
    printf("Enter input Array : ");
    for(i=0;i<N;i++)
        scanf("%d",&A[i]);
    FindSpan(A,N,S);
    printf("Day\tA[i]\tS[i]");
    for(i=0;i<N;i++)
        printf("\n%d\t%d\t%d",i,A[i],S[i]);
    return 0;
}
