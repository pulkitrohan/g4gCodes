#include<bits/stdc++.h>
using namespace std;
const int R = 3,C = 3;

int cal(int ones[R+1][C+1], int x, int y, int k) {
	int ans = ones[x-1][y-1];
	ans -= ones[x-1][y+k-1];
	ans -= ones[x+k-1][y-1];
	ans += ones[x+k-1][y+k-1];
	return ans;
}

int main(void)
{
	int A[][C] = {{0,0,1},
				  {0,0,1},
				  {1,0,1}};
	int ones[R+1][C+1] = {0};

	for(int i=1;i<=R;i++)
		for(int j=1;j<=C;j++)
			ones[i][j] = ones[i-1][j] + ones[i][j-1] - ones[i-1][j-1] + (A[i-1][j-1] == 1);

	int ans = 0;
	for(int k=1;k<=min(R,C);k++)
		for(int i=1;i+k-1 <= R;i++)
			for(int j=1;j+k-1 <= C;j++)
				ans = max(ans, ones[R][C] + k*k - 2* cal(ones, i, j, k));	
	cout<<ans<<endl;
	return 0;
}