#include<bits/stdc++.h>
using namespace std;

void PrintMinNumberForPattern(string S)
{
	vector<int> V;
	if(S.size() == 0)
		return;
	int min_avail = 3,index;
	if(S[0] == 'I')
	{
		V.push_back(1);
		V.push_back(2);
		index = 1;
	}
	else
	{	
		V.push_back(2);
		V.push_back(1);
		index = 0;
	}
	for(int i=1;i<S.length();i++)
	{
		if(S[i] == 'D')
		{
			V.push_back(V[i]);
			for(int j=index;j<=i;j++)
				V[j]++;
		}
		else
		{
			V.push_back(min_avail);
			index = i+1;
		}
		min_avail++;
	}
	for(int i=0;i<V.size();i++)
		cout<<V[i];
	cout<<endl;
}

int main(void)
{
	PrintMinNumberForPattern("IDID");
    PrintMinNumberForPattern("I");
    PrintMinNumberForPattern("DD");
    PrintMinNumberForPattern("II");
    PrintMinNumberForPattern("DIDI");
    PrintMinNumberForPattern("IIDDD");
    PrintMinNumberForPattern("DDIDDIID");
	return 0;
}