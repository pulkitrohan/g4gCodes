#include<stdio.h>
#include<stdlib.h>
#include<string.h>

void MemMove(void *dest,void *src,int N)
{
	char *des = (char *)dest;
	char *sr = (char *)src;
	char *temp = (char *)malloc(sizeof(char)*N));
	int i;
	for(i=0;i<N;i++)
		temp[i] = sr[i];
	for(int i=0;i<N;i++)
		des[i] = temp[i];
}

//Moving 4 bytes at a time is more optimal than moving 1 byte at a time.

void MemCopy(void *dest,void *src,int N)
{
	char *des = (char *)dest;
	char *sr = (char *)src;
	int loops = N/sizeof(int);
	int i;
	for(i=0;i<loops;i++)
	{
		*(int *)des = *(int *)src;
		des += sizeof(int);
		src += sizeof(int);
	}
	loops = N%sizeof(int);
	for(i=0;i<loops;i++)
	{
		*des = *src;
		++des;
		++src;
	}
}


int main(void)
{
	char A[] = "GEEKSFORGEEKS";
	char *B = A+5;
	MemMove(A+5,A,strlen(A)+1);
	return 0;
}