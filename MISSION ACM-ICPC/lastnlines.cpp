#include<bits/stdc++.h>
using namespace std;

int main(void)
{
	FILE *in = fopen("input.txt","r");
	if(!in)
	{
		perror("fopen");
		exit(EXIT_FAILURE);
	}
	FILE *out = fopen("output.txt","w");
	if(!out)
	{
		perror("fopen");
		exit(EXIT_FAILURE);
	}
	fseek(in,0,SEEK_END);
	int pos = ftell(in);
	cout<<pos<<endl;
	int count = 0;
	while(pos)
	{
		fseek(in,--pos,SEEK_SET);
		if(fgetc(in) == '\n')
		{
			if(++count == 20)
				break;
		}
	}
	char s[128];
	while(fgets(s,sizeof(s),in))
		fprintf(out,"%s",s);
	fclose(in);
	fclose(out);
	return 0;
}
