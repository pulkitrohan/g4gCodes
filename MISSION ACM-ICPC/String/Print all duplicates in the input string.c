#include<stdio.h>
#include<limits.h>
#define MAX 256
void PrintDuplicates(char *S)
{
    int i,count[MAX+1] = {0};
    for(i=0;S[i] != '\0';i++)
    {
        count[S[i]]++;
    }
    for(i=0;i<MAX;i++)
    {
        if(count[i] > 1)
            printf("%c occurs %d times\n",i,count[i]);
    }

}


int main(void)
{
    char s[MAX];
    printf("Enter the string : ");
    gets(s);
    PrintDuplicates(s);
    return 0;
}

