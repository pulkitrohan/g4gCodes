
/*
suppose number is 234.Then if we see the combinations we see that for 4 the possible values are ghi
then the value is rest to g if the value on the left of 4 is changed and similarly value of i-1 is reset
when value of i-2 is changed.So we can create a recursive solution for this.

Time Complexity : O(4^N)

*/

#include<stdio.h>
#include<string.h>
char hashtable[10][5] = {"","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};

void printWordsUtil(int *number,int start,char *output,int end)
{
    int i;
    if(start == end)        //When we have reached the last possible value of the current value of number
    {
        printf("%s\n",output);
        return ;
    }
    for(i=0;i<strlen(hashtable[number[start]]);i++)
    {
        output[start] = hashtable[number[start]][i];
        printWordsUtil(number,start+1,output,end);
        if(number[start] == 0 || number[start] == 1)
            return;
    }
}

void printWords(int *number,int N)
{
    char results[N+1];
    results[N] = '\0';
    printWordsUtil(number,0,results,N);
}

int main(void)
{
    int number[] = {2,3,4};
    int N = sizeof(number)/sizeof(number[0]);
    printWords(number,N);
    return 0;
}
