#include<bits/stdc++.h>

using namespace std;

void sortedPermutation(string A)
{
	int len = A.length(),i;
	sort(A.begin(),A.end());
	int finished = 0;
	while(!finished)
	{
		cout<<A<<endl;
		for(i=len-1;i>0;i--)
			if(A[i] > A[i-1])
				break;
		
		if(i == 0)
			finished = 1;
		else
		{
			char ch = A[i-1];
			int smallest = i;
			for(int j=i+1;j<len;j++)
				if(A[j] > ch && A[j] < A[smallest])
					smallest = j;
			swap(A[smallest],A[i-1]);
			reverse(A.begin()+i,A.end());
		//	sort(A.begin()+i,A.end());
		}
	}
}

int main(void)
{
	string A = "ABCD";
	sortedPermutation(A);
	return 0;
}