#include<bits/stdc++.h>
using namespace std;

vector<int> calculate(string S)
{
	vector<int> V(256,0);
	for(int i=0;i<S.length();i++)
		V[S[i]]++;
	for(int i=1;i<256;i++)
		V[i] += V[i-1];
	return V;
}
int fact(int N)
{
	int ans = 1;
	for(int i=2;i<=N;i++)	
		ans *= i;
	return ans;
}

void modify(vector<int> &V,char ch)
{	
	for(int i=ch;i<256;i++)
		V[i]--;
}

int rankbatao(string S)
{
	int N = S.length();
	int ans = 1;
	int F = fact(N);
	vector<int> count = calculate(S);
	for(int i=0;i<N;i++)
	{
		F /= (N-i);
		int val = count[S[i]-1]*F;
		ans += val;
		modify(count,S[i]);
	}
	return ans;
}		

int main(void)
{
	string S = "STRING";
	cout<<rankbatao(S);
	return 0;
}