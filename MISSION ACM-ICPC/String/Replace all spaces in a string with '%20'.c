#include<stdio.h>
#include<string.h>
#include<stdlib.h>

void ReplaceAllSpaces(char *A,int N)
{
    int i,count = 0;
    for(i=0;i<N;i++)
        if(isspace(A[i]))
            count++;
    int NewLength = N + 2*count;
    A = realloc(A,NewLength);
    A[NewLength--] = '\0';
    for(i=N-1;i>=0;i--)
    {
        if(isspace(A[i]))
        {
            A[NewLength--] = '0';
            A[NewLength--] = '2';
            A[NewLength--] = '%';
        }
        else
            A[NewLength--] = A[i];
    }
}

int main(void)
{
    int N = 10;
    char *A = (char *)malloc(sizeof(char) * N);
    gets(A);
    int len = 0,i;
    for(i=0;A[i]!= '\0';i++,len++);
    ReplaceAllSpaces(A,len);
    puts(A);
}
