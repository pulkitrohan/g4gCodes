#include<bits/stdc++.h>
using namespace std;

int nonDecNums(int N) {
	int dp[N+1][10];

	for(int i=0;i<=9;i++)
		dp[0][i] = 1;
	for(int i=1;i<=N;i++)
		dp[i][9] = 1;
	for(int i=1;i<=N;i++)
		for(int j=8;j>=0;j--)
			dp[i][j] = dp[i-1][j] + dp[i][j+1];
	return dp[N][0];
}
	
int main(void)
{
	int N = 2;
	cout<<nonDecNums(N)<<endl;
    return 0;
}
