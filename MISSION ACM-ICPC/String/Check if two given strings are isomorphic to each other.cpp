#include<bits/stdc++.h>
using namespace std;

bool isomorphic(string A,string B)
{
	int N = A.length(),M = B.length();
	if(N != M)
		return false;
	bool hash[256] = {false};
	int map[256];
	memset(map,-1,sizeof(map));
	for(int i=0;i<N;i++)
	{
		if(map[A[i]] == -1)
		{
			if(hash[B[i]])
				return false;
			hash[B[i]] = true;
			map[A[i]] = B[i];
		}
		else if(map[A[i]] != B[i])
			return false;
	}
	return true;
}

int main(void)
{
	cout<<isomorphic("aab","xxy")<<endl;
	cout<<isomorphic("aab","xyz")<<endl;
	return 0;
}