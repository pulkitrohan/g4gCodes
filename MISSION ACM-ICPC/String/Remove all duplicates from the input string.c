#include<stdio.h>
#define MAX 256

void RemoveDuplicates(char *A)
{
   int count[MAX+1] = {0},i,j;
   for(i=0,j=0;A[i] != '\0';i++)
   {
       if(count[A[i]] == 0)
       {
           count[A[i]]++;
           A[j++] = A[i];
       }
   }
   A[j] = '\0';
   printf("Input string after removing duplicates : %s\n",A);
}

RemoveDuplicatesNoBuffer(char *A)
{
    int k = 1,i,j;
    if(A[0] == '\0')
        return;
    for(i=1;A[i] != '\0';i++)
    {
        for(j=0;j<i;j++)
            if(A[i] == A[j])
                break;
        if(j == i)
            A[k++] = A[i];
    }
    A[k] = '\0';
    printf("%s",A);
}

int main(void)
{
    char S[MAX];
    printf("Enter the string : ");
    scanf("%[^\n]",S);
    RemoveDuplicatesNoBuffer(S);
    return 0;
}
