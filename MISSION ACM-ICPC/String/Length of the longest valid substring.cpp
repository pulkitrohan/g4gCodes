#include<bits/stdc++.h>
using namespace std;

int findMaxLen(string s) {
    int ans = 0;
    int left = 0, right = 0;
    for(int i=0;i<s.length();i++) {
        if(s[i] == '(')
            left++;
        else if(s[i] == ')')
            right++;
        if(left == right)
            ans = max(ans, 2*right);
        else if(right > left)
            left = 0,right = 0;
    }
    left = 0, right = 0;
    for(int i=s.length()-1;i>=0;i--) {
        if(s[i] == '(')
            left++;
        else if(s[i] == ')')
            right++;
        if(left == right)
            ans = max(ans, 2*right);
        else if(right < left)
            left = 0,right = 0;
    }
    return ans;
}


int main(void) {
    string S = "((()()";
    cout<<findMaxLen(S)<<endl;
    return 0;
}