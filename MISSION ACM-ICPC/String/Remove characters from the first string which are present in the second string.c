#include<stdio.h>

#define MAX 256
void RemoveChar(char *A,char *B)
{
    int count[MAX+1] = {0},i,j;
    for(i=0;B[i] != '\0';i++)
        count[B[i]]++;

    for(i=0,j=0;A[i] != '\0';i++)
    {
        if(count[A[i]] == 0)
            A[j++] = A[i];
    }
    A[j] = '\0';
}

int main(void)
{
    char A[100],B[100];
    gets(A);
    gets(B);
    RemoveChar(A,B);
    puts(A);
    return 0;
}
