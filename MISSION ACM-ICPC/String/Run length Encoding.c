#include<stdio.h>
#include<string.h>
#define MAX 256
/*
#include<bits/stdc++.h>
using namespace std;

string Encode(string A)
{
	if(A.length() == 0)
		return "";
	string B = "";
	B += A[0];
	int count = 1;
	for(int i=1;i<A.length();i++)
	{
		if(A[i] == A[i-1])
			count++;
			
		else
		{
			B.append(to_string(count));
			count = 1;
			B += A[i];
		}
	}
	return B.append(to_string(count));
}

int main(void)
{
	string S = "wwwwaaadexxxxxx";
	string A = Encode(S);
	cout<<A;
	return 0;
}
*/

char *RunLengthEncoding(char *A)
{
    int count[MAX] = {0},i,j=1;
    count[A[0]]++;
    char *B = (char *)malloc(sizeof(char)*strlen(A));
    for(i=1;A[i];i++)
    {
        if(A[i] == A[i-1])
            count[A[i]]++;
        else
        {
            A[j++] = count[A[i-1]] + '0';
			count[A[i-1]] = 0;
            A[j++] = A[i];
            count[A[i]]++;
        }
    }
    A[j++] = count[A[i-1]] + '0';
    A[j] = '\0';
    return B;
}

int main(void)
{
    char *A = "wwwwaaadexxxxxx";
    char *B = RunLengthEncoding(A);
    printf("%s",B);
    return 0;
}
