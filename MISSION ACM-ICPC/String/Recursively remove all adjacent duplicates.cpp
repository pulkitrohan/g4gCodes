#include<bits/stdc++.h>
using namespace std;

void solve(string &S)
{
	int count = 0,j = 0;
	while(S[j])
	{
		if(S[j] != S[j+1])
		{
			if(j != 0 && S[j-1] != S[j])
			{
				if(count != j && S[count] != S[j])
					S[++count] = S[j];
				else
					--count;
			}
		}
		j++;
	}
	S = S.substr(0,count+1).c_str();
}

int main(void)
{
	string S = "azxxxy";
	solve(S);
	cout<<S;
	return 0;
}