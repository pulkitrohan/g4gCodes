#include<stdio.h>
#include<limits.h>
#define MAX 256
void GetMaxOccurChar(char *S)
{
    int i,count[MAX+1] = {0};
    for(i=0;S[i] != '\0';i++)
    {
        count[S[i]]++;
    }
    int max = 0;
    char ch;
    for(i=0;i<MAX;i++)
    {
        if(count[i] > max)
        {
            max = count[i];
            ch = i;
        }
    }
    printf("%c occurs maximum of %d times\n",ch,max);
}


int main(void)
{
    char s[MAX];
    printf("Enter the string : ");
    gets(s);
    GetMaxOccurChar(s);
    return 0;
}
