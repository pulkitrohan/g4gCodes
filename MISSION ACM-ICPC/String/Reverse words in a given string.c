#include<stdio.h>

void Reverse(char *start,char *end)
{
    char temp;
    while(end > start)
    {
        temp = *start;
        *start = *end;
        *end = temp;
        start++,end--;
    }
}


void ReverseWords(char *A)
{
    char *B,*C;
    B = C = A;
    while(*B)
    {
        B++;
        if(*B == ' ')
        {
            Reverse(C,B-1);
            C = B + 1;
        }
        else if(*B == '\0')
            Reverse(C,B-1);
    }
    Reverse(A,B-1);
}

int main(void)
{
    char A[] = "i like this program very much";
    ReverseWords(A);
    printf("%s",A);
    return 0;
}
