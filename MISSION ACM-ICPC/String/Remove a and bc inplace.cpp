#include<bits/stdc++.h>
using namespace std;

void stringfilter(char *S)
{
	int N = strlen(S);
	int count = 0,j=0;
	while(j < N)
	{
		if(S[j] == 'b')
			j++;
		else if(S[j] == 'a' && j+1 < N && S[j+1] == 'c')
			j += 2;
		else if(count > 0 && S[j] == 'c' && S[count-1] == 'a')
			count--,j++;
		else
			S[count++] = S[j++];
	}
	S[count] = '\0';
}

int main(void)
{
	char S[] = "abc";
	stringfilter(S);
	cout<<S<<endl;
	return 0;
}