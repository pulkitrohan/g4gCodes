#include<stdio.h>
#include<string.h>
#define MAX 256

int compare(int *A,int *B)
{
    int i;
    for(i=0;i<MAX;i++)
    {
        if(A[i] != B[i])
            return 0;
    }
    return 1;
}

void search(char *pat,char *txt)
{
    int M = strlen(pat);
    int N = strlen(txt);
    int countP[MAX] = {0},countT[MAX] = {0},i;
    for(i=0;i<M;i++)
    {
        countP[pat[i]]++;
        countT[txt[i]]++;
    }
    for(i=M;i<=N;i++)
    {
        if(compare(countP,countT))
            printf("Found at index : %d\n",i-M);
        countT[txt[i]]++;
        countT[txt[i-M]]--;
    }
}

int main(void)
{
    char txt[] = "BACDGABCDA";
    char pat[] = "ABCD";
    search(pat,txt);
    return 0;
}
