#include<bits/stdc++.h>
using namespace std;

void PrintInterleaving(string A,string B,string C,int index1,int index2)
{
    if(index1 == A.length() && index2 == B.length())
		cout<<C<<endl;
    else
	{
		if(index1 < A.length())
		{
			C.push_back(A[index1]);
			PrintInterleaving(A,B,C,index1+1,index2);
			C.pop_back();
		}
		if(index2 < B.length())
		{
			C.push_back(B[index2]);
			PrintInterleaving(A,B,C,index1,index2+1);
			C.pop_back();
		}
	}
}

int main(void)
{
    string A = "AB";
	string B = "CD";
	string C = "";
    PrintInterleaving(A,B,C,0,0);
    return 0;
}
