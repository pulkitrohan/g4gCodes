#include<stdio.h>

#define MAX 256

void PrintList(char *list[],char *S,int N)
{
    int count[MAX+1] = {0};
    int i,j;
    for(i=0;S[i];i++)
        count[S[i]] = 1;
    int set ;
    int len = strlen(S);
    for(i=0;i<N;i++)
    {
        for(j=0,set=0;*(list[i]+j);j++)
        {
            if(count[*(list[i]+j)] == 1)
            {
                set++;
                count[*(list[i]+j)] = 0;
            }
        }
        if(set == len)
            printf("%s\n",list[i]);
        for(j=0;S[j];j++)
            count[S[j]] = 1;
    }
}

int main(void)
{
    char str[] ="sun";
    char *list[] = {"geeksforgeeks","unsorted","sunday","just","sss"};
    PrintList(list,str,5);
    return 0;
}
