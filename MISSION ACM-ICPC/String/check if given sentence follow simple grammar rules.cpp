#include<bits/stdc++.h>
using namespace std;

bool checkSentence(char *S)
{
	int len = strlen(S);
	if(S[0] < 'A' || S[0] > 'Z')
		return false;
	if(S[len-1] != '.')
		return false;
	int prev_state = 0,cur_state = 0;
	int index = 1;
	while(S[index])
	{
		if(S[index] >= 'A' && S[index] <= 'Z')
			cur_state = 0;
		else if(S[index] == ' ')
			cur_state = 1;
		else if(S[index] >= 'a' && S[index] <= 'z')
			cur_state = 2;
		else if(S[index] == '.')
			cur_state = 3;
			
		if(prev_state == cur_state && cur_state != 2)
			return false;
		if(prev_state == 2 && cur_state == 0)
			return false;
		if(cur_state == 3 && prev_state != 1)
			return(S[index+1] == '\0');
		index++;
		prev_state = cur_state;
	}
	return false;
}

int main(void)
{
	char *S[] = { "I love cinema.", "The vertex is S.",
                    "I am single.", "My name is KG.",
                    "I lovE cinema.", "GeeksQuiz. is a quiz site.",
                    "I love Geeksquiz and Geeksforgeeks.",
                    "  You are my friend.", "I love cinema" };
	int N = sizeof(S)/sizeof(S[0]);
	for(int i=0;i<N;i++)
	{
		if(checkSentence(S[i]))
			cout<<"CORRECT\n";
		else
			cout<<"INCORRECT\n";
	}
	return 0;
}