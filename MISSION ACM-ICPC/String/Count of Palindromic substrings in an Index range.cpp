#include<bits/stdc++.h>
using namespace std;

vector<vector<int>> CountPalindromicSubstring(char *A,int N)
{
	int dp[N+1][N+1];
	vector<vector<int> > count(N+1, vector<int>(N+1, 0));
	memset(dp,0,sizeof(dp));
	for(int i=N-1;i>=0;i--)
	{
		for(int j=i;j<N;j++)
		{
			if(i == j)
				dp[i][j] = 1;
			else if((i+1 == j) && A[i] == A[j])
				dp[i][j] = 1;
			else if(A[i] == A[j])
				dp[i][j] = dp[i+1][j-1];
			count[i][j] = count[i][j-1] + count[i+1][j] - count[i+1][j-1] + dp[i][j];
		}
	}
	return count;
}
	
int main(void)
{
    char A[] = "xyaabax";
    int N = strlen(A);
    vector<vector<int> > count = CountPalindromicSubstring(A, N);
    int start = 3, end = 5;
    cout<<count[start-1][end-1]<<endl;
    return 0;
}
