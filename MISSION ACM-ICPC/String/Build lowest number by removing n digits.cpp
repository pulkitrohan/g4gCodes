#include<bits/stdc++.h>
using namespace std;

void BuildLowestNumber(string S,int N,int res)
{
	if(N == 0)
	{
		res.append(S);
		return;
	}
	int len = S.length();
	if(len <= N)
		return;
	int min = 0;
	for(int i=1;i<=N;i++)
		if(S[i] < S[min])
			min = i;
	res.push_back(S[min]);
	S = S.substr(min+1,len-min);
	BuildLowestNumber(S,N-min,res);
}

int main(void)
{
	string S = "121198";
	int N = 2;
	string res = "";
	BuildLowestNumber(S,N,res);
	return 0;
}