#include<bits/stdc++.h>
using namespace std;
void PrintPermutations(string A,string S,int index)
{
    if(index == A.length())
    	cout<<S<<endl;
    else
    {
    	for(int i=0;i<A.length();i++)
    	{
    		S.push_back(A[i]);
    		PrintPermutations(A,S,index+1);
    		S.pop_back();
    	}
    }
}

int main(void)
{
    string A = "BAC";
    sort(A.begin(),A.end());
    string S = "";
    PrintPermutations(A,S,0);
    return 0;
}
