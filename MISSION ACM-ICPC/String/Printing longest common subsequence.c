#include<stdio.h>

#define Max(A,B) ((A > B) ? A : B)

int main(void)
{
    char A[100],B[100];
    scanf("%s",A);
    scanf("%s",B);
    int N = strlen(A);
    int M = strlen(B);
    int L[N+1][M+1];
    int i,j;
    for(i=0;i<=N;i++)
    {
        for(j=0;j<=M;j++)
        {
            if(i == 0 || j == 0)
                L[i][j] = 0;
            else if(A[i-1] == B[j-1])
                L[i][j] = L[i-1][j-1] + 1;
            else
                L[i][j] = Max(L[i-1][j],L[i][j-1]);
        }
    }
    int index = L[N][M];
    char LCS[index+1];
    LCS[index] ='\0';
    i = N,j = M;
    while(i > 0 && j > 0)
    {
        if(A[i-1] == B[j-1])
        {
            LCS[--index] = A[i-1];
            i--,j--;
        }
        else if(L[i-1][j] > L[i][j-1])
            i--;
        else
            j--;
    }
    printf("LCS : %s of length %d\n",LCS,L[N][M]);
    return 0;
}
