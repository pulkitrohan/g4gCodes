/*

       012345678901234
Text : BACBABABABACACA

		  0123456
Pattern : ABABACA
F[0] = 0
j = 0;
i = 1
A == B => False
j > 0 => False
F[1] = 0
j = 0
i = 2
A == A => True
F[2] = 1
j = 1
i = 3
B == B => True
F[3] = 2
i = 4
j = 2
A == A => True
F[4] = 3
i = 5
j = 3
B == C => False
j > 0 => True => j = F[3-1] => j = F[2] = 1
i=5
j=1
C == B => False
j > 0 =>True j = F[0] => 0
i = 5
j= 0
C == A => False
j = 0 => False
F[5] = 0
i = 6
j = 0
A == A => True
F[6] = 1


*/


#include<stdio.h>
#include<string.h>

int F[100];

void PrefixTable(char *P, int len)
{
    int i,j=0;
    F[0] = 0;
    for(i=1;i<len;)
    {
        if(P[i] == P[j])
            F[i++] = ++j;
        else if(j > 0)
            j = F[j-1];
        else
            F[i++] = 0;
    }
}

void KMP(char *T,char *P,int N,int M)
{
     PrefixTable(P,M);
     int i,j=0,flag = 0;
     for(i=0;i<N;)
     {
         if(T[i] == P[j])
         {
			i++,j++;
            if(j == M)
			{
				printf("FOUND AT %d\n",i-j);
				j = F[j-1];
				flag = 1;
			}
         }
         else if(j > 0)
            j = F[j-1];
         else
            i++;
     }
     if(!flag)
     	printf("PATTERN NOT FOUND\n");
}


int main(void)
{
    char T[1000],P[100];
    int flag = 0;
   // printf("Enter the text : ");
    scanf("%s",T);
   // printf("Enter the pattern : ");
    scanf("%s",P);
    KMP(T,P,strlen(T),strlen(P));
    
    return 0;
}
