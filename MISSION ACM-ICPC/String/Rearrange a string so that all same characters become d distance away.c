#include<stdio.h>
#define MAX 256
struct CharFreq
{
    int f;
    char c;
};

void Swap(struct CharFreq *A,struct CharFreq *B)
{
    struct CharFreq temp = *A;
    *A = *B;
    *B = temp;
}

void MaxHeapify(struct CharFreq *freq,int i,int N)
{
    int largest = i;
    int l = 2*i + 1;
    int r = 2*i + 2;
    if(l < N && freq[l].f > freq[largest].f)
        largest = l;
    if(r < N && freq[r].f > freq[largest].f)
        largest = r;
    if(i != largest)
    {
        Swap(&freq[i],&freq[largest]);
        MaxHeapify(freq,largest);
    }
}


void BuildHeap(struct CharFreq *freq,int N)
{
    for(i=(N-1)/2;i>=0;i--)
        MaxHeapify(freq,i,N);
}

Rearrange(char *A,int d)
{
    struct CharFreq freq[MAX];
    int m = 0; //Counting unique Characters
    for(i=0;i<strlen(A);i++)
    {
        char ch = A[i];
        if(freq[ch].f == 0)
        {
            freq[ch].c = ch;
            m++;
            freq[ch].f++;
            A[i] = '\0';
        }
    }
    BuildHeap(freq);
}

int main(void)
{
    char A[100];
    scanf("%s",A);
    int d = 3;
    Rearrange(A,d);
    printf("%s\n",A);
    return 0;
}
