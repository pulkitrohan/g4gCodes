#include<stdio.h>
#include<string.h>
#define MAX 256
int IsAnagram(char *A,char *B)
{
    if(strlen(A) != strlen(B))
        return 0;

    int i;
    int len = strlen(A);
    int count[MAX+1];
    for(i=0;i<MAX;i++)
        count[i] = 0;

    for(i=0;i<len;i++)
    {
        count[A[i]]++;
        count[B[i]]--;
    }

    for(i=0;i<256;i++)
    {
        if(count[i] != 0)
            return 0;
    }
    return 1;
}

int main(void)
{
    char A[] = "abcd";
    char B[] = "acdb";
    if(IsAnagram(A,B))
        printf("Yes,they are Anagrams\n");
    else
        printf("No\n");
    return 0;
}
