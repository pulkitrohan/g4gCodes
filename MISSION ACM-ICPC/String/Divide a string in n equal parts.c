#include<stdio.h>
#include<string.h>
void DivideString(char *A,int N)
{
    int len = strlen(A),i;
    if(len%N)
    {
        printf("Cannot divide the string\n");
        return ;
    }
    for(i=0;A[i] != '\0';i++)
    {
        if(i%N == 0)
            printf("\n");
        printf("%c",A[i]);
    }
}

int main(void)
{
    char A[100];
    int N;
    scanf("%s",A);
    scanf("%d",&N);
    DivideString(A,N);
    return 0;
}
