#include<stdio.h>
#include<string.h>

int IsInterleaving(char *A,char *B,char *C)
{
    while(*C)
    {
        if(*A == *C)
            A++;
        else if(*B == *C)
            B++;
        else
            return 0;
        C++;
    }
    return 1;
}

int main(void)
{
    char A[] = "AB";
    char B[] = "CD";
    char C[] = "ACDG";
    if(IsInterleaving(A,B,C))
        printf("Yes,interleaving\n");
    else
        printf("No,not interleaving\n");
    return 0;
}

