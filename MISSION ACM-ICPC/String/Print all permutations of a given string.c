#include<stdio.h>
#include<string.h>

void Swap(char *A,char *B)
{
    char temp = *A;
    *A = *B;
    *B = temp;
}

void permutation(char *A,int low,int high)
{
    if( low == high )
        printf( "%s\n",A );
    else
    {
        int j;
        for( j = low  ; j <= high ; j++ )
        {
            Swap( &A[low] , &A[j] );
            permutation( A , low + 1 , high );
            Swap( &A[low] , &A[j] );
        }
    }
}

int main(void)
{
    char A[] = "ABC";
    permutation(A,0,2);
    return 0;
}
