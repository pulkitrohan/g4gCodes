#include<bits/stdc++.h>
using namespace std;

bool isValid(int F[],int k)
{
	int count = 0;
	for(int i=0;i<26;i++)
		if(F[i] > 0)
			count++;
	return (count <= k);
}

void KUnique(string S,int k)
{
	int count = 0;
	int F[26] = {0};
	for(int i=0;i<S.length();i++)
	{
		if(F[S[i]-'a'] == 0)
			count++;
		F[S[i]-'a']++;
	}
	if(count < k)
	{
		cout<<"Not enough unique characters\n";
		return;
	}
	int cur_start = 0,cur_end = 0;
	int max_size = 1,max_start = 0;
	memset(F,0,sizeof(F));
	F[S[0]-'a']++;
	for(int i=1;i<S.length();i++)
	{
		F[S[i]-'a']++;
		cur_end++;
		while(!isValid(F,k))
			F[S[cur_start++]-'a']--;
		if(cur_end-cur_start+1 > max_size)
		{
			max_size = cur_end-cur_start+1;
			max_start = cur_start;
		}
	}
	cout<<S.substr(max_start,max_size)<<" "<<max_size<<endl;
}

int main(void)
{
	string S = "aabacbebebe";
	int k = 3;
	KUnique(S,k);
	return 0;
}