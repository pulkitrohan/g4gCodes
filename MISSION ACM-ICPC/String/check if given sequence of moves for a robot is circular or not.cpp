#include<bits/stdc++.h>
using namespace std;

#define N 0
#define E 1
#define S 2
#define W 3

int isCircular(char *path)
{
	int x = 0,y = 0,dir = N;
	for(int i=0;i<N;i++)
	{
		if(path[i] == 'R')
			dir = (dir+1)%4;
		else if(path[i] == 'L')
			dir = (4-dir+1)%4;
		else
		{
			if(dir == N)
				y++;
			else if(dir == S)
				y--;
			else if(dir == W)
				x--;
			else
				x++;
		}
	}
	return (x == 0 && y == 0);
}
				

int main(void)
{
	char path[] = "GGGG";
	if(isCircular(path))
		cout<<"CIRCULAR\n";
	else
		cout<<"NOT CIRCULAR\n";
	return 0;
}