//Time Complexity = O(nm) => n = length of Text, m = length of Pattern

#include<stdio.h>

int main(void)
{
    char T[1000],P[100];
    int i,j,k,flag = 0;
    printf("Enter the text : ");
    scanf("%[^\n]",T);
    printf("Enter the pattern : ");
    scanf("%s",P);
    for(i=0;T[i] != '\0';i++)
    {
        for(j=i,k=0;P[k] !='\0' && (P[k] == T[j]);j++,k++);
        if(P[k] == '\0')
        {
            printf("Pattern Matched at index %d\n",i);
            flag = 1;
        }
    }
    if(!flag)
        printf("Pattern is not Matched\n");
    return 0;
}
