#include<stdio.h>
#include<string.h>
#include<limits.h>
#define MAX 256
struct CountIndex
{
    int count;
    int index;
};

void GetFirstNonRepeat(char *A)
{
    struct CountIndex *count = (struct CountIndex *)malloc(sizeof(struct CountIndex)*strlen(A));
    int i;
    for(i=0;A[i] != '\0';i++)
    {
        (count[A[i]].count)++;
        if(count[A[i]].count == 1)
            count[A[i]].index = i;
    }
    int result = INT_MAX;
    for(i=0;i<MAX;i++)
    {
        if(count[i].count == 1 && count[i].index < result)
            result = count[i].index;
    }
    if(result == INT_MAX)
        printf("No non-Repeating character\n");
    else
        printf("First non-repeating Character : %c",A[result]);

}

int main(void)
{
    char A[100];
    scanf("%s",A);
    GetFirstNonRepeat(A);
    return 0;
}
