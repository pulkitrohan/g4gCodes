#include<bits/stdc++.h>
using namespace std;

string removeOccurrences(string S) {
    string ans = "";
    for(int i=0;i<S.size();i++) {
        if(S[i] == 'a' && ans.length() != 0 && ans.back() == 'a')
            continue;
        else if(S[i] == 'b')
            continue;
        ans += S[i];
    }
    return ans;
}

int main(void) {
    string str = "abcddabcddddabbbaaaaaa"; 
    cout << removeOccurrences(str); 
    
    return 0;
}