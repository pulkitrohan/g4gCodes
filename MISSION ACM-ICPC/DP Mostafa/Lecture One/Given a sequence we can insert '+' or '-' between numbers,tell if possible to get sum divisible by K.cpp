#include<bits/stdc++.h>
#define MAX 1000
int A[MAX+1];
int dp[MAX][MAX];
int N;
int K = 3;

int fix(int N)
{
	return (N%K + K)%K;
}

int tryAll(int i,int sum)
{
	int &ret = dp[i][sum];
	if(ret != -1)
		return ret;
	if(i == N)
		return ret = (sum == 0);
	if(tryAll(i+1,(fix(sum+A[i]))) || (tryAll(i+1,(fix(sum-A[i])))))
		return (ret = 1);
	return (ret = 0);
}

int main(void)
{
	scanf("%d",&N);
	for(int i=0;i<N;i++)
		scanf("%d",&A[i]);
    for(int i=0;i<MAX;i++)
        for(int j=0;j<MAX;j++)
            dp[i][j] = -1;
	if(N == 0)
		printf("YES\n");
	else
	{
		if(tryAll(0,0))
			printf("YES\n");
		else
			printf("NO\n");
	}
	return 0;
}
