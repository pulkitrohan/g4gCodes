#include<bits/stdc++.h>
using namespace std;
int main(void)
{
	int N,M;
	scanf("%d %d",&N,&M);
	int A[N+1][M+1],dp[N+1][M+1];
	for(int i=0;i<N;i++)
		for(int j=0;j<M;j++)
			scanf("%d",&A[i][j]);
	for(int i=0;i<N;i++)
		for(int j=0;j<M;j++)
			dp[i][j] = 0;
	for(int i=0;i<M;i++)
	{
		if(A[0][i] == 1)
			dp[0][i] = 1;
		else
			break;
	}
	for(int i=0;i<N;i++)
	{
		if(A[i][0] == 1)
			dp[i][0] = 1;
		else
			break;
	}
	for(int i=1;i<N;i++)
	{
		for(int j=1;j<M;j++)
		{
			if(A[i][j] == 1)
				dp[i][j] = dp[i-1][j] + dp[i][j-1];
		}
	}
	printf("%d\n",dp[N-1][M-1]);
	return 0;
}
