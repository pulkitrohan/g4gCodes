#include<bits/stdc++.h>
using namespace std;

class BIT
{
	int *ft,size;
public:
	BIT(int N)
	{
		size = N;
		ft = new int[N+1];
	}
	
	void update(int k,int v)
	{
		for(;k<=size;k+= k & -k)
			ft[k] += v;
	}
	
	int sum(int b)
	{
		int sum = 0;
		for(;b;b-= b & -b)
			sum += ft[b];
		return sum;
	}
	
	int sum(int a,int b)
	{
		return sum(b) - (a == 1 ? 0 : sum(a-1));
	}
	
	void scaleDown(int c)
	{
		for(int i=1;i<=size;i++)
			ft[i] /= c;
	}
	
	void scaleUp(int c)
	{
		for(int i=1;i<=size;i++)
			ft[i] *= c;
	}
};


int main(void)
{
	int N;
	scanf("%d",&N);
	BIT b(N);
	printf("%d\n",b.sum(10));
	b.update(5, 10);
    printf("%d %d %d %d\n", b.sum(4), b.sum(5), b.sum(10), b.sum(6, 10));
    b.scaleUp(2);
    printf("%d %d %d %d\n", b.sum(4), b.sum(5), b.sum(10), b.sum(6, 10));
    b.scaleDown(2);
    printf("%d %d %d %d\n", b.sum(4), b.sum(5), b.sum(10), b.sum(6, 10));
	return 0;
}