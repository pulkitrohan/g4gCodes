#include<bits/stdc++.h>
using namespace std;

//RMQ function
int RangeSum(int *st,int L,int R,int i,int j,int p)
{
	if(i <= L && j >= R)
		return st[p];
	if(i > R || j < L)
		return INT_MAX;
	int mid = (L+R)/2;
	return RangeSum(st,L,mid,i,j,2*p+1) + RangeSum(st,mid+1,R,i,j,2*p+2);
}

int RSQ(int *st,int N,int i,int j)
{
	if(i < 0 || j > N-1 || i > j)
		return INT_MAX;
	return RangeSum(st,0,N-1,i,j,0);
}

//Update function

int updateTree(int *st,int L,int R,int index,int diff,int p)
{
	if(L == R)
	{
		st[p] += diff;
		return st[p];
	}
	else
	{
		int mid = (L+R)/2;
		if(index <= mid)
			 updateTree(st,L,mid,index,diff,2*p+1);
		else
			 updateTree(st,mid+1,R,index,diff,2*p+2);
		st[p] = st[2*p+1] + st[2*p+2];
	}
}

void updateValue(int *A,int *st,int N,int index,int new_val)
{
	if(index < 0 || index > N-1)
		return;
	int diff = new_val - A[index];
	A[index] = new_val;
	updateTree(st,0,N-1,index,diff,0);
}


int constructTree(int *A,int L,int R,int *st,int p)
{
	if(L == R)
	{
		st[p] = A[L];
		return A[L];
	}
	else
	{
		int mid = (L+R)/2;
		st[p] = constructTree(A,L,mid,st,2*p+1)+constructTree(A,mid+1,R,st,2*p+2);
		return st[p];
	}
}

int *BuildTree(int *A,int N)
{
	int *st = new int[4*N];
	constructTree(A,0,N-1,st,0);
	return st;
}

int main(void)
{	
	int A[] = {1,3,5,7,9,11};
	int N = sizeof(A)/sizeof(A[0]);
	int *st = BuildTree(A,N);
	cout<<RSQ(st,N,0,5)<<endl;
	updateValue(A,st,N,0,20);
	cout<<RSQ(st,N,0,5)<<endl;
	updateValue(A,st,N,2,2);
	cout<<RSQ(st,N,0,3)<<endl;
	return 0;
}
	