#include<bits/stdc++.h>
using namespace std;

//RMQ function
int RangeMin(int *st,int L,int R,int i,int j,int p)
{
	if(i <= L && j >= R)
		returun st[p];
	if(i > R || j < L)
		return INT_MAX;
	int mid = (L+R)/2;
	return min(RangeMin(st,L,mid,i,j,2*p+1),RangeMin(st,mid+1,R,i,j,2*p+2));
}

int RMQ(int *st,int N,int i,int j)
{
	if(i < 0 || j > N-1 || i > j)
		return INT_MAX;
	return RangeMin(st,0,N-1,i,j,0);
}

//Update function

void updateTree(int *st,int *A,int L,int R,int index,int p,int val)
{
	if(L == R)
	{
		A[index] += val;
		st[p] += val;
	}
	else
	{
		int mid = (L+R)/2;
		if(L <= index && index <= mid)
			 updateTree(st,A,L,mid,index,2*p+1,val);
		else
			 updateTree(st,A,mid+1,R,index,2*p+2);
		st[p] = min(st[2*p+1],st[2*p+2]);
	}
}

void updateValue(int *A,int *st,int N,int index,int new_val)
{
	if(index < 0 || index > N-1)
		return;
	A[index] = new_val;
	updateTree(st,A,0,N-1,index,0);
}


void constructTree(int *A,int L,int R,int *st,int p)
{
	if(L == R)
		st[p] = A[L];
	else
	{
		int mid = (L+R)/2;
		constructTree(A,L,mid,st,2*p+1);
		constructTree(A,mid+1,R,st,2*p+2);
		st[p] = min(st[2*p+1],st[2*p+2]);
	}
}

int *BuildTree(int *A,int N)
{
	int *st = new int[4*N];
	constructTree(A,0,N-1,st,0);
	return st;
}

int main(void)
{	
	int A[] = {1,3,5,7,9,11};
	int N = sizeof(A)/sizeof(A[0]);
	int *st = BuildTree(A,N);
	cout<<RMQ(st,N,1,5);
	updateValue(A,st,N,20);
	cout<<RMQ(st,N,0,3);
	return 0;

	