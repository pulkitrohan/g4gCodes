#include<bits/stdc++.h>
using namespace std;

//RMQ function
int RangeSum(int *st,int *lazy,int L,int R,int i,int j,int p)
{
	if(lazy[p] != 0)
	{
		st[p] += (R-L+1)*lazy[p];
		if(L != R)
		{
			lazy[2*p+1] += lazy[p];
			lazy[2*p+2] += lazy[p];
		}
		lazy[p] = 0;
	}
	
	if(i > R || j < L || R < L)
		return INT_MAX;
	if(i <= L && j >= R)
		return st[p];
	int mid = (L+R)/2;
	return RangeSum(st,lazy,L,mid,i,j,2*p+1) + RangeSum(st,lazy,mid+1,R,i,j,2*p+2);
}


int RSQ(int *st,int *lazy,int N,int i,int j)
{
	if(i < 0 || j > N-1 || i > j)
		return INT_MAX;
	return RangeSum(st,lazy,0,N-1,i,j,0);
}

//Update function

void updateTreeRange(int *st,int *lazy,int L,int R,int start,int end,int diff,int p)
{
	if(lazy[p] != 0)
	{
		st[p] += (R-L+1)*lazy[p];
		if(L != R)
		{
			lazy[2*p+1] += lazy[p];
			lazy[2*p+2] += lazy[p];
		}
		lazy[p] = 0;
	}
	if(R > L || L > end || R < start)
		return;
	if(L >= start && end >= R)
	{
		st[p] += (R-L+1)*diff;
		if(L != R)
		{
			lazy[2*p+1] += diff;
			lazy[2*p+2] += diff;
		}
		return;
	}
	
	int mid = (L+R)/2;
	updateTreeRange(st,lazy,L,mid,start,end,diff,2*p+1);
	updateTreeRange(st,lazy,mid+1,R,start,end,diff,2*p+2);
	st[p] = st[2*p+1] + st[2*p+2];
}

void updateRange(int *st,int *lazy,int N,int start,int end,int new_val)
{
	updateTreeRange(st,lazy,0,N-1,start,end,new_val,0);
}
		
int constructTree(int *A,int L,int R,int *st,int p)
{
	if(L == R)
	{
		st[p] = A[L];
		return A[L];
	}
	else
	{
		int mid = (L+R)/2;
		st[p] = constructTree(A,L,mid,st,2*p+1)+constructTree(A,mid+1,R,st,2*p+2);
		return st[p];
	}
}

int *BuildTree(int *A,int N)
{
	int *st = new int[4*N];
	constructTree(A,0,N-1,st,0);
	return st;
}

int main(void)
{	
	int A[] = {1,3,5,7,9,11};
	int N = sizeof(A)/sizeof(A[0]);
	int *st = BuildTree(A,N);
	int *lazy = new int[4*N];
	cout<<RSQ(st,lazy,N,1,3)<<endl;
	updateRange(st,lazy,N,1,5,10);
	cout<<RSQ(st,lazy,N,1,3)<<endl;
	return 0;
}
	