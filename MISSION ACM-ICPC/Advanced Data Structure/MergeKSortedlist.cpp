#include<bits/stdc++.h>
using namespace std;

struct LL
{
	int data;
	struct LL *next;
};

struct cmp
{
	bool operator()(struct LL *A,struct LL *B)
	{
		return A->data > B->data;
	}
};

struct LL *push(struct LL *head,int data)
{
	struct LL *temp = (struct LL *)malloc(sizeof(struct LL));
	temp->data = data;
	temp->next = head;
	head = temp;
	return head;
}

struct LL *insert(int data)
{
	struct LL *temp = (struct LL *)malloc(sizeof(struct LL));
	temp->data = data;
	temp->next = NULL;
	return temp;
}

void printList(struct LL *A)
{
	while(A)
	{	
		cout<<A->data<<" ";
		A = A->next;
	}
}

void ExternalSort(struct LL *A[],int N)
{
	priority_queue<struct LL *,vector<struct LL *>,cmp> H;
	struct LL *dummy = insert(0);
	struct LL *tail = dummy;
	for(int i=0;i<N;i++)
		if(A[i])
			H.push(A[i]);
	while(!H.empty())
	{
		tail->next = H.top();
		tail = tail->next;
		H.pop();
		if(tail->next)
			H.push(tail->next);
	}
	printList(dummy->next); 
}
	
	
int main(void)
{
	int N = 3;
	struct LL *A[N];
	A[0] = NULL;
	A[0] = push(A[0],50);
	A[0] = push(A[0],40);
	A[0] = push(A[0],30);
	
	A[1] = NULL;
	A[1] = push(A[1],45);
	A[1] = push(A[1],35);
	
	A[2] = NULL;
	A[2] = push(A[2],100);
	A[2] = push(A[2],80);
	A[2] = push(A[2],70);
	A[2] = push(A[2],60);
	A[2] = push(A[2],10);
	
	ExternalSort(A,N);
	
	return 0;
}