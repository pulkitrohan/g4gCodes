#include<bits/stdc++.h>
using namespace std;

struct TreapNode
{
	int key,priority;
	struct TreapNode *left,*right;
};

TreapNode *newNode(int key)
{
	TreapNode *node = new TreapNode;
	node->key = key;
	node->priority = rand()%100;
	node->left = node->right = NULL;
	return node;
}

TreapNode *rightRotate(TreapNode *y)
{
    TreapNode *x = y->left,  *T2 = x->right;
    x->right = y;
    y->left = T2;
    return x;
}
 
TreapNode *leftRotate(TreapNode *x)
{
    TreapNode *y = x->right, *T2 = y->left;
    y->left = x;
    x->right = T2;
    return y;
}

TreapNode *search(TreapNode *root,int key)
{
	if(!root)
		return NULL;
	else if(root->key == key)
		return root;
	else if(root->key < key)
		return search(root->right,key);
	else
		return search(root->left,key);
}

TreapNode *insert(TreapNode *root,int key)
{
	if(!root)
		return newNode(key);
	else if(key <= root->key)
	{
		root->left = insert(root->left,key);
		if(root->left->priority > root->priority)
			root = rightRotate(root);
	}
	else
	{
		root->right = insert(root->right,key);
		if(root->right->priority > root->priority)
			root = leftRotate(root);
	}
	return root;
}

TreapNode *deleteNode(TreapNode *root,int key)
{
	if(!root)
		return NULL;
	else if(root->key > key)
		root->left = deleteNode(root->left,key);
	else if(root->data < key)
		root->right = deleteNode(root->right,key);
	else
	{
		if(!root->left)
		{
			TreapNode *temp = root->right;
			delete(root);
			root = temp;
		}
		else if(!root->right)
		{
			TreapNode *temp = root->left;
			delete(root);
			root = tenp;
		}
		else if(root->left->priority < root->right->priority)
		{
			root = leftRotate(root);
			root->left = deleteNode(root->left,key);
		}
		else
		{
			root = rightRotate(root);
			root->right = deleteNode(root->right,key);
		}
	}
	return root;
}

int main(void)
{
	srand(time(NULL));
	TreapNode *root = NULL;
	root = insert(root, 50);
    root = insert(root, 30);
    root = insert(root, 20);
    root = insert(root, 40);
    root = insert(root, 70);
    root = insert(root, 60);
    root = insert(root, 80);
	
	cout << "Inorder traversal of the given tree \n";
    inorder(root);
 
    cout << "\nDelete 20\n";
    root = deleteNode(root, 20);
    cout << "Inorder traversal of the modified tree \n";
    inorder(root);
 
    cout << "\nDelete 30\n";
    root = deleteNode(root, 30);
    cout << "Inorder traversal of the modified tree \n";
    inorder(root);
 
    cout << "\nDelete 50\n";
    root = deleteNode(root, 50);
    cout << "Inorder traversal of the modified tree \n";
    inorder(root);
 
    TreapNode *res = search(root, 50);
    (res == NULL)? cout << "\n50 Not Found ":
                   cout << "\n50 found";
	return 0;
}

	
	
	
	
	
	
	
	