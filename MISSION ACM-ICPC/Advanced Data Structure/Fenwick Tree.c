#include<stdio.h>
int ft[11] = {0};
int R;

int LsOne(int k)
{
    return (k & (-k));
}

void update(int k,int v)
{
    for(;k < sizeof(ft);k += LsOne(k))
        ft[k] += v;
}

int rsq1(int b)
{
    int sum = 0;
    for(;b;b -= LsOne(b))
        sum += ft[b];
    return sum;
}

int rsq(int a,int b)
{
    return rsq1(b) - (a == 1 ? 0 : rsq1(a-1));
}

int main(void)
{
    int N,i,R;
    printf("Enter the number of elements: " );
        scanf("%d",&N);
    int F[N+1];
    printf("\nEnter the Frequencies : ");
    for(i=0;i<N;i++)
        scanf("%d",&F[i]);
    for(i=0;i<N;i++)
        update(F[i],1);
        printf("%d\n",rsq(1,1));
        printf("%d\n",rsq(1,2));
        printf("%d\n",rsq(1,6));
        printf("%d\n",rsq(1,10));
        printf("%d\n",rsq(3,6));
        update(5,2);
        printf("%d\n",rsq(1,10));
    return 0;
}
