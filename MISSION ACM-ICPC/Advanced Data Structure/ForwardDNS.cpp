#include<bits/stdc++.h>
using namespace std;
#define SIZE 27
#define MAX 100
struct node
{
	int data;
	struct node *children[SIZE];
	char *IP;
};

struct trie
{
	struct node *root;
	int count;
};

struct node *createNode(void)
{
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp->data = 0;
	temp->IP = NULL;
	for(int i=0;i<SIZE;i++)
		temp->children[i] = NULL;
	return temp;
}

struct trie *Initialize(void)
{
	struct trie *T = (struct trie *)malloc(sizeof(struct trie));
	T->root = createNode();
	T->count = 0;
	return T;
}

int getIndex(char c)
{
	if(c == '.')
		return 10;
	else
		return (c - 'a');
}

void insert(struct trie *T,char *IP,char *URL)
{
	int len = strlen(URL);
	struct node *temp;
	T->count++;
	temp = T->root;
	for(int level = 0;level < len;level++)
	{
		int index = getIndex(URL[level]);
		if(!temp->children[index])
			temp->children[index] = createNode();
		temp = temp->children[index];
	}
	temp->data = T->count;
	temp->IP = (char *)malloc(sizeof(char)*(strlen(IP)+1));
	strcpy(temp->IP,IP);
}

char *search(struct trie *T,char *URL)
{
	int len = strlen(URL);
	struct node *temp = T->root;
	for(int level = 0;level < len;level++)
	{
		int index = getIndex(URL[level]);
		if(!temp->children[index])
			return NULL;
		temp = temp->children[index];
	}
	if (temp && temp->data)
		return temp->IP;
	else
		return NULL;
}

int main(void)
{
	struct trie *T = Initialize();
	char IP[][MAX] = {"107.108.11.123","107.109.123.255","74.125.200.106"};
	char URL[][50] = {"www.samsung.com","www.samsung.net","www.google.in"};
	int N = sizeof(IP)/sizeof(IP[0]);
	for(int i=0;i<N;i++)
		insert(T,IP[i],URL[i]);
	char url[] = "www.samsung.com";
	char *ip = search(T,url);
	if(ip)
		printf("%s %s\n",url,ip);
	else
		printf("FDNS not resolved\n");
	return 0;
}