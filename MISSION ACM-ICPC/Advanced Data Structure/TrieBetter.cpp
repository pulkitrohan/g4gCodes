#include<bits/stdc++.h>
using namespace std;
#define SIZE 26
struct node
{
	int data;
	struct node *children[SIZE];
};

struct trie
{
	struct node *root;
	int count;
};

struct node *createNode(void)
{
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp->data = 0;
	for(int i=0;i<SIZE;i++)
		temp->children[i] = NULL;
	return temp;
}

struct trie *Initialize(void)
{
	struct trie *T = (struct trie *)malloc(sizeof(struct trie));
	T->root = createNode();
	T->count = 0;
	return T;
}

void insert(struct trie *T,char *key)
{
	int len = strlen(key);
	struct node *temp;
	T->count++;
	temp = T->root;
	for(int level = 0;level < len;level++)
	{
		int index = key[level] - 'a';
		if(!temp->children[index])
			temp->children[index] = createNode();
		temp = temp->children[index];
	}
	temp->data = T->count;
}

int search(struct trie *T,char *key)
{
	int len = strlen(key);
	struct node *temp = T->root;
	for(int level = 0;level < len;level++)
	{
		int index = key[level] - 'a';
		if(!temp->children[index])
			return 0;
		temp = temp->children[index];
	}
	return temp && temp->data;
}

int isleafNode(struct node *temp)
{
	if(temp->data != 0)
		return 1;
	else
		return 0;
}

bool deletekey(struct node *temp,char *key,int level,int len)
{
	if(temp)
	{
		if(level == len)
		{
			if(temp->data)
			{
				temp->data = 0;
				if(isFreeNode(temp))
					return true;
				return false;
			}
		}
		else
		{
			int index = key[level] - 'a';
			if(deletekey(temp->children[index],key,level+1,len))
			{
				free(temp->children[index]);
				temp->children[index] = NULL;
				return (!isleafNode(temp) && isFreeNode(temp));
			}
		}
	}
	return false;
}

void delete(struct trie *T,char *key)
{	
	int len = strlen(key);
	if(len > 0)
		deletekey(T->root,key,0,len);
}

int main(void)
{
	struct trie *T = Initialize();
	char keys[][8] = {"the","a","there","answer","any","by","bye","their"};
	int N = sizeof(keys)/sizeof(keys[0]);
	for(int i=0;i<N;i++)
		insert(T,keys[i]);
	if(search(T,"these"))
		cout<<"Present\n";
	else
		cout<<"Not Present\n";
		
	delete(T,keys[0]);
	return 0;
}