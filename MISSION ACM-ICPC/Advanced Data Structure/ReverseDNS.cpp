#include<bits/stdc++.h>
using namespace std;
#define SIZE 11
#define MAX 100
struct trie
{
	int end;
	struct trie *children[SIZE];
	char *URL;
};

struct trie *createNode(void)
{
	struct trie *temp = (struct trie *)malloc(sizeof(struct trie));
	temp->end = 0;
	temp->URL = NULL;
	for(int i=0;i<SIZE;i++)
		temp->children[i] = NULL;
	return temp;
}


int getIndex(char c)
{
	if(c == '.')
		return 10;
	else
		return (c - '0');
}

void insert(struct trie *temp,char *key,char *URL)
{
	int len = strlen(key);
	for(int level = 0;level < len;level++)
	{
		int index = getIndex(key[level]);
		if(!temp->children[index])
			temp->children[index] = createNode();
		temp = temp->children[index];
	}
	temp->end = 1;
	temp->URL = (char *)malloc(sizeof(char)*(strlen(URL)+1));
	strcpy(temp->URL,URL);
}

char *search(struct trie *temp,char *key)
{
	int len = strlen(key);
	for(int level = 0;level < len;level++)
	{
		int index = getIndex(key[level]);
		if(!temp->children[index])
			return NULL;
		temp = temp->children[index];
	}
	if (temp && temp->end)
		return temp->URL;
	else
		return NULL;
}

int main(void)
{
	struct trie *T = createNode();
	char ip[][MAX] = {"107.108.11.123","107.109.123.255","74.125.200.106"};
	char URL[][50] = {"www.samsung.com","www.samsung.net","www.google.in"};
	int N = sizeof(ip)/sizeof(ip[0]);
	for(int i=0;i<N;i++)
		insert(T,ip[i],URL[i]);
	char ipAdd[] = "107.108.11.123";
	char *url = search(T,ipAdd);
	if(url)
		printf("%s %s\n",ipAdd,url);
	else
		printf("RDNS not resolved\n");
	char ipAdd1[] = "107.108.11.12";
	char *url1 = search(T,ipAdd1);
	if(url1)
		printf("%s %s\n",ipAdd1,url1);
	else
		printf("RDNS not resolved\n");
	return 0;
}