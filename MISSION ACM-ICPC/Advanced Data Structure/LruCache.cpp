#include<bits/stdc++.h>
using namespace std;

typedef struct QNode
{
	struct QNode *prev,*next;
	int pageNumber;
}QNode;

typedef struct Queue
{
	int count;
	int numberOfFrames;
	QNode *front,*rear;
}Queue;

typedef struct Hash
{
	int capacity;
	QNode* *array;
}Hash;

QNode *newNode(int data)
{
	QNode *temp = (QNode *)malloc(sizeof(QNode));
	temp->pageNumber = data;
	temp->prev = temp->next = NULL;
	return temp;
}

Queue *createQueue(int numberOfFrames)
{	
	Queue *Q = (Queue *)malloc(sizeof(Queue));
	Q->count = 0;
	Q->front = Q->rear = NULL;
	Q->numberOfFrames = numberOfFrames;
	return Q;
}

Hash *createHash(int size)
{
	Hash *H = (Hash *)malloc(sizeof(Hash));
	H->capacity = size;
	H->array = (QNode **)malloc(size*sizeof(QNode *));
	for(int i=0;i<size;i++)
		H->array[i] = NULL;
	return H;
}

int AreAllFramesFull(Queue *Q)
{
	return Q->count == Q->numberOfFrames;
}

int isQueueEmpty(Queue *Q)
{
	return Q->rear == NULL;
}

void Dequeue(Queue *Q)
{
	if(isQueueEmpty(Q))
		return;
	if(Q->front == Q->rear)
		Q->front = NULL;
	QNode *temp = Q->rear;
	Q->rear = Q->rear->prev;
	if(Q->rear)
		Q->rear->next = NULL;
	free(temp);
	Q->count--;
}

void Enqueue(Queue *Q,Hash *H,int pageNumber)
{
	if(AreAllFramesFull(Q))
	{
		H->array[Q->rear->pageNumber] = NULL;
		Dequeue(Q);
	}
	QNode *temp = newNode(pageNumber);
	temp->next = Q->front;
	if(isQueueEmpty(Q))
		Q->rear = Q->front = temp;
	else
	{
		Q->front->prev = temp;
		Q->front = temp;
	}
	H->array[pageNumber] = temp;
	Q->count++;
}

void ReferencePage(Queue *Q,Hash *H,int pageNumber)
{
	QNode *reqPage = H->array[pageNumber];
	if(!reqPage)
		Enqueue(Q,H,pageNumber);
	else if(reqPage != Q->front)
	{
		reqPage->prev->next = reqPage->next;
		if(reqPage->next)
			reqPage->next->prev = reqPage->prev;
		if(reqPage == Q->rear)
		{
			Q->rear = reqPage->prev;
			Q->rear->next = NULL;
		}
		reqPage->next = Q->front;
		reqPage->prev = NULL;
		reqPage->next->prev = reqPage;
		Q->front = reqPage;
	}
}

int main(void)
{
	Queue *Q = createQueue(4);
	Hash *H = createHash(10);
	ReferencePage(Q,H,1);
	ReferencePage(Q,H,2);
	ReferencePage(Q,H,3);
	ReferencePage(Q,H,1);
	ReferencePage(Q,H,4);
	ReferencePage(Q,H,5);
	printf ("%d ", Q->front->pageNumber);
    printf ("%d ", Q->front->next->pageNumber);
    printf ("%d ", Q->front->next->next->pageNumber);
    printf ("%d ", Q->front->next->next->next->pageNumber);
	return 0;
}