#include<stdio.h>

struct TST
{
    char data;
    int is_end;
    struct TST *left,*eq,*right;
};

struct TST *insert(struct TST *root,char *word)
{
    if(!root)
    {
        root = (struct TST *)malloc(sizeof(struct TST));
        root->data = *word;
        root->is_end = 0;
        root->left = root->right = root->eq = NULL;
    }
    if(*word < root->data)
        root->left = insert(root->left,word);
	else if(*word > root->data)
		root->right = insert(root->right,word)
    else
    {
        if(*(word+1))
            root->eq = insert(root->eq,word+1);
        else
            root->is_end = 1;
    }
    return root;
}

int search(struct TST *root,char *word)
{
    if(!root)
        return 0;
    if(!*word)
        return 0;
    if(*word < root->data)
        return search(root->left,word);
    else if(*word > root->data)
        return search(root->right,word);
    else
    {
        if(!(*(word+1)))
            return root->is_end;
        else
            return search(root->eq,word+1);
    }
}

int main(void)
{
    struct TST *root = NULL;
    root = insert(root, "cat");
    root = insert(root, "cats");
    root = insert(root, "up");
    root = insert(root, "bug");

    printf("\nFollowing are search results for cats, bu and cat respectively\n");
    search(root, "cats")? printf("Found\n"): printf("Not Found\n");
    search(root, "bu")?   printf("Found\n"): printf("Not Found\n");
    search(root, "cat")?  printf("Found\n"): printf("Not Found\n");
    return 0;
}

