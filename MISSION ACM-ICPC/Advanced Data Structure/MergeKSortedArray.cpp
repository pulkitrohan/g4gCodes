#include<bits/stdc++.h>
using namespace std;

struct LL
{
	int data;
	struct LL *next;
};

struct node
{
	struct LL *head;
};

struct MinHeap
{
	int count;
	int capacity;
	struct node *Array;
};

struct LL *push(struct LL *head,int data)
{
	struct LL *temp = (struct LL *)malloc(sizeof(struct LL));
	temp->data = data;
	temp->next = head;
	head = temp;
	return head;
}

struct MinHeap *createMinHeap(int N)
{
	struct MinHeap *H = (struct MinHeap *)malloc(sizeof(struct MinHeap));
	H->capacity = N;
	H->count = 0;
	H->Array = (struct node *)malloc(sizeof(struct node)*N);
	return H;
}

void swap(struct node *A,struct node *B)
{
	struct node temp = *A;
	*A = *B;
	*B = temp;
}

void MinHeapify(struct MinHeap *H,int index)
{
	int left = 2*index + 1;
	int right = 2*index + 2;
	int smallest = index;
	if(left < H->count && H->Array[left].head->data < H->Array[smallest].head->data)
		smallest = left;
	if(right < H->count && H->Array[right].head->data < H->Array[smallest].head->data)
		smallest = right;
	if(smallest != index)
	{
		swap(&H->Array[smallest],&H->Array[index]);
		MinHeapify(H,smallest);
	}
}
int isEmpty(struct MinHeap *H)
{
	return (H->count == 0);
}

void BuildMinHeap(struct MinHeap *H)
{
	int N = H->count-1;
	for(int i=(N-1)/2;i>=0;i--)
		MinHeapify(H,i);
}

void PopulateMinHeap(struct MinHeap *H,struct LL *A[],int N)
{
	for(int i=0;i<N;i++)
		H->Array[H->count++].head = A[i];
	BuildMinHeap(H);
}

struct LL *ExtractMin(struct MinHeap *H)
{
	if(isEmpty(H))
		return NULL;
	struct node temp = H->Array[0];
	if(temp.head->next)
		H->Array[0].head = temp.head->next;
	else
	{
		H->Array[0] = H->Array[H->count-1];
		--H->count;
	}
	MinHeapify(H,0);
	return temp.head;
}

void ExternalSort(struct LL *A[],int N)
{
	MinHeap *H = createMinHeap(N);
	PopulateMinHeap(H,A,N);
	while(!isEmpty(H))
	{
		struct LL *temp = ExtractMin(H);
		printf("%d ",temp->data);
	}
}
	
	
int main(void)
{
	int N = 3;
	struct LL *A[N];
	A[0] = NULL;
	A[0] = push(A[0],50);
	A[0] = push(A[0],40);
	A[0] = push(A[0],30);
	
	A[1] = NULL;
	A[1] = push(A[1],45);
	A[1] = push(A[1],35);
	
	A[2] = NULL;
	A[2] = push(A[2],100);
	A[2] = push(A[2],80);
	A[2] = push(A[2],70);
	A[2] = push(A[2],60);
	A[2] = push(A[2],10);
	
	ExternalSort(A,N);
	
	return 0;
}