#include<bits/stdc++.h>
using namespace std;
#define MAX 256

struct node
{
	char data;
	struct node *next,*prev;
};

void appendNode(struct node *&head,struct node *&tail,char x)
{
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp->data = x;
	temp->next = temp->prev = NULL;
	if(!head)
		head = tail = temp;
	else
	{
		tail->next = temp;
		temp->prev = tail;
		tail = temp;
	}
}

void removeNode(struct node *&head,struct node *&tail,struct node *temp)
{
	if(!head)
		return;
	if(head == temp)
		head = head->next;
	if(tail == temp)
		tail = tail->prev;
	if(temp->next)
		temp->next->prev = temp->prev;
	if(temp->prev)
		temp->prev->next= temp->next;
	free(temp);
}

void FindFirstNonRepeating(void)
{
	struct node *inDLL[MAX];
	bool repeated[MAX];
	struct node *head = NULL,*tail = NULL;
	for(int i=0;i<MAX;i++)
	{
		inDLL[i] = NULL;
		repeated[i] = false;
	}
	char A[] = "geeksforgeeksandgeeksquizfor";
	for(int i=0;A[i];i++)
	{
		char x = A[i];
		if(!repeated[x])
		{
			if(!inDLL[x])
			{
				appendNode(head,tail,x);
				inDLL[x] = tail;
			}
			else
			{
				removeNode(head,tail,inDLL[x]);
				inDLL[x] = NULL;
				repeated[x] = true;
			}
		}
		if(head)
			cout<<head->data<<endl;
	}
}

int main(void)
{	
	FindFirstNonRepeating();
	return 0;
}