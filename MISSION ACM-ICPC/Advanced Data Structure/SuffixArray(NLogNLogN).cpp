#include<cstdio>
#include<algorithm>
#include<cstring>

using namespace std;

struct suffix
{
    int index;
    int rank[2];
};

int cmp(struct suffix A,struct suffix B)
{
    if(A.rank[0] == B.rank[0])
    {
        if(A.rank[1] < B.rank[1])
            return 1;
        else
            return 0;
    }
    else if(A.rank[0] < B.rank[0])
        return 1;
    else
        return 0;
}


int *BuildSuffixArray(char txt[],int N)
{
    struct suffix S[N];
    for(int i=0;i<N;i++)
    {
        S[i].index = i;
        S[i].rank[0] = txt[i]-'a';
        if(i+1 < N)
            S[i].rank[1] = txt[i+1] - 'a';
        else
            S[i].rank[1] = -1;
    }
    sort(S,S+N,cmp);
    int ind[N];
    for(int k=4;k<2*N;k = k*2)
    {
        int rank = 0;
        int prev_rank = S[0].rank[0];
        S[0].rank[0] = rank;
        ind[S[0].index] = 0;

        for(int i=1;i<N;i++)
        {
            if(S[i].rank[0] == prev_rank && S[i].rank[1] == S[i-1].rank[1])
            {
                prev_rank = S[i].rank[0];
                S[i].rank[0] = rank;
            }
            else
            {
                prev_rank = S[i].rank[0];
                S[i].rank[0] = ++rank;
            }

            ind[S[i].index] = i;
        }

        for(int i=0;i<N;i++)
        {
            int nextindex = S[i].index + k/2;
            if(nextindex < N)
                S[i].rank[1] = S[ind[nextindex]].rank[0];
            else
                S[i].rank[1] = -1;
        }

        sort(S,S+N,cmp);
    }

    int *SufArr = new int[N];
    for(int i=0;i<N;i++)
        SufArr[i] = S[i].index;

    return SufArr;
}

int main(void)
{
    char txt[100001];
    scanf("%s",txt);
    int *SufArr = BuildSuffixArray(txt,strlen(txt));
    printf("Suffix Array of %s : ",txt);
    for(int i=0;i<strlen(txt);i++)
        printf("%d ",SufArr[i]);
    return 0;
}
