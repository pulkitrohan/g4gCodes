#include<stdio.h>
#include<stdlib.h>

#define SIZE 26
struct Trie
{
	int data;
	struct Trie *Childs[SIZE];
};

struct Trie *root = NULL;

struct Trie *create()
{
    struct Trie *node = (struct Trie *)malloc(sizeof(struct Trie));
    int i;
    for(i=0;i<SIZE;i++)
        node->Childs[i] = NULL;
    node->data = -1;
    return node;
}

void insert(char word[])
{
	int length = strlen(word);
	if(!root)
        root = create();
    struct Trie *q = root;
    int i;
    for(i=0;i<length;i++)
    {
        int index = word[i]-'a';
        if(!q->Childs[index])
            q->Childs[index] = create();
        q = q->Childs[index];
    }
    q->data = i;
}

int search(char word[])
{
	struct Trie *q = root;
	int length = strlen(word);
	int i;
	for(i=0;i<length;i++)
    {
        int index = word[i] - 'a';
        if(q->Childs[index])
            q = q->Childs[index];
        else
            break;
    }
    if(!word[i] && q->data != -1)
        return 1;
    return 0;
}

int main(void)
{
    insert("by");
    insert("program");
    insert("programming");
    insert("data structure");
    insert("coding");
    insert("code");
    printf("Searched value: %d\n",search("code"));
    printf("Searched value: %d\n",search("geeks"));
    printf("Searched value: %d\n",search("coding"));
    printf("Searched value: %d\n",search("programming"));
	return 0;
}
