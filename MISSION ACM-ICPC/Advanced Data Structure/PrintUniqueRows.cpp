#include<bits/stdc++.h>
using namespace std;
#define SIZE 2
#define ROW 4
#define COL 5

struct trie
{
	int data;
	struct trie *children[SIZE];
};

struct trie *Initialize(void)
{
	struct trie *T = (struct trie *)malloc(sizeof(struct trie));
	T->data = 0;
	T->children[0] = T->children[1] = NULL;
	return T;
}

int insert(struct trie *temp,int (*M)[COL],int row)
{
	
	for(int level = 0;level < COL;level++)
	{
		int index = M[row][level];
		if(!temp->children[index])
			temp->children[index] = Initialize();
		temp = temp->children[index];
	}
	if(temp->data)
		return 0;
	temp->data = 1;
	return 1;
}

void PrintRow(int (*M)[COL],int row)
{
	for(int i=0;i<COL;i++)
		printf("%d ",M[row][i]);
	printf("\n");
}

void FindUniqueRows(int (*M)[COL])
{
	struct trie *T = Initialize();
	for(int i=0;i<ROW;i++)
	{
		if(insert(T,M,i))
			PrintRow(M,i);
	}
}

int main(void)
{
	
	int M[ROW][COL] = { {0,1,0,0,1},	
						{1,0,1,1,0},
						{0,1,0,0,1},
						{1,1,1,0,0}};
	FindUniqueRows(M);
	return 0;
}