#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

struct Suffix
{
    int index;
    char *suff;
};

int cmp(struct Suffix A,struct Suffix B)
{
    return strcmp(A.suff,B.suff) < 0 ? 1 : 0;
}

int *BuildSuffixArray(char txt[],int N)
{
    struct Suffix Suf[N];
    for(int i=0;i<N;i++)
    {
        Suf[i].index = i;
        Suf[i].suff = (txt + i);
    }
    sort(Suf,Suf+N,cmp);
    int *S = new int[N];
    for(int i=0;i<N;i++)
        S[i] = Suf[i].index;
    return S;
}


int Search(char *pat,char *txt,int *SufArr,int N)
{
    int M = strlen(pat);
    int l = 0,r = N-1;
    while(r >= l)
    {
        int mid = l + (r-l)/2;
        int res = strncmp(pat,txt+SufArr[mid],M);
        if(res == 0)
            return SufArr[mid];
        else if(res < 0)
            r = mid - 1;
        else
            l = mid + 1;
    }
    return -1;
}

int main(void)
{
    char txt[] = "banana";
    int *SufArr = BuildSuffixArray(txt,strlen(txt));
    printf("Suffix Array of %s : ",txt);
    for(int i=0;i<strlen(txt);i++)
        printf("%d ",SufArr[i]);
    char pat[] = "ana";
    int ans = Search(pat,txt,SufArr,strlen(txt));
    if(ans == -1)
        printf("\nPattern not found\n");
    else
        printf("\nPattern found at index : %d\n",ans);
    return 0;
}
