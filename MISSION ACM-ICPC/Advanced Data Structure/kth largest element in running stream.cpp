#include<bits/stdc++.h>
using namespace std;

class cmp
{
	public:
		bool operator()(int &A,int &B)
		{
			return A > B;
		}
};

void kthLargest(int k)
{
	priority_queue<int,vector<int>,greater<int> > H;
	int N;
	while(1)
	{
		cout<<"Enter element : ";
		cin>>N;
		if(H.size() < k)
			H.push(N);
		else
		{
			if(N > H.top())
			{
				H.pop();
				H.push(N);
			}
			cout<<H.top()<<endl;
		}
	}
}

int main(void)
{
	kthLargest(3);
	return 0;
}
