#include<bits/stdc++.h>
using namespace std;
#define SIZE 26

struct node
{
	int data;
	struct node *next;
};

struct trie
{
	int data;
	struct trie *children[SIZE];
	struct node *head;
};

struct trie *createNode(void)
{
	struct trie *temp = (struct trie *)malloc(sizeof(struct trie));
	temp->data = 0;
	temp->head = NULL;
	for(int i=0;i<SIZE;i++)
		temp->children[i] = NULL;
	return temp;
}

struct node *createIndexNode(int index)
{
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp->data = index;
	temp->next = NULL;
	return temp;
}

void insert(struct trie *temp,string key,int i)
{
	for(int level = 0;level <key.length();level++)
	{
		int index = key[level] - 'a';
		if(!temp->children[index])
			temp->children[index] = createNode();
		temp = temp->children[index];
	}
	temp->data = 1;
	struct node *t = createIndexNode(i);
	if(!temp->head)
		temp->head = t;
	else
	{
		t->next = temp->head;
		temp->head = t;
	}
}

void printAnagramsUtil(struct trie *root,vector<string> &words)
{
	if(!root)
		return;
	if(root->data)
	{
		struct node *temp = root->head;
		while(temp)
		{
			cout<<words[temp->data]<<" ";
			temp = temp->next;
		}
		cout<<endl;
	}
	for(int i=0;i<SIZE;i++)
		printAnagramsUtil(root->children[i],words);
}

void printAnagramsTogether(vector<string> V)
{
	struct trie *T = createNode();
	for(int i=0;i<V.size();i++)
	{
		string S = V[i];
		sort(S.begin(),S.end());
		insert(T,S,i);
	}
	printAnagramsUtil(T,V);

}

int main(void)
{
	vector<string> V;
	V.push_back("cat");
	V.push_back("dog");
	V.push_back("tac");
	V.push_back("god");
	V.push_back("act");
	V.push_back("gdo");
	V.push_back("abc");
	printAnagramsTogether(V);
	return 0;
}