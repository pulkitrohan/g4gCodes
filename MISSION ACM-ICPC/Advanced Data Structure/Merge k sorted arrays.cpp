#include<bits/stdc++.h>

using namespace std;

struct node
{
	int data;
	int arr_no;
	int next_index;
};

class cmp
{
	public:
		bool operator()(node &n1,node &n2)
		{
			return (n1.data> n2.data);
		}
};


vector<int> mergeKArrays(vector< vector<int> > V)
{
	int k = V.size();
	int N = V[0].size();
	vector<int> result;
	priority_queue<node,vector<node>,cmp> H;
	for(int i=0;i<k;i++)
	{
		node temp;
		temp.data = V[i][0];
		temp.arr_no = i;
		temp.next_index = 1;
		H.push(temp);
	}
	for(int count = 0;count<N*k;count++)
	{
		node min_e = H.top();
		H.pop();
		result.push_back(min_e.data);
		if(min_e.next_index < N)
		{
			node new_node;
			new_node.arr_no = min_e.arr_no;
			new_node.data = V[min_e.arr_no][min_e.next_index];
			new_node.next_index = min_e.next_index+1;
			H.push(new_node);
		}
	}
	return result;
}

int main(void)
{
	int A[][8] = {{10,20,30,40,50,60,71,86},
				  {15,25,35,45,60,69,77,78},
				  {27,29,37,48,50,65,75,78},
				  {32,33,39,50,80,133,139,150}
				 };
	vector< vector<int> > V;
	for(int i=0;i<4;i++)
	{
		vector<int> v;
		for(int j=0;j<8;j++)
			v.push_back(A[i][j]);
		V.push_back(v);
	}
	vector<int> result = mergeKArrays(V);
	for(int i=0;i<result.size();i++)
		cout<<result[i]<<" ";
	return 0;
}