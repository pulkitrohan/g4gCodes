#include<bits/stdc++.h>
using namespace std;

struct iTree
{
	int low,high,max;
	struct iTree *left,*right;
};

bool cmp(pair<int,int> A,pair<int,int> B)
{
	return A.second > B.second;
}

iTree *insert(iTree *root,pair<int,int> P)
{
	if(!root)
	{
		root = (iTree *)malloc(sizeof(iTree));
		root->low = P.first;
		root->high = P.second;
		root->max = P.second;
		root->left = root->right = NULL;
		return root;
	}
	if(root->low < P.first)
		root->left = insert(root->left,P);
	else
		root->right = insert(root->right,P);
	if(root->max < P.second)
		root->max = P.second;
	return root;
}

iTree *overlapsearch(iTree *root,pair<int,int> P)
{
	if(!root)
		return NULL;
	if(root->low < P.second && P.first < root->high)
		return root;
	if(root->left && root->left->max >= P.first)
		return overlapsearch(root->left,P);
	else
		return overlapsearch(root->right,P);
}

void conflicts(vector<pair<int,int>> V)
{
	iTree *root = NULL;
	root = insert(root,V[0]);
	for(int i=1;i<V.size();i++)
	{
		iTree *temp = overlapsearch(root,V[i]);
		if(temp)
			cout<<temp->low<<" "<<temp->high<<" "<<V[i].first<<" "<<V[i].second<<endl;
		root = insert(root,V[i]);
	}
}

int main(void)
{
	vector<pair<int,int>> V;
	V.push_back(make_pair(1,5));
	V.push_back(make_pair(3,7));
	V.push_back(make_pair(2,6));
	V.push_back(make_pair(10,15));
	V.push_back(make_pair(5,6));
	V.push_back(make_pair(4,100));
	conflicts(V);
	return 0;
}