#include<bits/stdc++.h>
using namespace std;
#define SIZE 26
struct trie
{
	int end;
	struct trie *children[SIZE];
};

struct trie *createNode(void)
{
	struct trie *temp = (struct trie *)malloc(sizeof(struct trie));
	temp->end = 0;
	for(int i=0;i<SIZE;i++)
		temp->children[i] = NULL;
	return temp;
}

void insert(struct trie *T,string key)
{
	for(int level = 0;level < key.length();level++)
	{
		int index = key[level] - 'a';
		if(!T->children[index])
			T->children[index] = createNode();
		T = T->children[index];
	}
	T->end = 1;
}

string PrefixMatching(struct trie *temp,string key)
{
	string S = "";
	int prevMatch = 0;
	for(int level = 0;level < key.length();level++)
	{
		int index = key[level] - 'a';
		if(temp->children[index])
		{
			S += key[level];
			temp = temp->children[index];
			if(temp->end)
				prevMatch = level+1;
		}
		else
			break;
	}
	if(!temp->end)
		return S.substr(0,prevMatch);
	else
		return S;
}



int main(void)
{
	struct trie *T = createNode();
	insert(T,"are");
	insert(T,"area");
	insert(T,"base");
	insert(T,"cat");
	insert(T,"cater");
	insert(T,"children");
	insert(T,"basement");
	cout<<PrefixMatching(T,"caterer")<<endl;
	cout<<PrefixMatching(T,"basemexy")<<endl;
	cout<<PrefixMatching(T,"child")<<endl;
	return 0;
}