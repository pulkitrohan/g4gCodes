#include<bits/stdc++.h>
using namespace std;
#define mp make_pair
#define pii pair<int, int>

int policeThief(vector<char> &V, int k) {
	int ans = 0;
	vector<int> thief;
	vector<int> police;

	for(int i=0;i<V.size();i++) {
		if(V[i] == 'P')
			police.push_back(i);
		else if(V[i] == 'T')
			thief.push_back(i);
	}
	int l = 0, r = 0;
	while(l < thief.size() && r < police.size()) {
		if(abs(thief[l] - police[r]) <= k) {
			ans++;
			l++;
			r++;
		}
		else if(thief[l] < police[r]) 
			l++;
		else
			r++;
	}
	return ans;
}

int main(void) {
	vector<char> V = { 'P', 'T', 'T', 'P', 'T' };
	int k = 2;
	cout<<policeThief(V, k)<<endl;
}