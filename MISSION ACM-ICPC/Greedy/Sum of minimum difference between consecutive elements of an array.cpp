#include<bits/stdc++.h>
using namespace std;

int solve(vector<pair<int, int> > V, int N) {
	int ans = 0;
	int low = V[0].first;
	int high = V[0].second;

	for(auto x : V) {
		if((x.first >= low && x.first <= high) ||
		   (x.second >= low && x.second <= high)) {
		   		if(x.first > low)
		   			low = x.first;
		   		if(x.second < high) 
		   			high = x.second;
		}
		else if(x.first > high) {
			ans += abs(high - x.first);
			low = x.first;
			high = x.second;
		}
		else if(x.second < low) {
			ans += abs(low-x.second);
			low = x.second;
			high = x.second;
		}
	}
	return ans;
}

int main(void) {
	vector<pair<int,int> > V = { {1,3}, {2,5}, {6,8}, {1,2}, {2,3} };
	cout<<solve(V, V.size())<<endl;
	return 0;
}