#include<bits/stdc++.h>
using namespace std;
#define vi vector<int>
#define pii pair<int, int>
#define mp make_pair

bool checkJobs(vi &start, vi &end) {
	vector< pii > V;
	for(int i=0;i<start.size();i++)
		V.push_back(mp(start[i], end[i]));

	sort(V.begin(), V.end());

	int firstJobEnd = V[0].second, secondJobEnd = V[1].second;

	for(int i=2;i<V.size();i++) {
		if(V[i].first > firstJobEnd) {
			secondJobEnd = firstJobEnd;
			firstJobEnd = V[i].second;
		}
		else if(V[i].first > secondJobEnd) {
			secondJobEnd = V[i].second;;
		}
		else
			return false;
	}
	
	return true;
}

int main(void) {
	vi start = {1,2,4};
	vi end = {2,3,5};
	cout<<checkJobs(start, end);
	return 0;
}