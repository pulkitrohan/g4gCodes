#include<bits/stdc++.h>
using namespace std;

double optimum_sequence_jobs(vector<int> &V, double P) {
	sort(V.begin(), V.end());

	int N = V.size();
	double ans = 0;
	for(int i=N-1;i>=0;i--)
		ans += pow((1-P), N-i-1) * V[i];
	return ans;
}

int main(void) {
	vector<int> V = {3, 5, 4, 1, 2, 7, 6, 8, 9, 10};
	double P = 0.10;
	cout<<optimum_sequence_jobs(V, P);
	return 0;
}