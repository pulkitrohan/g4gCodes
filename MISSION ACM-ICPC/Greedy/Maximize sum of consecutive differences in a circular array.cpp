#include<bits/stdc++.h>
using namespace std;

int maxSum(int A[],int N)
{
	int sum = 0;
	sort(A, A+N);

	for(int i=0;i<N/2;i++) {
		sum -= (2 * A[i]);
		sum += (2 * A[N-i-1]);
	}
	return sum;
}

int main(void)
{
	int A[] = {4, 2, 1, 8};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<maxSum(A,N);
	return 0;
}