#include<bits/stdc++.h>
using namespace std;

pair<int, int> maximumCubes(int L, int B, int H) {
	int side = __gcd(L, __gcd(B, H));

	int num = L/side;
	num *= (B/side);
	num *= (H/side);

	return make_pair(side, num);
}

int main(void) {
	int L = 2, B = 4, H = 6;
	pair<int,int> P = maximumCubes(L, B, H);
	cout<<P.first<<" "<<P.second;
	return 0;
}