#include<bits/stdc++.h>
using namespace std;

int maxSum(vector<int> &A, int k) {
	priority_queue<int, vector<int>, greater<int> > PQ;
	for(int x : A)
		PQ.push(x);
	while(k--) {
		int temp = PQ.top();
		PQ.pop();
		temp *= -1;
		PQ.push(temp);
	}
	int sum = 0;
	while(!PQ.empty()) {
		sum += PQ.top();
		PQ.pop();
	}
	return sum;
}


int main(void) {
	vector<int> A = {-2, 0, 5, -1, 2};
	int k = 4;
	cout<<maxSum(A, k);
	return 0;
}