#include<bits/stdc++.h>
using namespace std;

int minProductSubset(vector<int> &A) {
	int N = A.size();
	if(N == 1)
		return A[0];

	int count_neg = 0,count_zero = 0;
	int max_neg = INT_MIN, min_pos = INT_MAX;
	int prod = 1;

	for(int i=0;i<N;i++) {
		if(A[i] == 0) {
			count_zero++;
			continue;
		}
		if(A[i] < 0) {
			count_neg++;
			max_neg = max(max_neg, A[i]);
		}
		prod *= A[i];
	}

	if(count_zero == N)
		return 0;

	if(count_neg & 1) {
		// All zeroes and negative
		if(count_neg == 1 && count_zero > 0 && count_zero == N-1)
			return 0;
		prod /= max_neg;
	}

	return prod;
}


int main(void) {
	vector<int> A = { -1, -1, -2, 4, 3};
	cout<<minProductSubset(A);
	return 0;
}