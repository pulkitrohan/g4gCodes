#include<bits/stdc++.h>
using namespace std;
#define vi vector<int>
#define mp make_pair
typedef pair<int, pair<int, int> > job;

bool cmp_pair(job A, job B) {
	return A.second.first * B.second.second > A.second.second * B.second.first;
}

void printOptimal(vi &L, vi &T) {
	vector<job> list;
	for(int i=0;i<L.size();i++) 
		list.push_back(mp(i+1, mp(L[i], T[i])));
	stable_sort(list.begin(), list.end(), cmp_pair);
	for(int i=0;i<list.size();i++) {
		cout<<list[i].first<<" ";
	}
}


int main(void) {
	vector<int> L = {1,2,3,5,6};
	vector<int> T = {2,4,1,3,2};
	printOptimal(L, T);
	return 0;
}