#include<bits/stdc++.h>
using namespace std;

int winner(int A[], int N, int k) {
	if(k >= N-1)
		return N;
	int best = 0, times = 0;
	for(int i=0;i<N;i++) {
		if(A[i] > best) {
			best = A[i];
			if(i)
				times = 1;
		}
		else
			times++;
		if(times >= k)
			return best;
	}
	return best;
}

int main(void)
{
	int A[] = {2,1,3,4,5};
	int N = sizeof(A)/sizeof(A[0]);
	int k = 2;
	cout<<winner(A, N, k);
	return 0;
}