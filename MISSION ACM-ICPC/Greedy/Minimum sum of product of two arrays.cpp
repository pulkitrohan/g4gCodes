#include<bits/stdc++.h>
using namespace std;
#define vi vector<int>

int minProduct(vi &A, vi &B, int K) {
	int diff = 0, res = 0, temp;

	for(int i=0;i<A.size();i++) {
		int product = A[i] * B[i];
		res += product;

		if((product < 0 && B[i] < 0) || (product > 0 && A[i] < 0))  {
			temp = (A[i] + 2*K) * B[i];
		}

		if((product < 0 && A[i] < 0) || (product > 0 && A[i] > 0)) {
			temp = (A[i] - 2*K) * B[i];
		}
		diff = max(diff, abs(product-temp));
	}
	return res - diff;
}

int main(void) {
	vector<int> A = {2,3,4,5,4};
	vector<int> B = {3,4,2,3,2};
	int K = 3;
	cout<<minProduct(A, B, K);
	return 0;
}