#include<bits/stdc++.h>
using namespace std;

int minOperation(vector<int> &V, int k) {
	int ans = 0;
	for(int x: V) {
		if(x != 1 && x > k)
			ans += min(x%k, ((k-x)%k + k)%k);
		else
			ans += k-x;
	}
	return ans;
}

int main(void) {
	vector<int> V = {5, 8, 9};
	int k = 5;
	cout<<minOperation(V, k)<<endl;
	return 0;
}