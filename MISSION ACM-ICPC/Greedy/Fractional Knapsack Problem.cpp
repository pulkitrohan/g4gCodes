#include<bits/stdc++.h>
using namespace std;
#define mp make_pair
#define pii pair<int, int>

bool cmp(pii P1, pii P2) {
	return ((double)P1.first/P1.second) > ((double)P2.first/P2.second);
}

double fractionalKnapsack(int W, vector<pii> &V) {
	sort(V.begin(), V.end(), cmp);
	double ans = 0;
	int curWeight = 0;
	for(int i=0;i<V.size();i++) {
		if(curWeight + V[i].second <= W) {
			curWeight += V[i].second;
			ans += V[i].first;
			cout<<ans<<endl;

		}
		else {
			ans += V[i].first * ((double) W - curWeight) / V[i].second;
			break;
		}
	}
	return ans;
}

int main(void) {
	int W = 50;
	vector<pair<int, int> > V = { mp(60, 10), mp(100, 20), mp(120, 30) };
	cout<<fractionalKnapsack(W, V);
}