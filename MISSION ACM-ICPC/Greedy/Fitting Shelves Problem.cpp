#include<bits/stdc++.h>
using namespace std;

void minSpacePreferLarge(int wall, int m, int n) {
	if(wall < n) {
		if(wall < m) {
			cout<<0<<" "<<0<<" "<<wall;
			return;
		}
		else {
			cout<<wall/m<<" "<<0<<" "<<wall%m;
			return;
		}
	}
	int num_n = 0, num_m = 0, min_empty = wall;
	int p = 0, q = 0, rem;
	wall += n;
	while(wall >= n) {
		q += 1;
		wall -= n;

		p = wall/m;
		rem = wall%m;

		if(rem <= min_empty) {
			min_empty = rem;
			num_n = q;
			num_m = p;
		}
	}
	cout<<num_m<<" "<<num_n-1<<" "<<min_empty;
}

int main(void) {
	int wall = 12, m = 3, n = 5;
	minSpacePreferLarge(wall, m, n);
}