#include<bits/stdc++.h>
using namespace std;
#define MAX_CHAR 256

int nextChar(int freq[], int distance[]) {
	int max = INT_MIN;
	for(int i=0;i<MAX_CHAR;i++)
		if(distance[i] <= 0 && freq[i] > 0 && (max == INT_MIN || freq[i] > freq[max]))
			max = i;
	return max;
}

bool canBeRearranged(string &S, string &ans, int d) {

	int freq[MAX_CHAR] = {0};

	for(char x : S)
		freq[x]++;

	int distance[MAX_CHAR] = {0};

	for(int i=0;i<S.length();i++) {
		
		int j = nextChar(freq, distance);

		if(j == INT_MIN)
			return false;

		ans += j;
		freq[j]--;
		distance[j] = d;
		for(int i=0;i<MAX_CHAR;i++)
			distance[i]--;
	}
	return true;

}
int main(void) {
	string S = "aaaabbbcc";
	string ans;
	if(canBeRearranged(S, ans, 2))
		cout<<ans<<endl;
	else
		cout<<"Cannot be Rearranged";
	return 0;
}