#include<bits/stdc++.h>
using namespace std;

int findMinimumAdjacentSwaps(int A[], int N) {
	vector<bool> visited(N+1, false);
	int ans = 0;
	for(int i=0;i<2*N;i++) {
		if(!visited[A[i]]) {
			visited[A[i]] = true;
			int count = 0;
			for(int j=i+1;j<2*N;j++) {
				if(!visited[A[j]])
					count++;
				else if(A[i] == A[j])
					ans += count;
			}
		}
	}
	return ans;
}

int main(void) {
	int A[] = {1,2,3,3,1,2};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<findMinimumAdjacentSwaps(A, N/2);
	return 0;
}