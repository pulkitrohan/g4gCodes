#include<bits/stdc++.h>
using namespace std;
#define vi vector<int>

int minElements(vi &V) {

	int sum = 0;
	for(auto x : V)
		sum += x;
	int halfSum = sum/2;

	sort(V.begin(), V.end(), greater<int>());

	int ans = 0, curSum = 0;
	for(auto x : V) {
		curSum += x;
		ans++;
		if(curSum > halfSum)
			return ans;
	}
	return ans;
}

int main(void) {
	vector<int> V = {2,1,2};
	cout<<minElements(V)<<endl;
	return 0;
}