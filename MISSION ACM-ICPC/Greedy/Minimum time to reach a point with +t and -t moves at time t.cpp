#include<bits/stdc++.h>
using namespace std;

int minTime(int N) {
	int time = 0, sum = 0;
	while(sum < N) {
		time++;
		sum += time;
	}
	return time;
}

int main(void) {
	int N = 6;
	cout<<minTime(N);
	return 0;
}