#include<bits/stdc++.h>
using namespace std;
#define vi vector<int>

int DecreasingArray(vi &A) {
	int sum = 0, diff = 0;
	priority_queue<int, vector<int>, greater<int> > PQ;

	for(int i=0;i<A.size();i++) {
		if(!PQ.empty() && PQ.top() < A[i]) {
			sum += A[i] - PQ.top();
			PQ.pop();
		}
		PQ.push(A[i]);
	}
	return sum;
}

int main(void) {
	vector<int> A = {1, 5, 5, 5};
	cout<<DecreasingArray(A);
	return 0;
}