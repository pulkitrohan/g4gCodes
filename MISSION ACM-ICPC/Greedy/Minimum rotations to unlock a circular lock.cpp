#include<bits/stdc++.h>
using namespace std;

int minRotation(int input, int unlockCode) {

	int ans = 0;
	while(input || unlockCode) {
		int inputDigit = input%10;
		int codeDigit = unlockCode%10;

		ans += min(abs(inputDigit - codeDigit), 10 - abs(inputDigit - codeDigit));

		input /= 10;
		unlockCode /= 10;
	}
	return ans;

}
int main(void) {
	int input = 28756;
	int unlockCode = 98234;
	cout<<minRotation(input, unlockCode);
	return 0;
}