#include<bits/stdc++.h>
using namespace std;
#define pii pair<int, int>
#define mp make_pair

int buyMaximumProducts(vector<int> &V, int k) {

	vector< pii > A;
	for(int i=0;i<V.size();i++)
		A.push_back(mp(V[i], i+1));

	sort(A.begin(), A.end());

	int ans = 0;
	for(pii p : A) {
		int count = min(p.second, k/p.first);
		ans += count;
		k -= p.first * count;
	}
	return ans;
}

int main(void) {
	vector<int> V = {10,7,19};
	cout<<buyMaximumProducts(V, 45)<<endl;
	return 0;
}