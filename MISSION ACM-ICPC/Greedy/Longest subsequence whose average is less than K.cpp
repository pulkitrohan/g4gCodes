#include<bits/stdc++.h>
using namespace std;

void longestSubsequence(int A[], int N, int Q[], int M) {
	sort(A, A+N);
	vector<int> B;
	int sum = 0;
	for(int i=0;i<N;i++) {
		sum += A[i];
		double avg = (double)(sum)/(double)(i+1);
		B.push_back((int)(avg+1));
	}
	sort(B.begin(), B.end());
	for(int i=0;i<M;i++) {
		int k = Q[i];
		cout<<"Query::"<<i+1<<" :"<<upper_bound(B.begin(), B.end(), k) - B.begin()<<endl;;
	}
}

int main(void) {
	int A[] = {1,3,2,5,4};
	int N = sizeof(A)/sizeof(A[0]);
	int Q[] = {4,2,1,5};
	int M = sizeof(Q)/sizeof(Q[0]);
	longestSubsequence(A, N, Q, M);
	return 0;
}