#include<bits/stdc++.h>
using namespace std;

int find(vector<int> &A, int pos, vector<bool> &isRunning) {
	int M = A.size();
	vector<int> d(M+1, 0);
	for(int i=M;i>pos;i--) {
		if(isRunning[A[i]]) 
			d[A[i]] = i;
	}
	int maxPos = 0;
	for(int ele : d)
		maxPos = max(ele, maxPos);
	return maxPos;
}

int minCost(vector<int> &A, int N) {
	int M = A.size();
	vector<vector<int> > freq[M];
	vector<int> newVec(M+1, 0);
	freq[M-1] = newVec;
	for(int i=M-2;i>=0;i--) {
		vector<int> nv;
		nv = freq[i+1];
		nv[arr[i+1]] += 1;
		freq[i] = nv;
	}

	int ans = 0, trueCount = 0;
	vector<bool> isRunning(M+1, false);
	for(int i=0;i<M;i++) {

		int ele = A[i];
		if(isRunning[ele])
			continue;

		if(trueCount < N) {
			count++;
			trueCount++;
			isRunning[ele] = true;
		}
		else {

			int minValue = INT_MAX, minIndex = 0;

			for(int j=1;j<=M;j++) {
				if(isRunning[j] && minValue > freq[i][j]) {
					minIndex = j;
					minValue = freq[i][j];
				}
			}

			if(minValue == 0) {
				isRunning[minIndex] = false;
				isRunning[ele] = true;
				cost += 1;
			}
			else {
				int farpos = find(A, i, isRunning);
				isRunning[A[farpos]] = false;
				isRunning[ele] = true;
				cost += 1;
			}
		}
	}
	return cost;

}

int main(void) {
	int N = 3, M = 6;
	vector<int> A = {1, 2, 1, 3, 4, 1};
	cout<<minCost(A,N);
	return 0;
}