#include<bits/stdc++.h>
using namespace std;

void split(int X, int N) {
	if(N > X) {
		cout<<"-1\n";
		return;
	}
	int divide = X/N;
	if(X%N == 0) {
		for(int i=0;i<N;i++)
			cout<<divide<<" ";
		return;
	}
	int num = N - X%N;
	for(int i=0;i<N;i++)
		if(i >= num)
			cout<<divide + 1<<" ";
		else
			cout<<divide<<" ";

}

int main(void) {
	int X = 23, N = 5;
	split(X, N);
	return 0;
}