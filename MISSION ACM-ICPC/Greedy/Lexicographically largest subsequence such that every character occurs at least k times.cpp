#include<bits/stdc++.h>
using namespace std;
#define pii pair<int, int>
#define mp make_pair

string subsequence(string S, int k) {

	int last = 0, newLast;
	string T;
	int hash[26] = {0};
	for(char ch = 'z'; ch >= 'a';ch--) {
		int count = 0;
		for(int i=last;i<S.length();i++)
			if(S[i] == ch)
				count++;
		if(count >= k) {
			for(int i=last;i<S.length();i++) {
				if(S[i] == ch) {
					T += ch;
					newLast = i;
				}
			}
			last = newLast;
		}
	}
	return T;
}

int main(void) {
	string S = "banana";
	int k = 2;
	cout<<subsequence(S, k)<<endl;
	return 0;
}