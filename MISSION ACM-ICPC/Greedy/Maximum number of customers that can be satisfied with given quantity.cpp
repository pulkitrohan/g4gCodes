#include<bits/stdc++.h>
using namespace std;
#define vi vector<int>

void solve(int N, int d, int a, int b, int A[][2]) {

	vector< pair<int, int> > V;

	for(int i=0;i<N;i++)
		V.push_back(make_pair(A[i][0] * a + A[i][1] * b, i+1));
	sort(V.begin(), V.end());

	vector<int> ans;
	for(auto x : V) {
		if(x.first <= d) {
			ans.push_back(x.second);
			d -= x.first;
		}
	}
	cout<<ans.size()<<endl;
	for(auto x : ans)
		cout<<x<<" ";
	
}

int main(void) {
	int n = 5; 
    long d = 5; 
    int a = 1, b = 1; 
    int arr[][2] = {{2, 0}, 
                    {3, 2}, 
                    {4, 4}, 
                    {10, 0}, 
                    {0, 1}}; 
      
    solve(n, d, a, b, arr);
	return 0;
}