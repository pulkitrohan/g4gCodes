#include<bits/stdc++.h>
using namespace std;

int countSwaps(string S) {
	vector<int> pos;
	for(int i=0;i<S.length();i++)
		if(S[i] == '[')
			pos.push_back(i);
	int count = 0, p = 0, sum = 0;

	for(int i=0;i<S.length();i++) {
		if(S[i] == '[') {
			count++;
			p++;
		}
		else if(S[i] == ']')
			count--;
		if(count < 0) {
			sum += pos[p]-i;
			swap(S[pos[p]], S[i]);
			p++;
			count = 1;
		}
	}
	return sum;
}

int main(void) {
	string S = "[]][][";
	cout<<countSwaps(S);
}