#include<bits/stdc++.h>
using namespace std;

int maxTime(vector<int> &mice, vector<int> &holes) {
	if(mice.size() != holes.size())
		return INT_MAX;
	sort(mice.begin(), mice.end());
	sort(holes.begin(), holes.end());
	int maxDiff = 0;
	for(int i=0;i<mice.size();i++)
		maxDiff = max(maxDiff, abs(mice[i]-holes[i]));
	return maxDiff;
}

int main(void) {
	vector<int> mice = {4, -4, 2};
	vector<int> holes = {4, 0, 5};
	cout<<maxTime(mice, holes);
}