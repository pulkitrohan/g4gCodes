#include<bits/stdc++.h>
using namespace std;

bool cmp(pair<int,int> P1,pair<int,int> P2)
{
	if(P1.second == P2.second)
		return (P1.first < P2.first);
	return (P1.second < P2.second);
}

void printMaxActivities(vector<pair<int,int> > V)
{	
	sort(V.begin(),V.end(),cmp);
	cout<<V[0].first<<" "<<V[0].second<<endl;
	int j = 0;
	for(int i=1;i<V.size();i++)
	{
		if(V[i].first >= V[j].second)
		{
			cout<<V[i].first<<" "<<V[i].second<<endl;
			j = i;
		}
	}
}

int main(void)
{
	int s[] = {1,3,0,5,8,5};
	int f[] = {2,4,6,7,9,9};
	int N = sizeof(s)/sizeof(s[0]);
	vector<pair<int,int> > V;
	for(int i=0;i<N;i++)
		V.push_back(make_pair(s[i],f[i]));
	printMaxActivities(V);
	return 0;
}