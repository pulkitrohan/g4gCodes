#include<bits/stdc++.h>
using namespace std;

int calculate(vector<int> &A) {
	sort(A.begin(), A.end());
	vector<int> copy;
	for(int i=0,j=A.size()-1;i < j;i++,j--)
		copy.push_back(A[i] + A[j]);

	int minNum = *min_element(copy.begin(), copy.end());
	int maxNum = *max_element(copy.begin(), copy.end());
	return abs(minNum - maxNum);
}

int main(void) {
	vector<int> A = {2,6,4,3};
	cout<<calculate(A);
	return 0;
}