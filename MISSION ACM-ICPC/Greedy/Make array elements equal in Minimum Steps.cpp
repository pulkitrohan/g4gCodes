#include<bits/stdc++.h>
using namespace std;

int steps(int N, int M) {
	if(N == 1)
		return 0;
	else if(N == 2)
		return M;
	return 2*M + (N-3);
}

int main(void) {
	int N = 4, M = 4;
	cout<<steps(N, M);
	return 0;
}