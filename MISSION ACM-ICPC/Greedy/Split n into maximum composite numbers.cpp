#include<bits/stdc++.h>
using namespace std;
#define pii pair<int, int>
#define mp make_pair

int count(int N) {
	if(N < 4)	return -1;

	int rem = N%4;

	if(rem == 0)
		return N/4;

	if(rem == 1) {
		if(N < 9)	return -1;
		return (N-9)/4 + 1;
	}

	if(rem == 2)
		return (N-6)/4 + 1;

	if(rem == 3) {
		if(N < 15) return -1;
		return (N-15)/4 + 2;
	}
}

int main(void) {
	int N = 143;
	cout<<count(N)<<endl;
	return 0;
}