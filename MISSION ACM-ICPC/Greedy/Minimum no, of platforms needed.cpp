#include<bits/stdc++.h>
using namespace std;

int FindMinPlatforms(int *A,int *D,int N)
{
	sort(A,A+N);
	sort(D,D+N);
	int i = 1,j = 0;
	int count = 1,result = 1;
	while(i < N && j < N)
	{
		if(A[i] < D[j])
		{
			i++;
			count++;
			result = max(count,result);
		}
		else
		{
			count--;
			j++;
		}
	}
	return result;
}

int main(void)
{	
	int A[] = {900,940,950,1100,1500,1800};
	int D[] = {910,1200,1120,1130,1900,2000};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<FindMinPlatforms(A,D,N);
	return 0;
}