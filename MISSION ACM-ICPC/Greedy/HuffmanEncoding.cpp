#include<bits/stdc++.h>
using namespace std;

struct node
{
	char ch;
	int freq;
	struct node *left,*right;
};

node *createNode(char c,int f)
{
	node *temp = (node *)malloc(sizeof(node));
	temp->ch = c;
	temp->freq = f;
	temp->left = temp->right = NULL;
	return temp;
}
class cmp
{
	public:
		bool operator()(node *A,node *B)
		{
			return (A->freq > B->freq);
		}
};

void print_Tree(node *root,string path)
{
	if(root)
	{
		if(root->ch)
			cout<<root->ch<<"-->"<<path<<endl;
		print_Tree(root->left,path + "0");
		print_Tree(root->right,path + "1");
	}
}

int main(void)
{
	priority_queue<node *,vector<node *>,cmp> H;
	H.push(createNode('a',5));
	H.push(createNode('b',9));
	H.push(createNode('c',12));
	H.push(createNode('d',13));
	H.push(createNode('e',16));
	H.push(createNode('f',45));
	
	while(H.size() > 1)
	{
		node *A = H.top();
		H.pop();
		node *B = H.top();
		H.pop();
		node *temp = createNode('\0',A->freq+B->freq);
		temp->left = A;
		temp->right = B;
		H.push(temp);
	}
	string S = "";
	print_Tree(H.top(),S);
	return 0;
}