#include<bits/stdc++.h>
using namespace std;

bool isPossible(int V[], int N) {

	int cp[N];
	copy(V, V + N, cp);
	sort(cp, cp+N);
	for(int i=0;i<N;i++)
		if(!(V[i] == cp[i]) && !(V[N-i-1] == cp[i]))
			return false;
	return true;
}

int main(void) {
	int V[] = {1,7,6,4,5,3,2,8};
	int N = sizeof(V)/sizeof(V[0]);
	if(isPossible(V, N)) 
		cout<<"Yes";
	else
		cout<<"No";
	return 0;
}