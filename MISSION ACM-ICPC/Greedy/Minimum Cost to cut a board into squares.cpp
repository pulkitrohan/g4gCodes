#include<bits/stdc++.h>
using namespace std;

int minimumCostOfBreaking(vector<int> &X, vector<int> &Y) {
	sort(X.begin(), X.end(), greater<int>());
	sort(Y.begin(), Y.end(), greater<int>());

	int h = 1, v = 1;
	int i = 0, j = 0;
	int ans = 0;

	while(i < X.size() && j < Y.size()) {
		if(X[i] > Y[j]) {
			ans += X[i++]*v;
			h++;
		}
		else {
			ans += Y[j++]*h;
			v++;
		}
	}
	int total = 0;
	while(i < X.size())
		total += X[i++];
	ans += total * v;

	total = 0;
	while(j < Y.size()) 
		total += Y[j++];
	ans += total * h;
	
	return ans;
}

int main(void) {
	vector<int> X = {2,1,3,1,4};
	vector<int> Y = {4,1,2};
	cout<<minimumCostOfBreaking(X, Y)<<endl;
}