#include<bits/stdc++.h>
using namespace std;

void findLAndB(int A[], int N) {
	sort(A, A+N);
	vector<double> V;
	for(int i=0;i<N-1;i++) {
		if(A[i] == A[i+1]) {
			V.push_back(A[i]);
			i++;
		}
	}

	double L = V[0];
	double B = V[1];
	for(int i=2;i<V.size();i++) {
		if((L/B + B/L) > (V[i]/V[i-1] + V[i-1]/V[i])) {
			L = V[i];
			B = V[i-1];
		}
	}
	cout<<L<<" "<<B<<endl;
}

int main(void) {
	int A[] = {4, 3, 5, 4, 3, 5, 7};
	int N = sizeof(A)/sizeof(A[0]);
	findLAndB(A, N);
	return 0;
}