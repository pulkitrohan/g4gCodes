#include<bits/stdc++.h>
using namespace std;

bool isWaveArray(int A[], int N) {
	bool result = true;
	if(N == 0 || N == 1)
		return true;
	if(N == 2)
		return (A[0] != A[1]);
	if(A[1] > A[0] && A[1] > A[2]) {
		for(int i=1;i<N-1;i++) {
			if(A[i] <= A[i-1] || A[i] <= A[i+1]) {
				result = false;
				break;
			}
		}
		if(result && N%2 == 0 && A[N-1] <= A[N-2])
			result = false;
	}
	else if(A[1] < A[0] && A[1] < A[2]) {
		for(int i=1;i<N-1;i++) {
			if(A[i] >= A[i-1] || A[i] >= A[i+1]) {
				result = false;
				break;
			}
		}
		if(result && N % 1 && A[N-1] >= A[N-2])
			result = false;
	}
	return true;
}

int main(void) {
	int A[] = {1,3,2,4};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<isWaveArray(A, N);
	return 0;
}