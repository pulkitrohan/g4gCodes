#include<bits/stdc++.h>
using namespace std;

int longestFibSubseq(int A[], int N) {
	unordered_set<int> S(A, A+N);
	int maxLen = 0;
	for(int i=0;i<N;i++) {
		for(int j=i+1;j<N;j++) {
			int x = A[i];
			int y = A[i] + A[j];
			int length = 2;
			while(S.find(y) != S.end()) {
				int z = x + y;
				x = y;
				y = z;
				maxLen = max(maxLen, ++length);
			}
		}
	}
	return maxLen >= 3 ? maxLen : 0;
}

int main(void) {
	int A[] = {1,2,3,4,5,6,7,8};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<longestFibSubseq(A, N);
	return 0;
}