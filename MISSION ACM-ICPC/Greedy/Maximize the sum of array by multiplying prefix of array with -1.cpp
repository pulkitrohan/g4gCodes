#include<bits/stdc++.h>
using namespace std;

void maxSum(int A[], int N) {
	int sum = 0;
	vector<int> V;
	for(int i=0;i<N;i++) {
		sum += abs(A[i]);
		if(A[i] >= 0)
			continue;
		if(i == 0)
			V.push_back(i+1);
		else {
			V.push_back(i+1);
			V.push_back(i);
		}
	}
	cout<<sum<<endl;
	for(auto x: V)
		cout<<x<<" ";
}

int main(void) {
	int A[] = {1, -2, -3, 4};
	int N = sizeof(A)/sizeof(A[0]);
	maxSum(A, N);
	return 0;
}