#include<bits/stdc++.h>
using namespace std;

int maxSum(vector<int> &V) {
	sort(V.begin(), V.end());

	int sum = 0;
	for(int i=0;i<V.size();i++)
		sum += V[i]*i;
	return sum;
}

int main(void) {
	vector<int> V = {3,5,6,1};
	cout<<maxSum(V)<<endl;
}