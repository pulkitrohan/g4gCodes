#include<bits/stdc++.h>
using namespace std;

void construct(int N, pair<double, double> ans) {
	if(ans.first == -1) {
		cout<<"Not possible";
		return;
	}
	double r = ans.second/ans.first;
	for(int i=1;i<N;i++) {
		cout<<ans.first * pow(r, i-1)<<" ";
	}
}

void findMinimumOperations(double A[], int N) {
	int ans = INT_MAX;
	int op[] = {-1, 0, 1};
	int M = sizeof(op)/sizeof(op[0]);
	int pos1 = -1, pos2 = -1;
	for(int i=0;i<M;i++) {
		for(int j=0;j<M;j++) {
			double a1 = A[1] + op[i];
			double a2 = A[2] + op[j];

			int temp = abs(a1 - A[1]) + abs(a2 - A[2]);
			if(a1 == 0 || a2 == 0)
				continue;

			double r = a2/a1;

			for(int k=3;k<N;k++) {
				double ak = a1 * pow(r, k-1);
				if(A[k] == ak)
					continue;
				else if(A[k] + 1 == ak || A[k] - 1 == ak)
					temp++;
				else {
					temp = INT_MAX;
					break;
				}
			}
			if(temp < ans) {
				ans = temp;
				pos1 = a1;
				pos2 = a2;
			}

		}
	}
	if(ans == -1) {
		cout<<"-1";
		return;
	}
	cout<<"Total operations:"<<ans<<endl;
	construct(N, make_pair(pos1, pos2));
}

int main(void) {
	double A[] = {0, 7, 20, 49, 125};
	int N = sizeof(A)/sizeof(A[0]);
	findMinimumOperations(A, N);
}