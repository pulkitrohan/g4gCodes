#include<bits/stdc++.h>
using namespace std;

int maximumHeight(vector<int> &V) {
	return floor((-1 + sqrt(1 + (8 * V.size())))/2);
}

int main(void) {
	vector<int> V = {40,100,20,30};
	cout<<maximumHeight(V);
	return 0;
}