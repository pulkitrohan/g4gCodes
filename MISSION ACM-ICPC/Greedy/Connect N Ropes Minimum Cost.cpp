#include<bits/stdc++.h>
using namespace std;

int MinCost(int *A,int N)
{
	priority_queue<int,vector<int>,greater<int> > H(A,A+N);
	int cost = 0;
	while(H.size() > 1)
	{
		int A = H.top();
		H.pop();
		int B = H.top();
		H.pop();
		cost += A+B;
		H.push(A+B);
	}
	return cost;
}

int main(void)
{
	int len[] = {4,3,2,6};
	int N = sizeof(len)/sizeof(len[0]);
	cout<<MinCost(len,N);
	return 0;
}