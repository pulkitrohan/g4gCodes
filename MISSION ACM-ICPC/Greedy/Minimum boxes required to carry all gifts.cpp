#include<bits/stdc++.h>
using namespace std;

int numBoxes(int A[], int N, int K) {
	sort(A, A+N);

	int i = 0, j = N-1;
	int ans = 0;
	while(i <= j) {
		ans++;
		if(A[i] + A[j] <= K)
			i++;
		j--;
	}
	return ans;
}

int main(void) {
	int A[] = {3,2,2,1};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<numBoxes(A, N, 3);
	return 0;
}