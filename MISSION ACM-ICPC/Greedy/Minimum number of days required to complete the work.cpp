#include<bits/stdc++.h>
using namespace std;
#define vi vector<int>
#define pii pair<int, int>

int minimumDays(vi &D1, vi &D2, int N) {
	
	vector< pii > V;

	for(int i = 0;i < N; i++)
		V.push_back(make_pair(D1[i], D2[i]));
	sort(V.begin(), V.end());

	int ans = INT_MIN;

	for(int i=0;i<N;i++) 
		ans = V[i].second >= ans ? V[i].second : V[i].first;

	return ans;
}

int main(void) {
	vi D1 = {6, 5, 4};
	vi D2 = {1, 2, 3};
	int N = D1.size();
	cout<<minimumDays(D1, D2, N);
	return 0;
}