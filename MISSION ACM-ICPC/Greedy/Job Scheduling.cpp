//https://www.youtube.com/watch?v=yHsDLU3ZqNM
#include<bits/stdc++.h>
using namespace std;

struct Job
{
	char id;
	int dead,profit;
};

bool cmp(Job A,Job B)
{
	return (A.profit > B.profit);
}

void printJobScheduling(Job *A,int N)
{
	sort(A,A+N,cmp);
	int result[N],slot[N];
	memset(slot,0,sizeof(slot));
	for(int i=0;i<N;i++)
	{	
		for(int j=min(N,A[i].dead)-1;j>=0;j--)
		{
			if(!slot[j])
			{
				result[j] = i;
				slot[j] = 1;
				break;
			}
		}
	}
	for(int i=0;i<N;i++)
		if(slot[i])
			cout<<A[result[i]].id<<" ";
}

int main(void)
{
	Job A[5] = { {'a',2,100},{'b',1,19},{'c',2,27},{'d',1,25},{'e',3,15} };
	int N = sizeof(A)/sizeof(A[0]);
	printJobScheduling(A,N);
	return 0;
}