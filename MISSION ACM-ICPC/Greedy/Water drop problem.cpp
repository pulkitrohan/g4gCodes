#include<bits/stdc++.h>
using namespace std;

int drops(int A[],int speed[], int L, int N) {
	vector<pair<int, double> > V;
	for(int i=0;i<N;i++) {
		int d = L - A[i];
		V.push_back(make_pair(A[i], d * 1.0 / speed[i]));
	}
	sort(V.begin(), V.end());
	int ans = 0;
	stack<double> S;
	for(int i=V.size()-1;i>=0;i--) {
		if(S.empty())
			S.push(V[i].second);
		if(V[i].second > S.top()) {
			S.pop();
			ans++;
			S.push(V[i].second);
		}
	}
	if(!S.empty()) {
		S.pop();
		ans++;
	}
	return ans;
}

int main(void) {
	int L = 12;
	int A[] = {10, 8, 0, 5, 3};
	int speed[] = {2,4,1,1,3};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<drops(A, speed, L, N)<<endl;
	return 0;
}