#include<bits/stdc++.h>
using namespace std;

int minSwaps(string S) {
	int even_0 = 0, even_1 = 0, odd_0 = 0, odd_1 = 0;
	for(int i=0;i<S.length();i++) {
		if(i & 1) {
			(S[i] == '0') ? odd_0++ : odd_1++;
		}
		else {
			(S[i] == '0') ? even_0++ : even_1++;
		}
	}
	if(abs(even_0 + odd_0 - even_1 - odd_1) > 1)
		return -1;
	return min(
		// Starts with 0
			min(even_0, odd_1),
		// Starts with 1
			min(even_1, odd_0)
		);
}

int main(void) {
	string S = "000111";
	cout<<minSwaps(S);
	return 0;
}