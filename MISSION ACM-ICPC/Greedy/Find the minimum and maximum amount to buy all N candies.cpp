#include<bits/stdc++.h>
using namespace std;

int findMinimum(vector<int> &V, int k) {
	int ans = 0;
	int N = V.size();
	for(int i=0;i<N;i++) {
		ans += V[i];
		N -= k;
	}
	return ans;
}

int findMaximum(vector<int> &V, int k) {
	int ans = 0;
	int index = 0;
	int N = V.size();
	for(int i=N-1;i>=index;i--) {
		ans += V[i];
		index += k;
	}
	return ans;
}


int main(void) {
	vector<int> V = { 3, 2, 1, 4 };
	int k = 2;
	sort(V.begin(), V.end());
	cout<<findMinimum(V, k)<<" "<<findMaximum(V, k)<<endl;
}