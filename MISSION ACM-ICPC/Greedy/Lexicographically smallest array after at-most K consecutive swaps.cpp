#include<bits/stdc++.h>
using namespace std;

void kSwapPermutation(int A[],int N,int k)
{
	for(int i=0;i<N-1 && k > 0;i++) {
		int index = i;
		for(int j = i+1;j<N;j++) {
			if(i + k < j)
				break;
			if(A[j] < A[index])
				index = j;
		}
		for(int j=index;j>i;j--)
			swap(A[j], A[j-1]);
		k -= index-i;
	}
	for(int i=0;i<N;i++)
		cout<<A[i]<<" ";

}

int main(void)
{
	int A[] = {7, 6, 9, 2, 1};
	int N = sizeof(A)/sizeof(A[0]);
	int k = 3;
	kSwapPermutation(A,N,k);
	return 0;
}