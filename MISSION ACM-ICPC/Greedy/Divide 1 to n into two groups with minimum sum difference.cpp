#include<bits/stdc++.h>
using namespace std;

void findTwoGroups(int N) {
	int sum = N * (N+1)/2;
	int group1Sum = sum/2;

	vector<int> A, B;
	for(int i=N;i>0;i--) {
		if(group1Sum >= i) {
			A.push_back(i);
			group1Sum -= i;
		}
		else 
			B.push_back(i);
	}
	for(auto x : A) cout<<x<<" ";
	cout<<endl;
	for(auto x : B) cout<<x<<" ";

}

int main(void) {
	int N = 5;
	findTwoGroups(N);
	return 0;
}