#include<bits/stdc++.h>
using namespace std;

int minOps(vector<int> &A, int k) {
	int maxElement = *max_element(A.begin(), A.end());
	int ans = 0;
	for(auto x : A) {
		if((maxElement-x)%k != 0)
			return -1;
		ans += (maxElement-x)/k;
	}
	return ans;
}

int main(void) {
	vector<int> A = {21, 33, 9, 45, 63};
	int k = 6;
	cout<<minOps(A,k);
	return 0;
}