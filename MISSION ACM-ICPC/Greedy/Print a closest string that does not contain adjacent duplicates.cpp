#include<bits/stdc++.h>
using namespace std;

string noAdjacentDup(string &S) {
	int N = S.length();
	for(int i=1;i<N;i++) {
		if(S[i] == S[i-1]) {
			S[i] = 'a';
			while(S[i] == S[i-1] || (i + 1 < N && S[i] == S[i+1]))
				S[i]++;
			i++;
		}
	}
	return S;
}

int main(void) {
	string S = "geeksforgeeks";
	cout<<noAdjacentDup(S);
	return 0;
}