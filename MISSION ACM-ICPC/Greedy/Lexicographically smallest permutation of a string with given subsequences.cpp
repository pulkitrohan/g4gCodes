#include<bits/stdc++.h>
using namespace std;
#define vi vector<int>
#define pii pair<int, int>

int nx = 0, ny = 0;

bool isValid(string S, int p, int q) {

	for(auto ch : S)
		ch == 'x' ? nx++ : ny++;

	return nx * ny == p + q;
}

string smallestPermutation(string S, int p, int q) {

	if(!isValid(S, p, q))
		return "Impossible";

	sort(S.begin(), S.end());
	int a = nx*ny, b = 0;

	if(a == p && b == q)
		return S;

	int i;
	while(1) {

		for(i=0;i<S.length()-1;i++) 
			if(S[i] == 'x' && S[i+1] == 'y')
				break;

		for(int j=i;j<S.length()-1;j++) {
			if(S[j] == 'x' && S[j+1] == 'y') {
				swap(S[j], S[j+1]);
				a--, b++;

				if(a == p && b == q)
					return S;
			}
		}
	}

}

int main(void) {
	string S = "yxxyx";
	int p = 3, q = 3;
	cout<<smallestPermutation(S, p, q)<<endl;
	return 0;
}