#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right;
};

Tree *newNode(int data)
{
	Tree *node = (Tree *)malloc(sizeof(Tree *));
	node->data = data;
	node->left = node->right = NULL;
	return node;
}

bool isLeaf(Tree *node)
{
	return !node->left && !node->right;
}

bool Issame(Tree *A,Tree *B)
{
	stack<Tree *> S1,S2;
	S1.push(A);
	S2.push(B);
	while(!S1.empty() || !S2.empty())
	{
		if(S1.empty() || S2.empty())
			return false;
		Tree *temp1 = S1.top();
		S1.pop();
		while(temp1 && !isLeaf(temp1))
		{
			if(temp1->right)
				S1.push(temp1->right);
			if(temp1->left)
				S1.push(temp1->left);
			temp1 = S1.top();
			S1.pop();
		}
		
		Tree *temp2 = S2.top();
		S2.pop();
		while(temp2 && !isLeaf(temp2))
		{
			if(temp2->right)
				S2.push(temp2->right);
			if(temp2->left)
				S2.push(temp2->left);
			temp2 = S2.top();
			S2.pop();
		}
		if(!temp1 && temp2)
			return false;
		if(temp1 && !temp2)
			return false;
		if(temp1 && temp2)
			if(temp1->data != temp2->data)
				return false;
	}
	return true;

}

int main(void)
{
	Tree *root1 = newNode(1);
	root1->left = newNode(2);
	root1->right = newNode(3);
    root1->left->left = newNode(4);
    root1->right->left = newNode(6);
    root1->right->right = newNode(7);
 
	Tree* root2 = newNode(0);
	root2->left = newNode(1);
	root2->right = newNode(5);
	root2->left->right = newNode(4);
	root2->right->left = newNode(6);
	root2->right->right = newNode(7);
	if(Issame(root1,root2))
		cout<<"SAME";
	else
		cout<<"NOT SAME";
	return 0;
}
