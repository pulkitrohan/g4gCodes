#include<stdio.h>

int isSet(unsigned N,int k)
{
    return (N & (1 <<k));
}

int isPalindrome(unsigned N)
{
    int l = 0;
    int r = sizeof(unsigned int)*8-1;
    while(r > l)
    {
        if(isSet(N,l) != isSet(N,r))
            return 0;
        l++,r--;
    }
    return 1;
}

int main(void)
{
    unsigned int N = 1<<15 + 1<<16;
    printf("%d\n",isPalindrome(N));
    return 0;
}
