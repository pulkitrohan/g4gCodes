#include<stdio.h>
#include<math.h>

unsigned int NextPower2(unsigned int N)
{
    return (1 << (int)ceil(log2(N)));
}

int main(void)
{
    unsigned  int N = 18;
    printf("%u\n",NextPower2(N));
    return 0;
}
