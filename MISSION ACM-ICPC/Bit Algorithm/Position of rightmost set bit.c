/*
    Example
    N = 1011
    -N = 0100+1 = 0101
    N & -N => 1011 & 0101 => 0001 => 1 in decimal
    log(1) => 0 + 1 => 1

*/

#include<stdio.h>

int SetBit(unsigned N)
{
    return log2(N & -N) + 1;
}

int main(void)
{
    unsigned N = 4;
    printf("Position of set bit : %d\n",SetBit(N));
    return 0;
}
