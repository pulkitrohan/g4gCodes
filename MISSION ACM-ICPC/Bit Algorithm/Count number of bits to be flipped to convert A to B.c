#include<stdio.h>

int SetBits(unsigned N)
{
    int count = 0;
    while(N)
    {
        N &= (N-1);
        count++;
    }
    return count;
}


int CountFlippedBits(unsigned A,unsigned B)
{
    return SetBits(A^B);
}

int main(void)
{
    unsigned A = 5,B = 6;
    printf("%d\n",CountFlippedBits(A,B));
    return 0;
}
