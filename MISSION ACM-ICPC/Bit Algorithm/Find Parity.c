#include<stdio.h>
#include<stdbool.h>
int Parity(unsigned N)
{
    bool parity = 0;
    while(N)
    {
        N &= (N-1);
        parity = !parity;
    }
    return parity;
}

int main(void)
{
    unsigned N = 9;
    if(Parity(N))
        printf("Odd Parity\n");
    else
        printf("Even Parity\n");
    return 0;
}
