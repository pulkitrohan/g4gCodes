#include<stdio.h>

unsigned LeftRotate(unsigned N,int d)
{
    return ((N << d ) | (N >> (32-d)));
}

unsigned RightRotate(unsigned N,int d)
{
    return ((N >> d ) | (N << (32-d)));
}
int main(void)
{
    unsigned int N = 16;
    int d = 2;
    printf("Left Rotate : %d\n",LeftRotate(N,d));
    printf("Right Rotate : %d\n",RightRotate(N,d));
    return 0;
}
