//For checking for divisibility by 9 we can use % operator or can use sum of digits and see if it is a multiple of 9
//Using Bitwise Operators we can do it in the following way :
//N/9 = N/8 - N/72
//N/8 => (N >> 3)
//N/72 => (N%8)/8
//N%8 => N & 7 =>eg N = 9 => 1101 & 0111 => 0001 => Remainder = 1
//N/8 => floor(N/8) + (N%8)/8
//N/72 => N/(8*9) => (floor(N/8) + (N%8)/8)/9 (Putting N/8)
//Now N/9 = floor(N/8) + (N%8)/8 - [(floor(N/8) + N%8)/8]/9
//N/9 = floor(N/8) - (floor(N/8)/9 -N%8/8 + (N%8/8)/9)
//N/9 = floor(N/8) - (floor(N/8)/9 - (N%8)/9)
//N is a multiple of 9 if floor(N/8) - (floor(N/8) + (N%8))/9 is an integer.
//Also if floor(N/8) + (N%8) is an integer then divisible by 9

#include<stdio.h>

int IsDivisible(int N)
{
    if(N == 0 || N == 9)
        return 1;
    else if(N < 9)
        return 0;
    else
        return IsDivisible((N>>3) - (N&7));
}

int main(void)
{
    int i;
    for(i=1;i<=100;i++)
        if(IsDivisible(i))
            printf("%u ",i);
    return 0;
}
