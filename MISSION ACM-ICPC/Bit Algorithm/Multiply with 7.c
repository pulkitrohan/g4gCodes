/*

If we have N and we shift it left by 3 positions it becomes
8 times N.So if we want to multiply by 7 then subtract N.If
we want to multiply by 9 then add N after shifting N by three
position on the left.

*/


#include<stdio.h>

unsigned Multiply7(unsigned N)
{
    return ((N << 3) - N);
}

unsigned Multiply9(unsigned N)
{
    return ((N << 3) + N);
}

int main(void)
{
    unsigned int N = 4;
    printf("%u\n",Multiply9(N));
    return 0;
}
