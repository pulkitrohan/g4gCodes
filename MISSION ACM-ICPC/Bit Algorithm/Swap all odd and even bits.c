/*
Let N = 23
Representation of N in binary is : 010111
Now we want to swap odd bits with even bits
so basically we want to shift even bits left by 1 position
and odd bits right by 1 position.
So we get 101011 => 43
So we first have to find a number whose even bits are 1 and odd bits
are zero.Such a number is A i.e. 10 => 1010
if we AND N with A we get all even bits
llrly we want to get all the odd bits only.So want to AND N with a
number which has all the odd bits as 1.Such a number is 5=>101
So we get the odd bits.Now shift even bits to left by 1 place
and odd bits to right by 1 place
and do inclusive OR operation between them.
*/


#include<stdio.h>

unsigned SwapBits(unsigned x)
{
    unsigned even = x & (0xAAAAAAAA);
    unsigned odd = x & (0x55555555);
    even >>= 1;
    odd <<= 1;
    return (even | odd);
}

int main(void)
{
    unsigned x = 23;
    printf("%u\n",SwapBits(x));
    return 0;
}
