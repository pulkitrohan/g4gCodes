#include<stdio.h>


int HasOppSign(int A,int B)
{
    return ((A^B) < 0);
}

int main(void)
{
    int x = 100,y = 1100;
    if(HasOppSign(x,y))
        printf("Signs are Opposite\n");
    else
        printf("Signs are not Opposite\n");
    return 0;
}
