/* Method 1
Arithmetic Operators

a = 5 b = 3
a =a + b = 8
b =a - b = 5
a = a - b =>3

Problem => Overflow may occur

Using Bitwise xor

a = a^b
b = a^b
a = a^b

 When we use pointers to variable and make a function swap, all of the above methods fail when
both pointers point to the same variable.

Bitwise XOR based method
x = x ^ x; // x becomes 0
x = x ^ x; // x remains 0
x = x ^ x; // x remains 0

Arithmetic based method
x = x + x; // x becomes 2x
x = x � x; // x becomes x
x = x � x; // x becomes 0

*/
//Correct solution

void Swap(int *A,int *B)
{
    if(A == B)
        return;
    *A = *A + *B;
    *B = *A - *B;
    *A = *A - *B;
}

int main(void)
{
    int x = 10;
    Swap(&x,&x);
    printf("After Swap : x = %d\n",x);
    return 0;
}
