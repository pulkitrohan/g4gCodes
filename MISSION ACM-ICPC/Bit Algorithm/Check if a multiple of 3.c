/*
In this the the only logic is that in the binary conversion of a number N
if the diff of set bits at odd pos and set bits at even pos is a multiple
of 3 then N is a multiple of 3.

*/


#include<stdio.h>

int IsMultiple3(int N)
{
    if(N < 0)
        N = -N;
    else if(N == 0)
        return 1;
    else if(N == 1)
        return 0;
    int even_count = 0,odd_count = 0;
    while(N)
    {
        if(N & 1)
            even_count++;
        N >>=1;
        if(N & 1)
            odd_count++;
        N >>=1;
    }
    return IsMultiple3(odd_count-even_count);
}

int main(void)
{
    int N = 11;
    if(IsMultiple3(N))
        printf("Multiple of 3\n");
    else
        printf("Not a Multiple of 3\n");
    return 0;
}
