//Nibbles are half of a byte
//So swap 4 leftmost bits of byte with 4 rightmost bits of byte

#include<stdio.h>

unsigned int SwapNibble(unsigned int ch)
{
    return ((ch & 0x0F) << 4 | (ch & 0xF0) >> 4);
}
int main(void)
{
    unsigned int ch = 100;
    printf("%u\n",SwapNibble(ch));
    return 0;
}
