#include<stdio.h>

int main(void)
{
	int A[] = {2,3,7,9,11,2,3,11};
	int N = sizeof(A)/sizeof(A[0]);
	int x = 0,y = 0,i;
	int xor = A[0];
	for(i=1;i<N;i++)
		xor ^= A[i];
	int set_bit = xor &  ~(xor-1);
	for(i=0;i<N;i++)
	{
		if(A[i] & set_bit)
			x ^= A[i];
		else
			y ^= A[i];
	}
	printf("%d %d\n",x,y);
	return 0;
}
