#include<stdio.h>

int Divby2(unsigned N)
{
    return (N && ((N & (N-1)) == 0));
}

int SetBit(unsigned N)
{
    if(!Divby2(N))
        return -1;
    int count = 0;
    while(N)
    {
        N >>= 1;
        count++;
    }
    return count;
}

int main(void)
{
    unsigned N = 32;
    int pos = SetBit(N);
    if(pos == -1)
        printf("Invalid Input\n");
    else
    printf("Position of set bit : %d\n",SetBit(N));
    return 0;
}
