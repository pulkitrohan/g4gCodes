#include<stdio.h>

int CountSetBits(unsigned N)
{
    int count = 0;
    while(N)
    {
        N = N & N-1;
        count++;
    }
    return count;
}

int main(void)
{
    unsigned int N = 8;
    printf("%d\n",CountSetBits(N));
    return 0;
}
