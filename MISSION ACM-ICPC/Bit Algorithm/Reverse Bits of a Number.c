/*
In this we take a variable reverse_num and initially declare it to value 0.
Now starting from the right most bit of the number.If it is 1 then
we inclusive OR the reverse_num with 1 in 31-i position
where i is the index of the bit from right.Hence we are reversing bits of a
number.

*/


#include<stdio.h>

unsigned ReverseBits(unsigned N)
{
    unsigned reverse_num = 0;
    int bits = sizeof(N) * 8-1;
    int i;
    for(i=0;i<bits;i++)
    {
        if(N & (1 << i))
            reverse_num |= (1 << (bits-i));
    }
    return reverse_num;
}

int main(void)
{
    unsigned N = 2;
    printf("%u\n",ReverseBits(N));
    return 0;
}
