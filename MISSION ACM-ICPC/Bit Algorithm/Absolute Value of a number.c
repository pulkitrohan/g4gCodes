/*
i = 8 => 1000
shifting i right by 31 position MASK = 0 as i is +ve.
so (i+0)^0 => i
llrly if i = -6.
shifting i right by 31 postioon MASK = 1 as i is -ve
so (i+1)^1 => -i because i is in 2's complement form
*/


#include<stdio.h>

int absolute(int i)
{
    int MASK = i >> 31;
    return (i + MASK) ^ MASK;
}
int main(void)
{
    int i = -8;
    printf("%d\n",absolute(i));
    return 0;
}
