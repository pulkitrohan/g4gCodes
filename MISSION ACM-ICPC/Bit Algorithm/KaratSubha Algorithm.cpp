/*
Consider 2 Numbers X and Y.
if we divide X into Xl,Xr where Xl are N/2 leftmost bits and Xr are N/2 rightmost bits.
llrly for Y into Yl,Yr.
Let's assume N is even for now.
So X = Xl*2^N/2 + Xr
and Y = Yl*2^N/2 + Yr
So the product XY can be written as
XY = (X1*2^N/2 + Xr)* (Yl*2^N/2 + Yr)
   = (XlY1)2^N +( XlYr + YlXr)2^N/2 + XrYr
   Problem is divided into 4 subproblem of size N/2
   So we require 4 multiplications here
   So Recurrence Relation is
   T(n) = 4T(n/2) + O(n) => O(n^2)
    But what if We convert(XlYr + YlXr) => (Xl+Xr)(Yl+Yr)  - XlYl - XrYr
So, We get XY = (XlY1)2^N +((Xl+Xr)(Yl+Yr)  - XlYl - XrYr)2^N/2 + XrYr
So will be required to do only 3 multiplications now
So, T(n) = 3T(n/2) + O(n) => O(n^1.59)

If the length of input string is odd then we append 0's in the
beginning.
So,to handle odd lengths we put floor(n/2) bits in left half and ceil(n/2) bits in right half.

*/


#include<cstdio>
#include<string>

using namespace std;


int MakeEqualLength(string &X,string &Y)
{
    int len1 = X.size();
    int len2 = Y.size();
    if(len1 < len2)
    {
        for(int i = 0;i<len2-len1;i++)
            X = '0' + X;
        return len2;
    }
    else if(len1 > len2)
    {
        for(int i = 0;i<len1-len2;i++)
            Y = '0' + Y;
    }
    return len1;
}

int MultiplySingleBit(string X,string Y)
{
    return (X[0] - '0') * (Y[0] - '0');
}

string addBitsStrings(string A,string B)
{
    string result;
    int length = MakeEqualLength(A,B);
    int carry = 0;
    for(int i = length-1;i>=0;i--)
    {
        int firstBit = A.at(i) - '0';
        int secondBit = B.at(i) - '0';
        int sum = (firstBit ^ secondBit ^ carry) + '0';
        result = (char)sum + result;
        carry = (firstBit & secondBit) | (secondBit & carry) | (carry & firstBit);
    }
    if(carry)
        result = '1' + result;
    return result;
}

long int multiply(string X,string Y)
{
    int N = MakeEqualLength(X,Y);
    if(N == 0)
        return 0;
    if(N == 1)
        return MultiplySingleBit(X,Y);
    int fh = N/2;
    int sh = (N-fh);

    string Xl = X.substr(0,fh);
    string Xr = X.substr(fh,sh);

    string Yl = Y.substr(0,fh);
    string Yr = Y.substr(fh,sh);

    long int P1 = multiply(Xl,Yl);
    long int P2 = multiply(Xr,Yr);
    long int P3 = multiply(addBitsStrings(Xl,Xr),addBitsStrings(Yl,Yr));

    return P1 * (1 <<(2*sh)) + (P3-P1-P2)*(1<<sh) + P2;
}

int main(void)
{
    printf("%ld\n",multiply("1100","1010"));
    return 0;
}
