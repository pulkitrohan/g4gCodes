#include<bits/stdc++.h>
using namespace std;

void maxLeftSubArraySym(int *A,int N,int *leftMax)
{
	leftMax[0] = A[0];
	int max_so_far = A[0],curr_max = A[0];
	for(int i=1;i<N;i++)
	{
		curr_max = max(A[i],A[i]+curr_max);
		max_so_far = max(max_so_far,curr_max);
		leftMax[i] = max_so_far;
	}
}

void maxRightSubArraySum(int *A,int N,int *rightMax)
{
	rightMax[N-1] = A[N-1];
	int max_so_far = A[N-1],curr_max = A[N-1];
	for(int i=N-2;i>=0;i--)
	{
		curr_max = max(A[i],A[i]+curr_max);
		max_so_far = max(max_so_far,curr_max);
		rightMax[i] = max_so_far;
	}
}

int FindMaxAbsDiff(int *A,int N)
{
	int leftMax[N];
	maxLeftSubArraySum(A,N,leftMax);
	
	int rightMax[N];
	maxRightSubArraySum(A,N,rightMax);
	
	int invertArr[N];
	for(int i=0;i<N;i++)
		invertArr[i] = -A[i];
	int leftMin[N];
	maxLeftSubArraySum(invertArr,N,leftMin);
	for(int i=0;i<N;i++)
		leftMin[i] = -leftMin[i];
	
	int rightMin[N];
	maxRightSubArraySum(invertArr,N,rightMin);
	for(int i=0;i<N;i++)
		rightMin[i] = -rightMin[i];
		
	int ans = INT_MIN;
	for(int i=0;i<N-1;i++)
		ans = max(ans,max(abs(leftMax[i]-rightMin[i+1]),abs(leftMin[i]-rightMax[i+1])));
	return ans;
	
}

int main(void)
{
	int A[] = {-2,-3,4,-1,-2,1,5,-3};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<FindMaxAbsDiff(A,N);
	return 0;
}