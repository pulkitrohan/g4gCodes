#include<bits/stdc++.h>
using namespace std;
struct Node
{
	int data;
	struct Node *left,*right;
};

Node *newNode(int data)
{
	Node *temp = new Node;
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

void PrintLevelOrder(Node *root)
{
	if(!root)
		return;
	printf("%d ",root->data);
	if(root->left)
		printf("%d %d ",root->left->data,root->right->data);
	if(!root->left->left)
		return;
	queue<Node *> Q;
	Q.push(root->left);
	Q.push(root->right);
	Node *first,*second;
	while(!Q.empty())
	{
		first = Q.front();
		Q.pop();
		second = Q.front();
		Q.pop();
		
		printf("%d %d %d %d ",first->left->data,second->right->data,first->right->data,second->left->data);
		if(first->left->left)
		{
			Q.push(first->left);
			Q.push(second->right);
			Q.push(first->right);
			Q.push(second->left);
		}
	}
}

		
int main(void)
{
	Node *root = newNode(1);
 
    root->left        = newNode(2);
    root->right       = newNode(3);
 
    root->left->left  = newNode(4);
    root->left->right = newNode(5);
    root->right->left  = newNode(6);
    root->right->right = newNode(7);
 
    root->left->left->left  = newNode(8);
    root->left->left->right  = newNode(9);
    root->left->right->left  = newNode(10);
    root->left->right->right  = newNode(11);
    root->right->left->left  = newNode(12);
    root->right->left->right  = newNode(13);
    root->right->right->left  = newNode(14);
    root->right->right->right  = newNode(15);
 
    root->left->left->left->left  = newNode(16);
    root->left->left->left->right  = newNode(17);
    root->left->left->right->left  = newNode(18);
    root->left->left->right->right  = newNode(19);
    root->left->right->left->left  = newNode(20);
    root->left->right->left->right  = newNode(21);
    root->left->right->right->left  = newNode(22);
    root->left->right->right->right  = newNode(23);
    root->right->left->left->left  = newNode(24);
    root->right->left->left->right  = newNode(25);
    root->right->left->right->left  = newNode(26);
    root->right->left->right->right  = newNode(27);
    root->right->right->left->left  = newNode(28);
    root->right->right->left->right  = newNode(29);
    root->right->right->right->left  = newNode(30);
    root->right->right->right->right  = newNode(31);
	PrintLevelOrder(root);
	return 0;
}
