#include<bits/stdc++.h>
using namespace std;
struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *newNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void Inorder(struct Tree *root)
{
    if(root)
    {
        Inorder(root->left);
        printf("%d ",root->data);
        Inorder(root->right);
    }
}

Tree * constructTree(int pre[], int post[], int &preIndex, int start, int end, int size) {
    if(preIndex >= size || start > end)
        return NULL;
    Tree *root = newNode(pre[preIndex++]);
    if(start == end)
        return root;
    int i;
    for(i=start;i<=end;i++)
        if(pre[preIndex] == post[i])
            break;
    if(i <= end) {
        root->left = constructTree(pre, post, preIndex, start, i, size);
        root->right = constructTree(pre, post, preIndex, i+1, end, size);
    }
    return root;
}

int main(void)
{
    int pre[] = {1, 2, 4, 8, 9, 5, 3, 6, 7}; 
    int post[] = {8, 9, 4, 5, 2, 6, 7, 3, 1}; 
    int size = sizeof( pre ) / sizeof( pre[0] ); 
  
    int preIndex = 0;
    struct Tree *root = constructTree(pre, post, preIndex, 0, size-1, size); 
    printf("Inorder traversal of the constructed tree: \n"); 
    Inorder(root); 
    return 0;
}
