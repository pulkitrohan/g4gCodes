//Algorithm:
//1)Start from the root.If root is NULL then return.else push the root into the pathArray.
//2)Check whether node is a leaf.If it is a leaf then print the pathArray
    //else Recursively call for left child and right child of the node.

#include<stdio.h>
#include<stdlib.h>
struct tNode
{
    int data;
    struct tNode *left,*right;
};

struct tNode *CreateNode(int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void PrintArray(int Path[],int PathLen)
{
    int i;
    for(i=0;i<PathLen;i++)
        printf("%d ",Path[i]);
    printf("\n");
}

void PrintPaths(struct tNode *root,int Path[],int PathLen)
{
    if(root)
    {
        Path[PathLen++] = root->data;
        if(!root->left && !root->right)
            PrintArray(Path,PathLen);
        else
        {
            PrintPaths(root->left,Path,PathLen);
            PrintPaths(root->right,Path,PathLen);
        }
    }
}

int main(void)
{
    struct tNode *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    int Path[100] = {0},PathLen = 0;
    PrintPaths(root,Path,PathLen);
    return ;
}
