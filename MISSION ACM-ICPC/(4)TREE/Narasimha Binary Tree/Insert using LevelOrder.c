#include<limits.h>
#include<stdlib.h>
struct tNode
{
    int data;
    struct tNode *left;
    struct tNode *right;
};


typedef struct Queue    //Queue
{
	struct tNode *t;
	struct Queue *next;
}Queue;

Queue *rear = NULL,*front = NULL;   //Rear and front for Queue

//Queue ADT Starts
void Enqueue(struct tNode *root)
{
    Queue *temp = (Queue *)malloc(sizeof(Queue));
    temp->t = root;
    temp->next = NULL;
    if(rear == NULL)
        rear = front = temp;
    else
    {
        rear->next = temp;
        rear = temp;
    }
}

struct tNode *Dequeue()
{
    if(isEmptyQueue())
        {
            return NULL;
        }
    else if(front == rear)
    {
        Queue *temp = front;
        front = rear = NULL;
            return temp->t;
    }
    else
    {
        Queue *temp = front;
        front = front->next;
        return temp->t;
    }
}

int isEmptyQueue()
{
    if(front == NULL)
        return 1;
    else
        return 0;
}
//Queue ADT Over

typedef struct Node   //Stack
{
    struct tNode *t;
    struct Node *next;
}node;

//Stack ADT Starts
void push(node *S,struct tNode *root)
{
		node *temp;
        temp= (node *)malloc(sizeof(node));
        temp->next = S->next;
        temp->t = root;
        S->next = temp;
}

struct tNode* pop(node *S)
{
        node *temp;
        temp = S->next;
        S->next = temp->next;
        return temp->t;
}

struct tNode *Top(node *S)
{
    return S->next->t;
}

int isEmpty(node *S)
{
    if(S->next == NULL)
        return 1;
    else return 0;
}

//Stack ADT Over

struct tNode *CreateNode(int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = NULL;
    temp->right = NULL;
}

void inOrder(struct tNode *root)
{
    node *S = (node *)malloc(sizeof(node));
    S->next = NULL;
    while(1)
    {
        while(root)
        {
            push(S,root);
            root = root->left;
        }
            if(isEmpty(S))
            break;
            root= pop(S);
            printf("%d ",root->data);
            root = root->right;
    }
}


//Return the root node else we will not get the correct tree
struct tNode *InsertNode(struct tNode *root,int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = NULL;
    temp->right = NULL;
    if(!root)
    {
        //root = (struct tNode *)malloc(sizeof(struct tNode));
        root = temp;
        return root;
    }
    else
    {
        struct tNode *Temp = root;
        Enqueue(Temp);
        while(!isEmptyQueue())
        {
            Temp = Dequeue();
            if(Temp->left)
                Enqueue(Temp->left);
            else
            {
                Temp->left = temp;
                break;
            }
            if(Temp->right)
                Enqueue(Temp->right);
            else
            {
                Temp->right = temp;
                break;
            }
        }
    }
    return root;
}

int main(void)
{
    int i,item;
    struct tNode *root = NULL;
    for(i=1;i<=7;i++)
        root = InsertNode(root,i);
        inOrder(root);
    return 0;
}
