#include<stdio.h>
#include<stdlib.h>
struct Tree
{
    int data;
    struct Tree *left,*right;
};


struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void PrintKDistanceNodeDown(struct Tree *root,int k)
{
	if(root)
	{
		if(k == 0)
			printf("%d ",root->data);
		else
		{
			PrintKDistanceNodeDown(root->left,k-1);
			PrintKDistanceNodeDown(root->right,k-1);
		}
	}
}

int PrintNodesKdistance(struct Tree *root,struct Tree *target,int k)
{
    if(!root)
        return -1;
    if(root == target)
    {
        PrintKDistanceNodeDown(root,k);
        return 0;
    }
    int dl = PrintNodesKdistance(root->left,target,k);
    if(dl != -1)
    {
        if(dl + 1 == k)
            printf("%d ",root->data);
        else
            PrintKDistanceNodeDown(root->right,k-dl-2);
        return 1 + dl;
    }
    int dr = PrintNodesKdistance(root->right,target,k);
    if(dr != -1)
    {
        if(dr + 1 == k)
            printf("%d ",root->data);
        else
            PrintKDistanceNodeDown(root->left,k-dr-2);
        return 1 + dr;
    }
    return -1;
}

int main(void)
{
    struct Tree *root = CreateNode(20);
    root->left = CreateNode(8);
    root->right = CreateNode(22);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(12);
    root->left->right->left = CreateNode(10);
    root->left->right->right = CreateNode(14);
    struct Tree *target = root->left->right->right;
    PrintNodesKdistance(root,target,3);
    return 0;
}
