#include<bits/stdc++.h>
using namespace std;
struct Node
{
	int data;
	struct Node *left,*right;
};

Node *newNode(int data)
{
	Node *temp = new Node;
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

void PrintLevelOrder(Node *root)
{
	if(!root)
		return;
	queue<Node *> Q;
	Q.push(root);
	while(1)
	{
		int count = Q.size();
		if(count == 0)
			break;
		while(count > 0)
		{
			Node *temp = Q.front();
			Q.pop();
			cout<<temp->data<<" ";
			if(temp->left)
				Q.push(temp->left);
			if(temp->right)
				Q.push(temp->right);
			count--;
		}
		cout<<endl;
	}
}

int main(void)
{
	Node *root = newNode(1);
	root->left = newNode(2);
	root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);
    root->right->right = newNode(6);
	PrintLevelOrder(root);
	return 0;
}
