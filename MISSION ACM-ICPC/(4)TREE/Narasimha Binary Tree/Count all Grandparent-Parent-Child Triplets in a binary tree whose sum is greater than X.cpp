#include<bits/stdc++.h>
using namespace std;

struct TreeNode {
	int data;
	TreeNode *left, *right;
	TreeNode(int data) {
		this->data = data;
		left = right = NULL;
	}
};

void countTriplet(TreeNode *grandParent, TreeNode *parent, TreeNode *child, int sum, int &count) {
	if(grandParent && parent && child  && 
		(grandParent->data + parent->data + child->data > sum)) {
		count++;
	} 
	if(!child) 
		return;
	countTriplet(parent, child, child->left, sum, count);
	countTriplet(parent, child, child->right, sum, count);
}

int main(void) {
    TreeNode *r10 = new TreeNode(10); 
    TreeNode *r1 = new TreeNode(1); 
    TreeNode *r22 = new TreeNode(22); 
    TreeNode *r35 = new TreeNode(35); 
    TreeNode *r4 = new TreeNode(4); 
    TreeNode *r15 = new TreeNode(15); 
    TreeNode *r67 = new TreeNode(67); 
    TreeNode *r57 = new TreeNode(57); 
    TreeNode *r38 = new TreeNode(38); 
    TreeNode *r9 = new TreeNode(9); 
    TreeNode *r10_2 = new TreeNode(10); 
    TreeNode *r110 = new TreeNode(110); 
    TreeNode *r312 = new TreeNode(312); 
    TreeNode *r131 = new TreeNode(131); 
    TreeNode *r414 = new TreeNode(414); 
    TreeNode *r8 = new TreeNode(8); 
    TreeNode *r39 = new TreeNode(39); 

    r10->left=r1; 
    r10->right=r22; 
    r1->left=r35; 
    r1->right=r4; 
    r22->left=r15; 
    r22->right=r67; 
    r35->left=r57; 
    r35->right=r38; 
    r4->left=r9; 
    r4->right=r10_2; 
    r15->left=r110; 
    r15->right=r312; 
    r67->left=r131; 
    r67->right=r414; 
    r312->left=r8; 
    r414->right=r39; 

    int count = 0;
    countTriplet(NULL, NULL, r10, 100, count);
    cout<<count;

	return 0;
}