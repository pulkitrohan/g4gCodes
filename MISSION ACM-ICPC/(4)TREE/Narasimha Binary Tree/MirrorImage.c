#include<stdio.h>
#include<limits.h>
#include<stdlib.h>
struct tNode
{
    int data;
    struct tNode *left;
    struct tNode *right;
};

struct tNode *CreateNode(int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = NULL;
    temp->right = NULL;
}

int IsMirrorImage(struct tNode *root,struct tNode *root1)
{
    if(!root && !root1)
        return 1;
    else if(!root && root1 || root && !root1)
        return 0;
    else
        return (root->data == root1->data && IsMirrorImage(root->left,root1->right) && IsMirrorImage(root->right,root1->left));
}

int main(void)
{
    struct tNode *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);

    struct tNode *root1 = CreateNode(1);
    root1->left = CreateNode(3);
    root1->right = CreateNode(2);
    root1->right->left = CreateNode(5);
    root1->right->right = CreateNode(4);
    int IsMirror = IsMirrorImage(root,root1);
    if(IsMirror)
        printf("Trees are Mirror Image of each other.\n");
    else
        printf("Trees are not Mirror Image of each other\n");

    return 0;
}
