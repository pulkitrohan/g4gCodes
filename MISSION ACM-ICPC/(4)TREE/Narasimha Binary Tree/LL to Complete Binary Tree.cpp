#include<cstdio>
#include<cstdlib>
#include<queue>

using namespace std;
struct LL
{
    int data;
    struct LL *next;
};

struct Tree
{
    int data;
    struct Tree *left,*right;
};


struct LL *Insert(struct LL *head,int data)
{
    struct LL *temp = (struct LL *)malloc(sizeof(struct LL));
    temp->data = data;
    temp->next = NULL;
    if(!head)
    {
        head = temp;
    }
    else
    {
        temp->next = head;
        head = temp;
    }
    return head;
}

void Print(struct LL *head)
{
    while(head)
    {
        printf("%d ",head->data);
        head = head->next;
    }
}

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
}

struct Tree *LLToBinaryTree(struct LL *head,struct Tree *root)
{
    queue<struct Tree *> Q;
    if(!head)
    {
        return NULL;
    }
    root = CreateNode(head->data);
    Q.push(root);
    head = head->next;
    while(head)
    {
        struct Tree *temp = Q.front();
        Q.pop();
        temp->left = CreateNode(head->data);
        Q.push(temp->left);
        head = head->next;
        if(head)
        {
            temp->right = CreateNode(head->data);
            Q.push(temp->right);
            head = head->next;
        }
    }
   return root;
}

void Inorder(struct Tree *root)
{
    if(root)
    {
        Inorder(root->left);
        printf("%d ",root->data);
        Inorder(root->right);
    }
}

int main(void)
{
    struct LL *head = NULL;
    head = Insert(head,36);
    head = Insert(head,30);
    head = Insert(head,25);
    head = Insert(head,15);
    head = Insert(head,12);
    head = Insert(head,10);
    Print(head);
    struct Tree *root = NULL;
    root = LLToBinaryTree(head,root);
    printf("\n");
    Inorder(root);
    return 0;
}
