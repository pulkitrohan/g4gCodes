#include<cstdio>
#include<cstdlib>
#include<queue>
struct tNode
{
    int data;
    struct tNode *left,*right;
};

using namespace std;
struct tNode *CreateNode(int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = temp->right = NULL;
}


int CheckSum(struct tNode *root)
{
    if(!root)
        return 1;
    queue<struct tNode *> Q;
    Q.push(root);
    while(!Q.empty())
    {
        struct tNode *temp = Q.front();
        Q.pop();
        int left = 0,right = 0;
		if(temp->left)	
		{
			left = temp->left->data;
			Q.push(temp->left);
		}
		if(temp->right)
		{
			right = temp->right->data;
			Q.push(temp->right);
		}
		if(left + right != temp->data)
			return 0;
    }
    return 1;
}

int CheckSumRecursive(struct tNode *root)
{
    if(!root)
        return 1;
    if(!root->left && !root->right)
        return 1;
    int left = 0,right = 0;
    if(root->left)
        left = root->left->data;
    if(root->right)
        right = root->right->data;
    return (root->data == (left+right) && CheckSumRecursive(root->left) && CheckSumRecursive(root->right));
}

int main(void)
{
    struct tNode *root = CreateNode(10);
   root->left = CreateNode(8);
   root->right = CreateNode(2);
   root->left->left = CreateNode(3);
   root->left->right = CreateNode(5);
   root->right->left = CreateNode(2);
   int check = CheckSumRecursive(root);
   if(check)
        printf("\nChildren Sum Property Satisfied\n");
   else
        printf("\nChildren Sum Property not Satisfied\n");
    return 0;
}
