#include<stdio.h>
#include<stdlib.h>

struct Tree
{
	int data;
	struct Tree *left,*right;
};

struct Tree *newNode(int data)
{
	struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

void DeepestLeftLeafNode(struct Tree *root,int curlevel,int *max,struct Tree **res,int isleft)
{
	if(root)
	{
		if(isleft && !root->left && !root->right && *max < curlevel)
		{
			*res = root;
			*max = curlevel;
			return;
		}
		DeepestLeftLeafNode(root->left,curlevel+1,max,res,1);
		DeepestLeftLeafNode(root->right,curlevel+1,max,res,0);
	}
}
int main(void)
{
	struct Tree *root = newNode(1);
	root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->right->left = newNode(5);
    root->right->right = newNode(6);
    root->right->left->right = newNode(7);
    root->right->right->right = newNode(8);
    root->right->left->right->left = newNode(9);
    root->right->right->right->right = newNode(10);
	int maxlevel = 0;
	struct Tree *res = NULL;
	int isleft = 0,curlevel = 0;
	DeepestLeftLeafNode(root,curlevel,&maxlevel,&res,0);
	if(res)
		printf("%d\n",res->data);
	else
		printf("No left leaf\n");
	return 0;
}
