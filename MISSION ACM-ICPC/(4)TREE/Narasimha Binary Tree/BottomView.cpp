#include<cstdio>
#include<cstdlib>
#include<map>
#include<iostream>

using namespace std;
#define forit(it,s) for(typeof(s.begin()) it=s.begin(); it!=s.end(); it++)
#define f first
#define s second
struct tNode
{
    int data;
    struct tNode *left;
    struct tNode *right;
};

struct tNode *CreateNode(int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = NULL;
    temp->right = NULL;
    return temp;
}

map<int,int> M;
void BottomView(struct tNode *root,int col)
{
    if(root)
    {
        M[col] = root->data;
        BottomView(root->left,col-1);
        BottomView(root->right,col+1);

    }
}

int main(void)
{
    struct tNode *root = CreateNode(20);
    root->left = CreateNode(8);
    root->right = CreateNode(22);
    root->left->left = CreateNode(5);
    root->left->right = CreateNode(3);
    root->right->left = CreateNode(4);
    root->right->right = CreateNode(25);
    root->left->right->left = CreateNode(10);
    root->left->right->right = CreateNode(14);
    BottomView(root,0);
    forit(it,M)
        cout<<"diagonal "<<it->f<<" has value "<<it->s<<"\n";
    return 0;
}
