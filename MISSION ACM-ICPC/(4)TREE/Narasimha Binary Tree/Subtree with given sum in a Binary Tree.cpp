#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right;
};

Tree *newnode(int data)
{
	Tree *temp = new Tree;
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

bool sumSubtreeUtil(Tree *root,int sum,int &cur_sum)
{
	if(!root)
	{
		cur_sum = 0;
		return false;
	}
	int left = 0,right = 0;
	return sumSubtreeUtil(root->left,sum,left) ||
		   sumSubtreeUtil(root->right,sum,right) || 
		   ((cur_sum = left + right + root->data) == sum);
}

bool sumSubtree(Tree *root,int sum)
{
	int cur_sum = 0;
	return sumSubtreeUtil(root,sum,cur_sum);
}

int main(void)
{
	Tree *root = newnode(8);
    root->left    = newnode(5);
    root->right   = newnode(4);
    root->left->left = newnode(9);
    root->left->right = newnode(7);
    root->left->right->left = newnode(1);
    root->left->right->right = newnode(12);
    root->left->right->right->right = newnode(2);
    root->right->right = newnode(11);
    root->right->right->left = newnode(3);
    int sum = 22;
    if(sumSubtree(root,sum))
    	cout<<"YES";
    else
    	cout<<"NO";
}