#include<stdio.h>
#include<stdlib.h>

struct Tree
{
	int data;
	struct Tree *left,*right;
};

struct Tree *newNode(int data)
{
	struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

int level(struct Tree *root,struct Tree *node,int l)
{
	if(!root)
        return 0;
    if(root == node)
        return l;
    int lev = level(root->left,node,l+1);
    if(lev != 0)
        return lev;
    return level(root->right,node,l+1);
}

int isSibling(struct Tree *root,struct Tree *n1,struct Tree *n2)
{
    if(!root)
        return 0;
    return (root->left == n1 && root->right == n2) ||
           (root->left == n2 && root->right == n1) ||
           isSibling(root->left,n1,n2) ||
           isSibling(root->right,n1,n2);
}


int isCousin(struct Tree *root,struct Tree *n1,struct Tree *n2)
{
	if((level(root,n1,1) == level(root,n2,1)) && !isSibling(root,n1,n2))
		return 1;
	return 0;
}

int main(void)
{
	struct Tree *root = newNode(1);
	root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);
    root->right->left = newNode(6);
    root->right->right = newNode(7);

	struct Tree *n1 = root->left->left;
	struct Tree *n2 = root->right->right;
	if(isCousin(root,n1,n2))
		printf("YES\n");
	else
		printf("NO\n");
	return 0;
}
