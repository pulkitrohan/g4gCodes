#include<bits/stdc++.h>
using namespace std;
struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *newNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void Inorder(struct Tree *root)
{
    if(root)
    {
        Inorder(root->left);
        printf("%d ",root->data);
        Inorder(root->right);
    }
}

void sumleft(struct Tree *root,bool isleft,int &sum)
{
    if(root)
    {
        sumleft(root->left,true,sum);
        sumleft(root->right,false,sum);
        if(!root->left && !root->right && isleft)
			sum += root->data;
    }
}

int main(void)
{
    struct Tree *root         = newNode(20);
    root->left                = newNode(9);
    root->right               = newNode(49);
    root->right->left         = newNode(23);
    root->right->right        = newNode(52);
    root->right->right->left  = newNode(50);
    root->left->left          = newNode(5);
    root->left->right         = newNode(12);
    root->left->right->right  = newNode(12);
    Inorder(root);
    printf("\n");
	int sum = 0;
    sumleft(root,false,sum);
	cout<<sum<<endl;
    return 0;
}
