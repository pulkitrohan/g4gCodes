#include<bits/stdc++.h>
using namespace std;

void PrintPostOrder(int *in,int *pre, int &index, 
    int start, int end, map<int, int> &hm)
{
    if(start > end)
        return;
    int inorderIndex = hm[pre[index++]];
    PrintPostOrder(in, pre, index, start, inorderIndex-1, hm);
    PrintPostOrder(in, pre, index, inorderIndex+1, end, hm);
    cout<<in[inorderIndex]<<" ";
}

int main(void)
{
    int in[] = {4,2,5,1,3,6};
    int pre[] = {1,2,4,5,3,6};
    int N = sizeof(in)/sizeof(in[0]);
    map<int,int> hm;
    for(int i=0;i<N;i++)
        hm[in[i]] = i;
    int index = 0;
    PrintPostOrder(in,pre, index, 0, N-1, hm);
    return 0;
}
