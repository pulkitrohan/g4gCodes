#include<cstdio>
#include<cstdlib>
#include<queue>
using namespace std;

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void DepthDeepestOddLevelLeaf(struct Tree *root,int level)
{
    if(root)
	{
		if(!root->left && !root->right && level & 1)
			return level;
		return max(DepthDeepestOddLevelLeaf(root->left,level+1),DepthDeepestOddLevelLeaf(root->right,level+1));
	}
}

int main(void)
{
    struct Tree *root = CreateNode(1);
    root->left = CreateNode(2);
    root->left->left = CreateNode(4);
    root->right = CreateNode(3);
    root->right->left = CreateNode(5);
    root->right->left->right = CreateNode(7);
    root->right->left->right->left = CreateNode(9);
    root->right->right = CreateNode(6);
    root->right->right->right = CreateNode(8);
    root->right->right->right->right = CreateNode(10);
    root->right->right->right->right->left = CreateNode(11);
    cout<<DepthDeepestOddLevelLeaf(root,1);
    return 0;
}
