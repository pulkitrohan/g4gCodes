#include<stdio.h>
#include<stdlib.h>

#define max(a,b) ((a > b) ? a : b)

struct tNode
{
    int data;
    struct tNode *left,*right;
};

struct tNode *CreateNode(int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

int height(struct tNode *root)
{
    if(!root)
        return 0;
    else
        return (max(height(root->left),height(root->right)) + 1 );
}

int isBalancedUnoptimized(struct tNode *root)
{
    if(!root)
        return 1;
    int lh = Height(root->left);
    int rh = Height(root->right);
    return (abs(lh-rh) <=1) && isBalancedUnoptimized(root->left) && isBalancedUnoptimized(root->right);
}

int IsBalanced(struct tNode *root,int *height)
{
	if(!root)
	{
		*height = 0;
		return 1;
	}
	int lh,rh,l,r;
	lh = rh = l = r = 0;
	l = IsBalanced(root->left,&lh);
	r = IsBalanced(root->right,&rh);
	*height = max(lh,rh) + 1;
    return (abs(lh-rh) <= 1) && (l) && (r);
}



int main(void)
{
    struct tNode *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    root->right->left = CreateNode(6);
    root->right->right = CreateNode(7);
    int height = 0;
    int Balanced = IsBalanced(root,&height);
    if(Balanced)
        printf("Yes,the Tree is balanced\n");
    else
        printf("Tree is not balanced\n");
    return 0;
}
