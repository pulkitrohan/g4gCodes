#include<bits/stdc++.h>
using namespace std;

struct TreeNode 
{
	int data;
	struct TreeNode *left,*right;
};

TreeNode *createNode(int data)
{
	TreeNode *temp = (TreeNode *)malloc(sizeof(TreeNode));
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

bool isLeaf(TreeNode *A)
{
	return !A->left && !A->right;
}

bool isSame(TreeNode *A,TreeNode *B)
{
	if(!A && !B)
		return true;
	if(!A || !B)
		return false;
	stack<TreeNode *> S1,S2;
	S1.push(A);
	S2.push(B);
	while(!S1.empty() || !S2.empty())
	{
		if(S1.empty() || S2.empty())
			return false;
			
		TreeNode *temp1 = S1.top();
		S1.pop();
		while(temp1 && !isLeaf(temp1))
		{
			if(temp1->right)
				S1.push(temp1->right);
			if(temp1->left)
				S1.push(temp1->left);
			temp1 = S1.top();
			S1.pop();
		}
		
		TreeNode *temp2 = S2.top();
		S2.pop();
		while(temp2 && !isLeaf(temp2))
		{
			if(temp2->right)
				S2.push(temp2->right);
			if(temp2->left)
				S2.push(temp2->left);
			temp2 = S2.top();
			S2.pop();
		}
		
		if(!temp1 && temp2 || temp1 && !temp2)
			return false;
		if(!temp1 && !temp2)
			if(temp1->data != temp2->data)
				return false;
	}
	return true;
}

int main(void)
{
	 TreeNode *A = createNode(1);
	 A->left = createNode(2);
	 A->right = createNode(3);
	 A->left->left = createNode(4);
	 A->right->left = createNode(6);
	 A->right->right = createNode(7);
	 
	 TreeNode *B = createNode(0);
	 B->left = createNode(1);
	 B->right = createNode(5);
	 B->left->right = createNode(4);
	 B->right->left = createNode(6);
	 B->right->right = createNode(7);
	 
	 if(isSame(A,B))
	 	cout<<"Same\n";
	else
		cout<<"Not Same\n";
	return 0;
}