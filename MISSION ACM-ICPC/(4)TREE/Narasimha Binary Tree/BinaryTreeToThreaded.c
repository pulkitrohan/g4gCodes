#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
    int isThreaded;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    temp->isThreaded = 0;
    return temp;
}

void BinaryToThreaded(struct Tree *root)
{
    if(!root)
        return ;
    struct Tree *current,*pre;
    current = root;
    while(current)
    {
        if(!current->left)
        {
           // printf("%d ",current->data);
            current = current->right;
        }
        else
        {
            pre = current->left;
            while(pre->right && pre->right != current)
                pre = pre->right;
            if(!pre->right)
            {
                pre->right = current;
                pre->isThreaded = 1;
                current = current->left;
            }
            else
            {
               // pre->right = NULL;
               // printf("%d ",current->data);
                current = current->right;
            }
        }
    }
}

struct Tree *leftmost(struct Tree *node)
{
	if(!node)
		return NULL;
	while(node->left)
		node = node->left;
	return node;
}

void inorder(struct Tree *root)
{
	struct Tree *cur = leftmost(root);
	while(cur)
	{
		printf("%d ",cur->data);
		if(cur->isThreaded)
			cur = cur->right;
		else
			cur = leftmost(cur->right);
	}
}

int main(void)
{
    struct Tree *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    root->right->left = CreateNode(6);
    root->right->right = CreateNode(7);
    BinaryToThreaded(root);
	inorder(root);
	/*
        1
      /  \
     2    3
    / \  / \
   4  5 6   7
   Inorder : 4 2 5 1 6 3 7

   */
    return 0;
}
