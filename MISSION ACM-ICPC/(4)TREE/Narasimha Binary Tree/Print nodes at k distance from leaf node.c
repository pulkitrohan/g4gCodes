#include<bits/stdc++.h>
using namespace std;
#define MAX 100
struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void PrintLeaf(struct Tree *root,int *Path,int *visited,int pathlen,int k)
{
	if(root)
	{
		Path[pathlen] = root->data;
		visited[pathlen++] = 0;
		if(!root->left && !root->right && (pathlen-1) >= k && !visited[pathlen-k-1])
		{
			cout<<Path[pathlen-k-1]<<" ";
			visited[pathlen-k-1] = 1;
		}
		else
		{
			PrintLeaf(root->left,Path,visited,pathlen,k);
			PrintLeaf(root->right,Path,visited,pathlen,k);
		}
	}
}

void PrintLeafDistanceK(struct Tree *root,int k)
{
    int Path[100] = {0};
    int visited[100] = {0};
    PrintLeaf(root,Path,visited,0,k);
}

int main(void)
{
    struct Tree *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    root->right->left = CreateNode(6);
    root->right->right = CreateNode(7);
    root->right->left->right = CreateNode(8);
    PrintLeafDistanceK(root,1);
    return 0;
}
