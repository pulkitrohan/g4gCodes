#include<bits/stdc++.h>
using namespace std;
struct Tree
{
	int data;
	struct Tree *left,*right;
};

Tree *newNode(int data)
{
	Tree *temp = new Tree;
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp; 
}

void printPath(vector<int> &path,int index)
{
	for(int i=index;i<path.size();i++)
		cout<<path[i]<<" ";
	cout<<endl;
}

void printKPath(Tree *root,int k,vector<int> &path)
{
	if(root)
	{
		path.push_back(root->data);
		printKPath(root->left,k,path);
		printKPath(root->right,k,path);
		int sum = 0;
		for(int i=path.size()-1;i>=0;i--)
		{
			sum += path[i];
			if(sum == k)
				printPath(path,i);
		}
		path.pop_back();
	}
}

int main(void)
{
	Tree *root = newNode(1);
    root->left = newNode(3);
    root->left->left = newNode(2);
    root->left->right = newNode(1);
    root->left->right->left = newNode(1);
    root->right = newNode(-1);
    root->right->left = newNode(4);
    root->right->left->left = newNode(1);
    root->right->left->right = newNode(2);
    root->right->right = newNode(5);
    root->right->right->right = newNode(2);
   	int k = 5;
   	vector<int> path;
   	printKPath(root,k,path);
    return 0;
}