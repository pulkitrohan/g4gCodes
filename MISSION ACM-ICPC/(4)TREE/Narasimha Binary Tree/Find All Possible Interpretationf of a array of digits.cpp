#include<bits/stdc++.h>
using namespace std;
struct Tree
{
	int data;
	struct Tree *left,*right;
};

struct Tree *newNode(int data)
{
	struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

char tochar(int val)
{
	return ('a' + val-1);
}

void makeTree(string S,int A[],int N)
{
	if(N == 0)
		cout<<S<<endl;
	else
	{
		int val = A[0];
		makeTree(S+tochar(val),A+1,N-1);
		if(N > 1)
		{
			val = A[0]*10 + A[1];
			if(val < 27)
				makeTree(S+tochar(val),A+2,N-2);
		}
	}
}


int main(void)
{
	int A[] = {1,2,1};
	string S = "";
	makeTree(S,A,3);
	return 0;
}
