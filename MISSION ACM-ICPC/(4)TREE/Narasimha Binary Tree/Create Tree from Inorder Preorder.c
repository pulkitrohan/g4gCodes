#include<stdio.h>
#include<stdlib.h>
#include<string.h>
struct tNode
{
    char data;
    struct tNode *left,*right;
};

struct tNode *CreateNode(char data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

int Search(char *inorder,int instr,int inend,char data)
{
    int i;
    for(i=instr;i<=inend;i++)
        if(inorder[i] == data)
            return i;
}

struct tNode *CreateTree(char *inorder,char *preorder,int start,int end)
{
    static preIndex = 0;
    struct tNode *newNode = NULL;
    if(end >= start)
    {
        newNode = CreateNode(preorder[preIndex++]);
        if(start == end)  //Only 1 Node in the tree
            return newNode;
        int inIndex = Search(inorder,start,end,newNode->data);
        newNode->left = CreateTree(inorder,preorder,start,inIndex-1);
        newNode->right = CreateTree(inorder,preorder,inIndex+1,end);
    }
    return newNode;
}

void Inorder(struct tNode *root)
{
    if(root)
    {
        Inorder(root->left);
        printf("%c " ,root->data);
        Inorder(root->right);
    }
}

int main(void)
{
    char inorder[100],preorder[100];
    printf("Enter the inorder traversal : ");
    scanf("%s",inorder);
    printf("Enter the preorder traversal : ");
    scanf("%s",preorder);
    int len = strlen(inorder);
    struct tNode *root = CreateTree(inorder,preorder,0,len-1);
    Inorder(root);
    return 0;
}
