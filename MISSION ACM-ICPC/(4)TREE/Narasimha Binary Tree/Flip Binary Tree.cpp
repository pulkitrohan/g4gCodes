#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right;
};

Tree *newNode(int data)
{
	Tree *temp = new Tree;
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

void inorder(Tree *root)
{
	if(root)
	{
		inorder(root->left);
		cout<<root->data<<" ";
		inorder(root->right);
	}
}

Tree *flipTree(Tree *root)
{
	if(!root)
		return root;
	if(!root->left && !root->right)
		return root;
	Tree *node = flipTree(root->left);
	root->left->left = root->right;
	root->left->right = root;
	root->left = root->right = NULL;
	return node;
}

int main(void)
{
	Tree *root = newNode(10);
    root->left = newNode(12);
    root->right = newNode(15);
    root->left->left = newNode(25);
    root->left->right = newNode(30);
    root->right->left = newNode(36);
    inorder(root);
    cout<<endl;
    root = flipTree(root);
    inorder(root);;
    return 0;
}