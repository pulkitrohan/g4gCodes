#include<bits/stdc++.h>
using namespace std;

struct Tree
{
    int data;
    struct Tree *left,*right;
};

Tree *newNode(int data)
{
    Tree *temp = new Tree;
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void printPath(Tree *cur,map<Tree *,Tree *> parent)
{
    stack<Tree *> S;
    while(cur)
    {
        S.push(cur);
        cur = parent[cur];
    }
    while(!S.empty())
    {
        cur = S.top();S.pop();
        cout<<cur->data<<" ";
    }
    cout<<endl;
}

void printRootToLeaf(Tree *root)
{
    if(!root)
        return;
    stack<Tree *> S;
    S.push(root);
    map<Tree *,Tree *> parent;
    parent[root] = NULL;
    while(!S.empty())
    {
        Tree *cur = S.top();
        S.pop();
        if(!cur->left && !cur->right)
            printPath(cur,parent);
        if(cur->right)
        {
            parent[cur->right] = cur;
            S.push(cur->right);
        }
        if(cur->left)
        {
            parent[cur->left] = cur;
            S.push(cur->left);
        }
    }
}

int main(void)
{
    Tree* root = newNode(10);
    root->left = newNode(8);
    root->right = newNode(2);
    root->left->left = newNode(3);
    root->left->right = newNode(5);
    root->right->left = newNode(2);
    printRootToLeaf(root);
    return 0;
}