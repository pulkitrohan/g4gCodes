#include<stdio.h>
struct Tree
{
    int data;
    struct Tree *left,*right;
};


struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

int PrintSum(struct Tree *root,int sum)
{
    if(!root)
        return 0;
    sum = sum*10 + root->data;
    if(!root->left && !root->right)
        return sum;
    return PrintSum(root->left,sum) + PrintSum(root->right,sum);
}

int main(void)
{
    struct Tree *root = CreateNode(6);
    root->left = CreateNode(3);
    root->right = CreateNode(5);
    root->left->left = CreateNode(2);
    root->left->right = CreateNode(5);
    root->left->right->left = CreateNode(7);
    root->left->right->right = CreateNode(4);
    root->right->right = CreateNode(4);
    printf("%d ",PrintSum(root,0));

    return 0;
}
