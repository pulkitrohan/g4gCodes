#include<stdio.h>
struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void inorder(struct Tree *root)
{
    if(root)
    {
        inorder(root->left);
        printf("%d ",root->data);
        inorder(root->right);
    }
}

void Convert(struct Tree *root)
{
    int left=0,right=0,diff;
	if(!root || (!root->left && !root->right)
		return;
    else
    {
        Convert(root->left);
        Convert(root->right);
        if(root->left)
        left = root->left->data;
        if(root->right)
        right = root->right->data;
        diff = left + right - root->data;
        if(diff > 0)
            root->data += diff;
        if(diff < 0)
            increment(root,-diff);
    }
}

void increment(struct Tree *root,int diff)
{
    if(root->left)
    {
        root->left->data += diff;
        increment(root->left,diff);
    }
    else if(root->right)
    {
        root->right->data += diff;
        increment(root->right,diff);
    }

}

int main(void)
{
    struct Tree *root = CreateNode(50);
    root->left = CreateNode(7);
    root->right = CreateNode(2);
    root->left->left = CreateNode(3);
    root->left->right = CreateNode(5);
    root->right->left = CreateNode(1);
    root->right->right = CreateNode(30);
    inorder(root);
    Convert(root);
    printf("\n");
    inorder(root);
    return 0;
}
