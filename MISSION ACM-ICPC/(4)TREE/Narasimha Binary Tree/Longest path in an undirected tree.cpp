#include<bits/stdc++.h>
using namespace std;

class Graph
{
	int V;
	list<int> *adj;

	public:
		Graph(int V);
		void addEdge(int start,int end);
		void longestPathLength();
		pair<int,int> bfs(int start);
};

Graph::Graph(int V)
{
	this->V = V;
	adj = new list<int>[V];
}

void Graph::addEdge(int start,int end){
	adj[start].push_back(end);
	adj[end].push_back(start);
}

pair<int,int> Graph::bfs(int start)
{
	vector<int> distance(V,-1);
	queue<int> Q;
	Q.push(start);
	distance[start] = 0;
	while(!Q.empty())
	{
		int val = Q.front();
		Q.pop();
		for(auto it = adj[val].begin();it != adj[val].end();it++)
		{
			int v = *it;
			if(distance[v] == -1)
			{
				Q.push(v);
				distance[v] = distance[val] + 1;
			}
		}
	}
	int maxDistance = 0,nodeIndex;
	for(int i=0;i<V;i++)
	{
		if(distance[i] > maxDistance)
		{
			nodeIndex = i;
			maxDistance = distance[i];
		}
	}
	return make_pair(nodeIndex,maxDistance);
}

void Graph::longestPathLength()
{
	pair<int,int> t1,t2;

	t1 = bfs(0);
	t2 = bfs(t1.first);
	cout<<"Longest path is from "<<t1.first<<" to "<<t2.first<<" of length "<<t2.second;
}


int main(void)
{
	Graph g(10);
	g.addEdge(0, 1);
    g.addEdge(1, 2);
    g.addEdge(2, 3);
    g.addEdge(2, 9);
    g.addEdge(2, 4);
    g.addEdge(4, 5);
    g.addEdge(1, 6);
    g.addEdge(6, 7);
    g.addEdge(6, 8);

    g.longestPathLength();
    return 0;
}


