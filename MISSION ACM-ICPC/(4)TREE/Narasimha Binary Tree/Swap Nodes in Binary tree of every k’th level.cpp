#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right;
};

Tree *newNode(int data)
{
	Tree *temp = new Tree;
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

bool existPath(Tree *root,int A[],int N,int index)
{
	if(!root)
		return (N == index);
	if(!root->left && !root->right && root->data == A[index] && index == N-1)
		return true;
	return (index < N) && (root->data == A[index]) &&
		   (existPath(root->left,A,N,index+1) || existPath(root->right,A,N,index+1));
}

void swapNodes(Tree *root,int level,int k){
	if(root)
	{
		if((level+1)%k == 0)
			swap(root->left,root->right);
		swapNodes(root->left,level+1,k);
		swapNodes(root->right,level+1,k);
	}
}

void inorder(Tree *root)
{
	if(root)
	{
		inorder(root->left);
		cout<<root->data<<" ";
		inorder(root->right);
	}
}

int main(void)
{
	Tree *root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->right->right = newNode(8);
    root->right->left = newNode(7);
    int k = 2;
    inorder(root);
    swapNodes(root,1,k);
    cout<<endl;
    inorder(root);
}