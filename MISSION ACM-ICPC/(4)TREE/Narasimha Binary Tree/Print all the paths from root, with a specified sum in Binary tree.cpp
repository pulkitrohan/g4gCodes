#include<bits/stdc++.h>
using namespace std;
struct Tree
{
	int data;
	struct Tree *left,*right;
};

Tree *newNode(int data)
{
	Tree *temp = new Tree;
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp; 
}

void printPath(vector<int> &path,int index)
{
	for(int i=index;i<path.size();i++)
		cout<<path[i]<<" ";
	cout<<endl;
}

void printKPath(Tree *root,int k,int sum,vector<int> &path)
{
	if(root)
	{
		path.push_back(root->data);
		sum += root->data;
		if(sum == k)
		{
			for(int i=0;i<path.size();i++)
				cout<<path[i]<<" ";
			cout<<endl;
		}
		if(root->left)
			printKPath(root->left,k,sum,path);
		if(root->right)
			printKPath(root->right,k,sum,path);
		path.pop_back();
	}
}

int main(void)
{
	Tree *root  = newNode(10);
    root->left  = newNode(28);
    root->right = newNode(13);
 
    root->right->left   = newNode(14);
    root->right->right  = newNode(15);
 
    root->right->left->left   = newNode(21);
    root->right->left->right  = newNode(22);
    root->right->right->left  = newNode(23);
    root->right->right->right = newNode(24);
   	int k = 38;
   	vector<int> path;
   	int sum = 0;
   	printKPath(root,k,sum,path);
    return 0;
}