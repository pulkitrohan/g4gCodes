#include<cstdio>
#include<cstdlib>
#include<queue>

using namespace std;

struct Tree
{
    int data;
    struct Tree *left,*right,*nextRight;
};

struct Tree *Insert(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = temp->nextRight = NULL;
    return temp;
}

void Connect(struct Tree *root)
{
    if(!root)
        return;
    queue<struct Tree *> Q;
    Q.push(root);
    Q.push(NULL);
    struct Tree *temp;
    while(!Q.empty())
    {
        temp = Q.front();
        Q.pop();
        if(!temp)
        {
            if(!Q.empty())
                Q.push(NULL);
        }
        else
        {
            temp->nextRight = Q.front();
            if(temp->left)
                Q.push(temp->left);
            if(temp->right)
                Q.push(temp->right);
        }
    }
}


int main(void)
{
    struct Tree *root = Insert(1);
    root->left = Insert(2);
    root->right = Insert(3);
    root->left->left = Insert(4);
    root->left->right = Insert(5);
    root->right->right = Insert(6);
    Connect(root);
    printf("%d\n",root->left->nextRight->data);
    return 0;
}
