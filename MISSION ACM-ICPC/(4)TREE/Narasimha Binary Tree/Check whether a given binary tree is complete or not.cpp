#include<bits/stdc++.h>
using namespace std;

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

int Complete(struct Tree *root)
{
    if(!root)
        return 1;
    queue<struct Tree *> Q;
    int flag = 0;
    Q.push(root);
    while(!Q.empty())
    {
        struct Tree *temp = Q.front();
        Q.pop();
        if(temp->left)
        {
            if(flag)
                return 0;
            Q.push(temp->left);
        }
        else
            flag = 1;
        if(temp->right)
        {
            if(flag)
                return 0;
            Q.push(temp->right);
        }
        else
            flag = 1;;
    }
    return 1;
}

int main(void)
{
    struct Tree *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->right->left = CreateNode(5);
    root->left->right = CreateNode(4);
    root->right->right = CreateNode(6);
    if(Complete(root))
        printf("Tree is Complete\n");
    else
        printf("Tree is not Complete\n");
    return 0;
}
