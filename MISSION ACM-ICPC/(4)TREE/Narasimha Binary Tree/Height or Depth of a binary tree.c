#include<stdio.h>
#define max(A,B) ((A > B) ? A : B)
struct Tree
{
    int data;
    struct Tree *left;
    struct Tree *right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

int Height(struct Tree *root)
{
    if(!root)
        return 0;
    return max(Height(root->left),Height(root->right))+ 1;
}

int main(void)
{
    struct Tree *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    root->right->left = CreateNode(6);
    root->right->right = CreateNode(7);
    printf("Height of Tree : %d\n",Height(root));
    return 0;
}
