#include<cstdio>
#include<cstdlib>
#include<queue>
using namespace std;

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void PrintLeftView(struct Tree *root)
{
    queue<struct Tree *> Q;
    Q.push(root);
    Q.push(NULL);
    int level = 0,max;
    struct Tree *temp;
    while(!Q.empty())
    {
        temp = Q.front();
        Q.pop();
        if(!temp)
        {
            if(!Q.empty())
                Q.push(NULL);
            level++;
        }
        else
        {
            if(temp->left)
                Q.push(temp->left);
            if(temp->right)
                Q.push(temp->right);
            if(!temp->left && !temp->right && level%2)
                max = level;
        }
    }
    printf("%d",max);
}

int main(void)
{
    struct Tree *root = CreateNode(12);
    root->left = CreateNode(10);
    root->right = CreateNode(30);
    root->right->left = CreateNode(25);
    root->right->right = CreateNode(40);
    PrintLeftView(root);
    return 0;
}
