#include<bits/stdc++.h>
using namespace std;
struct Tree
{
	char data;
	struct Tree *left,*right;
};

const char MARKER = '$';

Tree *newNode(char data)
{
	Tree *temp = new Tree;
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp; 
}

unordered_set<string> subtrees;

string dupSubUtil(Tree *root)
{
	string s = "";
	if(!root)
		return s + MARKER;

	string left = dupSubUtil(root->left);
	if(left.compare(s) == 0)
		return s;

	string right = dupSubUtil(root->right);
	if(right.compare(s) == 0)
		return s;

	s += root->data + left + right;

	if(s.length() > 3 && subtrees.find(s) != subtrees.end())
		return "";

	subtrees.insert(s);
	return s;
	
}

int main(void)
{
	Tree *root = newNode('A');
	root->left = newNode('B');
    root->right = newNode('C');
    root->left->left = newNode('D');
    root->left->right = newNode('E');
    root->right->right = newNode('B');
    root->right->right->right = newNode('E');
    root->right->right->left= newNode('D');
    string S = dupSubUtil(root);
    if(S.compare("") == 0)
    	cout<<"Yes";
    else
    	cout<<"No";
    return 0;
}