#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *newNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void Inorder(struct Tree *root)
{
    if(root)
    {
        Inorder(root->left);
        printf("%d ",root->data);
        Inorder(root->right);
    }
}

struct Tree *Prune(struct Tree *root)
{
	if(!root)
		return NULL;
    root->left = Prune(root->left);
    root->right = Prune(root->right);
    if(!root->left && !root->right)
        return root;
    if(!root->left)
	{
		struct Tree *temp = root->right;
		free(root);
		return temp;
	}
	if(!root->right)
	{
		struct Tree *temp = root->left;
		free(root);
		return temp;
	}
  	return root;
}

int main(void)
{
    struct Tree *root = newNode(2);
    root->left = newNode(7);
    root->right = newNode(5);
    root->left->right = newNode(6);
    root->right->right = newNode(9);
    root->left->right->left = newNode(1);
    root->left->right->right = newNode(11);
    root->right->right->left = newNode(4);
    Inorder(root);
    printf("\n");
    root = Prune(root);
    Inorder(root);
    return 0;
}
