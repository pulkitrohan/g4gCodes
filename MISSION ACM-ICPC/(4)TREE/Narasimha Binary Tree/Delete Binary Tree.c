#include<stdio.h>
#include<limits.h>
#include<stdlib.h>
struct tNode
{
    int data;
    struct tNode *left;
    struct tNode *right;
};


struct tNode *CreateNode(int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = NULL;
    temp->right = NULL;
    return temp;
}

struct tNode *DeleteTree(struct tNode *root)
{
    if(root)
    {
        DeleteTree(root->left);
        DeleteTree(root->right);
        free(root);
    }
    return NULL;
}

void Inorder(struct tNode *root)
{
    if(root)
    {
        Inorder(root->left);
        printf("%d ",root->data);
        Inorder(root->right);
    }
}
int main(void)
{
    struct tNode *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    root->right->left = CreateNode(6);
    root->right->right = CreateNode(7);
    root = DeleteTree(root);
    root = CreateNode(1);
    Inorder(root);
    return 0;
}
