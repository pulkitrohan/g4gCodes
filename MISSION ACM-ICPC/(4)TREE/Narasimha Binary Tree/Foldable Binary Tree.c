// tree can be folded if left and right subtrees
// of the tree are structure wise mirror image of each other.
// An empty tree is considered as foldable.

#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

int IsMirror(struct Tree *root,struct Tree *root1)
{
    if(!root && !root1)
        return 1;
    else if(!root || !root1)
        return 0;
    else
        return ( IsMirror(root->left,root1->right) && IsMirror(root->right,root1->left));
}

int IsFoldable(struct Tree *root)
{
    if(!root)
        return 1;
    else
        return IsMirror(root->left,root->right);
}


int main(void)
{
    struct Tree *root = CreateNode(10);
    root->left = CreateNode(7);
    root->right = CreateNode(15);
    root->left->left = CreateNode(9);
    root->right->left = CreateNode(11);
    if(IsFoldable(root))
        printf("Yes,Foldable\n");
    else
        printf("Not Foldable\n");
    return 0;
}

