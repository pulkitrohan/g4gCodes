#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *newNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void Inorder(struct Tree *root)
{
    if(root)
    {
        Inorder(root->left);
        printf("%d ",root->data);
        Inorder(root->right);
    }
}

struct Tree *Prune(struct Tree *root,int k)
{
    if(root)
    {
        root->left = Prune(root->left,k-root->data);
        root->right = Prune(root->right,k-root->data);
        if(!root->left && !root->right)
        {
            if(root->data < k)
            {
                free(root);
                return NULL;
            }
        }
    }
    return root;
}

int main(void)
{
    struct Tree *root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);
    root->right->left = newNode(6);
    root->right->right = newNode(7);
    root->left->left->left = newNode(8);
    root->left->left->right = newNode(9);
    root->left->right->left = newNode(12);
    root->right->right->left = newNode(10);
    root->right->right->left->right = newNode(11);
    root->left->left->right->left = newNode(13);
    root->left->left->right->right = newNode(14);
    root->left->left->right->right->left = newNode(15);
    Inorder(root);
    printf("\n");
    root = Prune(root,45);

    Inorder(root);

    return 0;
}
