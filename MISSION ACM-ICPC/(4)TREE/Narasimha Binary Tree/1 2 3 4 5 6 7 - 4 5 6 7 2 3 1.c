#include<stdio.h>
#include<limits.h>
#include<stdlib.h>
struct tNode
{
    int data;
    struct tNode *left;
    struct tNode *right;
};


typedef struct Queue    //Queue
{
	struct tNode *t;
	struct Queue *next;
}Queue;

Queue *rear = NULL,*front = NULL;   //Rear and front for Queue

//Queue ADT Starts
void Enqueue(struct tNode *root)
{
    Queue *temp = (Queue *)malloc(sizeof(Queue));
    temp->t = root;
    temp->next = NULL;
    if(rear == NULL)
        rear = front = temp;
    else
    {
        rear->next = temp;
        rear = temp;
    }
}

struct tNode *Dequeue()
{
    if(isEmptyQueue())
        {
            return NULL;
        }
    else if(front == rear)
    {
        Queue *temp = front;
        front = rear = NULL;
            return temp->t;
    }
    else
    {
        Queue *temp = front;
        front = front->next;
        return temp->t;
    }
}

int isEmptyQueue()
{
    if(front == NULL)
        return 1;
    else
        return 0;
}
//Queue ADT Over



typedef struct Node   //Stack
{
    struct tNode *t;
    struct Node *next;
}node;

//Stack ADT Starts
void push(node *S,struct tNode *root)
{
		node *temp;
        temp= (node *)malloc(sizeof(node));
        temp->next = S->next;
        temp->t = root;
        S->next = temp;
}

struct tNode* pop(node *S)
{
        node *temp;
        temp = S->next;
        S->next = temp->next;
        return temp->t;
}

struct tNode *Top(node *S)
{
    return S->next->t;
}

int isEmpty(node *S)
{
    if(S->next == NULL)
        return 1;
    else return 0;
}

//Stack ADT Over

PrintPattern(struct tNode *root)
{
    Enqueue(root);
    node *S = (node *)malloc(sizeof(node));
    S->next = NULL;
	while(!isEmptyQueue())
	{
		root = Dequeue();
		push(S,root);
		if(root->right)
            Enqueue(root->right);
		if(root->left)
            Enqueue(root->left);
	}
    while(!isEmpty(S))
    {
        root = pop(S);
        printf("%d ",root->data);
    }
}

struct tNode *CreateNode(int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = NULL;
    temp->right = NULL;
}
void levelOrder(struct tNode *root)
{
	Enqueue(root);
	while(!isEmptyQueue())
	{
		root = Dequeue();
		printf("%d " ,root->data);
		if(root->left != NULL)
            Enqueue(root->left);
		if(root->right != NULL)
            Enqueue(root->right);
	}
}


int main(void)
{
    struct tNode *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    root->right->left = CreateNode(6);
    root->right->right = CreateNode(7);
    printf("Leve Order Traversal : ");
    levelOrder(root);
    printf("\nSpecified Order :");
    PrintPattern(root);
    return 0;
}
