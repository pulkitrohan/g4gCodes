#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}


void PreOrder(struct Tree *root) //Very Similar to Morris Traversal for InOrder
{
    struct Tree *cur = root;
    while(cur)
    {
        if(!cur->left)
        {
            printf("%d ",cur->data);
            cur = cur->right;
        }
        else
        {
            struct Tree *pre = cur->left;
            while(pre->right && pre->right != cur)
                pre = pre->right;
            if(!pre->right)
            {
                printf("%d ",cur->data);
                pre->right = cur;
                cur = cur->left;
            }
            else
            {
                pre->right = NULL;
                cur = cur->right;
            }
        }
    }
}

int main(void)
{
    struct Tree *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    root->right->left = CreateNode(6);
    root->right->right = CreateNode(7);
    PreOrder(root);
    return 0;
}

