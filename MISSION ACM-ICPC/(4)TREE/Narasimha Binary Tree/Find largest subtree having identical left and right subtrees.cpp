#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right;
};

Tree *newNode(int data)
{
	Tree *temp = new Tree;
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

void inorder(Tree *root)
{
	if(root)
	{
		inorder(root->left);
		cout<<root->data<<" ";
		inorder(root->right);
	}
}

int largestSubtreeUtil(Tree *root,string &S,int &maxSize,Tree* &maxNode)
{
	if(!root)
		return 0;
	string left = "",right = "";
	int ls = largestSubtreeUtil(root->left,left,maxSize,maxNode);
	int rs = largestSubtreeUtil(root->right,right,maxSize,maxNode);
	int size = ls + rs + 1;
	if(left.compare(right) == 0)
	{
		if(size > maxSize)
		{
			maxNode = root;
			maxSize = size;
		}
	}
	S.append("|").append(left).append("|");
	S.append("|").append(to_string(root->data)).append("|");
	S.append("|").append(right).append("|");
	return size;
}

int largestSubtree(Tree *root,Tree * &maxNode)
{
	int maxSize = 0;
	string S = "";
	largestSubtreeUtil(root,S,maxSize,maxNode);
	return maxSize;
}

int main(void)
{
	Tree* root = newNode(50);
    root->left = newNode(10);
    root->right = newNode(60);
    root->left->left = newNode(5);
    root->left->right = newNode(20);
    root->right->left = newNode(70);
    root->right->left->left = newNode(65);
    root->right->left->right = newNode(80);
    root->right->right = newNode(70);
    root->right->right->left = newNode(65);
    root->right->right->right = newNode(80);
    Tree *maxNode = NULL;
    cout<<largestSubtree(root,maxNode)<<endl;
    cout<<maxNode->data;
    return 0;
}