#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right,*random;
};

struct Tree *newNode(int data)
{
	struct Tree *node = (struct Tree *)malloc(sizeof(struct Tree));
	node->data = data;
	node->left = node->right = node->random = NULL;
	return node;
}

struct Tree *copyLeftRightNode(struct Tree *root)
{
	if(!root)
		return NULL;
	struct Tree *left = root->left;
	root->left = newNode(root->data);
	root->left->left = left;
	if(left)
		left->left = copyLeftRightNode(left);
	root->left->right = copyLeftRightNode(root->right);
	return root->left;
}

void copyRandomNode(struct Tree *root,struct Tree *clone)
{
	if(!root)
		return;
	if(root->random)
		clone->random = root->random->left;
	else
		clone->random = NULL;
	if(root->left && clone->left)
		copyRandomNode(root->left->left,clone->left->left);
	copyRandomNode(root->right,clone->right);
}

void restoreTreeLeftNode(struct Tree *root,struct Tree *clone)
{
	if(!root)
		return;
	if(clone->left)
	{
		struct Tree *cloneLeft = clone->left->left;
		root->left = root->left->left;
		clone->left = cloneLeft;
	}
	else
		root->left = NULL;
	restoreTreeLeftNode(root->left,clone->left);
	restoreTreeLeftNode(root->right,clone->right);
}

struct Tree *cloneTree(struct Tree *root)
{
	if(!root)
		return NULL;
	struct Tree *clone = copyLeftRightNode(root);
	copyRandomNode(root,clone);
	restoreTreeLeftNode(root,clone);
	return clone;
}

void inorder(struct Tree *root)
{
	if(root)
	{
		inorder(root->left);
		printf("[ %d ",root->data);
		if(root->random)
			printf("%d ",root->randome->data);
		else
			printf(" NULL ");
		printf("]");
		inorder(root->right);
	}
}

int main(void)
{
	struct Tree *tree = newNode(1);
	tree->left = newNode(2);
    tree->right = newNode(3);
    tree->left->left = newNode(4);
    tree->left->right = newNode(5);
    tree->random = tree->left->right;
    tree->left->left->random = tree;
    tree->left->right->random = tree->right;
	inorder(tree);
	struct Tree *clone = cloneTree(tree);
	inorder(clone);
	return 0;
}