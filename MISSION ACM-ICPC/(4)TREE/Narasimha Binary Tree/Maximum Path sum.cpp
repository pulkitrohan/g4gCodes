#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right;
};

struct Tree *newNode(int data)
{
	struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

int FindMax(Tree *root,int &ans)
{
	if(!root)
		return 0;
	if(!root->left && !root->right)
		return root->data;
	int left = FindMax(root->left,ans);
	int right = FindMax(root->right,ans);
	ans = max(ans,left+root->data);
	ans = max(ans,right+root->data);
	ans = max(ans,left+right+root->data);
	ans = max(ans,root->data);
	return max(root->data,max(left+root->data,right+root->data));
}

int main(void)
{
	struct Tree *root = newNode(10);
	root->left        = newNode(2);
    root->right       = newNode(10);
    root->left->left  = newNode(20);
    root->left->right = newNode(1);
    root->right->right = newNode(-25);
    root->right->right->left   = newNode(3);
    root->right->right->right  = newNode(4);
	int ans = INT_MIN;
	FindMax(root,ans);
	cout<<ans<<endl;
	return 0;
}
	