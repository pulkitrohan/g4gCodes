#include<cstdio>
#include<cstdlib>
#include<queue>
using namespace std;
struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

int CheckLevelLeaves(struct Tree *root)
{
    queue<struct Tree *> Q;
    Q.push(root);
    Q.push(NULL);
    int flag = 0;
    while(!Q.empty())
    {
        struct Tree *temp = Q.front();
        Q.pop();
        if(!temp)
        {
            if(!Q.empty())
            {
                if(flag)    //Means there is another level below it
                    return 0;
                else
                    Q.push(NULL);
            }
        }
        else
        {
        if(temp->left)
            Q.push(temp->left);
        if(temp->right)
            Q.push(temp->right);
        if(!temp->left && !temp->right)
            flag = 1;
        }
    }
    return 1;
}

int main(void)
{
    struct Tree *root = CreateNode(12);
    root->left = CreateNode(5);
    root->left->right = CreateNode(7);
    root->left->left = CreateNode(3);
    root->left->left->left = CreateNode(1);
    root->left->right->left = CreateNode(2);
    if(!CheckLevelLeaves(root))
        printf("Not at same level\n");
    else
        printf("At same level\n");
    return 0;
}
