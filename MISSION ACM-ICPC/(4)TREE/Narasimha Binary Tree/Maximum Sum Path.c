//Algorithm:
//1)Start from the root.If root is NULL then return.else push the root into the pathArray.
//2)Check whether node is a leaf.If it is a leaf then print the pathArray
    //else Recursively call for left child and right child of the node.

#include<stdio.h>
#include<stdlib.h>
struct tNode
{
    int data;
    struct tNode *left,*right;
};

struct tNode *CreateNode(int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void PrintPaths(struct tNode *root,int *max,int cur_sum,struct tNode **node)
{
    if(root)
    {
        cur_sum += root->data;
        if(!root->left && !root->right)
        {
            if(cur_sum > *max)
            {
                *max = cur_sum;
                *node = root;
            }
        }
        else
        {
            PrintPaths(root->left,max,cur_sum,node);
            PrintPaths(root->right,max,cur_sum,node);
        }
    }
}

int PrintPath(struct tNode *root,struct tNode *node)
{
    if(root)
    {
        if(root == node)
        {
            printf("%d ",root->data);
            return 1;
        }
        if(PrintPath(root->left,node) || PrintPath(root->right,node))
        {
            printf("%d ",root->data);
            return 1;
        }
    }
}

int main(void)
{
    struct tNode *root = CreateNode(10);
    root->left = CreateNode(-2);
    root->right = CreateNode(7);
    root->left->left = CreateNode(8);
    root->left->right = CreateNode(-4);
    int max = 0;
    struct tNode *node = NULL;
    int cur_sum = 0;
    PrintPaths(root,&max,cur_sum,&node);
    //printf("%d",max);
    PrintPath(root,node);
    return ;
}
