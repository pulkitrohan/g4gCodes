#include<cstdio>
#include<cstdlib>
#include<queue>

using namespace std;

struct Tree
{
    int data;
    struct Tree *left,*right,*nextRight;
};

struct Tree *Insert(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = temp->nextRight = NULL;
    return temp;
}

struct Tree *getNextRight(struct Tree *root)
{
	struct Tree *temp = root->nextRight;
	while(temp)
	{
		if(temp->left)	
			return temp->left;
		if(temp->right)
			return temp->right;
		temp = temp->nextRight;
	}
	return NULL;
}

void Connect(struct Tree *root)
{
    if(!root)
		return;
	root->nextRight = NULL;
	while(root)
	{
		struct Tree *cur = root;
		while(cur)
		{
			if(cur->left)
			{
				if(cur->right)
					cur->left->nextRight = cur->right;
				else
					cur->left->nextRight = getNextRight(cur);
			}
			if(cur->right)
				cur->right->nextRight = getNextRight(cur);
			cur = cur->nextRight;
		}
		if(root->left)
			root = root->left;
		else if(root->right)
			root = root->right;
		else
			root = getNextRight(root);
	}
}


int main(void)
{
    struct Tree *root = Insert(1);
    root->left = Insert(2);
    root->right = Insert(3);
    root->left->left = Insert(4);
    root->left->right = Insert(5);
    root->right->right = Insert(6);
	root->nextRight = NULL;
    Connect(root);
    return 0;
}
