#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *newNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

int IsIsomorphic(struct Tree *root1,struct Tree *root2)
{
    if(!root1 && !root2)
        return 1;
    if(!root1 || !root2)
        return 0;
    return (root1->data == root2->data) && (IsIsomorphic(root1->left,root2->left) && IsIsomorphic(root1->right,root2->right) || IsIsomorphic(root1->left,root2->right) && IsIsomorphic(root1->right,root2->left));
}

int main(void)
{
    struct Tree *root1 = newNode(1);
    root1->left        = newNode(2);
    root1->right       = newNode(3);
    root1->left->left  = newNode(4);
    root1->left->right = newNode(5);
    root1->right->left  = newNode(6);
    root1->left->right->left = newNode(7);
    root1->left->right->right = newNode(8);
    struct Tree *root2 = newNode(1);
    root2->left         = newNode(3);
    root2->right        = newNode(2);
    root2->right->left   = newNode(4);
    root2->right->right   = newNode(5);
    root2->left->right   = newNode(6);
    root2->right->right->left = newNode(8);
    root2->right->right->right = newNode(7);
    if(IsIsomorphic(root1,root2))
        printf("Yes\n");
    else
        printf("No\n");
    return 0;
}
