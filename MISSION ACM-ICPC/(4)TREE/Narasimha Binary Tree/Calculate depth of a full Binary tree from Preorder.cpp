#include<bits/stdc++.h>
using namespace std;

int findDepth(char tree[],int N,int &index)
{
	if(index >= N || tree[index] == 'l')
		return 0;
	index++;
	int left = findDepth(tree,N,index);
	index++;
	int right = findDepth(tree,N,index);
	return max(left,right)+1;
}

int main(void)
{
	char tree[] = "nlnnlll";
	int N = strlen(tree);
	int index = 0;
	cout<<findDepth(tree,N,index);
	return 0;
}