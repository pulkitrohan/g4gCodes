#include<bits/stdc++.h>
using namespace std;
struct tNode
{
    int data;
    struct tNode *left;
    struct tNode *right;
};

struct tNode *CreateNode(int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = NULL;
    temp->right = NULL;
}

void DiagonalSum(struct tNode *root,map<int,int> &M,int col)
{
	if(root)
	{
		DiagonalSum(root->right,M,col);
		M[col] += root->data;
		DiagonalSum(root->left,M,col+1);
	}
}

int main(void)
{
    struct tNode *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(9);
    root->left->right = CreateNode(6);
    root->right->left = CreateNode(4);
    root->right->right = CreateNode(5);
    root->left->left->right = CreateNode(10);
    root->left->right->left = CreateNode(11);
    root->right->left->left = CreateNode(12);
    root->right->left->right = CreateNode(7);
    map<int,int> M;
    DiagonalSum(root,M,0);
    map<int,int> :: iterator it;
    for(it = M.begin() ; it != M.end();it++)
        cout<<it->second<<endl;
    return 0;
}
