#include<cstdio>
#include<queue>
#include<cstdlib>
using namespace std;

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

int MaxWidth(struct Tree *root)
{
    if(!root)
        return 0;
    queue<struct Tree *> Q;
    Q.push(root);
    Q.push(NULL);
    int count = 0;
    int max = 0;
    while(!Q.empty())
    {
        struct Tree *temp = Q.front();
        Q.pop();
        count++;
        if(!temp)
        {
            if(!Q.empty())
                Q.push(NULL);
            if(count > max)
                max = count;
            count = 0;
        }
        else
        {
            if(temp->left)
            Q.push(temp->left);
            if(temp->right)
            Q.push(temp->right);
        }
    }
    return max - 1;
}

int main(void)
{
    struct Tree *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    root->right->right = CreateNode(8);
    root->right->right->left = CreateNode(6);
    root->right->right->right = CreateNode(7);
    printf("Maximum Width is %d\n",MaxWidth(root));
    return 0;
}
