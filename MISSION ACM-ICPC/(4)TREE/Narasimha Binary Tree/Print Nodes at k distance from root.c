#include<stdio.h>
#include<stdlib.h>

struct tNode
{
    int data;
    struct tNode *left,*right;
};


struct tNode *CreateNode(int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void PrintNodes(struct tNode *root,int d)
{
    if(root)
    {
        if(d == 0)
            printf("%d ",root->data);
        else
        {
            PrintNodes(root->left,d-1);
            PrintNodes(root->right,d-1);
        }
    }
}

int main(void)
{
    struct tNode *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    root->right->left = CreateNode(8);
    int d;
    printf("Enter the distance : ");
    scanf("%d",&d);
    PrintNodes(root,d);
    return 0;
}
