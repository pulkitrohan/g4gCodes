#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *newNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void Inorder(struct Tree *root)
{
    if(root)
    {
        Inorder(root->left);
        printf("%d ",root->data);
        Inorder(root->right);
    }
}

struct Tree *Prune(struct Tree *root,int k,int level)
{
    if(root)
    {
        root->left = Prune(root->left,k,level+1);
        root->right = Prune(root->right,k,level+1);
        if(!root->left && !root->right)
        {
            if(k > level)
            {
            	free(root);
            	return NULL;
            }
        }
    }
    return root;
}

int main(void)
{
    struct Tree *root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);
    root->right->right = newNode(6);
    root->left->left->left = newNode(7);
    root->right->right->left = newNode(8);;
    Inorder(root);
    printf("\n");
    root = Prune(root,4,1);
    Inorder(root);
    return 0;
}
