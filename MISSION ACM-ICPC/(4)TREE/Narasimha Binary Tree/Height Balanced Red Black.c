#include<stdio.h>
#include<stdlib.h>

struct Tree
{
	int data;
	struct Tree *left,*right;
};

struct Tree *newNode(int data)
{
	struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

#define max2(A,B) ((A > B) ? A : B)
#define min2(A,B) ((A < B) ? A : B)

int isBalanced(struct Tree *root,int &max,int &min)
{
	if(!root)
	{
		max = min = 0;
		return 1;
	}

	int lmax,rmax,lmin,rmin;
	int left = isBalanced(root->left,lmax,lmin);
	int right = isBalanced(root->right,rmax,rmin);
	max = max2(lmax,rmax)+1;
	min = min2(lmin,rmin)+1;
	if(max > 2*min)
		return 0;
	return left && right;
}


int main(void)
{
	struct Tree *root = newNode(10);
    root->left = newNode(5);
    root->right = newNode(100);
    root->right->left = newNode(50);
    root->right->right = newNode(150);
    root->right->left->left = newNode(40);
	int max = 0,min = 0;
	if(isBalanced(root,max,min))
		printf("Balanced\n");
	else
		printf("Not Balanced\n");
	return 0;
}
