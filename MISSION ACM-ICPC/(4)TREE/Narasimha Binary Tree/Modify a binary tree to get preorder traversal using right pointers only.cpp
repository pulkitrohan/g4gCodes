#include<bits/stdc++.h>
using namespace std;

struct Node {
	int data;
	Node *left, *right;
};

Node* newNode(int data) 
{ 
    Node* temp = new Node; 
    temp->data = data; 
    temp->left = temp->right = NULL; 
    return temp; 
} 

void printpre(struct Node* root) 
{ 
    while (root != NULL) { 
        cout << root->data << " "; 
        root = root->right; 
    } 
} 

void modifytree(struct Node *root) {
	if(!root)
		return;
	stack<Node *> S;
	S.push(root);
	Node *pre = NULL;
	while(!S.empty()) {
		Node *node = S.top();
		S.pop();
		if(node->right)
			S.push(node->right);
		if(node->left)
			S.push(node->left);
		if(pre) 
			pre->right = node;
		pre = node;
	}
}

int main(void) {
    struct Node* root = newNode(10); 
    root->left = newNode(8); 
    root->right = newNode(2); 
    root->left->left = newNode(3); 
    root->left->right = newNode(5); 
  
    modifytree(root); 
    printpre(root); 

    return 0;
}