#include<bits/stdc++.h>
using namespace std;
struct Tree
{
    int data;
    struct Tree *left,*right;
};


struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

int IsIdentical(struct Tree *T,struct Tree *S)
{
    if(!T && !S)
        return 1;
    if(!T || !S)
        return 0;
    return ((T->data == S->data) && IsIdentical(T->left,S->left) && IsIdentical(T->right,S->right));
}

int IsSubTree(struct Tree *T,struct Tree *S)
{
    if(!S)
        return 1;
    if(!T)
        return 0;
    if(T->data == S->data)
        return IsIdentical(T,S);
    else
        return (IsSubTree(T->left,S) || IsSubTree(T->right,S));
}

void storeInorder(Tree *Node,string result)
{
    if(!Node)
        result += '$';
    else
    {
        storeInorder(Node->left,result);
        result += Node->data;
        storeInorder(Node->right,result);
    }
}


void storePreorder(Tree *Node,string result)
{
    if(!Node)
        result += '$';
    else
    {
        result += Node->data;
        storePreorder(Node->left,result);
        storePreorder(Node->right,result);
    }
}

bool isSubTreeOptimal(Tree *T,Tree *S)
{
    if(!S)
        return true;
    if(!T)
        return false;
    string Tin,Sin,Tpre,Spre;
    storeInorder(T,Tin);
    storeInorder(S,Sin);
    storePreorder(T,Tpre);
    storePreorder(S,Spre);
    return (Tin.find(Sin) != string::npos) && (Tpre.find(Spre) != string::npos);
}

int main(void)
{
    struct Tree *S = CreateNode(10);
    S->left = CreateNode(4);
    S->right = CreateNode(6);
    S->left->right = CreateNode(30);

    struct Tree *T = CreateNode(26);
    T->left = CreateNode(10);
    T->right = CreateNode(3);
    T->left->left = CreateNode(4);
    T->left->right = CreateNode(6);
    T->right->right = CreateNode(3);
    T->left->left->right = CreateNode(30);

    if(IsSubTree(T,S))
        printf("Yes given binary tree is subtree of another binary tree\n");
    else
        printf("No\n");
    cout<<isSubTreeOptimal(T,S);
    return 0;
}
