#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
};


struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

int IsLeaf(struct Tree *root)
{
    return !root->left && !root->right;
}

int SumTree(struct Tree *root)
{
    if(!root || IsLeaf(root))
        return 1;
    int left = 0,right = 0;
    if(!root->left)
        left = 0;
    else if(IsLeaf(root->left))
        left = root->left->data;
    else
        left = 2*(root->left->data);

    if(!root->right)
        right = 0;
    else if(IsLeaf(root->right))
        right = root->right->data;
    else
        right = 2*(root->right->data);

    return (left+right == root->data && SumTree(root->left) && SumTree(root->right));
}

int main(void)
{
    struct Tree *root = CreateNode(26);
    root->left = CreateNode(10);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(6);
    root->right->right = CreateNode(3);
    if(SumTree(root))
        printf("Tree is a Sum Tree\n");
    else
        printf("Not a sum Tree\n");
}
