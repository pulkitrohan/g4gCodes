#include<cstdio>
#include<cstdlib>
#include<queue>

using namespace std;

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
};

void Insert(struct Tree *&root,int data,queue<struct Tree *> &Q)
{

    struct Tree *temp = CreateNode(data);
    if(!root)
        root = temp;
    else
    {
        struct Tree *front = Q.front();
        if(!front->left)
            front->left = temp;
        else if(!front->right)
            front->right = temp;
        if(front->left && front->right)
            Q.pop();
    }
    Q.push(temp);
}

void Preorder(struct Tree *root)
{
    if(root)
    {
        printf("%d ",root->data);
        Preorder(root->left);
        Preorder(root->right);
    }
}

int main(void)
{
    queue<struct Tree *> Q;
    int i;
    struct Tree *root = NULL;
    for(i=1;i<=12;i++)
        Insert(root,i,Q);
    Preorder(root);
    return 0;
}
