#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right;
};

Tree *newnode(int data)
{
	Tree *temp = new Tree;
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

bool existPath(Tree *root,int A[],int N,int index)
{
	if(!root)
		return (N == index);
	if(!root->left && !root->right && root->data == A[index] && index == N-1)
		return true;
	return (index < N) && (root->data == A[index]) &&
		   (existPath(root->left,A,N,index+1) || existPath(root->right,A,N,index+1));
}

int main(void)
{
	int arr[] = {5, 8, 6, 7};
    int n = sizeof(arr)/sizeof(arr[0]);
    Tree *root = newnode(5);
    root->left    = newnode(3);
    root->right   = newnode(8);
    root->left->left = newnode(2);
    root->left->right = newnode(4);
    root->left->left->left = newnode(1);
    root->right->left = newnode(6);
    root->right->left->right = newnode(7);
    cout<<existPath(root,arr,n,0);
}