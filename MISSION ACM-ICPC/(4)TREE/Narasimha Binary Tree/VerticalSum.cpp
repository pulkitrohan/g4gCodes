#include<cstdio>
#include<cstdlib>
#include<map>
#include<iostream>
using namespace std;
struct tNode
{
    int data;
    struct tNode *left;
    struct tNode *right;
};

struct tNode *CreateNode(int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = NULL;
    temp->right = NULL;
}

void VerticalSum(struct tNode *root,int col,map<int,int> &M)
{
    if(root)
    {
        M[col] += root->data;
        VerticalSum(root->left,col-1,M);
        VerticalSum(root->right,col+1,M);
    }
}

int main(void)
{
    struct tNode *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    root->right->left = CreateNode(6);
    root->right->right = CreateNode(7);
    map<int,int> M;
    VerticalSum(root,0,M);
    map<int,int> :: iterator it;
    for(it = M.begin() ; it != M.end();it++)
        cout<<it->second<<endl;
    return 0;
}
