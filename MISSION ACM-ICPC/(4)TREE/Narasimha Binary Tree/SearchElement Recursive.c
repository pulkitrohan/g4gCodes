#include<stdio.h>
#include<limits.h>
#include<stdlib.h>
struct tNode
{
    int data;
    struct tNode *left;
    struct tNode *right;
};

typedef struct Node   //Stack
{
    struct tNode *t;
    struct Node *next;
}node;

//Stack ADT Starts
void push(node *S,struct tNode *root)
{
		node *temp;
        temp= (node *)malloc(sizeof(node));
        temp->next = S->next;
        temp->t = root;
        S->next = temp;
}

struct tNode* pop(node *S)
{
        node *temp;
        temp = S->next;
        S->next = temp->next;
        return temp->t;
}

struct tNode *Top(node *S)
{
    return S->next->t;
}

int isEmpty(node *S)
{
    if(S->next == NULL)
        return 1;
    else return 0;
}

//Stack ADT Over

struct tNode *CreateNode(int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = NULL;
    temp->right = NULL;
}

void inOrder(struct tNode *root)
{
    node *S = (node *)malloc(sizeof(node));
    S->next = NULL;
    while(1)
    {
        while(root)
        {
            push(S,root);
            root = root->left;
        }
            if(isEmpty(S))
            break;
            root= pop(S);
            printf("%d ",root->data);
            root = root->right;
    }
}

int FindMaximum(struct tNode *root)
{
    int left,right,root_val,max = INT_MIN;
    if(root)
    {
        root_val = root->data;
        left = FindMaximum(root->left);
        right = FindMaximum(root->right);
        if(left > right)
            max = left;
        else
            max = right;
        if(max < root_val)
            max = root_val;
    }
    return max;
}

int SearchElement(struct tNode *root,int element)
{
    if(!root)
        return 0;
    else if(root->data == element)
        return 1;
    else
        return (SearchElement(root->left,element) || SearchElement(root->right,element));
    return 0;
}

 /* Constructed binary tree is
            1
          /   \
        2      3
      /  \    /  \
    4     5  6    7
  */

int main(void)
{
    struct tNode *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    root->right->left = CreateNode(6);
    root->right->right = CreateNode(7);
    inOrder(root);
    int element;
    printf("\nEnter the element to be searched : ");
    scanf("%d",&element);
    int found = SearchElement(root,element);
    if(found)
        printf("Element Found\n");
    else
        printf("Not Found\n");
    return 0;
}

