#include<stdio.h>
#include<stdlib.h>
struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}


int search(int *in,int start,int end,int data)
{
    int i;
    for(i=start;i<=end;i++)
        if(in[i] == data)
            return i;
    return -1;
}

int *ExtractKey(int *in,int *level,int M,int N)
{
    int *Array = (int *)malloc(sizeof(int)*(M));
    int i,j = 0;
    for(i=0;i<N;i++)
        if(search(in,0,M-1,level[i]) != -1)
            Array[j++] = level[i];
    return Array;
}

struct Tree *BuildTree(int *in, int *level, int start, int end,int N)
{
    if(start > end)
        return NULL;
    struct Tree *root = CreateNode(level[0]);
    if(start == end)    //Only single node
        return root;
    int index = search(in,start,end,root->data);
    int *llevel = ExtractKey(in,level,index,N);
    int *rlevel = ExtractKey(in+index+1,level,N-index-1,N);
    root->left = BuildTree(in,llevel,start,index-1,N);
    root->right = BuildTree(in,rlevel,index+1,end,N);
    return root;
}

void Inorder(struct Tree *root)
{
    if(root)
    {
        Inorder(root->left);
        printf("%d ",root->data);
        Inorder(root->right);
    }
}

int main(void)
{
    int in[] = {4,8,10,12,14,20,22};
    int level[] = {20,8,22,4,12,10,14};
    int N = sizeof(in)/sizeof(in[0]);
    struct Tree *root = BuildTree(in,level,0,N-1,N);

    printf("Inorder Traversal : ");
    Inorder(root);
    return 0;
}
