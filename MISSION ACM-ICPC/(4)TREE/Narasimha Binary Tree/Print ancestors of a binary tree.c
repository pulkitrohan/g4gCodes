
//Algorithm:
//1)Start from the root.If root is NULL then return.else push the root into the pathArray.
//2)Check whether node is a leaf.If it is a leaf then print the pathArray
    //else Recursively call for left child and right child of the node.

#include<stdio.h>
#include<stdlib.h>
struct tNode
{
    int data;
    struct tNode *left,*right;
};

struct tNode *CreateNode(int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void PrintArray(int Path[],int PathLen)
{
    int i;
    for(i=PathLen-2;i>=0;i--)
        printf("%d ",Path[i]);
    printf("\n");
}

void PrintPaths(struct tNode *root,int Path[],int PathLen,int key)
{
    if(!root)
        return ;
    Path[PathLen++] = root->data;
    if(root->data == key)
    {
        PrintArray(Path,PathLen);
        return;
    }
    else
    {
        PrintPaths(root->left,Path,PathLen,key);
        PrintPaths(root->right,Path,PathLen,key);
    }
}

int main(void)
{
    struct tNode *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    root->right->left = CreateNode(6);
    root->right->right = CreateNode(7);
    root->left->left->left = CreateNode(8);
    root->left->right->right = CreateNode(9);
    root->right->right->left = CreateNode(10);
    int Path[100],PathLen = 0,i;
    for(i=1;i<=10;i++)
    {
        printf("%d : ",i);
        PrintPaths(root,Path,PathLen,i);
    }
    return ;
}
