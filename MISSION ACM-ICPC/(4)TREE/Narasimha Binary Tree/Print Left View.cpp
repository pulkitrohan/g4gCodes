#include<cstdio>
#include<cstdlib>
#include<queue>
using namespace std;

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void PrintLeftView(struct Tree *root)
{
    queue<struct Tree *> Q;
    Q.push(root);
    Q.push(NULL);
    int flag = 1;
    struct Tree *temp;
    while(!Q.empty())
    {
        temp = Q.front();
        Q.pop();
        if(flag)
        {
            printf("%d ",temp->data);
            flag = 0;
        }
        if(!temp)
        {
            if(!Q.empty())
                Q.push(NULL);
            flag = 1;
        }
        else
        {
            if(temp->left)
                Q.push(temp->left);
            if(temp->right)
                Q.push(temp->right);
        }
    }
}

int main(void)
{
    struct Tree *root = CreateNode(12);
    root->left = CreateNode(10);
    root->right = CreateNode(30);
    root->right->left = CreateNode(25);
    root->right->right = CreateNode(40);
    PrintLeftView(root);
    return 0;
}
