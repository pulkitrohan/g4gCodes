#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right;
};

Tree *newNode(int data)
{
	Tree *temp = new Tree;
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

void createNode(int *parent,Tree *created[],int i, Tree *&root)
{
	if(created[i])
		return;
	created[i] = newNode(i);
	if(parent[i] == -1)
	{
		root = created[i];
		return;
	}
	if(created[parent[i]] == NULL)
		createNode(parent,created,parent[i],root);
	Tree *p = created[parent[i]];
	if(!p->left)
		p->left = created[i];
	else
		p->right = created[i];
}

Tree *createTree(int *parent,int N)
{
	Tree *created[N];
	for(int i=0;i<N;i++)
		created[i] = NULL;
	Tree *root = NULL;
	for(int i=0;i<N;i++)
		createNode(parent,created,i,root);
	return root;
}
void inorder(Tree *root)
{
    if (root != NULL)
    {
        inorder(root->left);
        cout << root->data << " ";
        inorder(root->right);
    }
}

int main(void)
{
	int parent[] = {-1,0,0,1,1,3,5};
	int N = sizeof(parent)/sizeof(parent[0]);
	Tree *root = createTree(parent,N);
	inorder(root);
	return 0;
}
