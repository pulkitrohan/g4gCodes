//Diameter of a tree is the no. of nodes in the longest path
//between two leaves in the tree.
//It is the maximum of the following quantities :
//1)Diameter of the left subtree
//2)Diameter of the right subtree
//3)The longest path between the leaves through the root of Tree


//Algorithm
//1.)Get the diameter of left subtree and right subtree
//2.)Get the height of left subtree and right subtree
//3.)Return the max of the three i.e (max(lheight+rheight+1,max(ldiamter,rdiameter)))

#include<stdio.h>
#include<stdlib.h>

#define max(a,b) ((a > b) ? a : b)

struct tNode
{
    int data;
    struct tNode *left,*right;
};

struct tNode *CreateNode(int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

int height(struct tNode *root)
{
    if(!root)
        return 0;
    else
        return (max(height(root->left),height(root->right)) + 1 );
}

/*
    O(n^2) algorithm
    O(n) for finding height
    and O(n) for diamter*/
int diameter(struct tNode *root)
{
    if(!root)
        return 0;
    return max((height(root->left)+height(root->right)+1),max(diameter(root->left),diameter(root->right)));
}

/*Optimization
Can be optimized by calculating height in the same recursive call
rather than calling height() separately

int diameter(struct tNode *root,int *height)
{
    if(!root)
    {
        *height = 0;
        return 0;
    }
    int lh = 0,rh = 0,ld = 0,rd = 0;    //lh => height of left subtree, rh => height of right subtree
    //ld => Diameter of left subtree ,
    ld = diameter(root->left,&lh);
    rd = diameter(root->right,&rh);
    *height = max(lh,rh)+1; //Height of current node is max of lh and rh
    return max(lh+rh+1,max(ld,rd));
}
*/
int main(void)
{
    struct tNode *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    int height = 0;
    printf("Diameter is %d\n",diameter(root,&height));
    return 0;

}
