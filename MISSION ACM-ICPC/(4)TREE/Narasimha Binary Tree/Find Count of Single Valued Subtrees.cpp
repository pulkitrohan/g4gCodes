#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right;
};

Tree *newNode(int data)
{
	Tree *temp = new Tree;
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

bool countSingle(Tree *root,int &count)
{
	if(!root)
		return true;
	bool left = countSingle(root->left,count);
	bool right = countSingle(root->right,count);
	if(!left || !right)
		return false;
	if(root->left && root->data != root->left->data)
		return false;
	if(root->right && root->data != root->right->data)
		return false;
	count++;
	return true;
}

int main(void)
{
	Tree *root = newNode(5);
	root->left = newNode(4);
	root->right = newNode(5);
	root->left->left = newNode(4);
	root->left->right = newNode(4);
	root->right->right = newNode(5);
	int count = 0;
	countSingle(root,count);
	cout<<count<<endl;
	return 0;
}
