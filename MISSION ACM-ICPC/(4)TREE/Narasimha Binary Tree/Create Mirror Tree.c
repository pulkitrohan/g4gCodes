#include<stdio.h>
#include<limits.h>
#include<stdlib.h>
struct tNode
{
    int data;
    struct tNode *left;
    struct tNode *right;
};

typedef struct Queue    //Queue
{
	struct tNode *t;
	struct Queue *next;
}Queue;

Queue *rear = NULL,*front = NULL;   //Rear and front for Queue

//Queue ADT Starts
void Enqueue(struct tNode *root)
{
    Queue *temp = (Queue *)malloc(sizeof(Queue));
    temp->t = root;
    temp->next = NULL;
    if(rear == NULL)
        rear = front = temp;
    else
    {
        rear->next = temp;
        rear = temp;
    }
}

struct tNode *Dequeue()
{
    if(isEmptyQueue())
        {
            return NULL;
        }
    else if(front == rear)
    {
        Queue *temp = front;
        front = rear = NULL;
            return temp->t;
    }
    else
    {
        Queue *temp = front;
        front = front->next;
        return temp->t;
    }
}

int isEmptyQueue()
{
    if(front == NULL)
        return 1;
    else
        return 0;
}
//Queue ADT Over


struct tNode *CreateNode(int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = NULL;
    temp->right = NULL;
}
void levelOrder(struct tNode *root)
{
	Enqueue(root);
	while(!isEmptyQueue())
	{
		root = Dequeue();
		printf("%d " ,root->data);
		if(root->left != NULL)
            Enqueue(root->left);
		if(root->right != NULL)
            Enqueue(root->right);
	}
}

void CreateMirror(struct tNode *root)
{
    if(root)
    {
        CreateMirror(root->left);
        CreateMirror(root->right);
        struct tNode *temp = root->left;
        root->left = root->right;
        root->right = temp;
    }
}

int main(void)
{
    struct tNode *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);

    printf("Level Order Traversal of Original Tree : ");
    levelOrder(root);
    CreateMirror(root);
    printf("\nLevel Order Traversal of Mirror Tree : ");
    levelOrder(root);
    return 0;
}

