#include<bits/stdc++.h>
using namespace std;
struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *newNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void Inorder(struct Tree *root)
{
    if(root)
    {
        Inorder(root->left);
        printf("%d ",root->data);
        Inorder(root->right);
    }
}

int VertexCover(struct Tree *root)
{
	if(!root)
		return 0;
	if(!root->left && !root->right)
		return 0;
	int incl = 1 + VertexCover(root->left) + VertexCover(root->right);
	
	int excl = 0;
	if(root->left)
		excl += 1 + VertexCover(root->left->left) + VertexCover(root->left->right);
	if(root->right)
		excl += 1 + VertexCover(root->right->left) + VertexCover(root->right->right);
	return min(incl,excl);
}

int main(void)
{
    struct Tree *root         = newNode(20);
    root->left                = newNode(8);
    root->left->left          = newNode(4);
    root->left->right         = newNode(12);
    root->left->right->left   = newNode(10);
    root->left->right->right  = newNode(14);
    root->right               = newNode(22);
    root->right->right        = newNode(25);
    Inorder(root);
    printf("\n");
	cout<<VertexCover(root)<<endl;
    return 0;
}
