#include<bits/stdc++.h>
using namespace std;
struct Tree
{
	int data;
	struct Tree *left,*right;
	bool isThreaded;
};

Tree *newNode(int data)
{
	Tree *temp = new Tree;
	temp->data = data;
	temp->left = temp->right = NULL;
	temp->isThreaded = false;
	return temp; 
}

void createThreaded(Tree *root)
{
	Tree *cur = root;
	while(cur)
	{
		if(!cur->left)
			cur = cur->right;
		else
		{
			Tree *prev = cur->left;
			while(prev->right && prev->right != cur)
				prev = prev->right;
			if(!prev->right)
			{
				prev->right = cur;
				prev->isThreaded = true;
				cur = cur->left;
			}
			else
				cur = cur->right;
		}
	}
}

Tree *leftMost(Tree *root)
{
	while(root && root->left)
		root = root->left;
	return root;
}

void inorder(Tree *root)
{
	if(!root)
		return;
	Tree *cur = leftMost(root);
	while(cur)
	{
		cout<<cur->data<<" ";
		if(cur->isThreaded)
			cur = cur->right;
		else
			cur = leftMost(cur->right);
	}
}


int main(void)
{
	Tree *root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);
    root->right->left = newNode(6);
    root->right->right = newNode(7);
    createThreaded(root);
    inorder(root);
    return 0;
}