#include<stdio.h>
#include<stdlib.h>

struct Tree
{
	int data;
	struct Tree *left,*right;
};

struct Tree *newNode(int data)
{
	struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

void PrintLeaves(struct Tree *root)
{
	if(root)
	{
		PrintLeaves(root->left);
		if(!root->left && !root->right)
			printf("%d ",root->data);
		PrintLeaves(root->right);
	}
}


void PrintLeftBoundary(struct Tree *root)
{
	if(root)
	{
		if(root->left)
		{
			printf("%d ",root->data);
			PrintLeftBoundary(root->left);
		}
		else if(root->right)
		{
			printf("%d ",root->data);
			PrintLeftBoundary(root->right);
		}
	}
}

void PrintRightBoundary(struct Tree *root)
{
	if(root)
	{
		if(root->right)
		{
			PrintRightBoundary(root->right);
			printf("%d ",root->data);

		}
		else if(root->left)
		{
			PrintRightBoundary(root->left);
			printf("%d ",root->data);

		}
	}
}


void printBoundary(struct Tree *root)
{
	if(root)
	{
		printf("%d ",root->data);
		PrintLeftBoundary(root->left);
		PrintLeaves(root->left);
		PrintLeaves(root->right);
		PrintRightBoundary(root->right);
	}
}


int main(void)
{
	struct Tree *root = newNode(20);
	root->left                = newNode(8);
    root->left->left          = newNode(4);
    root->left->right         = newNode(12);
    root->left->right->left   = newNode(10);
    root->left->right->right  = newNode(14);
    root->right               = newNode(22);
    root->right->right        = newNode(25);
	printBoundary(root);
	return 0;
}
