#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right;
};

Tree *newnode(int data)
{
	Tree *temp = new Tree;
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

bool isPathSumUtil(Tree *root,unordered_set<int> &S,int val)
{
	if(!root)
		return false;
	int rem = val-root->data;
	if(S.find(rem) != S.end())
		return true;
	S.insert(root->data);
	bool ans = isPathSumUtil(root->left,S,val) || isPathSumUtil(root->right,S,val);
	S.erase(root->data);
	return ans;
}

bool isPathSum(Tree *root)
{
	unordered_set<int> S;
	return isPathSumUtil(root->left,S,root->data) || isPathSumUtil(root->right,S,root->data);
}

int main(void)
{
	Tree *root = newnode(8);
    root->left    = newnode(5);
    root->right   = newnode(4);
    root->left->left = newnode(9);
    root->left->right = newnode(7);
    root->left->right->left = newnode(1);
    root->left->right->right = newnode(12);
    root->left->right->right->right = newnode(2);
    root->right->right = newnode(11);
    root->right->right->left = newnode(3);
    cout<<isPathSum(root);
    return 0;
}