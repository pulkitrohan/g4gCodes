#include<bits/stdc++.h>
using namespace std;

struct TreeNode
{
	int data;
	struct TreeNode *left,*right;
};

TreeNode *newNode(int val)
{
	TreeNode *temp = (TreeNode *)malloc(sizeof(TreeNode));
	temp->data = val;
	temp->left = temp->right = NULL;
	return temp;
}

void LeafDown(TreeNode *root,int level,int &distance)
{
	if(root)
	{
		if(!root->left && !root->right)
			distance = min(distance,level);
		else
		{
			LeafDown(root->left,level+1,distance);
			LeafDown(root->right,level+1,distance);
		}
	}
}

int FindThroughParent(TreeNode *root,TreeNode *x,int &distance)
{
	if(!root)
		return -1;
	if(root == x)
		return 0;
	
	int left = FindThroughParent(root->left,x,distance);
	if(left != -1)
	{
		LeafDown(root->right,left+2,distance);
		return left+1;
	}
	
	int right = FindThroughParent(root->right,x,distance);
	if(right != -1)
	{
		LeafDown(root->left,right+2,distance);
		return right+1;
	}
	return -1;
}


int MinimumDistance(TreeNode *root,TreeNode *x)
{
	int distance = INT_MAX;
	LeafDown(x,0,distance);
	FindThroughParent(root,x,distance);
	return distance;
}


int main(void)
{
	TreeNode *root  = newNode(1);
    root->left  = newNode(12);
    root->right = newNode(13);
 
    root->right->left   = newNode(14);
    root->right->right  = newNode(15);
 
    root->right->left->left   = newNode(21);
    root->right->left->right  = newNode(22);
    root->right->right->left  = newNode(23);
    root->right->right->right = newNode(24);
 
    root->right->left->left->left  = newNode(1);
    root->right->left->left->right = newNode(2);
    root->right->left->right->left  = newNode(3);
    root->right->left->right->right = newNode(4);
    root->right->right->left->left  = newNode(5);
    root->right->right->left->right = newNode(6);
    root->right->right->right->left = newNode(7);
    root->right->right->right->right = newNode(8);
	
	TreeNode *x = root->right;
	cout<<MinimumDistance(root,x)<<endl;
	return 0;
}