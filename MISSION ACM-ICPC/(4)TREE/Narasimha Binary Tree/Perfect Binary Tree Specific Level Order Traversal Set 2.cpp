#include<bits/stdc++.h>
using namespace std;

struct Tree
{
    int data;
    struct Tree *left,*right;
};

Tree *newNode(int data)
{
    Tree *temp = new Tree;
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void printSpecificLevelOrderUtil(Tree *root,stack<Tree *> &S)
{
    if(!root)
        return;
    queue<Tree *> Q;
    Q.push(root->left);
    Q.push(root->right);
    while(!Q.empty())
    {
        Tree *first = Q.front();Q.pop();
        Tree *second = Q.front();Q.pop();
        S.push(second->left);
        S.push(first->right);
        S.push(second->right);
        S.push(first->left);
        if(first->left->left)
        {
            Q.push(first->right);
            Q.push(second->left);
            Q.push(first->left);
            Q.push(second->right);
        }
    }
}

void printSpecificLevelOrder(Tree *root)
{
    stack<Tree *> S;
    S.push(root);
    if(root->left)
    {
        S.push(root->right);
        S.push(root->left);
    }
    if(root->left->left)
        printSpecificLevelOrderUtil(root,S);
    while(!S.empty())
    {
        cout<<S.top()->data<<" ";
        S.pop();
    }
}

int main(void)
{
    Tree* root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    printSpecificLevelOrder(root);
    return 0;
}