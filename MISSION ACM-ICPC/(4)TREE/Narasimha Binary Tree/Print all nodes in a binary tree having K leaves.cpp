#include<bits/stdc++.h>
using namespace std;
struct Tree
{
	int data;
	struct Tree *left,*right;
	int sum;
};

Tree *newNode(int data)
{
	Tree *temp = new Tree;
	temp->data = data;
	temp->sum = 0;
	temp->left = temp->right = NULL;
	return temp; 
}

int printNodes(Tree *root,int k)
{
	if(!root)
		return 0;
	if(!root->left && !root->right)
		return 1;
	int count = printNodes(root->left,k) + printNodes(root->right,k);
	if(count == k)
		cout<<root->data<<" ";
	return count;
}


int main(void)
{
	Tree *root = newNode(1);
    root->left        = newNode(2);
    root->right       = newNode(4);
    root->left->left  = newNode(5);
    root->left->right = newNode(6);
    root->left->left->left  = newNode(9);
    root->left->left->right  = newNode(10);
    root->right->right = newNode(8);
    root->right->left  = newNode(7);
    root->right->left->left  = newNode(11);
    root->right->left->right  = newNode(12);
   	printNodes(root,2);
    return 0;
}