#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

int Max(int *inorder,int instr,int inend)
{
    int i,index_max,max = 0;
    for(i=instr;i<=inend;i++)
    {
        if(inorder[i] > max)
        {
            index_max = i;
            max = inorder[i];
        }
    }
    return index_max;
}

struct Tree *BuildTree(int *inorder,int start,int end)
{
    if(start > end)
        return NULL;
    int index = Max(inorder,start,end);
    struct Tree *root = CreateNode(inorder[index]);
    if(start == end)
        return root;
    root->left = BuildTree(inorder,start,index-1);
    root->right = BuildTree(inorder,index+1,end);
    return root;
}

void Inorder(struct Tree *root)
{
    if(root)
    {
        Inorder(root->left);
        printf("%d ",root->data);
        Inorder(root->right);
    }
}

int main(void)
{
    int inorder[] = {1,5,10,40,30,15,28,20};
    int len = sizeof(inorder)/sizeof(inorder[0]);
    struct Tree *root = BuildTree(inorder,0,len - 1);
    Inorder(root);
    return 0;
}
