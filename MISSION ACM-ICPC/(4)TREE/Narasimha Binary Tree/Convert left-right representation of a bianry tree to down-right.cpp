#include<bits/stdc++.h>
using namespace std;
struct Node
{
	int data;
	struct Node *left,*right;
};

Node *newNode(int data)
{
	Node *temp = new Node;
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

void convert(Node *root)
{
	if(!root)
	return;
	queue<Node *> Q;
	Q.push(root);
	Q.push(NULL);
	Node *firstnode = root,*temp,*prev = NULL;
	while(!Q.empty())
	{
		temp = Q.front();
		Q.pop();
		if(!temp)
		{
			prev->right = NULL;
			prev = NULL;
			if(!Q.empty())
				Q.push(NULL);
		}
		else
		{
			if(temp->left)
			Q.push(temp->left);
			if(temp->right)
				Q.push(temp->right);
			if(!prev)
			{
				firstnode->left = temp;
				firstnode = temp;
			}
			else
			{
				prev->right = temp;
				temp->left = NULL;
			}
			prev = temp;
		}
	}
}

void inorder(Node *root)
{
	if(root)
	{
		inorder(root->right);
		printf("%d ",root->data);
		inorder(root->left);
	}
}

int main(void)
{
	Node *root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->right->left = newNode(4);
    root->right->right = newNode(5);
    root->right->left->left = newNode(6);
    root->right->right->left = newNode(7);
    root->right->right->right = newNode(8);
	convert(root);
	inorder(root);
	return 0;
}
