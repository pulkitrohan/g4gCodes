#include<bits/stdc++.h>
using namespace std;

struct Tree
{
    int data;
    struct Tree *left,*right;
};

Tree *newNode(int data)
{
    Tree *temp = new Tree;
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void printList(Tree *head)
{
    while(head)
    {
        cout<<head->data<<" ";
        head = head->right;
    }
}

void push(Tree *&head,Tree *node)
{
    node->right = head;
    node->left = NULL;
    if(head)
        head->left = node;
    head = node;
}

void spiralLevelOrder(Tree *root)
{
    if(!root)
        return;
    deque<Tree *> Q;
    Q.push_front(root);
    stack<Tree *> S;
    int level = 0;
    while(!Q.empty())
    {
        int count = Q.size();
        if(level & 1)
        {
             while(count > 0)
            {
                Tree *node = Q.front();
                Q.pop_front();
                S.push(node);
                if(node->left)
                    Q.push_back(node->left);
                if(node->right)
                    Q.push_back(node->right);
                count--;
            }
        }
        else
        {
            while(count > 0)
            {
                Tree *node = Q.back();
                Q.pop_back();
                S.push(node);
                if(node->right)
                    Q.push_front(node->right);
                if(node->left)
                    Q.push_front(node->left);
                count--;
            }
        }
        level++;
    }
    Tree *head = NULL;
    while(!S.empty())
    {
        push(head,S.top());
        S.pop();
    }
    printList(head);
}

int main(void)
{
    Tree *root =  newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);
    root->right->left = newNode(6);
    root->right->right = newNode(7);

    root->left->left->left  = newNode(8);
    root->left->left->right  = newNode(9);
    root->left->right->left  = newNode(10);
    root->left->right->right  = newNode(11);
    root->right->left->right  = newNode(13);
    root->right->right->left  = newNode(14);
    spiralLevelOrder(root);
    return 0;
}