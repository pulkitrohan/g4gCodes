#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right;
};

struct Tree *newNode(int data)
{
	struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

void printSingles(Tree *root) {
	if(root) {
		if(root->left && root->right) {
			printSingles(root->left);
			printSingles(root->right);
		}
		else if(root->left) {
			cout<<root->left->data<<" ";
			printSingles(root->left);
		}
		else if(root->right) {
			cout<<root->right->data<<" ";
			printSingles(root->right);
		}
	}
}

int main(void)
{
	struct Tree *root = newNode(1);
	root->left = newNode(2);
    root->right = newNode(3);
    root->left->right = newNode(4);
    root->right->left = newNode(5);
    root->right->left->left = newNode(6);
    printSingles(root);
	return 0;
}
