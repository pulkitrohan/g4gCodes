#include<stdio.h>
#include<stdlib.h>

#define max(A,B) ((A > B) ? A : B)

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
};

int Height(struct Tree *root)
{
    if(!root)
        return 0;
    int left = Height(root->left);
    int right = Height(root->right);
    return (max(left,right) + 1);
}

void PrintGivenLevel(struct Tree *root,int level)
{
    if(root)
    {
        if(level == 1)
            printf("%d ",root->data);
        else
        {
            PrintGivenLevel(root->left,level-1);
            PrintGivenLevel(root->right,level-1);
        }
    }
}

void LevelOrderTraversal(struct Tree *root)
{
    int h = Height(root),i;
    for(i=1;i<=h;i++)
        PrintGivenLevel(root,i);
}

int main(void)
{
    struct Tree *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    root->right->left = CreateNode(6);
    root->right->right = CreateNode(7);
    LevelOrderTraversal(root);
    return 0;
}
