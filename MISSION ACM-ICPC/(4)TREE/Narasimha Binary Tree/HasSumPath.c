//Algorithm:
//1)Start from the root.If root is NULL then return.else push the root into the pathArray.
//2)Check whether node is a leaf.If it is a leaf then print the pathArray
    //else Recursively call for left child and right child of the node.

#include<stdio.h>
#include<stdlib.h>
struct tNode
{
    int data;
    struct tNode *left,*right;
};

struct tNode *CreateNode(int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void PrintArray(int Path[],int PathLen)
{
    printf("Path : ");
    int i;
    for(i=0;i<PathLen;i++)
        printf("%d ",Path[i]);
    printf("\n");
}

int HasSumPaths(struct tNode *root,int sum)
{

    if(!root)
        return (sum == 0);
    else
    {
        sum -= root->data;
        return (!root->left && !root->right && !sum) || (HasSumPaths(root->left,sum) || HasSumPaths(root->right,sum));
    }
}

int main(void)
{
    struct tNode *root = CreateNode(10);
    root->left = CreateNode(8);
    root->right = CreateNode(2);
    root->left->left = CreateNode(3);
    root->left->right = CreateNode(5);
    root->right->left = CreateNode(2);
    printf("Enter the Sum to be found : ");
    int sum;
    scanf("%d",&sum);
    int HasPath = HasSumPaths(root,sum);
    if(HasPath)
        printf("There is a Path with given Sum\n");
    else
        printf("No Such Path");
    return ;
}

