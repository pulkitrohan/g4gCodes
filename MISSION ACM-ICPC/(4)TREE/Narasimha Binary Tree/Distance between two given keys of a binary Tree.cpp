#include<cstdio>
#include<cstdlib>
#include<queue>

using namespace std;

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

struct Tree *LCAUtil(struct Tree *root,int n1,int n2)
{
    if(!root)
        return NULL;
    if(root->data == n1 || root->data == n2)
        return root;
    struct Tree *left_lca = LCAUtil(root->left,n1,n2);
    struct Tree *right_lca = LCAUtil(root->right,n1,n2);
    if(left_lca && right_lca)
        return root;
    else
        return (left_lca ? left_lca : (right_lca));
}

int find(struct Tree *root,int k)
{
    if(!root)
        return 0;
    if(root->data == k || find(root->left,k) || find(root->right,k))
        return 1;
    return 0;
}

struct Tree *LCA(struct Tree *root,int n1,int n2)
{
    if(find(root,n1) && find(root,n2))
        return LCAUtil(root,n1,n2);
    else
        return NULL;
}

int DistanceFromRoot(struct Tree *root,int k)
{
    if(!root)
        return 0;
    queue<struct Tree *> Q;
    Q.push(root);
    Q.push(NULL);
    int level = 0;
    while(!Q.empty())
    {
        struct Tree *temp = Q.front();
        Q.pop();
        if(!temp)
        {
            if(!Q.empty())
                Q.push(NULL);
            level++;
        }
        else
        {
            if(temp->data == k)
            return level;
            if(temp->left)
                Q.push(temp->left);
            if(temp->right)
                Q.push(temp->right);
        }
    }
}

int Distance(struct Tree *root,int n1,int n2)
{
    return (DistanceFromRoot(root,n1) + DistanceFromRoot(root,n2) - 2 * DistanceFromRoot(root,LCA(root,n1,n2)->data));
}

int main(void)
{
    struct Tree *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    root->right->left = CreateNode(6);
    root->right->right = CreateNode(7);
    root->right->left->right = CreateNode(8);
    int n1 = 8,n2 =5;
    printf("Distance : %d",Distance(root,n1,n2));
    return 0;
}
