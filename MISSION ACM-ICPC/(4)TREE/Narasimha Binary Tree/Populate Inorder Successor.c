#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left,*right,*next;
};

struct Tree *Insert(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = temp->next = NULL;
    return temp;
}

void PopulateInorderSuccessor(struct Tree *root,struct Tree **temp)
{
    if(root)
    {
        PopulateInorderSuccessor(root->right,temp);
        root->next = *temp;
        *temp = root;
        PopulateInorderSuccessor(root->left,temp);
    }
}

int main(void)
{
    struct Tree *root = Insert(1);
    root->left = Insert(2);
    root->right = Insert(3);
    root->left->left = Insert(4);
    root->left->right = Insert(5);
    root->right->right = Insert(6);
    struct Tree *temp = NULL;
    PopulateInorderSuccessor(root,&temp);
    printf("%d",root->left->next->data);
    return 0;
}

