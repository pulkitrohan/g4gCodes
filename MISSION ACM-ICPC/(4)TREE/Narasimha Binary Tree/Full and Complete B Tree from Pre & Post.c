#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *Insert(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
};

struct Tree *BuildCompleteTree(int *pre,int *post,int &preIndex,int start,int end)
{
    struct Tree *root = NULL;
    if(end >= start)
    {
        root = Insert(pre[preIndex++]);
        if(end == start)
            return root;
        int i;
        for(i=start;i<=end;i++)
            if(pre[preIndex] == post[i])
                break;
        if(i <= end)
        {
            root->left = BuildCompleteTree(pre,post,preIndex,start,i);
            root->right = BuildCompleteTree(pre,post,preIndex,i+1,end);
        }
    }
    return root;
}

void Inorder(struct Tree *root)
{
    if(root)
    {
        Inorder(root->left);
        printf("%d ",root->data);
        Inorder(root->right);
    }
}
int main(void)
{
    int pre[] = {1,2,4,8,9,5,3,6,7};
    int post[] = {8,9,4,5,2,6,7,3,1};
    int N = sizeof(pre)/sizeof(pre[0]);
    int preIndex = 0;
    struct Tree *root = BuildCompleteTree(pre,post,preIndex,0,N-1);
    Inorder(root);
    return 0;
}
