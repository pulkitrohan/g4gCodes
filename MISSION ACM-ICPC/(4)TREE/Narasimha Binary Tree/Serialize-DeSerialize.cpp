#include<stdio.h>
#include<stdlib.h>
#define MARKER -1

struct Tree
{
	int data;
	struct Tree *left,*right;
};

struct Tree *newNode(int data)
{
	struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

void serialize(struct Tree *root,FILE *fp)
{
	if(!root)
		fprintf(fp,"%d ",MARKER);
	else
	{
		fprintf(fp,"%d ",root->data);
		serialize(root->left,fp);
		serialize(root->right,fp);
	}
}

void inorder(struct Tree *root)
{
	if(root)
	{
		inorder(root->left);
		printf("%d ",root->data);
		inorder(root->right);
	}
}

void deserialize(struct Tree *&root,FILE *fp)
{
	int val;
	if(!fscanf(fp,"%d",&val) || val == MARKER)
		return;
	root = newNode(val);
	deserialize(root->left,fp);
	deserialize(root->right,fp);
}

int main(void)
{
	struct Tree *root = newNode(20);
	root->left               = newNode(8);
    root->right              = newNode(22);
    root->left->left         = newNode(4);
    root->left->right        = newNode(12);
    root->left->right->left  = newNode(10);
    root->left->right->right = newNode(14);
    //inorder(root);
	FILE *fp = fopen("tree.txt","w");
	if(!fp)
    {
        printf("File cannot be open\n");
        return 0;
    }
	serialize(root,fp);
	fclose(fp);
	struct Tree *root1 = NULL;
	fp = fopen("tree.txt","r");
	deserialize(root1,fp);
	inorder(root1);
	return 0;
}
