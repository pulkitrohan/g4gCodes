#include<bits/stdc++.h>
using namespace std;

struct Tree
{
    char data;
    struct Tree *left,*right;
};

int priority(char ch)
{
    switch(ch)
    {
        case '(' : return 1;
        case '+' : case '-' : return 2;
        case '*' : case '/' : return 3;
        case '$' : return 4;
    }
}

struct Tree *CreateNode(char ch)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = ch;
    temp->left = temp->right = NULL;
}

struct Tree *CreateExpressionTree(char *postfix)
{
    stack<struct Tree *> S;
    int i,ch;
    for(i=0;postfix[i] != '\0';i++)
    {
        ch = postfix[i];
        if(isalnum(ch))
        {
            struct Tree *root = CreateNode(ch);
            S.push(root);
        }
        else
        {
            struct Tree *T2 = S.top();
            S.pop();
            struct Tree *T1 = S.top();
            S.pop();
            struct Tree *root = CreateNode(ch);
            root->left = T1;
            root->right = T2;
            S.push(root);
        }
    }
    return S.top();
}

void inorder(struct Tree *root)
{
    if(root)
    {
        inorder(root->left);
        printf("%c ",root->data);
        inorder(root->right);
    }
}

int evaluateExpressionTree(Tree *root)
{
    if(!root)
        return 0;
    if(!root->left && !root->right)
        return (root->data-'0');
    int left = evaluateExpressionTree(root->left);
    int right = evaluateExpressionTree(root->right);
    switch(root->data)
    {
        case '+' : return left + right;
        case '-' : return left - right;
        case '*' : return left * right;
        case '/' : return left / right;
        case '$' : return pow(left,right);
    }
}

int main(void)
{
    string infix = "5*4+(100-20)";
    stack<char> S;
    int i,k=0;
    char postfix[100],ch;
    for(i=0;i<infix.length();i++)
    {
        ch = infix[i];
        if(isalnum(ch))
            postfix[k++] = ch;
        else if(ch == '(')
            S.push(ch);
        else if(ch == ')')
        {
            while(S.top() != '(')
            {
                postfix[k++] = S.top();
                S.pop();
            }
            S.pop();
        }
        else
        {
            while(!S.empty() && priority(ch) <= priority(S.top()))
            {
                postfix[k++] = S.top();
                S.pop();
            }
                S.push(ch);
        }
    }
    while(!S.empty())
    {
        postfix[k++] = S.top();
        S.pop();
    }
    postfix[k] = '\0';
    struct Tree *root = CreateExpressionTree(postfix);
    inorder(root);
    //cout<<evaluateExpressionTree(root);
    return 0;
}
