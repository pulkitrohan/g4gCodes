#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left;
    struct Tree *right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

struct Tree *ConstructTree(int *pre,char *preLN,int N)
{
    static int index = 0;
	struct Tree *root = NULL;
	if(index <= N)
	{
		root = CreateNode(pre[index]);
		if(index == N)
			return root;
		index++;
		if(preLN[index-1] == 'N')
		{
			root->left = ConstructTree(pre,preLN,N);
			root->right = ConstructTree(pre,preLN,N);
		}
	}
    return root;
}

void inorder(struct Tree *root)
{
    if(root)
    {
        inorder(root->left);
        printf("%d ",root->data);
        inorder(root->right);
    }
}


int main(void)
{
    int pre[] = {10,30,20,5,15};
    char preLN[] = {'N','N','L','L','N'};
    int N = sizeof(pre)/sizeof(pre[0]);
    struct Tree *root = ConstructTree(pre,preLN,N-1);
    inorder(root);
    return 0;
}
