#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *Insert(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

int ConvertSumTree(struct Tree *root)
{
    int old_val;
    if(root)
    {
        old_val = root->data;
        root->data = ConvertSumTree(root->left) + ConvertSumTree(root->right);
        return old_val + root->data;
    }
}

void Inorder(struct Tree *root)
{
    if(root)
    {
        Inorder(root->left);
        printf("%d ",root->data);
        Inorder(root->right);
    }
}

int main(void)
{
    struct Tree *root = Insert(10);
    root->left = Insert(-2);
    root->right = Insert(6);
    root->left->left = Insert(8);
    root->left->right = Insert(-4);
    root->right->right = Insert(5);
    root->right->left = Insert(7);
    Inorder(root);
    ConvertSumTree(root);
    printf("\n");
    Inorder(root);
    return 0;
}


