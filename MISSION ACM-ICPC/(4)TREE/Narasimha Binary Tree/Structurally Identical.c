#include<stdio.h>
#include<limits.h>
#include<stdlib.h>
struct tNode
{
    int data;
    struct tNode *left;
    struct tNode *right;
};

struct tNode *CreateNode(int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = NULL;
    temp->right = NULL;
}

int CheckIdentical(struct tNode *root,struct tNode *root1)
{
    if(!root && !root1)
        return 1;
    else if(!root || !root1)
        return 0;
    else
        return (root->data == root1->data && CheckIdentical(root->left,root1->left) && CheckIdentical(root->right,root1->right));
}

int main(void)
{
    struct tNode *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    root->right->left = CreateNode(6);
    root->right->right = CreateNode(7);

    struct tNode *root1 = CreateNode(1);
    root1->left = CreateNode(2);
    root1->right = CreateNode(3);
    root1->left->left = CreateNode(4);
    root1->left->right = CreateNode(5);
    root1->right->left = CreateNode(6);
    root1->right->right = CreateNode(7);
    int identical = CheckIdentical(root,root1);
    if(identical)
        printf("Trees are identical\n");
    else
        printf("Trees are not identical\n");

    return 0;
}
