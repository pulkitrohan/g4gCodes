#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right;
};

Tree *newNode(int data)
{
	Tree *temp = new Tree;
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

bool isLeaf(Tree *root)
{
	return root->left && root->left->right == root &&
		   root->right && root->right->left == root;
}

int maxDepth(Tree *root)
{
	if(!root)
		return 0;
	if(isLeaf(root))
		return 1;
	return 1 + max(maxDepth(root->left),maxDepth(root->right));
}


int main(void)
{
	Tree* root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);
    root->left->left->left = newNode(6);
    Tree *L1 = root->left->left->left;
    Tree *L2 = root->left->right;
    Tree *L3 = root->right;
    L1->right = L2, L2->right = L3, L3->right = L1;
    L3->left = L2, L2->left = L1, L1->left = L3;
    cout<<maxDepth(root)<<endl;
    return 0;
}