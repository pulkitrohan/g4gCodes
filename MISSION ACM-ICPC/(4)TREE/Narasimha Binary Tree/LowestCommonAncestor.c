#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

struct Tree *LCAUtil(struct Tree *root,int n1,int n2, bool &v1, bool &v2)
{
    if(!root)
        return NULL;
    if(root->data == n1) {
        v1 = true;
        return root;
    }
    if(root->data == n2) {
        v2 = true;
        return root;
    }
    struct Tree *left_lca = LCAUtil(root->left,n1,n2, v1, v2);
    struct Tree *right_lca = LCAUtil(root->right,n1,n2, v1, v2);
    if(left_lca && right_lca)
        return root;
    else
        return (left_lca ? left_lca : (right_lca));
}

int find(struct Tree *root,int k)
{
    if(!root)
        return 0;
    if(root->data == k || find(root->left,k) || find(root->right,k))
        return 1;
    return 0;
}

struct Tree *LCA(struct Tree *root,int n1,int n2)
{
    bool v1 = false, v2 = false;
    Tree *lca = LCAUtil(root, n1, n2, v1, v2);
    if(v1 && v2 || v1 && find(root, n2) || v2 && find(root, n1))
        return lca;
    return NULL;
}

int main(void)
{
    struct Tree *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    root->right->left = CreateNode(6);
    root->right->right = CreateNode(7);
    int n1 = 4,n2 =7;
    struct Tree *temp = LCA(root,n1,n2);
    if(!temp)
        printf("Not Present\n");
    else
        printf("%d ",temp->data);
    return 0;
}
