#include<bits/stdc++.h>
using namespace std;

struct tNode
{
    int data;
    struct tNode *left,*right;
};

struct tNode *newNode(int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}



int diameter(struct tNode *root,int &res)
{
    if(!root)
    {
        res = 0;
        return 0;
    }
	int lsum = 0,rsum = 0,lres = 0,rres = 0;
	lsum = diameter(root->left,lres);
	rsum = diameter(root->right,rres);
	res = max(lres,rres) + root->data;
	return max(lsum,max(rsum,lres+rres+root->data));
	
}

int main(void)
{
    struct tNode *root = newNode(-15);
    root->left = newNode(5);
    root->right = newNode(6);
    root->left->left = newNode(-8);
    root->left->right = newNode(1);
    root->left->left->left = newNode(2);
    root->left->left->right = newNode(6);
    root->right->left = newNode(3);
    root->right->right = newNode(9);
    root->right->right->right= newNode(0);
    root->right->right->right->left= newNode(4);
    root->right->right->right->right= newNode(-1);
    root->right->right->right->right->left= newNode(10);
    int sum = INT_MIN;
    cout<<diameter(root,sum);
    return 0;

}
