#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right;
};

struct Tree *newNode(int data)
{
	Tree *temp = new Tree;
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

int maxPathSum(Tree *root,int &res)
{
	if(!root)
		return 0;
	if(!root->left && !root->right)
		return root->data;
	int left = maxPathSum(root->left,res);
	int right = maxPathSum(root->right,res);
	if(root->left && root->right)
	{
		res = max(res,left+right+ root->data);
		return max(left,right) + root->data;
	}
	return (!root->left) ? right + root->data:
						   left + root->data;
}

int main(void)
{
	Tree *root = newNode(-15);
    root->left = newNode(5);
    root->right = newNode(6);
    root->left->left = newNode(-8);
    root->left->right = newNode(1);
    root->left->left->left = newNode(2);
    root->left->left->right = newNode(6);
    root->right->left = newNode(3);
    root->right->right = newNode(9);
    root->right->right->right= newNode(0);
    root->right->right->right->left= newNode(4);
    root->right->right->right->right= newNode(-1);
    root->right->right->right->right->left= newNode(10);
    int ans = INT_MIN;
    maxPathSum(root,ans);
    cout<<ans<<endl;
    return 0;
}