#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right;
};

Tree *newNode(int data)
{
	Tree *temp = new Tree;
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

bool treeContinuous(Tree *root)
{
	if(!root)
		return true;
	if(!root->left && !root->right)
		return true;
	if(!root->left)
		return (abs(root->right->data-root->data) == 1) && treeContinuous(root->right);
	if(!root->right)
		return (abs(root->left->data-root->data) == 1) && treeContinuous(root->left);
	return (abs(root->right->data-root->data) == 1) && treeContinuous(root->right) && 
		   (abs(root->left->data-root->data) == 1) && treeContinuous(root->left);
}
int main(void)
{
	Tree *root = newNode(3);
    root->left        = newNode(2);
    root->right       = newNode(4);
    root->left->left  = newNode(1);
    root->left->right = newNode(3);
    root->right->right = newNode(5);
    cout<<treeContinuous(root);
}