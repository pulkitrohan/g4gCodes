#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

struct Tree *ExtractLeaves(struct Tree *root,struct Tree *&head)
{
    if(root)
    {
        root->right = ExtractLeaves(root->right,head);
        if(!root->left && !root->right)
        {
            root->right = head;
            if(head)
                head->left = root;
            head = root;
            return NULL;
        }
        root->left = ExtractLeaves(root->left,head);
    }
    return root;
}

void PrintDLL(struct Tree *head)
{
    printf("Doubly Linked List : ");
    while(head)
    {
        printf("%d ",head->data);
        head = head->right;
    }
}

void Inorder(struct Tree *root)
{
    if(root)
    {
        Inorder(root->left);
        printf("%d ",root->data);
        Inorder(root->right);
    }
}

int main(void)
{
    struct Tree *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    root->right->right = CreateNode(6);
    root->left->left->left = CreateNode(7);
    root->left->left->right = CreateNode(8);
    root->right->right->left = CreateNode(9);
    root->right->right->right = CreateNode(10);
    struct Tree *head = NULL;
    root = ExtractLeaves(root,head);
    Inorder(root);
    printf("\n");
    PrintDLL(head);
    return 0;
}

