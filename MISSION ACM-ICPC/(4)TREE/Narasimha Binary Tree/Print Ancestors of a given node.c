#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
        struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
        temp->data = data;
        temp->left = temp->right = NULL;
        return temp;
}

int PrintAncestors(struct Tree *root,int data)
{
	if(root)       //if the root is null then return 0
    {
        if(root->data == data)   //if the root's data is equal to data then we have found the node.Now print ancestors
            return 1;
        if(PrintAncestors(root->left,data) || PrintAncestors(root->right,data)) //Checking for left and right child of each node
        {
            printf("%d ",root->data);    //  Print Ancestor
            return 1;   //To come out of the particular recursive call
        }
    }
	//return 0;
}

int main(void)
{
    struct Tree *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    root->left->left->left = CreateNode(7);
    int data;
    printf("Enter the node whose ancestors is to be found : ");
    scanf("%d",&data);
    PrintAncestors(root,data);
    return 0;
}
