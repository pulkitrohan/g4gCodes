#include<bits/stdc++.h>
using namespace std;
struct Tree
{
	char data;
	struct Tree *left,*right;
};

void Inorder(struct Tree *root)
{
	if(root)
	{
		Inorder(root->left);
		printf("%c ",root->data);
		Inorder(root->right);
	}
}

struct Tree *newNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
};


void storeAlternate(struct Tree *root,char *A,int *index,int level)
{
	if(root)
	{
		storeAlternate(root->left,A,index,level+1);
		if(level % 2)
			A[(*index)++] = root->data;
		storeAlternate(root->right,A,index,level+1);
	}
}

void ModifyTree(struct Tree *root,char *A,int *index,int level)
{
	if(root)
	{
		ModifyTree(root->left,A,index,level+1);
		if(level % 2)
			root->data = A[(*index)++];
		ModifyTree(root->right,A,index,level+1);
	}
}

void preorder(Tree *A,Tree *B,int level)
{
	if(A && B)
	{
		if(level % 2 == 0)
			swap(A->data,B->data);
		preorder(A->left,B->right,level+1);
		preorder(A->right,B->left,level+1);
	}
}

void Reverse(char *A,int N)
{
	int low = 0,high = N-1;
	while(high > low)
	{
		char temp = A[low];
		A[low] = A[high];
		A[high] = temp;
		high--,low++;
	}
}

void ReverseAlternate(struct Tree *root)
{
	char A[100];
	int index = 0;
	storeAlternate(root,A,&index,0);
	Reverse(A,index);
	index = 0;
	ModifyTree(root,A,&index,0);
}

int main(void)
{
	struct Tree *root = newNode('a');
	root->left = newNode('b');
    root->right = newNode('c');
    root->left->left = newNode('d');
    root->left->right = newNode('e');
    root->right->left = newNode('f');
    root->right->right = newNode('g');
    root->left->left->left = newNode('h');
    root->left->left->right = newNode('i');
    root->left->right->left = newNode('j');
    root->left->right->right = newNode('k');
    root->right->left->left = newNode('l');
    root->right->left->right = newNode('m');
    root->right->right->left = newNode('n');
    root->right->right->right = newNode('o');
	Inorder(root);
	//ReverseAlternate(root);
	preorder(root->left,root->right,0);
	printf("\n");
	Inorder(root);
	return 0;
}
