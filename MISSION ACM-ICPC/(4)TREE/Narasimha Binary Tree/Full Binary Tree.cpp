#include<bits/stdc++.h>
using namespace std;
struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *newNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void Inorder(struct Tree *root)
{
    if(root)
    {
        Inorder(root->left);
        printf("%d ",root->data);
        Inorder(root->right);
    }
}

bool isFull(struct Tree *root)
{
	if(!root)
		return true;
	return (!root->left && !root->right) || (root->left && root->right && isFull(root->left) && isFull(root->right));
}

int main(void)
{
    struct Tree *root = newNode(10);
    root->left = newNode(20);
    root->right = newNode(30);
    root->left->right = newNode(40);
    root->left->left = newNode(50);
    Inorder(root);
    printf("\n");
	cout<<isFull(root)<<endl;
    return 0;
}
