#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right;
};

Tree *newNode(int data)
{
	Tree *temp = new Tree;
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

void inorder(Tree *root)
{
	if(root)
	{
		inorder(root->left);
		cout<<root->data<<" ";
		inorder(root->right);
	}
}

int getLevel(Tree *root,Tree *node,int level)
{
	if(!root)
		return 0;
	if(root == node)
		return level;
	int left = getLevel(root->left,node,level+1);
	if(left != 0)
		return left;
	return getLevel(root->right,node,level+1);
}

void printGivenLevel(Tree *root,Tree *node,int level)
{
	if(root && level >= 2)
	{
		if(level == 2)
		{
			if(root->left == node || root->right == node)
				return;
			if(root->left)
				cout<<root->left->data<<" ";
			if(root->right)
				cout<<root->right->data<<" ";
		}
		else
		{
			printGivenLevel(root->left,node,level-1);
			printGivenLevel(root->right,node,level-1);
		}
	}
}

void printCousin(Tree *root,Tree *node)
{
	int level = getLevel(root,node,1);
	printGivenLevel(root,node,level);
}

int main(void)
{
	Tree *root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);
    root->left->right->right = newNode(15);
    root->right->left = newNode(6);
    root->right->right = newNode(7);
    root->right->left->right = newNode(8);
 	printCousin(root,root->left->right);
    
    return 0;
}