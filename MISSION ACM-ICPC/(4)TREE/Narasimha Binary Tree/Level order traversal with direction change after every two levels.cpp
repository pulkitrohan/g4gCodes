#include<bits/stdc++.h>
using namespace std;

struct Node {
	int data;
	Node *left, *right;
};

Node* newNode(int data) 
{ 
    Node* temp = new Node; 
    temp->data = data; 
    temp->left = temp->right = NULL; 
    return temp; 
} 

void modifiedLevelOrder(Node *root) {
	if(!root)
		return;
	if(!root->left && !root->right) {
		cout<<root->data;
		return;
	}

	queue<Node *> Q;
	stack<Node *> S;

	Node *temp = NULL;
	int level = 0;
	bool rightToLeft = false;

	Q.push(root);

	while(!Q.empty()) {
		level++;
		int size = Q.size();
		for(int i=0;i<size;i++) {
			temp = Q.front();
			Q.pop();
			if(rightToLeft == false)
				cout<<temp->data<<" ";
			else
				S.push(temp);

			if(temp->left)
				Q.push(temp->left);
			if(temp->right)
				Q.push(temp->right);
		}
		if(rightToLeft) {
			while(!S.empty()) {
				temp = S.top();
				S.pop();
				cout<<temp->data<<" ";
			}
		}

		if(level == 2) {
			rightToLeft = !rightToLeft;
			level = 0;
		}
		cout<<endl;
	}
}

int main(void) {
	Node* root = newNode(1); 
    root->left = newNode(2); 
    root->right = newNode(3); 
    root->left->left = newNode(4); 
    root->left->right = newNode(5); 
    root->right->left = newNode(6); 
    root->right->right = newNode(7); 
    root->left->left->left = newNode(8); 
    root->left->left->right = newNode(9); 
    root->left->right->left = newNode(3); 
    root->left->right->right = newNode(1); 
    root->right->left->left = newNode(4); 
    root->right->left->right = newNode(2); 
    root->right->right->left = newNode(7); 
    root->right->right->right = newNode(2); 
    root->left->right->left->left = newNode(16); 
    root->left->right->left->right = newNode(17); 
    root->right->left->right->left = newNode(18); 
    root->right->right->left->right = newNode(19); 
  
    modifiedLevelOrder(root); 
    return 0;
}