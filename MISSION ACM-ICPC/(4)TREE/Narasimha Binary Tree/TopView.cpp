#include<cstdio>
#include<cstdlib>
#include<map>
#include<iostream>
using namespace std;
struct tNode
{
    int data;
    struct tNode *left;
    struct tNode *right;
};

struct tNode *CreateNode(int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = NULL;
    temp->right = NULL;
	return temp;
}

void TopView(struct tNode *root,int col,map<int,int> &M)
{
    if(root)
    {
        TopView(root->left,col-1,M);
        TopView(root->right,col+1,M);
        M[col] = root->data;

    }
}

int main(void)
{
    struct tNode *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->right = CreateNode(4);
    root->left->right->right = CreateNode(5);
    root->left->right->right->right = CreateNode(6);
    map<int,int> M;
    TopView(root,0,M);
    map<int,int> :: iterator it;
    for(it = M.begin() ; it != M.end();it++)
        cout<<it->second<<" ";
    return 0;
}
