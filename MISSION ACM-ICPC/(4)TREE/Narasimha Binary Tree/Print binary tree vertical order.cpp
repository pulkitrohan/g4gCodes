#include<cstdio>
#include<map>
#include<vector>
#include<cstdlib>
using namespace std;


struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}


void GetVerticalOrder(struct Tree *root,int HD,map<int,vector<int> > &M)
{
    if(!root)
        return;
    M[HD].push_back(root->data);
    GetVerticalOrder(root->left,HD-1,M);
    GetVerticalOrder(root->right,HD+1,M);
}

void PrintVerticalOrder(struct Tree *root)
{
    map<int,vector<int> > M;
    GetVerticalOrder(root,0,M);
    map<int,vector<int> > :: iterator it;
    int j;
    for(it = M.begin(); it != M.end();it++)
    {
        for(j = 0;j<it->second.size();j++)
            printf("%d ",it->second[j]);
        printf("\n");
    }
}

int main(void)
{
    struct Tree *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    root->right->left = CreateNode(6);
    root->right->right = CreateNode(7);
    root->right->left->right = CreateNode(8);
    root->right->right->right = CreateNode(9);
    PrintVerticalOrder(root);
    return 0;

}
