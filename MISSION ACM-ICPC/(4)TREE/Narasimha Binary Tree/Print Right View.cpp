#include<cstdio>
#include<cstdlib>
#include<queue>
using namespace std;

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void PrintLeftView(struct Tree *root)
{
    queue<struct Tree *> Q;
    Q.push(root);
    Q.push(NULL);
   // int flag = 1;
    //printf("%d",root->data);
    struct Tree *temp;
    while(!Q.empty())
    {
        temp = Q.front();
        Q.pop();
        if(!Q.empty() && !Q.front())
            printf("%d ",temp->data);
        if(!temp)
        {
            if(!Q.empty())
                Q.push(NULL);
        }
        else
        {
            if(temp->left)
                Q.push(temp->left);
            if(temp->right)
                Q.push(temp->right);
        }
    }
}

int main(void)
{
    struct Tree *root = CreateNode(1);
    root->left = CreateNode(2);
    root->right = CreateNode(3);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(5);
    root->right->right = CreateNode(7);
    root->right->right->right = CreateNode(8);
    PrintLeftView(root);
    return 0;
}

