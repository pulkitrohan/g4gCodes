#include<cstdio>
#include<cstdlib>
#include<queue>
using namespace std;
struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

int NextRightNode(struct Tree *root,int item)
{
    queue<struct Tree *> Q;
    Q.push(root);
    Q.push(NULL);
    int flag = 0;
    while(!Q.empty())
    {
        struct Tree *temp = Q.front();
        Q.pop();
        if(temp->data == item)
        {
        	if(Q.front())
        		return Q.front()->data;
        	else
        		return NULL;
        }
        if(!temp)
        {
            if(!Q.empty())
                Q.push(NULL);
        }
        else
        {
            if(temp->left)
                Q.push(temp->left);
            if(temp->right)
                Q.push(temp->right);
        }
    }
   // return 1;
}

int main(void)
{
    struct Tree *root = CreateNode(10);
    root->left = CreateNode(2);
    root->left->right = CreateNode(8);
    root->left->left = CreateNode(4);
    root->right = CreateNode(6);
    root->right->right = CreateNode(5);
    if(!NextRightNode(root,2))
        printf("No Right Node\n");
    else
        printf("Right node data : %d\n",NextRightNode(root,2));
    return 0;
}
