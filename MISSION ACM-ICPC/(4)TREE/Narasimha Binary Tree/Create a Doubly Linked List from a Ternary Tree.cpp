#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*middle,*right;
};

Tree *newNode(int data)
{
	Tree *temp = new Tree;
	temp->data = data;
	temp->left = temp->middle = temp->right = NULL;
	return temp;
}

void push(Tree *&tail,Tree *root)
{
	if(!tail)
	{
		tail = root;
		root->left = root->middle = root->right = NULL;
	}
	else
	{
		tail->right = root;
		root->left = tail;
		root->middle = root->right = NULL;
		tail = root;
	}
}

Tree *TernaryTreeToList(Tree *root,Tree *&head)
{
	if(!root)
		return NULL;
	static Tree *tail = NULL;
	Tree *left = root->left;
	Tree *middle = root->middle;
	Tree *right = root->right;
	if(!head)
		head = root;
	push(tail,root);
	TernaryTreeToList(left,head);
	TernaryTreeToList(middle,head);
	TernaryTreeToList(right,head);
}

void printList(Tree *head)
{
	while(head)
	{
		cout<<head->data<<" ";
		head = head->right;
	}
}


int main(void)
{
	Tree* root = newNode(30);
 
    root->left = newNode(5);
    root->middle = newNode(11);
    root->right = newNode(63);
 
    root->left->left = newNode(1);
    root->left->middle = newNode(4);
    root->left->right = newNode(8);
 
    root->middle->left = newNode(6);
    root->middle->middle = newNode(7);
    root->middle->right = newNode(15);
 
    root->right->left = newNode(31);
    root->right->middle = newNode(55);
    root->right->right = newNode(65);
 
    Tree* head = NULL;

    TernaryTreeToList(root,head);
 
    printList(head);
    return 0;
}