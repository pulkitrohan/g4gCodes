#include<iostream>
#include<cstdio>
#include<stack>
#include<cstdlib>
struct tNode
{
    int data;
    struct tNode *left,*right;
};

using namespace std;
struct tNode *CreateNode(int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = temp->right = NULL;
}

void PrintSpiral(struct tNode *root)
{
    if(!root)
        return;
    stack<struct tNode *> s1;
    stack<struct tNode *> s2;
    s1.push(root);
    while(!s1.empty() || !s2.empty())
    {
        while(!s1.empty())
        {
            struct tNode *temp = s1.top();
            s1.pop();
            printf("%d ",temp->data);
            if(temp->right)
                s2.push(temp->right);
            if(temp->left)
                s2.push(temp->left);
        }
        while(!s2.empty())
        {
            struct tNode *temp = s2.top();
            s2.pop();
            printf("%d ",temp->data);
            if(temp->left)
                s1.push(temp->left);
            if(temp->right)
                s1.push(temp->right);
        }
    }
}

int main(void)
{
   struct tNode *root = CreateNode(1);
   root->left = CreateNode(2);
   root->right = CreateNode(3);
   root->left->left = CreateNode(4);
   root->left->right = CreateNode(5);
   root->right->left = CreateNode(6);
   root->right->right = CreateNode(7);
   PrintSpiral(root);
   return 0;
}
