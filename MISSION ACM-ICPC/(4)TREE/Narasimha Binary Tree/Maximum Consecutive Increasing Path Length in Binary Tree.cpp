#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right;
};

Tree *newNode(int data)
{
	Tree *temp = new Tree;
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

void longestConsecutive(Tree *root,int curLength,int val,int &ans)
{
	if(root)
	{
		if(root->data == val)
			curLength++;
		else
			curLength = 1;
		ans = max(ans,curLength);
		longestConsecutive(root->left,curLength,root->data+1,ans);
		longestConsecutive(root->right,curLength,root->data+1,ans);
	}
}


int main(void)
{
	Tree* root = newNode(6);
    root->right = newNode(9);
    root->right->left = newNode(7);
    root->right->right = newNode(10);
    root->right->right->right = newNode(11);
    int ans = 0;
    longestConsecutive(root,0,root->data,ans);
    cout<<ans<<endl;
    return 0;
}