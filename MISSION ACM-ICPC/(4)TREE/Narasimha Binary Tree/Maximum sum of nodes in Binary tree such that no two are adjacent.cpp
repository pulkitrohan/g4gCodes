#include<bits/stdc++.h>
using namespace std;
struct Tree
{
	int data;
	struct Tree *left,*right;
	int sum;
};

Tree *newNode(int data)
{
	Tree *temp = new Tree;
	temp->data = data;
	temp->sum = 0;
	temp->left = temp->right = NULL;
	return temp; 
}

int maxSum(Tree *root);
int sumGrandChildren(Tree *root)
{
	int sum = 0;
	if(root->left)
		sum += maxSum(root->left->left) + maxSum(root->left->right);
	if(root->right)
		sum += maxSum(root->right->left) + maxSum(root->right->right);
	return sum;
}

int maxSum(Tree *root)
{
	if(!root)
		return 0;
	if(root->sum)
		return root->sum;
	int incl = root->data + sumGrandChildren(root);
	int excl = maxSum(root->left) + maxSum(root->right);
	root->sum = max(incl,excl);
	return root->sum;
}

int main(void)
{
	Tree* root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->right->left = newNode(4);
    root->right->right = newNode(5);
    root->left->left = newNode(1);
   	cout<<maxSum(root);

     return 0;
}