#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right;
};

Tree *newNode(int data)
{
	Tree *temp = new Tree;
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

void preOrder(Tree* node)
{
    if (node == NULL) return;
    printf("%d ", node->data);
    preOrder(node->left);
    preOrder(node->right);
}

int search(int A[],int start,int end,int val)
{
	for(int i=start;i<=end;i++)
		if(A[i] == val)
			return i;
}

Tree *buildTree(int in[],int post[],int start,int end,int &index)
{
	if(start > end)
		return NULL;
	Tree *root = newNode(post[index--]);
	if(start == end)
		return root;
	int indexInorder = search(in,start,end,root->data);
	root->left = buildTree(in,post,start,indexInorder-1,index);
	root->right = buildTree(in,post,indexInorder+1,end,index);
	return root;
}

int main(void)
{
	int in[] = {4,8,2,5,1,6,3,7};
	int post[] = {8,4,5,2,6,7,3,1};
	int N = sizeof(in)/sizeof(in[0]);
	int index = N-1;
	Tree *root = buildTree(in,post,0,N-1,index);
	preOrder(root);
    return 0;
}