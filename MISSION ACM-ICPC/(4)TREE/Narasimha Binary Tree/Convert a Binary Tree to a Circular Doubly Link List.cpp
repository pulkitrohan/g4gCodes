#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right;
};

Tree *newNode(int data)
{
	Tree *temp = new Tree;
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

void treeToCDLLUtil(Tree *root,Tree *&head,Tree *&tail)
{
	if(root)
	{
		treeToCDLLUtil(root->left,head,tail);
		root->left = tail;
		if(!tail)
			head = root;
		else
			tail->right = root;
		tail = root;
		treeToCDLLUtil(root->right,head,tail);
	}
}

void treeToCDLLUtil2(Tree *root, Tree *&head, Tree *&tail) {
	if(root) {
		treeToCDLLUtil2(root->right, head, tail);
		root->right = head;
		if(!tail)
			tail = root;
		else
			head->left = root;
		head = root;
		treeToCDLLUtil2(root->left, head, tail);
	}
}


Tree *treeToCDLL(Tree *root)
{
	Tree *head = NULL,*tail = NULL;
	treeToCDLLUtil2(root,head,tail);
	tail->right = head;
	head->left = tail;
	return head;
}

void printList(Tree *head)
{
	if(!head)
		return;
	cout<<head->data<<" ";
	Tree *cur = head;
	while(head->right != cur)
	{
		head = head->right;
		cout<<head->data<<" ";
	}
}

int main(void)
{
	Tree *root = newNode(10);
    root->left = newNode(12);
    root->right = newNode(15);
    root->left->left = newNode(25);
    root->left->right = newNode(30);
    root->right->left = newNode(36);
    Tree *head = treeToCDLL(root);
    printList(head);
    return 0;
}