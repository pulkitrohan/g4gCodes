#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void BTreeToDLLInorder(struct Tree *root,struct Tree *&head)
{
    if(root)
    {
        BTreeToDLLInorder(root->right,head);
        root->right = head;
        if(head)
            (head)->left = root;
        head = root;
        BTreeToDLLInorder(root->left,head);
    }
}

void BTreeToDLLPreorder(struct Tree *root,struct Tree *&head)
{
    if(root)
    {
        BTreeToDLLPreorder(root->right,head);
        BTreeToDLLPreorder(root->left,head);
        root->right = head;
        if(head)
            head->left = root;
        head = root;
    }
}

void BTreeToDLLPostorder(struct Tree *root,struct Tree *&head,struct Tree *&first)
{
    if(root)
    {
        BTreeToDLLPostorder(root->left,head,first);
        BTreeToDLLPostorder(root->right,head,first);
        root->right = head;
        if(head)
            head->left = root;
        head = root;
        if(!first)
        	first = head;
    }
}


void PrintDLL(struct Tree *head)
{
    while(head)
    {
        printf("%d ",head->data);
        head = head->left;
    }
}
void Reverse(struct Tree **head)
{
    struct Tree *temp = NULL,*cur = *head;
    while(cur)
    {
        temp = cur->left;
        cur->left = cur->right;
        cur->right = temp;
        cur = cur->left;
    }
    if(temp)
        *head = temp->left;
}

int main(void)
{
    struct Tree *root = CreateNode(10);
    root->left = CreateNode(12);
    root->right = CreateNode(15);
    root->left->left = CreateNode(25);
    root->left->right = CreateNode(30);
    root->right->left = CreateNode(36);
    struct Tree *head = NULL;
    struct Tree *first = NULL;
    BTreeToDLLPostorder(root,head,first);
    head->left = NULL;
    PrintDLL(first);
    return 0;
}
