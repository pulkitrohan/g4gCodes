#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp  = malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void DoubleTree(struct Tree *root)
{
    if(root)
    {
        DoubleTree(root->left);
        DoubleTree(root->right);
        struct Tree *old = root->left;   //Duplicate this node to its left
        root->left = CreateNode(root->data);
        root->left->left = old;
    }
}

void inorder(struct Tree *root)
{
    if(root)
    {
        inorder(root->left);
        printf("%d ",root->data);
        inorder(root->right);
    }
}

int main(void)
{
    struct Tree *root = CreateNode(2);
    root->left = CreateNode(1);
    root->right = CreateNode(3);
    inorder(root);
    DoubleTree(root);
    printf("\n");
    inorder(root);
    return 0;
}
