#include<stdio.h>
#include<limits.h>
int IsIdenticalBST(int *A,int *B,int N,int index1,int index2,int min,int max)
{
    int j,k;
    for(j=index1;j<N;j++)
        if(A[j] > min && A[j] < max)
            break;
    for(k=index2;k<N;k++)
        if(B[k] > min && B[k] < max)
            break;
    if(j == N && k == N)
        return 1;
    if((j == N)^(k == N) || A[j] != B[k])
        return 0;
    return IsIdenticalBST(A,B,N,j+1,k+1,min,A[j]) && IsIdenticalBST(A,B,N,j+1,k+1,A[j],max);
}

int main(void)
{
    int A[] = {8,3,6,1,4,7,10,14,13};
    int B[] = {8,10,14,3,6,4,1,7,13};
    int N = sizeof(A)/sizeof(A[0]);
    if(IsIdenticalBST(A,B,N,0,0,INT_MIN,INT_MAX))
        printf("Yes\n");
    else
        printf("No\n");
    return 0;
}
