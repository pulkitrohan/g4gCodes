#include<stdio.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

struct Tree *CreateTree(int *A,int start,int end)
{
    struct Tree *root = NULL;
    if(end >= start)
    {
        int mid = start + (end-start)/2;
        root = CreateNode(A[mid]);
        root->left = CreateTree(A,start,mid-1);
        root->right = CreateTree(A,mid+1,end);
    }
    return root;
}

void inorder(struct Tree *root)
{
    if(root)
    {
        inorder(root->left);
        printf("%d ",root->data);
        inorder(root->right);
    }
}
int main(void)
{
    int array[] = {1,2,3,4};
    int len = sizeof(array)/sizeof(array[0]);
    struct Tree *root = CreateTree(array,0,len-1);
    inorder(root);
    return 0;
}
