#include<cstdio>
#include<cstdlib>
#include<stack>

using namespace std;

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *Insert(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void Inorder(struct Tree *root)
{
    if(root)
    {
        Inorder(root->left);
        printf("%d ",root->data);
        Inorder(root->right);
    }
}

void MergeBST(struct Tree *root1,struct Tree *root2)
{
    stack<struct Tree *> S1;
    stack<struct Tree *> S2;
    struct Tree *cur1 = root1,*cur2 = root2;
    if(!root1)
    {
        Inorder(root2);
        return;
    }
    if(!root2)
    {
        Inorder(root1);
        return ;
    }
    while(cur1 || !S1.empty() || cur2 || !S2.empty())
    {
        if(cur1 || cur2)
        {
            while(cur1)
            {
                S1.push(cur1);
                cur1 = cur1->left;
            }
            while(cur2)
            {
                S2.push(cur2);
                cur2 = cur2->left;
            }
        }
        else
        {
            if(S1.empty())
            {
                while(!S2.empty())
                {
                    cur2 = S2.top();
                    S2.pop();
                    cur2->left = NULL;  //Left Portion Already visited
                    Inorder(cur2);
                }
                return ;
            }
            if(S2.empty())
            {
                while(!S1.empty())
                {
                    cur1 = S1.top();
                    S1.pop();
                    cur1->left = NULL;     //Left Portion Already Vistied
                    Inorder(cur1);
                }
                return ;
            }
            cur1 = S1.top();
            S1.pop();
            cur2 = S2.top();
            S2.pop();
            if(cur1->data < cur2->data)
            {
                printf("%d ",cur1->data);
                cur1 = cur1->right;
                S2.push(cur2);
                cur2 = NULL;
            }
            else
            {
                printf("%d ",cur2->data);
                cur2 = cur2->right;
                S1.push(cur1);
                cur1 = NULL;
            }
        }
    }
}

int main(void)
{
    struct Tree *root1 = Insert(3);
    root1->left = Insert(1);
    root1->right = Insert(5);

    struct Tree *root2 = Insert(4);
    root2->left = Insert(2);
    root2->right = Insert(6);

    MergeBST(root1,root2);
    return 0;
}
