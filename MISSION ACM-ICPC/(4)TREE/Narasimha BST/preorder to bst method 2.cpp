#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	int data;
	struct Tree *left,*right;
};

struct Tree *createNode(int data)
{
	struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
};

struct Tree *constructTree(int pre[],int N)
{
	if(N <= 0)
		return NULL;
	stack<struct Tree *> S;
	struct Tree *root = createNode(pre[0]);
	S.push(root);
	for(int i=1;i<N;i++)
	{
		struct Tree *temp = NULL;
		while(!S.empty() && pre[i] > S.top()->data)
		{
			temp = S.top();
			S.pop();
		}
		if(temp)
		{
			temp->right = createNode(pre[i]);
			S.push(temp->right);
		}
		else
		{
			S.top()->left = createNode(pre[i]);
			S.push(S.top()->left);
		}
	}
	return root;
}
		
void inorder(struct Tree *root)
{
	if(root)
	{
		cout<<root->data<<" ";
		inorder(root->left);
		inorder(root->right);
	}
}

int main(void)
{
	int pre[] = {10,5,1,7,40,50};
	int N = sizeof(pre)/sizeof(pre[0]);
	struct Tree *root = constructTree(pre,N);
	inorder(root);
	return 0;
}