struct Tree *FindElement(struct Tree *root,int data)
{
    if(!root)
	return NULL;
    if(root->data > data)
	return FindElement(root->left,data);
    else if(root->data < data)
	return FindElement(root->right,data);
    else
	return root;
}


Non Recursive

struct Tree *FindElement(struct Tree *root,int data)
{
    if(!root)
	return NULL;
    else
    {
	while(root)
	{
		if(root->data == data)
			return root;
		else if(root->data > data)
			root = root->left;
		else
			root = root->right;
	}
    }
    return NULL;
}