#include<stdio.h>
#include<stdlib.h>
struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *Insert(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
};

struct Tree *Max(struct Tree *root)
{
    if(!root)
        return NULL;
    while(root->right)
        root = root->right;
    return root;
};

struct Tree *Min(struct Tree *root)
{
    if(!root)
        return NULL;
    while(root->left)
        root = root->left;
    return root;
};

void FindPreSuc(struct Tree *root,struct Tree **pre,struct Tree **suc,int key)
{
    if(!root)
        return;
    while(root)
    {
        if(root->data == key)
        {
            *pre = Max(root->left);
            *suc = Min(root->right);
			break;
        }
        else if(root->data > key)
        {
            *suc = root;
            root = root->left;
        }
        else
        {
            *pre = root;
            root = root->right;
        }
    }
}

int main(void)
{
    struct Tree *root = Insert(50);
    root->left = Insert(30);
    root->right = Insert(70);
    root->left->left = Insert(20);
    root->left->right = Insert(40);
    root->right->left = Insert(60);
    root->right->right = Insert(80);
    struct Tree *pre = NULL,*suc = NULL;
    FindPreSuc(root,&pre,&suc,65);
    if(pre)
        printf("Predecessor : %d\n",pre->data);
    else
        printf("No Predecessor\n");
    if(suc)
        printf("Successor : %d\n",suc->data);
    else
        printf("No Predecessor\n");
    return 0;
}
