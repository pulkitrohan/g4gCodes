#include<bits/stdc++.h>
using namespace std;

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *Insert(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

struct Tree *CreateBST(int *pre,int &index,int N,int key,int min,int max)
{
    if(index >= N)
        return NULL;
    struct Tree *root = NULL;
    if(key > min && key < max)
    {
        root = Insert(key);
        index++;
		if(index < N)
		{
			root->left = CreateBST(pre,index,N,pre[index],min,key);
			root->right = CreateBST(pre,index,N,pre[index],key,max);
		}
    }
    return root;
}
void Inorder(struct Tree *root)
{
    if(root)
    {
        Inorder(root->left);
        cout<<root->data<<" ";
        Inorder(root->right);
    }
}

int main(void)
{
    int pre[] = {10,5,1,7,40,50};
    int N = sizeof(pre)/sizeof(pre[0]);
    int index = 0;
    struct Tree *root = CreateBST(pre,index,N,pre[0],INT_MIN,INT_MAX);
    Inorder(root);
    return 0;
}
