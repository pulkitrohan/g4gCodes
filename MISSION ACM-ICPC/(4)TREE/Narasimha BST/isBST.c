#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
}
/*
int IsBST(struct Tree *root,int min,int max)    //Method1
{
    if(!root)
        return 1;
    return (root->data > min && root->data < max && IsBST(root->left,min,root->data) && IsBST(root->right,root->data,max));
}
*/
int IsBST(struct Tree *root,struct Tree *prev)
{
    if(root)
    {
        IsBST(root->left,prev);
        if(prev && root->data < prev->data)
            return 0;
        prev = root;
        IsBST(root->right,prev);
    }
    return 1;
}


int main(void)
{
    struct Tree *root = CreateNode(4);
    root->left = CreateNode(2);
    root->right = CreateNode(5);
    root->left->left = CreateNode(10);
    root->left->right = CreateNode(4);
    if(IsBST(root,NULL))
        printf("Yes Binary Tree is BST\n");
    else
        printf("No Binary Tree is not BST\n");
    return 0;
}
