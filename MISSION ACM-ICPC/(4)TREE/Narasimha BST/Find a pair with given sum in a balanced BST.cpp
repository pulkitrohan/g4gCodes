#include<cstdio>
#include<cstdlib>
#include<stack>
using namespace std;
struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

int FindPair(struct Tree *root,int sum)
{
    stack<struct Tree *> S1;
    stack<struct Tree *> S2;
    int done1 = 0,done2 = 0;
    struct Tree *cur1 = root,*cur2 = root;
    int val1 = 0,val2 = 0;
    while(1)
    {
        if(!done1)
        {
            while(cur1)
            {
                S1.push(cur1);
                cur1 = cur1->left;
            }

            if(S1.empty())
                done1 = 1;
            else
             {
                cur1 = S1.top();
                S1.pop();
                val1 = cur1->data;
                cur1= cur1->right;
                done1 = 1;
             }
        }

        if(!done2)
        {
            while(cur2)
            {
                S2.push(cur2);
                cur2 = cur2->right;
            }

            if(S2.empty())
                done2 = 1;
            else
            {
                cur2 = S2.top();
                S2.pop();
                val2 = cur2->data;
                cur2 = cur2->left;
                done2 = 1;
            }
        }
        if((val1 != val2) && (val1 + val2 == sum))
        {
            printf("Pair with values %d and %d is found\n",val1,val2);
            return 1;
        }
        else if(val1 + val2 > sum)
            done2 = 0;
        else if(val1 + val2 < sum)
            done1 = 0;
        if(val1 >= val2)
            return 0;
    }
}

int main(void)
{
    struct Tree *root = CreateNode(15);
    root->left = CreateNode(10);
    root->right = CreateNode(20);
    root->left->left = CreateNode(8);
    root->left->right = CreateNode(12);
    root->right->left = CreateNode(16);
    root->right->right = CreateNode(25);
    int sum = 33;
    if(!FindPair(root,sum))
        printf("No,there is no such Pair\n");
    return 0;
}
