#include<stdio.h>

int HasOneChild(int *A,int N)
{
    int i;
    for(i=0;i<N-1;i++)
    {
        if((A[i]- A[i+1]) * (A[i] - A[N-1]) < 0)
            return 0;
    }
    return 1;
}

int main(void)
{
    int pre[] = {8,3,5,7,6};
    int N = sizeof(pre)/sizeof(pre[0]);
    if(HasOneChild(pre,N))
        printf("Yes\n");
    else
        printf("No\n");
    return 0;
}
