#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
}

int LCA(struct Tree *root,int n1,int n2)
{
    while(root)
    {
        if(root->data > n1 && root->data > n2)
            root = root->left;
        else if(root->data < n1 && root->data < n2)
            root = root->right;
        else
            break;
    }
    return root->data;
}
int main(void)
{
    struct Tree *root = CreateNode(20);
    root->left = CreateNode(8);
    root->right = CreateNode(22);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(12);
    root->left->right->left = CreateNode(10);
    root->left->right->right = CreateNode(14);
    int n1,n2;
    printf("Enter node values whose LCA is to be found : ");
    scanf("%d %d",&n1,&n2);
    printf("\n%d ",LCA(root,n1,n2));
    return 0;
}
