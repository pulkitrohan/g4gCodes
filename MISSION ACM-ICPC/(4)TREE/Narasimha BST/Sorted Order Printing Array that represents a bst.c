#include<stdio.h>

void PrintSorted(int *Tree,int start,int end)
{
    if(end >= start)
    {
    PrintSorted(Tree,start*2+1,end);
    printf("%d ",Tree[start]);
    PrintSorted(Tree,start*2+2,end);
    }
}

int main(void)
{
    int Tree[] = {4,2,5,1,3};
    int len = sizeof(Tree)/sizeof(Tree[0]);
    PrintSorted(Tree,0,len-1);
    return 0;
}
