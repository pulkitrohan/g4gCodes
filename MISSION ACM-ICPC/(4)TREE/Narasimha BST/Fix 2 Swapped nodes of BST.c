#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *newNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void CorrectBST(struct Tree *root,struct Tree **first,struct Tree **middle,struct Tree **last,struct Tree **prev)
{
    if(root)
    {
        CorrectBST(root->left,first,middle,last,prev);
        if(*prev && root->data < (*prev)->data)
        {
            if(!*first)
            {
                *first = *prev;
                *middle = root;
            }
            else
                *last = root;
        }
        *prev = root;
        CorrectBST(root->right,first,middle,last,prev);
    }
}

void Inorder(struct Tree *root)
{
    if(root)
    {
        Inorder(root->left);
        printf("%d ",root->data);
        Inorder(root->right);
    }
}

void swap(int *A,int *B)
{
    int temp = *A;
    *A = *B;
    *B = temp;
}

int main(void)
{
    struct Tree *root = newNode(6);
    root->left        = newNode(10);
    root->right       = newNode(2);
    root->left->left  = newNode(1);
    root->left->right = newNode(3);
    root->right->right = newNode(12);
    root->right->left = newNode(7);
    struct Tree *first,*middle,*last,*prev;
    first = middle = last = prev = NULL;
    Inorder(root);
    printf("\n");
    CorrectBST(root,&first,&middle,&last,&prev);
    if(first && last)
        swap(&(first->data),&(last->data));
    else if(first && middle)
        swap(&(first->data),&(middle->data));
    Inorder(root);
    return 0;
}
