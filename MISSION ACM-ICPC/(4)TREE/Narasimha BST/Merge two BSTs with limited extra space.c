#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *Insert(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

int main(void)
{
    struct Tree *root1 = Insert(3);
    root1->left = Insert(1);
    root1->right = Insert(5);

    struct Tree *root2 = Insert(4);
    root2->left = Insert(2);
    root2->right = Insert(6);

    MergeBST(root1,root2);
    return 0;
}
