#include<bits/stdc++.h>
using namespace std;

struct node
{
	int data;
	node *left, *right;

	node(int data) {
		this->data = data;
		left = right = NULL;
	}
};

bool existsPair(node *root1, node *root2, int x) {
	stack<node *> it1, it2;

	node *root = root1;
	while(root) {
		it1.push(root);
		root = root->left;
	}
	root = root2;
	while(root) {
		it2.push(root);
		root = root->right;
	}

	while(it1.size() && it2.size()) {

		int v1 = it1.top()->data;
		int v2 = it2.top()->data;

		if(v1 + v2 == x)
			return true;

		//Forward iterator
		if(v1 + v2 < x) {
			root = it1.top()->right;
			it1.pop();
			while(root) {
				it1.push(root);
				root = root->left;
			}
		}
		//Backward iterator
		else {
			root = it2.top()->left;
			it2.pop();
			while(root) {
				it2.push(root);
				root = root->right;
			}
		}
	}
	return false;
}


int main(void) {
	// First BST 
    node* root1 = new node(11); 
    root1->right = new node(15); 
  
    // Second BST 
    node* root2 = new node(5); 
    root2->left = new node(3); 
    root2->right = new node(7); 
    root2->left->left = new node(2); 
    root2->left->right = new node(4); 
    root2->right->left = new node(6); 
    root2->right->right = new node(8); 
  
    int x = 23; 
  
    if (existsPair(root1, root2, x)) 
        cout << "Yes"; 
    else
        cout << "No"; 

	return 0;
}