#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
};


struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void Inorder(struct Tree *root,int *A,int *index)
{
    if(root)
    {
        Inorder(root->left,A,index);
        A[(*index)++] = root->data;
        Inorder(root->right,A,index);
    }
}

int *Merge(int *A,int *B,int size1,int size2)
{
    int i = 0,j = 0,k = 0;
    int *mergedArray = (int *)malloc(sizeof(int) * (size1+size2));
    while(i < size1 && j < size2)
    {
        if(A[i] < B[j])
            mergedArray[k++] = A[i++];
        else
            mergedArray[k++] = B[j++];
    }
    while(i < size1)
        mergedArray[k++] = A[i++];
    while(j < size2)
        mergedArray[k++] = B[j++];
    return mergedArray;
}

struct Tree *CreateTree(int *A,int start,int end)
{
    if(start > end)
        return NULL;
    int mid = (start + end)/2;
    struct Tree *root = CreateNode(A[mid]);
    root->left = CreateTree(A,start,mid-1);
    root->right = CreateTree(A,mid+1,end);
    return root;
}

struct Tree *MergedTrees(struct Tree *root1,struct Tree *root2,int size1,int size2)
{
    int arr1[size1+1],arr2[size2+1];
    int i = 0;
    Inorder(root1,arr1,&i);
    i = 0;
    Inorder(root2,arr2,&i);

    int *MergedArray = Merge(arr1,arr2,size1,size2);
    return CreateTree(MergedArray,0,size1+size2-1);
}

void printinorder(struct Tree *root)
{
    if(root)
    {
        printinorder(root->left);
        printf("%d ",root->data);
        printinorder(root->right);
    }
}

int main(void)
{
    struct Tree *root1 = CreateNode(100);
    root1->left = CreateNode(50);
    root1->right = CreateNode(300);
    root1->left->left = CreateNode(20);
    root1->left->right = CreateNode(70);

    struct Tree *root2 = CreateNode(80);
    root2->left = CreateNode(40);
    root2->right = CreateNode(120);

    struct Tree *root = MergedTrees(root1,root2,5,3);
    printinorder(root);
    return 0;
}
