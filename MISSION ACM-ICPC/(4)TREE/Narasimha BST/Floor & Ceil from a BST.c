#include<bits/stdc++.h>
using namespace std;

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(struct Tree *root,int data)
{
    if(!root)
    {
        root = (struct Tree *)malloc(sizeof(struct Tree));
        root->data = data;
        root->left = root->right = NULL;
    }
    else if(root->data > data)
        root->left = CreateNode(root->left,data);
    else if(root->data < data)
        root->right = CreateNode(root->right,data);
    return root;
}

void Inorder(struct Tree *root)
{
    if(root)
    {
        Inorder(root->left);
        printf("%d ",root->data);
        Inorder(root->right);
    }
}

int Ceil(struct Tree *root,int key)
{
    if(!root)
        return -1;
	int suc = -1;
	while(root)
	{
		if(root->data == key)
			return root->data;
		else if(root->data > key)
		{
			suc = root->data;
			root = root->left;
		}
		else
			root = root->right;
	}
    return suc;
}

int Floor(struct Tree *root,int key)
{
    if(!root)
        return -1;
    int pre = -1;
	while(root)
	{
		if(root->data == key)
			return root->data;
		else if(root->data < key)
		{
			pre = root->data;
			root = root->right;
		}
		else
			root = root->left;
	}
    return pre;
}

int main(void)
{
    int A[] = {8,4,12,2,6,10,14};
    int N = sizeof(A)/sizeof(A[0]),i;
    struct Tree *root = NULL;
    for(i=0;i<N;i++)
        root = CreateNode(root,A[i]);
    Inorder(root);
    printf("\n");
    for(i=0;i<16;i++)
    {
        printf("Ceil of %d is %d\n",i,Ceil(root,i));
        printf("Floor of %d is %d\n",i,Floor(root,i));
    }
    return 0;
}
