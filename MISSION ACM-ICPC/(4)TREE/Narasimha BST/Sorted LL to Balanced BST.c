#include<stdio.h>
#include<stdlib.h>

struct LL
{
    int data;
    struct LL *next;
};

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct LL *CreateLLNode(struct LL *head,int data)
{
    struct LL *temp = (struct LL *)malloc(sizeof(struct LL));
    temp->data = data;
    temp->next = NULL;
    if(!head)
        head = temp;
    else
    {
        temp->next = head;
        head = temp;
    }
    return head;
}

struct Tree *CreateTreeNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

int Count(struct LL *head)
{
    int count = 0;
    while(head)
    {
        head = head->next;
        count++;
    }
    return count;
}

struct Tree *SortedLLToBST(struct LL *&head,int N)
{
    if(N <= 0)
        return NULL;
    struct Tree *left = SortedLLToBST(head,N/2);
    struct Tree *root = CreateTreeNode(head->data);

    root->left = left;
    head = head->next;
    root->right = SortedLLToBST(head,N-N/2-1);
    return root;
}

void Inorder(struct Tree *root)
{
    if(root)
    {
        Inorder(root->left);
        printf("%d ",root->data);
        Inorder(root->right);
    }
}

int main(void)
{
    struct LL *head = NULL;
    head = CreateLLNode(head,7);
    head = CreateLLNode(head,6);
    head = CreateLLNode(head,5);
    head = CreateLLNode(head,4);
    head = CreateLLNode(head,3);
    head = CreateLLNode(head,2);
    head = CreateLLNode(head,1);

    struct Tree *root = SortedLLToBST(&head,Count(head));
    Inorder(root);
}

