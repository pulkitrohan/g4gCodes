
#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(struct Tree *root,int data)
{
    if(!root)
    {
        root = (struct Tree *)malloc(sizeof(struct Tree));
        root->data = data;
        root->left = root->right = NULL;
    }
    else if(root->data > data)
        root->left = CreateNode(root->left,data);
    else
        root->right = CreateNode(root->right,data);
    return root;
}

struct Tree *RemoveOutofRangeKeys(struct Tree *root,int min,int max)
{
    if(root)
    {
        root->left = RemoveOutofRangeKeys(root->left,min,max);
        root->right = RemoveOutofRangeKeys(root->right,min,max);
        if(root->data < min)
        {
            struct Tree *temp = root->right;
            free(root);
            return temp;
        }
        else if(root->data > max)
        {
            struct Tree *temp = root->left;
            free(root);
            return temp;
        }
    }
    return root;
}

void Inorder(struct Tree *root)
{
    if(root)
    {
        Inorder(root->left);
        printf("%d ",root->data);
        Inorder(root->right);
    }
}

int main(void)
{
    struct Tree *root = NULL;
    root = CreateNode(root,6);
    root = CreateNode(root,-13);
    root = CreateNode(root,14);
    root = CreateNode(root,-8);
    root = CreateNode(root,15);
    root = CreateNode(root,13);
    root = CreateNode(root,7);
    Inorder(root);
    printf("\n");
    root = RemoveOutofRangeKeys(root,-10,13);
    Inorder(root);
    return 0;
}
