#include<bits/stdc++.h>
using namespace std;

struct node
{
	int data;
	node *left, *right;

	node(int data) {
		this->data = data;
		left = right = NULL;
	}
};

class bstIterator {

private:
	stack<node *> S;

public:

	bstIterator(node *root) {
		node *cur = root;
		while(cur) {
			S.push(cur);
			cur = cur->left;
		}
	}

	node *curr() {
		return S.top();
	}

	void next() {
		node *cur = S.top()->right;
		S.pop();
		while(cur) {
			S.push(cur);
			cur = cur->left;
		}
	}

	bool isEnd() {
		return !S.size();
	}

};

void iterate(bstIterator it) {
	while(!it.isEnd()) {
		cout<<it.curr()->data<<" ";
		it->next();
	}
}

int main(void) {
	node *root = new node(5);
	root->left = new node(3); 
    root->right = new node(7); 
    root->left->left = new node(2); 
    root->left->right = new node(4); 
    root->right->left = new node(6); 
    root->right->right = new node(8); 

    bstIterator it(root);
    iterate(it);

	return 0;
}