#include<stdio.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
}
//Recursive
void ConvertBST(struct Tree *root,int *sum)
{
    if(!root)
        return;
    ConvertBST(root->right,sum);
    *sum += root->data;
    root->data = *sum;
    ConvertBST(root->left,sum);
}

//NonRecursive
//check cpp version
void ConvertBSTNonRecursive(struct Tree *root)
{
    int sum = 0;
    stack<struct Tree *> S;
    while(1)
    {
        while(root->right)
        {
            S.push(root->right);
            root = root->right;
        }
        if(S.empty())
            break;
        else
        {
            root = S.top();
            sum += root->data;
            root->data = sum;
            S.pop();
            root = root->left;
        }
    }
}

void inorder(struct Tree *root)
{
    if(root)
    {
        inorder(root->left);
        printf("%d ",root->data);
        inorder(root->right);
    }
}

int main(void)
{
    struct Tree *root = CreateNode(11);
    root->left = CreateNode(2);
    root->right = CreateNode(29);
    root->left->left = CreateNode(1);
    root->left->right = CreateNode(7);
    root->right->left = CreateNode(15);
    root->right->right = CreateNode(40);
    root->right->right->left = CreateNode(35);
    int sum = 0;
    ConvertBST(root,&sum);
    inorder(root);
    return 0;
}
