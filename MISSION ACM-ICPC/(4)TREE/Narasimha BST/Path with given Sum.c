
//Algorithm:
//1)Start from the root.If root is NULL then return.else push the root into the pathArray.
//2)Check whether node is a leaf.If it is a leaf then print the pathArray
    //else Recursively call for left child and right child of the node.

#include<stdio.h>
#include<stdlib.h>
struct tNode
{
    int data;
    struct tNode *left,*right;
};

struct tNode *CreateNode(int data)
{
    struct tNode *temp = (struct tNode *)malloc(sizeof(struct tNode));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

void PrintArray(int Path[],int PathLen)
{
    int i;
    for(i=0;i<PathLen;i++)
        printf("%d ",Path[i]);
    printf("\n");
}

int HasSumPath(struct tNode *root,int Sum)
{
    int ans = 0;
    if(root)
    {
        ans = 0;
        int SubSum = Sum - root->data;
        if(SubSum == 0 && !root->left && !root->right)
            return 1;
        if(root->left)
            ans = ans || HasSumPath(root->left,SubSum);
        if(root->right)
            ans = ans || HasSumPath(root->right,SubSum);
    }
    return ans;
}

int main(void)
{
    struct tNode *root = CreateNode(10);
    root->left = CreateNode(8);
    root->right = CreateNode(2);
    root->left->left = CreateNode(3);
    root->left->right = CreateNode(5);
    root->right->left = CreateNode(2);
    if(HasSumPath(root,21))
        printf("Yes\n");
    else
        printf("No\n");
    return ;
}
