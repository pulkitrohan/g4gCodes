#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
}

struct Tree *FindMin(struct Tree *root)
{
    if(!root)
        return 0;
    while(root->left)
        root = root->left;
    return root;
}

struct Tree *InorderSuccessor(struct Tree *root,struct Tree *temp)
{
    if(!root)
        return NULL;
    if(temp->right)
        return FindMin(temp->right);
    struct Tree *parent = NULL;
    while(root != temp)
    {
        if(temp->data < root->data)
        {
            parent = root;
            root = root->left;
        }
        else if(temp->data > root->data)
            root = root->right;
    }
    return parent;
}

struct node *InorderPredessor(struct node *root,struct node *t)
{	
	if(!root)
		return NULL;
	if(t->left)
		return FindMax(t->left);
	struct node *parent = NULL;
	while(root != t)
	{
		if(t->data > root->data)
		{
			parent = root;
			root = root->right;
		}
		else if(t->data < root->data)
			root = root->left;
	}
	return parent;
}

/*
struct Tree *InorderSuccessorParent(struct Tree *root,struct Tree *temp)
{
    if(!root)
        return NULL;
    if(temp->right)
        return Min(temp->right);
    struct Tree *p = temp->parent;
    while(p && p->right == temp)
    {
        temp = p;
        p = p->parent;
    }
	return p;
};*/

int main(void)
{
    struct Tree *root = CreateNode(20);
    root->left = CreateNode(8);
    root->right = CreateNode(22);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(12);
    root->left->right->left = CreateNode(10);
    root->left->right->right = CreateNode(14);
    struct Tree *temp = root->left->right->right;
    struct Tree *succ = InorderSuccessor(root,temp);
    if(succ)
        printf("InOrder Successor of %d is %d\n",temp->data,succ->data);
    else
        printf("Successor not found\n");
    return 0;
}
