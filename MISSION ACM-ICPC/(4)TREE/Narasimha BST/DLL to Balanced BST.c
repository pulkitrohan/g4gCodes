#include<stdio.h>
#include<stdlib.h>

struct Node
{
    int data;
    struct Node *next,*prev;
};

struct Node *Insert(struct Node *head,int data)
{
    if(!head)
    {
        head = (struct Node *)malloc(sizeof(struct Node));
        head->data = data;
        head->next = head->prev = NULL;
    }
    else
    {
        struct Node *temp = (struct Node *)malloc(sizeof(struct Node));
        temp->data = data;
        temp->prev = NULL;
        temp->next = head;
        head = temp;
    }
    return head;
}

int Count(struct Node *head)
{
    int count = 0;
    while(head)
    {
        count++;
        head = head->next;
    }
    return count;
}

void PrintList(struct Node *head)
{
    while(head)
    {
        printf("%d ",head->data);
        head = head->next;
    }
}

struct Node *DLLToBalancedBST(struct Node **head,int N)
{
    if(N <= 0)
        return NULL;
    struct Node *left = DLLToBalancedBST(head,N/2);
    struct Node *root = *head;
    root->prev = left;
    *head = (*head)->next;
    root->next = DLLToBalancedBST(head,N-N/2-1);
    return root;
}

void Inorder(struct Node *head)
{
    if(head)
    {
        Inorder(head->prev);
        printf("%d ",head->data);
        Inorder(head->next);
    }
}

int main(void)
{
    struct Node *head = NULL;
    head = Insert(head,7);
    head = Insert(head,6);
    head = Insert(head,5);
    head = Insert(head,4);
    head = Insert(head,3);
    head = Insert(head,2);
    head = Insert(head,1);
    PrintList(head);
    int count = Count(head);
    struct Node *root = DLLToBalancedBST(&head,count);
    printf("\n");
    Inorder(root);
    return 0;
}
