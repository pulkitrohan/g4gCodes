#include<bits/stdc++.h>
using namespace std;

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct info
{
    bool isBST;
    int size,min,max;
};


struct Tree *newnode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

struct info *InfoNode()
{
    struct info *temp = (struct info *)malloc(sizeof(struct info));
    temp->isBST = true;
    temp->size = 0;
    temp->min = INT_MAX;
    temp->max = INT_MIN;
    return temp;
}

struct info *LargestBSTUtil(struct Tree *root)
{
    if(!root)
        return InfoNode();
    struct info *left = LargestBSTUtil(root->left);
    struct info *right = LargestBSTUtil(root->right);

    struct info *node = InfoNode();
    if(!left->isBST || !right->isBST || left->max >= root->data || right->min <= root->data)
    {
        node->isBST = false;
        node->size = max(left->size,right->size);
    }
    else
    {
        node->isBST = true;
        node->size = left->size + right->size + 1;
        node->min = root->left != NULL ? left->min : root->data;
        node->max = root->right != NULL ? right->max : root->data;
    }
    return node;

}

int LargestBST(struct Tree *root)
{
    struct info *largest = LargestBSTUtil(root);
    return largest->size;
}


int main(void)
{
    struct Tree *root=newnode(50);
    root->left=newnode(30);
    root->right=newnode(60);
    root->left->left=newnode(5);
    root->left->right=newnode(20);
    root->right->left=newnode(45);
    root->right->right=newnode(70);
    root->right->right->left=newnode(65);
    root->right->right->right=newnode(80);

    printf("Size of largest BST is %d\n",LargestBST(root));
    return 0;
}
