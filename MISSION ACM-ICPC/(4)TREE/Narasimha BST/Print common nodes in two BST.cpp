#include<bits/stdc++.h>
using namespace std;

struct Node
{
	int data;
	struct Node *left,*right;
};

Node *insert(Node *root,int data)
{
	if(!root)
	{
		root = (Node *)malloc(sizeof(Node));
		root->data = data;
		root->left = root->right = NULL;
	}
	else if(data < root->data)
		root->left = insert(root->left,data);
	else if(data > root->data)
		root->right = insert(root->right,data);
	return root;
}

void commonNodes(Node *A,Node *B)
{
	stack<Node *> S1,S2;
	while(1)
	{
		if(A)
		{
			S1.push(A);
			A = A->left;
		}
		else if(B)
		{
			S2.push(B);
			B = B->left;
		}
		else if(!S1.empty() && !S2.empty())
		{
			A = S1.top();
			B = S2.top();
			if(A->data == B->data)
			{
				cout<<A->data<<" ";
				S1.pop();
				S2.pop();
				A = A->right;
				B = B->right;
			}
			else if(A->data < B->data)
			{
				S1.pop();
				A = A->right;
				B = NULL;
			}
			else if(B->data < A->data)
			{
				S2.pop();
				B = B->right;
				A = NULL;
			}
		}
		else
			break;
	}
}


int main(void)
{
	Node *root1 = NULL;
    root1 = insert(root1, 5);
    root1 = insert(root1, 1);
    root1 = insert(root1, 10);
    root1 = insert(root1,  0);
    root1 = insert(root1,  4);
    root1 = insert(root1,  7);
    root1 = insert(root1,  9);
    Node *root2 = NULL;
    root2 = insert(root2, 10);
    root2 = insert(root2, 7);
    root2 = insert(root2, 20);
    root2 = insert(root2, 4);
    root2 = insert(root2, 9);
	commonNodes(root1,root2);
	return 0;
}