#include<stdio.h>
#include<stdlib.h>

struct DLL
{
    int data;
    struct DLL *next,*prev;
};

void InsertAtBeg(struct DLL *head,int data)
{
    struct DLL *temp = (struct DLL *)malloc(sizeof(struct DLL));
    temp->data = data;
    temp->prev = NULL;
    temp->next = head->next;
    if(head->next)
        head->next->prev = temp;
    head->next = temp;
}

void PrintList(struct DLL *head)
{
    while(head->next)
    {
        head = head->next;
        printf("%d ",head->data);
    }
}

int Count(struct DLL *head)
{
    int count = 0;
    while(head->next)
    {
        count++;
        head = head->next;
    }
    return count;
}

struct DLL *SortedDLLToBST(struct DLL *head,int count)
{
    if(count <= 0)
        return NULL;
    struct DLL *left = SortedDLLToBST(head,count/2);
    struct DLL *root = head->next;
    root->prev = left;
    head = head->next;
    root->next = SortedDLLToBST(head,count/2 - 1);
    return root;
}

void Inorder(struct DLL *root)
{
    if(root)
    {
        Inorder(root->prev);
        printf("%d ",root->data);
        Inorder(root->next);
    }
}
int main(void)
{
    struct DLL *head = (struct DLL *)malloc(sizeof(struct DLL));
    head->next = head->prev = NULL;
    int i;
    for(i=7;i>=1;i--)
    InsertAtBeg(head,i);
   // PrintList(head);
    int count = Count(head);
    struct DLL *root = SortedDLLToBST(head->next,count);
    printf("\n");
    Inorder(root);
    return 0;
}
