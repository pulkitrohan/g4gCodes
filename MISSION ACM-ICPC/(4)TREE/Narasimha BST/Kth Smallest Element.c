#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
}

int Inorder(struct Tree *root,int key)
{
    static int i = 0;
    if(root)
    {
        Inorder(root->left,key);
        i++;
        if(i == key)
            printf("%d\n",root->data);
        Inorder(root->right,key);
    }
}

int main(void)
{
    struct Tree *root = CreateNode(20);
    root->left = CreateNode(8);
    root->right = CreateNode(22);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(12);
    root->left->right->left = CreateNode(10);
    root->left->right->right = CreateNode(14);
        Inorder(root,8);
    return 0;
}
