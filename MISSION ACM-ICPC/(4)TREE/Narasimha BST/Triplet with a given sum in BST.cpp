#include<bits/stdc++.h>
using namespace std;

struct node
{
	int data;
	node *left, *right;

	node(int data) {
		this->data = data;
		left = right = NULL;
	}
};

bool existsPair(node *root1, int x) {
	stack<node *> it1, it2;

	node *root = root1;
	while(root) {
		it1.push(root);
		root = root->left;
	}
	root = root1;
	while(root) {
		it2.push(root);
		root = root->right;
	}

	while(it1.size() && it2.size()) {

		int v1 = it1.top()->data;
		int v2 = it2.top()->data;

		if(v1 + v2 == x)
			return true;

		//Forward iterator
		if(v1 + v2 < x) {
			root = it1.top()->right;
			it1.pop();
			while(root) {
				it1.push(root);
				root = root->left;
			}
		}
		//Backward iterator
		else {
			root = it2.top()->left;
			it2.pop();
			while(root) {
				it2.push(root);
				root = root->right;
			}
		}
	}
	return false;
}

bool existsTriplet(node *root, node *cur, int x) {
	if(!cur)
		return 0;
	return 
		existsPair(root, x - cur->data) ||
		existsTriplet(root, cur->left, x) ||
		existsTriplet(root, cur->right, x);
}


int main(void) {
    node* root = new node(5); 
    root->left = new node(3); 
    root->right = new node(7); 
    root->left->left = new node(2); 
    root->left->right = new node(4); 
    root->right->left = new node(6); 
    root->right->right = new node(8); 
  
    int x = 24; 
  
    if (existsTriplet(root, root, x)) 
        cout << "Yes"; 
    else
        cout << "No"; 

	return 0;
}