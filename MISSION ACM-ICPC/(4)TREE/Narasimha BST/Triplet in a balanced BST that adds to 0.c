#include<stdio.h>
#include<stdlib.h>

struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *Insert(struct Tree *root,int data)
{
    if(!root)
    {
        root = (struct Tree *)malloc(sizeof(struct Tree));
        root->data = data;
        root->left = root->right = NULL;
    }
    else
    {
        if(root->data > data)
        root->left = Insert(root->left,data);
        else if(root->data < data)
        root->right = Insert(root->right,data);
    }
    return root;
}

void Inorder(struct Tree *root)
{
    if(root)
    {
        Inorder(root->left);
        printf("%d ",root->data);
        Inorder(root->right);
    }
}

void ConvertBSTtoDLL(struct Tree *root,struct Tree **head,struct Tree **tail)
{
    if(root)
    {
        ConvertBSTtoDLL(root->left,head,tail);
        root->left = *tail;
        if(*tail)
            (*tail)->right = root;
        else
            *head = root;
        *tail = root;
        ConvertBSTtoDLL(root->right,head,tail);
    }
}

int IsPresentInDLL(struct Tree *head,struct Tree *tail,int sum)
{
    while(head != tail)
    {
        int curr = head->data + tail->data;
        if(curr == sum)
            return 1;
        else if(curr > sum)
            tail = tail->left;
        else
            head = head->right;
    }
    return 0;
}

int IsTripletPresent(struct Tree *root)
{
    if(!root)
        return 0;
    struct Tree *head,*tail;
    head = tail = NULL;
    ConvertBSTtoDLL(root,&head,&tail);
    while(head->right != tail && (head->data < 0))
    {
        if(IsPresentInDLL(head->right,tail,-1*head->data))
            return 1;
        else
            head = head->right;
    }
    return 0;
}

int main(void)
{
    int A[] = {6,-13,14,-8,13,15,7};
    int N = sizeof(A)/sizeof(A[0]),i;
    struct Tree *root = NULL;
    for(i=0;i<N;i++)
        root = Insert(root,A[i]);
    if(IsTripletPresent(root))
        printf("Yes\n");
    else
        printf("No\n");
    return 0;
}
