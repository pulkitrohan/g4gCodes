#include<stdio.h>
#include<stdlib.h>

struct tNode
{
   int data;
   struct tNode* left;
   struct tNode* right;
};

struct tNode* newtNode(int data);
void inOrder(struct tNode *root);
void preOrder(struct tNode *root);
void postOrder(struct tNode *root);
struct tNode *insert(struct tNode *root,int data);
struct tNode *delete(struct tNode *root,int data);
struct tNode *findmin(struct tNode *root)
{
	if(!root)
		return NULL;
	else
	{
		while(root->left)
		root = root->left;
	}
	return root;
}
int main()
{
    int ch,data;

  struct tNode *root = NULL;
  start:
  printf("1.Insertion\n");
  printf("2.Deletion\n");
  printf("3.InOrder Traversal\n");
  printf("4.PreOrder Traversal\n");
  printf("5.PostOrder Traversal\n");
  printf("6.Exit\n");
  printf("Which Operation do you want to execute : ");
  scanf("%d",&ch);
  switch(ch)
  {
      case 1: printf("Enter an element : ");
              scanf("%d",&data);
              root = insert(root,data);
              break;
      case 2: printf("Enter the element to be deleted : ");
              scanf("%d",&data);
              root = delete(root,data);
              break;
      case 3: printf("\nInOrder Traversal : ");
              inOrder(root);
              break;
      case 4: printf("\nPreOrder Traversal : ");
              preOrder(root);
              break;
      case 5: printf("\nPostOrder Traversal : ");
              postOrder(root);
              break;
      case 6: exit(0);
  }
  printf("\n");
  goto start;
  return 0;
}

struct tNode* newtNode(int data)
{
  struct tNode* tNode = (struct tNode*)
                       malloc(sizeof(struct tNode));
  tNode->data = data;
  tNode->left = NULL;
  tNode->right = NULL;

  return(tNode);
}

void inOrder(struct tNode *root)
{
  if(root)
  {
      inOrder(root->left);
      printf("%d ",root->data);
      inOrder(root->right);
  }
}
void preOrder(struct tNode *root)
{
  if(root)
  {
      printf("%d ",root->data);
      preOrder(root->left);
      preOrder(root->right);
  }
}
void postOrder(struct tNode *root)
{
  if(root)
  {
      postOrder(root->left);
      postOrder(root->right);
      printf("%d ",root->data);
  }
}
//Recursive Code for Insertion
struct tNode *insert(struct tNode *root,int data)
{
    if(!root)
    {
        root = (struct tNode*)malloc(sizeof(struct tNode));
            root->data = data;
        root->left = root->right = NULL;
    }
    else
    {
        if(data < root->data)
            root->left = insert(root->left,data);
        else if(data > root->data)
            root->right = insert(root->right,data);
    }
    return root;
}
/* Non Recursive Code For insertion
struct tNode *insert(struct tNode *root,int data)
{
	if(!root)
	{
		root = (struct tNode *)malloc(sizeof(struct tNode));
		root->data = data;
		root->left = root->right = NULL;
	}
	else
	{
	    struct tNode *temp = root;
	    struct tNode *parent;
		while(temp)
		{
			if(temp->data > data)
			{
			    parent = temp;
			    temp = temp->left;
			}
			else if(temp->data < data)
			{
                parent = temp;
			    temp = temp->right;
			}
		}
		temp = (struct tNode *)malloc(sizeof(struct tNode));
		temp->data = data;
		if(parent->data > temp->data)
            parent->left = temp;
        else
            parent->right = temp;
		temp->left = temp->right = NULL;
	}
	return root;
}
*/
struct tNode *delete(struct tNode *root,int data)
{
	struct tNode *temp;
	if(!root)
		printf("NOT FOUND");
    else if(data < root->data)
		root->left = delete(root->left,data);
    else if(data > root->data)
		root->right = delete(root->right,data);
	else
	{
		if(root->left && root->right)
		{
			temp = findmin(root->right);
			root->data = temp->data;
			root->right = delete(root->right,root->data);
		}
		else
		{
			temp = root;
			if(root->left == NULL)
				root = root->right;
			else if(root->right == NULL )
				root = root->left;
			free(temp);
		}
	}
	return root;
}
