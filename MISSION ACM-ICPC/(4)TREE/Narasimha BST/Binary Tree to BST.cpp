#include<cstdio>
#include<cstdlib>
#include<algorithm>

using namespace std;

struct Tree
{
    int data;
    struct Tree *left,*right;
};


struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
    return temp;
}

int Size(struct Tree *root)
{
    if(!root)
        return 0;
    else
        return (Size(root->left) + 1 + Size(root->right));
}

void GetInorder(struct Tree *root,int *inorder,int *index)
{
    if(root)
    {
        GetInorder(root->left,inorder,index);
        inorder[(*index)++] = root->data;
        GetInorder(root->right,inorder,index);
    }
}

void arraytoBST(struct Tree *root,int *inorder,int *index)
{
    if(root)
    {
        arraytoBST(root->left,inorder,index);
        root->data = inorder[(*index)++];
        arraytoBST(root->right,inorder,index);
    }
}

void BinaryTreetoBST(struct Tree *root)
{
    int count = Size(root);
    int inorder[count+1];
    int i = 0;
    GetInorder(root,inorder,&i);
    sort(inorder,inorder + count);
    i = 0;
    arraytoBST(root,inorder,&i);
}

void Inorder(struct Tree *root)
{
    if(root)
    {
        Inorder(root->left);
        printf("%d ",root->data);
        Inorder(root->right);
    }
}
int main(void)
{
    struct Tree *root = CreateNode(10);
    root->left = CreateNode(2);
    root->right = CreateNode(7);
    root->left->left = CreateNode(8);
    root->left->right = CreateNode(4);
    BinaryTreetoBST(root);
    Inorder(root);
    return 0;
}
