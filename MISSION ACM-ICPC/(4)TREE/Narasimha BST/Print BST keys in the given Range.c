

#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
struct Tree
{
    int data;
    struct Tree *left,*right;
};

struct Tree *CreateNode(int data)
{
    struct Tree *temp = (struct Tree *)malloc(sizeof(struct Tree));
    temp->data = data;
    temp->left = temp->right = NULL;
}

int Inorder(struct Tree *root,int k1,int k2)
{
    if(root)
    {
        if(k1 < root->data)
            Inorder(root->left,k1,k2);
        if(root->data >= k1 && root->data <= k2)
            printf("%d ",root->data);
        if(k2 > root->data)
            Inorder(root->right,k1,k2);
    }
}

int main(void)
{
    struct Tree *root = CreateNode(20);
    root->left = CreateNode(8);
    root->right = CreateNode(22);
    root->left->left = CreateNode(4);
    root->left->right = CreateNode(12);
    int k1 = 10,k2 = 22;
        Inorder(root,k1,k2);
    return 0;
}
