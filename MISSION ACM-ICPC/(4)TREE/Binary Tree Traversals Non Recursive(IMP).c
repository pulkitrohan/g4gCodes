#include<stdio.h>
#include<stdlib.h>
#define MAX 100
struct tNode           //Tree
{
   int data;
   struct tNode* left;
   struct tNode* right;
};

typedef struct Node   //Stack
{
    struct tNode *t;
    struct Node *next;
}node;

typedef struct Queue    //Queue
{
	struct tNode *t;
	struct Queue *next;
}Queue;

Queue *rear = NULL,*front = NULL;   //Rear and front for Queue

//Stack ADT Starts
void push(node *S,struct tNode *root)
{
		node *temp;
        temp= (node *)malloc(sizeof(node));
        temp->next = S->next;
        temp->t = root;
        S->next = temp;
}

struct tNode* pop(node *S)
{
        node *temp;
        temp = S->next;
        S->next = temp->next;
        return temp->t;
}

struct tNode *Top(node *S)
{
    return S->next->t;
}

int isEmpty(node *S)
{
    if(S->next == NULL)
        return 1;
    else return 0;
}

//Stack ADT Over


//Queue ADT Starts
void Enqueue(struct tNode *root)
{
    Queue *temp = (Queue *)malloc(sizeof(Queue));
    temp->t = root;
    temp->next = NULL;
    if(rear == NULL)
        rear = front = temp;
    else
    {
        rear->next = temp;
        rear = temp;
    }
}

struct tNode *Dequeue()
{
    if(isEmptyQueue())
        {
            return NULL;
        }
    else if(front == rear)
    {
        Queue *temp = front;
        front = rear = NULL;
            return temp->t;
    }
    else
    {
        Queue *temp = front;
        front = front->next;
        return temp->t;
    }
}

int isEmptyQueue()
{
    if(front == NULL)
        return 1;
    else
        return 0;
}
//Queue ADT Over

struct tNode* newtNode(int data);
void inOrder(struct tNode *root);
void preOrder(struct tNode *root);
void postOrder(struct tNode *root);
struct tNode* pop(node *S);
void push(node *S,struct tNode *root);


int main(void)
{
    int ch;
  /* Constructed binary tree is
            1
          /   \
        2      3
      /  \    /  \
    4     5  6    7
  */
  struct tNode *root  = newtNode(1);
  root->left          = newtNode(2);
  root->right         = newtNode(3);
  root->left->left    = newtNode(4);
  root->left->right   = newtNode(5);
  root->right->left   = newtNode(6);
  root->right->right  = newtNode(7);
  start:
  printf("1.InOrder Traversal\n");
  printf("2.PreOrder Traversal\n");
  printf("3.PostOrder Traversal\n4.LevelOrder Traversal\n");
  printf("5.Exit\n");
  printf("Which Traversal Do you want to execute : ");
  scanf("%d",&ch);
  switch(ch)
  {
      case 1: printf("\nInOrder Traversal : ");
              inOrder(root);
              break;
      case 2: printf("\nPreOrder Traversal : ");
              preOrder(root);
              break;
     case 3:  printf("\nPostOrder Traversal : ");
              postOrder(root);
              break;
    case 4:   printf("\nLevelOrder Traversal : ");
              levelOrder(root);
              break;
      case 5: exit(0);
  }
  printf("\n");
  goto start;
  return 0;
}

struct tNode* newtNode(int data)
{
  struct tNode* tNode = (struct tNode*)
                       malloc(sizeof(struct tNode));
  tNode->data = data;
  tNode->left = NULL;
  tNode->right = NULL;

  return(tNode);
}

void inOrder(struct tNode *root)
{
    node *S = (node *)malloc(sizeof(node));
    S->next = NULL;
    while(1)
    {
        while(root)
        {
            push(S,root);
            root = root->left;
        }
            if(isEmpty(S))
            break;
            root= pop(S);
            printf("%d ",root->data);
            root = root->right;
    }
}

void preOrder(struct tNode *root)
{
    node *S = (node *)malloc(sizeof(node));
    S->next = NULL;
    while(1)
    {
        while(root)
        {
            printf("%d ",root->data);
            push(S,root);
            root = root->left;
        }
            if(isEmpty(S))
            break;
            root= pop(S);
            root = root->right;
    }
}
//Two Stacks
void postOrder(struct tNode *root)
{
	node *S1 = (node *)malloc(sizeof(node));
        S1->next = NULL;
    node *S2 = (node *)malloc(sizeof(node));
       S2->next = NULL;
	push(S1,root);
	struct tNode *temp;
	while(!isEmpty(S1))
	{
		temp = pop(S1);
		push(S2,temp);
		if(temp->left)
		push(S1,temp->left);
		if(temp->right)
		push(S1,temp->right);
	}
	while(!isEmpty(S2))
	{
		temp = pop(S2);
		printf("%d ",temp->data);
	}
}

void PostOrder2Stack(struct Tree *root)
{
	stack<struct Tree *> S1;
	stack<struct Tree *> S2;
	S1.push(root);
	while(!S1.empty())
	{
		node = S1.top();
		S1.pop();
		S2.push(node);
		if(node->left)
			S1.push(node->left);
		if(node->right)
			S1.push(node->right);
	}
	while(!S2.empty())
	{
		printf("%d ",S2.top()->data);
		S2.pop();
	}
}

void levelOrder(struct tNode *root)
{
	Enqueue(root);
	while(!isEmptyQueue())
	{
		root = Dequeue();
		printf("%d " ,root->data);
		if(root->left != NULL)
            Enqueue(root->left);
		if(root->right != NULL)
            Enqueue(root->right);
	}
}



//Single Stack
/*void postOrder(node *root)
{
	do
	{
		while(root)
		{
			if(root->right)
				push(S,root->right);
			push(S,root);
			root = root->left;
		}
		root = pop(S);
		if(root->right && Top(S) == root->right)
		{
			temp = Pop(S);
			Push(S,root);
			root = root->right;
		}
		else
		{
			printf("%d ",root->data);
			root = NULL;
		}
	}
	while(!isEmpty(S));
}

*/

void PostOrderSingleStack(struct Tree *root)
{
    stack<struct Tree *> S;
    do
    {
        while(root)
        {
            if(root->right)
                S.push(root->right);
            S.push(root);
            root = root->left;
        }
        root = S.top();
        S.pop();
        if(root->right && root->right == S.top())
        {
            temp = S.top();
            S.pop();
            S.push(root);
            root = temp;
        }
        else
        {
            printf("%d",root->data);
            root = NULL;
        }
    }
    while(!S.empty());

}
