#include<stdio.h>
#include<stdlib.h>
/* The constructed AVL Tree would be
            9
           /  \
          1    10
        /  \     \
       0    5     11
      /    /  \
     -1   2    6
    */
/* The AVL Tree after deletion of 10
            1
           /  \
          0    9
        /     /  \
       -1    5     11
           /  \
          2    6
    */

struct tNode
{
   int data;
   struct tNode* left;
   struct tNode* right;
   int height;
};

struct tNode* newtNode(int data);
void inOrder(struct tNode *root);
void preOrder(struct tNode *root);
void postOrder(struct tNode *root);
struct tNode *insert(struct tNode *root,int data);
struct tNode *delete(struct tNode *root,int data);
struct tNode *findmin(struct tNode *root);
int Height(struct tNode *root);
struct tNode *LL(struct tNode *X);
struct tNode *RR(struct tNode *W);
struct tNode *LR(struct tNode *Z);
struct tNode *RL(struct tNode *Z);
int max(int A,int B);
int getBalance(struct tNode *N)
{
    if (N == NULL)
        return 0;
        int k = Height(N->left) - Height(N->right);
    return k;
}
int main()
{
    int ch,data;

  struct tNode *root = NULL;
  start:
  printf("1.Insertion\n");
  printf("2.Deletion\n");
  printf("3.InOrder Traversal\n");
  printf("4.PreOrder Traversal\n");
  printf("5.PostOrder Traversal\n");
  printf("6.Exit\n");
  printf("Which Operation do you want to execute : ");
  scanf("%d",&ch);
  switch(ch)
  {
      case 1: printf("Enter an element : ");
              scanf("%d",&data);
              root = insert(root,data);
              break;
      case 2: printf("Enter the element to be deleted : ");
              scanf("%d",&data);
              root = delete(root,data);
              break;
      case 3: printf("\nInOrder Traversal : ");
              inOrder(root);
              break;
      case 4: printf("\nPreOrder Traversal : ");
              preOrder(root);
              break;
      case 5: printf("\nPostOrder Traversal : ");
              postOrder(root);
              break;
      case 6: exit(0);
  }
  printf("\n");
  goto start;
  return 0;
}

struct tNode* newtNode(int data)
{
  struct tNode* tNode = (struct tNode*)
                       malloc(sizeof(struct tNode));
  tNode->data = data;
  tNode->left = NULL;
  tNode->right = NULL;

  return(tNode);
}

void inOrder(struct tNode *root)
{
  if(root)
  {
      inOrder(root->left);
      printf("%d ",root->data);
      inOrder(root->right);
  }
}
void preOrder(struct tNode *root)
{
  if(root)
  {
      printf("%d ",root->data);
      preOrder(root->left);
      preOrder(root->right);
  }
}
void postOrder(struct tNode *root)
{
  if(root)
  {
      postOrder(root->left);
      postOrder(root->right);
      printf("%d ",root->data);
  }
}

struct tNode *insert(struct tNode *root,int data)
{
    if(!root)
    {
        root = (struct tNode*)malloc(sizeof(struct tNode));
        root->data = data;
        root->left = root->right = NULL;
    }
    else
    {
        if(data < root->data)
		{
			root->left = insert(root->left,data);
			if(Height(root->left) - Height(root->right) > 1)
			{
				if(data < root->left->data)
					root = LL(root);
				else
					root = LR(root);
			}
		}
        else if(data > root->data)
		{
			root->right = insert(root->right,data);
			if(Height(root->right) - Height(root->left) > 1)
			{
				if(data < root->right->data)
					root = RR(root);
				else
					root = RL(root);
			}
		}
    }
    root->height = max(Height(root->left),Height(root->right)) + 1;
    return root;
}

struct tNode *delete(struct tNode *root,int data)
{
	struct tNode *temp;
	if(!root)
		printf("NOT FOUND");
  else if(data < root->data)
	 root->left = delete(root->left,data);
  else if(data > root->data)
	 root->right = delete(root->right,data);
	else
	{
		if(root->left && root->right)
		{
			temp = findmin(root->right);
			root->data = temp->data;
			root->right = delete(root->right,root->data);
		}
		else
		{
			temp = root;
			if(root->left == NULL)
				root = root->right;
			else if(root->right == NULL )
				root = root->left;
			free(temp);
		}
	}
	root->height = max(Height(root->left), Height(root->right)) + 1;
    int balance = getBalance(root);


    if (balance > 1 && getBalance(root->left) >= 0)     //Left Left
        return LL(root);

    else if (balance > 1 && getBalance(root->left) < 0)      // Left Right
    {
        return LR(root);
    }
   else if (balance < -1 && getBalance(root->right) <= 0)   //Right Right
        return RR(root);

   else if (balance < -1 && getBalance(root->right) > 0)    //Right Left
    {
        return RL(root);
    }

	return root;
}
struct tNode *findmin(struct tNode *root)
{
	if(!root)
		return NULL;
	else
	{
		while(root->left)
		root = root->left;
	}
	return root;
}
int Height(struct tNode *root)
{
	if(!root)
	return -1;
	else
	return root->height;
}


int max(int A,int B)
{
	if( A > B )
	return A;
	else
	return B;
}

struct tNode *LL(struct tNode *X)
{
	struct tNode *W = X->left;
	X->left = W->right;
	W->right = X;
	X->height = max(Height(X->left),Height(X->right)) + 1;
	W->height = max(Height(W->left),Height(W->right)) + 1;
	return W;
}

struct tNode *RR(struct tNode *W)
{
	struct tNode *X = W->right;
	W->right = X->left;
	X->left = W;
	W->height = max(Height(W->right),Height(W->left)) + 1;
	X->height = max(Height(X->right),Height(X->left)) + 1;
	return X;
}

struct tNode *LR(struct tNode *Z)
{
	Z->left = RR(Z->left);
	return LL(Z);
}
struct tNode *RL(struct tNode *Z)
{
	Z->right = LL(Z->right);
	return RR(Z);
}
