#include<stdio.h>
typedef struct Tree
{
    int data;
    struct Tree *left;
    struct Tree *right;
}node;
int n;
#define MAX n
int a[100];
node *CreateTree(int i)
{
    if(i > MAX)
        return NULL;
    node *temp = (node *)malloc(sizeof(node));
    temp->data = a[i];
    temp->left = CreateTree(2*i);
    temp->right = CreateTree(2*i + 1);
    return temp;
}
void inorder(node *root)
{
    if(root)
    {
        inorder(root->left);
        printf("%d " ,root->data);
        inorder(root->right);
    }
}

int main(void)
{
    node *root = NULL;
    int i;
    printf("Enter the total number of elements : ");
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
        printf("\nEnter the element : ");
        scanf("%d",&a[i]);
    }
    i = 1;
    root = CreateTree(i);
    inorder(root);
    return 0;
}
