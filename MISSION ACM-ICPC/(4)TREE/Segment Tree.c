#include<stdio.h>
#define N 7

int st[4*N] = {0},A[N] = {0};

int left(int p)
{
    return p << 1;
}
int right(int p)
{
    return (p << 1) + 1;
}

void build(int p,int L,int R)
{
    if(L == R)
        st[p] = L;
    else
    {
        build(left(p),L,(L+R)/2);
        build(right(p),(L+R)/2+1,R);
        int p1 = st[left(p)],p2 = st[right(p)];
        st[p] = (A[p1] <= A[p2]) ? p1 : p2;
    }
}

int rmq1(int p,int L,int R,int i,int j)
{
    if(i > R || j < L)
        return -1;
    if(L >= i && R <= j)
        return st[p];
    int p1 = rmq1(left(p),L,(L+R)/2,i,j);
    int p2 = rmq1(right(p),(L+R)/2+1,R,i,j);
    if(p1 == -1)
        return p2;
    if(p2 == -1)
        return p1;
    return (A[p1] <= A[p2]) ? p1 : p2;
}

int rmq(int i,int j)
{
    return rmq1(1,0,N-1,i,j);
}

int update_point(int idx, int new_value)
{
    return update_point1(1, 0, N - 1, idx, new_value);
}

int update_point1(int p, int L, int R, int idx, int new_value)
{
    int i = idx, j = idx;
    if (i > R || j < L)
      return st[p];
    if (L == i && R == j)
    {
      A[i] = new_value;
      return st[p] = L;
    }

    int p1, p2;
    p1 = update_point1(left(p) , L , (L + R) / 2, idx, new_value);
    p2 = update_point1(right(p), (L + R) / 2 + 1, R, idx, new_value);

    return st[p] = (A[p1] <= A[p2]) ? p1 : p2;
  }


int main(void)
{
    int i;
    printf("\nEnter the Elements : ");
    for(i=0;i<N;i++)
    scanf("%d",&A[i]);
    build(1,0,N-1);
    printf("%d\n",rmq(1,3));
    printf("%d\n",rmq(4,6));
    printf("%d\n",rmq(3,4));
    printf("%d\n",rmq(0,0));
    printf("%d\n",rmq(0,1));
    printf("%d\n",rmq(0,6));
    update_point(5,100);
    printf("%d\n",rmq(0,6));
    printf("%d\n",rmq(4,6));
    printf("%d\n",rmq(4,5));
    return 0;
}
