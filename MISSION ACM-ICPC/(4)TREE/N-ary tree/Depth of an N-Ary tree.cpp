#include<bits/stdc++.h>
using namespace std;

struct Tree
{
	char data;
	vector<Tree *> child;
};

Tree *newNode(char data)
{
	Tree *temp = (Tree *)malloc(sizeof(Tree));
	temp->data = data;
	return temp;
}

int depth(Tree *root)
{
	if(!root)
		return 0;
	int maxDepth = 0;
	for(auto it = root->child.begin();it != root->child.end();it++)
		maxDepth = max(maxDepth,depth(*it));
	return maxDepth + 1;
}

int main(void)
{
	Tree *root = newNode('A');
   (root->child).push_back(newNode('B'));
   (root->child).push_back(newNode('F'));
   (root->child).push_back(newNode('D'));
   (root->child).push_back(newNode('E'));
   (root->child[0]->child).push_back(newNode('K'));
   (root->child[0]->child).push_back(newNode('J'));
   (root->child[2]->child).push_back(newNode('G'));
   (root->child[3]->child).push_back(newNode('C'));
   (root->child[3]->child).push_back(newNode('H'));
   (root->child[3]->child).push_back(newNode('I'));
   (root->child[0]->child[0]->child).push_back(newNode('N'));
   (root->child[0]->child[0]->child).push_back(newNode('M'));
   (root->child[3]->child[2]->child).push_back(newNode('L'));
   cout<<depth(root)<<endl;
   return 0;
}