#include<stdio.h>


void FindDepth(int *parent,int *depth,int i)
{
	if(depth[i])
		return;
	if(parent[i] == -1)
    {
        depth[i] = 1;
        return;
    }
	if(depth[parent[i]] == 0)
		FindDepth(parent,depth,parent[i]);
	depth[i] = depth[parent[i]] + 1;
}

int Height(int *parent,int N)
{
	int depth[N+1],i;
	for(i=0;i<N;i++)
		depth[i] = 0;
	for(i=0;i<N;i++)
		FindDepth(parent,depth,i);
	int h = depth[0];
	for(i=1;i<N;i++)
		if(h < depth[i])
			h = depth[i];
	return h;
}

int main(void)
{
	int parent[] = {-1,0,0,1,1,3,5};
	int N = sizeof(parent)/sizeof(parent[0]);
	printf("%d\n",Height(parent,N));
	return 0;
}
