#include<bits/stdc++.h>
using namespace std;
#define N 5
#define MARKET ')'

struct node
{
	char data;
	struct node *child[N];
};

struct node *newNode(char data)
{
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp->data = data;
	for(int i=0;i<N;i++0
		temp->child[i] = NULL;
	return temp;
}

void serialize(struct node *root,FILE *fp)
{
	if(root)
	{
		fprintf(fp,"%c ",root->data);
		for(int i=0;i<N && root->child[i];i++)
			serialize(root->child[i],fp);
		fprintf(fp,"%c ",MARKER);
	}
}

int Deserialize(struct node * &root,FILE *fp)
{
	char val;
	if(!fscanf(fp,"%c ",&val) || val == MARKER)
		return 1;
	root = newNode(val);
	for(int i=0;i<N;i++)
		if(Deserialize(root->child[i],fp))
			break;
	return 0;
}

void Inorder(struct node *root)
{
	if(root)
	{
		printf("%c ",root->data);
		for(int i=0;root->child[i];i++)
			Inorder(root->child[i]);
	}
}

int main(void)
{
	struct node *root = newNode('A');
	root->child[0] = newNode('B');
    root->child[1] = newNode('C');
    root->child[2] = newNode('D');
    root->child[0]->child[0] = newNode('E');
    root->child[0]->child[1] = newNode('F');
    root->child[2]->child[0] = newNode('G');
    root->child[2]->child[1] = newNode('H');
    root->child[2]->child[2] = newNode('I');
    root->child[2]->child[3] = newNode('J');
    root->child[0]->child[1]->child[0] = newNode('K');
	FILE *fp = fopen("tree.txt","w");
	if(!fp)
	{
		printf("COULD NOT OPEN FILE\n");
		return 0;
	}
	serialize(root,fp);
	fclose(fp);
	struct node *root1 = NULL;
	fp = fopen("tree.txt","r");
	Deserialize(root1,fp);
	Inorder(root1);
	return 0;
}