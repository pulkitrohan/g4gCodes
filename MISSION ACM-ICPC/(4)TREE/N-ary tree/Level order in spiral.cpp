#include<bits/stdc++.h>
using namespace std;

struct TreeNode
{
	int data,count;
	struct TreeNode **child;
};

TreeNode *createNode(int data)
{
	TreeNode *temp = (TreeNode *)malloc(sizeof(TreeNode));
	temp->data = data;
	temp->count = 0;
	temp->child = NULL;
	return temp;
}

void Spiral(TreeNode *root)
{
	if(!root)
		return;
	stack<TreeNode *> S1,S2;
	S1.push(root);
	TreeNode *temp;
	while(!S1.empty() || !S2.empty())
	{	
		while(!S1.empty())
		{
			temp = S1.top();
			S1.pop();
			cout<<temp->data<<" ";
			for(int i=0;i<temp->count;i++)
				S2.push(temp->child[i]);
		}
		cout<<endl;
		while(!S2.empty())
		{
			temp = S2.top();
			S2.pop();
			cout<<temp->data<<" ";
			for(int i=temp->count-1;i>=0;i--)
				S1.push(temp->child[i]);
		}
		cout<<endl;
	}
}

int main(void)
{
	TreeNode *root = createNode(1);
	root->count = 3;
	root->child = (TreeNode **)malloc(sizeof(TreeNode *)*root->count);
	root->child[0] = createNode(2);
	root->child[1] = createNode(3);
	root->child[2] = createNode(4);
	
	root->child[0]->count = 3;
	root->child[0]->child = (TreeNode **)malloc(sizeof(TreeNode *)*root->child[0]->count);
	root->child[0]->child[0] = createNode(5);
	root->child[0]->child[1] = createNode(6);
	root->child[0]->child[2] = createNode(7);
	
	root->child[2]->count = 2;
	root->child[2]->child = (TreeNode **)malloc(sizeof(TreeNode *)*root->child[2]->count);
	root->child[2]->child[0] = createNode(8);
	root->child[2]->child[1] = createNode(9);
	
	Spiral(root);
	return 0;
}