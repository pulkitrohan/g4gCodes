#include<bits/stdc++.h>
using namespace std;

struct Tree
{
   char data;
   vector<Tree *> child;
};

Tree *newNode(char data)
{
   Tree *temp = (Tree *)malloc(sizeof(Tree));
   temp->data = data;
   return temp;
}

map<Tree *,int> NodeHeightMap;

int depth(Tree *root)
{
   if(!root)
      return 0;
   int maxDepth = 0;
   for(auto it = root->child.begin();it != root->child.end();it++)
   {
      maxDepth = max(maxDepth,depth(*it));
      NodeHeightMap[*it] = maxDepth;
   }
   return maxDepth + 1;
}

int diameter(Tree *root)
{
   if(!root)
      return 0;
   int max1 = 0,max2 = 0;
   for(auto it = root->child.begin();it != root->child.end();it++)
   {
      int h = NodeHeightMap[*it];
      if(h > max1)
         max2 = max1,max1 = h;
      else if(h > max2)
         max2 = h;
   }
   int maxDiameter = 0;
   for(auto it = root->child.begin();it != root->child.end();it++)
      maxDiameter = max(maxDiameter,diameter(*it));
   return max(maxDiameter,max1+max2+1);
}

int DiameterTree(Tree *root)
{
   if(!root)
      return 0;
   depth(root);
   return diameter(root);
}

int main(void)
{
   Tree *root = newNode('A');
   (root->child).push_back(newNode('B'));
   (root->child).push_back(newNode('F'));
   (root->child).push_back(newNode('D'));
   (root->child).push_back(newNode('E'));
   (root->child[0]->child).push_back(newNode('K'));
   (root->child[0]->child).push_back(newNode('J'));
   (root->child[2]->child).push_back(newNode('G'));
   (root->child[3]->child).push_back(newNode('C'));
   (root->child[3]->child).push_back(newNode('H'));
   (root->child[3]->child).push_back(newNode('I'));
   (root->child[0]->child[0]->child).push_back(newNode('N'));
   (root->child[0]->child[0]->child).push_back(newNode('M'));
   (root->child[3]->child[2]->child).push_back(newNode('L'));
   cout<<DiameterTree(root)<<endl;
   return 0;
}