#include<stdio.h>
#include<stdlib.h>

struct tNode
{
   int data;
   struct tNode* left;
   struct tNode* right;
};

struct tNode* newtNode(int data);
void inOrder(struct tNode *root);
void preOrder(struct tNode *root);
void postOrder(struct tNode *root);
int main()
{
    int ch;
  /* Constructed binary tree is
            1
          /   \
        2      3
      /  \    /  \
    4     5  7    8
  */
  struct tNode *root  = newtNode(1);
  root->left          = newtNode(2);
  root->right         = newtNode(3);
  root->left->left    = newtNode(4);
  root->left->right   = newtNode(5);
  root->right->left   = newtNode(7);
  root->right->right  = newtNode(8);
  start:
  printf("1.InOrder Traversal\n");
  printf("2.PreOrder Traversal\n");
  printf("3.PostOrder Traversal\n");
  printf("4.Exit\n");
  printf("Which Traversal Do you want to execute : ");
  scanf("%d",&ch);
  switch(ch)
  {
      case 1: printf("\nInOrder Traversal : ");
              inOrder(root);
              break;
      case 2: printf("\nPreOrder Traversal : ");
              preOrder(root);
              break;
      case 3: printf("\nPostOrder Traversal : ");
              postOrder(root);
              break;
      case 4: exit(0);
  }
  printf("\n");
  goto start;
  return 0;
}

struct tNode* newtNode(int data)
{
  struct tNode* tNode = (struct tNode*)
                       malloc(sizeof(struct tNode));
  tNode->data = data;
  tNode->left = NULL;
  tNode->right = NULL;

  return(tNode);
}

void inOrder(struct tNode *root)
{
  if(root)
  {
      inOrder(root->left);
      printf("%d ",root->data);
      inOrder(root->right);
  }
}
void preOrder(struct tNode *root)
{
  if(root)
  {
      printf("%d ",root->data);
      preOrder(root->left);
      preOrder(root->right);
  }
}
void postOrder(struct tNode *root)
{
  if(root)
  {
      postOrder(root->left);
      postOrder(root->right);
      printf("%d ",root->data);
  }
}
