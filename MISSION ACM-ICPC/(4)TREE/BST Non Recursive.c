#include<stdio.h>
#include<stdlib.h>

struct tNode
{
   int data;
   struct tNode* left;
   struct tNode* right;
};

typedef struct Node   //Stack
{
    struct tNode *t;
    struct Node *next;
}node;

struct tNode* newtNode(int data);
void inOrder(struct tNode *root);
void preOrder(struct tNode *root);
void postOrder(struct tNode *root);
struct tNode *insert(struct tNode *root,int data);
struct tNode *delete(struct tNode *root,int data);
struct tNode *FindMax(struct tNode *root);
void push(node *S,struct tNode *root);
struct tNode* pop(node *S);
int isEmpty(node *S);
int main()
{
    int ch,data;

  struct tNode *root = NULL;
  start:
  printf("1.Insertion\n");
  printf("2.Deletion\n");
  printf("3.InOrder Traversal\n");
  printf("4.PreOrder Traversal\n");
  printf("5.PostOrder Traversal\n");
  printf("6.Exit\n");
  printf("Which Operation do you want to execute : ");
  scanf("%d",&ch);
  switch(ch)
  {
      case 1: printf("Enter an element : ");
              scanf("%d",&data);
              root = insert(root,data);
              break;
      case 2: printf("Enter the element to be deleted : ");
              scanf("%d",&data);
              root = delete(root,data);
              break;
      case 3: printf("\nInOrder Traversal : ");
              inOrder(root);
              break;
      case 4: printf("\nPreOrder Traversal : ");
              preOrder(root);
              break;
      case 5: printf("\nPostOrder Traversal : ");
              postOrder(root);
              break;
      case 6: exit(0);
  }
  printf("\n");
  goto start;
  return 0;
}

struct tNode* newtNode(int data)
{
  struct tNode* tNode = (struct tNode*)
                       malloc(sizeof(struct tNode));
  tNode->data = data;
  tNode->left = NULL;
  tNode->right = NULL;

  return(tNode);
}



struct tNode *insert(struct tNode *root,int data)
{
    if(!root)
    {
        root = (struct tNode*)malloc(sizeof(struct tNode));
        root->data = data;
        root->left = root->right = NULL;
    }
    else
    {
        if(data < root->data)
            root->left = insert(root->left,data);
        else if(data > root->data)
            root->right = insert(root->right,data);
    }
    return root;
}

struct tNode *delete(struct tNode *root,int data)
{
    struct tNode *temp;
    if(!root)
        printf("Element not Found");
    else if(data < root->data)
        root->left = delete(root->left,data);
    else if(data > root->data)
        root->right = delete(root->right,data);
    else
    {
        if(root->left && root->right)
        {
            temp = FindMax(root->left);
            root->data = temp->data;
            root->left = delete(root->left,root->data);
        }
        else
        {
            temp = root;
            if(root->left)
                root = root->left;
            else if(root->right)
                root = root->right;
            free(temp);
        }
    }
    return root;
}

struct tNode *FindMax(struct tNode *root)
{
    if(!root)
     return NULL;
    else if(!root->right)
        return root;
    else
        return FindMax(root->right);
}

void inOrder(struct tNode *root)
{
    node *S,*temp1;
        S = (node *)malloc(sizeof(node));
        temp1 = S;
        temp1->next = NULL;
    while(1)
    {
        while(root)
        {
            push(S,root);
            root = root->left;
        }
            if(isEmpty(S))
            break;
            root= pop(S);
            printf("%d ",root->data);
            root = root->right;
    }
}

void preOrder(struct tNode *root)
{
    node *S,*temp1;
        S = (node *)malloc(sizeof(node));
        temp1 = S;
        temp1->next = NULL;
    while(1)
    {
        while(root)
        {
            printf("%d ",root->data);
            push(S,root);
            root = root->left;
        }
            if(isEmpty(S))
            break;
            root= pop(S);
            root = root->right;
    }
}
void postOrder(struct tNode *root)
{
	node *S1 = (node *)malloc(sizeof(node));
        S1->next = NULL;
    node *S2 = (node *)malloc(sizeof(node));
       S2->next = NULL;
	push(S1,root);
	struct tNode *temp;
	while(!isEmpty(S1))
	{
		temp = pop(S1);
		push(S2,temp);
		if(temp->left)
		push(S1,temp->left);
		if(temp->right)
		push(S1,temp->right);
	}
	while(!isEmpty(S2))
	{
		temp = pop(S2);
		printf("%d ",temp->data);
	}
}


void push(node *S,struct tNode *root)
{
		node *temp;
        temp= (node *)malloc(sizeof(node));
        temp->next = S->next;
        temp->t = root;
        S->next = temp;
}

int isEmpty(node *S)
{
    if(S->next == NULL)
        return 1;
    else return 0;
}
struct tNode* pop(node *S)
{
        node *temp;
        temp = S->next;
        S->next = temp->next;
        return temp->t;

}

