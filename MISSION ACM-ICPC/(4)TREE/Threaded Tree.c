#include<stdio.h>
#include<stdlib.h>
enum boolean
{false = 0,true = 1};

typedef struct Threaded
{
    int data;
    struct Threaded *left;
    struct Threaded *right;
    enum boolean isleft;
    enum boolean isright;
}node;

void insert(node *s,int num);
void delete(node *root,int num);
int search(node *root,int num,node *par,node *x,int found);
void inOrder(node *root);
int main(void)
{
    node *head = NULL;
    int ch,data;
    start:
  printf("1.Insertion\n");
  printf("2.Deletion\n");
  printf("3.InOrder Traversal\n");
  printf("4.Exit\n");
  printf("Which Operation do you want to execute : ");
  scanf("%d",&ch);
  switch(ch)
  {
      case 1: printf("Enter an element : ");
              scanf("%d",&data);
              insert(head,data);
              break;
      case 2: printf("Enter the element to be deleted : ");
              scanf("%d",&data);
              delete(head,data);
              break;
      case 3: printf("\nInOrder Traversal : ");
              inOrder(head);
              break;
      case 4: exit(0);
  }
  printf("\n");
  goto start;
  return 0;
}

void insert(node *root,int num)
{
	node *p,*z;
	node *head = s;
	z = (node *)malloc(sizeof(node));
	z->data = num;
	z->isleft = true;	        //True indicates it doesnt have a left child
	z->isright = true;          //True indicates it doesnt have a right child

	if(!s)	//Tree is Empty
	{
		head = (node *)malloc(sizeof(node));
		head->isleft = false;       //False indicates it has a left child
		head->left = z;
		head->right = head;
		head->isright = false;      //False indicates it has a right child
		s = head;
		z->left = head;
		z->right = head;
	}
	else
	{
	p = head->left;
	while(p != head)
	{
		if(p->data > num)
		{
			if(p->isleft != true)
				p = p->left;
			else
			{
				z->left  = p->left;
				p->left = z;
				p->isleft = false;
				z->isright = true;
				z->right = p;
			}
		}
		else
		{
			if(p->data < num)
			{
				if(p->isright != true)
					p = p->right;
				else
				{
					z->right = p->right;
					p->right = z;
					p->isright = false;
					z->isleft = true;
					z->left = p;
				}
			}
		}
	}
	}
}
void delete(node *root,int num)
{
	int found;
	node *parent,*x,*xsucc;
	if(!root)
	{
		printf("Tree is Empty ");
		return ;
	}
	parent = x = NULL;
	found = search(root,num,parent,x,found);
	if(found == false)
	{
		printf("Data to be deleted is not found");
		return ;
	}
	if(x->isleft == false && x->isright == false)	//Has Two Children
	{
		parent = x;
		xsucc = x->right;
	 while(xsucc->isleft  == false)
		{
			parent = xsucc;
			xsucc = xsucc->left;
		}
	x->data = xsucc->data;
	x = xsucc;
	}
	if(x->isleft == true && x->isright == true)		//Has No Child
	{
		if(!parent)
		{
			root->left = root;
			root->isleft = true;
			free(x);
			return ;
		}
		if(parent->right == x)
		{
			parent->isright = true;
			parent->right = x->right;
		}
		else
		{
			parent->isleft = true;
			parent->left = x->left;
		}
		free(x);
		return ;
	}
	if(x->isleft == true && x->isright == false)	//Node has Only RightChild
	{
		if(!parent)
		{
			root->left = x->right;
			free(x);
			return ;
		}
		if(parent->left == x)
		{
			parent->left = x->right;
			x->right->left = x->left;
		}
		else
		{
			parent->right = x->right;
			x->right->left = parent;
		}
		free(x);
		return;
	}
	if(x->isleft == false && x->isright == true)
	{
		if(parent == NULL)
		{
			parent = x;
			xsucc = x->left;
			while(xsucc->isright == false)
				xsucc = xsucc->right;
			xsucc->right = root;
			root->left = x->left;
			free(x);
			return;
		}
		if(parent->left == x)
		{
			parent->left = x->left;
			x->left->right = parent;
		}
		else
		{
			parent->right = x->left;
			x->left->right = x->right;
		}
		free(x);
		return ;
	}
}

int search(node *root,int num,node *par,node *x,int found)
{
	node *q;
	q = root->left;
	found = false;
	par = NULL;
	while(q != root)
	{
		if(q->data == num)
		{
			found = true;
			x = q;
			return;
		}
		par = q;
		if(q->data > num)
		{
			if(q->isleft == true)
			{
				found = false;
				x = NULL;
				return;
			}
			q = q->left;
		}
		else
		{
			if(q->isright == true)
			{

				found = false;
				x = NULL;
				return;
			}
			q = q->right;
		}
	}
	return found;
}

void inOrder(node *root)
{
	node *p;
	p = root->left;
	while(p != root)
	{
		while(p->isleft == false)
			p = p->left;
		printf("%d ",p->data);
		while(p->isright == true)
		{
			p = p->right;
			if(p == root)
				break;
			printf("%d " ,p->data);
		}
		p = p->right;
	}
}

