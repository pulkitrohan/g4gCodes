#include <stdio.h>
#define MAXSIZE 1000000
static char memory[MAXSIZE];
static char *mempointer = memory;
char *alloc(int N)
{
	if(memory+MAXSIZE - mempointer >= N)
	{
		mempointer += N;
		return mempointer - N;
	}
	else
		return 0;
}

void afree(char *P)
{
	if(P >= memory && P <= memory+MAXSIZE)
		mempointer = P;
}

int main(void)
{
	char *A = (char *)alloc(100);
	if(!A)
		printf("NO MEMORY\n");
	else
	{
		strcpy(A,"I LOVE IT");
		printf("%s\n",A);
	}
	afree(A);
	return 0;
}