#include<stdio.h>
#include<stdlib.h>

typedef struct LL
{
    int data;
    struct LL *next;
}node;

struct List
{
    node *head;
};

typedef struct chain
{
    int V;
    struct List *array;
}Chain;

Chain *CreateChain(int V)
{
    int i;
    Chain *C = (Chain *)malloc(sizeof(Chain));
    C->V = V;
    C->array = (struct List *)malloc(sizeof(struct List)*V);
    for(i=0;i<V;i++)
        C->array[i].head = NULL;
    return C;
}
node *Createnode(int data)
{
    node *temp = (node *)malloc(sizeof(node));
    temp->data = data;
    temp->next = NULL;
}

void Additem(Chain *C,int val)
{
    node *temp = Createnode(val);
    val = val%C->V;
    temp->next = C->array[val].head;
    C->array[val].head = temp;
}
void search(Chain *C,int val)
{
    int k = val%C->V;
    node *Crawl = C->array[k].head;
    while(Crawl->data != val)
        Crawl = Crawl->next;
    if(!Crawl)
        printf("NOT FOUND");
    else
        printf("FOUND");
}

int main(void)
{
    int n,val;
    printf("Enter the table size : ");
    scanf("%d",&n);
    Chain *C = CreateChain(n);
    start:
    printf("Enter the element : ");
    scanf("%d",&val);
    Additem(C,val);
    if(val != 0)
        goto start;
    printf("Enter the number to be searched : ");
    scanf("%d",val);
    search(C,val);
    return 0;
}
