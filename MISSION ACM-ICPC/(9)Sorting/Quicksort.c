#include<stdio.h>

void Swap(int *A,int *B)
{
	int temp = *A;
	*A = *B;
	*B = temp;
}

int Partition(int *A,int low,int high)
{
	int pivot = A[low],j = low,i;
	for(i=low+1;i<=high;i++)
	{
		if(A[i] <= pivot)
		{
			j++;
			Swap(&A[i],&A[j]);
		}
	}
	Swap(&A[j],&A[low]);
	return j;
}

void QuickSort(int *A,int low,int high)
{

	if(high > low)
	{
		int pivot_index = Partition(A,low,high);
		QuickSort(A,low,pivot_index-1);
		QuickSort(A,pivot_index+1,high);
	}
}


int main(void)
{
    int N,i;
   printf("Enter the number of elements : ");
   scanf("%d",&N);
   int A[N+1];
   printf("Enter the numbers : ");
   for(i=0;i<N;i++)
    scanf("%d",&A[i]);
   QuickSort(A,0,N-1);
   for(i=0;i<N;i++)
    printf("%d ",A[i]);
    return 0;
}


