#include<bits/stdc++.h>
using namespace std;

void Merge(int *A,int low,int mid,int high)
{
	int i=low,j=mid+1,k = 0;
	int temp[high-low+1];
	while(i <= mid && j <= high)
	{
		if(A[i] < A[j])
			temp[k++] = A[i++];
		else
			temp[k++] = A[j++];
	}
	while(i <= mid)
		temp[k++] = A[i++];
	while(j <= high)
		temp[k++] = A[j++];
	for(int i=0;i<k;i++)
		A[low+i] = temp[i];
}

void MergeSort(int *A,int low,int high)
{
	if(high > low)
	{
		int mid = low + (high-low)/2;
		MergeSort(A,low,mid);
		MergeSort(A,mid+1,high);
		Merge(A,low,mid,high);
	}
}

int main(void)
{
	int N;
	scanf("%d",&N);
	int A[N+1];
	for(int i=0;i<N;i++)
		scanf("%d",&A[i]);
	printf("BEFORE SORTING : ");
	for(int i=0;i<N;i++)
		printf("%d ",A[i]);
	printf("\n");
	MergeSort(A,0,N-1);
	printf("AFTER SORTING : ");
	for(int i=0;i<N;i++)
		printf("%d ",A[i]);
	return 0;
}