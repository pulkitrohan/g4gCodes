#include<cstdio>
#include<algorithm>
using namespace std;
int RandomizedPivot(int *A,int l,int r)
{
    int PivotIndex = l + (rand() % (r - l +1));
    int pivot = A[PivotIndex];
    swap(A[PivotIndex],A[l]);
    int i = l,j,temp;
    for(j=l+1;j<=r;j++)
    {
        if(A[j] < pivot)
        {
            i++;
            swap(A[i],A[j]);
        }
    }
    swap(A[i],A[l]);
    return i;
}

int RandomizedSelect(int *A,int l,int r,int k)
{
    if(l == r)
        return A[l];
    int q = RandomizedPivot(A,l,r);
    if(q+1 == k)
        return A[q];
    else if(q + 1 > k)
        RandomizedSelect(A,l,q-1,k);
    else
        RandomizedSelect(A,q+1,r,k);
}

int main(void)
{
    int N,i,k;
    printf("Enter the size of Array : ");
    scanf("%d",&N);
    int A[N+1];
    printf("Enter the Elements : ");
    for(i=0;i<N;i++)
        scanf("%d",&A[i]);
    printf("Enter the order statistics to be found : ");
    scanf("%d",&k);
    printf("%d",RandomizedSelect(A,0,N-1,k));
}
