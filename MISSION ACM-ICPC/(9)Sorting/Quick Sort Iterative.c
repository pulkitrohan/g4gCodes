#include<stdio.h>

void Swap(int *A,int *B)
{
	int temp = *A;
	*A = *B;
	*B = temp;
}

int Partition(int *A,int low,int high)
{
	int pivot = A[low],j = low,i;
	for(i=low+1;i<=high;i++)
	{
		if(A[i] <= pivot)
		{
			j++;
			Swap(&A[i],&A[j]);
		}
	}
	Swap(&A[j],&A[low]);
	return j;
}


void QuickSortIterative(int *A,int l,int h)
{
    int Stack[h-l+1];
    int top = -1;
    Stack[++top] = l;
    Stack[++top] = h;
    while(top >= 0)
    {
        h = Stack[top--];
        l = Stack[top--];
        int p = Partition(A,l,h);
        if(p-1 > l)
        {
            Stack[++top] = l;
            Stack[++top] = p-1;
        }
        else if(p+1 < h)
        {
            Stack[++top] = p+1;
            Stack[++top] = h;
        }
    }
}

int main(void)
{
    int A[] = {4,3,5,2,1,3,2,3},i;
    int N = sizeof(A)/sizeof(A[0]);
    QuickSortIterative(A,0,N-1);
    for(i=0;i<N;i++)
        printf("%d ",A[i]);
    return 0;
}
