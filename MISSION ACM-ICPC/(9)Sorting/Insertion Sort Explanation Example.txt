Insertion Sort

Suppose we have an array of elements of type integer.
the index of elements are from 0 to n-1
so starting from let say j = 2 to n-1
we set i = j-1 and key = element at index j
now while i>= 0 AND element at index i > key
assign value at index i to index i+1 and decrement.
Now when either of the above stated condition is voilated then
we it means that either i becomes less than 0 or value at index i is not less than key
now we assign the value of key to array index i+1.

Stable Sort => Yes
Online => Yes
Sorting Inplace => Yes
Adaptive Sorting => Yes

for(j=1;j<N;j++)
{
	i = j-1;
	key = A[j];
	while(i >= 0 && A[i] > key)
		A[i+1] = A[i];
	A[i+1] = key;
}

eg : 12 11 13 5 6

j = 1 i = 0 key = 11
	12 12 13 5 6 => 11 12 13 5 6
j=2 i = 1 key = 13
Remains Same => 11 12 13 5 6
j = 3 i = 2 key = 5
i >= 0 and A[i] > key => true => 11 12 13 13 6 i set to 1
i >= 0 and A[i] > key => true => 11 12 12 13 6 i set to 0
i >= 0 and A[i] > key => true => 11 11 12 13 6 i set to -1
i >= 0 and A[i] > key => false.Set A[i+1] to key i.e now we get 5 11 12 13 6

j = 4 i = 3 key = 6
i >= 0 and A[i] > key => true => 5 11 12 13 13 i set to 2
i >= 0 and A[i] > key => true => 5 11 12 12 13 i set to 1
i >= 0 and A[i] > key => true => 5 11 11 12 13 i set to 0
i >= 0 and A[i] > key => true => 5 5 11 12 13 i set to -1
i >= 0 and A[i] > key => false.
Set A[i+1] to key i.e now we get 5 6 11 12 13


