#include<stdio.h>
void quicksort(int *a,int first,int last);
int partition(int *a,int first,int last);
int main(void)
{
    int n,i,a[20];
    printf("Enter how many elements do you want:");
    scanf("%d",&n);
    for(i=0;i<n;i++)
    {
        printf("\nEnter the element:");
        scanf("%d",&a[i]);
    }
    quicksort(a,0,n-1);
    for(i=0;i<n;i++)
    printf("%d ",a[i]);
    return 0;
}

void quicksort(int *a,int first,int last)
{
    int pivot_index;
    if(last > first)
    {
     pivot_index=partition(a,first,last);
     quicksort(a,first,pivot_index-1);
     quicksort(a,pivot_index+1,last);
    }
}
int partition(int *a,int first,int last)
{
    int pivot = a[first],i = first,j = last,temp;
    while(i < j)
    {
        while(a[i] <= pivot)
            i++;
        while(a[j] >= pivot)
            j--;
        if(i < j)
        {
            temp = a[i];
            a[i] = a[j];
            a[j] = temp;
        }
    }
    temp = a[first];
    a[first] = a[j];
    a[j] = temp;
    return j;
}
