#include<bits/stdc++.h>
using namespace std;
int GetMax(int *A,int N)
{
    int max = A[0],i;
    for(i=1;i<N;i++)
        if(A[i] > max)
            max = A[i];
    return max;
}

void CountSort(int *A,int N,int exp)
{
    int i,count[N],output[N];
    memset(count,0,sizeof(count));
    for(i=0;i<N;i++)
        count[(A[i]/exp)%N]++;
    for(i=1;i<10;i++)
        count[i] += count[i-1];
    for(i=N-1;i>=0;i--)
    {
        output[count[(A[i]/exp)%N]-1] = A[i];
        count[(A[i]/exp)%N]--;
    }
    for(i=0;i<N;i++)
        A[i] = output[i];
}

void RadixSort(int *A,int N)
{
    int max = GetMax(A,N),i;
    for(i = 1;max/i > 0;i *= N)
        CountSort(A,N,i);

}

int main(void)
{
    int A[] = {170,45,75,90,802,24,2,66};
    int N = sizeof(A)/sizeof(A[0]),i;
    printf("Before Sorting : ");
    for(i=0;i<N;i++)
        printf("%d ",A[i]);
    RadixSort(A,N);
    printf("\nAfter Sorting : ");
    for(i=0;i<N;i++)
        printf("%d ",A[i]);
    return 0;
}
