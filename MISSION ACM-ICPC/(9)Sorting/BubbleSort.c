#include<stdio.h>


int main(void)
{
    int N,i,j;
    printf("Enter N : ");
    scanf("%d",&N);
    int A[N+1];
    printf("Enter the Array : ");
    for(i=0;i<N;i++)
        scanf("%d",&A[i]);
    printf("Before Sorting : ");
    for(i=0;i<N;i++)
        printf("%d ",A[i]);
    for(i=0;i<N;i++)
    {
        for(j=0;j<N-i-1;j++)
        {
            if(A[j] > A[j+1])
            {
                int temp = A[j];
                A[j] = A[j+1];
                A[j+1] = temp;
            }
        }
    }
    printf("\nAfter Sorting  : ");
    for(i=0;i<N;i++)
        printf("%d ",A[i]);
    return 0;
}
