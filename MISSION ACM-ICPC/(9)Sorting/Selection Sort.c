#include<stdio.h>
#define MAX 30000

void Swap(int *A,int *B)
{
    int temp = *A;
    *A = *B;
    *B = temp;
}

int main(void)
{
    int min_index = 0,min = MAX,n,a[100],temp,i,j;
    printf("Enter the number of elements you want : ");
    scanf("%d",&n);
    printf("Enter Array : ");
    for(i=0;i<n;i++)
        scanf("%d",&a[i]);
    printf("Before Sorting : ");
    for(i=0;i<n;i++)
        printf(" %d ",a[i]);
    for(i=0;i<n;i++)
    {
        min_index = i;
        for(j=i+1;j<n;j++)
        {
            if(a[min_index] > a[j])
                min_index = j;
        }
        if(min_index != i)
        {
            Swap(&a[min_index],&a[i]);
        }
    }
    printf("\nAfter Sorting : ");
    for(i=0;i<n;i++)
        printf(" %d ",a[i]);
    return 0;
}
