#include<stdio.h>
void mergesort(int *a,int first,int last);
void merge(int *a,int first,int mid,int last);
int main(void)
{
    int i,n;
    long a[100];
    n = 6;
    for( i = 0 ; i < n ; i++ )
        scanf("%d",&a[i]);
    mergesort(a,0,n-1);
    for( i = 0 ; i < n ; i++)
        printf("%d ",a[i]);
    return 0;
}

void mergesort(int *a,int first,int last)
{
    int mid = ( first+last )/2;
    if( last > first)
    {
        mergesort(a,first,mid);
        mergesort(a,mid+1,last);
        merge(a,first,mid,last);
    }
}
void merge(int *a,int first,int mid,int last)
{
    int pos = 0,i = first,j = mid+1;
    int temp[last-first+1];
    while( i <= mid && j <= last )
    {
        if( a[i] > a[j] )
            temp[pos++] = a[j++];
            else
                temp[pos++] = a[i++];
    }
    while(i <= mid)
        temp[pos++] = a[i++];
    while(j <= last)
        temp[pos++] = a[j++];
    for( i = 0 ; i < pos ; i++ )
    {
        a[i+first] = temp[i];
    }
}
