#include<bits/stdc++.h>
using namespace std;

int BinarySearch(int *A,int temp,int start,int end)
{
	if(start >= end)
		return (A[start] < temp) ? (start+1) : start;
	int mid = start + (end-start)/2;
	if(A[mid] == temp)
		return mid+1;
	else if(A[mid] < temp)
		return BinarySearch(A,temp,mid+1,end);
	else
		return BinarySearch(A,temp,start,mid-1);
}

void InsertionSort(int *A,int N)
{
	for(int i=1;i<N;i++)
	{
		int j = i-1;
		int temp = A[i];
		int loc = BinarySearch(A,temp,0,j);
		while(j >= loc)
			A[j+1] = A[j--];
		A[j+1] = temp;
	}
}
		

int main(void)
{
	int A[] = {37,23,0,17,12,72,31,46,100,88,54};
	int N = sizeof(A)/sizeof(A[0]);
	InsertionSort(A,N);
	for(int i=0;i<N;i++)
		cout<<A[i]<<" ";
	return 0;
}