#include<bits/stdc++.h>
using namespace std;

int Partition(int *A,int low,int high)
{
	int pivot = A[low];
	int j = low;
	for(int i=low+1;i<=high;i++)
	{
		if(A[i] < pivot)
		{
			j++;
			swap(A[i],A[j]);
		}
	}
	swap(A[j],A[low]);
	return j;
}

void QuickSort(int *A,int low,int high)
{
	if(high > low)
	{
		int pivot_index = Partition(A,low,high);
		QuickSort(A,low,pivot_index-1);
		QuickSort(A,pivot_index+1,high);
	}
}

int main(void)
{
	int N;
	scanf("%d",&N);
	int A[N+1];
	for(int i=0;i<N;i++)
		scanf("%d",&A[i]);
	printf("BEFORE SORTING : ");
	for(int i=0;i<N;i++)
		printf("%d ",A[i]);
	printf("\n");
	QuickSort(A,0,N-1);
	printf("AFTER SORTING : ");
	for(int i=0;i<N;i++)
		printf("%d ",A[i]);
	return 0;
}
