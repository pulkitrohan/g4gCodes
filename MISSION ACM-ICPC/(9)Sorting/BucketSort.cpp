#include<bits/stdc++.h>
using namespace std;

void BucketSort(float *A,int N)
{
	vector<float> b[N];
	for(int i=0;i<N;i++)
		b[(int)A[i]*N].push_back(A[i]);
	for(int i=0;i<N;i++)
		sort(b[i].begin(),b[i].end());
	int index = 0;
	for(int i=0;i<N;i++)
		for(int j=0;j<b[i].size();j++)
			A[index++] = b[i][j];
}
		

int main(void)
{
	float A[] = {0.897,0.565,0.656,0.1234,0.665,0.3434};
	int N = sizeof(A)/sizeof(A[0]);
	BucketSort(A,N);
	for(int i=0;i<N;i++)
		cout<<A[i]<<" ";
	return 0;
}
	