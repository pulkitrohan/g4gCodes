/*
  Non Recursive MergeSort.
  Downside is it uses extra memory than Recursive mergeSort.
  Time Complexity : O(NlogN)
  Space Complexity : O(N)
*/

#include<stdio.h>
void mergesort(int *a,int first,int last);
void merge(int *a,int first,int mid,int last);
int main(void)
{
    int i,n;
    int a[100];
    n = 6;
    for( i = 0 ; i < n ; i++ )
        scanf("%d",&a[i]);
    mergesort(a,0,n-1);
    for( i = 0 ; i < n ; i++)
        printf("%d ",a[i]);
    return 0;
}

int min(int A,int B)
{
   return ( A > B) ? B : A;
}
void mergesort(int *a,int first,int last)
{
    int N = last + 1,i,j;
    for(i=1;i<N;i = i + i)
    {
        for(j=0;j<N-i;j += i + i)
            merge(a,j,j+i-1,min(j+i+i-1,N-1));
    }
}
void merge(int *a,int first,int mid,int last)
{
    int pos = 0,i = first,j = mid+1;
    int temp[last-first+1];
    while( i <= mid && j <= last )
    {
        if( a[i] > a[j] )
            temp[pos++] = a[j++];
            else
                temp[pos++] = a[i++];
    }
    while(i <= mid)
        temp[pos++] = a[i++];
    while(j <= last)
        temp[pos++] = a[j++];
    for( i = 0 ; i < pos ; i++ )
    {
        a[i+first] = temp[i];
    }
}
