/* CH6PR7.C: Program to implement a circular queue as a linked list. */

#include <stdio.h>
#include <conio.h>
#include <malloc.h>
#include <windows.h>

/* structure containing a data part and link part */
struct node
{
	int data ;
	struct node * link ;
} ;

void addcirq ( struct node **, struct node **, int ) ;
int delcirq ( struct node **, struct node ** ) ;
void cirq_display ( struct node * ) ;

int main( )
{
	struct node *front, *rear ;
    int data,ch;
	front = rear = NULL ;
    label:
        printf("1.Enqueue");
        printf("\n2.Dequeue");
        printf("\n3.Print All");
        printf("\n4.Exit");

    printf("\nEnter your choice:");
    scanf("%d",&ch);
    switch(ch)
    {
        case 1:  printf("\nEnter the element to be Enqueued :");
                 scanf("%d",&data);
                 addcirq ( &front, &rear, data ) ;
                 break;
        case 2:  delcirq ( &front, &rear ) ;
                 break;
        case 3:  cirq_display ( front );
                 break;
        case 4:  exit(0);
                 break;
    }
    goto label;
	return 0 ;
}

/* adds a new element at the end of queue */
void addcirq ( struct node **f, struct node **r, int item )
{
	struct node *q ;

	/* create new node */
	q = ( struct node * ) malloc ( sizeof ( struct node ) ) ;
	q -> data = item ;

	/* if the queue is empty */
	if ( *f == NULL )
		*f = q ;
	else
		( *r ) -> link = q ;

	*r = q ;
	( *r ) -> link = *f ;
}

/* removes an element from front of queue */
int delcirq ( struct node **f, struct node **r )
{
	struct node *q ;
	int item ;

	/* if queue is empty */
	if ( *f == NULL )
		printf ( "queue is empty\n" ) ;
	else
	{
		if ( *f == *r )
		{
			item = ( *f ) -> data ;
			free ( *f ) ;
			*f = NULL ;
			*r = NULL ;
		}
		else
		{
			/* delete the node */
			q = *f ;
			item = q -> data ;
			*f = ( *f ) -> link ;
			( *r ) -> link = *f ;
			free ( q ) ;
		}
		return ( item ) ;
	}
	return NULL ;
}

/* displays whole of the queue */
void cirq_display ( struct node *f )
{
	struct node *q = f, *p = NULL ;

	/* traverse the entire linked list */
	while ( q != p )
	{
		printf ( "%d ", q -> data ) ;

		q = q -> link ;
		p = f ;
	}
	printf ( "\n" ) ;
}
