#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
    int data;
    struct Node *next;
}node;

void insertatend(node *pointer,int data)
{
    while(pointer->next != NULL)
        pointer = pointer->next;
    pointer->next = (node *)malloc(sizeof(node));
    pointer = pointer->next;
    pointer->data = data;
    pointer->next = NULL;
}

void insertbelem(node *pointer,int data,int a)
{
    if(pointer->next != NULL)
    {
        while(pointer->next != NULL && pointer->next->data  != a)
        pointer = pointer->next;
        if(pointer->next == NULL)
            printf("Number cannot be inserted");
        else
        {
            node *temp;
            temp=(node *)malloc(sizeof(node));
            temp->next=pointer->next;
            temp->data=data;
            pointer->next=temp;
        }
    }
}
void insertaelem(node *pointer,int data,int a)
{
    if(pointer->next)
    {
        pointer = pointer->next;
    while(pointer != NULL && pointer->data  != a)
        pointer = pointer->next;
        if(pointer->next == NULL)
            printf("Number cannot be inserted");
        else
        {
            node *temp;
            temp=(node *)malloc(sizeof(node));
            temp->next=pointer->next;
            temp->data=data;
            pointer->next=temp;
        }
    }
}

void insertbpos(node *pointer,int data,int pos)
{
    int count=1;
    while(count != pos)
    {
        pointer = pointer->next;
        count++;
    }
    node *temp;
    temp=(node *)malloc(sizeof(node));
    temp->next=pointer->next;
    temp->data=data;
    pointer->next=temp;
}
void insertapos(node *pointer,int data,int pos)
{
    int count=1;
    while(count != pos + 1)
    {
        pointer = pointer->next;
        count++;
    }
    node *temp;
    temp=(node *)malloc(sizeof(node));
    temp->next=pointer->next;
    temp->data=data;
    pointer->next=temp;
}



void search(node *pointer,int data)
{
    while(pointer->next != NULL && (pointer->next)->data != data)
        pointer = pointer->next;
    if(pointer->next == NULL)
    {
        printf("\nElement not found");
    }
    else if((pointer->next)->data == data)
    {
        printf("\nElement found");
    }
}

void deletebelem(node *pointer,int data)
{
    while(pointer->next != NULL && pointer->next->next->data != data)
        pointer = pointer->next;
    if(pointer->next == NULL)
    {
        printf("\nElement not found");
    }
    else
    {
        node *temp;
        temp = pointer->next;
        pointer->next = temp->next;
        free(temp);
        printf("\nElement deleted");
    }
}

void deleteaelem(node *pointer,int data)
{
    while(pointer->next != NULL && pointer->data != data)
        pointer = pointer->next;
    if(pointer->next == NULL)
    {
        printf("\nElement not found");
    }
    else
    {
        node *temp;
        temp = pointer->next;
        pointer->next = temp->next;
        free(temp);
        printf("\nElement deleted");
    }
}



void deletebpos(node *pointer,int pos)
{
    int count = 1;
    while(count != pos-1 && pointer->next != NULL)
        {
            pointer = pointer->next;
            count++;
        }
    if(pointer->next == NULL)
    {
        printf("\nElement not found");
    }
    else
    {
        node *temp;
        temp = pointer->next;
        pointer->next = temp->next;
        free(temp);
        printf("\nElement deleted");
    }
}

void deleteapos(node *pointer,int pos)
{
    int count = 1;
    while(count != pos + 1 && pointer->next != NULL)
        {
            pointer = pointer->next;
            count++;
        }
    if(pointer->next == NULL)
    {
        printf("\nElement not found");
    }
    else
    {
        node *temp;
        temp = pointer->next;
        pointer->next = temp->next;
        free(temp);
        printf("\nElement deleted");
    }
}
void printandcount(node *pointer)
{
    int count=0;
    printf("\nThe list is : ");
    pointer = pointer->next;
    while(pointer != NULL)
    {
        printf("%d ",pointer->data);
        pointer = pointer->next;
        count++;
    }
    printf("\nTotal elements are : %d",count);
}
//head 1 2 3 4 NULL

int deletelist(node *pointer)
{
    while(pointer->next)
    {
        node *temp;
        temp = pointer->next;
        pointer->next = temp->next;
        free(temp);
    }
    printf("List Deleted");
}

int main(void)
{
    node *start;
    start = (node *)malloc(sizeof(node));
    start->next = NULL;

    int ch,data,pos,a;

    insertatend(start,1);
    insertatend(start,2);
    insertatend(start,3);
    insertatend(start,4);
    insertatend(start,5);
    insertatend(start,6);
    printandcount(start);

label:
    printf("\n1.Insert");
    printf(" 2.Delete");
    printf("\n3.Print & Count");
    printf(" 4.Find");
    printf("\n5.Delete Entire List");

    printf("\nEnter your choice:");
    scanf("%d",&ch);
    switch(ch)
    {
        case 1:  printf("\nEnter the element to be inserted :");
                 scanf("%d",&data);
                 printf("\n1.Insert before an element");
                 printf("\n2.Insert after an element");
                 printf("\n3.Insert before the position");
                 printf("\n4.Insert after the position");
                 printf("\nEnter your choice:");
                 scanf("%d",&ch);
                 switch(ch)
                 {
                 case 1: printf("\nEnter the element before which the element is to be added : ");
                         scanf("%d",&a);
                         insertbelem(start,data,a);
                         break;
                 case 2: printf("\nEnter the element after which the element is to be added : ");
                         scanf("%d",&a);
                         insertaelem(start,data,a);
                         break;
                 case 3: printf("\nEnter the position before which the element is to be added : ");
                         scanf("%d",&pos);
                         insertbpos(start,data,pos);
                         break;
                 case 4: printf("\nEnter the position after which the element is to be added : ");
                         scanf("%d",&pos);
                         insertapos(start,data,pos);
                         break;
                 }
                 break;

        case 2:  printf("\n1.Delete before an element");
                 printf("\n2.Delete after an element");
                 printf("\n3.Delete before the position");
                 printf("\n4.Delete after the position");
                 printf("\nEnter your choice:");
                 scanf("%d",&ch);
                 switch(ch)
                 {
                 case 1: printf("\nEnter the element before which the element is to be deleted : ");
                         scanf("%d",&a);
                         deletebelem(start,a);
                         break;
                 case 2: printf("\nEnter the element after which the element is to be deleted : ");
                         scanf("%d",&a);
                         deleteaelem(start,a);
                         break;
                 case 3: printf("\nEnter the position before which the element is to be deleted : ");
                         scanf("%d",&pos);
                         deletebpos(start,pos);
                         break;
                 case 4: printf("\nEnter the position after which the element is to be deleted : ");
                         scanf("%d",&pos);
                         deleteapos(start,pos);
                         break;
                 }
                 break;
        case 3:  printandcount(start);
                 break;
        case 4:  printf("\nEnter the element to be find :");
                 scanf("%d",&data);
                 search(start,data);
                 break;
        case 5:  deletelist(start);
                 break;
        case 6:  exit(0);
                 break;
    }
    goto label;
    return 0;
}
