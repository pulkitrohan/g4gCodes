#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
    int data;
    struct Node *next;
}node;

void push(node *S,int data)
{
		node *temp;
        temp= (node *)malloc(sizeof(node));
        temp->next = S->next;
        temp->data = data;
        S->next = temp;
}
void pop(node *S)
{
    if(S->next == NULL)
    {
        printf("\nUnderflow!!");
    }
    else
    {
        node *temp;
        temp = S->next;
        S->next = temp->next;
        free(temp);
    }
}
int top(node *S)
    {
        return S->next->data;
    }
void print(node *S)
{
    if(!S->next)
    {
        printf("Stack Empty");
        return;
    }
    while(S->next)
    {
        printf("%d ",S->next->data);
        S = S->next;
    }
}


int main(void)
{
    int ch,data;
        node *S;
        S = (node *)malloc(sizeof(node));
        S->next = NULL;
        printf("1.Push");
        printf("\n2.Pop");
        printf("\n3.Print Top");
        printf("\n4.Print");
        printf("\n5.Exit");

    label:
    printf("\nEnter your choice:");
    scanf("%d",&ch);
    switch(ch)
    {
        case 1:  printf("\nEnter the element to be Pushed :");
                 scanf("%d",&data);
                 push(S,data);
                 printf("\nElement Pushed");
                 break;
        case 2:  pop(S);
                 printf("\nElement is Popped");
                 break;
        case 3:  printf("\nTop element is : %d",top(S));
                 break;
        case 4:  print(S);
                 break;
        case 5:  exit(0);
                 break;
    }
    goto label;
        return 0;
}
