#include<stdio.h>
#include<stdlib.h>

typedef struct queue
{
    int *element;
    int capacity;
    int front;
    int rear;
    int size;
}queue;

queue *createQueue(int MaxSize)
{
    queue *Q;
    Q = (queue *)malloc(sizeof(queue));
    Q->element = (int *)malloc(sizeof(int)*MaxSize);
    Q->capacity = MaxSize;
    Q->front = 0;
    Q->rear = -1;
    Q->size = 0;
    return Q;
}

void Enqueue(queue *Q,int element)
{
    if(Q->size == Q->capacity)
        printf("\nQueue is Full");
    else
    {
       Q->size++;
       Q->rear++;
       Q->element[Q->rear] = element;
    }
}

void Dequeue(queue *Q)
{
    if(Q->size == 0)
        printf("\nQueue is Empty");
    else
    {
        Q->size--;
        int data = Q->element[Q->front];;
        Q->front++;
        printf("\n%d is Dequeued",data);
    }

}

int front(queue *Q)
{
    if(Q->size == 0)
    {
        printf("\nQueue is Empty");
        exit(0);
    }
    return Q->element[Q->front];
}

void print(queue *Q)
{
    int k =Q->size;
    while(k--)
        printf("%d ",Q->element[k]);
}
int main(void)
{
    int n,ch,data;
    table:
        printf("Enter the size of Queue : ");
        scanf("%d",&n);
        if(n <= 0)
        {
            printf("Enter the size again..\n");
            goto table;
        }
        queue  *Q = createQueue(n);

        printf("\n1.Enqueue");
        printf("\n2.Dequeue");
        printf("\n3.Print Front");
        printf("\n4.Print");
        printf("\n5.Exit");
    label:
    printf("\nEnter your choice:");
    scanf("%d",&ch);
    switch(ch)
    {
        case 1:  printf("\nEnter the element to be Enqueued :");
                 scanf("%d",&data);
                 Enqueue(Q,data);
                 break;
        case 2:  Dequeue(Q);
                 break;
        case 3:  printf("\nTop element is : %d",front(Q));
                 break;
        case 4:  print(Q);
                 break;
        case 5:  exit(0);
                 break;
    }
    goto label;
        return 0;
}
