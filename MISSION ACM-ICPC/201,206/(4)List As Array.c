#include<stdio.h>

int n = 5;
void insertaelem(int *array,int data,int a)
{

    int i = n-1;
    while(array[i] != a)
    {
        array[i+1] = array[i];
        i--;
    }
    n++;
    array[i+1] = data;

}
void insertbelem(int *array,int data,int a)
{
    int i = n-1;
    while(array[i] != a)
    {
        array[i+1] = array[i];
        i--;
    }

    array[i+1]=array[i];
    i--;
    array[i+1] = data;
    n++;
}

void insertbpos(int *array,int data,int pos)
{
    int i=n-1;
    while(i >= pos-1)
    {
        array[i+1] = array[i];
        i--;
    }
    array[i+1] = data;
    n++;
}

void insertapos(int *array,int data,int pos)
{
    int i = n;
    while(i > pos-1)
    {
        array[i+1] = array[i];
        i--;
    }
    array[i+1] = data;
    n++;
}

void printandcount(int *array)
{
    int i;
    printf("\nArray : ");
    for(i = 0;i<n;i++)
        printf("%d ",array[i]);
    printf("\nCount =  %d",n);
}
void deleteaelem(int *array,int data)
{
    int i=0,j;
    while(array[i] != data)
    {
        i++;
    }
    if(array[i] == data)
    {
        for(j=i+1;j<n-1;j++)
        {
        array[j]= array[j+1];
        }
        n--;
    }
}

void deletebelem(int *array,int data)
{

int i=0,j;
    while(array[i] != data)
    {
        i++;
    }
    if(array[i] == data)
    {
        for(j=i-1;j<n-1;j++)
        {
        array[j]= array[j+1];
        }
        n--;
    }
}

void deletebpos(int *array,int pos)
{
    int i=0,j;
    while(i !=pos-1)
        i++;
        if(i== pos-1)
    {
        for(j=i;j<n-1;j++)
        array[j]=array[j+1];
        n--;
    }
}

void deleteapos(int *array,int pos)
{
    int i=0,j;
    while(i !=pos)
        i++;
        if(i== pos)
   {
        for(j=i;j<n-1;j++)
        array[j]=array[j+1];
        n--;
   }
}
void search(int *array,int data)
{
    int i;
    for(i = 0;i<n;i++)
    {
        if(array[i] == data)
            {
                printf("%d is found at %d position",data,i+1);
                return ;
            }
        else continue;
    }
    if(i==n)
        printf("NOT FOUND");
}
int main(void)
{

    int ch,data,pos,a,array[100],i;

    for(i=0;i<n;i++)
        array[i] = i+1;

   /*
    printf("Enter the inital size of array :");
    scanf("%d",&n);
    printf("\nEnter the elements : ");
    for(i=0;i<n;i++)
        scanf("%d",&array[i]);
    printf("\nArray : "); */
    for(i=0;i<n;i++)
        printf("%d ",array[i]);

    printf("\n1.Insert");
    printf(" 2.Delete");
    printf("\n3.Print & Count");
    printf(" 4.Find");
    printf("\n5.Exit");

    label:
    printf("\nEnter your choice:");
    scanf("%d",&ch);
    switch(ch)
    {
        case 1:  printf("\nEnter the element to be inserted :");
                 scanf("%d",&data);
                 printf("\n1.Insert before an element");
                 printf("\n2.Insert after an element");
                 printf("\n3.Insert before the position");
                 printf("\n4.Insert after the position");
                 printf("\nEnter your choice:");
                 scanf("%d",&ch);
                 switch(ch)
                 {
                 case 1: printf("\nEnter the element before which the element is to be added : ");
                         scanf("%d",&a);
                         insertbelem(array,data,a);
                         break;
                 case 2: printf("\nEnter the element after which the element is to be added : ");
                         scanf("%d",&a);
                         insertaelem(array,data,a);
                         break;
                 case 3: printf("\nEnter the position before which the element is to be added : ");
                         scanf("%d",&pos);
                         insertbpos(array,data,pos);
                         break;
                 case 4: printf("\nEnter the position after which the element is to be added : ");
                         scanf("%d",&pos);
                         insertapos(array,data,pos);
                         break;
                 }
                 break;
        case 2:  printf("\n1.Delete before an element");
                 printf("\n2.Delete after an element");
                 printf("\n3.Delete before the position");
                 printf("\n4.Delete after the position");
                 printf("\nEnter your choice:");
                 scanf("%d",&ch);
                 switch(ch)
                 {
                 case 1: printf("\nEnter the element before which the element is to be deleted : ");
                         scanf("%d",&a);
                         deletebelem(array,a);
                         break;
                 case 2: printf("\nEnter the element after which the element is to be deleted : ");
                         scanf("%d",&a);
                         deleteaelem(array,a);
                         break;
                 case 3: printf("\nEnter the position before which the element is to be deleted : ");
                         scanf("%d",&pos);
                         deletebpos(array,pos);
                         break;
                 case 4: printf("\nEnter the position after which the element is to be deleted : ");
                         scanf("%d",&pos);
                         deleteapos(array,pos);
                         break;
                 }
                 break;
        case 3:  printandcount(array);
                 break;
        case 4:  printf("\nEnter the element to be find :");
                 scanf("%d",&data);
                 search(array,data);
                 break;
        case 5:  exit(0);
                 break;
    }
    goto label;
    return 0;
}
