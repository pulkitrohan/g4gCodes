#include<stdio.h>
#include<stdlib.h>

typedef struct queue
{
    int *element;
    int size;
    int capacity;
    int front;
    int rear;
}queue;

queue *createQueue(int MaxSize)
{
    queue *Q;
    Q = (queue *)malloc(sizeof(queue));
    Q->size = 0;
    Q->element = (int *)malloc(sizeof(int)*MaxSize);
    Q->capacity = MaxSize;
    Q->front = -1;
    Q->rear = -1;
    return Q;
}

void Enqueueatrear(queue *Q,int element)
{
    if(Q->size == Q->capacity)
        printf("\nQueue is Full");
    else
    {
        Q->size++;
        Q->rear++;
        Q->element[Q->rear] = element;
    }
}

void Enqueueatfront(queue *Q,int element)
{
    if(Q->size == Q->capacity)
        printf("\nQueue is Full");
    else
    {
        Q->size++;
        Q->front--;
        Q->element[Q->front] = element;
    }
}

void Dequeueatfront(queue *Q)
{
        if(Q->size == 0)
        printf("\nQueue is Empty");
    else
    {
        Q->size--;
        int data = Q->element[Q->front];
        Q->front++;
        printf("\n%d is Dequeued",data);
    }

}
void Dequeueatrear(queue *Q)
{
    if(Q->size == 0)
        printf("\nQueue is Empty");
    else
    {
        int data = Q->element[Q->rear];
        Q->size--;
        Q->rear--;;
        printf("\n%d is Dequeued",data);
    }
}

int front(queue *Q)
{
    if(Q->size == 0)
    {
        printf("\nQueue is Empty");
        exit(0);
    }
    return Q->element[Q->front];
}
void print(queue *Q)
{
   int i;
printf("\n The Queue is::");
for(i=Q->front;i<=Q->rear;i++)
{
  printf("%d  ", Q->element[i]);
}
}

int main(void)
{
    int n,ch,data;
        printf("Enter the size of Queue : ");
        scanf("%d",&n);
       queue  *Q = createQueue(n);
        printf("\n1.Enqueue at front");
        printf("\n2.Enqueue at rear");
        printf("\n3.Dequeue at front");
        printf("\n4.Dequeue at rear");
        printf("\n5.Print First");
        printf("\n6.Print");
        printf("\n7.Exit");

    label:
    printf("\nEnter your choice:");
    scanf("%d",&ch);
    switch(ch)
    {
        case 1:  printf("\nEnter the element to be Enqueued at front :");
                 scanf("%d",&data);
                 Enqueueatfront(Q,data);
                 printf("\nElement Enqueued");
                 break;
        case 2:  printf("\nEnter the element to be Enqueued at rear :");
                 scanf("%d",&data);
                 Enqueueatrear(Q,data);
                 printf("\nElement Enqueued");
                 break;
        case 3:  Dequeueatfront(Q);
                 printf("\nElement Dequeued");
                 break;
        case 4:  Dequeueatrear(Q);
                 printf("\nElement Dequeued");
                 break;
        case 5:  printf("\nTop element is : %d",front(Q));
                 break;
        case 6:  print(Q);
                 break;
        case 7:  exit(0);
                 break;
    }
    goto label;
        return 0;
}
