#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
    int data;
    struct Node *next;
    struct Node *prev;
}node;


void insertatend(node *pointer,int data)
{
    while(pointer->next != NULL)
        pointer = pointer->next;
        pointer->next = (node *)malloc(sizeof(node));
        pointer = pointer->next;
        pointer->prev=pointer;
        pointer->data = data;
        pointer->next = NULL;
}

void insertbelem(node *pointer,int data,int a)
{
    if(pointer)
    {
        while(pointer->next != NULL && pointer->next->data  != a)
        pointer = pointer->next;
        if(pointer->data == NULL)
            printf("Number cannot be inserted");
        else
        {
            node *temp;
            temp = (node *)malloc(sizeof(node));
            temp->data = data;
            temp->prev = pointer;
            temp->next = pointer->next;
            pointer->next = temp;
            pointer = pointer->next;
            pointer->prev = temp;
        }
    }
}
void insertaelem(node *pointer,int data,int a)
{
    if(pointer)
    {
        pointer = pointer->next;
    while(pointer != NULL && pointer->data  != a)
        pointer = pointer->next;
        if(pointer->next == NULL)
            printf("Number cannot be inserted");
        else
        {
            node *temp;
            temp = (node *)malloc(sizeof(node));
            temp->data = data;
            temp->prev = pointer;
            temp->next = pointer->next;
            pointer->next = temp;
            pointer = pointer->next;
            pointer->prev = temp;
        }
    }
}

void insertbpos(node *pointer,int data,int pos)
{
    int count=1;
    while(count != pos)
    {
        pointer = pointer->next;
        count++;
    }
    node *temp;
    temp = (node *)malloc(sizeof(node));
    temp->data = data;
    temp->prev = pointer;
    temp->next = pointer->next;
    pointer->next = temp;
    pointer = pointer->next;
    pointer->prev = temp;
}
void insertapos(node *pointer,int data,int pos)
{
    int count=1;
    while(count != pos + 1)
    {
        pointer = pointer->next;
        count++;
    }
    node *temp;
    temp = (node *)malloc(sizeof(node));
    temp->data = data;
    temp->prev = pointer;
    temp->next = pointer->next;
    pointer->next = temp;
    pointer = pointer->next;
    pointer->prev = temp;
}

void find(node *pointer,int data)
{
    while(pointer->next != NULL && (pointer->next)->data != data)
        pointer = pointer->next;
    if(pointer->next == NULL)
    {
        printf("\nElement not found");
    }
    else if((pointer->next)->data == data)
    {
        printf("\nElement found");
    }
}

void printandcount(node *pointer)
{
    int count=0;
    printf("\nThe list is : ");
    pointer = pointer->next;
    while(pointer != NULL)
    {
        printf("%d ",pointer->data);
        pointer = pointer->next;
        count++;
    }
    printf("\nTotal elements are : %d",count);
}

void delete(node *pointer,int data)
{
    while(pointer->next != NULL && (pointer->next)->data != data)
        pointer = pointer->next;
    if(pointer->next == NULL)
    {
        printf("\nElement not found");
    }
    else
    {
        node *temp;
        temp = pointer->next;
        pointer->next = temp->next;
        temp->prev=pointer;
        free(temp);
        printf("\nElement Deleted");

    }
}

void deletebelem(node *pointer,int data)
{
    while(pointer->next != NULL && pointer->next->next->data != data)
        pointer = pointer->next;
    if(pointer->next == NULL)
    {
        printf("\nElement not found");
    }
    else
    {
        node *temp;
        temp = pointer->next;
        pointer->next = temp->next;
        temp->prev=pointer;
        free(temp);
        printf("\nElement Deleted");
    }
}

void deleteaelem(node *pointer,int data)
{
    while(pointer->next != NULL && pointer->data != data)
        pointer = pointer->next;
    if(pointer->next == NULL)
    {
        printf("\nElement not found");
    }
    else
    {
        node *temp;
        temp = pointer->next;
        pointer->next = temp->next;
        temp->prev=pointer;
        free(temp);
        printf("\nElement Deleted");
    }
}



void deletebpos(node *pointer,int pos)
{
    int count = 1;
    while(count != pos-1 && pointer->next != NULL)
        {
            pointer = pointer->next;
            count++;
        }
    if(pointer->next == NULL)
    {
        printf("\nElement not found");
    }
    else
    {
        node *temp;
        temp = pointer->next;
        pointer->next = temp->next;
        temp->prev=pointer;
        free(temp);
        printf("\nElement Deleted");
    }
}

void deleteapos(node *pointer,int pos)
{
    int count = 1;
    while(count != pos + 1 && pointer->next != NULL)
        {
            pointer = pointer->next;
            count++;
        }
    if(pointer->next == NULL)
    {
        printf("\nElement not found");
    }
    else if(count == pos + 1)
    {
        node *temp;
        temp = pointer->next;
        pointer->next = temp->next;
        temp->prev=pointer;
        free(temp);
        printf("\nElement Deleted");
    }
    else
        printf("\nCannot be Deleted");
}

void search(node *pointer,int data)
{
    while(pointer->next != NULL && (pointer->next)->data != data)
        pointer = pointer->next;
    if(pointer->next == NULL)
    {
        printf("\nElement not found");
    }
    else if((pointer->next)->data == data)
    {
        printf("\nElement found");
    }
}

 int deletelist(node *pointer)
{
    while(pointer->next)
    {
        node *temp;
        temp = pointer->next;
        free(pointer);
        pointer = temp;
    }
    printf("List Deleted");
    return NULL;
}

int main(void)
{
    node *start,*temp;
    start = (node *)malloc(sizeof(node));
    temp = start;
    temp->prev=NULL;
    temp->next = NULL;

    int ch,data,pos,a;

    insertatend(start,1);
    insertatend(start,2);
    insertatend(start,3);
    insertatend(start,4);
    insertatend(start,5);
    insertatend(start,6);
    printandcount(start);

    printf("\n1.Insert");
    printf(" 2.Delete");
    printf("\n3.Print & Count");
    printf(" 4.Find");
    printf("\n5.Delete Entire List");
    printf(" 6.Exit");
    label:
    printf("\nEnter your choice:");
    scanf("%d",&ch);
    switch(ch)
    {
        case 1:  printf("\nEnter the element to be inserted :");
                 scanf("%d",&data);
                 printf("\n1.Insert before an element");
                 printf("\n2.Insert after an element");
                 printf("\n3.Insert before the position");
                 printf("\n4.Insert after the position");
                 printf("\nEnter your choice:");
                 scanf("%d",&ch);
                 switch(ch)
                 {
                 case 1: printf("\nEnter the element before which the element is to be added : ");
                         scanf("%d",&a);
                         insertbelem(start,data,a);
                         break;
                 case 2: printf("\nEnter the element after which the element is to be added : ");
                         scanf("%d",&a);
                         insertaelem(start,data,a);
                         break;
                 case 3: printf("\nEnter the position before which the element is to be added : ");
                         scanf("%d",&pos);
                         insertbpos(start,data,pos);
                         break;
                 case 4: printf("\nEnter the position after which the element is to be added : ");
                         scanf("%d",&pos);
                         insertapos(start,data,pos);
                         break;
                 }
                 break;

        case 2:  printf("\n1.Delete before an element");
                 printf("\n2.Delete after an element");
                 printf("\n3.Delete before the position");
                 printf("\n4.Delete after the position");
                 printf("\nEnter your choice:");
                 scanf("%d",&ch);
                 switch(ch)
                 {
                 case 1: printf("\nEnter the element before which the element is to be deleted : ");
                         scanf("%d",&a);
                         deletebelem(start,a);
                         break;
                 case 2: printf("\nEnter the element after which the element is to be deleted : ");
                         scanf("%d",&a);
                         deleteaelem(start,a);
                         break;
                 case 3: printf("\nEnter the position before which the element is to be deleted : ");
                         scanf("%d",&pos);
                         deletebpos(start,pos);
                         break;
                 case 4: printf("\nEnter the position after which the element is to be deleted : ");
                         scanf("%d",&pos);
                         deleteapos(start,pos);
                         break;
                 }
                 break;
        case 3:  printandcount(start);
                 break;
        case 4:  printf("\nEnter the element to be find :");
                 scanf("%d",&data);
                 search(start,data);
                 break;
        case 5:  start = deletelist(start);
                 break;
        case 6:  exit(0);
                 break;
    }
    goto label;
    return 0;
}
