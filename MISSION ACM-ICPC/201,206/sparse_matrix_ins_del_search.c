#include <stdio.h>
#include <conio.h>
#include <malloc.h>

#define MAX1 3
#define MAX2 3

struct sparse
{
	int *sp ;
	int row ;
} ;

void initsparse ( struct sparse *p )
{
	p -> sp = NULL ;
}

void create_array ( struct sparse *p )
{
	int n, i ;

	p -> sp = ( int * ) malloc ( MAX1 * MAX2 * sizeof ( int ) ) ;

	for ( i = 0 ; i < MAX1 * MAX2 ; i++ )
	{
			printf ( "Enter element no. %d: ", i ) ;
			scanf ( "%d", &n ) ;
			* ( p -> sp + i ) = n ;
	}
	printf ( "\n" ) ;
}

void display ( struct sparse p )
{
	int i ;

	for ( i = 0 ; i < MAX1 * MAX2 ; i++ )
	{
		if ( i % MAX2 == 0 )
			printf ( "\n" ) ;
		printf ( "%d\t", * ( p.sp + i ) ) ;
	}
	printf ( "\n\n" ) ;
}

void insert ( struct sparse *p, int elem, int loc )
{
	int i ;

	for ( i = 0 ; i < MAX1 * MAX2 ; i++ )
	{
			if( i == loc )
			* ( p -> sp + i ) = elem ;
	}
}

void delete ( struct sparse *p, int elem )
{
	int n, i ;

	for ( i = 0 ; i < MAX1 * MAX2 ; i++ )
	{
			if(* ( p -> sp + i ) == elem )
			* ( p -> sp + i ) = 0 ;
	}
}

void  search ( struct sparse *p , int elem)
{
	int i ;
	for ( i = 0 ; i < MAX1 * MAX2 ; i++ )
	{
		if( * ( p->sp + i  ) == elem )
		{
		    printf ( "%d is Found", * ( p->sp + i ) ) ;
		    break;
		}
	}
	if(i == MAX1 * MAX2)
        printf("\nNot Found");

}
int count ( struct sparse p )
{
	int cnt = 0, i ;

	for ( i = 0 ; i < MAX1 * MAX2 ; i++ )
	{
		if ( * ( p.sp + i ) != 0 )
			cnt++ ;
	}
	return cnt ;
}

void create_tuple ( struct sparse *p, struct sparse s )
{
	int r = 0 , c = -1, l = -1, i ;

	p -> row = count ( s ) + 1 ;

	p -> sp = ( int * ) malloc ( p -> row * 3 * sizeof ( int ) ) ;
	* ( p -> sp + 0 ) = MAX1 ;
	* ( p -> sp + 1 ) = MAX2 ;
	* ( p -> sp + 2 ) = p -> row - 1 ;

	l = 2 ;

	for ( i = 0 ; i < MAX1 * MAX2 ; i++ )
	{
		c++ ;

		if ( ( ( i % MAX2 ) == 0 ) && ( i != 0 ) )
		{
			r++ ;
			c = 0 ;
		}
		if ( * ( s.sp + i ) != 0 )
		{
			l++ ;
			* ( p -> sp + l ) = r ;
			l++ ;
			* ( p -> sp + l ) = c ;
			l++ ;
			* ( p -> sp + l ) = * ( s.sp + i ) ;
		}
	}
}
void display_tuple ( struct sparse p )
{
	int i ;
    printf("\nT.Rows\tT.Column Total Non 0");
	for ( i = 0 ; i < p.row * MAX2 ; i++ )
	{
	    if(i== MAX2)
            printf("\nRow\tColumn\tElement");
		if ( i % 3 == 0 )
			printf ( "\n" ) ;
		printf ( " %d\t", * ( p.sp + i ) ) ;
	}
}

int main( )
{
	struct sparse s1, s2 ;
	int c, elem,loc,ch;


	initsparse ( &s1 ) ;
	initsparse ( &s2 ) ;

	create_array ( &s1 ) ;
	printf ( "Elements in Sparse Matrix:" ) ;
	display ( s1 ) ;
    lable:
    printf("\n1.Insertion");
    printf("\n2.Deletion");
    printf("\n3.Searching");
    printf("\n4.Print Matrix");
    printf("\n5.Print Sparse Matrix with Non 0 terms only");
    printf("\n6.Exit");
    printf("\nEnter the Choice : ");
    scanf("%d",&ch);
    switch(ch)
    {
        case 1: printf("\nEnter the element to be inserted and its location (0-8): ");
                scanf("%d %d",&elem,&loc);
                insert(&s1,elem,loc);
                break;
        case 2: printf("\nEnter the element to be deleted : ");
                scanf("%d",&elem);
                delete(&s1,elem);
                break;
        case 3: printf("\nEnter the element to be searched : ");
                scanf("%d",&elem);
                search(&s1,elem);
                break;
        case 4: printf ( "Elements in Matrix:" ) ;
                display ( s1 ) ;
                break;
        case 5: c = count ( s1 ) ;
                printf ( "Number of non-zero elements: %d\n\n", c ) ;
                create_tuple ( &s2, s1 ) ;
                printf ( "Array of non-zero elements:" ) ;
                display_tuple ( s2 ) ;
                break;
        case 6: exit(0);

    }
    goto lable;
	return 0 ;
}


