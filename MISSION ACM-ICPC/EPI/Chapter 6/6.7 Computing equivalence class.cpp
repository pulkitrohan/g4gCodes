#include<bits/stdc++.h>
using namespace std;


int backtrack(int i,vector<int> F)
{
	while(i != F[i])
		i = F[i];
	return i;
}

int main(void)
{
	int N;
	cin>>N;
	vector<int> A{1,5,3,6};
	vector<int> B{2,1,0,5};
	vector<int> F(N);
	iota(F.begin(),F.end(),0);
	for(int i=0;i<A.size();i++)
	{
		int a = backtrack(A[i],F);
		int b = backtrack(B[i],F);
		if(b > a)
			F[b] = a;
		else
			F[a] = b;
	}
	for(int &i : F)
	{
		while(i != F[i])
			i = F[i];
	}
	for(int i=0;i<F.size();i++)
		cout<<F[i]<<" ";
	return 0;
}