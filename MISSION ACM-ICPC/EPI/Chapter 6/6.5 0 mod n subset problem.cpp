#include<bits/stdc++.h>
using namespace std;

vector<int> SubsetSum(vector<int> A)
{
	vector<int> prefix(A.size(),0);
	prefix[0] = A[0];
	for(int i=1;i<A.size();i++)
		prefix[i] = (prefix[i-1] + A[i])%A.size();
	vector<int> table(A.size(),-1);
	for(int i=0;i<A.size();i++)
	{
		if(prefix[i] == 0)
		{
			vector<int> ans(i+1);
			iota(ans.begin(),ans.end(),0);
			return ans;
		}
		else if(table[prefix[i]] != -1)
		{
			vector<int> ans(i-table[prefix[i]]);
			iota(ans.begin(),ans.end(),table[prefix[i]] + 1);
			return ans;
		}
		table[prefix[i]] = i;
	}	
}

int main(void)
{
	int N,x;
	cin>>N;
	vector<int> A;
	for(int i=0;i<N;i++)
	{
		cin>>x;
		A.push_back(x);
	}
	vector<int> V = SubsetSum(A);
	for(int i=0;i<V.size();i++)
		cout<<V[i]<<" ";
	return 0;
}
	
	