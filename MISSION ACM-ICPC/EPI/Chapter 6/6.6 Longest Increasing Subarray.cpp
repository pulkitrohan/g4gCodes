#include<bits/stdc++.h>
using namespace std;
int main(void)
{
	int N;
	cin>>N;
	vector<int> V(N);
	for(int i=0;i<N;i++)
		cin>>V[i];
	vector<int> dp(N,0);
	dp[0] = 1;
	for(int i=1;i<N;i++)
		if(V[i] > V[i-1])
			dp[i] = dp[i-1] + 1;
		else
			dp[i] = 1;
	int max = INT_MIN;
	for(int i=0;i<dp.size();i++)
		if(dp[i] > max)
			max = dp[i];
	int max2 = max,start,end;
	for(int i=N-1;i>=0;i--)
	{
		if(dp[i] == max2)
		{
			if(max == max2)
				end = i;
			max2--;
			if(max2 == 0)
			{
				start = i;
				break;
			}
		}
	}
	cout<<start<<" "<<end;
	return 0;
}