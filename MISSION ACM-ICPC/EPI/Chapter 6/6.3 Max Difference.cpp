#include<bits/stdc++.h>
using namespace std;

int MinimumBattery(int *A,int N)
{
	int battery = 0,min_height = A[0];
	for(int i=1;i<N;i++)
	{
		if(A[i] - min_height > battery)
			battery = A[i] - min_height;
		if(A[i] < min_height)
			min_height = A[i];
	}
	return battery;
}
int main(void)
{
	int N;
	cin>>N;
	int A[N+1];
	for(int i=0;i<N;i++)
		cin>>A[i];
	cout<<MinimumBattery(A,N);
	return 0;
}