#include<bits/stdc++.h>
using namespace std;

int MaxVal(int *A,int N)
{
	int max_sum = INT_MIN;
	for(int i=1;i<N-1;i++)
	{
		int one = 0,min_height = INT_MAX;
		for(int j=0;j<=i;j++)
		{
			if(A[j] - min_height > one)
				one = A[j] - min_height;
			if(A[j] < min_height)
				min_height = A[j];
		}
		int two = 0;
		min_height = INT_MAX;
		for(int j=i+1;j<N;j++)
		{
			if(A[j] - min_height > two)
				two = A[j] - min_height;
			if(A[j] < min_height)
				min_height = A[j];
		}
		max_sum = max(max_sum,one+two);
	}
	return max_sum;
}



int main(void)
{
	int N;
	cin>>N;
	int A[N+1];
	for(int i=0;i<N;i++)
		cin>>A[i];
	cout<<MaxVal(A,N);
	return 0;
}