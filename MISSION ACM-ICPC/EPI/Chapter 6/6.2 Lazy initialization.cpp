#include<bits/stdc++.h>
using namespace std;

int A[1000],S[1000],P[1000];
int t = 0;

bool valid(int index)
{
	if(0 <= P[index] < t && S[P[index]] == index)
		return true;
	else 
		return false;
}

bool read(int index,int *val)
{
	if(!valid(index))
		return false;
	else
	{
		*val = A[index];
		return true;
	}
}

void write(int index,int val)
{
	if(!valid(index))
	{
		S[t] = index;
		P[index] = t++;
	}
	A[index] = val;
}

int main(void)
{
	write(7,10);
	write(2,20);
	write(1,30);
	int val = 0;
	if(read(7,&val))
		cout<<val<<endl;
	else
		cout<<"No element at given index\n";
	if(read(20,&val))
		cout<<val<<endl;
	else
		cout<<"No element at given index\n";
	return 0;
}