#include<bits/stdc++.h>
using namespace std;

bool cmp(int *A,int *B)
{
	return *A < *B;
}

int main(void)
{
	int A[] = {10,9,8,7,6,5,4,3,2,1};
	int N = sizeof(A)/sizeof(A[0]);
	vector<int> V(A,A+N);
	vector<int *> P;
	for(int i=0;i<V.size();i++)
		P.emplace_back(&V[i]);
	sort(P.begin(),P.end(),cmp);
	for(int i=0;i<P.size();i++)
		cout<<*P[i]<<" ";
	return 0;
}