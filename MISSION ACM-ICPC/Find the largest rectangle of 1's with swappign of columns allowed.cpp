#include<bits/stdc++.h>
using namespace std;
#define R 3
#define C 5

int maxArea(bool A[R][C])
{
	int H[R+1][C+1];
	for(int i=0;i<C;i++)
	{
		H[0][i] = A[0][i];
		for(int j=1;j<R;j++)
			H[j][i] = (A[j][i] == 0) ? 0 : H[j-1][i]+1;
	}
	for(int i=0;i<R;i++)
	{
		int count[R+1] = {0};
		for(int j=0;j<C;j++)
			count[H[i][j]]++;
		int col_no = 0;
		for(int j=R;j>=0;j--)
			if(count[j] > 0)
				for(int k=0;k<count[j];k++)
					H[i][col_no++] = j;
	}
	int max_area = 0;
	for(int i=0;i<R;i++)
		for(int j=0;j<C;j++)
			max_area = max((j+1)*H[i][j],max_area);
	return max_area;
}

int main(void)
{
	bool A[R][C] = {{0,1,0,1,0},
					{0,1,0,1,1},
					{1,1,0,1,0}
				   };
	cout<<maxArea(A);
	return 0;
}

