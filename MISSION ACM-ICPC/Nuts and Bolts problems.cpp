#include<bits/stdc++.h>
using namespace std;

int partition(char *A,int low,int high,int pivot)
{
	int j = low;
	for(int i=low;i<high;i++)
	{
		if(A[i] < pivot)
		{
			
			swap(A[i],A[j]);
			j++;
		}
		else if(A[i] == pivot)
		{
			swap(A[i],A[high]);
			i--;
		}
	}
	swap(A[high],A[j]);
	return j;
}

void matchpairs(char *nuts,char *bolts,int low,int high)
{
	if(high > low)
	{
		int index = partition(nuts,low,high,bolts[high]);
		partition(bolts,low,high,nuts[index]);
		matchpairs(nuts,bolts,low,index-1);
		matchpairs(nuts,bolts,index+1,high);
	}
}

int main(void)
{
	char nuts[] = {'@', '#', '$', '%', '^', '&'};
	char bolts[] = {'$', '%', '&', '^', '@', '#'};
	int N = sizeof(nuts)/sizeof(nuts[0]);
	matchpairs(nuts,bolts,0,N-1);
	for(int i=0;i<N;i++)
		cout<<nuts[i]<<" ";
	cout<<endl;
	for(int i=0;i<N;i++)	
		cout<<bolts[i]<<" ";
	return 0;
}