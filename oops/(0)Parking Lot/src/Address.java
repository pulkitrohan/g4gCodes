public enum VehicleType{
    CAR,
    TRUCK,
    ELECTRIC,
    VAN,
    MOTORBIKE
}

public enum ParkingSlotType{
    HANDICAPPED,
    COMPACT,
    LARGE,
    MOTORBIKE,
    ELECTRIC
}

public enum AccountStatus{
    ACTIVE,
    BLOCKED,
    BANNED,
    COMPROMIZED,
    ARCHIVED,
    UNKNOWN
}

public enum ParkingTicketStatus{
    ACTIVE,
    PAID,
    LOST
}

public class Address {
    private String streetAddress;
    private String city;
    private String state;
    private String zipCode;
    private String country;
}
