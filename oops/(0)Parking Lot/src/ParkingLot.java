import java.util.HashMap;

public class ParkingLot {
    private String name;
    private Location addres;
    private ParkingRate parkingRate;

    private int compactSlotCount;
    private int largeSlotCount;
    private int motorbikeSlotCount;
    private int electricSlotCount;
    private final int maxCompactCount;
    private final int maxLargeCount;
    private final int maxMotorbikeCount;
    private final int maxElectricCount;

    private HashMap<String, EntrancePanel> entrancePanels;
    private HashMap<String, ExitPanel> exitPanels;
    private HashMap<String, ParkingFloor> parkingFloors;

    // all active parking tickets, identified by their ticketNumber
    private HashMap<String, ParkingTicket> activeTickets;

    // singleton ParkingLot to ensure only one object of ParkingLot in the system,
    // all entrance panels will use this object to create new parking ticket: getNewParkingTicket(),
    // similarly exit panels will also use this object to close parking tickets
    private static ParkingLot parkingLot = null;

    // private constructor to restrict for singleton
    private ParkingLot() {
        // 1. initialize variables: read name, address and parkingRate from database
        // 2. initialize parking floors: read the parking floor map from database,
        //  this map should tell how many parking slots are there on each floor. This
        //  should also initialize max slot counts too.
        // 3. initialize parking slot counts by reading all active tickets from database
        // 4. initialize entrance and exit panels: read from database
    }

    // static method to get the singleton instance of StockExchange
    public static ParkingLot getInstance()
    {
        if(parkingLot == null) {
            parkingLot = new ParkingLot();
        }
        return parkingLot;
    }

    // note that the following method is 'synchronized' to allow multiple entrances
    // panels to issue a new parking ticket without interfering with each other
    public synchronized ParkingTicket getNewParkingTicket(Vehicle vehicle)
            throws ParkingFullException {
        if(this.isFull(vehicle.getType())) {
            throw new ParkingFullException();
        }
        ParkingTicket ticket = new ParkingTicket();
        vehicle.assignTicket(ticket);
        ticket.saveInDB();
        // if the ticket is successfully saved in the database, we can increment the parking slot count
        this.incrementSlotCount(vehicle.getType());
        this.activeTickets.put(ticket.getTicketNumber(), ticket);
        return ticket;
    }

    public boolean isFull(VehicleType type) {
        // trucks and vans can only be parked in LargeSlot
        if(type == VehicleType.Truck || type == VehicleType.Van) {
            return largeSlotCount >= maxLargeCount;
        }

        // moterbikes can only be parked at motorbike slots
        if(type == VehicleType.Moterbike) {
            return motorbikeSlotCount >= maxMotorbikeCount;
        }

        // cars can be parked at compact or large slots
        if(type == VehicleType.Car) {
            return (compactSlotCount + largeSlotCount) >= (maxCompactCount + maxLargeCount);
        }

        // electric car can be parked at compact, large or electric slots
        return (compactSlotCount + largeSlotCount + electricSlotCount)
                >= (maxCompactCount + maxLargeCount + maxElectricCount);
    }

    // increment the parking slot count based on the vehicle type
    private void incrementSlotCount(VehicleType type) {
        if(type == VehicleType.Truck || type == VehicleType.Van) {
            largeSlotCount++;
        } else if (type == VehicleType.Moterbike) {
            motorbikeSlotCount++;
        } else if (type == VehicleType.Car) {
            if(compactSlotCount < maxCompactCount) {
                compactSlotCount++;
            } else {
                largeSlotCount++;
            }
        } else { // electric car
            if(electricSlotCount < maxElectricCount) {
                electricSlotCount++;
            } else if(compactSlotCount < maxCompactCount) {
                compactSlotCount++;
            } else {
                largeSlotCount++;
            }
        }
    }

    public boolean isFull() {
        for (String key : parkingFloors.keySet()) {
            if(!parkingFloors.get(key).isFull()) {
                return false;
            }
        }
        return true;
    }

    public void addParkingFloor(ParkingFloor floor) { /* stores in database */ }
    public void addEntrancePanel(EntrancePanel entrancePanel) { /* stores in database */ }
    public void addExitPanel(ExitPanel exitPanel) { /* stores in database */ }
}