public abstract class ParkingSlot {
    private String number;
    private boolean free;
    private Vehicle vehicle;
    private final ParkingSlotType type;

    public boolean IsFree();

    public ParkingSlot(ParkingSlotType type) {
        this.type = type;
    }

    public void assignVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
        free = false;
    }

    public void removeVehicle() {
        this.vehicle = null;
        free = true;
    }
}

public class HandicappedSlot extends ParkingSlot {
    public HandicappedSlot() {
        super(ParkingSlotType.HANDICAPPED);
    }
}

public class CompactSlot extends ParkingSlot {
    public CompactSlot() {
        super(ParkingSlotType.COMPACT);
    }
}

public class LargeSlot extends ParkingSlot {
    public LargeSlot() {
        super(ParkingSlotType.LARGE);
    }
}

public class MotorbikeSlot extends ParkingSlot {
    public MotorbikeSlot() {
        super(ParkingSlotType.MOTORBIKE);
    }
}

public class ElectricSlot extends ParkingSlot {
    public ElectricSlot() {
        super(ParkingSlotType.ELECTRIC);
    }
}