public class ParkingDisplayBoard {
    private String id;
    private HandicappedSlot handicappedFreeSlot;
    private CompactSlot compactFreeSlot;
    private LargeSlot largeFreeSlot;
    private MotorbikeSlot motorbikeFreeSlot;
    private ElectricSlot electricFreeSlot;

    public void showEmptySlotNumber() {
        String message = "";
        if(handicappedFreeSlot.IsFree()){
            message += "Free Handicapped: " + handicappedFreeSlot.getNumber();
        } else {
            message += "Handicaped is full";
        }
        message += System.lineSeparator();

        if(compactFreeSlot.IsFree()){
            message += "Free Compact: " + compactFreeSlot.getNumber();
        } else {
            message += "Compact is full";
        }
        message += System.lineSeparator();

        if(largeFreeSlot.IsFree()){
            message += "Free Large: " + largeFreeSlot.getNumber();
        } else {
            message += "Large is full";
        }
        message += System.lineSeparator();

        if(motorbikeFreeSlot.IsFree()){
            message += "Free Motorbike: " + motorbikeFreeSlot.getNumber();
        } else {
            message += "Motorbike is full";
        }
        message += System.lineSeparator();

        if(electricFreeSlot.IsFree()){
            message += "Free Electric: " + electricFreeSlot.getNumber();
        } else {
            message += "Electric is full";
        }

        Show(message);
    }
}