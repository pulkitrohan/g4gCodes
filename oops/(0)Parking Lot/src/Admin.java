// For simplicity, we are not defining getter and setter functions. The reader can
// assume that all class attributes are private and accessed through their respective
// public getter method and modified only through their public setter method.



public class Admin extends Account {
    public bool addParkingFloor(ParkingFloor floor);
    public bool addParkingSlot(String floorName, ParkingSlot slot);
    public bool addParkingDisplayBoard(String floorName, ParkingDisplayBoard displayBoard);
    public bool addCustomerInfoPanel(String floorName, CustomerInfoPanel infoPanel);

    public bool addEntrancePanel(EntrancePanel entrancePanel);
    public bool addExitPanel(ExitPanel exitPanel);
}