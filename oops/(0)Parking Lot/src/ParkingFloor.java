import java.util.HashMap;

public class ParkingFloor {
    private String name;
    private HashMap<String, HandicappedSlot> handicappedSlots;
    private HashMap<String, CompactSlot> compactSlots;
    private HashMap<String, LargeSlot> largeSlots;
    private HashMap<String, MotorbikeSlot> motorbikeSlots;
    private HashMap<String, ElectricSlot> electricSlots;
    private HashMap<String, CustomerInfoPortal> infoPortals;
    private ParkingDisplayBoard displayBoard;

    public ParkingFloor(String name) {
        this.name = name;
    }

    public void addParkingSlot(ParkingSlot slot) {
        switch(slot.getType()) {
            case ParkingSlotType.HANDICAPPED:
                handicappedSlots.put(slot.getNumber(), slot);
                break;
            case ParkingSlotType.COMPACT:
                compactSlots.put(slot.getNumber(), slot);
                break;
            case ParkingSlotType.LARGE:
                largeSlots.put(slot.getNumber(), slot);
                break;
            case ParkingSlotType.MOTORBIKE:
                motorbikeSlots.put(slot.getNumber(), slot);
                break;
            case ParkingSlotType.ELECTRIC:
                electricSlots.put(slot.getNumber(), slot);
                break;
            default: print("Wrong parking slot type!");
        }
    }

    public void assignVehicleToSlot(Vehicle vehicle, ParkingSlot slot){
        slot.assignVehicle(vehicle);
        switch(slot.getType()) {
            case ParkingSlotType.HANDICAPPED:
                updateDisplayBoardForHandicapped(slot); break;
            case ParkingSlotType.COMPACT:
                updateDisplayBoardForCompact(slot); break;
            case ParkingSlotType.LARGE:
                updateDisplayBoardForLarge(slot); break;
            case ParkingSlotType.MOTORBIKE:
                updateDisplayBoardForMotorbike(slot); break;
            case ParkingSlotType.ELECTRIC:
                updateDisplayBoardForElectric(slot); break;
            default: print("Wrong parking slot type!");
        }
    }

    private void updateDisplayBoardForHandicapped(ParkingSlot slot) {
        if(this.displayBoard.getHandicappedFreeSlot().getNumber() == slot.getNumber()) {
            // find another free handicapped parking and assign to displayBoard
            for (String key : handicappedSlots.keySet()) {
                if(handicappedSlots.get(key).isFree()) {
                    this.displayBoard.setHandicappedFreeSlot(handicappedSlots.get(key));
                }
            }
            this.displayBoard.showEmptySlotNumber();
        }
    }

    private void updateDisplayBoardForCompact(ParkingSlot slot) {
        if(this.displayBoard.getCompactFreeSlot().getNumber() == slot.getNumber()) {
            // find another free compact parking and assign to displayBoard
            for (String key : compactSlots.keySet()) {
                if(compactSlots.get(key).isFree()) {
                    this.displayBoard.setCompactFreeSlot(compactSlots.get(key));
                }
            }
            this.displayBoard.showEmptySlotNumber();
        }
    }

    public void freeSlot(ParkingSlot slot){
        slot.removeVehicle();
        switch(slot.getType()) {
            case ParkingSlotType.HANDICAPPED: freeelectricSlotCount++; break;
            case ParkingSlotType.COMPACT: freeCompactSlotCount++; break;
            case ParkingSlotType.LARGE: freeLargeSlotCount++; break;
            case ParkingSlotType.MOTORBIKE: freeMotorbikeSlotCount++; break;
            case ParkingSlotType.ELECTRIC: freeelectricSlotCount++; break;
            default: print("Wrong parking slot type!");
        }
    }
}