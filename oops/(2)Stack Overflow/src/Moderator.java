public class Moderator extends Member {
    public boolean closeQuestion(Question question);
    public boolean undeleteQuestion(Question question);
}
