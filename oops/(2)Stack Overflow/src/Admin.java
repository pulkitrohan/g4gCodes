public class Admin extends Member {
    public boolean blockMember(Member member);
    public boolean unblockMember(Member member);
}
