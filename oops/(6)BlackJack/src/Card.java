public class Card {
    private SUIT suit;
    private int faceValue;

    public SUIT getSuit() {
        return suit;
    }

    public int getFaceValue() {
        return faceValue;
    }

    Card(SUIT suit, int faceValue) {
        this.suit = suit;
        this.faceVale = faceValue;
    }
}

public class BlackjackCard extends Card {
    private int gameValue;

    public int getGameValue() {
        return gameValue;
    }

    public BlackjackCard(SUIT suit, int faceValue) {
        super(suit, faceValue);
        this.gameValue = faceValue;
        if(this.gameValue > 10) {
            this.gameValue = 10;
        }
    }
}

public enum SUIT {
    HEART,
    SPADE,
    CLUB,
    DIAMOND
}