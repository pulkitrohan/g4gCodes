public enum AccountStatus{
    ACTIVE,
    BLOCKED,
    BANNED,
    COMPROMIZED,
    ARCHIVED,
    UNKNOWN
}
