public class Bank {
    private String name;
    private String bankCode;

    public String getBankCode();
    public boolean addATM();
}

public class ATM {
    private int atmID;
    private Address location;

    private CashDispenser cashDispenser;
    private Keypad keypad;
    private Screen screen;
    private Printer printer;
    private CheckDeposit checkDeposit;
    private CashDeposit cashDeposit;

    public boolean authenticateUser();
    public boolean makeTransaction(Customer customer, Transaction transaction);
}

public class CashDispenser {
    private int totalFiveDollarBills;
    private int totalTwentyDollarBills;

    public boolean dispenseCash(double amount);
    public boolean canDispenseCash();
}

public class Keypad {
    public String getInput();
}

public class Screen {
    public boolean showMessage(String message);
    public TransactionType getInput();
}

public class Printer {
    public boolean printReciept(Transaction transaction);
}

public abstract class DepositSlot {
    private double totalAmount;
    public double getTotalAmount();
}

public class CheckDeposit extends DepositSlot {
    public double getCheckAmount();
}

public class CashDeposit extends DepositSlot {
    public double receiveDollarBill();
}