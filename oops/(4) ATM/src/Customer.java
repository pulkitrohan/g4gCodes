// For simplicity, we are not defining getter and setter functions. The reader can
// assume that all class attributes are private and accessed through their respective
// public getter method and modified only through their public setter function.

import java.util.Date;

public class Customer {
    private String name;
    private String email;
    private String phone;
    private Address address;
    private CustomerStatus status;

    private Card card;
    private Account account;

    public boolean makeTransaction(Transaction transaction);
    public Address getBillingAddress();
}

public class Card {
    private String cardNumber;
    private String customerName;
    private Date cardExpiry;
    private int pin;

    public Address getBillingAddress();
}

public abstract class Account {
    private int accountNumber;
    private double totalBalance;
    private double availableBalance;

    public double getAvailableBalance();
}

public class SavingAccount extends Account {
    private double withdrawLimit;
}

public class CheckingAccount extends Account {
    private String debitCardNumber;
}