// For simplicity, we are not defining getter and setter functions. The reader can
// assume that all class attributes are private and accessed through their respective
// public getter method and modified only through their public setter method.

import java.util.Date;
import java.util.List;

public class Account {
    private String id;
    private String password;
    private AccountStatus status;

    public boolean resetPassword();
}

public class Person {
    private String name;
    private Address address;
    private String email;
    private String phone;

    private Account account;
}

public class Customer extends Person {
    private String frequentFlyerNumber;

    public List<Itinerary> getItineraries();
}

public class Passenger {
    private String name;
    private String passportNumber;
    private Date dateOfBirth;

    public String getPassportNumber() {
        return this.passportNumber;
    }
}