import java.sql.Time;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class WeeklySchedule {
    private int dayOfWeek;
    private Time departureTime;
}

public class CustomSchedule {
    private Date customDate;
    private Time departureTime;
}

public class Flight {
    private String flightNumber;
    private Airport departure;
    private Airport arrival;
    private int durationInMinutes;
    private Aircraft aircraft;

    private List<WeeklySchedule> weeklySchedules;
    private List<CustomSchedule> customSchedules;
    private List<FlightInstance> flightInstances;
}

public class FlightInstance {
    private Date departureTime;
    private String gate;
    private FlightStatus status;

    public boolean cancel();
    public void updateStatus(FlightStatus status);
}

public class FlightReservation {
    private String reservationNumber;
    private FlightInstance flight;
    private Map<Passenger, FlightSeat> seatMap;
    private Date creationDate;
    private ReservationStatus status;

    public static FlightReservation fectchReservationDetails(String reservationNumber);
    public List<Passenger> getPassengers();
}

public class Itinerary {
    private String customerId;
    private Airport startingAirport;
    private Airport finalAirport;
    private Date creationDate;
    private List<FlightReservation> reservations;

    public List<FlightReservation> getReservations();
    public boolean makeReservation();
    public boolean makePayment();
}