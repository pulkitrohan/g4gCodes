import java.util.List;

public class Airport {
    private String name;
    private Address address;
    private String code;

    public List<FlightInstance> getFlights();
}

public class Aircraft {
    private String name;
    private String model;
    private int manufacturingYear;
    private List<Seat> seats;

    public List<FlightInstance> getFlights();
}

public class Seat {
    private String seatNumber;
    private SeatType type;
    private SeatClass seatClass;
}

public class FlightSeat extends Seat {
    private double fare;
    public double getFare();
}