public class Meal {
    private int mealID;
    private TableSeat seat;
    private List<MenuItem> menuItems;

    public boolean addMealItem(MealItem mealItem);
}

public class MealItem {
    private int mealItemID;
    private int quantity;
    private MenuItem menuteItem;

    public boolean updateQuantity(int quantity);
}

public class Order {
    private int OrderID;
    private OrderStatus status;
    private Date creationTime;

    private Meal[] meals;
    private Table table;
    private Check check;
    private Waiter waiter;
    private Chef chef;

    public boolean addMeal(Meal meal);
    public boolean removeMeal(Meal meal);
    public OrderStatus getStatus();
    public boolean setStatus(OrderStatus status);
}