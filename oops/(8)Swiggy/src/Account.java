// For simplicity, we are not defining getter and setter functions. The reader can
// assume that all class attributes are private and accessed through their respective
// public getter methods and modified only through their public setter function.

public abstract class Account {
    private String id;
    private String password;
    private Address address;
    private AccountStatus status;

    public boolean resetPassword();
}

public abstract class Person extends Account {
    private String name;
    private String email;
    private String phone;
}


public class Employee extends Person {
    private int employeeID;
    private Date dateJoined;
}

public class Receptionist extends Employee {
    public boolean createReservation();
    public List<Customer> searchCustomer(String name);
}

public class Manager extends Employee {
    public boolean addEmployee();
}

public class Chef extends Employee {
    public boolean takeOrder();
}