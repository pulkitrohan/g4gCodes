public enum PaymentStatus{
    UNPAID,
    PENDING,
    COMPLETED,
    FILLED,
    DECLINED,
    CANCELLED,
    ABONDENED,
    SETTLING,
    SETTLED,
    REFUNDED
}
