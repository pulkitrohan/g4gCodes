public abstract class Customer {
    private ShoppingCart cart;
    private Order order;

    public ShoppingCart getShoppingCart();
    public bool addItemToCart(Item item);
    public bool removeItemFromCart(Item item);
}
