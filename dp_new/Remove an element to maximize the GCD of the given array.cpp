#include<bits/stdc++.h>
using namespace std;

//https://www.geeksforgeeks.org/remove-an-element-to-maximize-the-gcd-of-the-given-array/
int maxGCD(int A[], int N) {
	int prefix[N+2], suffix[N+2];

	prefix[1] = A[0];
	for(int i=2;i<=N;i++)
		prefix[i] = __gcd(prefix[i-1], A[i-1]);

	suffix[N] = A[N-1];
	for(int i=N-1;i>=1;i--)
		suffix[i] = __gcd(suffix[i+1], A[i-1]);

	int ans = max(suffix[2], prefix[N-1]);
	for(int i=2;i<N;i++)
		ans = max(ans, __gcd(prefix[i-1], suffix[i+1]));
	return ans;
}

int main(void) {
	int A[] = {14, 17, 28, 70};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<maxGCD(A, N);
	return 0;
}