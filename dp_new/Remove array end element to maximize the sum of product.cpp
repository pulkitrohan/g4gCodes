#include<bits/stdc++.h>
using namespace std;
#define vi vector<int> 
#define vii vector< vi >

int solve(int A[], vii &dp, int start, int end, int turn) {
	if(start == end)
		return A[start] * turn;
	if(dp[start][end] != 0)
		return dp[start][end];

	dp[start][end] = max(A[start] * turn + solve(A, dp, start + 1, end, turn + 1),
						 A[end] * turn + solve(A, dp, start, end - 1, turn + 1));

	return dp[start][end];
}

int main(void) {
	int A[] = { 1, 3, 1, 5, 2};
	int N = sizeof(A)/sizeof(A[0]);
	vii dp(N, vi(N, 0));
	cout<<solve(A, dp, 0, N-1, 1)<<endl;
	return 0;
}