/*
	Explanation: https://github.com/MathProgrammer/CodeForces/blob/master/Explanations/Explanations%20-%204/Tetrahedron%20-%20Explanation.txt
*/
#include<bits/stdc++.h>
using namespace std;

int countPaths(int N) {
	int prevD = 1, prevABC = 0;
	for(int i=1;i<=N;i++) {
		int curD = prevABC * 3;
		int curABC = (prevABC * 2 + prevD);
		prevABC = curABC;
		prevD = curD;
	}
	return prevD;
}

int main(void) {
	int N = 3;
	cout<<countPaths(N)<<endl;
	return 0;
}