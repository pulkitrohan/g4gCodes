#include<bits/stdc++.h>
using namespace std;
#define vii vector< vector<int> >
#define vi vector<int>

int totalWays(int N, int M, int X) {
	int dp[N+1][2];

	if(X == 1) {

		dp[0][0] = 1;
		dp[1][0] = 0;
		dp[1][1] = M-1;
	}

	else {
		dp[0][1] = 0;
		dp[1][0] = 1;
		dp[1][1] = M-2;
	}

	for(int i=2;i<N;i++) {
		dp[i][0] = dp[i-1][1];
		dp[i][1] = dp[i-1][0] * (M-1) + dp[i-1][1] * (M-2);
	}

	return dp[N-1][0];

}

int main(void) {
	int N = 4, M = 3, X = 2;
	cout<<totalWays(N, M, X);
	return 0;
}