#include<bits/stdc++.h>
using namespace std;

int minCubesCount(int k) {
	int dp[k+1];
	dp[0] = 1;
	for(int i=1;i<=k;i++) {
		int j = 1, t = 1;
		dp[i] = INT_MAX;
		while(j <= i) {
			if(i == j)
				dp[i] = 1;
			else if(dp[i] > dp[i-j])
				dp[i] = dp[i-j] + 1;
			t++;
			j = t * t * t;
		}
	}
	return dp[k];
}

int main(void) { 
	int num = 15;
	cout<<minCubesCount(num);
	return 0;
}