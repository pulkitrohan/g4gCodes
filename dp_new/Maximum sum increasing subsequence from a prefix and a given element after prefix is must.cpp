#include<bits/stdc++.h>
using namespace std;

int MISS(vector<int> &A, int N, int index, int K) {

	vector< vector<int> > dp(N, vector<int> (N, 0));

	for(int i=0;i<N;i++)
		dp[0][i] = (A[i] > A[0]) ? A[0] + A[i] : A[i];

	for(int i=1;i<N;i++) {
		for(int j=0;j<N;j++) {
			if(A[j] > A[i] && j > i) 
				dp[i][j] = max(dp[i-1][i] + A[j], dp[i-1][j]);
			else
				dp[i][j] = dp[i-1][j];
		}
	}
	return dp[index][K];
}

int main(void) {
	vector<int> A = { 1, 101, 2, 3, 100, 4, 5 };
	int index = 4, k = 6;
	cout<<MISS(A, A.size(), index, k);
	return 0;
}