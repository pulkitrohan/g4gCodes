#include<bits/stdc++.h>
using namespace std;
struct AL
{
    int data;
    struct AL *next;
};

void addEdge(vector<struct AL *> &G,int start,int end)
{
    struct AL *temp = (struct AL *)malloc(sizeof(struct AL));
    temp->data = end;
    temp->next = G[start];  //G[start] initially contains NULL
    G[start] = temp;    //Now G[start] contains value at temp
}

vector<int> TopologicalSort(vector<struct AL *> &G,int N)
{
    int indegree[N+1];
    memset(indegree,0,sizeof(indegree));
    vector<int> Sorted;
    queue<int> Q;

    for(int i=0;i<N;i++)
    {
        struct AL *temp = G[i];
        while(temp)
        {
            indegree[temp->data]++;
                temp = temp->next;
        }
    }

    for(int i = 0;i<N;i++)
        if(!indegree[i])
            Q.push(i);
    int k = 0,count = 0;
    while(!Q.empty())
    {
        int j = Q.front();
        Q.pop();
        Sorted.push_back(j);
        count++;
        struct AL *temp = G[j];
        while(temp)
        {

            indegree[temp->data]--;
            if(!indegree[temp->data])
                Q.push(temp->data);
            temp = temp->next;
        }
    }
    if(count != N) {
        printf("Graph has cycle\n");
        return {};
    }
    return Sorted;
}

int numberOfPaths(vector<struct AL *> &G, int source, int destination) {
    vector<int> sorted = TopologicalSort(G, G.size());
    for(int i=0;i<sorted.size();i++)
        cout<<sorted[i]<<" ";
    cout<<endl;
    vector<int> dp(G.size(), 0);
    dp[destination] = 1;
    for(int i=sorted.size()-1; i>=0; i--) {
        struct AL *temp = G[i];
        while(temp) {
            dp[sorted[i]] += dp[temp->data];
            temp = temp->next;
        }
    }
    return dp[source];
}

int main(void)
{
    int N = 5;
    vector<struct AL *> Graph(N, NULL);
    addEdge(Graph, 0, 1);
    addEdge(Graph, 0, 2);
    addEdge(Graph, 0, 3);
    addEdge(Graph, 0, 4);
    addEdge(Graph, 2, 3);
    addEdge(Graph, 3, 4);
    cout<<numberOfPaths(Graph, 0, 3);
    return 0;
}
