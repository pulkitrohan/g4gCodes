#include<bits/stdc++.h>
using namespace std;
#define vii vector<vector<int> >
#define ULL unsigned long long
#define LL long long
#define L long 

const int MAX = 30; 

int solve(string A, string B, int N, int M, int K, int dp[][MAX][MAX]) {

	if(K == 0)
		return 0;
	if(N < 0 || M < 0)
		return 1e9;
	if(dp[N][M][K] != -1)
		return dp[N][M][K];
	int cost = (A[N] - 'a') ^ (B[M] - 'a');
	return dp[N][M][K] = min({
		cost + solve(A, B, N-1, M-1, K-1, dp),
		solve(A, B, N-1, M, K, dp),
		solve(A, B, N, M-1, K, dp)
	});
	
}

int main(void) {
	string A = "abble";
	string B = "pie";
	int N = A.length();
	int M = B.length();
	int K = 2;
	int dp[MAX][MAX][MAX];
	memset(dp, -1, sizeof dp);
	int ans = solve(A, B, N-1, M-1, K, dp);
	cout<<(ans == 1e9 ? -1 : ans)<<endl;
    return 0;
}