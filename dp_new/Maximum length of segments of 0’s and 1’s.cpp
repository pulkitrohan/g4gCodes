#include<bits/stdc++.h>
using namespace std;
#define vb vector<bool>
#define pii pair<int,int>
#define vii vector< vector<int> >
#define vbb vector< vector<bool> >
#define MAX 10000

int find(string &S, int start,int N, vector<int> &dp) {
	if(start == N)
		return 0;
	if(dp[start] != -1)
		return dp[start];
	dp[start] = 0;
	int one = 0, zero = 0;
	for(int i=start;i<N;i++) {
		(S[i] == '1') ? one++ : zero++;
		dp[start] = (one > zero) ?
			max(dp[start], find(S, i+1, N, dp) + i-start+1) :
			max(dp[start], find(S, i+1, N, dp));
	}
	return dp[start];
}

int main(void) {
	string S = "100110001010001";
	int N = S.length();
	vector<int> dp(N+1, -1);
	cout<<find(S, 0, N, dp);
    return 0;
}
