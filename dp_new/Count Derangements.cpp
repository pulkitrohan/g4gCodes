#include<bits/stdc++.h>
using namespace std;

int countDer(int N) {
	int dp[N+1];
	dp[0] = 1;
	dp[1] = 0;
	dp[2] = 1;

	for(int i=3;i<=N;i++)
		dp[i] = (i-1) * (dp[i-1] + dp[i-2]);
	return dp[N];
}

int main(void) {
	int N = 4;
	cout<<countDer(N)<<endl;
	return 0;
}