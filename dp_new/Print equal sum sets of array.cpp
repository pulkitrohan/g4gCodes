#include<bits/stdc++.h>
using namespace std;

void printEqualSumSets(int A[], int N) {
	
	int sum = accumulate(A, A+N, 0);
	if(sum & 1) {
		cout<<"-1\n";
		return;
	}

	int k = sum >> 1;
	bool dp[N+1][k+1];

	for(int i=0;i<=k;i++)
		dp[0][i] = false;

	for(int i=0;i<=N;i++)
		dp[i][0] = true;

	for(int i=1;i<=N;i++)
		for(int curSum = 1; curSum <= k; curSum++) {
			dp[i][curSum] = dp[i-1][curSum];
			if(A[i-1] <= curSum) 
				dp[i][curSum] = dp[i][curSum] | dp[i-1][curSum-A[i-1]];
		}
	if(!dp[N][k]) {
		cout<<"-1\n";
		return;
	}
	vector<int> setA, setB;
	int i = N, curSum = k;
	while(i > 0 && curSum >= 0) {
		if(dp[i-1][curSum]) {
			i--;
			setB.push_back(A[i]);
		}
		else if(dp[i-1][curSum-A[i-1]]) {
			i--;
			curSum -= A[i];
			setA.push_back(A[i]);
		}

	}
	cout<<"Set 1 elements: ";
	for(int i=0;i<setA.size();i++)
		cout<<setA[i]<<" ";
	cout<<"\nSet 2 elements: ";
	for(int i=0;i<setB.size();i++)
		cout<<setB[i]<<" ";
}

int main(void) {
	int A[] = { 5, 5, 1, 11 };
	int N = sizeof(A)/sizeof(A[0]);
	printEqualSumSets(A, N);
	return 0;
}