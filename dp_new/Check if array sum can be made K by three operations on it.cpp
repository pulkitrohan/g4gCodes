#include<bits/stdc++.h>
using namespace std;
#define vii vector< vector<int> >
#define vi vector<int>

int isSumPossibleUtil(int A[], int N, int k,
	 int i, int currentSum, vii &dp) {
	if(currentSum <= 0)
		return false;

	if(i >= N) 
		return currentSum == k;

	if(dp[i][currentSum] != -1)
		return dp[i][currentSum];

	dp[i][currentSum] = 
		isSumPossibleUtil(A, N, k, i+1, currentSum - 2*A[i], dp) || 
		isSumPossibleUtil(A, N, k, i+1, currentSum, dp);

	dp[i][currentSum] = 
		isSumPossibleUtil(A, N, k, i+1, currentSum-i-1, dp) || dp[i][currentSum];

	dp[i][currentSum] = 
		isSumPossibleUtil(A, N, k, i+1, currentSum+i+1, dp) || dp[i][currentSum];

	return dp[i][currentSum];
}

bool isSumPossible(int A[], int N, int k) {

	int sum = 0;
	
	for(int i=0;i<N;i++)
		sum += A[i];
	vii dp(N+1, vi(sum+1, -1));
	return isSumPossibleUtil(A, N, k, 0, sum, dp) == 1;

}

int main(void) {
	int A[] = { 1, 2, 3, 4 };
	int N = sizeof(A)/sizeof(A[0]);
	int k = 5;

	cout<<isSumPossible(A, N, k);
	return 0;
}