#include<bits/stdc++.h>
using namespace std;

bool sumEqualToK(int A[], int &up, int &down, int N, int k) {

	unordered_map<int, int> um;
	int sum = 0, maxLen = 0;
	for(int i=0;i<N;i++) {
		sum += A[i];
		if(sum == k) {
			maxLen = i+1;
			up = 0, down = i;
		}
		else if(um.find(sum) == um.end())
			um[sum] = i;
		else if(um.find(sum-k) != um.end()) {
			int index = um[sum-k];
			if(maxLen < i-index) {
				maxLen = i-index;
				up = index+1;
				down = i;
			}
		}
	}
	return (maxLen != 0);
}

void sumZeroMatrix(int mat[][4], int row, int col, int k) {
	int temp[row];
	int up = -1, down = -1;
	int maxArea = 0;
	int finalUp = -1, finalDown = -1, finalLeft = -1, finalRight = -1;
	for(int left=0;left<col;left++) {
		memset(temp, 0, sizeof(temp));
		for(int right=left;right<col;right++) {
			for(int i=0;i<row;i++) 
				temp[i] += mat[i][right];

			bool isSumK = sumEqualToK(temp, up, down, row, k);
			int area = (down-up+1) * (right-left + 1);
			if(isSumK && area > maxArea) {
				maxArea = area;
				cout<<maxArea<<endl;
				finalUp = up;
				finalDown = down;
				finalLeft = left;
				finalRight = right;
			}
		}
	}
	cout<<"(Top, Left): ("<<finalUp<<", "<<finalLeft<<")\n";
	cout<<"(Bottom, Right:): ("<<finalDown<<", "<<finalRight<<")\n";
	cout<<"Area::"<< maxArea;
}

int main(void) {
	int mat[][4] = { { 1, 7, -6, 5 }, 
                     { -8, 6, 7, -2 }, 
                     { 10, -15, 3, 2 }, 
                     { -5, 2, 0, 9 } 
                 };
    int row = 4, col = 4;
    int k =7;
    sumZeroMatrix(mat, row, col, k);
	return 0;
}