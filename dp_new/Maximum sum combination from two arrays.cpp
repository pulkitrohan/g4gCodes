#include<bits/stdc++.h>
using namespace std;

//https://www.geeksforgeeks.org/maximum-sum-combination-from-two-arrays/
int maxSum(int A[],int B[], int N) {
	int dp[N][2];

	for(int i=0;i<N;i++) {
		if(i == 0) {
			dp[i][0] = A[i];
			dp[i][1] = B[i];
			continue;
		}
		dp[i][0] = max(dp[i-1][0], dp[i-1][1] + A[i]);
		dp[i][1] = max(dp[i-1][0] + B[i], dp[i-1][1]);
	}
	return max(dp[N-1][0], dp[N-1][1]);
}

int main(void) {
	int A[] = {9, 3, 5, 7, 3};
	int B[] = {5, 8, 1, 4, 5};

	int N = sizeof(A)/sizeof(A[0]);
	cout<<maxSum(A, B, N);


	return 0;
}