#include<bits/stdc++.h>
using namespace std;
#define vi vector<int> 
#define vii vector< vector<int> >
#define ULL unsigned long long
#define LL long long
#define L long 

int numberOfWays(string A, string B) {

	int N = A.length();
	int M = B.length();
	vii dpl(N+2, vi(M+2, 0));
	for(int i=1;i<=N;i++) {
		for(int j=1;j<=M;j++) {
			if(A[i-1] == B[j-1])
				dpl[i][j] = 1 + dpl[i-1][j-1];
			else
				dpl[i][j] = max(dpl[i-1][j], dpl[i][j-1]);
		}
	}

	int LCS = dpl[N][M];

	vii dpr(N+2, vi(M+2, 0));
	for(int i=N;i>0;i--) {
		for(int j=M;j>0;j--) {
			if(A[i-1] == B[j-1])
				dpr[i][j] = 1 + dpr[i+1][j+1];
			else
				dpr[i][j] = max(dpr[i+1][j], dpr[i][j+1]);
		}
	}

	vector<int> pos[256];
	for(int i = 0;i < M; i++)
		pos[B[i]].push_back(i+1);
		

	int ans = 0;
	for(int i = 0;i<=N;i++)
		for(int j=0;j<256;j++)
			for(auto x : pos[j])
				if(dpl[i][x-1] + dpr[i+1][x+1] == LCS) {
					ans++;
					break;
				}
					
	return ans;
}

int main(void) {
	string A = "aa", B = "baaa";
	cout<<numberOfWays(A, B);
    return 0;
}