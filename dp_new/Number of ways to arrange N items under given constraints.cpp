#include<bits/stdc++.h>
using namespace std;
#define vi vector<int> 
#define vii vector< vector<int> >
#define ULL unsigned long long
#define LL long long
#define L long 

int waysToArrange(int N, vi &k) {
	int K = k.size();
	vii C(N+1, vi(N+1, 1));
	for(int i=0;i<=N;i++) {
		for(int j=0;j<=i;j++) {
			if(i == 0 || j == i)
				C[i][j] = 1;
			else
				C[i][j] = C[i-1][j] + C[i-1][j-1];
		}
	}
	int dp[K];

	int count = 0;
	dp[0] = 1;

	for(int i=0;i<K;i++) {
		dp[i+1] = dp[i] * C[count+k[i]-1][k[i]-1];
		count += k[i];
	}
	return dp[K];
}

int main(void) {
	int N = 4;
	vi k = { 2, 2 };
    cout<<waysToArrange(N, k)<<endl;
    return 0;
}