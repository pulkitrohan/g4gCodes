#include<bits/stdc++.h>
using namespace std;
#define vii vector< vector<int> >
#define MAX 15000

int countTripletSum(int A[], int N) {
	vii dp(N, vector<int> (MAX+1, 0));
	for(int i=0;i<N;i++)
		for(int j=1;j<=MAX;j++) {
			if(i == 0)
				dp[i][j] = (j == A[i]);
			else
				dp[i][j] = dp[i-1][j] + (j == A[i]);
		}

	int ans = 0;
	for(int i=0;i<N-2;i++) {
		for(int j=i+1;j<N-1;j++) {
			for(int k=1;k<=24;k++) {
				int cube = k*k*k;
				int rem = cube - A[i] - A[j];
				if(rem > 0)
					ans += dp[N-1][rem] - dp[j][rem];
			}
		}
	}
	return ans;
}

int main(void) {
	int A[] = { 2, 5, 1, 20, 6 };
	int N = sizeof(A)/sizeof(A[0]);
	cout<<countTripletSum(A, N)<<endl;
	return 0;
}