#include<bits/stdc++.h>
using namespace std;
#define vi vector<int> 
#define vii vector< vi >

string LRS(string S) {

	int N = S.length();
	vii dp(N+1, vi(N+1, 0));
	for(int i = 1;i<=N;i++)
		for(int j=1;j<=N;j++)
			if(S[i-1] == S[j-1] && i != j)
				dp[i][j] = 1 + dp[i-1][j-1];
			else
				dp[i][j] = max(dp[i][j-1], dp[i-1][j]);

	cout<<"LRS::"<<dp[N][N]<<endl;
	string ans = "";
	int i = N, j = N;
	while(i > 0 && j > 0) {
		if(dp[i][j] == dp[i-1][j-1] + 1) {
			ans += S[i-1];
			i--,j--;
		}
		else if(dp[i][j] == dp[i-1][j])
			i--;
		else
			j--;
	}
	reverse(ans.begin(), ans.end());
	return ans;


}
int main(void) {
	string S = "AABEBCDD";
	cout<<LRS(S);
	return 0;
}