#include<bits/stdc++.h>
using namespace std;

int countStr(int N, int dp[][2][3], int bCount = 1, int cCount = 2) {
	if(bCount < 0 || cCount < 0) return 0;
	if(N == 0)	return 1;
	if(bCount == 0 && cCount == 0)	return 1;
	if(dp[N][bCount][cCount] != -1)	return dp[N][bCount][cCount];

	int res = countStr(N-1, dp, bCount, cCount);
	res += countStr(N-1, dp, bCount-1, cCount);
	res += countStr(N-1, dp, bCount, cCount-1);
	return dp[N][bCount][cCount] = res;
}

int main(void) {
	int N = 3;
	int dp[N+1][2][3];
	memset(dp, -1, sizeof(dp));
	cout<<countStr(N, dp);
}