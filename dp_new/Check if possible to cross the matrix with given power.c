#include<bits/stdc++.h>
using namespace std;
#define vi vector<int> 
#define vii vector< vector<int> >
#define vbb vector< vector<bool> >
#define ULL unsigned long long
#define LL long long
#define L long 

int maximumValue(vii &A, int K) {
	int N = A.size(), M = A[0].size();
	vii dp(N, vector<int>(M, 10000));
	for(int i=0;i<N;i++) {
		for(int j=0;j<M;j++) {
			if(i == 0 && j == 0)
				dp[i][j] = A[i][j];
			else if(i == 0)
				dp[i][j] = min(dp[i][j-1] + A[i][j], dp[i][j]);
			else if(j == 0)
				dp[i][j] = min(dp[i-1][j] + A[i][j], dp[i][j]);
			else
				dp[i][j] = min(dp[i-1][j-1], min(dp[i-1][j], dp[i][j-1])) + A[i][j];
		}
	}
	return (dp[N-1][M-1] <= K) ? dp[N-1][M-1] : -1;
}

int main(void) {
	vii A = { { 2, 3, 1 },
            { 6, 1, 9 },
            { 8, 2, 3 } };
	cout<<maximumValue(A, 7)<<endl;
    return 0;
}