#include<bits/stdc++.h>
using namespace std;

int minTime(int A[], int N) {
	if(N <= 0)
		return 0;
	int incl = A[0];
	int excl = 0;
	for(int i=1;i<N;i++) {

		int incl_new = min(excl, incl) + A[i];
		int excl_new = incl;

		incl = incl_new;
		excl = excl_new;
	}
	return min(incl, excl);
}

int main(void) {
	int A[] = {10, 5, 2, 7, 10};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<minTime(A, N)<<endl;
	return 0;
}