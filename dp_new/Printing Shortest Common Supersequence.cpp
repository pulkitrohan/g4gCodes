#include<bits/stdc++.h>
using namespace std;
#define MAX 100

string SCSS(string X, string Y) {
	int M = X.length();
	int N = Y.length();

	int dp[M+1][N+1];
	for(int i=0;i<=M;i++) {
		for(int j=0;j<=N;j++) {
			if(i == 0)
				dp[i][j] = j;
			else if(j == 0)
				dp[i][j] = i;
			else if(X[i-1] == Y[j-1])
				dp[i][j] = 1 + dp[i-1][j-1];
			else
				dp[i][j] = 1 + min(dp[i-1][j], dp[i][j-1]);
		}
	}
	int index = dp[M][N];
	string str;
	int i = M, j = N;
	while(i > 0 && j > 0) {

		if(X[i-1] == Y[j-1]) {
			str.push_back(X[i-1]);
			i--,j--,index--;
		}
		else if(dp[i-1][j] > dp[i][j-1]) {
			str.push_back(Y[j-1]);
			j--, index--;
		}
		else {
			str.push_back(X[i-1]);
			i--,index--;
		}
	}

	while(i > 0) {
		str.push_back(X[i-1]);
		i--, index--;
	}

	while(j > 0) {
		str.push_back(Y[j-1]);
		j--, index--;
	}

	reverse(str.begin(), str.end());
	return str;
}

int main(void)
{
    string X = "AGGTAB";
    string Y = "GXTXAYB";
	cout<<SCSS(X, Y);
	return 0;
}