#include<bits/stdc++.h>
using namespace std;

int longestSubsequence(int *A, int N) {
	vector<int> dp(N, 0);
	unordered_map<int, int> mp;

	int maxLen = INT_MIN;
	for(int i = 0; i < N; i++) {
		if(mp.find(A[i]-1) != mp.end()) 
			dp[i] = 1 + dp[mp[A[i]-1]];
		else
			dp[i] = 1;
		mp[A[i]] = i;
		maxLen = max(maxLen, dp[i]);
	}
	return maxLen;
}

int main(void) {
	int A[] = { 3, 10, 3, 11, 4, 5, 6, 7, 8, 12 };
	int N = sizeof(A)/sizeof(A[0]);
	cout<<longestSubsequence(A, N);
	return 0;
}