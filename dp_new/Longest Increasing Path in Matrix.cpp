#include<bits/stdc++.h>
using namespace std;
#define vi vector<int> 
#define vii vector< vector<int> >
#define ULL unsigned long long
#define LL long long
#define L long 

int LIP(vii &mat, vii &dp, int N, int M, int x, int y) {
	if(dp[x][y] != -1)
		return dp[x][y];

	if(x == N-1 && y == M-1)
		return dp[x][y] = 1;

	int result = 0;

	if(x == N-1 || y == M-1)
		result = 1;

	if(x+1 < N && mat[x][y] < mat[x+1][y])
		result = 1 + LIP(mat, dp, N, M, x+1, y);

	if(y+1 < M && mat[x][y] < mat[x][y+1])
		result = max(result, 1 + LIP(mat, dp, N, M, x, y+1));
	return dp[x][y] = result;
}

int main(void) {
	vii mat = {{ 1, 2, 3, 4 },
               { 2, 2, 3, 4 },
               { 3, 2, 3, 4 },
               { 4, 5, 6, 7 }};
    int N = 4, M = 4;
    vii dp(N, vi(M, -1));
    cout<<LIP(mat, dp, N, M, 0, 0);
    return 0;
}