#include<bits/stdc++.h>
using namespace std;

int minCost(int N, int P, int Q) {
	int ans = 0;
	while(N > 0) {
		if(N & 1) {
			ans += P;
			N--;
		}
		else {
			N = N/2;
			// Dividing by two and checking if subtracting N/2 times vs dividing
			ans += (N * P > Q) ? Q : N*P;
		}
	}
	return ans;
}

int main(void) {
	int N = 9, P = 5, Q = 1;
	cout<<minCost(N, P, Q);
	return 0;
}
