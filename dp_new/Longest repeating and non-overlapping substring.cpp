#include<bits/stdc++.h>
using namespace std;

string longestRepeatingSubstring(string S) {

	int N = S.length();
	int dp[N+1][N+1];
	memset(dp, 0, sizeof(dp));

	int resLength = 0;
	int index = 0;

	for(int i=1;i<=N;i++) {
		for(int j=i+1;j<=N;j++) {
			if(S[i-1] == S[j-1]) {
				dp[i][j] = 1 + dp[i-1][j-1];
				if(dp[i][j] > resLength) {
					resLength = dp[i][j];
					index = max(i, index);
				}
			}
		}
	}
	return S.substr(index-resLength, resLength);
}

int main(void){
	
	string S = "geeksforgeeks";
	cout<<longestRepeatingSubstring(S);
	return 0;
}