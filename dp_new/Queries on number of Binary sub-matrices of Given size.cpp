#include<bits/stdc++.h>
using namespace std;
#define vii vector< vector<int> > 

void solveQuery(vii mat, int N, int A[], int binary[]) {
	int maxLen = 1;
	vector< vector<int> > dp(mat.size(), vector<int> (mat[0].size(), 1));
	for(int i = 0; i < mat.size(); i++) {
		for(int j = 0; j < mat[i].size(); j++) {
			if(i == 0 || j == 0) 
				dp[i][j] = 1;
			else if(mat[i][j] == mat[i-1][j] && 
					mat[i][j] == mat[i][j-1] &&
					mat[i][j] == mat[i-1][j-1]) {
				dp[i][j] = min(dp[i-1][j], min(dp[i][j-1], dp[i-1][j-1])) + 1;
				maxLen = max(maxLen, dp[i][j]);
			}
			else
				dp[i][j] = 1;
		}
	}
	vector<int> freq0(maxLen, 0), freq1(maxLen, 0);
	for(int i=0; i < mat.size(); i++) {
		for(int j = 0; j < mat[i].size(); j++) {
			if(mat[i][j] == 0)
				freq0[dp[i][j]]++;
			else
				freq1[dp[i][j]]++;
		}
	}
	for(int i = maxLen-1; i >= 0; i--) {
		freq0[i] += freq0[i+1];
		freq1[i] += freq1[i+1];
	}
	for(int i = 0;i < N; i++) {
		if (binary[i] == 0)
            		cout << freq0[A[i]] << endl;
		 else
            		cout << freq1[A[i]] << endl;
	}
}

int main(void) {
	vii mat = {
        { 0, 0, 1, 1 },
        { 0, 0, 1, 0 },
        { 0, 1, 1, 1 },
        { 1, 1, 1, 1 },
        { 0, 1, 1, 1 }
    };
    int N = 2;
    int A[] = { 2, 2 };
    int binary[] = { 1, 0 };
    solveQuery(mat, N, A, binary);
	return 0;
}