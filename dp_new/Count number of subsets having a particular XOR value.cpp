#include<bits/stdc++.h>
using namespace std;

int subsetXor(int A[], int N, int K) {
	int maxElement = A[0];
	for(int i=1;i<N;i++)
		if(A[i] > maxElement)
			maxElement = A[i];
	int M = (1 << (int)(log2(maxElement) + 1)) - 1;
	int dp[N+1][M+1];
	memset(dp, 0, sizeof(dp));
	dp[0][0] = 1;
	for(int i=1;i<=N;i++)
		for(int j=0;j<=M;j++)
			dp[i][j] = dp[i-1][j] + dp[i-1][j^A[i-1]];
	return dp[N][K];
}

int main(void) {
	int A[] = {1,2,3,4,5};
	int k = 4;
	int N = sizeof(A)/sizeof(A[0]);
	cout<<subsetXor(A, N, k)<<endl;
	return 0;
}