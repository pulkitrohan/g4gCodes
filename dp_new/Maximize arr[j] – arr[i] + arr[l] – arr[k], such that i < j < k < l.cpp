#include<bits/stdc++.h>
using namespace std;

int findMaxVaue(int A[], int N) {
	vector<int> table1(N+1, -1);
	vector<int> table2(N, -1);
	vector<int> table3(N-1, -1);
	vector<int> table4(N-2, -1);

	for(int i=N-1;i>=0;i--)
		table1[i] = max(table1[i+1], A[i]);

	for(int i=N-2;i>=0;i--)
		table2[i] = max(table2[i+1], table1[i+1] - A[i]);

	for(int i=N-3;i>=0;i--)
		table3[i] = max(table3[i+1], table2[i+1] + A[i]);

	for(int i=N-4;i>=0;i--)
		table4[i] = max(table4[i+1], table3[i+1] - A[i]);

	return table4[0];
}

int main(void)
{
	int A[] = {4,8,9,2,20};
	int N = sizeof(A)/sizeof(A[0]);
	cout<<findMaxVaue(A,N);
	return 0;
}