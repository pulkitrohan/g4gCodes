#include<bits/stdc++.h>
using namespace std;
#define vii vector< vector<int> >

void printAlignedStrings(string A, string B, vii dp, int pxy, int pgap) {

	int M = A.length(), N = B.length();
	int l = M + N;
	int i = M, j = N;
	string alignedStringA = "", alignedStringB = "";
	while(i != 0 && j != 0) {
		if(A[i-1] == B[j-1]) {
			alignedStringA = A[i-1] + alignedStringA;
			alignedStringB = B[j-1] + alignedStringB;
			i--,j--;
		}
		else if(dp[i-1][j-1] + pxy == dp[i][j]) {
			alignedStringA = A[i-1] + alignedStringA;
			alignedStringB = B[j-1] + alignedStringB;
			i--,j--;
		}
		else if(dp[i-1][j] + pgap == dp[i][j]) {
			alignedStringA = A[i-1] + alignedStringA;
			alignedStringB = "_" + alignedStringB;
			i--;
		}
		else if(dp[i][j-1] + pgap == dp[i][j]) {
			alignedStringA = "_" + alignedStringA;
			alignedStringB = B[j-1] + alignedStringB;
			j--;
		}
	}

	while(alignedStringA.length() != l) {
		alignedStringA = i > 0 ? A[--i] + alignedStringA : '_' + alignedStringA;
	}

	while(alignedStringB.length() != l) {
		alignedStringB = i > 0 ? B[--j] + alignedStringB : '_' + alignedStringB;
	}

	int id = l;
	for(int i=l;i>=1;i--) {
		if(alignedStringA[i] == '_' && alignedStringB[i] == '_') {
			id = i+1;
			break;
		}

	}
	cout<<alignedStringA.substr(id).c_str()<<endl;
	cout<<alignedStringB.substr(id).c_str()<<endl;
}

void getMinimumPenalty(string A, string B, int pxy, int pgap) {

	int M = A.length();
	int N = B.length();

	vii dp(N+M+1, vector<int>(N+M+1, 0));

	for(int i=0;i<=N+M;i++) {
		dp[i][0] = i * pgap;
		dp[0][i] = i * pgap;
	}

	for(int i=1;i<=M;i++) {
		for(int j=1;j<=N;j++) {
			if(A[i-1] == B[j-1])
				dp[i][j] = dp[i-1][j-1];
			else
				dp[i][j] = min(
					dp[i-1][j-1] + pxy, 
					min(
						dp[i-1][j] + pgap, 
						dp[i][j-1] + pgap
					)
				);
		}
	}
	cout<<"Minimum Penalty: "<<dp[M][N]<<endl;;
	printAlignedStrings(A, B, dp, pxy, pgap);
}

int main(void) {
	string A = "AGGGCT";
	string B = "AGGCA";
	int misMatchPenalty = 3, gapPenalty = 2;
	getMinimumPenalty(A, B, misMatchPenalty, gapPenalty);
	return 0;
}