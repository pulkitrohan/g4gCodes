#include<bits/stdc++.h>
using namespace std;

void print(vector<int> &V, int N) {
	for(int i=0;i<N;i++)
		cout<<V[i]<<" ";
}

void printMaxSumIS(int A[], int N) {
	vector< vector<int> > LIS(N);
	LIS[0].push_back(A[0]);
	for(int i=1;i<N;i++) {
		for(int j=0;j<i;j++) {
			if(A[i] > A[j] && LIS[i].size() < LIS[j].size())
				LIS[i] = LIS[j]; 
		}
		LIS[i].push_back(A[i]);
	}


	vector< vector<int> > LDS(N);
	LDS[N-1].push_back(A[N-1]);
	for(int i=N-2;i>=0;i--) {
		for(int j=N-1;j>i;j--) {
		if(A[i] > A[j] && LDS[i].size() < LDS[j].size())
				LDS[i] = LDS[j]; 
		}
		LDS[i].push_back(A[i]);
	}

	for(int i=0;i<N;i++)
		reverse(LDS[i].begin(), LDS[i].end());

	int maxLen = 0;
	int maxIndex = -1;

	for(int i=0;i<N;i++) {
		if(LIS[i].size() + LDS[i].size() - 1 > maxLen) {
			maxLen = LIS[i].size() + LDS[i].size() - 1;
			maxIndex = i;
		}
	}
	print(LIS[maxIndex], LIS[maxIndex].size() - 1);
	print(LDS[maxIndex], LDS[maxIndex].size());
}

int main(void)
{
	int A[] = {1, 11, 2, 10, 4, 5, 2, 1};
	int N = sizeof(A)/sizeof(A[0]);
	printMaxSumIS(A,N);
	return 0;
}