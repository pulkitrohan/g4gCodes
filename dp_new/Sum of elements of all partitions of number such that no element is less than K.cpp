#include<bits/stdc++.h>
using namespace std;
#define vii vector< vector<int> >

int countPartitions(int N, int K, vii dp) {

    if(dp[N][K] != -1)
        return dp[N][K];
    if(K > N)
        return 0;
    if(2*K > N)
        return 1;
    int ans = 1;
    for(int i=K;i<N;i++)
        ans += countPartitions(N-i, i, dp);
    return (dp[N][K] = ans);
}

int main(void)
{
    int N = 10, K = 3;
    vii dp(N+1, vector<int>(N+1, -1));
    cout<<countPartitions(N, K, dp) * N;
    return 0;
}
