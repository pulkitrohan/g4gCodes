#include<bits/stdc++.h>
using namespace std;

int findMaxSumUtil(int A[], int start, int end) {

    int incl = A[0], excl = 0, excl_new;

    for(int i=start;i<=end;i++) {
        excl_new = max(incl, excl);
        incl = excl + A[i];
        excl = excl_new;
    }
    return max(incl, excl);
}

int findMaxSum(int A[], int N) {
    return max(findMaxSumUtil(A, 0, N-2), findMaxSumUtil(A, 1, N-1));
}

int main(void)
{
    int A[] = {1, 2, 3, 4, 5, 1};
    int N = sizeof(A)/sizeof(A[0]);
    cout<<findMaxSum(A, N);
    return 0;
}
