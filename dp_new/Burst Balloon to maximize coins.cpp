#include<bits/stdc++.h>
using namespace std;
#define vb vector<bool>
#define pii pair<int,int>
#define vii vector< vector<int> >
#define vbb vector< vector<bool> >
#define MAX 10000

int dp[MAX][MAX];

int getMaxScore(int A[], int l, int r, int N) {

	if(dp[l][r] != -1)
		return dp[l][r];
	dp[l][r] = 0;
	for(int i =l+1;i<r;i++) {
		int val = getMaxScore(A, l, i, N) + getMaxScore(A, i+1, r, N);
		if(l == 0 && r == N)
			val += A[i];
		else
			val += A[l]*A[i]*A[r];
		dp[l][r] = max(dp[l][r], val);
	}
	return dp[l][r];

}


int main(void) {
	int N = 4;
	int A[] = { 1, 1, 2, 3, 4, 1};
	memset(dp, -1, sizeof(dp));
	cout<<getMaxScore(A, 0, N+1, N+1);
    return 0;
}
