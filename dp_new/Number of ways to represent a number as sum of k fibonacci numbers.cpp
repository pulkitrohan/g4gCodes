#include<bits/stdc++.h>
using namespace std;

vector<int> fibonacci(void) {
    vector<int> fibonacciList;
    fibonacciList.push_back(1);
    fibonacciList.push_back(2);
    for(int i=2;i<43;i++) 
        fibonacciList.push_back(fibonacciList[i-1] + fibonacciList[i-2]);
    return fibonacciList;
}

int rec(int N, int K, vector<int> &fibonacciList, int last) {
    if(K == 0)
        return (N == 0) ? 1 : 0;
    int count = 0;
    for(int i=last;i>=0 && fibonacciList[i] * K >= N;i--) {
        if(fibonacciList[i] > N)
            continue;
        count += rec(N-fibonacciList[i], K-1, fibonacciList, i);
    }
    return count;
}

int main(void)
{
    vector<int> fibonacciList = fibonacci();
    int N = 13, K = 3;
    cout<<rec(N, K, fibonacciList, fibonacciList.size()-1);
    return 0;
}
