#include<bits/stdc++.h>
using namespace std;

int minFallingSum(vector< vector<int> > &V) {
	int N = V.size();
	int M = V[0].size();
	for(int i=N-2;i>=0;i--) {
		for(int j=0;j<N;j++) {
			int val = V[i+1][j];
			if(j > 0)
				val = min(val, V[i+1][j-1]);
			if(j < N-1)
				val = min(val, V[i+1][j+1]);
			V[i][j] += val;
		}
	}
	return *min_element(V[0].begin(), V[0].end());
}

int main(void) {
	vector< vector<int> > V = { {1,2,3},
			  {4,5,6},
			  {7,8,9}	
	};
	cout<<minFallingSum(V);
	return 0;
}
