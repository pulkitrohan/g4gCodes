#include<bits/stdc++.h>
using namespace std;

bool isPowerOf(int val, int base) {
	
	while(val > 1) {
		if(val%base != 0)
			return false;
		val /= base;
	}
	return true;
}

int numberOfPartitions(string A) {
	
	int N = A.length();
	int dp[N+2];
	dp[N-1] = (A[0]-'0') == 0 ? -1 : 1;

	for(int i=N-2;i>=0;i--) {

		int val = 0;
		if((A[i] - '0') == 0) {
			dp[i] = -1;
			continue;
		}

		dp[i] = INT_MAX;

		for(int j=i;j<N;j++) {
			val = val * 2 + (A[j]-'0');
			if(isPowerOf(val, 4) || isPowerOf(val, 6)) {
				if(j == N-1)
					dp[i] = 1;
				else if(dp[j+1] != -1) 
					dp[i] = min(dp[i], 1 + dp[j+1]);
			}
		}

		if(dp[i] == INT_MAX)
			dp[i] = -1;
	}

	return dp[0];

}

int main(void) {
	string A = "100110110";
	cout<<numberOfPartitions(A)<<endl;
}