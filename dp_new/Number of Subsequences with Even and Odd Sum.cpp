#include<bits/stdc++.h>
using namespace std;

pair<int, int> countSum(int A[], int N) {
	int countOdd[N+1], countEven[N+1];
	countEven[0] = countOdd[0] = 0;

	for(int i=1;i<=N;i++) {
		if(A[i-1]%2 == 0) {
			countEven[i] = countEven[i-1] + countEven[i-1] + 1;
			countOdd[i] = countOdd[i-1] + countOdd[i-1];
		} else {
			countEven[i] = countOdd[i-1] + countEven[i-1];
			countOdd[i] = countEven[i-1] + countOdd[i-1] + 1;
		}
	}
	return { countEven[N], countOdd[N] };
}

int main(void) {
	int A[] = {1, 2, 2, 3};
	int N = sizeof(A)/sizeof(A[0]);
	pair<int, int> ans = countSum(A, N);
	cout<<"Even Sum Subsequences = " << ans.first<<endl;
	cout<<"Odd Sum Subsequences = " << ans.second<<endl;

	return 0;
}