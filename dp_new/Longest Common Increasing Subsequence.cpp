#include<bits/stdc++.h>
using namespace std;

int LCIS(int A[], int N, int B[], int M) {
	int dp[M], parent[M];
	memset(dp, 0, sizeof(dp));
	for(int i=0;i<N;i++) {
		int current = 0, last = -1;
		for(int j=0;j<M;j++) {
			if(A[i] == B[j] && dp[j] < 1 + current) {
				parent[j] = last;
				dp[j] = 1 + current;
			}
			else if(A[i] > B[j] && current < dp[j]) {
				current = dp[j];
				last = j;
			}
		}
	}
	int ans = 0, index = -1;
	for(int i=0;i<M;i++) {
		if(dp[i] > ans) {
			ans = dp[i];
			index = i;
		}
	}
	int lcis[ans];
	for(int i=0;index != -1;i++) {
		lcis[i] = B[index];
		index = parent[index];
	}
	for(int i=ans-1;i>=0;i--)
		cout<<lcis[i]<<" ";
	return ans;
}

int main(void)
{
    int arr1[] = {3, 4, 9, 1};
    int arr2[] = {5, 3, 8, 9, 10, 2, 1};
 
    int n = sizeof(arr1)/sizeof(arr1[0]);
    int m = sizeof(arr2)/sizeof(arr2[0]);
 
    cout << "\nLength of LCIS is "
         << LCIS(arr1, n, arr2, m);
	return 0;
}