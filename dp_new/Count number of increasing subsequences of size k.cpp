#include<bits/stdc++.h>
using namespace std;
#define vi vector<int> 
#define vii vector< vector<int> >
#define ULL unsigned long long
#define LL long long

int numberOfWays(vi &A, int K) {
	int N = A.size();
	vii dp(K+1, vi(N+1, 0));
	for(int i=0;i<N;i++)
		dp[0][i] = 1;
	for(int L=1;L<K;L++) {
		for(int i=L;i<N;i++) {
			dp[L][i] = 0;
			for(int j=L-1;j<i;j++)
				if(A[j] < A[i])
					dp[L][i] += dp[L-1][j];
		}
	}
	int ans = 0;
	for(int i=K-1;i<N;i++)
		ans += dp[K-1][i];
	return ans;
}

int main(void) {
	vi V = {12,8,11,13,10,15,14,16,20};
	int K = 4;
	cout<<numberOfWays(V, K);
    return 0;
}