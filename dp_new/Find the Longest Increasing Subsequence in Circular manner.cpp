#include<bits/stdc++.h>
using namespace std;

int LICSUtil(int arr[], int start, int end) {
	vector<int> dp(end-start+1, 1);
	for(int i = start+1; i < end; i++) {
		for(int j = start; j < i; j++) 
			if(arr[i] > arr[j])
				dp[i] = max(dp[i], 1 + dp[j]);
	}

	int ans = INT_MIN;
	for(int i = start; i < end; i++)
		ans = max(ans, dp[i]);
	cout<<ans<<endl;
	return ans;
}

int LICS(int arr[], int N) {
	int circBuff[2*N];
	for(int i=0;i<N;i++) 
		circBuff[i] = arr[i];
	for(int i=N;i < 2*N;i++) 
		circBuff[i] = arr[i-N];

	int ans = INT_MIN;
	for(int i = 0;i < N; i++)
		ans = max(ans, LICSUtil(circBuff, i, i + N));
	return ans;
}

int main(void) {
	int arr[] = { 1, 4, 6, 2, 3 };
	int N = sizeof(arr)/sizeof(arr[0]);
	cout<<LICS(arr, N);
	return 0;
}