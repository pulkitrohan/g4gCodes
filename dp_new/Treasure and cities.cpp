#include<bits/stdc++.h>
using namespace std;
const int MAX = 1001;
int dp[MAX][MAX];

int maxProfit(int treasure[], int color[], int N, int i, int col, int A, int B) {
	if(i == N)
		return dp[i][col] = 0;
	if(dp[i][col] != -1)
		return dp[i][col];
	int sum = 0;

	if(col == color[i]) 
		sum += max(
			A * treasure[i] + maxProfit(treasure, color, N, i+1, color[i], A, B),
			maxProfit(treasure, color, N, i+1, col, A, B)
			);
	else
		sum += max(
			B * treasure[i] + maxProfit(treasure, color, N, i+1, color[i], A, B),
			maxProfit(treasure, color, N, i+1, col, A, B)
			);
	return dp[i][col] = sum;
}

int main(void) {
	int A = -5, B = 7;
	int treasure[] = {4,8,2,9};
	int color[] = {2,2,6,2};
	int N = sizeof(color)/sizeof(color[0]);
	memset(dp, -1, sizeof dp);
	cout<<maxProfit(treasure, color, N, 0, 0, A, B);
	return 0;
}
