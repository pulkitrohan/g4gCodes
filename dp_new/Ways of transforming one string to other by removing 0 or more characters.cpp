#include<bits/stdc++.h>
using namespace std;
#define vii vector< vector<int> >

int countTransformations(string A, string B) {
	int N = A.length();
	int M = B.length();
	if(M == 0)
		return 1;

	vii dp(M+1, vector<int> (N+1, 0));
	for(int i=0;i<M;i++) {
		for(int j=0;j<N;j++) {
			if(i == 0) {
				if(B[i] == A[j] && j == 0)
					dp[i][j] = 1;
				else if(B[i] == A[j])
					dp[i][j] = dp[i][j-1] + 1;
				else 
					dp[i][j] = dp[i][j-1];
			}
			else {
				if(B[i] == A[j])
					dp[i][j] = dp[i][j-1] + dp[i-1][j-1];
				else
					dp[i][j] = dp[i][j-1];
			}
		}
	}
	return dp[M-1][N-1];
}

int main(void) {
	string A = "abcccdf", B = "abccdf";
	cout<<countTransformations(A, B)<<endl;
	return 0;
}