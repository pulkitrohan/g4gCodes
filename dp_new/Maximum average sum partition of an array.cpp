#include<bits/stdc++.h>
using namespace std;
#define vii vector<vector<double> >
#define vi vector<int>

double score(vi &A, vii &dp, int N, int K) {
	if(dp[N][K] > 0)
		return dp[N][K];
	double sum = 0;
	for(int i = N-1;i > 0;i--) {
		sum += A[i];
		//dp(N,K) = max(dp(N,K), score(A, dp, i, K-1) + average(i, j)) for all i from N-1 to 1
		dp[N][K] = max(dp[N][K], score(A, dp, i, K-1) + sum/(N-i));
	}
	return dp[N][K];
}

double longestSumOfAverages(vector<int> &A, int K) {
	double sum = 0;
	vii dp(A.size()+1, vector<double> (K+1, 0));
	for(int i = 0; i < A.size(); i++) {
		sum += A[i];
		dp[i+1][1] = sum/(i+1);
	}
	return score(A, dp, A.size(), K);
}

int main(void) {
	vector<int> A = { 9, 1, 2, 3, 9 };
	int K = 3;
	cout<<longestSumOfAverages(A, K);
	return 0;
}