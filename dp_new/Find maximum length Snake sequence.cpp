#include<bits/stdc++.h>
using namespace std;
#define M 4
#define N 4

vector< pair<int, int> > findPath(int Mat[M][N], int dp[M][N], int i, int j) {
	vector< pair<int, int> > path;
	path.push_back(make_pair(i, j));
	while(dp[i][j] != 0) {
		if(i > 0 && dp[i-1][j] + 1 == dp[i][j]) {
			path.push_back(make_pair(i-1, j));
			i--;
		}
		else if(j > 0 && dp[i][j-1] + 1 == dp[i][j]) {
			path.push_back(make_pair(i, j-1));
			j--;
		}
	}
	return path;
}

void findSnakeSequence(int Mat[M][N]) {
	int dp[M][N];
	memset(dp, 0, sizeof(dp));
	int maxLen = 0, maxRow = 0, maxCol = 0;
	
	for(int i=0;i<M;i++) {
		for(int j=0;j<N;j++) {
			if(i || j) {

				if(i > 0 && abs(Mat[i-1][j]-Mat[i][j]) == 1) {
					dp[i][j] = max(dp[i][j], dp[i-1][j] + 1);
					if(maxLen < dp[i][j]) {
						maxLen = dp[i][j];
						maxRow = i, maxCol = j;
					}
				}


				if(j > 0 && abs(Mat[i][j-1]-Mat[i][j]) == 1) {
					dp[i][j] = max(dp[i][j], dp[i][j-1] + 1);
					if(maxLen < dp[i][j]) {
						maxLen = dp[i][j];
						maxRow = i, maxCol = j;
					}
				}

			}
		}
	}
	cout<<"maxLen::"<<maxLen<<endl;
	vector< pair<int, int> > path = findPath(Mat, dp, maxRow, maxCol);
	for(auto it = path.end() -1; it != path.begin();it--) {
		cout<<Mat[it->first][it->second]<<"("<<it->first<<","<<it->second<<")"<<endl;
	}
}

int main(void)
{
	int mat[M][N] =
    {
        {9, 6, 5, 2},
        {8, 7, 6, 5},
        {7, 3, 1, 6},
        {1, 1, 1, 7},
    };
 
    findSnakeSequence(mat);
    return 0;
}