#include<bits/stdc++.h>
using namespace std;
#define MAX_VAL 100000000
#define vii vector< vector<int> >
#define vp vector<pair<int, float> >

double findProbability(vector<vp> &G, int N, int start, int end, int T) {
	vector< vector<float> > dp(N+1, vector<float>(T+1, 0));
	dp[start][0] = 1.0;

	for(int i=1;i<=T;i++) {
		for(int j=1;j<=N;j++)
			for(auto k : G[j])
				dp[j][i] += k.second * dp[k.first][i-1];
	}
	return dp[end][T];
}

int main(void) {

	vector<vp> G(7); 
    G[1] = vp({ { 2, 0.09 } }); 
    G[2] = vp({ { 1, 0.23 }, { 6, 0.62 } }); 
    G[3] = vp({ { 2, 0.06 } }); 
    G[4] = vp({ { 1, 0.77 }, { 3, 0.63 } }); 
    G[5] = vp({ { 4, 0.65 }, { 6, 0.38 } }); 
    G[6] = vp({ { 2, 0.85 }, { 3, 0.37 }, { 4, 0.35 }, { 5, 1.0 } }); 
  
    // N is the number of states 
    int N = 6; 
  
    int S = 4, F = 2, T = 100; 
  
    cout << "The probability of reaching " << F 
         << " at time " << T << " \nafter starting from "
         << S << " is " << findProbability(G, N, F, S, T); 	return 0;
}
