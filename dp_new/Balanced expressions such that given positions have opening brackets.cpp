#include<bits/stdc++.h>
using namespace std;
#define vii vector< vector<int> >

int countExpressions(int index, int open, int N, vii &dp, int adj[]) {
	
	if(open < 0)
		return 0;

	if(index == N) {
		return (open == 0) ? 1 : 0;
	}

	if(dp[index][open] != -1)
		return dp[index][open];

	dp[index][open] = adj[index] == 1 ? 
	countExpressions(index+1, open+1, N, dp, adj) : 
	countExpressions(index+1, open+1, N, dp, adj) + countExpressions(index+1, open-1, N, dp, adj);

	return dp[index][open];

}

int main(void) {
	int adj[4] = {1, 0, 0, 0};
	int N = sizeof(adj)/sizeof(int);
	vii dp(N, vector<int>(N, -1));
	cout<<countExpressions(0, 0, N, dp, adj);
	return 0;
}