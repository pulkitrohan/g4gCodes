#include<bits/stdc++.h>
using namespace std;
#define vii vector< vector<int> >
#define vi vector<int>

int sumSubSequenceCount(vi &V, int M) {

	int sum = 0;
	for(int num : V)
		sum += num;
	int N = V.size();

	vii dp(N+1, vi(sum+1, 0));

	for(int i=0;i<=N;i++)
		dp[i][0]++;

	for(int i=1;i<=N;i++) {
		dp[i][V[i-1]]++;
		for(int j=1;j<=sum;j++) {
			if(dp[i-1][j] > 0) {
				dp[i][j]++;
				dp[i][j+V[i-1]]++;
			}
		}
	}
	
	int count = 0;
	for(int i=1;i<=sum;i++) {
		if(dp[N][i] > 0 && i%M == 0)
			count += dp[N][i];
	}

	return count;
}

int main(void) {
	vector<int> V{1, 2, 3};
	int M = 3;
	cout<<sumSubSequenceCount(V, M)<<endl;
	return 0;
}