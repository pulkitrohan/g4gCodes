#include<bits/stdc++.h>
using namespace std;
#define LL long long
#define MOD 1000000007

LL countBT(int h) {
	LL dp[h+1];
	dp[0] = dp[1] = 1;
	for(int i = 2; i <= h; i++) {
		dp[i] = (dp[i-1]* ((2*dp[i-2])%MOD + dp[i-1]%MOD)%MOD)%MOD;
	}
	return dp[h];
}

int main(void) {
	int h = 3;
	cout<<countBT(h)<<endl;
}