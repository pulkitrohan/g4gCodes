#include<bits/stdc++.h>
using namespace std;
#define vi vector<int> 
#define vii vector< vector<int> >
#define ULL unsigned long long
#define LL long long
#define L long 

void longestAlternating(vi &A) {
	int count = 1;
	int N = A.size();
	int prev = A[0];
	for(int i=1;i<N;i++) {
		if(prev * A[i] >= 0) {
			while(count)
				cout<<count--<<" ";
		}
		count++;
		prev = A[i];
	}
		while(count)
				cout<<count--<<" ";
}

int main(void) {
	vi A = { -5, -1, -1, 2, -2, -3 };
	longestAlternating(A);
    return 0;
}