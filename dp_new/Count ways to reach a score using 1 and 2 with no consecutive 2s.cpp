#include<bits/stdc++.h>
using namespace std;
const int MAX = 101;
int dp[MAX][2];

int countWays(int N, bool flag) {
	if(dp[N][flag] != -1)
		return dp[N][flag];
	if(N == 0)
		return dp[N][flag] = 1;
	int sum = 0;
	if(!flag && N > 1)
		sum += countWays(N-1, false) + countWays(N-2, true);
	else
		sum += countWays(N-1, false);
	return dp[N][flag] = sum;
}

int main(void) {
	int N = 5;
	memset(dp, -1, sizeof dp);
	cout<<countWays(N, false);
	return 0;
}
