#include<bits/stdc++.h>
using namespace std;

int commonSubsequenceCount(string S, string T) {
	int N = S.length();
	int M = T.length();
	int dp[N+1][M+1];
	memset(dp, 0, sizeof(dp));

	for(int i=1;i<=N;i++) {
		for(int j=1;j<=M;j++) {
			if(S[i-1] == T[j-1])
				dp[i][j] = 1 + dp[i-1][j] + dp[i][j-1];
			else
				dp[i][j] = dp[i-1][j] + dp[i][j-1] - dp[i-1][j-1];
		}
	}
	return dp[N][M];
}

int main(void) {
	string S = "ajblqcpdz";
	string T = "aefcnbtdi";
	cout<<commonSubsequenceCount(S, T)<<endl;
	return 0;
}