#include<bits/stdc++.h>
using namespace std;
#define vb vector<bool>
#define pii pair<int,int>
#define vii vector< vector<int> >
#define vbb vector< vector<bool> >
#define MAX 10000

vb sieve() {
	vb prime(MAX, true);
	for(int i=2;i*i<=MAX;i++) {
		if(prime[i])
			for(int j=i*i;j<=MAX;j+=i)
				prime[j] = false;
	}
	return prime;
}

vii preprocessMatrix(vii &V) {
	vb prime = sieve();
	vii mappedMatrix(V.size(),vector<int>(V[0].size(), 0));
	for(int i=0;i<V.size();i++) {
		for(int j=0;j<V[i].size();j++) {
			int val = prime[V[i][j]];
			mappedMatrix[i][j] = val;
		}
	}
	return mappedMatrix;
}

void dfs(int i, int j, vector< pii> &ans, vii &mappedMatrix,
vbb &visited, int N,int M, int &size) {
	if(mappedMatrix[i][j] == 0 || i > N || j > M || visited[i][j] || size) 
		return;
	visited[i][j] = true;
	ans.push_back(make_pair(i, j));
	if(i == N && j == M) {
		size = ans.size();
		return;
	}
	dfs(i+1, j, ans, mappedMatrix, visited, N, M, size);
	dfs(i, j+1, ans, mappedMatrix, visited, N, M, size);
	dfs(i+1, j+1, ans, mappedMatrix, visited, N, M, size);
	ans.pop_back();
}

void lexicographicalPath(vii &mappedMatrix) {
	vector< pii > ans;
	int N = mappedMatrix.size(), M = mappedMatrix[0].size();
	vbb visited(N, vb(M, false));
	int size = 0;
	dfs(0, 0, ans, mappedMatrix, visited, N, M, size);
	for(auto p: ans)
		cout<<p.first<<" "<<p.second<<endl;
}

int countPrimePaths(vii &mappedMatrix) {
	int N = mappedMatrix.size(), M = mappedMatrix[0].size();
	vii dp(N,vector<int>(M, 0));

	for(int i=0;i<N;i++) {
		for(int j=0;j<M;j++) {
			if(mappedMatrix[i][j] == 0)
				dp[i][j] = 0;
			else if(i == 0 && j == 0)
				dp[i][j] = 1;
			else {
				if(i == 0)
					dp[i][j] = dp[i][j-1];
				else if(j == 0)
					dp[i][j] = dp[i-1][j];
				else
					dp[i][j] = dp[i-1][j] + dp[i][j-1] + dp[i-1][j-1];
			}
		}
	}
	return dp[N-1][M-1];
}


int main(void) {
    vii V = { { 2, 3, 7 }, 
              { 5, 4, 2 }, 
              { 3, 7, 11 }
            };
    vii mappedMatrix = preprocessMatrix(V);
    cout<<countPrimePaths(mappedMatrix);
    lexicographicalPath(mappedMatrix);
    return 0;
}
