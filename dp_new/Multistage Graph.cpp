#include<bits/stdc++.h>
using namespace std;
#define INF 100000
#define vi vector<int>
#define vii vector< vector<int> >

int shortestDistance(vii &graph) {
	int N = graph.size();
	vi dp(N, 0);
	for(int i=N-2;i>=0;i--) {
		dp[i] = INF;
		for(int j=i;j<N;j++) {
			if(graph[i][j] == INF)
				continue;
			dp[i] = min(dp[i], graph[i][j] + dp[j]);
		}
	}
	return dp[0];
}

int main(void) {
	vii graph = {{INF, 1, 2, 5, INF, INF, INF, INF}, 
       {INF, INF, INF, INF, 4, 11, INF, INF}, 
       {INF, INF, INF, INF, 9, 5, 16, INF}, 
       {INF, INF, INF, INF, INF, INF, 2, INF}, 
       {INF, INF, INF, INF, INF, INF, INF, 18}, 
       {INF, INF, INF, INF, INF, INF, INF, 13}, 
       {INF, INF, INF, INF, INF, INF, INF, 2}};
    cout<<shortestDistance(graph);
	return 0;
}