#include<bits/stdc++.h>
using namespace std;
#define vii vector<vector<int> >
#define ULL unsigned long long
#define LL long long
#define L long 
#define MAX 256

void precompute(string A, vii &prefix, vii &suffix) {
	int N = A.length();
	prefix[A[0]-'a'][0] = 1;

	for(int i=1;i<N;i++) {
		for(int j=0;j<MAX;j++)
			prefix[j][i] += prefix[j][i-1];
		prefix[A[i]-'a'][i]++;
	}

	suffix[A[N-1]-'a'][N-1] = 1;

	for(int i=N-2;i>=0;i--) {
		for(int j=0;j<MAX;j++)
			suffix[j][i] += suffix[j][i+1];
		suffix[A[i]-'a'][i]++;
	}
}

int countPalindrome(string A, int K) {
	int N = A.length();
	vii prefix(MAX, vector<int>(N+1, 0));
	vii suffix(MAX, vector<int>(N+1, 0));
	precompute(A, prefix, suffix);

	int ans = 0;
	if(K == 1) {
		for(int i=0;i<MAX;i++)
			ans += prefix[i][N-1];
		return ans;
	}

	if(K == 2) {
		for(int i=0;i<MAX;i++) 
			ans += (prefix[i][N-1] * (prefix[i][N-1]-1))/2;
		return ans;
	}

	for(int i=1;i<N-1;i++) 
		for(int j=0;j<MAX;j++)
			ans += prefix[j][i] * suffix[j][i+1];
	return ans;
}

int main(void) {
	string A = "aabab";
	int K = 2;
	cout<<countPalindrome(A, K);
    return 0;
}