#include<bits/stdc++.h>
using namespace std;
#define vi vector<int>

int find(vi &V, int M) {
	int N = V.size();
	int dp[N+1][N+1][N+1];
	vector<int> sweet(N+1, 0);
	for(int i=1;i<=M;i++)
		sweet[i] = V[i-1];
	for(int i=0;i<=M;i++) {
		for(int k=0;k<=M;k++)
			dp[i][0][k] = 0;
		for(int k=1;k<=N;k++)
			dp[i][k][0] = -1;
	}

	for(int i=0;i<=M;i++) {
		for(int j=1;j<=N;j++) {
			for(int k=1;k<=M;k++) {
				dp[i][j][k] = -1;
				if(i > 0 && j >= k && sweet[k] > 0 && dp[i-1][j-k][k] != -1)
					dp[i][j][k] = dp[i-1][j-k][k] + sweet[k];
				if(dp[i][j][k] == -1 || (dp[i][j][k-1] != -1 && dp[i][j][k-1] < dp[i][j][k]))
				dp[i][j][k] = dp[i][j][k-1];
			}
		}
	}
	return dp[M][N][M];
}

int main(void)
{
	int M = 3;
	vi V = {2, 1, 3, 0, 4, 10};
	cout<<find(V, M);
	return 0;
}
