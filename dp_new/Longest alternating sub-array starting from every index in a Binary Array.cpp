#include<bits/stdc++.h>
using namespace std;
#define vi vector<int> 
#define vii vector< vector<int> >
#define ULL unsigned long long
#define LL long long
#define L long 

int alternateSubarray(bool arr[], int N) {
	int count = 1;
	bool prev = arr[0];
	for(int i = 1;i < N;i ++) {
		if((arr[i]^prev) == 0) {
			while(count) {
				cout<<count--<<" ";
			}
		}
		count++;
		prev = arr[i];
	}
	while(count) {
		cout<<count--<<" ";
	}
}

int main(void) {
	bool arr[] = { 1, 0, 1, 0, 0, 1 };
	int N = sizeof(arr)/sizeof(arr[0]);
   	alternateSubarray(arr, N);
    return 0;
}