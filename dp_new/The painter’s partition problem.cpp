#include<bits/stdc++.h>
using namespace std;
#define vi vector<int> 
#define vii vector< vector<int> >
#define ULL unsigned long long
#define LL long long
#define L long 

int numbersOfPainters(vi arr, int maxLen) {
	int total = 0, numPainters = 1;
	for(int i = 0; i<arr.size();i++) {
		total += arr[i];
		if(total > maxLen) {
			total = arr[i];
			numPainters++;
		}
	}
	return numPainters;
}

int partition(vi arr, int k) {
	int low = *max_element(arr.begin(), arr.end());
	int high = accumulate(arr.begin(), arr.end(), 0);

	while(high > low) {
		int mid = low + (high-low)/2;
		int requiredPainters = numbersOfPainters(arr, mid);

		if(requiredPainters <= k)
			high = mid;
		else
			low = mid+1;
	}
	return low;
}
int main(void) {
	vi arr = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	int k = 3;
	cout<<partition(arr, k)<<endl;
    return 0;
}