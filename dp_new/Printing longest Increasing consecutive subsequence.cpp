#include<bits/stdc++.h>
using namespace std;

void longestSubsequence(int *A, int N) {
	vector<int> dp(N, 0);
	unordered_map<int, int> mp;

	int maxLen = INT_MIN;
	int index;
	for(int i = 0; i < N; i++) {
		if(mp.find(A[i]-1) != mp.end()) 
			dp[i] = 1 + dp[mp[A[i]-1]];
		else
			dp[i] = 1;
		mp[A[i]] = i;
		if(maxLen < dp[i]) {
			maxLen = dp[i];
			index = i;
		}
	}
	for(int val = A[index] - maxLen + 1; val <= A[index]; val++)
		cout<<val<<" ";
}

int main(void) {
	int A[] = { 3, 10, 3, 11, 4, 5, 6, 7, 8, 12 };
	int N = sizeof(A)/sizeof(A[0]);
	longestSubsequence(A, N);
	return 0;
}