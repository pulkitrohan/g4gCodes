#include<bits/stdc++.h>
using namespace std;
#define MAX_VAL 100000000
#define vii vector< vector<int> >

int dp[100][100];

int find(int i, int open, int N, vii &V) {
	if(open < 0)
		return MAX_VAL;
	if(i == N) {
		return (open == 0) ? 0 : MAX_VAL;
	}
	if(dp[i][open] != -1)
		return dp[i][open];
	dp[i][open] = min(
		V[i][1] + find(i+1, open+1, N, V),
		V[i][0] + find(i+1, open-1, N, V)
	);
	return dp[i][open];
}

int main(void) {

	vector< vector<int> > V = { 
		{5000, 3000},
		{6000, 2000},
		{8000, 1000},
		{9000, 6000}
	};
	memset(dp, -1, sizeof dp);
	cout<<find(1, 1, V.size(), V) + V[0][1]<<endl;
	return 0;
}
