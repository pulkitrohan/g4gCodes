#include<bits/stdc++.h>
using namespace std;
#define MOD 1000000007

int sumOdd(int N) {
	int first = 1;
	int second = 1;
	int dp[N+1] = {0};
	dp[0] = 0;
	dp[1] = 1;
	dp[2] = 2;
	dp[3] = 5;
	dp[4] = 10;
	dp[5] = 23;
	for(int i=6;i<=N;i++)
		dp[i] = (dp[i-1] + (4*dp[i-2])%MOD - (4*dp[i-3])%MOD + dp[i-4] - dp[i-5])%MOD;
	return dp[N];
}

int main(void) {
	int N = 6;
	cout<<sumOdd(N);
	return 0;
}
