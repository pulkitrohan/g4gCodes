#include<bits/stdc++.h>
using namespace std;
#define vii vector< vector<int> >

int maxPlus(vii &V) {
    int N = V.size();
    int M = V[0].size();
    int left[N+1][N+1], right[N+1][N+1], up[N+1][N+1], down[N+1][N+1];

    for(int i=0;i<N;i++) {
        for(int j=0;j<M;j++) {
            left[i][j] = max(0, (
                    j ? left[i][j-1] : 0)
                ) + V[i][j];
            up[i][j] = max(0, (
                    i ? up[i-1][j] : 0)
                ) + V[i][j];
        }
    }

    for(int i=N-1;i>=0;i--) {
        for(int j=M-1;j>=0;j--) {
            right[i][j] = max(0,
                j == M-1 ? 0 : right[i][j+1]
                ) + V[i][j];
            down[i][j] = max(0,
                i == N-1 ? 0 : down[i+1][j]
                ) + V[i][j];
        }
    }

    int ans = 0;
    for(int i=1;i<N-1;i++)
        for(int j=1;j<M-1;j++)
            ans = max(ans, V[i][j] + up[i-1][j] + down[i+1][j] + left[i][j-1] + right[i][j+1]);
    return ans;
}

int main(void) {
    vii V = { { 1, 2, 3 }, 
              {  -6, 1, -4, }, 
              { 1, 1, 1 },
              {  7, 8, 9 },
              {  6, 3, 2 }
              };
    cout<<maxPlus(V);
    return 0;
}
