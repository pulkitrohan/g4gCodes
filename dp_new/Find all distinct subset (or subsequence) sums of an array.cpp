#include<bits/stdc++.h>
using namespace std;
#define vi vector<int>

void find(vi &V) {
	int maxSum = 0;
	for(int x : V)
		maxSum += x;
	vector<bool> dp(maxSum+1, false);
	dp[V[0]] = true;
	for(int i=1;i<V.size();i++) 
		for(int j=maxSum+1;j>=1;j--)
			if(V[i] <= j)
				dp[j] = (V[i] == j) || dp[j] || dp[j-V[i]];
	cout<<"0 ";
	for(int i=1;i<dp.size();i++)
		if(dp[i])
			cout<<i<<" ";

}

int main(void)
{
	vi V = {2, 1, 3, 0, 4, 10};
	find(V);
	return 0;
}
