#include<bits/stdc++.h>
using namespace std;
#define INF 100000
#define vi vector<int>
#define vii vector< vector<int> >

int maxSubarraySumRepeated(int A[], int N, int K) {
	int max_so_far = INT_MIN,max_ending_here = 0;
	for(int i=0;i<N;i++) {
		max_ending_here = max(max_ending_here + A[i], A[i]);
		max_so_far = max(max_so_far, max_ending_here);
	}
	int totalSum = accumulate(A, A+N, 0);
	if(totalSum > 0 && K > 1)
		return totalSum * (K-2) + max_so_far;
	return max_so_far;
}

int main(void) {
	int A[] = {10, 20, -30, -1};
	int N = sizeof(A)/sizeof(A[0]);
	int K = 3;
	cout<<maxSubarraySumRepeated(A, N, K);
	return 0;
}