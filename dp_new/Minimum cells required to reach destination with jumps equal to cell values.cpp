#include<bits/stdc++.h>
using namespace std;
#define vi vector<int> 
#define vii vector< vector<int> >
#define ULL unsigned long long
#define LL long long
#define L long 
#define MAX 10000

int minCells(vii mat, int N, int M) {
	vii dp(N, vi(M, 10000));
	dp[0][0] = 1;
	for(int i=0;i<N;i++) {
		for(int j=0;j<M;j++) {
			if(dp[i][j] != MAX && (i + mat[i][j] < N))
				dp[i + mat[i][j]][j] = min(dp[i][j] + 1, dp[i + mat[i][j]][j]);
			if(dp[i][j] != MAX && (j + mat[i][j] < M))
				dp[i][j + mat[i][j]] = min(dp[i][j] + 1, dp[i][j + mat[i][j]]);
		}
	}
	if(dp[N-1][M-1] != MAX)
		return dp[N-1][M-1];
	return -1;
}

int main(void) {
	vii mat = { { 2, 3, 2, 1, 4 },
                { 3, 2, 5, 8, 2 },
                { 1, 1, 2, 2, 1 } };
    int N = 3, M = 5;
    cout<<minCells(mat, N, M);
    return 0;
}