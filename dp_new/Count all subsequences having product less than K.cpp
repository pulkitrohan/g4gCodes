#include<bits/stdc++.h>
using namespace std;
#define vi vector<int> 
#define vii vector< vector<int> >
#define ULL unsigned long long
#define LL long long
#define L long 

int productSubSeqCount(vi &A, int k) {
	int N = A.size();
	vii dp(k+1, vi(N+1, 0));

	for(int i=1;i<=k;i++) {
		for(int j=1;j<=N;j++) {
			dp[i][j] = dp[i][j-1];
			if((A[j-1] <= i))
				dp[i][j] += dp[i/A[j-1]][j-1] + 1;
		}
	}
	return dp[k][N];
}

int main(void) {
	vi A = { 4, 8, 7, 2 };
	int k = 10;
    cout<<productSubSeqCount(A, k)<<endl;
    return 0;
}