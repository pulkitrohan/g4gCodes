#include<bits/stdc++.h>
using namespace std;

int minimalSteps(string S) {
	int N = S.length();
	if(N == 0 || N == 1)
		return 1;
	vector<int> dp(N+1, INT_MAX);
	dp[0] = 1;
	string A = "", B = "";
	A += S[0];
	for(int i = 1; i < N; i++) {
		A += S[i];
		B = S.substr(i+1, i+1);
		dp[i] = min(dp[i], dp[i-1] + 1);
		if(A == B)
			dp[2*i+1] = min(dp[i] + 1, dp[2*i+1]);
	}
	return dp[N-1];
}

int main(void) {
	string A = "aaaaaaaa";
	cout<<minimalSteps(A)<<endl;
	return 0;
}