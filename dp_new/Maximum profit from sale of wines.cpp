#include<bits/stdc++.h>
using namespace std;

#define vii vector< vector<int > >

int maxProfitUtil(int *price, int N, int begin, int end, vii &dp, vii &sell) {
	if(dp[begin][end] != -1)
		return dp[begin][end];

	int year = N - (end-begin);
	if(begin == end)
		return year * price[begin];

	int x = price[begin] * year + maxProfitUtil(price, N, begin+1, end, dp, sell);
	int y = price[end] * year + maxProfitUtil(price, N, begin, end-1, dp, sell);

	int ans = max(x, y);
	sell[begin][end] = (x >= y) ? 0 : 1;
	return (dp[begin][end] = ans);
}

int maxProfit(int *price, int N) {

	vii dp(N , vector<int>(N, -1));
	vii sell(N, vector<int>(N, -1));
	int ans = maxProfitUtil(price, N, 0, N-1, dp, sell);
	int i = 0, j = N-1;
	while( j >= i) {
		if(sell[i][j] == 0) {
			cout<< "beg ";
			i++:
		}
		else {
			cout<< "end ";
			j--;
		}
	}
	cout<<endl;
	return ans;
}

int main(void) {
	int prices[] = { 2, 4, 6, 2, 5 };
	int N = sizeof(prices)/sizeof(prices[0]);
	cout<<maxProfit(price, N);
	return 0;
}