#include<bits/stdc++.h>
using namespace std;
#define vi vector<int> 
#define vii vector< vi >
#define ULL unsigned long long
#define LL long long
#define L long 


int countStrs(int N) {
	LL dp[N+1][27];
	memset(dp, 0, sizeof(dp));
	for(int i = 0;i < 26; i++)
		dp[1][i] = 1;
	for(int i=2;i<=N;i++)
		for(int j=0;j< 26; j++)
			if(j == 0)
				dp[i][j] = dp[i-1][j+1];
			else
				dp[i][j] = dp[i-1][j-1] + dp[i-1][j+1];
	LL sum = 0;
	for(int i = 0;i < 26;i++) {
		sum += dp[N][i];
	}
	return sum;
}

int main(void) {
	int N = 3;
	cout<<countStrs(N);
	return 0;
}