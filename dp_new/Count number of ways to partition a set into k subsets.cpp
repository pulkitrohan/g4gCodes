#include<bits/stdc++.h>
using namespace std;

int countP(int N, int K) {
	int dp[N+1][K+1];
	memset(dp, 0, sizeof(dp));

	for(int i=1;i<=N;i++) {
		for(int j=1;j<=K;j++) {
			if(j == 1 || i == j)
				dp[i][j] = 1;
			else
				dp[i][j] = dp[i-1][j-1] + j*dp[i-1][j];
		}
	}
	return dp[N][K];
}

int main(void) {
	cout<<countP(5, 2)<<endl;
	return 0;
}