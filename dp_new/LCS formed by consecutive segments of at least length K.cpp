#include<bits/stdc++.h>
using namespace std;
#define INF 100000
#define vi vector<int>
#define vii vector< vector<int> >

int longestSubsequenceCommonSegment(string A, string B, int K) {
	int N = A.length();
	int M = B.length();
	vii LCS(N+1, vector<int>(M+1, 0));
	vii count(N+1, vector<int>(M+1, 0));

	for(int i=1;i<=N;i++) {
		for(int j=1;j<=M;j++) {
			LCS[i][j] = max(LCS[i-1][j], LCS[i][j-1]);

			if(A[i-1] == B[j-1])
				count[i][j] = 1 + count[i-1][j-1];
			if(count[i][j] >= K) {
				for(int a=K;a<=count[i][j];a++) 
					LCS[i][j] = max(LCS[i][j], LCS[i-a][j-a] + a);
			}
		}
	}
	return LCS[N][M];
}

int main(void) {
	string A = "aggasdfa";
	string B = "aggajasdfa";
	int K = 4;
	cout<<longestSubsequenceCommonSegment(A, B, K);
	return 0;
}