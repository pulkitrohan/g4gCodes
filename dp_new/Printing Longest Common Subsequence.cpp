#include<bits/stdc++.h>
using namespace std;
#define MAX 100
int dp[MAX+1][MAX+1];

set<string> findLCS(string X, string Y, int M, int N) {
	set<string> S;

	if(M == 0 || N == 0) {
		S.insert("");
		return S;
	}

	if(X[M-1] == Y[N-1]) {
		set<string> temp = findLCS(X, Y, M-1, N-1);
		for(string str : temp)
			S.insert(str + X[M-1]);
	}
	else {

		if(dp[M-1][N] >= dp[M][N-1]) 
			S = findLCS(X, Y, M-1, N);

		if(dp[M-1][N] <= dp[M][N-1]) {
			set<string> temp = findLCS(X, Y, M, N-1);
			S.insert(temp.begin(), temp.end());
		}

	}

	return S;

}

void LCS(string X, string Y, int M, int N) {
	int dp[M+1][N+1];
	memset(dp, 0, sizeof(dp));
	for(int i=0;i<=M;i++) {
		for(int j=0;j<=N;j++) {
			if(i == 0 || j == 0)
				dp[i][j] = 0;
			else if(X[i-1] == Y[j-1])
				dp[i][j] = 1 + dp[i-1][j-1];
			else
				dp[i][j] = min(dp[i-1][j], dp[i][j-1]);
		}
	}

	set<string> S = findLCS(X, Y, M, N);
	for(string str : S) {
		cout<<str<<endl;
	}
} 

int main(void)
{
	string X = "AGTGATG";
	string Y = "GTTAG";
	int M = X.length();
	int N = Y.length();
	LCS(X, Y, M, N);
	return 0;
}