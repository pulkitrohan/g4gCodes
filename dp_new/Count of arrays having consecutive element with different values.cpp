#include<bits/stdc++.h>
using namespace std;
#define vi vector<int> 
#define vii vector< vector<int> >
#define ULL unsigned long long
#define LL long long
#define L long 

int countArray(int N, int k, int x) {

	vi dp(N+1, 0);
	dp[1] = 1;
	for(int i=2;i<N;i++) {
		dp[i] = dp[i-2]*(k-1) + dp[i-1]*(k-2);
	}
	return (x == 1) ? dp[N-2] * (k-1) : dp[N-1];
}
int main(void) {
	int N = 4, k = 3, x = 2;
	cout<<countArray(N, k , x)<<endl;
    return 0;
}