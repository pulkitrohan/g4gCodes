#include<bits/stdc++.h>
using namespace std;
#define mpiii map< pair<int, int>, int >
#define MOD 1000000007

int maxWeightUtil(int wt[], int val[], int K, mpiii &mp, int last, int diff) {

	if(last == -1)
		return (diff == 0) ? 0 : INT_MIN;
	
	pair< int, int > temp = make_pair(last, diff);
	if(mp.find(temp) != mp.end())
		return mp[temp];

	mp[temp] = max(
					maxWeightUtil(wt, val, K, mp, last-1, diff),
					wt[last] + maxWeightUtil(wt, val, K, mp, last-1, diff + wt[last] - val[last]*K)
			   );
	return mp[temp];
}


int maxWeight(int wt[], int val[], int N, int K) {
	mpiii mp;
	return maxWeightUtil(wt, val, K, mp, N-1, 0);
}

int main(void) {
	int wt[] = { 4, 8, 9 };
	int val[] = { 2, 4, 6 };
	int N = sizeof(wt)/sizeof(wt[0]);
	int K = 2;
	cout<<maxWeight(wt, val, N, K);
	return 0;
}