#include<bits/stdc++.h>
using namespace std;
#define vi vector<int> 
#define vii vector< vector<int> >
#define ULL unsigned long long
#define LL long long
#define L long 


int LCS(vi &A, vi &B, vector< vii > &dp, int N, int M, int k) {
	if(k < 0)
		return -1e7;
	if(N < 0 || M < 0)
		return 0;
	if(dp[N][M][k] != -1)
		return dp[N][M][k];

	int ans = max(LCS(A, B, dp, N-1, M, k), LCS(A, B, dp, N, M-1 , k));

	if(A[N] == B[M])
		ans = max(ans, 1 + LCS(A, B, dp, N-1, M-1, k));

	ans = max(ans, 1 + LCS(A, B, dp, N-1, M-1, k-1));

	return (dp[N][M][k] = ans);
}

int main(void) {
	int k = 1;
	vi A = { 1, 2, 3, 4, 5 };
	vi B = { 5, 3, 1, 4, 2 };
	int N = A.size(), M = B.size();
	vector< vii > dp(N+1, vii(M+1, vi(k+1, -1)));
	cout<<LCS(A, B, dp, N-1, M-1, k);

    return 0;
}