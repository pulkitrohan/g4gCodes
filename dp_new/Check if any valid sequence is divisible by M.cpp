#include<bits/stdc++.h>
using namespace std;
#define vi vector<int>
#define vii vector< vector<int> >

int isPossible(int N, int index, int sum, int M, vi &V, vii &dp) {
	sum = sum%M;

	if(index == N)
		return sum == 0;
	if(dp[index][sum] != -1)
		return dp[index][sum];

	bool placeAdd = isPossible(N, index+1, sum+V[index], M, V, dp);
	bool placeMinus = isPossible(N, index+1, sum-V[index], M, V, dp);
	return dp[index][sum] = placeAdd || placeMinus;
}

int main(void) {
	vi V = {1,2,3,4,6};
	int M = 4, N = V.size();
	vii dp(N, vi(M+1, -1));
	cout<<isPossible(N, 1, V[0], M, V, dp);
	return 0;
}