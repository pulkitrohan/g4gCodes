#include<bits/stdc++.h>
using namespace std;
#define INF 100000
#define vi vector<int>
#define vbb vector< vector<bool> >

bool check(string A, string B) {
	int N = A.length();
	int M = B.length();

	vbb dp(N+2, vector<bool>(M+1, false));
	dp[0][0] = true;
	for(int i=0;i<N;i++) {
		for(int j=0;j<=M;j++) {
			if(dp[i][j]) {
				dp[i+1][j+1] = j < M && toupper(A[i]) == B[j];
				dp[i+1][j] = !isupper(A[i]);
			}
		}
	}
	return dp[N][M];
}

int main(void) {
	string A = "daBcd";
	string B = "ABC";
	cout<<check(A, B);
	return 0;
}