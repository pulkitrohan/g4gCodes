#include<bits/stdc++.h>
using namespace std;

int findSum(vector<int> &V) {
	int sum = 0;
	for(int x : V)
		sum += x;
	return sum;
}

void printMaxSumIS(int A[], int N) {
	vector< vector<int> > L(N);
	L[0].push_back(A[0]);
	for(int i=1;i<N;i++) {
		for(int j=0;j<i;j++) {
			if(A[i] > A[j] && findSum(L[i]) < findSum(L[j]))
				L[i] = L[j]; 
		}
		L[i].push_back(A[i]);
	}
	vector<int> ans = L[0];
	for(vector<int> x : L) {
		if(findSum(x) > findSum(ans))
			ans = x;
	}
	for(int i : ans)
		cout<<i<<" ";
	cout<<endl;
}

int main(void)
{
	int A[] = {3, 2, 6, 4, 5, 1};
	int N = sizeof(A)/sizeof(A[0]);
	printMaxSumIS(A,N);
	return 0;
}