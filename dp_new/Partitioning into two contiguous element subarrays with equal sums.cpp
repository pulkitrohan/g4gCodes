#include<bits/stdc++.h>
using namespace std;

void findMinElement(int *A, int N) {
	int prefixSum[N+1];
	int suffixSum[N+1];

	prefixSum[0] = A[0];
	for(int i = 1; i < N; i++)
		prefixSum[i] = prefixSum[i-1] + A[i];

	suffixSum[N-1] = A[N-1];
	for(int i = N - 2; i >= 0; i--)
		suffixSum[i] = suffixSum[i+1] + A[i];

	int minDiff = suffixSum[0];
	int pos;
	for(int i = 0; i < N-1; i++) {
		if(abs(prefixSum[i] - suffixSum[i+1]) < minDiff) {
			minDiff = abs(prefixSum[i] - suffixSum[i+1]);
			pos = (prefixSum[i] > suffixSum[i+1]) ? i+1 : i;
		}
	}
	cout<<"Min Diff:: "<<minDiff << endl;
	cout<<"Pos:: "<<pos;
}

int main(void) {
	int A[] = { 5, 4  };
	int N = sizeof(A)/sizeof(A[0]);
	findMinElement(A, N);
	return 0;
}