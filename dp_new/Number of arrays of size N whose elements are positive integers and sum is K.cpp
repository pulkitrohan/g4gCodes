#include<bits/stdc++.h>
using namespace std;

int binomialCoeff(int N, int K) {
	int dp[K+1];
	memset(dp, 0, sizeof(dp));
	dp[0] = 1;
	for(int i = 1; i <= N; i++)
		for(int j = min(i,K); j > 0; j--)
			dp[j] += dp[j-1];
	return dp[K];
}


int countArray(int N, int K) {
	if( N > K)
		return 0;
	if (N == K)
		return 1;
	return binomialCoeff(K-1, N-1);
}

int main(void) {
	int N = 2, K = 3;
	cout<<countArray(N, K)<<endl;
	return 0;
}