#include<bits/stdc++.h>
using namespace std;

const int MAX = 10;
int F[MAX][MAX];
int C[MAX];

int noOfAssignments(string &S, int &N, int i, int c_x) {
	if(F[i][c_x] != -1)
		return F[i][c_x];
	if(i == N) {
		return (F[i][c_x] = (c_x == 0));
	}

	int c_y = C[i] - c_x;
	F[i][c_x] = 0;
	if(S[i] == '(') {
		F[i][c_x] = noOfAssignments(S, N, i+1, c_x+1) +
					noOfAssignments(S, N, i+1, c_x);
		return F[i][c_x];
	}

	if(c_x) 
		F[i][c_x] += noOfAssignments(S, N, i+1, c_x-1);
	if(c_y)
		F[i][c_x] += noOfAssignments(S, N, i+1, c_x);
	return F[i][c_x];
}

int main(void) {
	string S = "(())";
	int N = S.length();
	memset(F, -1, sizeof(F));
	C[0] = 0;
	for(int i=0;i<N;i++)
		C[i+1] = (S[i] == '(') ? C[i] + 1 : C[i] - 1;
	cout<<noOfAssignments(S, N, 0, 0);

	return 0;
}
