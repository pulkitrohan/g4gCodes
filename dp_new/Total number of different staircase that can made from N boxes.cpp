#include<bits/stdc++.h>
using namespace std;

int countStaircases(int N) {
	int dp[N+2][N+2];
	memset(dp, 0, sizeof(dp));
	dp[3][2] = dp[4][2] = 1;

	for(int i=5;i<=N;i++) {
		for(int j=2;j<=i;j++) {
			if(j == 2)
				dp[i][j] = dp[i-j][j] + 1;
			else
				dp[i][j] = dp[i-1][j] + dp[i-j][j-1];
		}
	}
	int ans = 0;
	for(int i=1;i<=N;i++)
		ans += dp[N][i];
	return ans;
}

int main(void) {
	int N = 6;
	cout<<countStaircases(N);
	return 0;
}