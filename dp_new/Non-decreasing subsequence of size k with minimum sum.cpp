#include<bits/stdc++.h>
using namespace std;
#define vii vector< vector<int> > 
const int INF = 2e9;

int solve(int A[], vii &dp, int i, int k) {
	if(dp[i][k] != -1)
		return dp[i][k];

	if(i < 0)
		return INF;

	int ans = INF;

	if(k == 1) {
		for(int j=0;j<i;j++)
			ans = min(ans, A[j]);
		return ans;
	}

	for(int j=0;j<i;j++) {

		if(A[i] >= A[j])
			ans = min(ans, min(solve(A, dp, j, k), solve(A, dp, j, k-1) + A[i]) );
		else
			ans = min(ans, solve(A, dp, j, k));

	}
	return (dp[i][k] = ans);
}


int main(void) {
	int A[] = { 58, 12, 11, 12, 82, 30, 20, 77, 16, 86 };
	int N = sizeof(A)/sizeof(A[0]);
	int K = 4;
	vector< vector<int> > dp(N+1, vector<int>(K+1, -1));
	cout<<solve(A, dp, N-1, K)<<endl;
	return 0;
}