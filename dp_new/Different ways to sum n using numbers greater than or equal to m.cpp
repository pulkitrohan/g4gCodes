#include<bits/stdc++.h>
using namespace std;
#define vi vector<int> 
#define vii vector< vector<int> >
#define ULL unsigned long long
#define LL long long
#define L long 

int numberOfWays(int N, int M) {

	vii dp(N+2, vi(N+2,0));
	dp[0][N+1] = 1;

	for(int num=N; num >= M; num--) {
		for(int sum = 0; sum <= N; sum++) {
			dp[sum][num] = dp[sum][num+1];
			if(sum >= num) 
				dp[sum][num] += dp[sum-num][num];
		}
	}
	return dp[N][M];
}

int main(void) {
	int N = 3, M = 1;
	cout<<numberOfWays(N, M)<<endl;
    return 0;
}