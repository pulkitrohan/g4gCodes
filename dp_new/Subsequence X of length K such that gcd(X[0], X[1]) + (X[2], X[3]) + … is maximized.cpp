#include<bits/stdc++.h>
using namespace std;
const int MAX = 100;

// https://www.geeksforgeeks.org/subsequence-x-of-length-k-such-that-gcdx0-x1-x2-x3-is-maximized/
int recur(int index, int count, int last, int A[], int N, int k, int dp[][MAX]) {
	if(count == k) 
		return 0;
	if(index == N)
		return -1e9;

	if(dp[index][count] != -1)
		return dp[index][count];

	int ans = 0;

	for(int i=index;i<N;i++) {
		if(count%2 == 0)
			ans = max(ans, recur(i+1, count + 1, i, A, N, k, dp));
		else
			ans = max(ans, __gcd(A[last], A[i]) + recur(i+1, count + 1, 0, A, N, k, dp));
	}
	return dp[index][count] = ans;
}


int main(void) {
	int A[] = {4, 5, 3, 7, 8, 10, 9, 8};
	int N = sizeof(A)/sizeof(A[0]);
	int k = 4;
	int dp[N][MAX];
	memset(dp, -1, sizeof dp);
	cout<<recur(0, 0, 0, A, N, k, dp);
	return 0;
}