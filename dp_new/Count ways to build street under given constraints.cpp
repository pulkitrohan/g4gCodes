#include<bits/stdc++.h>
using namespace std;
#define vii vector< vector<int> >

long countWays(int N) {
	long dp[N+1][2];
	dp[1][0] = 1;
	dp[1][1] = 2;

	for(int i=2;i<=N;i++) {
		dp[i][0] = dp[i-1][0] + dp[i-1][1];
		dp[i][1] = dp[i-1][0]*2 + dp[i-1][1];
	}
	return dp[N][0] + dp[N][1];
}

int main(void) {
	int N = 5;
	cout<<countWays(N)<<endl;
	return 0;
}