#include<bits/stdc++.h>
using namespace std;

#define LL long long
#define MOD 1000000007

LL dp[1001][1001];

void preProcess() {
	dp[0][0] = 1;
	for(int i=1;i<=1000;i++) {
		dp[i][0] = 1;
		for(int j=1;j<i;j++) 
			dp[i][j] = (dp[i-1][j] + dp[i-1][j-1]) % MOD;
		dp[i][i] = 1;
	}
}

LL powmod(LL x, LL N) {
	if(N == 0)
		return 1;
	LL val = powmod(x, N/2);
	val = (val * val) % MOD;
	return (N % 2) ? (val * x) % MOD : val;
}

LL countSubset(int A[] ,int N) {
	LL ans = powmod(2, N-1);
	sort(A, A + N);
	for(int i = 0; i < N; i++) {
		int j = i + 1;
		while(j < N && A[i] == A[j]) {
			int right = N - 1 - j;
			int left = i;
			ans = (ans + dp[left + right][left]) % MOD;
			j++;
		}
	}
	return ans;
}

int main(void) { 
	preProcess();
	int A[] = { 2, 3, 2 };
	int N = sizeof(A)/sizeof(A[0]);
	cout<<countSubset(A, N);
	return 0;
}