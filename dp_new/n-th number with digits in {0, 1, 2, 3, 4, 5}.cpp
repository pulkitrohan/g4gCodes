#include<bits/stdc++.h>
using namespace std;

vector<int> baseConversion(int num, int base) {
	if(num == 0)
		return { 0 };
	vector<int> ans;
	while(num > 0) {
		ans.push_back(num%base);
		num /= base;
	}
	return ans;
}

int main(void) {
	int N = 10;
	vector<int> ans = baseConversion(N-1, 6);
	for(int i=ans.size()-1; i >= 0; i--)
		cout<<ans[i];
	return 0;
}