#include<bits/stdc++.h>
using namespace std;
#define vi vector<int>

int findMaxVal(vi A, int num, int maxLimit) {
	int N = A.size();
	bool dp[A.size()][maxLimit+1];
	for(int i = 0; i < A.size(); i++) { 
		for(int val = 0; val <= maxLimit; val++) { 
			if(i == 0) {
				dp[i][val] = (num-A[i] == val || num + A[i] == val);
			}
			else {
				if(val - A[i] >= 0 && val + A[i] <= maxLimit)
					dp[i][val] = dp[i-1][val-A[i]] || dp[i-1][val+A[i]];
				else if(val-A[i] >= 0)
					dp[i][val] = dp[i-1][val-A[i]];
				else if(val + A[i] <= maxLimit)
					dp[i][val] = dp[i-1][val+A[i]];
				else
					dp[i][val] = 0;
			}
		}
	}
	for(int val = maxLimit; val >= 0; val--)
		if(dp[N-1][val])
			return val;
	return -1;
}

int main(void) {
	vector<int> A = { 3, 10, 6, 4, 5 };
	int maxLimit = 15, num = 1;
	cout<<findMaxVal(A, num, maxLimit);
	return 0;
}