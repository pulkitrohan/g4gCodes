#include<bits/stdc++.h>
using namespace std;
#define vii vector< vector<int> > 
const int INF = 2e9;

vii constructDP(string S) {
	int N = S.length();
	vii dp(N, vector<int> (N , 0));
	vii isPalin(N, vector<int> (N , 0));

	for(int start = N-1; start>=0; start--) { 
		dp[start][start] = 1;
		isPalin[start][start] = 1;
		for(int end = start+1; end < N;end++) {
			isPalin[start][end] = (S[start] == S[end] && ((end == start + 1) || isPalin[start+1][end-1]));
			dp[start][end] = dp[start][end-1] + dp[start+1][end] - dp[start+1][end-1] + isPalin[start][end];
		}
	}
	return dp;
}

int main(void) {
	string S = "xyaabax";
	vii dp = constructDP(S);
	int l = 3, r = 5;
	cout<<dp[l][r]<<endl;
	return 0;
}