// https://codeforces.com/contest/455/problem/A

include<bits/stdc++.h>
using namespace std;
#define vb vector<bool>
#define pii pair<int,int>
#define vii vector< vector<int> >
#define vbb vector< vector<bool> >
#define MAX 10000

int maxCost(int A[], int N, int l, int r) {
	int mx = 0;
	for(int i=0;i<N;i++)
		mx = max(mx, A[i]);
	int count[mx+1] = {0};
	for(int i=0;i<N;i++)
		count[A[i]]++;
	int dp[mx+1];
	dp[0] = 0;
	l = min(l, r);
	for(int i=1;i<=mx;i++) {
		dp[i] = i * count[i];
		int k = max(i-l-1, 0);
		dp[i] += dp[k];
		dp[i] = max(dp[i], dp[i-1]);
	}
	return dp[mx];
}

int main(void) {
	int A[] = { 2, 1, 2, 3, 2, 2, 1 };
	int l = 1, r = 1; 
	int N = sizeof(A)/sizeof(A[0]);
	cout<<maxCost(A, N, l, r);
    return 0;
}
